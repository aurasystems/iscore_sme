<?php

function save_audit($action) {
    $CI = &get_instance();
    $CI->db->set('id',  incremented("audits"));
    $CI->db->set('user_id',$CI->session->userdata('user_id') ? $CI->session->userdata('user_id') : 0);
    $CI->db->set('action',$action);
    $CI->db->set('"timestamp"',"to_date('".date('Y-m-d H:i:s')."','YYYY-MM-DD HH24:MI:SS')",FALSE);
    $CI->db->insert('audits');
//    $CI->db->insert('audits', array('user_id' => $CI->session->userdata('user_id') ? $CI->session->userdata('user_id') : 0
//        , 'action' => $action,
//        'timestamp' => date('Y-m-d H:i:s')));

    if (clear_expired_logs()) {
        reconstruct_txt_file();
        generate_cef($action);
    } else {

        // write this single change to the file ....
        $myfile = fopen("audit_trail/changes.txt", "a");
        if (!$myfile) {
            return;
        }
        $by = '';
        if ($CI->session->userdata('user_id')) {
            $by = ' by ' . $CI->session->userdata('user_first_name') . ' ' .
                    $CI->session->userdata('user_last_name');
        }
        $txt = "\r\n" . $action . $by
                . ' at ' . date('Y-m-d H:i:s');
        fwrite($myfile, $txt);
        fclose($myfile);
        generate_cef($action);
    }
}

function clear_expired_logs() {
    $CI = &get_instance();

    $CI->db->select('audits_expiry');
    $days = $CI->db->get('back_settings')->row();
    if ($days && $days->audits_expiry != 0) {
        $now = date_create(date('Y-m-d'));
        date_sub($now, date_interval_create_from_date_string($days->audits_expiry . " days"));
        $date = date_format($now, "Y-m-d");
//        $CI->db->delete('audits', array('date(timestamp) <=' => $date));
        $CI->db->where('"'.'timestamp'.'" <=',"to_date('".$date."','YYYY-MM-DD HH24:MI:SS')",FALSE);
        $CI->db->delete('audits');
    }
    return $CI->db->affected_rows();
}

function reconstruct_txt_file() {
    $CI = &get_instance();

    // write to the file
    $myfile = fopen("audit_trail/changes.txt", "w");
    if (!$myfile) {
        return;
    }
    $CI->load->model('admin/Reports_model');
    $audits = $CI->Reports_model->get_logs();

    $txt = '';
    foreach ($audits as $audit) {
//        print_r($audit);die;
        $txt .= $audit->action . ' by ' . $audit->first_name . ' ' . $audit->last_name . ' at ' . $audit->timestamp . "\r\n";
    }
    fwrite($myfile, $txt);
    fclose($myfile);
}

function generate_cef($action){
    $CI = &get_instance();
    $audit="";
    $path= "audit_trail/cef/".date('Y-m-d').".cef";
    if (!file_exists($path)) {
        $audit.= date("M d H:i:s")." ".escape_cef($_SERVER["SERVER_NAME"])." CEF:0| ";
       }
    $myfile = fopen($path, "a");
    if (!$myfile) {
        return;
    }
    $by = '';
//    escape \= \\ 
//    \n and \r within the value not key
//    utf-encode msg
    if ($CI->session->userdata('user_id')) {
        $by = ' by ' . $CI->session->userdata('user_first_name') . ' ' .
                $CI->session->userdata('user_last_name');
    }
    $action = $action .$by. ' at ' . date('Y-m-d H:i:s');
    
      $audit.="dvchost=".escape_cef($_SERVER["SERVER_NAME"])." dvc=".escape_cef($_SERVER["REMOTE_ADDR"])." msg=".escape_cef($action)." request=".escape_cef(current_url())." rt=".escape_cef(date("M d H:i:s"))." ";
       if ($CI->session->userdata('user_id')) {
       $audit.="suid=".escape_cef($CI->session->userdata('user_id'))." ";
       }
       
    $audit= utf8_encode($audit);
    
    fwrite($myfile, $audit);
    fclose($myfile);
}

function escape_cef($string){
   $string= str_replace('=', '\=', $string);
   $string=str_replace('|', '\|', $string);
   return $string;
    
}
