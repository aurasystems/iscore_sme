<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function get_audit_msg() {
    $audits = array(
        "login" => "",
        "login_failed" => "",
        "case_generated" => "",
        "case_approved" => "",
        "password_changesd" => ""
    );

    return $audits;
}

function test() {
    echo get_arr();
}

function check_login() {
    $CI = &get_instance();
    if (!$CI->session->userdata("user_id")) {
        redirect(base_url("admin"));
    }
}

function check_parameter($param) {
    //checks if the parameter exists
    if (empty($param)) {
        redirect(base_url("Welcome/permission_denied"));
    }
}

function get_options_dynamically($key) {
    $CI = &get_instance();
    if ($CI->session->userdata('language') == 'arabic') {
        $select_field = 'arabic_value';
    } else {
        $select_field = 'value';
    }
    $CI->db->select("code," . $select_field . " as value");
    $CI->db->where($select_field . ' !=', '""');
    return $CI->db->get_where($key, array('deleted' => 0))->result();
}

function get_value_from_code($key, $code, $check_lang = 0) {
    $CI = &get_instance();
    $select_field = 'value';
    if ($check_lang) {
        if ($CI->session->userdata('language') == 'arabic') {
            $select_field = 'arabic_value';
        } else {
            $select_field = 'value';
        }
    }
    $CI->db->select($select_field . " as value");
    $CI->db->where('code', $code);
    $row = $CI->db->get_where(strtolower($key), array('deleted' => 0))->row();
    if ($row) {
        return $row->value;
    }
    return '';
}

function get_options($key) {
    $data['borrower_information_C6'] = array(
        "Alexandria (Al Iskandariyah)",
        "Assiut (Asyut)",
        "Aswan",
        "Bani Souwaif (Bani Suwayf)",
        "Cairo (Al Qahirah)",
        "Dakahliya (Ad Daqahliyah)",
        "Damietta (Dimyat)",
        "El-Beheira (Al Buhayrah)",
        "Fayoum (Al Fayyum)",
        "Gharbeya (Al Gharbiyah)",
        "Giza (Al Jizah)",
        "Ismailia (Al Isma\'iliyah)",
        "Kafr el Shiekh (Kafr ash Shaykh)",
        "Kalyobiya (Al Qalyubiyah)",
        "Luxor (Al Uqsur)",
        "Matrouh (Matruh)",
        "Menia (Al Minya)",
        "Monofiya (Al Minufiyah)",
        "New Valley (Al Wadi al Jadid)",
        "North Sinai (Shamal Sina\')",
        "Port-Said (Bur Sa\'id)",
        "Qena (Qina)",
        "Sharkia (Ash Sharqiyah)",
        "Sohag (Suhaj)",
        "South Sinai (Janub Sina\')",
        "Suez (As Suways)",
        "The Red Sea (Al Bahr al Ahmar)",
    );


    $data['borrower_information_C9'] = array(
        "000111-growing grains , Legumes and oilseeds ",
        "000112-Growing rice",
        "000113-Planting vegetables , water melons and tubers",
        "000114-Growing sugar crops",
        "000115-Growing tobacco",
        "000116-Growing fiber crops",
        "000119-Growing other temporary crops",
        "000121-Growing grapes",
        "000122-Growing tropical and semi tropical fruits",
        "000123-Growing citric fruits ( citrus fruits )",
        "000124-Growing stone fruits",
        "000125-Growing other fruit trees and nuts",
        "000126-Growing oily fruits",
        "000127-Growing juicy crops",
        "000128-Growing spicy , aromatic , medical and pharmaceutical Crops",
        "000129-Growing other permanent crops",
        "000130-Reproductive plants",
        "000141-Breeding cattle ( buffalos and cows )",
        "000142-Breeding horses and other mares",
        "000143-Breeding camels",
        "000144-Raising sheep , goats , with its products",
        "000145-Raising pigs",
        "000146-Raising poultry",
        "000149-Raising other animals",
        "000150-Growing crops doing with raising animals",
        "000161-Activities of supporting crops production",
        "000162-Activities of supporting animal production",
        "000163-Post harvest activities",
        "000164-Preparing and growing seeds for reproduction activities",
        "000170-Hunting , trapping with relevant services",
        "000210-Agriculture services with other forest activities",
        "000220-Cutting trees wood",
        "000230-Collecting wild plants",
        "000240-Supporting services in forestry and cutting trees fields ",
        "000311-Fishing in marine water",
        "000312-Fishing in fresh water",
        "000321-breeding  marine aquatic animals",
        "000322-breeding  fresh water animals",
        "000510-Mining solid coal",
        "000520-Mining Lignite coal",
        "000610-Extracting oil ( crude oil ) ",
        "000620-Extracting natural gas",
        "000710-Mining iron raw material ( ores ) ",
        "000721-Mining uranium , Lithium raw material ( ores )",
        "000722-Mining non iron raw material ( ores )",
        "000810-Making use of quarries for extracting stones , sands and clay",
        "000891-Extracting chemical minerals and natural fertilizers",
        "000892-Extracting ( compacted ) slag and accumulation",
        "000893-Extracting salt",
        "000899-Other mining activities",
        "000910-Supporting services activities of petroleum and natural gas extraction",
        "000990-Supporting services activities of mining and quarries exploitation",
        "001010-Preparing, preserving meats with its products",
        "001020-Preparing , preserving fishes with canning fish products",
        "001030-Preparing , preserving fruits and vegetables",
        "001040-Manufacturing vegetarian , animals oils and fats",
        "001050-Manufacturing milk products",
        "001061-grinding grains industry with its products",
        "001062-Manufacturing Starch with its products",
        "001071-Bakeries products industry",
        "001072-Sugar and refining industry",
        "001073-Cacao , chocolate , and candies industry",
        "001074-Macaroni , and starch products industry",
        "001075-Meals , and ready plates industry",
        "001079-Manufacturing other unclassified food products in other place",
        "001080-animal forage industry",
        "001101-Distillation , refining , and mixing alcoholic beverages producing ethanol from fermented substances",
        "001102-Wines industry",
        "001103-Malt , alcohol industry as a  derivate  from ( beer )",
        "001104-Non alcoholics , and mineral water industry",
        "001200-Tobacco products industry",
        "001311-Weaving , preparing fibers industry",
        "001312-Weaving textile",
        "001313-Accomplishing textile preparation",
        "001321-Fabrics , and types of knitting and crochet industry",
        "001322-Ready fabrics  industry except for clothes",
        "001323-Carpets and clems industry",
        "001324-Ropes , and nets industry",
        "001329-Manufacturing other unclassified fabrics in other place",
        "001410-Manufacturing , and tailoring readymade clothes ( except for clothes made of fur ) ",
        "001420-Manufacturing fur articles",
        "001430-Tailoring clothes and other articles from knitting and crochet",
        "001511-Manufacturing , preparing , dying leather and fur",
        "001512-Manufacturing suitcases , handbags , and saddles",
        "001520-Manufacturing chooses",
        "001610-Sawing and leveling wood",
        "001621-Manufacturing laminated , compressed strengthened, and granular wood boards with other types of wood boards",
        "001622-Manufacturing carpentry products used in building and construction",
        "001623-Manufacturing wooden utensils",
        "001629-Manufacturing other wooden products   corkboard , straw , and entwined products",
        "001701-Manufacturing pulp paper and cardboard pastes",
        "001702-Manufacturing waving cardboard and paper utensils",
        "001709-Manufacturing other products from paper and cardboard",
        "001811-Pressing",
        "001812-Pressing Related services activities",
        "001820-Copywriting recorded means of media",
        "001910-Manufacture of coke oven products",
        "001920-Manufacture of petroleum refining products",
        "002011-Manufacturing main chemicals",
        "002012-Manufacturing fertilizers and azotes compounds",
        "002013-Manufacturing resins ( plastics ) in its preliminary form , and synthetic rubber ",
        "002021-Manufacturing insecticides , rodenticides , fungicides , and other agricultural chemicals",
        "002022-Manufacturing polishers , varnishes , with other similar paintings and printer toners ,  ",
        "002023-Manufacturing soap ,disinfectants , cleaning , polishing , and cosmetic products",
        "002029-Manufacturing other non classified products in other place",
        "002030-Manufacturing fibers and synthetic threads",
        "002100-Manufacturing pharmaceutical, chemical, and medical plant products",
        "002211-Manufacturing tires , rubber tubes , with renewing external surfaces of rubber tires , and  reconstructing them",
        "002219-Manufacturing other rubber products",
        "002220-Manufacturing plasticized products",
        "002310-Manufacturing glass, and glass products",
        "002391-Manufacturing thermal porcelain products",
        "002392-Manufacturing constructural clay products",
        "002393-Manufacturing other porcelain products",
        "002394-Manufacturing cement , limestone , and gypsum",
        "002395-Manufacturing articles from concrete , cement , and gypsum",
        "002396-Cutting , craving , and completing stones preparation",
        "002399-Manufacturing other unclassified non metallic products in other place",
        "002410-Manufacturing iron and steel",
        "002420-Manufacturing precious metals apart from iron and steel",
        "002431-casting iron and steel",
        "002432-Casting non iron metals",
        "002511-Manufacturing constructural metallic products",
        "002512-Manufacturing cisterns , tanks and vessels from metals",
        "002513-Manufacturing steam generators except for central heating boilers run in hot water",
        "002520-Manufacturing weapons and ammunitions",
        "002591-Shaping metals by hammering , pressing , casting , rolling , and powder metallurgy",
        "002592-Treating , paining metals , with the general mechanical engineering processes",
        "002593-Manufacturing cutting , manual tools , and general metal tools",
        "002599-Manufacturing products from other unclassified shaped metals in other place",
        "002610-Manufacturing electronic components",
        "002620-Manufacturing computers and terminal devices ,",
        "002630-Manufacturing means of transportation",
        "002640-Manufacturing consumer electronics",
        "002651-Manufacturing tools , devices used for measuring , verification , testing , navigation and other purposes",
        "002652-Manufacturing watches on its variety",
        "002660-Manufacturing x ray with medical and therapeutic devices",
        "002670-Manufacturing audio visual equipment",
        "002680-industry of Magnetic and visual medium",
        "002710-Manufacturing motors , generators , transformers , bus bars and control panels",
        "002720-Batteries and lead acid batteries",
        "002731-Manufacturing optical fiber cables",
        "002732-Manufacturing electronic , electrical wires and cables",
        "002733-Manufacturing electrical conductors",
        "002740-Manufacturing electrical lighting equipments",
        "002750-Manufacturing domestic equipment and appliances",
        "002790-Manufacturing other unclassified electrical equipment in other place",
        "002811-Manufacturing motors and turbines , except for airplanes , cars , and steam bicycles",
        "002812-Manufacturing equipment which runs in hydraulic energy",
        "002813-Manufacturing pumps , compressors , taps and valves",
        "002814-Manufacturing chassis , gears , gearboxes and crank devices",
        "002815-Manufacturing ovens , cast furnaces , and iron burners",
        "002816-Manufacturing lifting and handling equipments",
        "002817-Manufacturing office equipment except for computers and terminal units",
        "002818-Manufacturing motorized  manual tools",
        "002819-Manufacturing other machines for general purposes",
        "002821-Manufacturing machines used in agriculture and forests",
        "002822-Manufacturing manual machines",
        "002823-Manufacturing machines which use metallurgy ( treating hot metals and casting ) ",
        "002824-Manufacturing machines used in mines , quarries and building works",
        "002825-Manufacturing foods , drinks and tobacco equipment",
        "002826-Manufacturing equipments of fabrics, clothes , and leather production",
        "002829-Manufacturing specialized machines for other purposes",
        "002910-Manufacturing motorized vehicles",
        "002920-Manufacturing bodies ( preparing motorized vehicles , trailer vehicles , and semi trailer vehicles )",
        "002930-Manufacturing parts , accessories , and motors of motorized vehicles",
        "003011-Building ships and floating frameworks",
        "003012-Building boats for picnic and sports",
        "003020-Manufacturing trains and equipment which move on railways",
        "003030-Manufacturing air and space vehicles , with other relevant equipment",
        "003040-Manufacturing military fighting vehicles",
        "003091-Manufacturing steam cycles ( motorcycles )",
        "003092-Manufacturing ordinary bicycles and handicapped vehicles",
        '003099-Manufacturing "other non classified transportation equipments" in other place',
        '003100-Manufacturing "other unclassified furniture and wooden products" in other place',
        "003211-Manufacturing jewels and other related products",
        "003212-Manufacturing false jewels and other related jewelry",
        "003220-Manufacturing musical instruments",
        "003230-Manufacturing sports equipments",
        "003240-Manufacturing games and toys , ",
        "003250-Manufacturing medical and dentistry equipment",
        "003290-Other unclassified transformational industries in other place",
        "003311-Fixing fabricated metallic products ",
        "003312-Fixing machines",
        "003313-Fixing electronic and optical equipment",
        "003314-Fixing electrical equipment",
        "003315-Fixing transportation equipment except for motorized vehicles",
        "003319-Fixing other equipment",
        "003320-Installing , preparing equipment and industrial machines",
        "003510-Generating , transferring and distributing electricity ",
        "003520-Producing gas and distributing it within the main piping",
        "003530-Steam production and air conditioning supplies",
        "003600-Collecting , purifying and water distribution",
        "003700-Sewage systems",
        "003811-collecting of non hazardous wastes",
        "003812-Collecting of hazardous wastes",
        "003821-treating non hazardous wastes with dumping it",
        "003822-treating hazardous wastes with dumping it",
        "003830-Recycling unused materials",
        "003900-Treatment activities and other services relating Recycling process ",
        "004100-Establishing buildings",
        "004210-establishing roads and railways",
        "004220-Establishing utilities projects",
        "004290-Establishing other engineering projects",
        "004311-Demolishing work",
        "004312-Site mobilization",
        "004321-Installing electrical equipment",
        "004322-Installing sanitary fixtures , and HAVC systems",
        "004329-Other building installations ( except for electrical ones ) ",
        "004330-Building finishes",
        "004390-Other specialized construction activities",
        "004510-Motorized vehicles trade  in (retail and wholesale )",
        "004520-Maintaining and fixing motorized vehicles",
        "004530-Trading motorized vehicles parts and accessories in ( retail and wholesale )",
        "004540-Trading , maintenance , and fixing steam cycles ( motorcycles ) with providing accessories , and related spare parts",
        "004610-Wholesales trading based on contact or against a certain fee ( including relevant services of wholesale )",
        "004620-Wholesale trade for agricultural raw materials and live cattle",
        "004630-Wholesale trade for foods, drinks , and tobacco",
        "004641-Wholesale trade for fabrics , clothes , and choses",
        "004649-Wholesale trade for other domestic articles",
        "004651-Wholesale trade for computer sets , external parts and relevant software programs",
        "004652-Wholesale trade for electronic equipment , communication devices with its relevant parts ",
        "004653-Wholesale trade for agricultural tools and equipment",
        "004659-Wholesale trade for other agricultural tools and equipment",
        "004661-Wholesale trade for solid , liquid and gaseous fuel and products doing with",
        "004662-Wholesale trade for minerals and its raw materials",
        "004663-Wholesale trade for building materials , equipment , tools with sanitary and warming accessories ",
        "004669-Wholesale trade for wastes , junk materials , and other non classified products in other disposal",
        "004690-unspecialized wholesale trade ",
        "004711-Retail trade in unspecialized stores in which foods , drinks or tobacco are the prevailed  articles",
        "004719-Other retail trade in unspecialized stores",
        "004721-Retail trade for foods in specialized stores",
        "004722-Retail trade for drinks in specialized stores",
        "004723-Retail trade for tobacco and its derivatives in specialized stores",
        "004730-Retail trade for vehicles fuel",
        "004741-Retail trade for computer tools , terminal units , programs and communications ",
        "004742-Retail trade for audio visual equipment",
        "004751-Retail trade for fabrics",
        "004752-Retail trade for metal junk , painting , glass and building accessories",
        "004753-Retail trade for carpets, clems , walls and floor coverings",
        "004759-Retail trade for electrical domestic appliances , furniture , lighting equipment and other equipment",
        "004761-Retail trade for books , newspapers , and stationery products",
        "004762-Retail trade for musical and video records",
        "004763-Retail trade for sports equipment",
        "004764-Retail trade for games and children toys",
        "004771-Retail trade for clothes , chooses , and leather articles",
        "004772-Retail trade for pharmaceutical , medical , together with cosmetics and beauty articles  ",
        "004773-Other retail trade for new articles in specialized stores",
        "004774-Retail trade for second hand articles",
        "004781-Retail trade in movable units , and markets for food products , drinks and tobacco",
        "004782-Retail trade in movable units , and markets for fabrics , clothes , and chooses",
        "004789-Retail trade in movable units , and markets for other articles",
        "004791-Retail trade by mail , E mail and internet",
        "004799-Other retail trade outside stores",
        "004911-Carrying passengers by railways",
        "004912-Carrying goods by railways",
        "004921-land transportation for passengers in cities and outskirts",
        "004922-Other types of land transportation for  passengers ",
        "004923-Land transportation for goods",
        "004930-Transferring through pipes ",
        "005011-Marine , coastal transportation for passengers",
        "005012-Marine , coastal transportation for goods",
        "005021-local water transportation for passengers",
        "005022-Local water transportation for goods",
        "005110-Air transportation for passengers",
        "005120-Air transportation for goods",
        "005210-Storage and stores",
        "005221-Accompanied services activities for land transportation",
        "005222-Accompanied services activities for water transportation",
        "005223-Accompanied services activities for air transportation",
        "005224-Loading and unloading",
        "005229-Other supporting transportation activities",
        "005310-Postal activities",
        "005320-Express transportation services",
        "005510-Sort term stay activities",
        "005520-Tents , recreational boats , and caravans sites",
        "005590-Other stay services",
        "005610-Restaurants, transferred meals services",
        "005621-Supplying foods on occasions",
        "005629-Other services for serving meals",
        "005630-served drinks  Activities",
        "005811-Publishing books",
        "005812-Publishing references , and E mail lists",
        "005813-Publishing papers, and journals",
        "005819-Other publishing activities",
        "005820-Publishing ready programs",
        "005911-Producing films, television programs and videos activities",
        "005912-Final processes activities for films, television programs and videos",
        "005913-Distribution activities for films, television programs and videos",
        "005914-Display activities for films and  films videos , ",
        "005920-Activities of producing sound and musical records",
        "006010-Radio Broadcasting",
        "006021-Television broadcasting",
        "006022-Cables, satellite, and other subscription programs",
        "006110-Wire communication activities",
        "006120-Wireless communication activities",
        "006130-Communication activities within satellites ",
        "006190-Other communication activities",
        "006201-Computer programs activities",
        "006202-Computer department  activities  for consultation and facilities",
        "006209-Other information systems, and computer services activities",
        "006311-Processing data , and relevant hosting activities",
        "006312-Internet",
        "006321-News agencies activities",
        "006329-Other information services activities",
        "006411-Central banks",
        "006419-Other financial mediums",
        "006420-Holding companies activities",
        "006430-Funding, crediting , and other funding means",
        "006491-Leasing",
        "006492-Other gifted loans",
        "006499-Other non classified financial activities in other place",
        "006511-Life insurance",
        "006512-General insurances apart from life insurance",
        "006520-Covering insurance ( reinsurance )",
        "006530-Retirement pensions , ",
        "006611-Financial markets administration",
        "006612-Handling securities activities",
        "006619-Assisting activities of financial intermediate in other place",
        "006621-Assessing losses and damages",
        "006622-Insurance agents and brokers activities",
        "006629-Other activities for saving credits for pensions contracts",
        "006630-Funding management activities",
        "006810-Possessed or rented real estate activities",
        "006820-Real estate's activities based on contract or against fee",
        "006910-Legal activities",
        "006920-Accounting , holding books , auditing activities , with tax consultative experience",
        "007010-Main center activities",
        "007020-Consultative experiences activities",
        "007110-Architectural and engineering activities with ones doing with technical consultants",
        "007120-Technical tests and analysis",
        "007210-Researches , improvement in physical and engineering fields",
        "007220-Researches , improvement in sociology and anthropology",
        "007310-Advertising",
        "007320-Markets researches , with public questionnaires , ",
        "007410-Specialized designs activities",
        "007420-Photography",
        "007490-Other unclassified Specialized scientific and technical activities in other place",
        "007500-Veterinary activities",
        "007710-Renting vehicles without drivers",
        "007721-Renting sports articles and means of entertainments",
        "007722-Renting video tapes and discs",
        "007729-Renting other unclassified personal and domestic articles in other place",
        '007730-Renting unclassified "machines , equipment , and other materials without their operators" in other place',
        "007740-Renting intangible assts    ",
        "007810-Labors recruitment agencies",
        "007820-activities of Temporary labor authority",
        "007830-Providing Human resources  activities, with managing tasks of such human resources",
        "007911-Travel agencies activities",
        "007912-Travel tours organization activities",
        "007920-Other tourism services activities",
        "008010-Private security activities",
        "008020-Security services activities",
        "008030-Monitoring activities , ",
        "008110-Assisting activities for facilitations set",
        "008121-General hygiene for buildings",
        "008129-Other cleaning activities for domestic and industrial buildings",
        "008130-Activities of caring about the general appearance and maintenance",
        "008211- Executive office services activities",
        "008219-Photocopy , preparing documents and supporting offices activities",
        "008220-Communication centers activities",
        "008230-Arranging agreements and trade offers",
        "008291-Collection agencies and credit offices activities",
        "008292-Packing activities",
        "008299-Other activities for supporting unclassified tasks in other place",
        "008311-Cars loans",
        "008312-Personal loans",
        "008313-Real estate loans",
        "008314-Credit cards",
        "008411-Comprehensive Public state affairs activities",
        "008412-Systems activities which serve entities that provides health care , educational and cultural services and other social services , ",
        "008413-Business management activities organization with taking part in operating them more efficiently",
        "008419-Governmental assisting services",
        "008421-Foreign affairs",
        "008422-Armed forces activities",
        "008423-General regulations and safety activities",
        "008430-Mandatory social insurance activities",
        '008510-Preliminary "kindergarten", primary, and preparatory education',
        "008521-General secondary school certificate",
        "008522-Industrial and technical secondary school certificate",
        "008530-university and higher education",
        "008541-Sports and recreational education",
        "008542-Cultural education",
        "008549-Other unclassified  types of education in other place",
        "008550-Educational supporting services",
        "008610-Hospital activities",
        "008620-Medical practises , and dentistry activities",
        "008690-Other activities doing with human health",
        "008710-Nursing care facilities",
        "008720-Taking after hospitalized of mental retarded , psychotics , and handicapped activities  ",
        "008730-Taking after hospitalized of aged and handicapped",
        "008790-Taking after other unclassified hospitalized in other place",
        "008810-Social action activities for non hospitalized aged and handicapped",
        "008890-Other unclassified Social action activities for non hospitalized in other place ",
        "009000-Theatre , musical arts , with recreational activities",
        "009101-Libraries and documentary houses activities",
        "009102-Museums, famous places, and historical buildings activities",
        "009103-Plants , zoological gardens and natural wild life activities",
        "009200-Gambling and betting activities",
        "009311-Running sports activities",
        "009312-Sports clubs activities",
        "009319-Other sports activities",
        "009321-Entertainment picnic activities",
        "009329-Other non classified recreational and entertainment activities in other place",
        "009411-free business  Organizations and employers activities",
        "009412-Professional organizations activities",
        "009420-Activities of labors associations inside organizational units",
        "009491-Religious authorities activities",
        "009492-organizations activities political  ",
        "009499-Activities of other unclassified organizations for other memberships in other place",
        "009511-Fixing computers and adjoined devices",
        "009512-Fixing communication equipment",
        "009521-Fixing consumer electronic devices",
        "009522-Fixing domestic devices and garden equipment",
        "009523-Fixing chooses and leather products",
        "009524-Fixing furniture with furnishing home",
        "009529-Fixing personal and other domestic articles",
        "009601-Laundry , dying clothes , fabrics and fur services",
        "009602-Hair dressing , and make up services",
        "009603-Preparing funerals and burying services",
        "009609-Other personal services activities",
        "009700-Domestic services for families members",
        "009810-Non special articles production for domestic application",
        "009820-Non special articles production for domestic application",
        "009900-Organizational, International authorities, regional, embassies, and foreign consulates services",
    );


    $data['borrower_information_C10'] = array(
        "Manufacturing",
        "Services",
        "Trading/Commercial",
        "Contractors",
    );

    $data['borrower_information_C11'] = array(
        "Open Stock Company",
        "Closed Stock Company",
        "Limited Liability Company (LLC)",
        "Limited Partnership Co.",
        "Partnership Co.",
        "Sole Proprietorship",
        "Other",
    );

    $data['borrower_assessment_F5'] = array("Low", "Medium", "High", "Very High");

    $data['borrower_assessment_F10'] = array("Policies are highly favorable and unambiguous and likely to continue in the foreseeable future",
        "Moderately favorable policies (such as protective import tariffs/ incentives) positively impacting profitability",
        "The existing government policies are not significantly favorable/ unfavorable for the industry. Profitability is not particularly influenced by existing / foreseen regulatory measures",
        "Negative influence due to the current government policies. No new policy expected in the near term that will change the scenario",
        "Government policy has a significantly negative influence e.g. in the form of high excise burden, inverted import duty structure, unviable price regulation, etc.",
        "Government policy towards industry is extremely unfavorable");

    $data['borrower_assessment_F18'] = array("Low -  Not subjected to any forex risk. Any such risks are naturally hedged, with a robust policy in place to manage such risks",
        "Moderate -  Subjected to unhedged forex risks with no natural hedges",
        "High -  Subjected to very high forex risk which is not hedged");

    $data['borrower_assessment_F22'] = array("Company is the sole buyer from its supplier/large off take. Alternate suppliers are readily available or the cost of switching to alternate supplier is very low",
        "Company’s off-take forms a significant portion of the suppliers output. Limited number alternate key buyers available",
        "Company’s off-take forms a large portion of the suppliers output. Buyers base with similar product requirement available",
        "Company’s off-take forms a moderate portion of the suppliers output. Supplier has fairly large number of alternate companies to target for selling its product. Specialized or slightly customized products being sourced",
        "Company’s off-take forms small portion of the suppliers output. Oligopolistic market dynamics with few suppliers having complete control over supply. Specialized or fully customized products being sourced",
        "Company’s off-take forms a very small portion of the suppliers output; supplier has monopolistic market ");

    $data['borrower_assessment_F29'] = array("Minimal dependency on customers. Enjoys longstanding relationship with reputed customers and /or higher proportion of sales from high quality customers ",
        "Selling targeted at large number of customers with medium to long term contracts in place with customers and / or proportion of sales from high quality customers are less than 50%",
        "Customer base is fairly diverse with medium term contracts in place with customers limited revenue from high quality customers",
        "Small customer base,  good relationship with few key customers and transactional relationship with most of the customers; customer quality is average",
        "Customer base is small - top 5 customers accounting for 70% or more revenue. transactional relationship with customers and weaker customer profile",
        "Very high dependence on a single customer / absence of any medium term contract with the customer");

    $data['borrower_assessment_F36'] = array("High - Business located in prime location where demand for the product/offering is excellent, very few competitors , proximity to customers/end users and availability of excellent infrastructure ",
        "Moderate - Moderate demand, some degree of competition and not so robust infrastructure",
        "Low – Location not so conducive to business because of low demand, strong competition and lack of supporting strong infrastructure");

    $data['borrower_assessment_F40'] = array("No change in business location over the last 3 years Or Change to business location with better infrastructure and facilities",
        "One change in business location within the city over the last 3 years, has moved to own premises from rented premise",
        "More than one change in business location within the city over the last 3 years",
        "Change of City over the last 3 years");

    $data['borrower_assessment_F45'] = array("Entity serves large number of customers/multiple markets and low concentration risk in terms of revenue or if there are key customers, entity has long and established relation with them OR Entity has multiple product lines/product variants contributing to total revenue with little/no concentration risk",
        "Limited number of customers/products (>=3 to <=7) and contribution of top 3 products/customers to revenue is less than 50%",
        "Limited number of customers/products (>=3 to <=7) and contribution of top 3 products/customers to revenue is more than 50%",
        "Very few (less than 3) customers/products resulting in high concentration risk");

    $data['borrower_assessment_F50'] = array("Business premises is registered in the business’ name",
        "Business premises is solely owned by the promoter/partners",
        "Business premises is owned by the promoter’s family, with the promoter being one of the heirs to the property  OR Premises is on long-term lease of 15 years or more and has been occupied by the entity since more than 4 years",
        "Business premises is rented/leased of less than 15 years and has been occupied by the entity since more than 4 years but more than 2 years",
        "Business premises is rented/leased and has been occupied by the entity since less than 2 years");
//modified
//    $data['borrower_assessment_F56'] = array(">= 10 years",
//        ">=5 to <10 years",
//        ">= 3 to <5 years",
//        "<3 years");
    $data['borrower_assessment_F56'] = array(">= 15 years",
        ">= 10 to <15 years",
        ">=5 to <10 years",
        ">= 3 to <5 years",
        "<3 years");

    $data['borrower_assessment_F61'] = array(">= 15 years",
        ">= 10 to <15 years",
        ">=5 to <10 years",
        "<5 years");

    $data['borrower_assessment_F66'] = array("Permanent",
        "Temporary",
        "In progress",
        "No license");

    $data['borrower_assessment_F71'] = array("New",
        "Refurbished",
        "Used");

    $data['borrower_assessment_F75'] = array("Low Impact",
        "Medium Impact",
        "High Impact");

    $data['borrower_assessment_F79'] = array("The company is known for best quality offerings and has excellent reputation in the market",
        "The company is known for good quality offerings with largely positive feedback from customers and enjoys fairly well established reputation",
        "Above average quality with limited brand recall/reputation in the market, relatively positive feedback from customers",
        "Offerings are of average quality and require some improvements, weak brand/reputation in the market",
        "The service quality is below average and require major improvements, absence of branded offerings",
        "The service quality is extremely poor, absence of branded offerings");

    $data['borrower_assessment_F86'] = array("Extremely comprehensive processes and controls in place; response time and execution timelines always exceed or meet expectations",
        "The controls and processes are highly organized; mostly meets the timelines and SLAs, no instances of escalation and / or penalty in the past five years",
        "Adequate controls and processes; timelines are met generally; few instances of escalation; however none in recent past (2 years)",
        "The processes and controls are required to be redefined;  few instances of escalation in recent past due to delays in execution ",
        "Inadequate processes and controls; on-going projects are regularly delayed; lack of project / timeline management structure",
        "No controls and processes in place; large penalties;  achieving project closures are concern resulting in loss of business opportunities");

    $data['borrower_assessment_F93'] = array("Average Inventory Holding Period is lower than the industry standard with a portfolio of products susceptible to low fluctuations/stable prices",
        "Average Inventory Holding Period is in line than the industry standard with a portfolio of products susceptible to low fluctuations/stable prices  OR Average Inventory Holding Period lower than the industry standard with a portfolio of products susceptible to moderate fluctuations in prices",
        "Average Inventory Holding Period is in line with industry standard with a portfolio of products susceptible to moderate fluctuations in prices",
        "Average Inventory Holding Period lower the industry standard with a portfolio of products susceptible to moderate fluctuations/stable prices  OR Average Inventory Holding Period higher than the industry standard with a portfolio of products susceptible to low fluctuations/stable prices",
        "Average Inventory Holding Period higher than the industry standard with a portfolio of products susceptible to moderate fluctuations in prices  OR Average Inventory Holding Period is in line with industry standard with a portfolio of products susceptible to high fluctuations in prices",
        "Average Inventory Holding Period is higher than the industry standard with a portfolio of products susceptible to wide fluctuations in the prices");

    $data['borrower_assessment_F100'] = array("Presence in all major product segments in the industry with the largest market share in the main product segments",
        "The entity is expected to remain at the forefront in most of the product segments. The entity is among the top 5 players in the main product segments, in terms of market share",
        "Presence in majority of the product segments in the industry with average market share",
        "Presence in a limited range of relatively high margin products. Market share of the entity is below average",
        "Presence in lower margin products within limited categories. Market share of the entity is low",
        "Single/two product company, not the best in their respective segments. Market share is insignificant");

    $data['borrower_assessment_F107'] = array("Rank 7", "Rank 6", "Rank 5", "Rank 4", "Rank 3", "Rank 2", "Rank 1");

    $data['borrower_assessment_F115'] = array("Very good track record of execution of similar projects with little/no time and cost overrun.",
        "The company has a good track record of timely, cost-bound project completion but has suffered some overruns in a few projects",
        "There are many instances where the company has not been able to deliver on time",
        "Extremely poor project management and execution skills with frequent project time overrun.");

    $data['borrower_assessment_F121'] = array("Audited Financials", "Unaudited Financials", "Not Available");

    $data['borrower_assessment_F125'] = array("Audited financials -  Good quality",
        "Provisional/Audited Financials - Average Quality",
        "Unaudited - Below Average Quality");

    $data['borrower_assessment_F129'] = array("Entity is not exposed to seasonality trends in revenue",
        "Entity is exposed to seasonality trends leading to fluctuations in revenue");

    $data['borrower_assessment_F133'] = array("Positive -  Has high degree of integrity and very good market reputation. Has great track record with all stake holders in all dealings.",
        "Neutral/Mixed - Has fairly moderate reputation with few cases of customer/creditor/banker dissatisfaction",
        "Negative - market reputation and integrity is suspect. There is low degree of comfort when it comes to dealing with the entity/person");

    $data['borrower_assessment_F137'] = array("Outstanding",
        "Very Good",
        "Not Available",
        "Satisfactory",
        "Below Satisfactory",
        "High Risk / Defaulted");

    $data['borrower_assessment_F144'] = array("Outstanding",
        "Very Good",
        "Satisfactory",
        "Below Satisfactory",
        "High Risk / Defaulted",
        "Not Applicable/Not Available");

    $data['borrower_assessment_F151'] = array("Low Risk - formal succession plan in place, company has access to good managerial pool for future leadership capable of running the  company with minimal dependence on a single person/few individuals",
        "Medium Risk - Some dependence on individual/ a group of individuals with some uncertainty regarding the future allocation of key managerial roles leading to moderate continuity risk",
        "High Risk - Huge dependence on a single individuals to run the business");

    $data['borrower_assessment_F155'] = array("Low - Minimal dependence on one individual, presence of legal heir/more than one active partners/stakeholders involved running the business",
        "Medium - Some dependence on key person/group of persons on account of legal heir with limited experience/interest in business and only one active partner/stakeholders involved in running the business",
        "High (no legal heir/partner/shareholder involved, business is entirely dependent on one person)");

    $data['borrower_assessment_F159'] = array("Highly qualified providing a definite competitive edge with experience in excess of fifteen years in the industry",
        "Good level of competence with experience between ten to fifteen years in the industry",
        "Average level of competence with experience between five to ten years in the industry",
        "Marginally experienced and capabilities are perceived to below average with experience between three to five years in the industry",
        "Start-up company with management having no prior experience (less than 3 years)/ competence in the sector to run the business or experience less than one year.");

    $data['borrower_assessment_F165'] = array("Excellent – All payments made on time on a consistent basis",
        "Satisfactory – Delays observed on few instances but settled in few days",
        "Below Satisfactory – Delay/irregularity observed in multiple payments");

    $data['borrower_assessment_F169'] = array("Self-owned residential house",
        "Lives in parental/ancestral residence and is the sole heir",
        "Lives in parental/ancestral residence with multiple heirs to the property",
        "Has been living in the same rented property since more than 3 years",
        "Has been living in the same rented property since less than 3 years");

    $data['borrower_assessment_F175'] = array("The promoters/partners reside in the well known/above average localities in the city/town with good infrastructure availability for future growth/sustainability",
        "The promoters/partners reside in average locality/emerging areas where infrastructure is not of the highest order",
        "The locality of residence is on the outskirts of the city/town with little/no infrastructure for future sustainability");

    $data['borrower_assessment_F179'] = array("All are available",
        "2 of them are available",
        "1 of them is available",
        "None are available");

    $data['borrower_assessment_F186'] = array("All past debt obligations serviced on time or before",
        "Debt obligation mostly made on time with delays of not more than 10 days",
        "Debt obligation(s) are occasionally delayed but settled within 30 days",
        "A few debt obligation(s) delayed but settled within 60 days",
        "A few debt obligation(s) delayed more than 60 days",
        "Cases of non-payment delayed more than 90 days. Classified in default category",
        "N/A");

    $data['borrower_assessment_F194'] = array(">=0% to <50%",
        ">=50% to <75%",
        ">=75% to <90%",
        ">=90%",
        "N/A");

    $data['borrower_assessment_F200'] = array("Relationship with concerned bank for more than 10 years",
        "Relationship with concerned bank for 5-10 years",
        "Relationship with concerned bank for 3-5 years",
        "Relationship with concerned bank for 1-3 years",
        "Relationship with concerned bank for less than1 year",
        "Entity/Promoter has not been associated with banking sector",
        "N/A");

    $data['borrower_assessment_F208'] = array("Zero cheque bounces",
        "> 0% to < 3 %",
        ">= 3% to < 5 %",
        ">= 5%",
        "N/A");

    $data['borrower_assessment_F216'] = array("Term loan and/or facility including term loan and contingent",
        "Contingent with tenor exceeding 1 year",
        "WC loan and/or facility includes Contingent and direct facility",
        "Contingent with tenor less than 1 year");

    $data['borrower_assessment_F221'] = array(">=2",
        ">=1.7 to <2",
        ">=1.5 to <1.7",
        ">=1.3 to <1.5",
        ">=1 to <1.3",
        "<1",
        "N/A");

    $data['borrower_assessment_F229'] = array("1",
        "0.9",
        "0.8",
        "NA"
    );

    return $data[$key];
}

function date_converter($date) {
    return date("Y-m-d H:i:s", strtotime($date));
}

function check_perm($function = NULL, $p = NULL) {
    $CI = &get_instance();
    $levels = $CI->session->userdata("levels");
    $res = substr($levels->$function, $p, 1);
    if (!$res) {
        redirect(base_url("Welcome/permission_denied"));
    }
}

function get_perm($function = NULL, $p = NULL) {
    $CI = &get_instance();
    $levels = $CI->session->userdata("levels");
    $res = substr($levels->$function, $p, 1);
    return $res;
}

function get_cases_perm() {
    $CI = &get_instance();
    $levels = $CI->session->userdata("levels");
    $access_level = $CI->session->userdata("access_level");
    $res = "me";
    if ($access_level == 1) {
        $res = "all";
        return $res;
    }
    $view = substr($levels->cases, 0, 1);
    if (!$view) {
        return $res;
    }
    if ($levels->bank_only) {
        $res = "bank";
        return $res;
    }
    if ($levels->branch_only) {
        $res = "branch";
        return $res;
    }
    $res = "all";
    return $res;
}

function sanitize($param) {
    if ($param != NULL) {
        $CI = &get_instance();
        $param = $CI->security->xss_clean($param);
//    $param=  escapeshellcmd($param);
        /// Path Traversal Owasp testing issue
        foreach ($CI->input->get() as $key => $value) {
            $_GET[$key] = $CI->security->sanitize_filename($value);
            $_GET[$key] = $CI->db->escape($value);
        }
        /////////////
    }
    return $param;
}

function increment($table, $data) {
    $CI = &get_instance();
    $CI->db->limit(1);
    $CI->db->order_by("id", "desc");
    $row = $CI->db->get($table)->row();
    $id = 1;
    if (!empty($row)) {
        $id = $row->id + 1;
    }
    $data['id'] = $id;
    return $data;
}

function incremented($table) {
    $CI = &get_instance();
    $CI->db->limit(1);
    $CI->db->order_by("id", "desc");
    $row = $CI->db->get($table)->row();
    $id = 1;
    if (!empty($row)) {
        $id = $row->id + 1;
    }
    return $id;
}

function non_subscriber_bank($case_id) {
    $CI = &get_instance();
    $CI->db->select('banks.non_subscriber');
    $CI->db->join('users', 'users.id=cases.user_id');
    $CI->db->join('banks', 'banks.id=users.bank_id');
    $case_bank = $CI->db->get_where('cases', array('cases.id' => $case_id))->row();
    if ($case_bank && $case_bank->non_subscriber == 1) {
        return TRUE;
    }
    return FALSE;
}

function check_params($params) {
    foreach ($params as $one) {
        if (!isset($one)) {
            throw("One or more parameters are missing");
        }
    }
}

function check_param($param) {
    if (!isset($param)) {
        echo "One or more parameters are missing";
        die();
    }
}

function get_level_id($level_name=NULL){
    $CI= &get_instance();
    $level_id= $CI->session->userdata("access_level");
    if($level_name){
        switch ($level_name){
            case "super_admin":
                return 1;
            case "bank_admin":
                return 4;
            case "verifier":
            return 5;
            case "verifier2":
            return 7;
            case "maker":
                return 6;    
            default :
                return $level_id;
        }
    }
    return $level_id;
}

function compare_level($level_name){
    $CI= &get_instance();
    $level_id= $CI->session->userdata("access_level");
    switch ($level_name){
            case "super_admin":
                if($level_id==="1"){
                    return TRUE;
                }
                break;
            case "bank_admin":
                if($level_id==="4"){
                    return TRUE;
                }
                break;
            case "verifier":
                if($level_id==="5" || $level_id==="7"){
                    return TRUE;
                }
                break;
            case "maker":
                if($level_id==="6"){
                    return TRUE;
                }
                break;
            default :
                return FALSE;
        }
        
        return FALSE;
}

//UAT
//Array
//(
//    [0] => stdClass Object
//        (
//            [id] => 1
//            [name] => Super Admin
//            [created] => 2017-04-03 10:05:30
//        )
//
//    [1] => stdClass Object
//        (
//            [id] => 4
//            [name] => Bank admin
//            [created] => 2017-12-24 11:50:44
//        )
//
//    [2] => stdClass Object
//        (
//            [id] => 5
//            [name] => Risk Approver
//            [created] => 2017-12-24 11:51:42
//        )
//
//    [3] => stdClass Object
//        (
//            [id] => 6
//            [name] => Maker
//            [created] => 2017-12-24 11:51:59
//        )
//
//    [4] => stdClass Object
//        (
//            [id] => 7
//            [name] => Business Approver
//            [created] => 2018-10-11 13:45:29
//        )
//
//)
function pd($value){
    echo "<pre>";
    print_r($value);
    echo "</pre>";
    die;
}
function p($value){
    echo "<pre>";
    print_r($value);
    echo "</pre>";
}
function replace_bank_name($bank_name){
    $needles=['&lt;','&gt;','/','\\',':','<','>', '*','?','"','|','&',',','.'];
    return str_replace($needles, "_", $bank_name);
}
function get_session_user_bank() {
    $CI = &get_instance();
    $user_id = $CI->session->userdata("user_id");
    $CI->db->select('bank_id');
    $user = $CI->db->get_where('users', array('id' => $user_id))->row();
    if($user) {
        return $user->bank_id;
    }
    redirect(base_url('Login/Logout'));
}