<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('front/Login_model');
        $this->load->model('admin/Front_settings_model');
        if($this->session->userdata('language') != "" )
        {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        }
        else
        {
            $this->lang->load('default');
        }
    }
    public function index()
    {
        if(($this->session->userdata('client_id') != ""))
        {
            $this->welcome();
        }
        else
        {
            $data['title']= 'Login';
            $data['front_settings'] = $this->Front_settings_model->get_front_settings();
            $this->load->view("front/pages/login", $data);
        }
    }
    function welcome()
    {
        if(($this->session->userdata('client_id') != ""))
        {     
            $data = array('title' => 'Home');
            $data['front_settings'] = $this->Front_settings_model->get_front_settings();
            $this->load->view('front/pages/home', $data);
        }
        else
        {
            $this->index();
        }
    }
    function do_login()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $email = $this->input->post('email');
            $password = do_hash($this->input->post('password'), 'md5');
            $check_login_result = $this->Login_model->check_login($email,$password);
            if($check_login_result)
            {
                redirect('Dashboard');
            }
            else
            {
                $this->session->set_flashdata('login_msg', 'Wrong email or password');
                $this->index();
            }
        }
    }
    function Logout()
    {
        if(($this->session->userdata('client_id') != ""))
        {
            $new_data = array(
            'client_id' => '',
            'client_email' => '',
            'client_first_name' => '',
            'client_last_name' => '',
            'client_display_name' => '',
            'logged_in' => FALSE
            );
            $this->session->unset_userdata($new_data);
            $this->session->sess_destroy();
            redirect('Dashboard');
        }
        else
        {
            redirect('Dashboard');
        }
    }
}