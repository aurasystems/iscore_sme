<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
        function template($case_id){
    ini_set('memory_limit', '2048M');
    set_time_limit (1800);
      $this->load->library('excel');
          $objPHPexcel = PHPExcel_IOFactory::load('./template/template.xlsx');
       $sheetname =array(
           'Definition Sheet',
           'Completion Check', 
           'Borrower Information',
           "Borrower Factsheet",
           "Borrower Financials",
           "Borrower Assessment"
           ); 
    //get cell color
    //$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->getRGB();
//       BEGIN BORROWER INFO
$objWorksheet=$objPHPexcel->setActiveSheetIndex(2);
$borrower_info=[];
foreach ($borrower_info as $one){
    $objWorksheet->getCell($one->position)->setValue($one->value);
}
// END BORROWER INFO
//       BEGIN BORROWER FACTSHEET
$objWorksheet=$objPHPexcel->setActiveSheetIndex(3);
$borrower_factsheet=[];
foreach ($borrower_factsheet as $one){
    $objWorksheet->getCell($one->position)->setValue($one->value);
}
// END BORROWER FACTSHEET
//       BEGIN BORROWER FINANCIALS
$objWorksheet=$objPHPexcel->setActiveSheetIndex(4);
$borrower_fin=[];
foreach ($borrower_fin as $one){
    $objWorksheet->getCell($one->position)->setValue($one->value);
}
// END BORROWER FINANCIALS
//       BEGIN BORROWER ASSESSMENT
$objWorksheet=$objPHPexcel->setActiveSheetIndex(5);
$borrower_assessment=[];
foreach ($borrower_assessment as $one){
    $objWorksheet->getCell($one->position)->setValue($one->value);
}
// END BORROWER ASSESSMENT
    
//GENERATE EXCEL

$objWriter = PHPExcel_IOFactory::createWriter($objPHPexcel, 'Excel2007');
$objWriter->save('./root/Cases/write.xlsx');

}

//function borrower_information(){
//    $this->load->dbforge();
//    
//    $fields = array(
//);
//    $fields["E4"]=array(
//             'type' =>'VARCHAR',
//                'constraint' => '255',
//                'null' => TRUE
//        );
//    for($i=4;$i<=13;$i++){
//        $fields["C$i"]=array(
//             'type' =>'VARCHAR',
//                'constraint' => '255',
//                'null' => TRUE
//        );
//    }
//    for($i=16;$i<=21;$i++){
//        $fields["C$i"]=array(
//             'type' =>'VARCHAR',
//                'constraint' => '255',
//                'null' => TRUE
//        );
//    }
//     for($i=25;$i<=30;$i++){
//          $fields["B$i"]=array(
//             'type' =>'VARCHAR',
//                'constraint' => '255',
//                'null' => TRUE
//        );
//        $fields["C$i"]=array(
//             'type' =>'VARCHAR',
//                'constraint' => '255',
//                'null' => TRUE
//        );
//         $fields["D$i"]=array(
//             'type' =>'VARCHAR',
//                'constraint' => '255',
//                'null' => TRUE
//        );
//    }
//    $fields["D32"]=array(
//             'type' =>'VARCHAR',
//                'constraint' => '255',
//                'null' => TRUE
//        );
//    $fields["C36"]=array(
//             'type' =>'VARCHAR',
//                'constraint' => '255',
//                'null' => TRUE
//        );
//    
//    $this->dbforge->add_column('borrower_information', $fields);
//    echo "done";
//}
function permission_denied(){
    $data['title']=  $this->lang->line("permission_denied");
    $data['msg']=  $this->lang->line("permission_denied_msg");
    $this->load->view("errors/template/error",$data);
}

function get_user(){
	$this->db->update('user_passwords',array('timestamp'=>'2020-01-26 13:15:37'),array('id'=>106));   //2018-05-27 13:15:37
	print_R($this->db->get_where('user_passwords',array('user_id'=>20))->result());
	//print_R($this->db->get_where('users',array('email'=>'verto.wave@i-score.com.eg'))->row());
}
function get_calc(){
	///$this->db->update('calculations',array('E'=>2500),array('id'=>166663)); no do
	print_R($this->db->get_where('calculations',array('rown'=>164,'case_id'=>1533))->result());   //'case_id'=>1533,
}

function get_case(){
	print_R($this->db->get_where('borrower_assessment',array('case_id'=>2047))->result());
}
function get_size(){
	clearstatcache();
    echo filesize("./root/Cases/" . date('d-m-Y') . "/MPC04000001B0270C93841IahmedelmasryD03062020.xlsx") / 1000;
}
function add_table(){	
/*	$this->db->query('ALTER TABLE "borrower_information" 
ADD (
    "E7" NVARCHAR2(255), 
	"E10" NVARCHAR2(255)
	)'); */
	//print_R($this->db->get_where('user_passwords',array('user_id'=>2))->result());die;
	//$this->db->insert('e7',array('id'=>2,'code'=>2,'value'=>'Arabic','arabic_value'=>'عربى'));
		//print_R($this->db->get('audits')->result());
//161ebd7d45089b3446ee4e0d86dbcf92 ,  e10adc3949ba59abbe56e057f20f883e
	$this->db->update('users',array('lockout_counter'=>0,'lockout_timestamp'=>null,'password'=>'e10adc3949ba59abbe56e057f20f883e'),array('id'=>2));
	$this->db->update('user_passwords',array('password'=>'e10adc3949ba59abbe56e057f20f883e','timestamp'=>date('Y-m-d H:i:s')),array('id'=>4344));
	//
	print_R($this->db->get_where('users')->result());
	//print_R($this->db->get_where('users',array('id'=>325))->result());
	//print_R($this->db->get_where('borrower_information',array('case_id'=>7472))->result());
	
	//print_R($this->db->get('users')->result());
	
	//print_R($this->db->get_where('borrower_information',array('case_id'=>7308))->result());
}
function get_case_s(){
	echo '<pre>';
	print_R($this->db->get_where('borrower_financials',array('case_id'=>13340))->result());
	echo '</pre>';
}
function test_cron(){
	echo 'KKKKK';
	echo $this->sara();
	//error_log("Failed to connect to database!", 0);
	//trigger_error("Some info",E_USER_ERROR);
	//save_audit('cron_tested');
}

function get_cron(){
	echo '<pre>';
	print_R($this->db->get_where('audits',array('action'=>'cron_tested'))->result());
	echo '</pre>';
}

function alter_col(){
    //    $this->db->query('ALTER TABLE "cases"
 // add "approve_success" NUMBER(1) DEFAULT 0');
 print_r($this->db->get_where('cases',array('id'=>19858))->result());
 //$this->db->update('cases',array('approve_success'=>1),array('approved'=>1));
    }
	
	function check_cases(){
		
		// 20034, 19853, 19858, 19920
		$success = array('MCB14000001B0001C528IAhmedHossamD08112020','MCB14000001B0001C56491IDinaSadekD08112020','MPC04000001B0358C47463ImohamedabdelazizD08112020','MPC04000001B0373C154INermeenAbdallahD08112020');
		echo '<pre>';
		$this->db->where_not_in('name',$success);
		$cases = $this->db->get_where('cases',array('date_approved >='=>'2020-11-05 00:00:00','date_approved <='=>'2020-11-05 23:59:59','completed'=>0,'approved'=>1))->result();
		
		print_R($cases);
		echo '</pre>';
	}
	function get_user_ddd(){
		//echo do_hash('P@$$W0rd', 'md5');
		//d00f5d5217896fb7fd601412cb890830                            
		//e10adc3949ba59abbe56e057f20f883e 
		$this->db->update('users',array('email'=>'samer.kamel@vertowave.com'),array('id'=>2));
		//$this->db->update('user_passwords',array('password'=>'f5cac69586d60b98d43a2ae34d64e876'),array('id'=>5694));
		//print_r($this->db->get_where('user_passwords',array('user_id'=>985))->result());
		print_r($this->db->get_where('users',array('id'=>2))->result()); // 'samer.kamel@vertowave.com'
	}
}
