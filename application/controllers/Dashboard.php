<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        redirect('admin/Dashboard');
        $this->load->model('front/Dashboard_model');
        $this->load->model('admin/Front_settings_model');
        if($this->session->userdata('language') != "" )
        {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        }
        else
        {
            $this->lang->load('default');
        }
    }
    public function index()
    {
        if(($this->session->userdata('client_id') != ""))
        {
            $this->welcome();
        }
        else
        {
            $data['title']= 'Home';
            $data['front_settings'] = $this->Front_settings_model->get_front_settings();
            $this->load->view('front/pages/home',$data);
        }
    }
    function welcome()
    {
        if(($this->session->userdata('client_id') != ""))
        {     
            $data = array('title' => 'Home');
            $data['front_settings'] = $this->Front_settings_model->get_front_settings();
            $this->load->view('front/pages/home', $data);
        }
        else
        {
            $this->index();
        }
    }
}