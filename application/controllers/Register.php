<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Register extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('front/Register_model');
        $this->load->model('admin/Front_settings_model');
        if($this->session->userdata('language') != "" )
        {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        }
        else
        {
            $this->lang->load('default');
        }
    }
    public function index()
    {
        if(($this->session->userdata('client_id') != ""))
        {
            $this->welcome();
        }
        else
        {
            $data['title']= 'Register';
            $data['front_settings'] = $this->Front_settings_model->get_front_settings();
            $this->load->view("front/pages/register", $data);
        }
    }
    function welcome()
    {
        if(($this->session->userdata('client_id') != ""))
        {     
            $data = array('title' => 'Home');
            $data['front_settings'] = $this->Front_settings_model->get_front_settings();
            $this->load->view('front/pages/home', $data);
        }
        else
        {
            $this->index();
        }
    }
    function do_register()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name', 'first name', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('last_name', 'last name', 'trim|required|min_length[4]');
        $this->form_validation->set_rules('display_name', 'display name', 'trim|required|min_length[4]');
        $this->form_validation->set_rules('phone', 'phone', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[clients.email]');
        $this->form_validation->set_rules('address', 'address', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required|matches[password2]|min_length[4]|max_length[32]');
        $this->form_validation->set_rules('password2', 'password confirmation', 'trim|required|matches[password]');
        $this->form_validation->set_rules('terms_conditions', 'terms and conditions', 'trim|required');
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'display_name' => $this->input->post('display_name'),
                'phone' => $this->input->post('phone'),
                'email' => $this->input->post('email'),
                'address' => $this->input->post('address'),
                'password' => do_hash($this->input->post('password'), 'md5')
            );
            $this->Global_model->global_insert('clients',$data);
            redirect('Register/register_success');
        }
    }
    function register_success()
    {
        if(($this->session->userdata('client_id') != ""))
        {
            $this->welcome();
        }
        else
        {
            $data['title']= 'Register Success';
            $data['front_settings'] = $this->Front_settings_model->get_front_settings();
            $this->load->view("front/pages/register_success", $data);
        }
    }
}