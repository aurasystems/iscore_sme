<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contact_us extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('front/Contact_us_model');
        $this->load->model('admin/Front_settings_model');
        $this->load->model('admin/Email_settings_model');
        if($this->session->userdata('language') != "" )
        {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        }
        else
        {
            $this->lang->load('default');
        }
    }
    public function index()
    {
        $data['title'] = 'Contact Us';
        $data['attributes'] = array('class' => 'form-horizontal', 'role'=>'form');
        $data['label_att'] = array ('class' => 'col-sm-4 control-label');
        $data['front_settings'] = $this->Front_settings_model->get_front_settings();
        $this->load->view("front/pages/contact_us", $data);
    }
    function send_contact_us()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'name', 'trim|required|min_length[4]');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('subject', 'subject', 'trim|required');
        $this->form_validation->set_rules('message', 'message', 'trim|required');
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $data['name'] = $this->input->post('name');
            $data['email'] = $this->input->post('email');
            $data['subject'] = $this->input->post('subject');
            $data['message'] = $this->input->post('message');
            $this->send_email_contact_us($data);
            redirect('Contact_us/contact_us_success');
        }
    }
    function send_email_contact_us($data)
    {
        $msg_content = $data['message'];
        // config email smtp
        $settings = $this->Email_settings_model->get_email_settings();
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => $settings->smtp_host, //ssl://smtp.googlemail.com
            'smtp_port' => $settings->smtp_port, // 465 
            'smtp_user' => $settings->smtp_user,
            'smtp_pass' => $settings->smtp_pass,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1'
        );
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from($data['email'],$data['name']);
        $this->email->to($settings->host_mail);
        $this->email->subject($data['subject']);
        $this->email->message($msg_content);
        $this->email->send();
    }
    function contact_us_success()
    {
        $data['title'] = 'Contact Us Success';
        $data['front_settings'] = $this->Front_settings_model->get_front_settings();
        $this->load->view("front/pages/contact_us_success", $data);
    }
}