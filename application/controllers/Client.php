<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Client extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('front/Client_model');
        $this->load->model('admin/Front_settings_model');
        if($this->session->userdata('language') != "" )
        {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        }
        else
        {
            $this->lang->load('default');
        }
    }
    public function index($status = NULL)
    {
        if(($this->session->userdata('client_id') != ""))
        {
            $client_id = $this->session->userdata['client_id'];
            $data = array('title' => 'Client Profile');
            $data['status'] = $status;
            $data['client_id'] = $client_id;
            $data['client_data'] = $this->Global_model->get_data_by_id('clients',$client_id);
            $data['front_settings'] = $this->Front_settings_model->get_front_settings();
            $this->load->view('front/pages/profile', $data);
        }
        else
        {
            redirect('Dashboard');
        }
    }
    function do_update_client()
    {
        if(($this->session->userdata('client_id') != ""))
        {
            $client_id = $this->session->userdata['client_id'];
            $this->load->library('form_validation');
            $this->form_validation->set_rules('first_name', 'first name', 'trim|required|min_length[3]');
            $this->form_validation->set_rules('last_name', 'last name', 'trim|required|min_length[4]');
            $this->form_validation->set_rules('display_name', 'display name', 'trim|required|min_length[4]');
            $this->form_validation->set_rules('phone', 'phone', 'trim|required');
            $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
            $this->form_validation->set_rules('address', 'address', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|matches[password2]|min_length[4]|max_length[32]');
            $this->form_validation->set_rules('password2', 'Password Confirmation', 'trim|matches[password]');
            if($this->form_validation->run() == FALSE)
            {
                $this->index();
            }
            else
            {
                if($this->input->post('password') != "" && $this->input->post('password2') != "")
                {
                    $data = array(
                        'first_name' => $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'display_name' => $this->input->post('display_name'),
                        'phone' => $this->input->post('phone'),
                        'email' => $this->input->post('email'),
                        'address' => $this->input->post('address'),
                        'password' => md5($this->input->post('password'))
                    );
                }
                else
                {
                    $data = array(
                        'first_name' => $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'display_name' => $this->input->post('display_name'),
                        'phone' => $this->input->post('phone'),
                        'email' => $this->input->post('email'),
                        'address' => $this->input->post('address')
                    );
                }
                $this->Global_model->update_client('clients',$client_id, $data);
                redirect('Client/index/success');
            }
        }
        else
        {
            redirect('Dashboard');
        }
    }
    function reset_password()
    {
        if(($this->session->userdata('client_id') != ""))
        {
            $this->index();
        }
        else
        {
            $data = array('title' => 'Recover Password');
            $data['front_settings'] = $this->Front_settings_model->get_front_settings();
            $this->load->view("front/pages/forget_password", $data);
        }
    }
    function do_password_reset()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        if($this->form_validation->run() == FALSE)
        {
            $this->reset_password();
        }
        else
        {
            $email = $this->input->post('email');
            $check_client = $this->Global_model->check_email('clients',$email);
            if($check_client)
            {
                // user is exists
                $new_password = random_string('alnum',10);
                $this->Global_model->reset_password('clients',$email,$new_password);
                redirect('Client/password_reset_success');
            }
            else
            {
                // user not exists
                $this->session->set_flashdata('reset_password_msg', 'This email is not found');
                $this->reset_password();
            }
        }
    }
    function password_reset_success()
    {
        if(($this->session->userdata('client_id') != ""))
        {
            $this->index();
        }
        else
        {
            $data['title'] = 'Password Recover Success';
            $data['front_settings'] = $this->Front_settings_model->get_front_settings();
            $this->load->view("front/pages/password_recover_success", $data);
        }
    }
	
	//function update_db(){
	//	$this->db->query('ALTER TABLE "VERTO"."email_settings" ADD(
	//"smtp_host_b" NVARCHAR2(100), 
	//"smtp_port_b" NVARCHAR2(100), 
	//"smtp_user_b" NVARCHAR2(100), 
	//"smtp_pass_b" NVARCHAR2(100), 
	//"host_mail_b" NVARCHAR2(100)) ');
	//}
}