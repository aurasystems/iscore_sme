<?php

class Banks extends CI_Controller {

    public function __construct() {
        parent::__construct();
        check_login();
        $this->load->model('admin/Cases_two_model');
        if ($this->session->userdata('language') != "") {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        } else {
            $this->lang->load('default');
            $this->lang->load('default_1');
        }
        $this->session->set_userdata('active', 'banks');
    }

    public function index() {
        //load view
        if (substr($this->session->userdata('levels')->banks, 0, 1) == '0') {
            $this->session->set_flashdata('no_permission', 1);
            redirect('admin/Dashboard');
        }
        $data['title'] = $this->lang->line("banks");
        $data['banks'] = $this->db->get_where('banks', array('deleted' => 0))->result();
        $data['label_att'] = 'col-sm-3 control-label';
        $this->load->view("admin/pages/banks/index", $data);
    }

    public function bank($id = '') {
        $id = sanitize($id);
        $bank = $this->db->get_where('banks', array('id' => $id))->row();
        $unique_name = '';
        $unique_code = '';
        if ($id != '') {
            if (substr($this->session->userdata('levels')->banks, 2, 1) == '0') {
                $this->session->set_flashdata('no_permission', 1);
                redirect('admin/Dashboard');
            }
            if ($this->input->post() && $bank->name != $this->input->post('bank_name')) {
                $unique_name = "|is_unique[banks.name]";
            }
            if ($this->input->post() && $bank->code != $this->input->post('bank_code')) {
                $unique_code = "|is_unique[banks.code]";
            }
        } else {
            if (substr($this->session->userdata('levels')->banks, 1, 1) == '0') {
                $this->session->set_flashdata('no_permission', 1);
                redirect('admin/Dashboard');
            }
            $unique_name = "|callback_unique_bank";
            $unique_code = "|callback_unique_code";
        }
        $this->form_validation->set_rules("bank_name", $this->lang->line("bank_name"), "xss_clean|trim|required$unique_name");
        $this->form_validation->set_rules("bank_code", $this->lang->line("bank_code"), "xss_clean|trim|required$unique_code");

        if ($this->form_validation->run()) {
            if ($_FILES['userfile']['name'] != '') {
                $config['upload_path'] = './uploads/banks_logos';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '10000';
                $config['max_width'] = '5000';
                $config['max_height'] = '5000';
                $this->lang->load('upload');
                $this->load->library('upload', $config);
                if ($this->upload->do_upload()) {
                    $upload_data = $this->upload->data();
                    $data['logo'] = $upload_data['file_name'];
                } else {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    $data['title'] = $this->lang->line("banks");
                    if ($id != '') {
                        $data['bank'] = $bank;
                    }
                    $data['label_att'] = 'col-sm-2 control-label';
                    $data['attributes'] = array('class' => 'form-horizontal', 'role' => 'form');
                    $this->load->view("admin/pages/banks/bank", $data);
                    return;
                }
            }
            $data['name'] = $this->input->post('bank_name');
            $data['code'] = $this->input->post('bank_code');
            $data['sidebar_bg_color'] = $this->input->post('sidebar_bg_color');
            $data['topbar_bg_color'] = $this->input->post('topbar_bg_color');
            $data['sidebar_links_highlight'] = $this->input->post('sidebar_links_highlight');
            $data['sidebar_links_font_color'] = $this->input->post('sidebar_links_font_color');
            $data['topbar_links_font_color'] = $this->input->post('topbar_links_font_color');
            $data['pages_header_bg'] = $this->input->post('pages_header_bg');
            $data['pages_header_font_color'] = $this->input->post('pages_header_font_color');
            if ($this->input->post('non_subscriber')) {
                $data['non_subscriber'] = 1;
            } else {
                $data['non_subscriber'] = 0;
            }

            if ($id == '') {        // Add bank
//                if ($data['non_subscriber'] == 0) {
                $bank_name=  $this->replace_bank_name($this->input->post('bank_name'));
                    if (!is_dir('root/' . trim($bank_name))) {
                        mkdir('root/' . trim($bank_name), 0777, TRUE);
                        //add index
                        copy('root/index.php','root/' . trim($bank_name)."/index.php" );
                    }
//                }
                $data['id'] = incremented('banks');
                $data['deleted'] = 0;
                $this->db->insert('banks', $data);
                // audit
                save_audit("Insert new bank -- " . $data['name']);
                /////////
            } else {      // Edit bank
//                if ($data['non_subscriber'] == 0) {
                $bank_name=  $this->replace_bank_name($this->input->post('bank_name'));
                    if (!is_dir('root/' . $this->replace_bank_name($bank->name))) {
                        mkdir('root/' . trim($bank_name), 0777, TRUE);
                        //add index file
                        copy('root/index.php','root/' . trim($bank_name)."/index.php" );
                    } else if (!is_dir('root/' . trim($bank_name))) {
                        rename('root/' . $this->replace_bank_name($bank->name), 'root/' . trim($bank_name));
                    }
//                }
                $this->db->update('banks', $data, array('id' => $id));
                // audit
                save_audit("Update bank Information -- " . $data['name']);
                /////////
            }
            $this->session->set_flashdata('success', $this->lang->line("data_saved_successfully"));
            redirect('admin/Banks');
        } else {
            $data['title'] = $this->lang->line("banks");
            if ($id != '') {
                $data['bank'] = $this->db->get_where('banks', array('id' => $id))->row();
            }
            $data['label_att'] = 'col-sm-2 control-label';
            $data['attributes'] = array('class' => 'form-horizontal', 'role' => 'form');
            $this->load->view("admin/pages/banks/bank", $data);
        }
    }

    function valid_dir_name($dir_name) {
        $dir_name = sanitize($dir_name);
        if (strpos($dir_name, '/') !== false || strpos($dir_name, '\\') !== false ||
                strpos($dir_name, ':') !== false || strpos($dir_name, '*') !== false ||
                strpos($dir_name, '?') !== false || strpos($dir_name, '"') !== false ||
                strpos($dir_name, '<') !== false || strpos($dir_name, '>') !== false ||
                strpos($dir_name, '|') !== false || strpos($dir_name, '&') !== false ||
                strpos($dir_name, ',') !== false || strpos($dir_name, '.') !== false) {
            $this->form_validation->set_message('valid_dir_name', "The {field} can't contain any of the following charachters /\:*?\"<>|&, ");
            return FALSE;
        }
        return TRUE;
    }

    function delete_bank($bank_id) {
        $bank_id = sanitize($bank_id);
        if (substr($this->session->userdata('levels')->banks, 3, 1) == '0') {
            $this->session->set_flashdata('no_permission', 1);
            redirect('admin/Dashboard');
        }
        $this->db->update('banks', array('deleted' => 1), array('id' => $bank_id));
        $bank = $this->db->get_where('banks', array('id' => $bank_id))->row();
        if ($bank && $bank->logo != '') {
            unlink('./uploads/banks_logos/' . $bank->logo);
        }
        // audit
        save_audit("delete bank -- " . $bank->name);
        /////////
        $this->session->set_flashdata('success', $this->lang->line('data_deleted_successfully'));
        redirect('admin/Banks');
    }

    function get_bank_branches($bank_name) {
        $bank_name = sanitize($bank_name);
        $bank_name = str_replace('%20', ' ', $bank_name);
        echo json_encode(scandir('root/' . $bank_name));
    }
    
    function unique_bank($value){
        $this->form_validation->set_message("unique_bank",  $this->lang->line("bank_name_unique"));
        $bank = $this->db->get_where('banks', array('name' => $value,"deleted !="=>1))->row();
        if(!empty($bank)){
            return FALSE;
        }
        return TRUE;
    }
    function unique_code($value){
        $this->form_validation->set_message("unique_code",  $this->lang->line("bank_code_unique"));
        $bank = $this->db->get_where('banks', array('code' => $value,"deleted !="=>1))->row();
        if(!empty($bank)){
            return FALSE;
        }
        return TRUE;
    }
  
    function replace_bank_name($bank_name){
        $needles=['&lt;','&gt;','/','\\',':','<','>', '*','?','"','|','&',',','.'];
        return str_replace($needles, "_", $bank_name);
    }
	
	function test_SS(){
		echo 'FFFFFF';
	}
}
