<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bulk extends CI_Controller {

    public $errors;
    public $excel_check_times = 0;
    public $excel_size_check_times = 0;

    public function __construct() {
        parent::__construct();
        check_login();
        $this->load->model('admin/Bulk_model');
        if ($this->session->userdata('language') != "") {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        } else {
            $this->lang->load('default');
            $this->lang->load('default_1');
        }
        $this->session->set_userdata('active', 'bulk');
        $this->errors = [];
    }

    public function index() {
        $this->form_validation->set_rules("cases_file", $this->lang->line("cases_file"), "callback_file_validation");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("bulk_case_creation");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['sample_link'] = base_url() . "template/Bulk Template.xlsx";
            $this->load->view("admin/pages/bulk/index", $data);
        } else {
            //upload file
            $config['upload_path'] = "./bulk/";
            $config['allowed_types'] = 'xlsx';
            $this->lang->load('upload');
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('cases_file')) {
                $upload_data = $this->upload->data();
                $file_name = $upload_data['file_name'];
                //read file
                $this->read_file($file_name);
                // remove valid cases from file
                //export file
            } else {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata("error", $error);
                redirect(base_url("admin/Bulk"));
            }
            echo "Success!";
        }
    }

    public function file_validation() {
        $this->form_validation->set_message("file_validation", $this->lang->line("file_required"));
        if ($_FILES['cases_file']['name'] == "") {
            return FALSE;
        }
        return TRUE;
    }

    public function read_file($file_name) {
//        $file_name = 'Bulk_File_160820181.csv';
        ini_set('memory_limit', '2048M');
        set_time_limit(1800);
        
        $objPHPexcel = \PhpOffice\PhpSpreadsheet\IOFactory::load("./bulk/$file_name");
        $sheetCount = $objPHPexcel->getSheetCount();
        $active_sheet = 0;
        if($sheetCount > 1){
            $active_sheet = 1;
        }
        $objWorksheet = $objPHPexcel->setActiveSheetIndex($active_sheet);
        
        $max_row = $objWorksheet->getHighestRow();
        $max_col = $objWorksheet->getHighestColumn();

        $info_labels = array('A' => 'C4', 'B' => 'E4', 'C' => 'C5', 'D' => 'C6', 'E' => 'C7', 'F' => 'C8', 'G' => 'C9', 'H' => 'C10', 'I' => 'C11', 'J' => 'C12', 'K' => 'C13',
            'BA' => 'B25', 'BB' => 'C25', 'BC' => 'D25', 'BD' => 'B26', 'BE' => 'C26', 'BF' => 'D26', 'BG' => 'B27', 'BH' => 'C27', 'BI' => 'D27', 'BJ' => 'B28', 'BK' => 'C28', 'BL' => 'D28', 'BM' => 'B29', 'BN' => 'C29', 'BO' => 'D29', 'BP' => 'B30', 'BQ' => 'C30', 'BR' => 'D30', 'BS' => 'D32',
            'L' => 'C36', 'VM' => 'E7', 'VN' => 'E10');
        $fact_labels = array('M' => 'C4', 'N' => 'E4', 'O' => 'C5',
            'MG' => 'C9', 'MH' => 'D9', 'MI' => 'C10', 'MJ' => 'D10', 'MK' => 'C11', 'ML' => 'D11', 'MM' => 'C12', 'MN' => 'D12', 'MO' => 'C13', 'MP' => 'D13', 'MQ' => 'C14', 'MR' => 'D14', 'MS' => 'C15', 'MT' => 'D15', 'MU' => 'C16', 'MV' => 'D16', 'MW' => 'C17', 'MX' => 'D17', 'MY' => 'C18', 'MZ' => 'D18', 'NA' => 'C22', 'NB' => 'D22', 'NC' => 'C23', 'ND' => 'D23', 'NE' => 'C24', 'NF' => 'D24', 'NG' => 'C25', 'NH' => 'D25', 'NI' => 'C26', 'NJ' => 'D26', 'NK' => 'C27', 'NL' => 'D27', 'NM' => 'C28', 'NN' => 'D28', 'NO' => 'C29', 'NP' => 'D29', 'NQ' => 'C30', 'NR' => 'D30', 'NS' => 'C31', 'NT' => 'D31', 'NU' => 'C35', 'NV' => 'D35', 'NW' => 'C36', 'NX' => 'D36', 'NY' => 'C37', 'NZ' => 'D37', 'OA' => 'C38', 'OB' => 'D38', 'OC' => 'C39', 'OD' => 'D39', 'OE' => 'C40', 'OF' => 'D40', 'OG' => 'C41', 'OH' => 'D41', 'OI' => 'C42', 'OJ' => 'D42', 'OK' => 'C43', 'OL' => 'D43', 'OM' => 'C44', 'ON' => 'D44'
        );
        $fin_labels = array('BT' => 'E3', 'BU' => 'E4', 'BV' => 'G8',
            //base (E)
            'BW' => 'E10', 'BX' => 'E13', 'BY' => 'E14', 'BZ' => 'E18', 'CA' => 'E19', 'CB' => 'E22', 'CC' => 'E26', 'CD' => 'E27', 'CE' => 'E28', 'CF' => 'E32', 'CG' => 'E33', 'CH' => 'E34', 'CI' => 'E35', 'CJ' => 'E36', 'CK' => 'E38', 'CL' => 'E39', 'CM' => 'E43', 'CN' => 'E49', 'CO' => 'E50', 'CP' => 'E51', 'CQ' => 'E55', 'CR' => 'E56', 'CS' => 'E58', 'CT' => 'E62', 'CU' => 'E63', 'CV' => 'E66', 'CW' => 'E67', 'CX' => 'E71', 'CY' => 'E78', 'CZ' => 'E79', 'DA' => 'E80', 'DB' => 'E81', 'DC' => 'E82', 'DD' => 'E83', 'DE' => 'E84', 'DF' => 'E85', 'DG' => 'E86', 'DH' => 'E87', 'DI' => 'E88', 'DJ' => 'E89', 'DK' => 'E90', 'DL' => 'E92', 'DM' => 'E93', 'DN' => 'E94', 'DO' => 'E97', 'DP' => 'E98', 'DQ' => 'E99', 'DR' => 'E100', 'DS' => 'E106', 'DT' => 'E107', 'DU' => 'E108', 'DV' => 'E109', 'DW' => 'E110', 'DX' => 'E111', 'DY' => 'E112', 'DZ' => 'E113', 'EA' => 'E114', 'EB' => 'E115', 'EC' => 'E117', 'ED' => 'E119', 'EE' => 'E126', 'EF' => 'E127', 'EG' => 'E129', 'EH' => 'E130', 'EI' => 'E132', 'EJ' => 'E133', 'EK' => 'E134', 'EL' => 'E135', 'EM' => 'E136', 'EN' => 'E137', 'EO' => 'E138', 'EP' => 'E139', 'EQ' => 'E140', 'ER' => 'E145', 'ES' => 'E146', 'ET' => 'E147', 'EU' => 'E148', 'EV' => 'E149', 'EW' => 'E153', 'EX' => 'E154', 'EY' => 'E155', 'EZ' => 'E156', 'FA' => 'E158', 'FB' => 'E159', 'FC' => 'E160', 'FD' => 'E161', 'FE' => 'E162', 'FF' => 'E163', 'FG' => 'E166', 'FH' => 'E167',
            //hist two (C)
            'IU' => 'C10', 'IV' => 'C13', 'IW' => 'C14', 'IX' => 'C18', 'IY' => 'C19', 'IZ' => 'C22', 'JA' => 'C26', 'JB' => 'C27', 'JC' => 'C28', 'JD' => 'C32', 'JE' => 'C33', 'JF' => 'C34', 'JG' => 'C35', 'JH' => 'C36', 'JI' => 'C38', 'JJ' => 'C39', 'JK' => 'C43', 'JL' => 'C49', 'JM' => 'C50', 'JN' => 'C51', 'JO' => 'C55', 'JP' => 'C56', 'JQ' => 'C58', 'JR' => 'C62', 'JS' => 'C63', 'JT' => 'C66', 'JU' => 'C67', 'JV' => 'C71', 'JW' => 'C78', 'JX' => 'C79', 'JY' => 'C80', 'JZ' => 'C81', 'KA' => 'C82', 'KB' => 'C83', 'KC' => 'C84', 'KD' => 'C85', 'KE' => 'C86', 'KF' => 'C87', 'KG' => 'C88', 'KH' => 'C89', 'KI' => 'C90', 'KJ' => 'C92', 'KK' => 'C93', 'KL' => 'C94', 'KM' => 'C97', 'KN' => 'C98', 'KO' => 'C99', 'KP' => 'C100', 'KQ' => 'C106', 'KR' => 'C107', 'KS' => 'C108', 'KT' => 'C109', 'KU' => 'C110', 'KV' => 'C111', 'KW' => 'C112', 'KX' => 'C113', 'KY' => 'C114', 'KZ' => 'C115', 'LA' => 'C117', 'LB' => 'C119', 'LC' => 'C126', 'LD' => 'C127', 'LE' => 'C129', 'LF' => 'C130', 'LG' => 'C132', 'LH' => 'C133', 'LI' => 'C134', 'LJ' => 'C135', 'LK' => 'C136', 'LL' => 'C137', 'LM' => 'C138', 'LN' => 'C139', 'LO' => 'C140', 'LP' => 'C145', 'LQ' => 'C146', 'LR' => 'C147', 'LS' => 'C148', 'LT' => 'C149', 'LU' => 'C153', 'LV' => 'C154', 'LW' => 'C155', 'LX' => 'C156', 'LY' => 'C158', 'LZ' => 'C159', 'MA' => 'C160', 'MB' => 'C161', 'MC' => 'C162', 'MD' => 'C163', 'ME' => 'C166', 'MF' => 'C167',
            //hist one (D)
            'FI' => 'D10', 'FJ' => 'D13', 'FK' => 'D14', 'FL' => 'D18', 'FM' => 'D19', 'FN' => 'D22', 'FO' => 'D26', 'FP' => 'D27', 'FQ' => 'D28', 'FR' => 'D32', 'FS' => 'D33', 'FT' => 'D34', 'FU' => 'D35', 'FV' => 'D36', 'FW' => 'D38', 'FX' => 'D39', 'FY' => 'D43', 'FZ' => 'D49', 'GA' => 'D50', 'GB' => 'D51', 'GC' => 'D55', 'GD' => 'D56', 'GE' => 'D58', 'GF' => 'D62', 'GG' => 'D63', 'GH' => 'D66', 'GI' => 'D67', 'GJ' => 'D71', 'GK' => 'D78', 'GL' => 'D79', 'GM' => 'D80', 'GN' => 'D81', 'GO' => 'D82', 'GP' => 'D83', 'GQ' => 'D84', 'GR' => 'D85', 'GS' => 'D86', 'GT' => 'D87', 'GU' => 'D88', 'GV' => 'D89', 'GW' => 'D90', 'GX' => 'D92', 'GY' => 'D93', 'GZ' => 'D94', 'HA' => 'D97', 'HB' => 'D98', 'HC' => 'D99', 'HD' => 'D100', 'HE' => 'D106', 'HF' => 'D107', 'HG' => 'D108', 'HH' => 'D109', 'HI' => 'D110', 'HJ' => 'D111', 'HK' => 'D112', 'HL' => 'D113', 'HM' => 'D114', 'HN' => 'D115', 'HO' => 'D117', 'HP' => 'D119', 'HQ' => 'D126', 'HR' => 'D127', 'HS' => 'D129', 'HT' => 'D130', 'HU' => 'D132', 'HV' => 'D133', 'HW' => 'D134', 'HX' => 'D135', 'HY' => 'D136', 'HZ' => 'D137', 'IA' => 'D138', 'IB' => 'D139', 'IC' => 'D140', 'ID' => 'D145', 'IE' => 'D146', 'IF' => 'D147', 'IG' => 'D148', 'IH' => 'D149', 'II' => 'D153', 'IJ' => 'D154', 'IK' => 'D155', 'IL' => 'D156', 'IM' => 'D158', 'IN' => 'D159', 'IO' => 'D160', 'IP' => 'D161', 'IQ' => 'D162', 'IR' => 'D163', 'IS' => 'D166', 'IT' => 'D167',
            //proj one (F)
            'OO' => 'F10', 'OP' => 'F13', 'OQ' => 'F14', 'OR' => 'F18', 'OS' => 'F19', 'OT' => 'F22', 'OU' => 'F26', 'OV' => 'F27', 'OW' => 'F28', 'OX' => 'F32', 'OY' => 'F33', 'OZ' => 'F34', 'PA' => 'F35', 'PB' => 'F36', 'PC' => 'F38', 'PD' => 'F39', 'PE' => 'F43', 'PF' => 'F49', 'PG' => 'F50', 'PH' => 'F51', 'PI' => 'F55', 'PJ' => 'F56', 'PK' => 'F58', 'PL' => 'F62', 'PM' => 'F63', 'PN' => 'F66', 'PO' => 'F67', 'PP' => 'F71', 'PQ' => 'F78', 'PR' => 'F79', 'PS' => 'F80', 'PT' => 'F81', 'PU' => 'F82', 'PV' => 'F83', 'PW' => 'F84', 'PX' => 'F85', 'PY' => 'F86', 'PZ' => 'F87', 'QA' => 'F88', 'QB' => 'F89', 'QC' => 'F90', 'QD' => 'F92', 'QE' => 'F93', 'QF' => 'F94', 'QG' => 'F97', 'QH' => 'F98', 'QI' => 'F99', 'QJ' => 'F100', 'QK' => 'F106', 'QL' => 'F107', 'QM' => 'F108', 'QN' => 'F109', 'QO' => 'F110', 'QP' => 'F111', 'QQ' => 'F112', 'QR' => 'F113', 'QS' => 'F114', 'QT' => 'F115', 'QU' => 'F117', 'QV' => 'F119', 'QW' => 'F126', 'QX' => 'F127', 'QY' => 'F129', 'QZ' => 'F130', 'RA' => 'F132', 'RB' => 'F133', 'RC' => 'F134', 'RD' => 'F135', 'RE' => 'F136', 'RF' => 'F137', 'RG' => 'F138', 'RH' => 'F139', 'RI' => 'F140', 'RJ' => 'F145', 'RK' => 'F146', 'RL' => 'F147', 'RM' => 'F148', 'RN' => 'F149', 'RO' => 'F153', 'RP' => 'F154', 'RQ' => 'F155', 'RR' => 'F156', 'RS' => 'F158', 'RT' => 'F159', 'RU' => 'F160', 'RV' => 'F161', 'RW' => 'F162', 'RX' => 'F163', 'RY' => 'F166', 'RZ' => 'F167',
            ///proj two (G)
            'SA' => 'G10', 'SB' => 'G13', 'SC' => 'G14', 'SD' => 'G18', 'SE' => 'G19', 'SF' => 'G22', 'SG' => 'G26', 'SH' => 'G27', 'SI' => 'G28', 'SJ' => 'G32', 'SK' => 'G33', 'SL' => 'G34', 'SM' => 'G35', 'SN' => 'G36', 'SO' => 'G38', 'SP' => 'G39', 'SQ' => 'G43', 'SR' => 'G49', 'SS' => 'G50', 'ST' => 'G51', 'SU' => 'G55', 'SV' => 'G56', 'SW' => 'G58', 'SX' => 'G62', 'SY' => 'G63', 'SZ' => 'G66', 'TA' => 'G67', 'TB' => 'G71', 'TC' => 'G78', 'TD' => 'G79', 'TE' => 'G80', 'TF' => 'G81', 'TG' => 'G82', 'TH' => 'G83', 'TI' => 'G84', 'TJ' => 'G85', 'TK' => 'G86', 'TL' => 'G87', 'TM' => 'G88', 'TN' => 'G89', 'TO' => 'G90', 'TP' => 'G92', 'TQ' => 'G93', 'TR' => 'G94', 'TS' => 'G97', 'TT' => 'G98', 'TU' => 'G99', 'TV' => 'G100', 'TW' => 'G106', 'TX' => 'G107', 'TY' => 'G108', 'TZ' => 'G109', 'UA' => 'G110', 'UB' => 'G111', 'UC' => 'G112', 'UD' => 'G113', 'UE' => 'G114', 'UF' => 'G115', 'UG' => 'G117', 'UH' => 'G119', 'UI' => 'G126', 'UJ' => 'G127', 'UK' => 'G129', 'UL' => 'G130', 'UM' => 'G132', 'UN' => 'G133', 'UO' => 'G134', 'UP' => 'G135', 'UQ' => 'G136', 'UR' => 'G137', 'US' => 'G138', 'UT' => 'G139', 'UU' => 'G140', 'UV' => 'G145', 'UW' => 'G146', 'UX' => 'G147', 'UY' => 'G148', 'UZ' => 'G149', 'VA' => 'G153', 'VB' => 'G154', 'VC' => 'G155', 'VD' => 'G156', 'VE' => 'G158', 'VF' => 'G159', 'VG' => 'G160', 'VH' => 'G161', 'VI' => 'G162', 'VJ' => 'G163', 'VK' => 'G166', 'VL' => 'G167'
        );
        $assess_labels = array('P' => 'F5', 'Q' => 'F10', 'R' => 'F18', 'S' => 'F22', 'T' => 'F29', 'U' => 'F36', 'V' => 'F40', 'W' => 'F45', 'X' => 'F50', 'Y' => 'F56', 'Z' => 'F66', 'AA' => 'F71', 'AB' => 'F75', 'AC' => 'F79', 'AD' => 'F86', 'AE' => 'F93', 'AF' => 'F100', 'AG' => 'F107', 'AH' => 'F115', 'AI' => 'F121', 'AJ' => 'F125', 'AK' => 'F129', 'AL' => 'F133', 'AM' => 'F137', 'AN' => 'F144', 'AO' => 'F151', 'AP' => 'F155', 'AQ' => 'F159', 'AR' => 'F165', 'AS' => 'F169', 'AT' => 'F175', 'AU' => 'F179', 'AV' => 'F186', 'AW' => 'F194', 'AX' => 'F200', 'AY' => 'F208', 'AZ' => 'F216');

        $sheet_data = $objWorksheet->rangeToArray(
                "A2:{$max_col}{$max_row}", // The worksheet range that we want to retrieve
                NULL, // Value that should be returned for empty cells
                TRUE, // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
                TRUE, // Should values be formatted (the equivalent of getFormattedValue() for each cell)
                TRUE         // Should the array be indexed by cell row and cell column
        );
//                        print_r($sheet_data);die;
        $valid_cases = [];
        $invalid_cases = [];
        $valid_fins = [];
		
        foreach ($sheet_data as $row => $case_arr) {
			
            if (empty($case_arr['A']) || $case_arr['A'] == "" || strtolower($case_arr['A']) == 'eof') {
                break;
            }
			
            $this->declare_errors();
            $empty_case = TRUE;
            $info = [];
            $fact = [];
            $fin = [];
            $assess = [];
            $current = "info";
            foreach ($info_labels as $file_col => $original_col) {
                if ($original_col == "C9") {
                    $info[$original_col] = "" . $case_arr[$file_col];
                } else {
                    $info[$original_col] = $case_arr[$file_col];
                }
            }
            foreach ($fact_labels as $file_col => $original_col) {
                $fact[$original_col] = $case_arr[$file_col];
            }
            foreach ($fin_labels as $file_col => $original_col) {
                if($file_col == 'BT'){
                    $case_arr[$file_col] = $case_arr[$file_col] == '' ? '2' : $case_arr[$file_col];
                }
                $fin[$original_col] = $case_arr[$file_col];
            }
            foreach ($assess_labels as $file_col => $original_col) {
                $assess[$original_col] = $case_arr[$file_col];
            }
            $out_of_cat_errors = "";
            //map catalogue values
            if(is_numeric($case_arr['H'])) {        // for case with code not dropdown (in bulk excel file)
                $info = $this->map_info($info, $out_of_cat_errors);
    
                $fin = $this->map_fin($fin, $out_of_cat_errors);
    
                $assess = $this->map_assess($assess, $out_of_cat_errors);
            }
            ////////////////
            //validate case
            $all_validation_errors = "";
            $info_valid = $this->validate_info($info);
            $all_validation_errors .= $this->check_validation($info_valid);
            $fact_valid = $this->validate_fact($fact);
            $all_validation_errors .= $this->check_validation($fact_valid);
            $fin_valid = $this->validate_fin($fin, $info_valid["info"]);
            $all_validation_errors .= $this->check_validation($fin_valid);
            $assess_valid = $this->validate_assess($assess, $info_valid["info"]);
            $all_validation_errors .= $this->check_validation($assess_valid);
            if ($info_valid['result'] == "success" && $fact_valid['result'] == "success" && $fin_valid['result'] == "success" && $assess_valid['result'] == "success") {
                //check old case existence
                $this->check_case_existence($info_valid['info']['C5']);
                //insert data if valid
                $info_valid['info'] = $this->unmap_info($info_valid['info']);
                $case_id = $this->insert_info($info_valid['info']);
                $fact_valid['fact']['case_id'] = $case_id;
                $fin_valid['fin']['case_id'] = $case_id;
                $assess_valid['assess']['case_id'] = $case_id;
                $this->insert_fact($fact_valid['fact']);
                $valid_fins[] = $fin_valid['fin'];
                //                $fin_valid['fin']=$this->unmap_fin($fin_valid['fin']);
                $this->insert_fin($fin_valid['fin']);
                $assess_valid['assess'] = $this->unmap_assess($assess_valid['assess']);
                $this->insert_assess($assess_valid['assess']);
                //add row to array to be deleted from sheet
                $valid_cases[] = $row;
                $approved = $this->approve_case($case_id, $file_name);
                if ($approved !== TRUE) {
                    $case_name = $this->get_case_name($case_id);
                    $invalid_cases[$row] = $this->lang->line($approved) . ' with name ' . $case_name;
                }
            } else {
                $invalid_cases[$row] = $all_validation_errors;
                //check old case existence
                $this->check_case_existence($info_valid['info']['C5']);
//                insert and set status to borrower_information in order to be edited
                $info_valid['info'] = $this->unmap_info($info_valid['info']);
                $case_id = $this->insert_info($info_valid['info'], 1);
                $fact_valid['fact']['case_id'] = $case_id;
                $fin_valid['fin']['case_id'] = $case_id;
                $assess_valid['assess']['case_id'] = $case_id;
                $this->insert_fact($fact_valid['fact']);
                $valid_fins[] = $fin_valid['fin'];
                $this->insert_fin($fin_valid['fin']);
                $assess_valid['assess'] = $this->unmap_assess($assess_valid['assess']);
                $this->insert_assess($assess_valid['assess']);
                $case_name = $this->get_case_name($case_id);
                save_audit('Bulk Xlsx : case : ' . $case_name . ' added with validation errors from file:' . $file_name);
            }
            ////////////////////
//            }
        }
        $failed_url = "";
        if (!empty($valid_cases)) {
            rsort($valid_cases);
            for ($i = 0; $i < count($valid_cases); $i++) {
                $objWorksheet->removeRow($valid_cases[$i], 1);
            }
        }
        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objPHPexcel, 'Xlsx');
        if (!is_dir('bulk_failed')) {
            mkdir('bulk_failed', 0777, TRUE);
            //add index file
            copy('root/index.php', "bulk_failed/index.php");
        }
        $failed_name = strtotime("now") . "_" . $file_name;
        $objWriter->save("./bulk_failed/" . $failed_name);
        $failed_url = base_url() . "bulk_failed/" . $failed_name;
        //display errors report
//        $valid_fins=array(array("case_id"=>33, "C1"=>"a", "C2"=>"b"));
        if (empty($valid_cases) || !empty($invalid_cases)) {
            $bulk_data['invalid_cases'] = $invalid_cases;
            $bulk_data['failed_url'] = $failed_url;
            $bulk_data['valid_fins'] = $valid_fins;
            $bulk_data['status'] = "invalid";
            $this->session->set_flashdata("bulk_data", $bulk_data);
            redirect(base_url("admin/Bulk/report"));
        } else {
            $bulk_data['valid_fins'] = $valid_fins;
            $bulk_data['status'] = "valid";
            $this->session->set_flashdata("bulk_data", $bulk_data);
            redirect(base_url("admin/Bulk/report"));
        }
    }

    public function report() {
        $data = $this->session->flashdata("bulk_data");
        if ($data) {
            $data['title'] = $this->lang->line("bulk_case_creation_report");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['fin_labels'] = array('E3', 'E4', 'G8', 'C10', 'D10', 'E10', 'F10', 'G10', 'C13', 'D13', 'E13', 'F13', 'G13', 'C14', 'D14', 'E14', 'F14', 'G14', 'C18', 'D18', 'E18', 'F18', 'G18', 'C19', 'D19', 'E19', 'F19', 'G19', 'C22', 'D22', 'E22', 'F22', 'G22', 'C26', 'D26', 'E26', 'F26', 'G26', 'C27', 'D27', 'E27', 'F27', 'G27', 'C28', 'D28', 'E28', 'F28', 'G28', 'C32', 'D32', 'E32', 'F32', 'G32', 'C33', 'D33', 'E33', 'F33', 'G33', 'C34', 'D34', 'E34', 'F34', 'G34', 'C35', 'D35', 'E35', 'F35', 'G35', 'C36', 'D36', 'E36', 'F36', 'G36', 'C38', 'D38', 'E38', 'F38', 'G38', 'C39', 'D39', 'E39', 'F39', 'G39', 'C43', 'D43', 'E43', 'F43', 'G43', 'C49', 'D49', 'E49', 'F49', 'G49', 'C50', 'D50', 'E50', 'F50', 'G50', 'C51', 'D51', 'E51', 'F51', 'G51', 'C55', 'D55', 'E55', 'F55', 'G55', 'C56', 'D56', 'E56', 'F56', 'G56', 'C58', 'D58', 'E58', 'F58', 'G58', 'C62', 'D62', 'E62', 'F62', 'G62', 'C63', 'D63', 'E63', 'F63', 'G63', 'C66', 'D66', 'E66', 'F66', 'G66', 'C67', 'D67', 'E67', 'F67', 'G67', 'C71', 'D71', 'E71', 'F71', 'G71', 'C78', 'D78', 'E78', 'F78', 'G78', 'C79', 'D79', 'E79', 'F79', 'G79', 'C80', 'D80', 'E80', 'F80', 'G80', 'C81', 'D81', 'E81', 'F81', 'G81', 'C82', 'D82', 'E82', 'F82', 'G82', 'C83', 'D83', 'E83', 'F83', 'G83', 'C84', 'D84', 'E84', 'F84', 'G84', 'C85', 'D85', 'E85', 'F85', 'G85', 'C86', 'D86', 'E86', 'F86', 'G86', 'C87', 'D87', 'E87', 'F87', 'G87', 'C88', 'D88', 'E88', 'F88', 'G88', 'C89', 'D89', 'E89', 'F89', 'G89', 'C90', 'D90', 'E90', 'F90', 'G90', 'C92', 'D92', 'E92', 'F92', 'G92', 'C93', 'D93', 'E93', 'F93', 'G93', 'C94', 'D94', 'E94', 'F94', 'G94', 'C97', 'D97', 'E97', 'F97', 'G97', 'C98', 'D98', 'E98', 'F98', 'G98', 'C99', 'D99', 'E99', 'F99', 'G99', 'C100', 'D100', 'E100', 'F100', 'G100', 'C106', 'D106', 'E106', 'F106', 'G106', 'C107', 'D107', 'E107', 'F107', 'G107', 'C108', 'D108', 'E108', 'F108', 'G108', 'C109', 'D109', 'E109', 'F109', 'G109', 'C110', 'D110', 'E110', 'F110', 'G110', 'C111', 'D111', 'E111', 'F111', 'G111', 'C112', 'D112', 'E112', 'F112', 'G112', 'C113', 'D113', 'E113', 'F113', 'G113', 'C114', 'D114', 'E114', 'F114', 'G114', 'C115', 'D115', 'E115', 'F115', 'G115', 'C117', 'D117', 'E117', 'F117', 'G117', 'C119', 'D119', 'E119', 'F119', 'G119', 'C126', 'D126', 'E126', 'F126', 'G126', 'C127', 'D127', 'E127', 'F127', 'G127', 'C129', 'D129', 'E129', 'F129', 'G129', 'C130', 'D130', 'E130', 'F130', 'G130', 'C132', 'D132', 'E132', 'F132', 'G132', 'C133', 'D133', 'E133', 'F133', 'G133', 'C134', 'D134', 'E134', 'F134', 'G134', 'C135', 'D135', 'E135', 'F135', 'G135', 'C136', 'D136', 'E136', 'F136', 'G136', 'C137', 'D137', 'E137', 'F137', 'G137', 'C138', 'D138', 'E138', 'F138', 'G138', 'C139', 'D139', 'E139', 'F139', 'G139', 'C140', 'D140', 'E140', 'F140', 'G140', 'C144', 'D144', 'E144', 'F144', 'G144', 'C145', 'D145', 'E145', 'F145', 'G145', 'C146', 'D146', 'E146', 'F146', 'G146', 'C147', 'D147', 'E147', 'F147', 'G147', 'C148', 'D148', 'E148', 'F148', 'G148', 'C149', 'D149', 'E149', 'F149', 'G149', 'C153', 'D153', 'E153', 'F153', 'G153', 'C154', 'D154', 'E154', 'F154', 'G154', 'C155', 'D155', 'E155', 'F155', 'G155', 'C156', 'D156', 'E156', 'F156', 'G156', 'C158', 'D158', 'E158', 'F158', 'G158', 'C159', 'D159', 'E159', 'F159', 'G159', 'C160', 'D160', 'E160', 'F160', 'G160', 'C161', 'D161', 'E161', 'F161', 'G161', 'C162', 'D162', 'E162', 'F162', 'G162', 'C163', 'D163', 'E163', 'F163', 'G163', 'C166', 'D166', 'E166', 'F166', 'G166', 'C167', 'D167', 'E167', 'F167', 'G167');
            $data['fields'] = array('C12', 'D12', 'E12', 'F12', 'G12', 'C20', 'D20', 'E20', 'F20', 'G20', 'C23', 'D23', 'E23', 'F23', 'G23', 'C29', 'D29', 'E29', 'F29', 'G29', 'C31', 'D31', 'E31', 'F31', 'G31', 'C41', 'D41', 'E41', 'F41', 'G41', 'C45', 'D45', 'E45', 'F45', 'G45', 'C48', 'D48', 'E48', 'F48', 'G48', 'C52', 'D52', 'E52', 'F52', 'G52', 'C54', 'D54', 'E54', 'F54', 'G54', 'C60', 'D60', 'E60', 'F60', 'G60', 'C65', 'D65', 'E65', 'F65', 'G65', 'C69', 'D69', 'E69', 'F69', 'G69', 'C73', 'D73', 'E73', 'F73', 'G73', 'C91', 'D91', 'E91', 'F91', 'G91', 'C95', 'D95', 'E95', 'F95', 'G95', 'C101', 'D101', 'E101', 'F101', 'G101', 'C103', 'D103', 'E103', 'F103', 'G103', 'C116', 'D116', 'E116', 'F116', 'G116', 'C121', 'D121', 'E121', 'F121', 'G121', 'C128', 'D128', 'E128', 'F128', 'G128', 'C131', 'D131', 'E131', 'F131', 'G131', 'C141', 'D141', 'E141', 'F141', 'G141', 'C144', 'D144', 'E144', 'F144', 'G144', 'C150', 'D150', 'E150', 'F150', 'G150', 'C157', 'D157', 'E157', 'F157', 'G157', 'C164', 'D164', 'E164', 'F164', 'G164', 'C168', 'D168', 'E168', 'F168', 'G168', 'C170', 'D170', 'E170', 'F170', 'G170', 'C171', 'D171', 'E171', 'F171', 'G171', 'C175', 'D175', 'E175', 'F175', 'G175', 'C176', 'D176', 'E176', 'F176', 'G176', 'C177', 'D177', 'E177', 'F177', 'G177', 'C178', 'D178', 'E178', 'F178', 'G178', 'C179', 'D179', 'E179', 'F179', 'G179', 'C180', 'D180', 'E180', 'F180', 'G180', 'C183', 'D183', 'E183', 'F183', 'G183', 'C184', 'D184', 'E184', 'F184', 'G184', 'C185', 'D185', 'E185', 'F185', 'G185', 'C186', 'D186', 'E186', 'F186', 'G186', 'C187', 'D187', 'E187', 'F187', 'G187', 'C189', 'D189', 'E189', 'F189', 'G189', 'C190', 'D190', 'E190', 'F190', 'G190', 'C191', 'D191', 'E191', 'F191', 'G191', 'C192', 'D192', 'E192', 'F192', 'G192', 'C195', 'D195', 'E195', 'F195', 'G195', 'C196', 'D196', 'E196', 'F196', 'G196', 'C197', 'D197', 'E197', 'F197', 'G197', 'C198', 'D198', 'E198', 'F198', 'G198', 'C199', 'D199', 'E199', 'F199', 'G199', 'C200', 'D200', 'E200', 'F200', 'G200', 'C203', 'D203', 'E203', 'F203', 'G203', 'C204', 'D204', 'E204', 'F204', 'G204', 'C205', 'D205', 'E205', 'F205', 'G205', 'C206', 'D206', 'E206', 'F206', 'G206', 'C207', 'D207', 'E207', 'F207', 'G207', 'C208', 'D208', 'E208', 'F208', 'G208', 'C209', 'D209', 'E209', 'F209', 'G209', 'C210', 'D210', 'E210', 'F210', 'G210', 'C211', 'D211', 'E211', 'F211', 'G211', 'C212', 'D212', 'E212', 'F212', 'G212', 'C213', 'D213', 'E213', 'F213', 'G213', 'C214', 'D214', 'E214', 'F214', 'G214', 'C218', 'D218', 'E218', 'F218', 'G218', 'C219', 'D219', 'E219', 'F219', 'G219', 'C220', 'D220', 'E220', 'F220', 'G220', 'C221', 'D221', 'E221', 'F221', 'G221', 'C222', 'D222', 'E222', 'F222', 'G222', 'C223', 'D223', 'E223', 'F223', 'G223', 'C224', 'D224', 'E224', 'F224', 'G224');
            $this->load->view("admin/pages/bulk/report", $data);
        } else {
            $this->session->set_flashdata('error',  $this->lang->line('something_went_wrong'));
            redirect(base_url("admin/Bulk"));
        }
    }

    public function validate_info($info) {
        $response['result'] = "success";
        $matched = $info;
        $matched['excel_name'] = "";
        $this->form_validation->set_data($matched);
        $this->form_validation->set_rules("E4", $this->lang->line("borrower_information_E4"), "xss_clean|trim|required|max_length[40]|callback_arabic_only");
        $this->form_validation->set_rules("E7", $this->lang->line("borrower_information_E7"), "xss_clean|trim|required");
        $this->form_validation->set_rules("E10", $this->lang->line("borrower_information_E10"), "xss_clean|trim|required|numeric");
        for ($i = 4; $i <= 13; $i++) {
            $extra = "";
            if ($i == 4) {
                //modified
                $extra = "|max_length[40]|callback_english_only";
            } elseif ($i == 5) {
                //modified
                $extra = "|greater_than_equal_to[1]|less_than_equal_to[99999999999999999999]|numeric|max_length[20]";
            } elseif ($i == 7) {
                //modified
                $extra = "";
                if (!empty($matched["C$i"]) && $matched["C$i"] != "") {
                    //modified
                    $extra = "|greater_than_equal_to[1]|less_than_equal_to[999999999999]|numeric|max_length[12]";
                }
                $this->form_validation->set_rules("C$i", $this->lang->line("borrower_information_C$i"), "xss_clean$extra");
                continue;
            } elseif ($i == 8) {
                //modified
                $extra = "";
                $matched["C$i"] = strval($matched["C$i"]);
                if ($matched["C$i"] === 0 || ($matched["C$i"] != NULL && $matched["C$i"] != "")) {
                    $extra = "|greater_than_equal_to[1]|less_than_equal_to[99999999999999]|numeric|exact_length[14]";
                }
                $validation_msgs = ['greater_than_equal_to' => $this->lang->line("nid_validation_error"), 'less_than_equal_to' => $this->lang->line("nid_validation_error"), 'numeric' => $this->lang->line("nid_validation_error"), 'exact_length' => $this->lang->line("nid_validation_error")];
                $this->form_validation->set_rules("C$i", $this->lang->line("borrower_information_C$i"), "xss_clean$extra", $validation_msgs);
                continue;
            } elseif ($i == 12) {
                $extra = "|greater_than_equal_to[0]|less_than_equal_to[200000000]";
            } elseif ($i == 13) {
                $extra = "|numeric|greater_than_equal_to[-500]|less_than_equal_to[500]";
            }
            $this->form_validation->set_rules("C$i", $this->lang->line("borrower_information_C$i"), "xss_clean|required$extra");
        }
        for ($i = 25; $i <= 30; $i++) {
            $extra = "";
            $this->form_validation->set_rules("B$i", $this->lang->line("borrower_information_B$i"), "xss_clean|trim");
            $this->form_validation->set_rules("C$i", $this->lang->line("borrower_information_C$i"), "xss_clean|trim");
            $this->form_validation->set_rules("D$i", $this->lang->line("borrower_information_D$i"), "xss_clean|trim");
        }
        //modified
        $this->form_validation->set_rules("D32", $this->lang->line("borrower_information_D32"), "xss_clean|trim");
        $this->form_validation->set_rules("C36", $this->lang->line("borrower_information_C36"), "xss_clean|trim|required|callback_validate_C36_date");
        $id = 0;
//        $this->form_validation->set_rules("excel_name", $this->lang->line("excel_name"), "callback_validate_excel_name[$id]");
        if (!$this->form_validation->run()) {
            $response['result'] = "fail";
            $response['errors'] = $this->set_errors();
//            $response['errors'] = validation_errors();
        }
        $response['info'] = $matched;
        return $response;
    }

    function english_only($value) {
        $this->form_validation->set_message("english_only", $this->lang->line("english_only"));
        if (!preg_match('/[^A-Za-z\s]/', $value)) {
            return TRUE;
        }
        return FALSE;
    }

    function arabic_only($value) {
        $this->form_validation->set_message("arabic_only", $this->lang->line("arabic_only"));
        if (preg_match('/^[\s\p{Arabic}]+$/u', $value)) {
            return TRUE;
        }
        return FALSE;
    }

    function validate_C36_date($date) {
        $date = sanitize($date);
        $this->form_validation->set_message('validate_C36_date', $this->lang->line("C36_date_msg"));
        $start = strtotime("2014-01-01");
        $end = strtotime("2025-12-31");
        $date = strtotime($date);
        $today = strtotime(date("Y-m-d"));
        if ($date > $today) {
            $this->form_validation->set_message('validate_C36_date', $this->lang->line("C36_today_date_msg"));
            return FALSE;
        }
        if ($date >= $start && $date <= $end) {
            return TRUE;
        }
        return FALSE;
    }

    function validate_excel_name($value, $id) {
        $this->load->model("admin/Cases_model");
        $this->form_validation->set_message('validate_excel_name', $this->lang->line("excel_name_msg"));
        $user = $this->Cases_model->get_user_info($this->session->userdata("user_id"));
        $name = "M" . $user->code . "B" . $user->branch_code . "C" . $this->input->post("C5") . "I" .
                $user->first_name . $user->last_name;
        $name = str_replace(" ", "", $name);
        $check_name = $this->Cases_model->check_case($name, $id);
        if (empty($check_name)) {
            return TRUE;
        }
        return FALSE;
    }

    function validate_fact($fact) {
        $response['result'] = "success";
        $matched = $fact;
        $this->form_validation->set_data($matched);
//        $this->form_validation->set_rules("amounts", $this->lang->line("amounts"), "xss_clean|trim|required");
        for ($i = 4; $i <= 5; $i++) {
            $extra = '';
            $year = date("Y");
            if ($year > 2025) {
                $year = 2025;
            }
            if ($i == 4) {
                $extra = '|required|numeric|greater_than_equal_to[1900]|less_than_equal_to[' . $year . ']';
            } else {
                $extra = '|required|numeric|greater_than_equal_to[0]|less_than_equal_to[1000000000]';
            }
            $this->form_validation->set_rules("C$i", $this->lang->line("borrower_factsheet_C$i"), "xss_clean|trim$extra");
            if ($i == 4) {
                $this->form_validation->set_rules("E$i", $this->lang->line("borrower_factsheet_E$i"), "xss_clean|trim|required|numeric|greater_than_equal_to[0]|less_than_equal_to[200000000]");
            }
        }

        $investor_groups = array();
        $sum_share = 0;
//        print_r($matched);die;
        $count = 0;
        for ($i = 9; $i <= 18; $i++) {
            $count++;
            if (is_numeric($matched["D$i"])) {
                $sum_share += $matched["D$i"];
            }
            //modified
            $D = "";
            $matched["C$i"] = strval($matched["C$i"]);
            if ($matched["C$i"] != "") {
                $D = "|required";
                array_push($investor_groups, $matched["C$i"]);
                $this->form_validation->set_rules("C$i", $this->lang->line("investor_group") . " $count", "xss_clean|callback_check_chars[investor_group]|callback_check_unique[" . json_encode($matched) . "]|trim|max_length[40]");
            }
            $this->form_validation->set_rules("D$i", $this->lang->line("share") . " $count", "xss_clean|trim|numeric|greater_than_equal_to[0]|less_than_equal_to[100]|callback_check_on_decimal$D");
        }

        $count = 0;
        for ($i = 22; $i <= 31; $i++) {
            $count++;
            $extra = '';
            $c_extra = '';
            $matched["C$i"] = strval($matched["C$i"]);
            $matched["D$i"] = strval($matched["D$i"]);
            if ($matched["C$i"] != '') {
                $extra = '|required|callback_check_chars[remarks]';
                $c_extra = '|callback_unique_directors|callback_check_chars[name]|max_length[40]';
            }
            $this->form_validation->set_rules("C$i", $this->lang->line("bod") . " " . $this->lang->line("name") . " $count", "xss_clean|trim$c_extra");
            $this->form_validation->set_rules("D$i", $this->lang->line("bod") . " " . $this->lang->line("remarks") . " $count", "xss_clean|trim|max_length[30]$extra");
        }
        $count = 0;
        for ($i = 35; $i <= 44; $i++) {
            $count++;
            $extra = '';
            $c_extra = '';
            $matched["C$i"] = strval($matched["C$i"]);
            if ($matched["C$i"] != '') {
                $extra = '|required|callback_check_chars';
                $c_extra = '|callback_unique_personnel|callback_check_chars[name]|max_length[40]';
            }
            $this->form_validation->set_rules("C$i", $this->lang->line("key_personnel") . " " . $this->lang->line("name") . " $count", "xss_clean|trim$c_extra");
            $this->form_validation->set_rules("D$i", $this->lang->line("key_personnel") . " " . $this->lang->line("designation") . " $count", "xss_clean|trim|max_length[30]$extra");
        }

        $response['result'] = "fail";
        $sum_share2 = 100;
        if ($this->input->post()) {
            $sum_share2 = 0;
            for ($i = 9; $i <= 18; $i++) {
                if (is_numeric($matched["D$i"])) {
                    $sum_share2 += $matched["D$i"];
                }
            }
        }
        if ($this->form_validation->run() || $sum_share > 100) {
            $response['result'] = "success";
        }
//        $response['errors'] = validation_errors();
        $response['errors'] = $this->set_errors("fact");
        $response['fact'] = $matched;
        return $response;
    }

    function check_unique($group, $matched) {
        $matched = json_decode($matched, TRUE);
        $found = 0;
        for ($i = 9; $i <= 18; $i++) {
            if ($matched["C$i"] === $group) {
                $this->form_validation->set_message('check_unique', "The {field} " . $this->lang->line('group_must_unique'));
                $found++;
                if ($found == 2) {
                    return FALSE;
                }
            }
        }
        return TRUE;
    }

    function unique_directors($group, $matched) {
        $matched = json_decode($matched, TRUE);
        $found = 0;
        for ($i = 22; $i <= 31; $i++) {
            if ($matched["C$i"] == $group) {
                $this->form_validation->set_message('unique_directors', "The {field} " . $this->lang->line('director_must_unique'));
                $found++;
                if ($found == 2) {
                    return FALSE;
                }
            }
        }
        return TRUE;
    }

    function unique_personnel($group, $matched) {
        $matched = json_decode($matched, TRUE);
        $found = 0;
        for ($i = 35; $i <= 44; $i++) {
            if ($matched["C$i"] == $group) {
                $this->form_validation->set_message('unique_personnel', "The {field} " . $this->lang->line('personnel_must_unique'));
                $found++;
                if ($found == 2) {
                    return FALSE;
                }
            }
        }
        return TRUE;
    }

    function check_chars($string, $label = NULL) {
        $message = "";
        if ($label) {
            $message.=$this->lang->line($label) . " ";
        }
        $this->form_validation->set_message('check_chars', "The {field} " . $this->lang->line('no_special_chars_validation'));
        $string = str_replace(" ", '', $string); // remove spaces
        $string = preg_replace('/[\x{0600}-\x{06FF}A-Za-z]/u', '', $string); // remove arabic chars
        if ($string == "") {
            return TRUE;
        }
        return FALSE;
    }

    function check_on_decimal($value) {
        if ($value != "") {
            $decimal_arr = explode('.', $value);
            if (count($decimal_arr) > 1) {
                //no decimal allowed
                $this->form_validation->set_message('check_on_decimal', "The {field} " . $this->lang->line('no_decimals_allowed'));
                return FALSE;
            }
            if (!(is_numeric($value) && $value >= 0 && $value <= 100)) {
                $this->form_validation->set_message('check_on_decimal', "The {field} " . $this->lang->line('between_0_100'));
                return FALSE;
            }
        }
        return TRUE;
    }

    public function validate_fin($fin, $info) {
        $response['result'] = "success";
        $response['errors'] = "";
        $matched = $fin;
        $matched['total_sales'] = "total_sales";
        $matched['assets'] = "assets";
        $this->form_validation->set_data($matched);
        $turnover = $info['C12'];

        $this_year_required = '';
        if ($turnover > ANNUAL_TOVR) {
            //modified
//            $this_year_required = '|required';
            $this->form_validation->set_rules("E3", $this->lang->line("borrower_financials"), "callback_validate_financials[" . json_encode($matched) . "]");
        }
        if ($matched['E3'] == 'yes') {
            $this->form_validation->set_rules("G8", $this->lang->line("borrower_financial_G8"), "xss_clean|trim|required");
        }
        for ($i = 10; $i <= 167; $i++) {
            if ($i == 11 || $i == 12 ||
                    ($i >= 15 && $i <= 17) ||
                    $i == 20 || $i == 21 ||
                    ($i >= 23 && $i <= 25) ||
                    ($i >= 29 && $i <= 31) || $i == 37 ||
                    ($i >= 40 && $i <= 42) ||
                    ($i >= 44 && $i <= 48) ||
                    ($i >= 52 && $i <= 54) || $i == 57 ||
                    ($i >= 59 && $i <= 61) || $i == 64 || $i == 65 ||
                    ($i >= 68 && $i <= 70) ||
                    ($i >= 72 && $i <= 77) || $i == 91 || $i == 95 || $i == 96 ||
                    ($i >= 101 && $i <= 105) || $i == 116 || $i == 118 ||
                    ($i >= 120 && $i <= 125) || $i == 128 || $i == 131 ||
                    ($i >= 141 && $i <= 144) ||
                    ($i >= 150 && $i <= 152) || $i == 157 || $i == 164 || $i == 165) {
                continue;
            }
            //modified
            $req = "";
            if ($i == 13) {
                $req = "|required";
            }
            if ($i == 14) {
                $req .= "|max_length[25]";
            }
            $this->form_validation->set_rules("D$i", $this->lang->line("borrower_financial_$i") . " " . $this->lang->line("hist1"), "xss_clean|trim$req");
            $this->form_validation->set_rules("C$i", $this->lang->line("borrower_financial_$i") . " " . $this->lang->line("hist2"), "xss_clean|trim$req");
            //modified
            $this->form_validation->set_rules("E$i", $this->lang->line("borrower_financial_$i") . " " . $this->lang->line("base_year"), "xss_clean|trim$req");
            if ($matched['E4'] == 'yes') {
                $this->form_validation->set_rules("F$i", $this->lang->line("borrower_financial_$i") . " " . $this->lang->line("proj1"), "xss_clean|trim");
                $this->form_validation->set_rules("G$i", $this->lang->line("borrower_financial_$i") . " " . $this->lang->line("proj2"), "xss_clean|trim");
            }
        }

        if ($matched['E3'] == 'yes') {
            $this->form_validation->set_rules("D10", "Type Of Financials" . " " . $this->lang->line("hist1"), "xss_clean|trim|required");
            $this->form_validation->set_rules("C10", "Type Of Financials" . " " . $this->lang->line("hist2"), "xss_clean|trim|required");
            $this->form_validation->set_rules("E10", "Type Of Financials" . " " . $this->lang->line("base_year"), "xss_clean|trim|required");

            $this->form_validation->set_rules("D14", $this->lang->line("borrower_financial_14") . " " . $this->lang->line("hist1"), "xss_clean|trim|callback_not_zero[borrower_financial_14]");
            $this->form_validation->set_rules("C14", $this->lang->line("borrower_financial_14") . " " . $this->lang->line("hist2"), "xss_clean|trim|callback_not_zero[borrower_financial_14]");
            $this->form_validation->set_rules("E14", $this->lang->line("borrower_financial_14") . " " . $this->lang->line("base_year"), "xss_clean|trim|callback_not_zero[borrower_financial_14]");
        }
        if ($matched['E4'] == 'yes') {
            $this->form_validation->set_rules("F10", "Type Of Financials" . " " . $this->lang->line("proj1"), "xss_clean|trim|required");
            $this->form_validation->set_rules("G10", "Type Of Financials" . " " . $this->lang->line("proj2"), "xss_clean|trim|required");

            $this->form_validation->set_rules("F14", $this->lang->line("borrower_financial_14") . " " . $this->lang->line("proj1"), "xss_clean|trim|callback_not_zero[borrower_financial_14]");
            $this->form_validation->set_rules("G14", $this->lang->line("borrower_financial_14") . " " . $this->lang->line("proj2"), "xss_clean|trim|callback_not_zero[borrower_financial_14]");
        }

        $param['info'] = $info;
        $param['matched'] = $matched;
        $param = json_encode($param);
        $this->form_validation->set_rules("total_sales", "total_sales", "xss_clean|trim|required|callback_validate_total_sales[$param]");
        $this->form_validation->set_rules("assets", "assets", "xss_clean|trim|required|callback_validate_assets[" . json_encode($matched) . "]");
        $response['result'] = "fail";
        if ($this->form_validation->run() || ($matched['E3'] == 'no' && $turnover < ANNUAL_TOVR)) {
            if ($matched['E3'] == 'yes') {
                $E224 = $this->E224($matched);
                $matched['E224_Total_Net_Sales'] = $E224;
                if (abs($turnover - $matched['E224_Total_Net_Sales']) > 1000) {
                    $response['errors'].="<p>" . $this->lang->line('financial_borrower_error') . "</p>";
                } else {
                    $response['result'] = "success";
                }
            } else {
                $response['result'] = "success";
            }
        }
//        $response['errors'] .= validation_errors();
        $response['errors'] .=$this->set_errors("fin");
        $response['fin'] = $matched;
        return $response;
    }

    function validate_financials($hold, $matched) {
        $matched = json_decode($matched, TRUE);
        $this->form_validation->set_message("validate_financials", $this->lang->line("financials_required"));
        if ($matched['E3'] == 'no') {
            return FALSE;
        }
        return TRUE;
    }

    function validate_total_sales($hold, $param) {
        $param = json_decode($param, TRUE);
        $info = $param['info'];
        $matched = $param['matched'];
        $hold = sanitize($hold);
        $borrower_information = $info;
        $this->form_validation->set_message('validate_total_sales', $this->lang->line("bi_incomplete"));
        if (!empty($borrower_information)) {
            $this->form_validation->set_message('validate_total_sales', $this->lang->line("total_sales_msg"));
            $annual_turnover = 0;
            $G8 = $matched["G8"];
            $months = $matched["E13"];
            if ($borrower_information['C12']) {
                $annual_turnover = $borrower_information['C12'];
                $annual_turnover = ($annual_turnover / 12) * $months;
                $sum = $this->get_numeric($matched["E18"]) + $this->get_numeric($matched["E19"]);
                if ($G8 == "Thousands") {
                    $sum = $sum * 1000;
                } elseif ($G8 == "Millions") {
                    $sum = $sum * 1000000;
                }
                $diff = abs($sum - $annual_turnover);
//                $diff = abs($sum - 0);
                if ($diff <= 1000) {
                    return TRUE;
                }
            }
            return FALSE;
        }
        return FALSE;
    }

    function validate_assets($hold, $matched) {
        $matched = json_decode($matched, TRUE);
        $hold = sanitize($hold);
//        liabilities C119+C117+C116+C101+C95
//        C119 +
//        C117+
//        C106->115+
//        C97->C100+
//        SUM(C78:C90)+C91+C94
        $this->form_validation->set_message('validate_assets', $this->lang->line("assets_msg"));
        //E23
//        $E23 = $this->get_numeric($matched["E18"]) + $this->get_numeric($matched["E19"]) + $this->get_numeric($matched["E22"]);
//        if ($E23 === 0) {
//            $this->form_validation->set_message('validate_assets', $this->lang->line("borrower_financial_23") . $this->lang->line("non_zero"));
//            return FALSE;
//        }
        //E29
        $E29 = $this->get_numeric($matched["E26"]) + $this->get_numeric($matched["E27"]) + $this->get_numeric($matched["E28"]);
        if ($E29 === 0) {
            $this->form_validation->set_message('validate_assets', $this->lang->line("borrower_financial_29") . $this->lang->line("non_zero"));
            return FALSE;
        }
        //E121
        //E170
        //liabilities
        $liabilities[0] = $this->get_numeric($matched["C119"]) + $this->get_numeric($matched["C117"]) + $this->get_numeric($matched["C92"]) + $this->get_numeric($matched["C93"]) + $this->get_numeric($matched["C94"]);
        $liabilities[1] = $this->get_numeric($matched["D119"]) + $this->get_numeric($matched["D117"]) + $this->get_numeric($matched["D92"]) + $this->get_numeric($matched["D93"]) + $this->get_numeric($matched["D94"]);
        $liabilities[2] = $this->get_numeric($matched["E119"]) + $this->get_numeric($matched["E117"]) + $this->get_numeric($matched["E92"]) + $this->get_numeric($matched["E93"]) + $this->get_numeric($matched["E94"]);
        for ($i = 106; $i <= 115; $i++) {
            $liabilities[0] +=$this->get_numeric($matched["C$i"]);
            $liabilities[1] +=$this->get_numeric($matched["D$i"]);
            $liabilities[2] +=$this->get_numeric($matched["E$i"]);
        }
        for ($i = 97; $i <= 100; $i++) {
            $liabilities[0] +=$this->get_numeric($matched["C$i"]);
            $liabilities[1] +=$this->get_numeric($matched["D$i"]);
            $liabilities[2] +=$this->get_numeric($matched["E$i"]);
        }
        for ($i = 78; $i <= 90; $i++) {
            $liabilities[0] +=$this->get_numeric($matched["C$i"]);
            $liabilities[1] +=$this->get_numeric($matched["D$i"]);
            $liabilities[2] +=$this->get_numeric($matched["E$i"]);
        }

        if ($liabilities[2] === 0) {
            $this->form_validation->set_message('validate_assets', $this->lang->line("total_liabilities") . $this->lang->line("non_zero"));
            return FALSE;
        }

        //assets
//        Assets C168+C164+C141+C150
///        C166-167 +
///        C153+C154+C155+C157+C156 +
//        C126+C127+C128+C138+C131+C137+C139+C140 +
///        (C145->148)-C149 
        $assets[0] = ($this->get_numeric($matched["C166"]) - $this->get_numeric($matched["C167"])) +
                (($this->get_numeric($matched["C145"]) + $this->get_numeric($matched["C146"]) + $this->get_numeric($matched["C147"]) + $this->get_numeric($matched["C148"])) - $this->get_numeric($matched["C149"])) +
                //modified
                ($this->get_numeric($matched["C129"]) - $this->get_numeric($matched["C130"]));
        $assets[1] = ($this->get_numeric($matched["D166"]) - $this->get_numeric($matched["D167"])) +
                (($this->get_numeric($matched["D145"]) + $this->get_numeric($matched["D146"]) + $this->get_numeric($matched["D147"]) + $this->get_numeric($matched["D148"])) - $this->get_numeric($matched["D149"])) +
                //modified
                ($this->get_numeric($matched["D129"]) - $this->get_numeric($matched["D130"]));
        $assets[2] = ($this->get_numeric($matched["E166"]) - $this->get_numeric($matched["E167"])) +
                (($this->get_numeric($matched["E145"]) + $this->get_numeric($matched["E146"]) + $this->get_numeric($matched["E147"]) + $this->get_numeric($this->input->post("E148"))) - $this->get_numeric($matched["E149"])) +
                //modified
                ($this->get_numeric($matched["E129"]) - $this->get_numeric($matched["E130"]));
        for ($i = 153; $i <= 156; $i++) {
            $assets[0] +=$this->get_numeric($matched["C$i"]);
            $assets[1] +=$this->get_numeric($matched["D$i"]);
            $assets[2] +=$this->get_numeric($matched["E$i"]);
        }
        for ($i = 158; $i <= 163; $i++) {
            $assets[0] +=$this->get_numeric($matched["C$i"]);
            $assets[1] +=$this->get_numeric($matched["D$i"]);
            $assets[2] +=$this->get_numeric($matched["E$i"]);
        }
        for ($i = 126; $i <= 127; $i++) {
            $assets[0] +=$this->get_numeric($matched["C$i"]);
            $assets[1] +=$this->get_numeric($matched["D$i"]);
            $assets[2] +=$this->get_numeric($matched["E$i"]);
        }
        for ($i = 137; $i <= 140; $i++) {
            $assets[0] +=$this->get_numeric($matched["C$i"]);
            $assets[1] +=$this->get_numeric($matched["D$i"]);
            $assets[2] +=$this->get_numeric($matched["E$i"]);
        }
        for ($i = 132; $i <= 136; $i++) {
            $assets[0] +=$this->get_numeric($matched["C$i"]);
            $assets[1] +=$this->get_numeric($matched["D$i"]);
            $assets[2] +=$this->get_numeric($matched["E$i"]);
        }

        //modified2
        $sub[0] = $assets[0] - $liabilities[0];
        $sub[1] = $assets[1] - $liabilities[1];
        $sub[2] = $assets[2] - $liabilities[2];
//        if($assets[0]==$liabilities[0] && $assets[1]==$liabilities[1] && $assets[2]==$liabilities[2]){
//            return TRUE;
//        }
        if ($sub[0] == 0 && $sub[1] == 0 && $sub[2] == 0) {
            return TRUE;
        }

        return FALSE;
    }

    function validate_assess($assess, $info) {
        $response['result'] = "success";
        $matched = $assess;
        $this->form_validation->set_data($matched);
        $i = 23;
        $borrower_information = $info;
        $turnover = $borrower_information["C12"];
        $segment = $borrower_information["C10"];
        $condition = "return FALSE;";
        //modified
        if (($turnover < ANNUAL_TOVR && $segment == "Manufacturing") || ($turnover < ANNUAL_TOVR && $segment == "Services") || ($turnover < ANNUAL_TOVR && $segment == "Trading/Commercial")) {
//            4,5,11,13,14,15,16,17,18,19,20,22& 27
//            4,5,12,13,14,15,16,17,18,19,21& 26
            $condition = 'return $n==4||$n==5||$n==12||$n==13||$n==14||$n==15||$n==16||$n==17||$n==18||$n==19||$n==21||$n==26 ;';
        } elseif ($turnover > ANNUAL_TOVR && $segment == "Manufacturing") {
//            8,10,15,16,17,18,19,20,21,28,30,31& 32
//            8,14,15,16,17,18,19,20,27,29,30& 31
            $condition = 'return $n==8||$n==14||$n==15||$n==16||$n==17||$n==18||$n==19||$n==20||$n==27||$n==29||$n==30||$n==31 ;';
        } elseif ($turnover > ANNUAL_TOVR && $segment == "Services") {
//              8,10,13,14,17,18,19,20,21,28,30,31& 32
//            8,12,13,16,17,18,19,20,27,29,30& 31
            $condition = 'return $n==8||$n==12||$n==13||$n==16||$n==17||$n==18||$n==19||$n==20||$n==27||$n==29||$n==30||$n==31 ;';
        } elseif ($turnover > ANNUAL_TOVR && $segment == "Trading/Commercial") {
//          8,10,13,14,15,16,19,20,21,28,30,31& 32
//            8,12,13,14,15,18,19,20,27,29,30& 31
            $condition = 'return $n==8||$n==12||$n==13||$n==14||$n==15||$n==18||$n==19||$n==20||$n==27||$n==29||$n==30||$n==31 ;';
        } elseif ($turnover < ANNUAL_TOVR && $segment == "Contractors") {
//          4,5,11,13,14,15,16,17,18,20,22& 27
//            4,5,12,13,14,15,16,17,19,21& 26
            $condition = 'return $n==4||$n==5||$n==12||$n==13||$n==14||$n==15||$n==16||$n==17||$n==19||$n==21||$n==26 ;';
        } elseif ($turnover > ANNUAL_TOVR && $segment == "Contractors") {
//          8,10,13,14,15,16,17,18,21,28,30,31& 32
//            8,12,13,14,15,16,17,20,27,29,30& 31
            $condition = 'return $n==8||$n==12||$n==13||$n==14||$n==15||$n==16||$n==17||$n==20||$n==27||$n==29||$n==30||$n==31 ;';
        }
        $n = 1;
        for ($i = 4; $i <= 229; $i++) {
            $required = '|required';
            if ($this->lang->line("borrower_assessment_F$i") != '') {
                /*if ($i == 4 || $i == 17 || $i == 120 || $i == 132 || $i == 184 || $i == 214 || $i == 221 || $i == 229) {
                    continue;
                }*/
				if ($i == 4 || $i == 17 || $i == 120 || $i == 132 || $i == 184 || $i == 214) {
					continue;
                }
                if (($turnover < ANNUAL_TOVR && $segment == "Manufacturing") || ($turnover < ANNUAL_TOVR && $segment == "Services") || ($turnover < ANNUAL_TOVR && $segment == "Trading/Commercial")) {
                    // 4,5,11,13,14,15,16,17,18,19,20,22& 27
                    // 4,5,12,13,14,15,16,17,18,19,21& 26
                    if ($n == 4 || $n == 5 || $n == 12 || $n == 13 || $n == 14 || $n == 15 || $n == 16 || $n == 17 || $n == 18 || $n == 19 || $n == 21 || $n == 26) {
                        $required = '';
                    }
                } elseif ($turnover > ANNUAL_TOVR && $segment == "Manufacturing") {
                    // 8,10,15,16,17,18,19,20,21,28,30,31& 32
                    // 8,14,15,16,17,18,19,20,27,29,30& 31
                    if ($n == 8 || $n == 14 || $n == 15 || $n == 16 || $n == 17 || $n == 18 || $n == 19 || $n == 20 || $n == 27 || $n == 29 || $n == 30 || $n == 31) {
                        $required = '';
                    }
                } elseif ($turnover > ANNUAL_TOVR && $segment == "Services") {
                    // 8,10,13,14,17,18,19,20,21,28,30,31& 32
                    // 8,12,13,16,17,18,19,20,27,29,30& 31
                    if ($n == 8 || $n == 12 || $n == 13 || $n == 16 || $n == 17 || $n == 18 || $n == 19 || $n == 20 || $n == 27 || $n == 29 || $n == 30 || $n == 31) {
                        $required = '';
                    }
                } elseif ($turnover > ANNUAL_TOVR && $segment == "Trading/Commercial") {
                    // 8,10,13,14,15,16,19,20,21,28,30,31& 32
                    // 8,12,13,14,15,18,19,20,27,29,30& 31
                    if ($n == 8 || $n == 12 || $n == 13 || $n == 14 || $n == 15 || $n == 18 || $n == 19 || $n == 20 || $n == 27 || $n == 29 || $n == 30 || $n == 31) {
                        $required = '';
                    }
                } elseif ($turnover < ANNUAL_TOVR && $segment == "Contractors") {
                    // 4,5,11,13,14,15,16,17,18,20,22& 27
                    // 4,5,12,13,14,15,16,17,19,21& 26
                    if ($n == 4 || $n == 5 || $n == 12 || $n == 13 || $n == 14 || $n == 15 || $n == 16 || $n == 17 || $n == 19 || $n == 21 || $n == 26) {
                        $required = '';
                    }
                } elseif ($turnover > ANNUAL_TOVR && $segment == "Contractors") {
                    // 8,10,13,14,15,16,17,18,21,28,30,31& 32
                    // 8,12,13,14,15,16,17,20,27,29,30& 31
                    if ($n == 8 || $n == 12 || $n == 13 || $n == 14 || $n == 15 || $n == 16 || $n == 17 || $n == 20 || $n == 27 || $n == 29 || $n == 30 || $n == 31) {
                        $required = '';
                    }
                } else {
                    $required = '';
                }
                // it was
                // if (eval($condition)) {
                // $required = '';
                // }
                $this->form_validation->set_rules("F$i", $this->lang->line("borrower_assessment_F$i"), "xss_clean$required");
                $n++;
            }
        }
        if (!$this->form_validation->run()) {
            $response['result'] = "fail";
//            $response['errors'] = validation_errors();
            $response['errors'] = $this->set_errors("assess");
        }
        $response['assess'] = $matched;
        return $response;
    }

    public function test() {
//        print_r($this->session->userdata("dump"));
//        $dump=$this->session->userdata("dump");
//        $arr="array(";
//        foreach ($dump as $key=>$val){
//            $arr.="'$val',";
//        }
//        echo $arr;
        $var = '';
        echo isset($var);
    }

    public function insert_info($data, $invalid = NULL) {
        unset($data['excel_name']);
        $data['turnover_amount'] = 1;
        $data['bank_amount'] = 1;
        for ($i = 25; $i <= 30; $i++) {
            if (!($data["B$i"] != "" && strtotime($data["B$i"]))) {
                $data["B$i"] = "NA";
            }
            if (!($data["C$i"] && $data["C$i"] != "")) {
                $data["C$i"] = "NA";
            }
            if (!($data["D$i"] && $data["D$i"] != "")) {
                $data["D$i"] = "NA";
            }
        }
        if (!($data["D32"] && $data["D32"] != "")) {
            $data["D32"] = "NA";
        }
        $data["C36"] = date("j-M-y", strtotime($data["C36"]));


        //insert case
        $case = array(
            'user_id' => $this->session->userdata("user_id"),
            'date_created' => date("Y-m-d H:i:s"),
            'id' => incremented("cases"),
            'status' => "saved"
        );
        if ($invalid) {
            $case['status'] = "borrower_information";
        }

        $this->Crud_model->insert("cases", $case);
        $case_id = $case["id"];


        $this->load->model("admin/Cases_model");
        $user_info = $this->Cases_model->get_user_info($this->session->userdata("user_id"));
        $data["C16"] = $user_info->bank_name;
        $data["C17"] = $user_info->branch_code;
        $data["C18"] = $user_info->first_name . " " . $user_info->last_name;

        $data["case_id"] = $case_id;
        $data["C20"] = $case['date_created'];
        $data["id"] = incremented("borrower_information");
        $this->Crud_model->insert("borrower_information", $data);

        $excel_name = $this->Cases_model->construct_excel_file_name($case_id);
        $this->db->update('cases', array('name' => $excel_name), array('id' => $case_id));
        return $case_id;
    }

    public function insert_fact($fact) {
        $i = 0;
        $data = array();
        $row_data = array();
        $row_data['case_id'] = $fact["case_id"];
        $case_id = $fact["case_id"];
        $incremented = incremented("borrower_factsheet");
        for ($i = 4; $i <= 44; $i++) {
//                6,7,8 , 20,21,  33,34
//                d4,5
            if ($i == 6 || $i == 7 || $i == 8 || $i == 20 || $i == 21 || $i == 33 || $i == 34) {
                continue;
            }
            $row_data["id"] = $incremented;
            $row_data['row'] = $i;
            $row_data['C'] = isset($fact['C' . $i]) ? $fact['C' . $i] : '';
            $row_data['D'] = isset($fact['D' . $i]) ? $fact['D' . $i] : '';
            $row_data['E'] = isset($fact['E' . $i]) ? $fact['E' . $i] : '';
            array_push($data, $row_data);
            $incremented++;
        }

        $this->db->delete('borrower_factsheet', array('case_id' => $case_id));

        $if_exist = $this->db->affected_rows();
        $this->load->model("admin/Cases_two_model");
        //modified
        if (!empty($data)) {
            $this->db->insert_batch('borrower_factsheet', $data);
        }
        $case_name = $this->get_case_name($case_id);
        save_audit("Case Borrower factsheet info. inserted and marked as continue later in case -- " . $case_name);
    }

    function get_case_name($case_id) {
        $case_id = sanitize($case_id);
        $case = $this->db->get_where('cases', array('id' => $case_id))->row();
        if ($case) {
            return $case->name;
        }
        return '';
    }

    public function insert_fin($fin) {
        $i = 0;
        $data = array();
        $row_data = array();
        $case_id = $fin['case_id'];
        $row_data['case_id'] = $case_id;
        $incremented = incremented("borrower_financials");
        $this->load->model("admin/Cases_two_model");
        for ($i = 3; $i <= 167; $i++) {
            if ($fin['E3'] == 'no' && $i > 4) {
                break;
            }
            $row_data['id'] = $incremented;
            $row_data['row'] = $i;
            $row_data['C'] = isset($fin['C' . $i]) ? $fin['C' . $i] : '';
            $row_data['D'] = isset($fin['D' . $i]) ? $fin['D' . $i] : '';
            $row_data['E'] = isset($fin['E' . $i]) ? $fin['E' . $i] : '';
            if ($fin['E4'] == 'no') {
                //modified
                $row_data['F'] = '';
                $row_data['G'] = '';
                if ($i == 10) {
                    $row_data['F'] = 'Projected';
                    $row_data['G'] = 'Projected';
                }
                if ($i == 12) {
                    $borrower_info_date = $this->Cases_two_model->get_borrower_info_date($case_id);
                    $row_data['C'] = date('d-m-Y', strtotime("-2 year", strtotime($borrower_info_date)));
                    $row_data['D'] = date('d-m-Y', strtotime("-1 year", strtotime($borrower_info_date)));
                    $row_data['E'] = date('d-m-Y', strtotime($borrower_info_date));
                    $row_data['F'] = date('d-m-Y', strtotime("+1 year", strtotime($borrower_info_date)));
                    $row_data['G'] = date('d-m-Y', strtotime("+2 year", strtotime($borrower_info_date)));
                }
                if ($i == 13) {
                    $row_data['F'] = 12;
                    $row_data['G'] = 12;
                }
            } else {
                $row_data['F'] = isset($fin['F' . $i]) ? $fin['F' . $i] : '';
                $row_data['G'] = isset($fin['G' . $i]) ? $fin['G' . $i] : '';
            }
            if ($i == 8) {
                $row_data['F'] = '';
                $row_data['G'] = isset($fin['G' . $i]) ? $fin['G' . $i] : '';
            }
//                array_push($data, $row_data);
            if ($row_data['C'] != '' || $row_data['D'] != '' || $row_data['E'] != '' || $row_data['F'] != '' || $row_data['G'] != '') {
                array_push($data, $row_data);
            }
            $incremented++;
        }

        $this->db->delete('borrower_financials', array('case_id' => $case_id));

        $this->db->insert_batch('borrower_financials', $data);

        $this->insert_calc($case_id);

        $case_name = $this->get_case_name($case_id);

        save_audit("Case Borrower financials info. inserted and marked as continue later in case -- " . $case_name);
    }

    public function insert_calc($case_id) {
        $data = array(
            "case_id" => $case_id,
            "rown" => 219,
            "C" => 0,
            "D" => 0,
            "E" => 0,
            "F" => 0,
            "G" => 0
        );
        $data = increment("calculations", $data);
        $this->db->insert("calculations", $data);
    }

    public function insert_assess($assess) {
        $i = 0;
        $data = array();
        $row_data = array();
        $row_data['case_id'] = $assess['case_id'];
        $case_id = $assess['case_id'];
        $incremented = incremented("borrower_assessment");
        for ($i = 4; $i <= 229; $i++) {
            if ($this->lang->line("borrower_assessment_F$i") != '') {
                if ($i == 4 || $i == 17 || $i == 120 || $i == 132 || $i == 184 || $i == 214 || $i == 221 || $i == 229) {
                    continue;
                }
                $row_data["id"] = $incremented;
                $row_data['row'] = $i;
                $row_data['F'] = isset($assess['F' . $i]) ? $assess['F' . $i] : '';

                array_push($data, $row_data);
                $incremented++;
            }
        }

        $this->db->delete('borrower_assessment', array('case_id' => $case_id));

        $if_exist = $this->db->affected_rows();

        $this->db->insert_batch('borrower_assessment', $data);

        $case_name = $this->get_case_name($case_id);
        $this->load->model("admin/Cases_two_model");
        if ($if_exist) {
            save_audit("Case Borrower assessment info. upated and marked as continue later in case -- " . $case_name);
        } else {
            save_audit("Case Borrower assessment info. inserted and marked as continue later  in case -- " . $case_name);
        }
    }

    function get_numeric($value) {
        if (is_numeric($value)) {
            return $value;
        }
        return 0;
    }

    public function delete_inserted_cases($file_name) {
//        $file_name = 'remove.xlsx';
        ini_set('memory_limit', '2048M');
        set_time_limit(1800);
        
        $objPHPexcel = \PhpOffice\PhpSpreadsheet\IOFactory::load("./bulk/$file_name");
        $objWorksheet = $objPHPexcel->setActiveSheetIndex(0);
        $objWorksheet->removeRow(2, 1);
        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objPHPexcel, 'Xlsx');
        if (!is_dir('root/bulk_failed')) {
            mkdir('root/bulk_failed', 0777, TRUE);
            //add index file
            copy('root/index.php', "root/bulk_failed/index.php");
        }
        $objWriter->save("./root/bulk_failed/$file_name");
    }

    function check_validation($data) {
        $this->form_validation->reset_validation();
        if ($data['result'] != "success") {
           return $data['errors'];
        }
        return '';
    }

    function save_calc() {
        $calcs = json_decode($this->input->post("calc"), true);
        if (!empty($calcs)) {
            foreach ($calcs as $case_id => $calc) {
                $this->db->where("case_id", $case_id);
                $this->db->delete("calculations");
                $incremented = incremented("calculations");
                for ($i = 12; $i <= 224; $i++) {
                    if (isset($calc["C$i"]) || isset($calc["D$i"]) || isset($calc["E$i"]) || isset($calc["F$i"]) || isset($calc["G$i"])) {
                        $one = array(
                            "id" => $incremented,
                            "case_id" => $case_id,
                            "rown" => $i,
                            "C" => isset($calc["C$i"]) ? $calc["C$i"] : NULL,
                            "D" => isset($calc["D$i"]) ? $calc["D$i"] : NULL,
                            "E" => isset($calc["E$i"]) ? $calc["E$i"] : NULL,
                            "F" => isset($calc["F$i"]) ? $calc["F$i"] : NULL,
                            "G" => isset($calc["G$i"]) ? $calc["G$i"] : NULL
                        );
                        $this->db->insert("calculations", $one);
                        $incremented++;
                    }
                }
            }
            echo "s: " . json_encode($calcs);
        } else {
            echo "f: " . json_encode($calcs);
        }
    }

    function E224($matched) {
        $E20 = $matched['E18'] + $matched['E19'];
        $E220 = 1;
        if ($matched['E13'] == 3) {
            $E220 = 4;
        } else if ($matched['E13'] == 6) {
            $E220 = 2;
        } else if ($matched['E13'] == 9) {
            $E220 = 1 / 0.75;
        } else if ($matched['E13'] == 12) {
            $E220 = 1;
        }
        $E221 = 1;
        if ($matched['G8'] == 'Actuals') {
            $E221 = 1;
        } else if ($matched['G8'] == 'Thousands') {
            $E221 = 1000;
        } else if ($matched['G8'] == 'Millions') {
            $E221 = 1000000;
        }
        $E222 = $E220 * $E221;
        $E224 = $E222 * $E20;
        return $E224;
    }

    function write_labels() {
        $headers = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ', 'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CQ', 'CR', 'CS', 'CT', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ', 'DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DK', 'DL', 'DM', 'DN', 'DO', 'DP', 'DQ', 'DR', 'DS', 'DT', 'DU', 'DV', 'DW', 'DX', 'DY', 'DZ', 'EA', 'EB', 'EC', 'ED', 'EE', 'EF', 'EG', 'EH', 'EI', 'EJ', 'EK', 'EL', 'EM', 'EN', 'EO', 'EP', 'EQ', 'ER', 'ES', 'ET', 'EU', 'EV', 'EW', 'EX', 'EY', 'EZ', 'FA', 'FB', 'FC', 'FD', 'FE', 'FF', 'FG', 'FH', 'FI', 'FJ', 'FK', 'FL', 'FM', 'FN', 'FO', 'FP', 'FQ', 'FR', 'FS', 'FT', 'FU', 'FV', 'FW', 'FX', 'FY', 'FZ', 'GA', 'GB', 'GC', 'GD', 'GE', 'GF', 'GG', 'GH', 'GI', 'GJ', 'GK', 'GL', 'GM', 'GN', 'GO', 'GP', 'GQ', 'GR', 'GS', 'GT', 'GU', 'GV', 'GW', 'GX', 'GY', 'GZ', 'HA', 'HB', 'HC', 'HD', 'HE', 'HF', 'HG', 'HH', 'HI', 'HJ', 'HK', 'HL', 'HM', 'HN', 'HO', 'HP', 'HQ', 'HR', 'HS', 'HT', 'HU', 'HV', 'HW', 'HX', 'HY', 'HZ', 'IA', 'IB', 'IC', 'ID', 'IE', 'IF', 'IG', 'IH', 'II', 'IJ', 'IK', 'IL', 'IM', 'IN', 'IO', 'IP', 'IQ', 'IR', 'IS', 'IT', 'IU', 'IV', 'IW', 'IX', 'IY', 'IZ', 'JA', 'JB', 'JC', 'JD', 'JE', 'JF', 'JG', 'JH', 'JI', 'JJ', 'JK', 'JL', 'JM', 'JN', 'JO', 'JP', 'JQ', 'JR', 'JS', 'JT', 'JU', 'JV', 'JW', 'JX', 'JY', 'JZ', 'KA', 'KB', 'KC', 'KD', 'KE', 'KF', 'KG', 'KH', 'KI', 'KJ', 'KK', 'KL', 'KM', 'KN', 'KO', 'KP', 'KQ', 'KR', 'KS', 'KT', 'KU', 'KV', 'KW', 'KX', 'KY', 'KZ', 'LA', 'LB', 'LC', 'LD', 'LE', 'LF', 'LG', 'LH', 'LI', 'LJ', 'LK', 'LL', 'LM', 'LN', 'LO', 'LP', 'LQ', 'LR', 'LS', 'LT', 'LU', 'LV', 'LW', 'LX', 'LY', 'LZ', 'MA', 'MB', 'MC', 'MD', 'ME', 'MF', 'MG', 'MH', 'MI', 'MJ', 'MK', 'ML', 'MM', 'MN', 'MO', 'MP', 'MQ', 'MR', 'MS', 'MT', 'MU', 'MV', 'MW', 'MX', 'MY', 'MZ', 'NA', 'NB', 'NC', 'ND', 'NE', 'NF', 'NG', 'NH', 'NI', 'NJ', 'NK', 'NL', 'NM', 'NN', 'NO', 'NP', 'NQ', 'NR', 'NS', 'NT', 'NU', 'NV', 'NW', 'NX', 'NY', 'NZ', 'OA', 'OB', 'OC', 'OD', 'OE', 'OF', 'OG', 'OH', 'OI', 'OJ', 'OK', 'OL', 'OM', 'ON', 'OO', 'OP', 'OQ', 'OR', 'OS', 'OT', 'OU', 'OV', 'OW', 'OX', 'OY', 'OZ', 'PA', 'PB', 'PC', 'PD', 'PE', 'PF', 'PG', 'PH', 'PI', 'PJ', 'PK', 'PL', 'PM', 'PN', 'PO', 'PP', 'PQ', 'PR', 'PS', 'PT', 'PU', 'PV', 'PW', 'PX', 'PY', 'PZ', 'QA', 'QB', 'QC', 'QD', 'QE', 'QF', 'QG', 'QH', 'QI', 'QJ', 'QK', 'QL', 'QM', 'QN', 'QO', 'QP', 'QQ', 'QR', 'QS', 'QT', 'QU', 'QV', 'QW', 'QX', 'QY', 'QZ', 'RA', 'RB', 'RC', 'RD', 'RE', 'RF', 'RG', 'RH', 'RI', 'RJ', 'RK', 'RL', 'RM', 'RN', 'RO', 'RP', 'RQ', 'RR', 'RS', 'RT', 'RU', 'RV', 'RW', 'RX', 'RY', 'RZ', 'SA', 'SB', 'SC', 'SD', 'SE', 'SF', 'SG', 'SH', 'SI', 'SJ', 'SK', 'SL', 'SM', 'SN', 'SO', 'SP', 'SQ', 'SR', 'SS', 'ST', 'SU', 'SV', 'SW', 'SX', 'SY', 'SZ', 'TA', 'TB', 'TC', 'TD', 'TE', 'TF', 'TG', 'TH', 'TI', 'TJ', 'TK', 'TL', 'TM', 'TN', 'TO', 'TP', 'TQ', 'TR', 'TS', 'TT', 'TU', 'TV', 'TW', 'TX', 'TY', 'TZ', 'UA', 'UB', 'UC', 'UD', 'UE', 'UF', 'UG', 'UH', 'UI', 'UJ', 'UK', 'UL', 'UM', 'UN', 'UO', 'UP', 'UQ', 'UR', 'US', 'UT', 'UU', 'UV', 'UW', 'UX', 'UY', 'UZ', 'VA', 'VB', 'VC', 'VD', 'VE', 'VF', 'VG', 'VH', 'VI', 'VJ', 'VK', 'VL', 'VM', 'VN', 'VO', 'VP', 'VQ', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ', 'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CQ', 'CR', 'CS', 'CT', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ', 'DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DK', 'DL', 'DM', 'DN', 'DO', 'DP', 'DQ', 'DR', 'DS', 'DT', 'DU', 'DV', 'DW', 'DX', 'DY', 'DZ', 'EA', 'EB', 'EC', 'ED', 'EE', 'EF', 'EG', 'EH', 'EI', 'EJ', 'EK', 'EL', 'EM', 'EN', 'EO', 'EP', 'EQ', 'ER', 'ES', 'ET', 'EU', 'EV', 'EW', 'EX', 'EY', 'EZ', 'FA', 'FB', 'FC', 'FD', 'FE', 'FF', 'FG', 'FH', 'FI', 'FJ', 'FK', 'FL', 'FM', 'FN', 'FO', 'FP', 'FQ', 'FR', 'FS', 'FT', 'FU', 'FV', 'FW', 'FX', 'FY', 'FZ', 'GA', 'GB', 'GC', 'GD', 'GE', 'GF', 'GG', 'GH', 'GI', 'GJ', 'GK', 'GL', 'GM', 'GN', 'GO', 'GP', 'GQ', 'GR', 'GS', 'GT', 'GU', 'GV', 'GW', 'GX', 'GY', 'GZ', 'HA', 'HB', 'HC', 'HD', 'HE', 'HF', 'HG', 'HH', 'HI', 'HJ', 'HK', 'HL', 'HM', 'HN', 'HO', 'HP', 'HQ', 'HR', 'HS', 'HT', 'HU', 'HV', 'HW', 'HX', 'HY', 'HZ', 'IA', 'IB', 'IC', 'ID', 'IE', 'IF', 'IG', 'IH', 'II', 'IJ', 'IK', 'IL', 'IM', 'IN', 'IO', 'IP', 'IQ', 'IR', 'IS', 'IT', 'IU', 'IV', 'IW', 'IX', 'IY', 'IZ', 'JA', 'JB', 'JC', 'JD', 'JE', 'JF', 'JG', 'JH', 'JI', 'JJ', 'JK', 'JL', 'JM', 'JN', 'JO', 'JP', 'JQ', 'JR', 'JS', 'JT', 'JU', 'JV', 'JW', 'JX', 'JY', 'JZ', 'KA', 'KB', 'KC', 'KD', 'KE', 'KF', 'KG', 'KH', 'KI', 'KJ', 'KK', 'KL', 'KM', 'KN', 'KO', 'KP', 'KQ', 'KR', 'KS', 'KT', 'KU', 'KV', 'KW', 'KX', 'KY', 'KZ', 'LA', 'LB', 'LC', 'LD', 'LE', 'LF', 'LG', 'LH', 'LI', 'LJ', 'LK', 'LL', 'LM', 'LN', 'LO', 'LP', 'LQ', 'LR', 'LS', 'LT', 'LU', 'LV', 'LW', 'LX', 'LY', 'LZ', 'MA', 'MB', 'MC', 'MD', 'ME', 'MF', 'MG', 'MH', 'MI', 'MJ', 'MK', 'ML', 'MM', 'MN', 'MO', 'MP', 'MQ', 'MR', 'MS', 'MT', 'MU', 'MV', 'MW', 'MX', 'MY', 'MZ', 'NA', 'NB', 'NC', 'ND', 'NE', 'NF', 'NG', 'NH', 'NI', 'NJ', 'NK', 'NL', 'NM', 'NN', 'NO', 'NP', 'NQ', 'NR', 'NS', 'NT', 'NU', 'NV', 'NW', 'NX', 'NY', 'NZ', 'OA', 'OB', 'OC', 'OD', 'OE', 'OF', 'OG', 'OH', 'OI', 'OJ', 'OK', 'OL', 'OM', 'ON', 'OO', 'OP', 'OQ', 'OR', 'OS', 'OT', 'OU', 'OV', 'OW', 'OX', 'OY', 'OZ', 'PA', 'PB', 'PC', 'PD', 'PE', 'PF', 'PG', 'PH', 'PI', 'PJ', 'PK', 'PL', 'PM', 'PN', 'PO', 'PP', 'PQ', 'PR', 'PS', 'PT', 'PU', 'PV', 'PW', 'PX', 'PY', 'PZ', 'QA', 'QB', 'QC', 'QD', 'QE', 'QF', 'QG', 'QH', 'QI', 'QJ', 'QK', 'QL', 'QM', 'QN', 'QO', 'QP', 'QQ', 'QR', 'QS', 'QT', 'QU', 'QV', 'QW', 'QX', 'QY', 'QZ', 'RA', 'RB', 'RC', 'RD', 'RE', 'RF', 'RG', 'RH', 'RI', 'RJ', 'RK', 'RL', 'RM', 'RN', 'RO', 'RP', 'RQ', 'RR', 'RS', 'RT', 'RU', 'RV', 'RW', 'RX', 'RY', 'RZ', 'SA', 'SB', 'SC', 'SD', 'SE', 'SF', 'SG', 'SH', 'SI', 'SJ', 'SK', 'SL', 'SM', 'SN', 'SO', 'SP', 'SQ', 'SR', 'SS', 'ST', 'SU', 'SV', 'SW', 'SX', 'SY', 'SZ', 'TA', 'TB', 'TC', 'TD', 'TE', 'TF', 'TG', 'TH', 'TI', 'TJ', 'TK', 'TL', 'TM', 'TN', 'TO', 'TP', 'TQ', 'TR', 'TS', 'TT', 'TU', 'TV', 'TW', 'TX', 'TY', 'TZ', 'UA', 'UB', 'UC', 'UD', 'UE', 'UF', 'UG', 'UH', 'UI', 'UJ', 'UK', 'UL', 'UM', 'UN', 'UO', 'UP', 'UQ', 'UR', 'US', 'UT', 'UU', 'UV', 'UW', 'UX', 'UY', 'UZ', 'VA', 'VB', 'VC', 'VD', 'VE', 'VF', 'VG', 'VH', 'VI', 'VJ', 'VK', 'VL', 'VM', 'VN', 'VO', 'VP', 'VQ', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ', 'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CQ', 'CR', 'CS', 'CT', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ', 'DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DK', 'DL', 'DM', 'DN', 'DO', 'DP', 'DQ', 'DR', 'DS', 'DT', 'DU', 'DV', 'DW', 'DX', 'DY', 'DZ', 'EA', 'EB', 'EC', 'ED', 'EE', 'EF', 'EG', 'EH', 'EI', 'EJ', 'EK', 'EL', 'EM', 'EN', 'EO', 'EP', 'EQ', 'ER', 'ES', 'ET', 'EU', 'EV', 'EW', 'EX', 'EY', 'EZ', 'FA', 'FB', 'FC', 'FD', 'FE', 'FF', 'FG', 'FH', 'FI', 'FJ', 'FK', 'FL', 'FM', 'FN', 'FO', 'FP', 'FQ', 'FR', 'FS', 'FT', 'FU', 'FV', 'FW', 'FX', 'FY', 'FZ', 'GA', 'GB', 'GC', 'GD', 'GE', 'GF', 'GG', 'GH', 'GI', 'GJ', 'GK', 'GL', 'GM', 'GN', 'GO', 'GP', 'GQ', 'GR', 'GS', 'GT', 'GU', 'GV', 'GW', 'GX', 'GY', 'GZ', 'HA', 'HB', 'HC', 'HD', 'HE', 'HF', 'HG', 'HH', 'HI', 'HJ', 'HK', 'HL', 'HM', 'HN', 'HO', 'HP', 'HQ', 'HR', 'HS', 'HT', 'HU', 'HV', 'HW', 'HX', 'HY', 'HZ', 'IA', 'IB', 'IC', 'ID', 'IE', 'IF', 'IG', 'IH', 'II', 'IJ', 'IK', 'IL', 'IM', 'IN', 'IO', 'IP', 'IQ', 'IR', 'IS', 'IT', 'IU', 'IV', 'IW', 'IX', 'IY', 'IZ', 'JA', 'JB', 'JC', 'JD', 'JE', 'JF', 'JG', 'JH', 'JI', 'JJ', 'JK', 'JL', 'JM', 'JN', 'JO', 'JP', 'JQ', 'JR', 'JS', 'JT', 'JU', 'JV', 'JW', 'JX', 'JY', 'JZ', 'KA', 'KB', 'KC', 'KD', 'KE', 'KF', 'KG', 'KH', 'KI', 'KJ', 'KK', 'KL', 'KM', 'KN', 'KO', 'KP', 'KQ', 'KR', 'KS', 'KT', 'KU', 'KV', 'KW', 'KX', 'KY', 'KZ', 'LA', 'LB', 'LC', 'LD', 'LE', 'LF', 'LG', 'LH', 'LI', 'LJ', 'LK', 'LL', 'LM', 'LN', 'LO', 'LP', 'LQ', 'LR', 'LS', 'LT', 'LU', 'LV', 'LW', 'LX', 'LY', 'LZ', 'MA', 'MB', 'MC', 'MD', 'ME', 'MF', 'MG', 'MH', 'MI', 'MJ', 'MK', 'ML', 'MM', 'MN', 'MO', 'MP', 'MQ', 'MR', 'MS', 'MT', 'MU', 'MV', 'MW', 'MX', 'MY', 'MZ', 'NA', 'NB', 'NC', 'ND', 'NE', 'NF', 'NG', 'NH', 'NI', 'NJ', 'NK', 'NL', 'NM', 'NN', 'NO', 'NP', 'NQ', 'NR', 'NS', 'NT', 'NU', 'NV', 'NW', 'NX', 'NY', 'NZ', 'OA', 'OB', 'OC', 'OD', 'OE', 'OF', 'OG', 'OH', 'OI', 'OJ', 'OK', 'OL', 'OM', 'ON', 'OO', 'OP', 'OQ', 'OR', 'OS', 'OT', 'OU', 'OV', 'OW', 'OX', 'OY', 'OZ', 'PA', 'PB', 'PC', 'PD', 'PE', 'PF', 'PG', 'PH', 'PI', 'PJ', 'PK', 'PL', 'PM', 'PN', 'PO', 'PP', 'PQ', 'PR', 'PS', 'PT', 'PU', 'PV', 'PW', 'PX', 'PY', 'PZ', 'QA', 'QB', 'QC', 'QD', 'QE', 'QF', 'QG', 'QH', 'QI', 'QJ', 'QK', 'QL', 'QM', 'QN', 'QO', 'QP', 'QQ', 'QR', 'QS', 'QT', 'QU', 'QV', 'QW', 'QX', 'QY', 'QZ', 'RA', 'RB', 'RC', 'RD', 'RE', 'RF', 'RG', 'RH', 'RI', 'RJ', 'RK', 'RL', 'RM', 'RN', 'RO', 'RP', 'RQ', 'RR', 'RS', 'RT', 'RU', 'RV', 'RW', 'RX', 'RY', 'RZ', 'SA', 'SB', 'SC', 'SD', 'SE', 'SF', 'SG', 'SH', 'SI', 'SJ', 'SK', 'SL', 'SM', 'SN', 'SO', 'SP', 'SQ', 'SR', 'SS', 'ST', 'SU', 'SV', 'SW', 'SX', 'SY', 'SZ', 'TA', 'TB', 'TC', 'TD', 'TE', 'TF', 'TG', 'TH', 'TI', 'TJ', 'TK', 'TL', 'TM', 'TN', 'TO', 'TP', 'TQ', 'TR', 'TS', 'TT', 'TU', 'TV', 'TW', 'TX', 'TY', 'TZ', 'UA', 'UB', 'UC', 'UD', 'UE', 'UF', 'UG', 'UH', 'UI', 'UJ', 'UK', 'UL', 'UM', 'UN', 'UO', 'UP', 'UQ', 'UR', 'US', 'UT', 'UU', 'UV', 'UW', 'UX', 'UY', 'UZ', 'VA', 'VB', 'VC', 'VD', 'VE', 'VF', 'VG', 'VH', 'VI', 'VJ', 'VK', 'VL', 'VM', 'VN', 'VO', 'VP', 'VQ', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ', 'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CQ', 'CR', 'CS', 'CT', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ', 'DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DK', 'DL', 'DM', 'DN', 'DO', 'DP', 'DQ', 'DR', 'DS', 'DT', 'DU', 'DV', 'DW', 'DX', 'DY', 'DZ', 'EA', 'EB', 'EC', 'ED', 'EE', 'EF', 'EG', 'EH', 'EI', 'EJ', 'EK', 'EL', 'EM', 'EN', 'EO', 'EP', 'EQ', 'ER', 'ES', 'ET', 'EU', 'EV', 'EW', 'EX', 'EY', 'EZ', 'FA', 'FB', 'FC', 'FD', 'FE', 'FF', 'FG', 'FH', 'FI', 'FJ', 'FK', 'FL', 'FM', 'FN', 'FO', 'FP', 'FQ', 'FR', 'FS', 'FT', 'FU', 'FV', 'FW', 'FX', 'FY', 'FZ', 'GA', 'GB', 'GC', 'GD', 'GE', 'GF', 'GG', 'GH', 'GI', 'GJ', 'GK', 'GL', 'GM', 'GN', 'GO', 'GP', 'GQ', 'GR', 'GS', 'GT', 'GU', 'GV', 'GW', 'GX', 'GY', 'GZ', 'HA', 'HB', 'HC', 'HD', 'HE', 'HF', 'HG', 'HH', 'HI', 'HJ', 'HK', 'HL', 'HM', 'HN', 'HO', 'HP', 'HQ', 'HR', 'HS', 'HT', 'HU', 'HV', 'HW', 'HX', 'HY', 'HZ', 'IA', 'IB', 'IC', 'ID', 'IE', 'IF', 'IG', 'IH', 'II', 'IJ', 'IK', 'IL', 'IM', 'IN', 'IO', 'IP', 'IQ', 'IR', 'IS', 'IT', 'IU', 'IV', 'IW', 'IX', 'IY', 'IZ', 'JA', 'JB', 'JC', 'JD', 'JE', 'JF', 'JG', 'JH', 'JI', 'JJ', 'JK', 'JL', 'JM', 'JN', 'JO', 'JP', 'JQ', 'JR', 'JS', 'JT', 'JU', 'JV', 'JW', 'JX', 'JY', 'JZ', 'KA', 'KB', 'KC', 'KD', 'KE', 'KF', 'KG', 'KH', 'KI', 'KJ', 'KK', 'KL', 'KM', 'KN', 'KO', 'KP', 'KQ', 'KR', 'KS', 'KT', 'KU', 'KV', 'KW', 'KX', 'KY', 'KZ', 'LA', 'LB', 'LC', 'LD', 'LE', 'LF', 'LG', 'LH', 'LI', 'LJ', 'LK', 'LL', 'LM', 'LN', 'LO', 'LP', 'LQ', 'LR', 'LS', 'LT', 'LU', 'LV', 'LW', 'LX', 'LY', 'LZ', 'MA', 'MB', 'MC', 'MD', 'ME', 'MF', 'MG', 'MH', 'MI', 'MJ', 'MK', 'ML', 'MM', 'MN', 'MO', 'MP', 'MQ', 'MR', 'MS', 'MT', 'MU', 'MV', 'MW', 'MX', 'MY', 'MZ', 'NA', 'NB', 'NC', 'ND', 'NE', 'NF', 'NG', 'NH', 'NI', 'NJ', 'NK', 'NL', 'NM', 'NN', 'NO', 'NP', 'NQ', 'NR', 'NS', 'NT', 'NU', 'NV', 'NW', 'NX', 'NY', 'NZ', 'OA', 'OB', 'OC', 'OD', 'OE', 'OF', 'OG', 'OH', 'OI', 'OJ', 'OK', 'OL', 'OM', 'ON', 'OO', 'OP', 'OQ', 'OR', 'OS', 'OT', 'OU', 'OV', 'OW', 'OX', 'OY', 'OZ', 'PA', 'PB', 'PC', 'PD', 'PE', 'PF', 'PG', 'PH', 'PI', 'PJ', 'PK', 'PL', 'PM', 'PN', 'PO', 'PP', 'PQ', 'PR', 'PS', 'PT', 'PU', 'PV', 'PW', 'PX', 'PY', 'PZ', 'QA', 'QB', 'QC', 'QD', 'QE', 'QF', 'QG', 'QH', 'QI', 'QJ', 'QK', 'QL', 'QM', 'QN', 'QO', 'QP', 'QQ', 'QR', 'QS', 'QT', 'QU', 'QV', 'QW', 'QX', 'QY', 'QZ', 'RA', 'RB', 'RC', 'RD', 'RE', 'RF', 'RG', 'RH', 'RI', 'RJ', 'RK', 'RL', 'RM', 'RN', 'RO', 'RP', 'RQ', 'RR', 'RS', 'RT', 'RU', 'RV', 'RW', 'RX', 'RY', 'RZ', 'SA', 'SB', 'SC', 'SD', 'SE', 'SF', 'SG', 'SH', 'SI', 'SJ', 'SK', 'SL', 'SM', 'SN', 'SO', 'SP', 'SQ', 'SR', 'SS', 'ST', 'SU', 'SV', 'SW', 'SX', 'SY', 'SZ', 'TA', 'TB', 'TC', 'TD', 'TE', 'TF', 'TG', 'TH', 'TI', 'TJ', 'TK', 'TL', 'TM', 'TN', 'TO', 'TP', 'TQ', 'TR', 'TS', 'TT', 'TU', 'TV', 'TW', 'TX', 'TY', 'TZ', 'UA', 'UB', 'UC', 'UD', 'UE', 'UF', 'UG', 'UH', 'UI', 'UJ', 'UK', 'UL', 'UM', 'UN', 'UO', 'UP', 'UQ', 'UR', 'US', 'UT', 'UU', 'UV', 'UW', 'UX', 'UY', 'UZ', 'VA', 'VB', 'VC', 'VD', 'VE', 'VF', 'VG', 'VH', 'VI', 'VJ', 'VK', 'VL', 'VM', 'VN', 'VO', 'VP', 'VQ');
//        $file_name = '1.xlsx';
        ini_set('memory_limit', '2048M');
        set_time_limit(1800);
        
        $objPHPexcel = \PhpOffice\PhpSpreadsheet\IOFactory::load("./bulk/$file_name");
        $objWorksheet = $objPHPexcel->setActiveSheetIndex(0);
        $max_row = $objWorksheet->getHighestRow();
        $max_col = $objWorksheet->getHighestColumn();
        $max_info = "AE";
        $max_fact = "CP";
        $max_fin = "UF";
        $max_assess = "VQ";
        $info = array('A' => 'C4', 'B' => 'E4', 'C' => 'C5', 'D' => 'C6', 'E' => 'C7', 'F' => 'C8', 'G' => 'C9', 'H' => 'C10', 'I' => 'C11', 'J' => 'C12', 'K' => 'C13', 'L' => 'B25', 'M' => 'C25', 'N' => 'D25', 'O' => 'B26', 'P' => 'C26', 'Q' => 'D26', 'R' => 'B27', 'S' => 'C27', 'T' => 'D27', 'U' => 'B28', 'V' => 'C28', 'W' => 'D28', 'X' => 'B29', 'Y' => 'C29', 'Z' => 'D29', 'AA' => 'B30', 'AB' => 'C30', 'AC' => 'D30', 'AD' => 'D32', 'AE' => 'C36', 'VM' => 'E7', 'VN' => 'E10');
        foreach ($info as $col => $label) {
            $objWorksheet->getCell($col . "1")->setValue($this->lang->line("borrower_information_$label"));
        }
        $fact = array('AF' => 'C4', 'AG' => 'E4', 'AH' => 'C5', 'AI' => 'C9', 'AJ' => 'D9', 'AK' => 'C10', 'AL' => 'D10', 'AM' => 'C11', 'AN' => 'D11', 'AO' => 'C12', 'AP' => 'D12', 'AQ' => 'C13', 'AR' => 'D13', 'AS' => 'C14', 'AT' => 'D14', 'AU' => 'C15', 'AV' => 'D15', 'AW' => 'C16', 'AX' => 'D16', 'AY' => 'C17', 'AZ' => 'D17', 'BA' => 'C18', 'BB' => 'D18', 'BC' => 'C22', 'BD' => 'D22', 'BE' => 'C23', 'BF' => 'D23', 'BG' => 'C24', 'BH' => 'D24', 'BI' => 'C25', 'BJ' => 'D25', 'BK' => 'C26', 'BL' => 'D26', 'BM' => 'C27', 'BN' => 'D27', 'BO' => 'C28', 'BP' => 'D28', 'BQ' => 'C29', 'BR' => 'D29', 'BS' => 'C30', 'BT' => 'D30', 'BU' => 'C31', 'BV' => 'D31', 'BW' => 'C35', 'BX' => 'D35', 'BY' => 'C36', 'BZ' => 'D36', 'CA' => 'C37', 'CB' => 'D37', 'CC' => 'C38', 'CD' => 'D38', 'CE' => 'C39', 'CF' => 'D39', 'CG' => 'C40', 'CH' => 'D40', 'CI' => 'C41', 'CJ' => 'D41', 'CK' => 'C42', 'CL' => 'D42', 'CM' => 'C43', 'CN' => 'D43', 'CO' => 'C44', 'CP' => 'D44');
        foreach ($fact as $col => $label) {
            $objWorksheet->getCell($col . "1")->setValue($this->lang->line("borrower_factsheet_$label"));
        }
        $fin = array('CQ' => 'E3', 'CR' => 'E4', 'CS' => 'G8', 'CT' => 'C10', 'CU' => 'D10', 'CV' => 'E10', 'CW' => 'F10', 'CX' => 'G10', 'CY' => 'C13', 'CZ' => 'D13', 'DA' => 'E13', 'DB' => 'F13', 'DC' => 'G13', 'DD' => 'C14', 'DE' => 'D14', 'DF' => 'E14', 'DG' => 'F14', 'DH' => 'G14', 'DI' => 'C18', 'DJ' => 'D18', 'DK' => 'E18', 'DL' => 'F18', 'DM' => 'G18', 'DN' => 'C19', 'DO' => 'D19', 'DP' => 'E19', 'DQ' => 'F19', 'DR' => 'G19', 'DS' => 'C22', 'DT' => 'D22', 'DU' => 'E22', 'DV' => 'F22', 'DW' => 'G22', 'DX' => 'C26', 'DY' => 'D26', 'DZ' => 'E26', 'EA' => 'F26', 'EB' => 'G26', 'EC' => 'C27', 'ED' => 'D27', 'EE' => 'E27', 'EF' => 'F27', 'EG' => 'G27', 'EH' => 'C28', 'EI' => 'D28', 'EJ' => 'E28', 'EK' => 'F28', 'EL' => 'G28', 'EM' => 'C32', 'EN' => 'D32', 'EO' => 'E32', 'EP' => 'F32', 'EQ' => 'G32', 'ER' => 'C33', 'ES' => 'D33', 'ET' => 'E33', 'EU' => 'F33', 'EV' => 'G33', 'EW' => 'C34', 'EX' => 'D34', 'EY' => 'E34', 'EZ' => 'F34', 'FA' => 'G34', 'FB' => 'C35', 'FC' => 'D35', 'FD' => 'E35', 'FE' => 'F35', 'FF' => 'G35', 'FG' => 'C36', 'FH' => 'D36', 'FI' => 'E36', 'FJ' => 'F36', 'FK' => 'G36', 'FL' => 'C38', 'FM' => 'D38', 'FN' => 'E38', 'FO' => 'F38', 'FP' => 'G38', 'FQ' => 'C39', 'FR' => 'D39', 'FS' => 'E39', 'FT' => 'F39', 'FU' => 'G39', 'FV' => 'C43', 'FW' => 'D43', 'FX' => 'E43', 'FY' => 'F43', 'FZ' => 'G43', 'GA' => 'C49', 'GB' => 'D49', 'GC' => 'E49', 'GD' => 'F49', 'GE' => 'G49', 'GF' => 'C50', 'GG' => 'D50', 'GH' => 'E50', 'GI' => 'F50', 'GJ' => 'G50', 'GK' => 'C51', 'GL' => 'D51', 'GM' => 'E51', 'GN' => 'F51', 'GO' => 'G51', 'GP' => 'C55', 'GQ' => 'D55', 'GR' => 'E55', 'GS' => 'F55', 'GT' => 'G55', 'GU' => 'C56', 'GV' => 'D56', 'GW' => 'E56', 'GX' => 'F56', 'GY' => 'G56', 'GZ' => 'C58', 'HA' => 'D58', 'HB' => 'E58', 'HC' => 'F58', 'HD' => 'G58', 'HE' => 'C62', 'HF' => 'D62', 'HG' => 'E62', 'HH' => 'F62', 'HI' => 'G62', 'HJ' => 'C63', 'HK' => 'D63', 'HL' => 'E63', 'HM' => 'F63', 'HN' => 'G63', 'HO' => 'C66', 'HP' => 'D66', 'HQ' => 'E66', 'HR' => 'F66', 'HS' => 'G66', 'HT' => 'C67', 'HU' => 'D67', 'HV' => 'E67', 'HW' => 'F67', 'HX' => 'G67', 'HY' => 'C71', 'HZ' => 'D71', 'IA' => 'E71', 'IB' => 'F71', 'IC' => 'G71', 'ID' => 'C78', 'IE' => 'D78', 'IF' => 'E78', 'IG' => 'F78', 'IH' => 'G78', 'II' => 'C79', 'IJ' => 'D79', 'IK' => 'E79', 'IL' => 'F79', 'IM' => 'G79', 'IN' => 'C80', 'IO' => 'D80', 'IP' => 'E80', 'IQ' => 'F80', 'IR' => 'G80', 'IS' => 'C81', 'IT' => 'D81', 'IU' => 'E81', 'IV' => 'F81', 'IW' => 'G81', 'IX' => 'C82', 'IY' => 'D82', 'IZ' => 'E82', 'JA' => 'F82', 'JB' => 'G82', 'JC' => 'C83', 'JD' => 'D83', 'JE' => 'E83', 'JF' => 'F83', 'JG' => 'G83', 'JH' => 'C84', 'JI' => 'D84', 'JJ' => 'E84', 'JK' => 'F84', 'JL' => 'G84', 'JM' => 'C85', 'JN' => 'D85', 'JO' => 'E85', 'JP' => 'F85', 'JQ' => 'G85', 'JR' => 'C86', 'JS' => 'D86', 'JT' => 'E86', 'JU' => 'F86', 'JV' => 'G86', 'JW' => 'C87', 'JX' => 'D87', 'JY' => 'E87', 'JZ' => 'F87', 'KA' => 'G87', 'KB' => 'C88', 'KC' => 'D88', 'KD' => 'E88', 'KE' => 'F88', 'KF' => 'G88', 'KG' => 'C89', 'KH' => 'D89', 'KI' => 'E89', 'KJ' => 'F89', 'KK' => 'G89', 'KL' => 'C90', 'KM' => 'D90', 'KN' => 'E90', 'KO' => 'F90', 'KP' => 'G90', 'KQ' => 'C92', 'KR' => 'D92', 'KS' => 'E92', 'KT' => 'F92', 'KU' => 'G92', 'KV' => 'C93', 'KW' => 'D93', 'KX' => 'E93', 'KY' => 'F93', 'KZ' => 'G93', 'LA' => 'C94', 'LB' => 'D94', 'LC' => 'E94', 'LD' => 'F94', 'LE' => 'G94', 'LF' => 'C97', 'LG' => 'D97', 'LH' => 'E97', 'LI' => 'F97', 'LJ' => 'G97', 'LK' => 'C98', 'LL' => 'D98', 'LM' => 'E98', 'LN' => 'F98', 'LO' => 'G98', 'LP' => 'C99', 'LQ' => 'D99', 'LR' => 'E99', 'LS' => 'F99', 'LT' => 'G99', 'LU' => 'C100', 'LV' => 'D100', 'LW' => 'E100', 'LX' => 'F100', 'LY' => 'G100', 'LZ' => 'C106', 'MA' => 'D106', 'MB' => 'E106', 'MC' => 'F106', 'MD' => 'G106', 'ME' => 'C107', 'MF' => 'D107', 'MG' => 'E107', 'MH' => 'F107', 'MI' => 'G107', 'MJ' => 'C108', 'MK' => 'D108', 'ML' => 'E108', 'MM' => 'F108', 'MN' => 'G108', 'MO' => 'C109', 'MP' => 'D109', 'MQ' => 'E109', 'MR' => 'F109', 'MS' => 'G109', 'MT' => 'C110', 'MU' => 'D110', 'MV' => 'E110', 'MW' => 'F110', 'MX' => 'G110', 'MY' => 'C111', 'MZ' => 'D111', 'NA' => 'E111', 'NB' => 'F111', 'NC' => 'G111', 'ND' => 'C112', 'NE' => 'D112', 'NF' => 'E112', 'NG' => 'F112', 'NH' => 'G112', 'NI' => 'C113', 'NJ' => 'D113', 'NK' => 'E113', 'NL' => 'F113', 'NM' => 'G113', 'NN' => 'C114', 'NO' => 'D114', 'NP' => 'E114', 'NQ' => 'F114', 'NR' => 'G114', 'NS' => 'C115', 'NT' => 'D115', 'NU' => 'E115', 'NV' => 'F115', 'NW' => 'G115', 'NX' => 'C117', 'NY' => 'D117', 'NZ' => 'E117', 'OA' => 'F117', 'OB' => 'G117', 'OC' => 'C119', 'OD' => 'D119', 'OE' => 'E119', 'OF' => 'F119', 'OG' => 'G119', 'OH' => 'C126', 'OI' => 'D126', 'OJ' => 'E126', 'OK' => 'F126', 'OL' => 'G126', 'OM' => 'C127', 'ON' => 'D127', 'OO' => 'E127', 'OP' => 'F127', 'OQ' => 'G127', 'OR' => 'C129', 'OS' => 'D129', 'OT' => 'E129', 'OU' => 'F129', 'OV' => 'G129', 'OW' => 'C130', 'OX' => 'D130', 'OY' => 'E130', 'OZ' => 'F130', 'PA' => 'G130', 'PB' => 'C132', 'PC' => 'D132', 'PD' => 'E132', 'PE' => 'F132', 'PF' => 'G132', 'PG' => 'C133', 'PH' => 'D133', 'PI' => 'E133', 'PJ' => 'F133', 'PK' => 'G133', 'PL' => 'C134', 'PM' => 'D134', 'PN' => 'E134', 'PO' => 'F134', 'PP' => 'G134', 'PQ' => 'C135', 'PR' => 'D135', 'PS' => 'E135', 'PT' => 'F135', 'PU' => 'G135', 'PV' => 'C136', 'PW' => 'D136', 'PX' => 'E136', 'PY' => 'F136', 'PZ' => 'G136', 'QA' => 'C137', 'QB' => 'D137', 'QC' => 'E137', 'QD' => 'F137', 'QE' => 'G137', 'QF' => 'C138', 'QG' => 'D138', 'QH' => 'E138', 'QI' => 'F138', 'QJ' => 'G138', 'QK' => 'C139', 'QL' => 'D139', 'QM' => 'E139', 'QN' => 'F139', 'QO' => 'G139', 'QP' => 'C140', 'QQ' => 'D140', 'QR' => 'E140', 'QS' => 'F140', 'QT' => 'G140', 'QU' => 'C144', 'QV' => 'D144', 'QW' => 'E144', 'QX' => 'F144', 'QY' => 'G144', 'QZ' => 'C145', 'RA' => 'D145', 'RB' => 'E145', 'RC' => 'F145', 'RD' => 'G145', 'RE' => 'C146', 'RF' => 'D146', 'RG' => 'E146', 'RH' => 'F146', 'RI' => 'G146', 'RJ' => 'C147', 'RK' => 'D147', 'RL' => 'E147', 'RM' => 'F147', 'RN' => 'G147', 'RO' => 'C148', 'RP' => 'D148', 'RQ' => 'E148', 'RR' => 'F148', 'RS' => 'G148', 'RT' => 'C149', 'RU' => 'D149', 'RV' => 'E149', 'RW' => 'F149', 'RX' => 'G149', 'RY' => 'C153', 'RZ' => 'D153', 'SA' => 'E153', 'SB' => 'F153', 'SC' => 'G153', 'SD' => 'C154', 'SE' => 'D154', 'SF' => 'E154', 'SG' => 'F154', 'SH' => 'G154', 'SI' => 'C155', 'SJ' => 'D155', 'SK' => 'E155', 'SL' => 'F155', 'SM' => 'G155', 'SN' => 'C156', 'SO' => 'D156', 'SP' => 'E156', 'SQ' => 'F156', 'SR' => 'G156', 'SS' => 'C158', 'ST' => 'D158', 'SU' => 'E158', 'SV' => 'F158', 'SW' => 'G158', 'SX' => 'C159', 'SY' => 'D159', 'SZ' => 'E159', 'TA' => 'F159', 'TB' => 'G159', 'TC' => 'C160', 'TD' => 'D160', 'TE' => 'E160', 'TF' => 'F160', 'TG' => 'G160', 'TH' => 'C161', 'TI' => 'D161', 'TJ' => 'E161', 'TK' => 'F161', 'TL' => 'G161', 'TM' => 'C162', 'TN' => 'D162', 'TO' => 'E162', 'TP' => 'F162', 'TQ' => 'G162', 'TR' => 'C163', 'TS' => 'D163', 'TT' => 'E163', 'TU' => 'F163', 'TV' => 'G163', 'TW' => 'C166', 'TX' => 'D166', 'TY' => 'E166', 'TZ' => 'F166', 'UA' => 'G166', 'UB' => 'C167', 'UC' => 'D167', 'UD' => 'E167', 'UE' => 'F167', 'UF' => 'G167');
        foreach ($fin as $col => $label) {
            $label = substr($label, 1);
            $objWorksheet->getCell($col . "1")->setValue($this->lang->line("borrower_financial_$label"));
        }
        $assess = array('UG' => 'F5', 'UH' => 'F10', 'UI' => 'F18', 'UJ' => 'F22', 'UK' => 'F29', 'UL' => 'F36', 'UM' => 'F40', 'UN' => 'F45', 'UO' => 'F50', 'UP' => 'F56', 'UQ' => 'F66', 'UR' => 'F71', 'US' => 'F75', 'UT' => 'F79', 'UU' => 'F86', 'UV' => 'F93', 'UW' => 'F100', 'UX' => 'F107', 'UY' => 'F115', 'UZ' => 'F121', 'VA' => 'F125', 'VB' => 'F129', 'VC' => 'F133', 'VD' => 'F137', 'VE' => 'F144', 'VF' => 'F151', 'VG' => 'F155', 'VH' => 'F159', 'VI' => 'F165', 'VJ' => 'F169', 'VK' => 'F175', 'VL' => 'F179', 'VM' => 'F186', 'VN' => 'F194', 'VO' => 'F200', 'VP' => 'F208', 'VQ' => 'F216');
        foreach ($assess as $col => $label) {
            $objWorksheet->getCell($col . "1")->setValue($this->lang->line("borrower_assessment_$label"));
        }
        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objPHPexcel, 'Xlsx');
        $objWriter->save("./bulk/" . "Template.xlsx");
    }

    function map_values() {
        
    }

    function arrays() {
        $info = array('A' => 'C4', 'B' => 'E4', 'C' => 'C5', 'D' => 'C6', 'E' => 'C7', 'F' => 'C8', 'G' => 'C9', 'H' => 'C10', 'I' => 'C11', 'J' => 'C12', 'K' => 'C13',
            'BA' => 'B25', 'BB' => 'C25', 'BC' => 'D25', 'BD' => 'B26', 'BE' => 'C26', 'BF' => 'D26', 'BG' => 'B27', 'BH' => 'C27', 'BI' => 'D27', 'BJ' => 'B28', 'BK' => 'C28', 'BL' => 'D28', 'BM' => 'B29', 'BN' => 'C29', 'BO' => 'D29', 'BP' => 'B30', 'BQ' => 'C30', 'BR' => 'D30', 'BS' => 'D32',
            'L' => 'C36');
        $fact = array('M' => 'C4', 'N' => 'E4', 'O' => 'C5',
            'MG' => 'C9', 'MH' => 'D9', 'MI' => 'C10', 'MJ' => 'D10', 'MK' => 'C11', 'ML' => 'D11', 'MM' => 'C12', 'MN' => 'D12', 'MO' => 'C13', 'MP' => 'D13', 'MQ' => 'C14', 'MR' => 'D14', 'MS' => 'C15', 'MT' => 'D15', 'MU' => 'C16', 'MV' => 'D16', 'MW' => 'C17', 'MX' => 'D17', 'MY' => 'C18', 'MZ' => 'D18', 'NA' => 'C22', 'NB' => 'D22', 'NC' => 'C23', 'ND' => 'D23', 'NE' => 'C24', 'NF' => 'D24', 'NG' => 'C25', 'NH' => 'D25', 'NI' => 'C26', 'NJ' => 'D26', 'NK' => 'C27', 'NL' => 'D27', 'NM' => 'C28', 'NN' => 'D28', 'NO' => 'C29', 'NP' => 'D29', 'NQ' => 'C30', 'NR' => 'D30', 'NS' => 'C31', 'NT' => 'D31', 'NU' => 'C35', 'NV' => 'D35', 'NW' => 'C36', 'NX' => 'D36', 'NY' => 'C37', 'NZ' => 'D37', 'OA' => 'C38', 'OB' => 'D38', 'OC' => 'C39', 'OD' => 'D39', 'OE' => 'C40', 'OF' => 'D40', 'OG' => 'C41', 'OH' => 'D41', 'OI' => 'C42', 'OJ' => 'D42', 'OK' => 'C43', 'OL' => 'D43', 'OM' => 'C44', 'ON' => 'D44'
        );
        $assess = array('P' => 'F5', 'Q' => 'F10', 'R' => 'F18', 'S' => 'F22', 'T' => 'F29', 'U' => 'F36', 'V' => 'F40', 'W' => 'F45', 'X' => 'F50', 'Y' => 'F56', 'Z' => 'F66', 'AA' => 'F71', 'AB' => 'F75', 'AC' => 'F79', 'AD' => 'F86', 'AE' => 'F93', 'AF' => 'F100', 'AG' => 'F107', 'AH' => 'F115', 'AI' => 'F121', 'AJ' => 'F125', 'AK' => 'F129', 'AL' => 'F133', 'AM' => 'F137', 'AN' => 'F144', 'AO' => 'F151', 'AP' => 'F155', 'AQ' => 'F159', 'AR' => 'F165', 'AS' => 'F169', 'AT' => 'F175', 'AU' => 'F179', 'AV' => 'F186', 'AW' => 'F194', 'AX' => 'F200', 'AY' => 'F208', 'AZ' => 'F216');

        $fin = array('BT' => 'E3', 'BU' => 'E4', 'BV' => 'G8',
            //base (E)
            'BW' => 'E10', 'BX' => 'E13', 'BY' => 'E14', 'BZ' => 'E18', 'CA' => 'E19', 'CB' => 'E22', 'CC' => 'E26', 'CD' => 'E27', 'CE' => 'E28', 'CF' => 'E32', 'CG' => 'E33', 'CH' => 'E34', 'CI' => 'E35', 'CJ' => 'E36', 'CK' => 'E38', 'CL' => 'E39', 'CM' => 'E43', 'CN' => 'E49', 'CO' => 'E50', 'CP' => 'E51', 'CQ' => 'E55', 'CR' => 'E56', 'CS' => 'E58', 'CT' => 'E62', 'CU' => 'E63', 'CV' => 'E66', 'CW' => 'E67', 'CX' => 'E71', 'CY' => 'E78', 'CZ' => 'E79', 'DA' => 'E80', 'DB' => 'E81', 'DC' => 'E82', 'DD' => 'E83', 'DE' => 'E84', 'DF' => 'E85', 'DG' => 'E86', 'DH' => 'E87', 'DI' => 'E88', 'DJ' => 'E89', 'DK' => 'E90', 'DL' => 'E92', 'DM' => 'E93', 'DN' => 'E94', 'DO' => 'E97', 'DP' => 'E98', 'DQ' => 'E99', 'DR' => 'E100', 'DS' => 'E106', 'DT' => 'E107', 'DU' => 'E108', 'DV' => 'E109', 'DW' => 'E110', 'DX' => 'E111', 'DY' => 'E112', 'DZ' => 'E113', 'EA' => 'E114', 'EB' => 'E115', 'EC' => 'E117', 'ED' => 'E119', 'EE' => 'E126', 'EF' => 'E127', 'EG' => 'E129', 'EH' => 'E130', 'EI' => 'E132', 'EJ' => 'E133', 'EK' => 'E134', 'EL' => 'E135', 'EM' => 'E136', 'EN' => 'E137', 'EO' => 'E138', 'EP' => 'E139', 'EQ' => 'E140', 'ER' => 'E145', 'ES' => 'E146', 'ET' => 'E147', 'EU' => 'E148', 'EV' => 'E149', 'EW' => 'E153', 'EX' => 'E154', 'EY' => 'E155', 'EZ' => 'E156', 'FA' => 'E158', 'FB' => 'E159', 'FC' => 'E160', 'FD' => 'E161', 'FE' => 'E162', 'FF' => 'E163', 'FG' => 'E166', 'FH' => 'E167',
            //hist one (C)
            'FI' => 'C10', 'FJ' => 'C13', 'FK' => 'C14', 'FL' => 'C18', 'FM' => 'C19', 'FN' => 'C22', 'FO' => 'C26', 'FP' => 'C27', 'FQ' => 'C28', 'FR' => 'C32', 'FS' => 'C33', 'FT' => 'C34', 'FU' => 'C35', 'FV' => 'C36', 'FW' => 'C38', 'FX' => 'C39', 'FY' => 'C43', 'FZ' => 'C49', 'GA' => 'C50', 'GB' => 'C51', 'GC' => 'C55', 'GD' => 'C56', 'GE' => 'C58', 'GF' => 'C62', 'GG' => 'C63', 'GH' => 'C66', 'GI' => 'C67', 'GJ' => 'C71', 'GK' => 'C78', 'GL' => 'C79', 'GM' => 'C80', 'GN' => 'C81', 'GO' => 'C82', 'GP' => 'C83', 'GQ' => 'C84', 'GR' => 'C85', 'GS' => 'C86', 'GT' => 'C87', 'GU' => 'C88', 'GV' => 'C89', 'GW' => 'C90', 'GX' => 'C92', 'GY' => 'C93', 'GZ' => 'C94', 'HA' => 'C97', 'HB' => 'C98', 'HC' => 'C99', 'HD' => 'C100', 'HE' => 'C106', 'HF' => 'C107', 'HG' => 'C108', 'HH' => 'C109', 'HI' => 'C110', 'HJ' => 'C111', 'HK' => 'C112', 'HL' => 'C113', 'HM' => 'C114', 'HN' => 'C115', 'HO' => 'C117', 'HP' => 'C119', 'HQ' => 'C126', 'HR' => 'C127', 'HS' => 'C129', 'HT' => 'C130', 'HU' => 'C132', 'HV' => 'C133', 'HW' => 'C134', 'HX' => 'C135', 'HY' => 'C136', 'HZ' => 'C137', 'IA' => 'C138', 'IB' => 'C139', 'IC' => 'C140', 'ID' => 'C145', 'IE' => 'C146', 'IF' => 'C147', 'IG' => 'C148', 'IH' => 'C149', 'II' => 'C153', 'IJ' => 'C154', 'IK' => 'C155', 'IL' => 'C156', 'IM' => 'C158', 'IN' => 'C159', 'IO' => 'C160', 'IP' => 'C161', 'IQ' => 'C162', 'IR' => 'C163', 'IS' => 'C166', 'IT' => 'C167',
            //hist two (D)
            'IU' => 'D10', 'IV' => 'D13', 'IW' => 'D14', 'IX' => 'D18', 'IY' => 'D19', 'IZ' => 'D22', 'JA' => 'D26', 'JB' => 'D27', 'JC' => 'D28', 'JD' => 'D32', 'JE' => 'D33', 'JF' => 'D34', 'JG' => 'D35', 'JH' => 'D36', 'JI' => 'D38', 'JJ' => 'D39', 'JK' => 'D43', 'JL' => 'D49', 'JM' => 'D50', 'JN' => 'D51', 'JO' => 'D55', 'JP' => 'D56', 'JQ' => 'D58', 'JR' => 'D62', 'JS' => 'D63', 'JT' => 'D66', 'JU' => 'D67', 'JV' => 'D71', 'JW' => 'D78', 'JX' => 'D79', 'JY' => 'D80', 'JZ' => 'D81', 'KA' => 'D82', 'KB' => 'D83', 'KC' => 'D84', 'KD' => 'D85', 'KE' => 'D86', 'KF' => 'D87', 'KG' => 'D88', 'KH' => 'D89', 'KI' => 'D90', 'KJ' => 'D92', 'KK' => 'D93', 'KL' => 'D94', 'KM' => 'D97', 'KN' => 'D98', 'KO' => 'D99', 'KP' => 'D100', 'KQ' => 'D106', 'KR' => 'D107', 'KS' => 'D108', 'KT' => 'D109', 'KU' => 'D110', 'KV' => 'D111', 'KW' => 'D112', 'KX' => 'D113', 'KY' => 'D114', 'KZ' => 'D115', 'LA' => 'D117', 'LB' => 'D119', 'LC' => 'D126', 'LD' => 'D127', 'LE' => 'D129', 'LF' => 'D130', 'LG' => 'D132', 'LH' => 'D133', 'LI' => 'D134', 'LJ' => 'D135', 'LK' => 'D136', 'LL' => 'D137', 'LM' => 'D138', 'LN' => 'D139', 'LO' => 'D140', 'LP' => 'D145', 'LQ' => 'D146', 'LR' => 'D147', 'LS' => 'D148', 'LT' => 'D149', 'LU' => 'D153', 'LV' => 'D154', 'LW' => 'D155', 'LX' => 'D156', 'LY' => 'D158', 'LZ' => 'D159', 'MA' => 'D160', 'MB' => 'D161', 'MC' => 'D162', 'MD' => 'D163', 'ME' => 'D166', 'MF' => 'D167',
            //proj one (F)
            'OO' => 'F10', 'OP' => 'F13', 'OQ' => 'F14', 'OR' => 'F18', 'OS' => 'F19', 'OT' => 'F22', 'OU' => 'F26', 'OV' => 'F27', 'OW' => 'F28', 'OX' => 'F32', 'OY' => 'F33', 'OZ' => 'F34', 'PA' => 'F35', 'PB' => 'F36', 'PC' => 'F38', 'PD' => 'F39', 'PE' => 'F43', 'PF' => 'F49', 'PG' => 'F50', 'PH' => 'F51', 'PI' => 'F55', 'PJ' => 'F56', 'PK' => 'F58', 'PL' => 'F62', 'PM' => 'F63', 'PN' => 'F66', 'PO' => 'F67', 'PP' => 'F71', 'PQ' => 'F78', 'PR' => 'F79', 'PS' => 'F80', 'PT' => 'F81', 'PU' => 'F82', 'PV' => 'F83', 'PW' => 'F84', 'PX' => 'F85', 'PY' => 'F86', 'PZ' => 'F87', 'QA' => 'F88', 'QB' => 'F89', 'QC' => 'F90', 'QD' => 'F92', 'QE' => 'F93', 'QF' => 'F94', 'QG' => 'F97', 'QH' => 'F98', 'QI' => 'F99', 'QJ' => 'F100', 'QK' => 'F106', 'QL' => 'F107', 'QM' => 'F108', 'QN' => 'F109', 'QO' => 'F110', 'QP' => 'F111', 'QQ' => 'F112', 'QR' => 'F113', 'QS' => 'F114', 'QT' => 'F115', 'QU' => 'F117', 'QV' => 'F119', 'QW' => 'F126', 'QX' => 'F127', 'QY' => 'F129', 'QZ' => 'F130', 'RA' => 'F132', 'RB' => 'F133', 'RC' => 'F134', 'RD' => 'F135', 'RE' => 'F136', 'RF' => 'F137', 'RG' => 'F138', 'RH' => 'F139', 'RI' => 'F140', 'RJ' => 'F145', 'RK' => 'F146', 'RL' => 'F147', 'RM' => 'F148', 'RN' => 'F149', 'RO' => 'F153', 'RP' => 'F154', 'RQ' => 'F155', 'RR' => 'F156', 'RS' => 'F158', 'RT' => 'F159', 'RU' => 'F160', 'RV' => 'F161', 'RW' => 'F162', 'RX' => 'F163', 'RY' => 'F166', 'RZ' => 'F167',
            ///proj two (G)
            'SA' => 'G10', 'SB' => 'G13', 'SC' => 'G14', 'SD' => 'G18', 'SE' => 'G19', 'SF' => 'G22', 'SG' => 'G26', 'SH' => 'G27', 'SI' => 'G28', 'SJ' => 'G32', 'SK' => 'G33', 'SL' => 'G34', 'SM' => 'G35', 'SN' => 'G36', 'SO' => 'G38', 'SP' => 'G39', 'SQ' => 'G43', 'SR' => 'G49', 'SS' => 'G50', 'ST' => 'G51', 'SU' => 'G55', 'SV' => 'G56', 'SW' => 'G58', 'SX' => 'G62', 'SY' => 'G63', 'SZ' => 'G66', 'TA' => 'G67', 'TB' => 'G71', 'TC' => 'G78', 'TD' => 'G79', 'TE' => 'G80', 'TF' => 'G81', 'TG' => 'G82', 'TH' => 'G83', 'TI' => 'G84', 'TJ' => 'G85', 'TK' => 'G86', 'TL' => 'G87', 'TM' => 'G88', 'TN' => 'G89', 'TO' => 'G90', 'TP' => 'G92', 'TQ' => 'G93', 'TR' => 'G94', 'TS' => 'G97', 'TT' => 'G98', 'TU' => 'G99', 'TV' => 'G100', 'TW' => 'G106', 'TX' => 'G107', 'TY' => 'G108', 'TZ' => 'G109', 'UA' => 'G110', 'UB' => 'G111', 'UC' => 'G112', 'UD' => 'G113', 'UE' => 'G114', 'UF' => 'G115', 'UG' => 'G117', 'UH' => 'G119', 'UI' => 'G126', 'UJ' => 'G127', 'UK' => 'G129', 'UL' => 'G130', 'UM' => 'G132', 'UN' => 'G133', 'UO' => 'G134', 'UP' => 'G135', 'UQ' => 'G136', 'UR' => 'G137', 'US' => 'G138', 'UT' => 'G139', 'UU' => 'G140', 'UV' => 'G145', 'UW' => 'G146', 'UX' => 'G147', 'UY' => 'G148', 'UZ' => 'G149', 'VA' => 'G153', 'VB' => 'G154', 'VC' => 'G155', 'VD' => 'G156', 'VE' => 'G158', 'VF' => 'G159', 'VG' => 'G160', 'VH' => 'G161', 'VI' => 'G162', 'VJ' => 'G163', 'VK' => 'G166', 'VL' => 'G167'
        );
        //            'BW'=>'C10','CU'=>'D10','CV'=>'E10','CW'=>'F10','CX'=>'G10',
//            'BX'=>'C13','CZ'=>'D13','DA'=>'E13','DB'=>'F13','DC'=>'G13',
//            'BY'=>'C14','DE'=>'D14','DF'=>'E14','DG'=>'F14','DH'=>'G14',
//            'DI'=>'C18','DJ'=>'D18','DK'=>'E18','DL'=>'F18','DM'=>'G18','DN'=>'C19','DO'=>'D19','DP'=>'E19','DQ'=>'F19','DR'=>'G19','DS'=>'C22','DT'=>'D22','DU'=>'E22','DV'=>'F22','DW'=>'G22','DX'=>'C26','DY'=>'D26','DZ'=>'E26','EA'=>'F26','EB'=>'G26','EC'=>'C27','ED'=>'D27','EE'=>'E27','EF'=>'F27','EG'=>'G27','EH'=>'C28','EI'=>'D28','EJ'=>'E28','EK'=>'F28','EL'=>'G28','EM'=>'C32','EN'=>'D32','EO'=>'E32','EP'=>'F32','EQ'=>'G32','ER'=>'C33','ES'=>'D33','ET'=>'E33','EU'=>'F33','EV'=>'G33','EW'=>'C34','EX'=>'D34','EY'=>'E34','EZ'=>'F34','FA'=>'G34','FB'=>'C35','FC'=>'D35','FD'=>'E35','FE'=>'F35','FF'=>'G35','FG'=>'C36','FH'=>'D36','FI'=>'E36','FJ'=>'F36','FK'=>'G36','FL'=>'C38','FM'=>'D38','FN'=>'E38','FO'=>'F38','FP'=>'G38','FQ'=>'C39','FR'=>'D39','FS'=>'E39','FT'=>'F39','FU'=>'G39','FV'=>'C43','FW'=>'D43','FX'=>'E43','FY'=>'F43','FZ'=>'G43','GA'=>'C49','GB'=>'D49','GC'=>'E49','GD'=>'F49','GE'=>'G49','GF'=>'C50','GG'=>'D50','GH'=>'E50','GI'=>'F50','GJ'=>'G50','GK'=>'C51','GL'=>'D51','GM'=>'E51','GN'=>'F51','GO'=>'G51','GP'=>'C55','GQ'=>'D55','GR'=>'E55','GS'=>'F55','GT'=>'G55','GU'=>'C56','GV'=>'D56','GW'=>'E56','GX'=>'F56','GY'=>'G56','GZ'=>'C58','HA'=>'D58','HB'=>'E58','HC'=>'F58','HD'=>'G58','HE'=>'C62','HF'=>'D62','HG'=>'E62','HH'=>'F62','HI'=>'G62','HJ'=>'C63','HK'=>'D63','HL'=>'E63','HM'=>'F63','HN'=>'G63','HO'=>'C66','HP'=>'D66','HQ'=>'E66','HR'=>'F66','HS'=>'G66','HT'=>'C67','HU'=>'D67','HV'=>'E67','HW'=>'F67','HX'=>'G67','HY'=>'C71','HZ'=>'D71','IA'=>'E71','IB'=>'F71','IC'=>'G71','ID'=>'C78','IE'=>'D78','IF'=>'E78','IG'=>'F78','IH'=>'G78','II'=>'C79','IJ'=>'D79','IK'=>'E79','IL'=>'F79','IM'=>'G79','IN'=>'C80','IO'=>'D80','IP'=>'E80','IQ'=>'F80','IR'=>'G80','IS'=>'C81','IT'=>'D81','IU'=>'E81','IV'=>'F81','IW'=>'G81','IX'=>'C82','IY'=>'D82','IZ'=>'E82','JA'=>'F82','JB'=>'G82','JC'=>'C83','JD'=>'D83','JE'=>'E83','JF'=>'F83','JG'=>'G83','JH'=>'C84','JI'=>'D84','JJ'=>'E84','JK'=>'F84','JL'=>'G84','JM'=>'C85','JN'=>'D85','JO'=>'E85','JP'=>'F85','JQ'=>'G85','JR'=>'C86','JS'=>'D86','JT'=>'E86','JU'=>'F86','JV'=>'G86','JW'=>'C87','JX'=>'D87','JY'=>'E87','JZ'=>'F87','KA'=>'G87','KB'=>'C88','KC'=>'D88','KD'=>'E88','KE'=>'F88','KF'=>'G88','KG'=>'C89','KH'=>'D89','KI'=>'E89','KJ'=>'F89','KK'=>'G89','KL'=>'C90','KM'=>'D90','KN'=>'E90','KO'=>'F90','KP'=>'G90','KQ'=>'C92','KR'=>'D92','KS'=>'E92','KT'=>'F92','KU'=>'G92','KV'=>'C93','KW'=>'D93','KX'=>'E93','KY'=>'F93','KZ'=>'G93','LA'=>'C94','LB'=>'D94','LC'=>'E94','LD'=>'F94','LE'=>'G94','LF'=>'C97','LG'=>'D97','LH'=>'E97','LI'=>'F97','LJ'=>'G97','LK'=>'C98','LL'=>'D98','LM'=>'E98','LN'=>'F98','LO'=>'G98','LP'=>'C99','LQ'=>'D99','LR'=>'E99','LS'=>'F99','LT'=>'G99','LU'=>'C100','LV'=>'D100','LW'=>'E100','LX'=>'F100','LY'=>'G100','LZ'=>'C106','MA'=>'D106','MB'=>'E106','MC'=>'F106','MD'=>'G106','ME'=>'C107','MF'=>'D107','MG'=>'E107','MH'=>'F107','MI'=>'G107','MJ'=>'C108','MK'=>'D108','ML'=>'E108','MM'=>'F108','MN'=>'G108','MO'=>'C109','MP'=>'D109','MQ'=>'E109','MR'=>'F109','MS'=>'G109','MT'=>'C110','MU'=>'D110','MV'=>'E110','MW'=>'F110','MX'=>'G110','MY'=>'C111','MZ'=>'D111','NA'=>'E111','NB'=>'F111','NC'=>'G111','ND'=>'C112','NE'=>'D112','NF'=>'E112','NG'=>'F112','NH'=>'G112','NI'=>'C113','NJ'=>'D113','NK'=>'E113','NL'=>'F113','NM'=>'G113','NN'=>'C114','NO'=>'D114','NP'=>'E114','NQ'=>'F114','NR'=>'G114','NS'=>'C115','NT'=>'D115','NU'=>'E115','NV'=>'F115','NW'=>'G115','NX'=>'C117','NY'=>'D117','NZ'=>'E117','OA'=>'F117','OB'=>'G117','OC'=>'C119','OD'=>'D119','OE'=>'E119','OF'=>'F119','OG'=>'G119','OH'=>'C126','OI'=>'D126','OJ'=>'E126','OK'=>'F126','OL'=>'G126','OM'=>'C127','ON'=>'D127','OO'=>'E127','OP'=>'F127','OQ'=>'G127','OR'=>'C129','OS'=>'D129','OT'=>'E129','OU'=>'F129','OV'=>'G129','OW'=>'C130','OX'=>'D130','OY'=>'E130','OZ'=>'F130','PA'=>'G130','PB'=>'C132','PC'=>'D132','PD'=>'E132','PE'=>'F132','PF'=>'G132','PG'=>'C133','PH'=>'D133','PI'=>'E133','PJ'=>'F133','PK'=>'G133','PL'=>'C134','PM'=>'D134','PN'=>'E134','PO'=>'F134','PP'=>'G134','PQ'=>'C135','PR'=>'D135','PS'=>'E135','PT'=>'F135','PU'=>'G135','PV'=>'C136','PW'=>'D136','PX'=>'E136','PY'=>'F136','PZ'=>'G136','QA'=>'C137','QB'=>'D137','QC'=>'E137','QD'=>'F137','QE'=>'G137','QF'=>'C138','QG'=>'D138','QH'=>'E138','QI'=>'F138','QJ'=>'G138','QK'=>'C139','QL'=>'D139','QM'=>'E139','QN'=>'F139','QO'=>'G139','QP'=>'C140','QQ'=>'D140','QR'=>'E140','QS'=>'F140','QT'=>'G140','QU'=>'C144','QV'=>'D144','QW'=>'E144','QX'=>'F144','QY'=>'G144','QZ'=>'C145','RA'=>'D145','RB'=>'E145','RC'=>'F145','RD'=>'G145','RE'=>'C146','RF'=>'D146','RG'=>'E146','RH'=>'F146','RI'=>'G146','RJ'=>'C147','RK'=>'D147','RL'=>'E147','RM'=>'F147','RN'=>'G147','RO'=>'C148','RP'=>'D148','RQ'=>'E148','RR'=>'F148','RS'=>'G148','RT'=>'C149','RU'=>'D149','RV'=>'E149','RW'=>'F149','RX'=>'G149','RY'=>'C153','RZ'=>'D153','SA'=>'E153','SB'=>'F153','SC'=>'G153','SD'=>'C154','SE'=>'D154','SF'=>'E154','SG'=>'F154','SH'=>'G154','SI'=>'C155','SJ'=>'D155','SK'=>'E155','SL'=>'F155','SM'=>'G155','SN'=>'C156','SO'=>'D156','SP'=>'E156','SQ'=>'F156','SR'=>'G156','SS'=>'C158','ST'=>'D158','SU'=>'E158','SV'=>'F158','SW'=>'G158','SX'=>'C159','SY'=>'D159','SZ'=>'E159','TA'=>'F159','TB'=>'G159','TC'=>'C160','TD'=>'D160','TE'=>'E160','TF'=>'F160','TG'=>'G160','TH'=>'C161','TI'=>'D161','TJ'=>'E161','TK'=>'F161','TL'=>'G161','TM'=>'C162','TN'=>'D162','TO'=>'E162','TP'=>'F162','TQ'=>'G162','TR'=>'C163','TS'=>'D163','TT'=>'E163','TU'=>'F163','TV'=>'G163','TW'=>'C166','TX'=>'D166','TY'=>'E166','TZ'=>'F166','UA'=>'G166','UB'=>'C167','UC'=>'D167','UD'=>'E167','UE'=>'F167','UF'=>'G167'

        $count = 0;
        $string = "";
        foreach ($fin as $key => $value) {
            if ($value[0] == "C") {
                $string.= "'" . $value . "',";
                $count++;
            }
        }
        echo $count;
        echo $string;
    }

    function get_labels($data) {
        $data = $data[1];
//        print_r($dsata);
        $start = false;
        $count = 0;
        $string = "";
        $fin = array(
            'BW' => 'C10', 'CU' => 'D10', 'CV' => 'E10', 'CW' => 'F10', 'CX' => 'G10',
            'BX' => 'C13', 'CZ' => 'D13', 'DA' => 'E13', 'DB' => 'F13', 'DC' => 'G13',
            'BY' => 'C14', 'DE' => 'D14', 'DF' => 'E14', 'DG' => 'F14', 'DH' => 'G14',
            'DI' => 'C18', 'DJ' => 'D18', 'DK' => 'E18', 'DL' => 'F18', 'DM' => 'G18', 'DN' => 'C19', 'DO' => 'D19', 'DP' => 'E19', 'DQ' => 'F19', 'DR' => 'G19', 'DS' => 'C22', 'DT' => 'D22', 'DU' => 'E22', 'DV' => 'F22', 'DW' => 'G22', 'DX' => 'C26', 'DY' => 'D26', 'DZ' => 'E26', 'EA' => 'F26', 'EB' => 'G26', 'EC' => 'C27', 'ED' => 'D27', 'EE' => 'E27', 'EF' => 'F27', 'EG' => 'G27', 'EH' => 'C28', 'EI' => 'D28', 'EJ' => 'E28', 'EK' => 'F28', 'EL' => 'G28', 'EM' => 'C32', 'EN' => 'D32', 'EO' => 'E32', 'EP' => 'F32', 'EQ' => 'G32', 'ER' => 'C33', 'ES' => 'D33', 'ET' => 'E33', 'EU' => 'F33', 'EV' => 'G33', 'EW' => 'C34', 'EX' => 'D34', 'EY' => 'E34', 'EZ' => 'F34', 'FA' => 'G34', 'FB' => 'C35', 'FC' => 'D35', 'FD' => 'E35', 'FE' => 'F35', 'FF' => 'G35', 'FG' => 'C36', 'FH' => 'D36', 'FI' => 'E36', 'FJ' => 'F36', 'FK' => 'G36', 'FL' => 'C38', 'FM' => 'D38', 'FN' => 'E38', 'FO' => 'F38', 'FP' => 'G38', 'FQ' => 'C39', 'FR' => 'D39', 'FS' => 'E39', 'FT' => 'F39', 'FU' => 'G39', 'FV' => 'C43', 'FW' => 'D43', 'FX' => 'E43', 'FY' => 'F43', 'FZ' => 'G43', 'GA' => 'C49', 'GB' => 'D49', 'GC' => 'E49', 'GD' => 'F49', 'GE' => 'G49', 'GF' => 'C50', 'GG' => 'D50', 'GH' => 'E50', 'GI' => 'F50', 'GJ' => 'G50', 'GK' => 'C51', 'GL' => 'D51', 'GM' => 'E51', 'GN' => 'F51', 'GO' => 'G51', 'GP' => 'C55', 'GQ' => 'D55', 'GR' => 'E55', 'GS' => 'F55', 'GT' => 'G55', 'GU' => 'C56', 'GV' => 'D56', 'GW' => 'E56', 'GX' => 'F56', 'GY' => 'G56', 'GZ' => 'C58', 'HA' => 'D58', 'HB' => 'E58', 'HC' => 'F58', 'HD' => 'G58', 'HE' => 'C62', 'HF' => 'D62', 'HG' => 'E62', 'HH' => 'F62', 'HI' => 'G62', 'HJ' => 'C63', 'HK' => 'D63', 'HL' => 'E63', 'HM' => 'F63', 'HN' => 'G63', 'HO' => 'C66', 'HP' => 'D66', 'HQ' => 'E66', 'HR' => 'F66', 'HS' => 'G66', 'HT' => 'C67', 'HU' => 'D67', 'HV' => 'E67', 'HW' => 'F67', 'HX' => 'G67', 'HY' => 'C71', 'HZ' => 'D71', 'IA' => 'E71', 'IB' => 'F71', 'IC' => 'G71', 'ID' => 'C78', 'IE' => 'D78', 'IF' => 'E78', 'IG' => 'F78', 'IH' => 'G78', 'II' => 'C79', 'IJ' => 'D79', 'IK' => 'E79', 'IL' => 'F79', 'IM' => 'G79', 'IN' => 'C80', 'IO' => 'D80', 'IP' => 'E80', 'IQ' => 'F80', 'IR' => 'G80', 'IS' => 'C81', 'IT' => 'D81', 'IU' => 'E81', 'IV' => 'F81', 'IW' => 'G81', 'IX' => 'C82', 'IY' => 'D82', 'IZ' => 'E82', 'JA' => 'F82', 'JB' => 'G82', 'JC' => 'C83', 'JD' => 'D83', 'JE' => 'E83', 'JF' => 'F83', 'JG' => 'G83', 'JH' => 'C84', 'JI' => 'D84', 'JJ' => 'E84', 'JK' => 'F84', 'JL' => 'G84', 'JM' => 'C85', 'JN' => 'D85', 'JO' => 'E85', 'JP' => 'F85', 'JQ' => 'G85', 'JR' => 'C86', 'JS' => 'D86', 'JT' => 'E86', 'JU' => 'F86', 'JV' => 'G86', 'JW' => 'C87', 'JX' => 'D87', 'JY' => 'E87', 'JZ' => 'F87', 'KA' => 'G87', 'KB' => 'C88', 'KC' => 'D88', 'KD' => 'E88', 'KE' => 'F88', 'KF' => 'G88', 'KG' => 'C89', 'KH' => 'D89', 'KI' => 'E89', 'KJ' => 'F89', 'KK' => 'G89', 'KL' => 'C90', 'KM' => 'D90', 'KN' => 'E90', 'KO' => 'F90', 'KP' => 'G90', 'KQ' => 'C92', 'KR' => 'D92', 'KS' => 'E92', 'KT' => 'F92', 'KU' => 'G92', 'KV' => 'C93', 'KW' => 'D93', 'KX' => 'E93', 'KY' => 'F93', 'KZ' => 'G93', 'LA' => 'C94', 'LB' => 'D94', 'LC' => 'E94', 'LD' => 'F94', 'LE' => 'G94', 'LF' => 'C97', 'LG' => 'D97', 'LH' => 'E97', 'LI' => 'F97', 'LJ' => 'G97', 'LK' => 'C98', 'LL' => 'D98', 'LM' => 'E98', 'LN' => 'F98', 'LO' => 'G98', 'LP' => 'C99', 'LQ' => 'D99', 'LR' => 'E99', 'LS' => 'F99', 'LT' => 'G99', 'LU' => 'C100', 'LV' => 'D100', 'LW' => 'E100', 'LX' => 'F100', 'LY' => 'G100', 'LZ' => 'C106', 'MA' => 'D106', 'MB' => 'E106', 'MC' => 'F106', 'MD' => 'G106', 'ME' => 'C107', 'MF' => 'D107', 'MG' => 'E107', 'MH' => 'F107', 'MI' => 'G107', 'MJ' => 'C108', 'MK' => 'D108', 'ML' => 'E108', 'MM' => 'F108', 'MN' => 'G108', 'MO' => 'C109', 'MP' => 'D109', 'MQ' => 'E109', 'MR' => 'F109', 'MS' => 'G109', 'MT' => 'C110', 'MU' => 'D110', 'MV' => 'E110', 'MW' => 'F110', 'MX' => 'G110', 'MY' => 'C111', 'MZ' => 'D111', 'NA' => 'E111', 'NB' => 'F111', 'NC' => 'G111', 'ND' => 'C112', 'NE' => 'D112', 'NF' => 'E112', 'NG' => 'F112', 'NH' => 'G112', 'NI' => 'C113', 'NJ' => 'D113', 'NK' => 'E113', 'NL' => 'F113', 'NM' => 'G113', 'NN' => 'C114', 'NO' => 'D114', 'NP' => 'E114', 'NQ' => 'F114', 'NR' => 'G114', 'NS' => 'C115', 'NT' => 'D115', 'NU' => 'E115', 'NV' => 'F115', 'NW' => 'G115', 'NX' => 'C117', 'NY' => 'D117', 'NZ' => 'E117', 'OA' => 'F117', 'OB' => 'G117', 'OC' => 'C119', 'OD' => 'D119', 'OE' => 'E119', 'OF' => 'F119', 'OG' => 'G119', 'OH' => 'C126', 'OI' => 'D126', 'OJ' => 'E126', 'OK' => 'F126', 'OL' => 'G126', 'OM' => 'C127', 'ON' => 'D127', 'OO' => 'E127', 'OP' => 'F127', 'OQ' => 'G127', 'OR' => 'C129', 'OS' => 'D129', 'OT' => 'E129', 'OU' => 'F129', 'OV' => 'G129', 'OW' => 'C130', 'OX' => 'D130', 'OY' => 'E130', 'OZ' => 'F130', 'PA' => 'G130', 'PB' => 'C132', 'PC' => 'D132', 'PD' => 'E132', 'PE' => 'F132', 'PF' => 'G132', 'PG' => 'C133', 'PH' => 'D133', 'PI' => 'E133', 'PJ' => 'F133', 'PK' => 'G133', 'PL' => 'C134', 'PM' => 'D134', 'PN' => 'E134', 'PO' => 'F134', 'PP' => 'G134', 'PQ' => 'C135', 'PR' => 'D135', 'PS' => 'E135', 'PT' => 'F135', 'PU' => 'G135', 'PV' => 'C136', 'PW' => 'D136', 'PX' => 'E136', 'PY' => 'F136', 'PZ' => 'G136', 'QA' => 'C137', 'QB' => 'D137', 'QC' => 'E137', 'QD' => 'F137', 'QE' => 'G137', 'QF' => 'C138', 'QG' => 'D138', 'QH' => 'E138', 'QI' => 'F138', 'QJ' => 'G138', 'QK' => 'C139', 'QL' => 'D139', 'QM' => 'E139', 'QN' => 'F139', 'QO' => 'G139', 'QP' => 'C140', 'QQ' => 'D140', 'QR' => 'E140', 'QS' => 'F140', 'QT' => 'G140', 'QU' => 'C144', 'QV' => 'D144', 'QW' => 'E144', 'QX' => 'F144', 'QY' => 'G144', 'QZ' => 'C145', 'RA' => 'D145', 'RB' => 'E145', 'RC' => 'F145', 'RD' => 'G145', 'RE' => 'C146', 'RF' => 'D146', 'RG' => 'E146', 'RH' => 'F146', 'RI' => 'G146', 'RJ' => 'C147', 'RK' => 'D147', 'RL' => 'E147', 'RM' => 'F147', 'RN' => 'G147', 'RO' => 'C148', 'RP' => 'D148', 'RQ' => 'E148', 'RR' => 'F148', 'RS' => 'G148', 'RT' => 'C149', 'RU' => 'D149', 'RV' => 'E149', 'RW' => 'F149', 'RX' => 'G149', 'RY' => 'C153', 'RZ' => 'D153', 'SA' => 'E153', 'SB' => 'F153', 'SC' => 'G153', 'SD' => 'C154', 'SE' => 'D154', 'SF' => 'E154', 'SG' => 'F154', 'SH' => 'G154', 'SI' => 'C155', 'SJ' => 'D155', 'SK' => 'E155', 'SL' => 'F155', 'SM' => 'G155', 'SN' => 'C156', 'SO' => 'D156', 'SP' => 'E156', 'SQ' => 'F156', 'SR' => 'G156', 'SS' => 'C158', 'ST' => 'D158', 'SU' => 'E158', 'SV' => 'F158', 'SW' => 'G158', 'SX' => 'C159', 'SY' => 'D159', 'SZ' => 'E159', 'TA' => 'F159', 'TB' => 'G159', 'TC' => 'C160', 'TD' => 'D160', 'TE' => 'E160', 'TF' => 'F160', 'TG' => 'G160', 'TH' => 'C161', 'TI' => 'D161', 'TJ' => 'E161', 'TK' => 'F161', 'TL' => 'G161', 'TM' => 'C162', 'TN' => 'D162', 'TO' => 'E162', 'TP' => 'F162', 'TQ' => 'G162', 'TR' => 'C163', 'TS' => 'D163', 'TT' => 'E163', 'TU' => 'F163', 'TV' => 'G163', 'TW' => 'C166', 'TX' => 'D166', 'TY' => 'E166', 'TZ' => 'F166', 'UA' => 'G166', 'UB' => 'C167', 'UC' => 'D167', 'UD' => 'E167', 'UE' => 'F167', 'UF' => 'G167'
        );
        $cols = array('C10', 'C13', 'C14', 'C18', 'C19', 'C22', 'C26', 'C27', 'C28', 'C32', 'C33', 'C34', 'C35', 'C36', 'C38', 'C39', 'C43', 'C49', 'C50', 'C51', 'C55', 'C56', 'C58', 'C62', 'C63', 'C66', 'C67', 'C71', 'C78', 'C79', 'C80', 'C81', 'C82', 'C83', 'C84', 'C85', 'C86', 'C87', 'C88', 'C89', 'C90', 'C92', 'C93', 'C94', 'C97', 'C98', 'C99', 'C100', 'C106', 'C107', 'C108', 'C109', 'C110', 'C111', 'C112', 'C113', 'C114', 'C115', 'C117', 'C119', 'C126', 'C127', 'C129', 'C130', 'C132', 'C133', 'C134', 'C135', 'C136', 'C137', 'C138', 'C139', 'C140', 'C145', 'C146', 'C147', 'C148', 'C149', 'C153', 'C154', 'C155', 'C156', 'C158', 'C159', 'C160', 'C161', 'C162', 'C163', 'C166', 'C167');
        $new = array('BW', 'BX', 'BY', 'BZ', 'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CQ', 'CR', 'CS', 'CT', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ', 'DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DK', 'DL', 'DM', 'DN', 'DO', 'DP', 'DQ', 'DR', 'DS', 'DT', 'DU', 'DV', 'DW', 'DX', 'DY', 'DZ', 'EA', 'EB', 'EC', 'ED', 'EE', 'EF', 'EG', 'EH', 'EI', 'EJ', 'EK', 'EL', 'EM', 'EN', 'EO', 'EP', 'EQ', 'ER', 'ES', 'ET', 'EU', 'EV', 'EW', 'EX', 'EY', 'EZ', 'FA', 'FB', 'FC', 'FD', 'FE', 'FF', 'FG', 'FH');
        $hist_one = array('FI', 'FJ', 'FK', 'FL', 'FM', 'FN', 'FO', 'FP', 'FQ', 'FR', 'FS', 'FT', 'FU', 'FV', 'FW', 'FX', 'FY', 'FZ', 'GA', 'GB', 'GC', 'GD', 'GE', 'GF', 'GG', 'GH', 'GI', 'GJ', 'GK', 'GL', 'GM', 'GN', 'GO', 'GP', 'GQ', 'GR', 'GS', 'GT', 'GU', 'GV', 'GW', 'GX', 'GY', 'GZ', 'HA', 'HB', 'HC', 'HD', 'HE', 'HF', 'HG', 'HH', 'HI', 'HJ', 'HK', 'HL', 'HM', 'HN', 'HO', 'HP', 'HQ', 'HR', 'HS', 'HT', 'HU', 'HV', 'HW', 'HX', 'HY', 'HZ', 'IA', 'IB', 'IC', 'ID', 'IE', 'IF', 'IG', 'IH', 'II', 'IJ', 'IK', 'IL', 'IM', 'IN', 'IO', 'IP', 'IQ', 'IR', 'IS', 'IT');
        $hist_two = array('IU', 'IV', 'IW', 'IX', 'IY', 'IZ', 'JA', 'JB', 'JC', 'JD', 'JE', 'JF', 'JG', 'JH', 'JI', 'JJ', 'JK', 'JL', 'JM', 'JN', 'JO', 'JP', 'JQ', 'JR', 'JS', 'JT', 'JU', 'JV', 'JW', 'JX', 'JY', 'JZ', 'KA', 'KB', 'KC', 'KD', 'KE', 'KF', 'KG', 'KH', 'KI', 'KJ', 'KK', 'KL', 'KM', 'KN', 'KO', 'KP', 'KQ', 'KR', 'KS', 'KT', 'KU', 'KV', 'KW', 'KX', 'KY', 'KZ', 'LA', 'LB', 'LC', 'LD', 'LE', 'LF', 'LG', 'LH', 'LI', 'LJ', 'LK', 'LL', 'LM', 'LN', 'LO', 'LP', 'LQ', 'LR', 'LS', 'LT', 'LU', 'LV', 'LW', 'LX', 'LY', 'LZ', 'MA', 'MB', 'MC', 'MD', 'ME', 'MF');
        $proj_one = array('OO', 'OP', 'OQ', 'OR', 'OS', 'OT', 'OU', 'OV', 'OW', 'OX', 'OY', 'OZ', 'PA', 'PB', 'PC', 'PD', 'PE', 'PF', 'PG', 'PH', 'PI', 'PJ', 'PK', 'PL', 'PM', 'PN', 'PO', 'PP', 'PQ', 'PR', 'PS', 'PT', 'PU', 'PV', 'PW', 'PX', 'PY', 'PZ', 'QA', 'QB', 'QC', 'QD', 'QE', 'QF', 'QG', 'QH', 'QI', 'QJ', 'QK', 'QL', 'QM', 'QN', 'QO', 'QP', 'QQ', 'QR', 'QS', 'QT', 'QU', 'QV', 'QW', 'QX', 'QY', 'QZ', 'RA', 'RB', 'RC', 'RD', 'RE', 'RF', 'RG', 'RH', 'RI', 'RJ', 'RK', 'RL', 'RM', 'RN', 'RO', 'RP', 'RQ', 'RR', 'RS', 'RT', 'RU', 'RV', 'RW', 'RX', 'RY', 'RZ', 'SA', 'SB', 'SC', 'SD', 'SE', 'SF', 'SG', 'SH', 'SI', 'SJ', 'SK', 'SL', 'SM', 'SN', 'SO', 'SP', 'SQ', 'SR', 'SS', 'ST', 'SU', 'SV', 'SW', 'SX', 'SY', 'SZ', 'TA', 'TB', 'TC', 'TD', 'TE', 'TF', 'TG', 'TH', 'TI', 'TJ', 'TK', 'TL', 'TM', 'TN', 'TO', 'TP', 'TQ', 'TR', 'TS', 'TT', 'TU', 'TV', 'TW', 'TX', 'TY', 'TZ', 'UA', 'UB', 'UC', 'UD', 'UE', 'UF', 'UG', 'UH', 'UI', 'UJ', 'UK', 'UL', 'UM', 'UN', 'UO', 'UP', 'UQ', 'UR', 'US', 'UT', 'UU', 'UV', 'UW', 'UX', 'UY', 'UZ', 'VA', 'VB', 'VC', 'VD', 'VE', 'VF', 'VG', 'VH', 'VI', 'VJ', 'VK', 'VL');
        $proj_two = array('SA', 'SB', 'SC', 'SD', 'SE', 'SF', 'SG', 'SH', 'SI', 'SJ', 'SK', 'SL', 'SM', 'SN', 'SO', 'SP', 'SQ', 'SR', 'SS', 'ST', 'SU', 'SV', 'SW', 'SX', 'SY', 'SZ', 'TA', 'TB', 'TC', 'TD', 'TE', 'TF', 'TG', 'TH', 'TI', 'TJ', 'TK', 'TL', 'TM', 'TN', 'TO', 'TP', 'TQ', 'TR', 'TS', 'TT', 'TU', 'TV', 'TW', 'TX', 'TY', 'TZ', 'UA', 'UB', 'UC', 'UD', 'UE', 'UF', 'UG', 'UH', 'UI', 'UJ', 'UK', 'UL', 'UM', 'UN', 'UO', 'UP', 'UQ', 'UR', 'US', 'UT', 'UU', 'UV', 'UW', 'UX', 'UY', 'UZ', 'VA', 'VB', 'VC', 'VD', 'VE', 'VF', 'VG', 'VH', 'VI', 'VJ', 'VK', 'VL');
//        foreach ($data as $col=>$value){
//            if($col=="SA"){
//                $start=TRUE;
//            }
//            if($col=="VL"){
//                $start=FALSE;
//            }
//            if($start){
//                $string.= "'".$col."',";
//                $count++;
//            }
//        }
////        echo "FH"." => ".$data["FH"]."<br>";
////        echo ++$count;
//        echo $string;die;
        $s = "";
        for ($i = 0; $i <= 91; $i++) {
            $cols[$i][0] = "G";
            $s.="'" . $proj_two[$i] . "'=>'" . $cols[$i] . "',";
        }
        echo $s;
        die;
        $final = array('BW' => 'C10', 'BX' => 'C13', 'BY' => 'C14', 'BZ' => 'C18', 'CA' => 'C19', 'CB' => 'C22', 'CC' => 'C26', 'CD' => 'C27', 'CE' => 'C28', 'CF' => 'C32', 'CG' => 'C33', 'CH' => 'C34', 'CI' => 'C35', 'CJ' => 'C36', 'CK' => 'C38', 'CL' => 'C39', 'CM' => 'C43', 'CN' => 'C49', 'CO' => 'C50', 'CP' => 'C51', 'CQ' => 'C55', 'CR' => 'C56', 'CS' => 'C58', 'CT' => 'C62', 'CU' => 'C63', 'CV' => 'C66', 'CW' => 'C67', 'CX' => 'C71', 'CY' => 'C78', 'CZ' => 'C79', 'DA' => 'C80', 'DB' => 'C81', 'DC' => 'C82', 'DD' => 'C83', 'DE' => 'C84', 'DF' => 'C85', 'DG' => 'C86', 'DH' => 'C87', 'DI' => 'C88', 'DJ' => 'C89', 'DK' => 'C90', 'DL' => 'C92', 'DM' => 'C93', 'DN' => 'C94', 'DO' => 'C97', 'DP' => 'C98', 'DQ' => 'C99', 'DR' => 'C100', 'DS' => 'C106', 'DT' => 'C107', 'DU' => 'C108', 'DV' => 'C109', 'DW' => 'C110', 'DX' => 'C111', 'DY' => 'C112', 'DZ' => 'C113', 'EA' => 'C114', 'EB' => 'C115', 'EC' => 'C117', 'ED' => 'C119', 'EE' => 'C126', 'EF' => 'C127', 'EG' => 'C129', 'EH' => 'C130', 'EI' => 'C132', 'EJ' => 'C133', 'EK' => 'C134', 'EL' => 'C135', 'EM' => 'C136', 'EN' => 'C137', 'EO' => 'C138', 'EP' => 'C139', 'EQ' => 'C140',
            'ER' => 'C145', 'ES' => 'C146', 'ET' => 'C147', 'EU' => 'C148', 'EV' => 'C149', 'EW' => 'C153', 'EX' => 'C154', 'EY' => 'C155', 'EZ' => 'C156', 'FA' => 'C158', 'FB' => 'C159', 'FC' => 'C160', 'FD' => 'C161', 'FE' => 'C162', 'FF' => 'C163', 'FG' => 'C166', 'FH' => 'C167');
        $s = "";
        foreach ($final as $key => $value) {
            $value[0] = "E";
            $s.="'" . $key . "'=>'" . $value . "',";
        }
        echo $s;
        $final_base = array('BW' => 'E10', 'BX' => 'E13', 'BY' => 'E14', 'BZ' => 'E18', 'CA' => 'E19', 'CB' => 'E22', 'CC' => 'E26', 'CD' => 'E27', 'CE' => 'E28', 'CF' => 'E32', 'CG' => 'E33', 'CH' => 'E34', 'CI' => 'E35', 'CJ' => 'E36', 'CK' => 'E38', 'CL' => 'E39', 'CM' => 'E43', 'CN' => 'E49', 'CO' => 'E50', 'CP' => 'E51', 'CQ' => 'E55', 'CR' => 'E56', 'CS' => 'E58', 'CT' => 'E62', 'CU' => 'E63', 'CV' => 'E66', 'CW' => 'E67', 'CX' => 'E71', 'CY' => 'E78', 'CZ' => 'E79', 'DA' => 'E80', 'DB' => 'E81', 'DC' => 'E82', 'DD' => 'E83', 'DE' => 'E84', 'DF' => 'E85', 'DG' => 'E86', 'DH' => 'E87', 'DI' => 'E88', 'DJ' => 'E89', 'DK' => 'E90', 'DL' => 'E92', 'DM' => 'E93', 'DN' => 'E94', 'DO' => 'E97', 'DP' => 'E98', 'DQ' => 'E99', 'DR' => 'E100', 'DS' => 'E106', 'DT' => 'E107', 'DU' => 'E108', 'DV' => 'E109', 'DW' => 'E110', 'DX' => 'E111', 'DY' => 'E112', 'DZ' => 'E113', 'EA' => 'E114', 'EB' => 'E115', 'EC' => 'E117', 'ED' => 'E119', 'EE' => 'E126', 'EF' => 'E127', 'EG' => 'E129', 'EH' => 'E130', 'EI' => 'E132', 'EJ' => 'E133', 'EK' => 'E134', 'EL' => 'E135', 'EM' => 'E136', 'EN' => 'E137', 'EO' => 'E138', 'EP' => 'E139', 'EQ' => 'E140', 'ER' => 'E145', 'ES' => 'E146', 'ET' => 'E147', 'EU' => 'E148', 'EV' => 'E149', 'EW' => 'E153', 'EX' => 'E154', 'EY' => 'E155', 'EZ' => 'E156', 'FA' => 'E158', 'FB' => 'E159', 'FC' => 'E160', 'FD' => 'E161', 'FE' => 'E162', 'FF' => 'E163', 'FG' => 'E166', 'FH' => 'E167');
        $hist_one_final = array('FI' => 'C10', 'FJ' => 'C13', 'FK' => 'C14', 'FL' => 'C18', 'FM' => 'C19', 'FN' => 'C22', 'FO' => 'C26', 'FP' => 'C27', 'FQ' => 'C28', 'FR' => 'C32', 'FS' => 'C33', 'FT' => 'C34', 'FU' => 'C35', 'FV' => 'C36', 'FW' => 'C38', 'FX' => 'C39', 'FY' => 'C43', 'FZ' => 'C49', 'GA' => 'C50', 'GB' => 'C51', 'GC' => 'C55', 'GD' => 'C56', 'GE' => 'C58', 'GF' => 'C62', 'GG' => 'C63', 'GH' => 'C66', 'GI' => 'C67', 'GJ' => 'C71', 'GK' => 'C78', 'GL' => 'C79', 'GM' => 'C80', 'GN' => 'C81', 'GO' => 'C82', 'GP' => 'C83', 'GQ' => 'C84', 'GR' => 'C85', 'GS' => 'C86', 'GT' => 'C87', 'GU' => 'C88', 'GV' => 'C89', 'GW' => 'C90', 'GX' => 'C92', 'GY' => 'C93', 'GZ' => 'C94', 'HA' => 'C97', 'HB' => 'C98', 'HC' => 'C99', 'HD' => 'C100', 'HE' => 'C106', 'HF' => 'C107', 'HG' => 'C108', 'HH' => 'C109', 'HI' => 'C110', 'HJ' => 'C111', 'HK' => 'C112', 'HL' => 'C113', 'HM' => 'C114', 'HN' => 'C115', 'HO' => 'C117', 'HP' => 'C119', 'HQ' => 'C126', 'HR' => 'C127', 'HS' => 'C129', 'HT' => 'C130', 'HU' => 'C132', 'HV' => 'C133', 'HW' => 'C134', 'HX' => 'C135', 'HY' => 'C136', 'HZ' => 'C137', 'IA' => 'C138', 'IB' => 'C139', 'IC' => 'C140', 'ID' => 'C145', 'IE' => 'C146', 'IF' => 'C147', 'IG' => 'C148', 'IH' => 'C149', 'II' => 'C153', 'IJ' => 'C154', 'IK' => 'C155', 'IL' => 'C156', 'IM' => 'C158', 'IN' => 'C159', 'IO' => 'C160', 'IP' => 'C161', 'IQ' => 'C162', 'IR' => 'C163', 'IS' => 'C166', 'IT' => 'C167');
        $hist_two_final = array('IU' => 'D10', 'IV' => 'D13', 'IW' => 'D14', 'IX' => 'D18', 'IY' => 'D19', 'IZ' => 'D22', 'JA' => 'D26', 'JB' => 'D27', 'JC' => 'D28', 'JD' => 'D32', 'JE' => 'D33', 'JF' => 'D34', 'JG' => 'D35', 'JH' => 'D36', 'JI' => 'D38', 'JJ' => 'D39', 'JK' => 'D43', 'JL' => 'D49', 'JM' => 'D50', 'JN' => 'D51', 'JO' => 'D55', 'JP' => 'D56', 'JQ' => 'D58', 'JR' => 'D62', 'JS' => 'D63', 'JT' => 'D66', 'JU' => 'D67', 'JV' => 'D71', 'JW' => 'D78', 'JX' => 'D79', 'JY' => 'D80', 'JZ' => 'D81', 'KA' => 'D82', 'KB' => 'D83', 'KC' => 'D84', 'KD' => 'D85', 'KE' => 'D86', 'KF' => 'D87', 'KG' => 'D88', 'KH' => 'D89', 'KI' => 'D90', 'KJ' => 'D92', 'KK' => 'D93', 'KL' => 'D94', 'KM' => 'D97', 'KN' => 'D98', 'KO' => 'D99', 'KP' => 'D100', 'KQ' => 'D106', 'KR' => 'D107', 'KS' => 'D108', 'KT' => 'D109', 'KU' => 'D110', 'KV' => 'D111', 'KW' => 'D112', 'KX' => 'D113', 'KY' => 'D114', 'KZ' => 'D115', 'LA' => 'D117', 'LB' => 'D119', 'LC' => 'D126', 'LD' => 'D127', 'LE' => 'D129', 'LF' => 'D130', 'LG' => 'D132', 'LH' => 'D133', 'LI' => 'D134', 'LJ' => 'D135', 'LK' => 'D136', 'LL' => 'D137', 'LM' => 'D138', 'LN' => 'D139', 'LO' => 'D140', 'LP' => 'D145', 'LQ' => 'D146', 'LR' => 'D147', 'LS' => 'D148', 'LT' => 'D149', 'LU' => 'D153', 'LV' => 'D154', 'LW' => 'D155', 'LX' => 'D156', 'LY' => 'D158', 'LZ' => 'D159', 'MA' => 'D160', 'MB' => 'D161', 'MC' => 'D162', 'MD' => 'D163', 'ME' => 'D166', 'MF' => 'D167');
        $proj_one_final = array('OO' => 'F10', 'OP' => 'F13', 'OQ' => 'F14', 'OR' => 'F18', 'OS' => 'F19', 'OT' => 'F22', 'OU' => 'F26', 'OV' => 'F27', 'OW' => 'F28', 'OX' => 'F32', 'OY' => 'F33', 'OZ' => 'F34', 'PA' => 'F35', 'PB' => 'F36', 'PC' => 'F38', 'PD' => 'F39', 'PE' => 'F43', 'PF' => 'F49', 'PG' => 'F50', 'PH' => 'F51', 'PI' => 'F55', 'PJ' => 'F56', 'PK' => 'F58', 'PL' => 'F62', 'PM' => 'F63', 'PN' => 'F66', 'PO' => 'F67', 'PP' => 'F71', 'PQ' => 'F78', 'PR' => 'F79', 'PS' => 'F80', 'PT' => 'F81', 'PU' => 'F82', 'PV' => 'F83', 'PW' => 'F84', 'PX' => 'F85', 'PY' => 'F86', 'PZ' => 'F87', 'QA' => 'F88', 'QB' => 'F89', 'QC' => 'F90', 'QD' => 'F92', 'QE' => 'F93', 'QF' => 'F94', 'QG' => 'F97', 'QH' => 'F98', 'QI' => 'F99', 'QJ' => 'F100', 'QK' => 'F106', 'QL' => 'F107', 'QM' => 'F108', 'QN' => 'F109', 'QO' => 'F110', 'QP' => 'F111', 'QQ' => 'F112', 'QR' => 'F113', 'QS' => 'F114', 'QT' => 'F115', 'QU' => 'F117', 'QV' => 'F119', 'QW' => 'F126', 'QX' => 'F127', 'QY' => 'F129', 'QZ' => 'F130', 'RA' => 'F132', 'RB' => 'F133', 'RC' => 'F134', 'RD' => 'F135', 'RE' => 'F136', 'RF' => 'F137', 'RG' => 'F138', 'RH' => 'F139', 'RI' => 'F140', 'RJ' => 'F145', 'RK' => 'F146', 'RL' => 'F147', 'RM' => 'F148', 'RN' => 'F149', 'RO' => 'F153', 'RP' => 'F154', 'RQ' => 'F155', 'RR' => 'F156', 'RS' => 'F158', 'RT' => 'F159', 'RU' => 'F160', 'RV' => 'F161', 'RW' => 'F162', 'RX' => 'F163', 'RY' => 'F166', 'RZ' => 'F167');
        $proj_two_final = array('SA' => 'G10', 'SB' => 'G13', 'SC' => 'G14', 'SD' => 'G18', 'SE' => 'G19', 'SF' => 'G22', 'SG' => 'G26', 'SH' => 'G27', 'SI' => 'G28', 'SJ' => 'G32', 'SK' => 'G33', 'SL' => 'G34', 'SM' => 'G35', 'SN' => 'G36', 'SO' => 'G38', 'SP' => 'G39', 'SQ' => 'G43', 'SR' => 'G49', 'SS' => 'G50', 'ST' => 'G51', 'SU' => 'G55', 'SV' => 'G56', 'SW' => 'G58', 'SX' => 'G62', 'SY' => 'G63', 'SZ' => 'G66', 'TA' => 'G67', 'TB' => 'G71', 'TC' => 'G78', 'TD' => 'G79', 'TE' => 'G80', 'TF' => 'G81', 'TG' => 'G82', 'TH' => 'G83', 'TI' => 'G84', 'TJ' => 'G85', 'TK' => 'G86', 'TL' => 'G87', 'TM' => 'G88', 'TN' => 'G89', 'TO' => 'G90', 'TP' => 'G92', 'TQ' => 'G93', 'TR' => 'G94', 'TS' => 'G97', 'TT' => 'G98', 'TU' => 'G99', 'TV' => 'G100', 'TW' => 'G106', 'TX' => 'G107', 'TY' => 'G108', 'TZ' => 'G109', 'UA' => 'G110', 'UB' => 'G111', 'UC' => 'G112', 'UD' => 'G113', 'UE' => 'G114', 'UF' => 'G115', 'UG' => 'G117', 'UH' => 'G119', 'UI' => 'G126', 'UJ' => 'G127', 'UK' => 'G129', 'UL' => 'G130', 'UM' => 'G132', 'UN' => 'G133', 'UO' => 'G134', 'UP' => 'G135', 'UQ' => 'G136', 'UR' => 'G137', 'US' => 'G138', 'UT' => 'G139', 'UU' => 'G140', 'UV' => 'G145', 'UW' => 'G146', 'UX' => 'G147', 'UY' => 'G148', 'UZ' => 'G149', 'VA' => 'G153', 'VB' => 'G154', 'VC' => 'G155', 'VD' => 'G156', 'VE' => 'G158', 'VF' => 'G159', 'VG' => 'G160', 'VH' => 'G161', 'VI' => 'G162', 'VJ' => 'G163', 'VK' => 'G166', 'VL' => 'G167');
//        echo count($cols);
//        echo count($new);
        die;
    }

    function map_info($info, $errors = "") {
        $info_keys = ['C6', 'C9', 'C10', 'C11', 'E7'];
        $orig = [];
        foreach ($info_keys as $key) {
            $orig[$key] = $info[$key];
            $info[$key] = $this->Bulk_model->map_code(strtolower($key), $info[$key]);
            if (isset($orig[$key]) && trim($orig[$key]) != "" && $info[$key] === NULL) {
//                $errors.="<p>".$this->lang->line("borrower_information_$key")." ".$this->lang->line("out_of_catalogue")."</p>";
                $this->errors["info"][$key].="<p>" . $this->lang->line("borrower_information_$key") . " " . $this->lang->line("out_of_catalogue") . "</p>";
            }
        }
//        return ["info"=>$info,"errors"=>$errors];
        return $info;
    }

    function map_fin($fin, $errors = "") {
        $fin_catalogue = $this->fin_catalogue();
        $orig = [];
        $fin_keys_all = ['E3', 'E4', 'G8', 'C10', 'D10', 'E10', 'C13', 'D13', 'E13', 'F13', 'G13'];
        $fin_keys = ['E3', 'E4', 'G8'];
        $fin_keys2 = ['C10', 'D10', 'E10', 'C13', 'D13', 'E13', 'F13', 'G13'];
        $labels = ['C' => $this->lang->line("hist2"), 'D' => $this->lang->line("hist1"), 'E' => $this->lang->line("base_year"),
            'F' => $this->lang->line("proj1"), 'G' => $this->lang->line("proj2"),];

        foreach ($fin_keys_all as $key) {
            $orig[$key] = $fin[$key];
        }

        $fin['E3'] = $this->check_cat_empty("E3", $fin['E3']);
        $fin['E4'] = $this->check_cat_empty("E4", $fin['E4']);
        $fin['G8'] = $this->check_cat_empty("G8", $fin['G8']);

        $fin['C10'] = $this->check_cat_empty("C10", $fin['C10']);
        $fin['D10'] = $this->check_cat_empty("D10", $fin['D10']);
        $fin['E10'] = $this->check_cat_empty("E10", $fin['E10']);
//        $fin['F10'] = $this->check_cat_empty("F10", $fin['F10']);
//        $fin['G10'] = $this->check_cat_empty("G10", $fin['G10']);

        $fin['C13'] = $fin['C13'] != "" && $fin['C13'] != NULL ? $fin['C13'] : 1;
        $fin['D13'] = $fin['D13'] != "" && $fin['D13'] != NULL ? $fin['D13'] : 1;
        $fin['E13'] = $fin['E13'] != "" && $fin['E13'] != NULL ? $fin['E13'] : 1;
        $fin['F13'] = $fin['F13'] != "" && $fin['F13'] != NULL ? $fin['F13'] : 1;
        $fin['G13'] = $fin['G13'] != "" && $fin['G13'] != NULL ? $fin['G13'] : 1;

        $fin['C13'] = $this->check_cat_empty("C13", $fin['C13']);
        $fin['D13'] = $this->check_cat_empty("D13", $fin['D13']);
        $fin['E13'] = $this->check_cat_empty("E13", $fin['E13']);
        $fin['F13'] = $this->check_cat_empty("F13", $fin['F13']);
        $fin['G13'] = $this->check_cat_empty("G13", $fin['G13']);

        foreach ($fin_keys as $key) {
            if (isset($orig[$key]) && trim($orig[$key]) != "" && $fin[$key] === NULL) {
                $l = " " . $labels[substr($key, 0, 1)];
                $this->errors['fin'][$key].="<p>" . $this->lang->line("borrower_financial_$key") . $l . " " . $this->lang->line("out_of_catalogue") . "</p>";
            }
        }
        foreach ($fin_keys2 as $key) {
            if (isset($orig[$key]) && trim($orig[$key]) != "" && $fin[$key] === NULL) {
                $l = " " . $labels[substr($key, 0, 1)];
                $this->errors['fin'][$key].="<p>" . $this->lang->line("borrower_financial_" . substr($key, 1)) . $l . " " . $this->lang->line("out_of_catalogue") . "</p>";
            }
        }

//        $fin['E3'] = $fin_catalogue['E3'][$fin['E3']];
//        $fin['E4'] = $fin_catalogue['E4'][$fin['E4']];
//        $fin['G8'] = $fin_catalogue['G8'][$fin['G8']];
//        
//        $fin['C10'] = $fin_catalogue['C10'][$fin['C10']];
//        $fin['D10'] = $fin_catalogue['D10'][$fin['D10']];
//        $fin['E10'] = $fin_catalogue['E10'][$fin['E10']];
//        $fin['F10'] = $fin_catalogue['F10'][$fin['F10']];
//        $fin['G10'] = $fin_catalogue['G10'][$fin['G10']];
//        
//        $fin['C13'] = $fin_catalogue['C13'][$fin['C13']];
//        $fin['D13'] = $fin_catalogue['D13'][$fin['D13']];
//        $fin['E13'] = $fin_catalogue['E13'][$fin['E13']];
//        $fin['F13'] = $fin_catalogue['F13'][$fin['F13']];
//        $fin['G13'] = $fin_catalogue['G13'][$fin['G13']];

        return $fin;
    }

    function check_cat_empty($col, $value) {
        $fin_catalogue = $this->fin_catalogue();
        if ($value != "" && $value != NULL && !empty($fin_catalogue[$col][$value])) {
            $value = $fin_catalogue[$col][$value];
        } else {
            $value = NULL;
        }
        return $value;
    }

    function map_assess($assess, $errors = "") {
        $assess_labels = array('P' => 'F5', 'Q' => 'F10', 'R' => 'F18', 'S' => 'F22', 'T' => 'F29', 'U' => 'F36', 'V' => 'F40', 'W' => 'F45', 'X' => 'F50', 'Y' => 'F56', 'Z' => 'F66', 'AA' => 'F71', 'AB' => 'F75', 'AC' => 'F79', 'AD' => 'F86', 'AE' => 'F93', 'AF' => 'F100', 'AG' => 'F107', 'AH' => 'F115', 'AI' => 'F121', 'AJ' => 'F125', 'AK' => 'F129', 'AL' => 'F133', 'AM' => 'F137', 'AN' => 'F144', 'AO' => 'F151', 'AP' => 'F155', 'AQ' => 'F159', 'AR' => 'F165', 'AS' => 'F169', 'AT' => 'F175', 'AU' => 'F179', 'AV' => 'F186', 'AW' => 'F194', 'AX' => 'F200', 'AY' => 'F208', 'AZ' => 'F216');
        $orig = [];
        foreach ($assess_labels as $col => $label) {
            $orig[$label] = $assess[$label];
            $assess[$label] = $this->Bulk_model->map_code(strtolower($label), $assess[$label]);
            if (isset($orig[$label]) && trim($orig[$label]) != "" && $assess[$label] === NULL) {
                $this->errors['assess'][$label].="<p>" . $this->lang->line("borrower_assessment_$label") . " " . $this->lang->line("out_of_catalogue") . "</p>";
            }
        }
        return $assess;
    }

    function fin_catalogue() {
        $fin_catalogue = array(
            "E3" => array(
                "1" => "yes",
                "2" => "no"
            ),
            "E4" => array(
                "1" => "yes",
                "2" => "no"
            ),
            "G8" => array(
                "1" => "Actuals",
                "2" => "Thousands",
                "3" => "Millions",
            ),
            "C10" => array(
                "1" => "Audited",
                "2" => "Provisional"
            ),
            "D10" => array(
                "1" => "Audited",
                "2" => "Provisional"
            ),
            "E10" => array(
                "1" => "Audited",
                "2" => "Provisional"
            ),
            "F10" => array(
                "1" => "Audited",
                "2" => "Provisional"
            ),
            "G10" => array(
                "1" => "Audited",
                "2" => "Provisional"
            ),
            "C13" => array(
                "1" => 3,
                "2" => 6,
                "3" => 9,
                "4" => 12
            ),
            "D13" => array(
                "1" => 3,
                "2" => 6,
                "3" => 9,
                "4" => 12
            ),
            "E13" => array(
                "1" => 3,
                "2" => 6,
                "3" => 9,
                "4" => 12
            ),
            "F13" => array(
                "1" => 3,
                "2" => 6,
                "3" => 9,
                "4" => 12
            ),
            "G13" => array(
                "1" => 3,
                "2" => 6,
                "3" => 9,
                "4" => 12
            )
        );
        return $fin_catalogue;
    }

//    function insert(){
//         $assess_labels = array('P' => 'F5', 'Q' => 'F10', 'R' => 'F18', 'S' => 'F22', 'T' => 'F29', 'U' => 'F36', 'V' => 'F40', 'W' => 'F45', 'X' => 'F50', 'Y' => 'F56', 'Z' => 'F66', 'AA' => 'F71', 'AB' => 'F75', 'AC' => 'F79', 'AD' => 'F86', 'AE' => 'F93', 'AF' => 'F100', 'AG' => 'F107', 'AH' => 'F115', 'AI' => 'F121', 'AJ' => 'F125', 'AK' => 'F129', 'AL' => 'F133', 'AM' => 'F137', 'AN' => 'F144', 'AO' => 'F151', 'AP' => 'F155', 'AQ' => 'F159', 'AR' => 'F165', 'AS' => 'F169', 'AT' => 'F175', 'AU' => 'F179', 'AV' => 'F186', 'AW' => 'F194', 'AX' => 'F200', 'AY' => 'F208', 'AZ' => 'F216');
//        foreach ($assess_labels as $col => $label) {
//           $data=array("code"=>1,"value"=>$label);
//           $data= increment(strtolower($label), $data);
//           $this->db->insert(strtolower($label),$data);
//        }
//        echo "DOne!";
//    }
    //    BEGIN UNMAP
    function unmap_info($info) {
        $info['C6'] = $this->Bulk_model->unmap_code("c6", $info['C6']);
        $info['C9'] = $this->Bulk_model->unmap_code("c9", $info['C9']);
        $info['C10'] = $this->Bulk_model->unmap_code("c10", $info['C10']);
        $info['C11'] = $this->Bulk_model->unmap_code("c11", $info['C11']);
        $info['E7'] = $this->Bulk_model->unmap_code("e7", $info['E7']);
        return $info;
    }

    function unmap_fin($fin) {
        $fin_catalogue = $this->fin_catalogue();

        $fin['E3'] = $this->check_cat_empty("E3", $fin['E3']);
        $fin['E4'] = $this->check_cat_empty("E4", $fin['E4']);
        $fin['G8'] = $this->check_cat_empty("G8", $fin['G8']);

        $fin['C10'] = $this->check_cat_empty("C10", $fin['C10']);
        $fin['D10'] = $this->check_cat_empty("D10", $fin['D10']);
        $fin['E10'] = $this->check_cat_empty("E10", $fin['E10']);
        $fin['F10'] = $this->check_cat_empty("F10", $fin['F10']);
        $fin['G10'] = $this->check_cat_empty("G10", $fin['G10']);

        $fin['C13'] = $fin['C13'] != "" && $fin['C13'] != NULL ? $fin['C13'] : 1;
        $fin['D13'] = $fin['D13'] != "" && $fin['D13'] != NULL ? $fin['D13'] : 1;
        $fin['E13'] = $fin['E13'] != "" && $fin['E13'] != NULL ? $fin['E13'] : 1;
        $fin['F13'] = $fin['F13'] != "" && $fin['F13'] != NULL ? $fin['F13'] : 1;
        $fin['G13'] = $fin['G13'] != "" && $fin['G13'] != NULL ? $fin['G13'] : 1;

        $fin['C13'] = $this->check_cat_empty("C13", $fin['C13']);
        $fin['D13'] = $this->check_cat_empty("D13", $fin['D13']);
        $fin['E13'] = $this->check_cat_empty("E13", $fin['E13']);
        $fin['F13'] = $this->check_cat_empty("F13", $fin['F13']);
        $fin['G13'] = $this->check_cat_empty("G13", $fin['G13']);

//        $fin['E3'] = $fin_catalogue['E3'][$fin['E3']];
//        $fin['E4'] = $fin_catalogue['E4'][$fin['E4']];
//        $fin['G8'] = $fin_catalogue['G8'][$fin['G8']];
//        
//        $fin['C10'] = $fin_catalogue['C10'][$fin['C10']];
//        $fin['D10'] = $fin_catalogue['D10'][$fin['D10']];
//        $fin['E10'] = $fin_catalogue['E10'][$fin['E10']];
//        $fin['F10'] = $fin_catalogue['F10'][$fin['F10']];
//        $fin['G10'] = $fin_catalogue['G10'][$fin['G10']];
//        
//        $fin['C13'] = $fin_catalogue['C13'][$fin['C13']];
//        $fin['D13'] = $fin_catalogue['D13'][$fin['D13']];
//        $fin['E13'] = $fin_catalogue['E13'][$fin['E13']];
//        $fin['F13'] = $fin_catalogue['F13'][$fin['F13']];
//        $fin['G13'] = $fin_catalogue['G13'][$fin['G13']];

        return $fin;
    }

    function unmap_assess($assess) {
        $assess_labels = array('P' => 'F5', 'Q' => 'F10', 'R' => 'F18', 'S' => 'F22', 'T' => 'F29', 'U' => 'F36', 'V' => 'F40', 'W' => 'F45', 'X' => 'F50', 'Y' => 'F56', 'Z' => 'F66', 'AA' => 'F71', 'AB' => 'F75', 'AC' => 'F79', 'AD' => 'F86', 'AE' => 'F93', 'AF' => 'F100', 'AG' => 'F107', 'AH' => 'F115', 'AI' => 'F121', 'AJ' => 'F125', 'AK' => 'F129', 'AL' => 'F133', 'AM' => 'F137', 'AN' => 'F144', 'AO' => 'F151', 'AP' => 'F155', 'AQ' => 'F159', 'AR' => 'F165', 'AS' => 'F169', 'AT' => 'F175', 'AU' => 'F179', 'AV' => 'F186', 'AW' => 'F194', 'AX' => 'F200', 'AY' => 'F208', 'AZ' => 'F216');
        foreach ($assess_labels as $col => $label) {
            $assess[$label] = $this->Bulk_model->unmap_code(strtolower($label), $assess[$label]);
        }
        return $assess;
    }

//    END UNMAP
    function approve_case($case_id, $file_name) {
        $case = $this->db->get_where('cases', array('id' => $case_id))->row();
        //update case data to approved and complete
        $update = array(
            'approved' => 1,
            'date_approved' => date("Y-m-d H:i:s"),
            'approved_by' => $this->session->userdata("user_id")
        );
        
        $this->Crud_model->update("cases", $case_id, $update);
        /////////
        //generate excel
        //   $excel_genertaed = $this->generate_excel($case_id);
        //   if (!$excel_genertaed[0]) {
        //       return $excel_genertaed[1];
        //   }
        //if (non_subscriber_bank($case_id)) {
        //    $update['date_completed'] = date('Y-m-d H:i:s');
        //    $update['completed'] = 1;
        //}
        //  $this->Crud_model->update("cases", $case_id, $update);
        // audit
        //  $case_name = $this->get_case_name($case_id);
        save_audit("bulk csv : Case marked as approved -- " . $case->name . " From File : " . $file_name);
        
        //  $this->send_notification($case_id);
        return TRUE;
    }

    function generate_excel($case_id) {
        $this->load->model("admin/Cases_model");
        $case_id = sanitize($case_id);

        $case = $this->Cases_model->get_case($case_id);

        ini_set('memory_limit', '2048M');
        set_time_limit(1800);
        
        $objPHPexcel = \PhpOffice\PhpSpreadsheet\IOFactory::load('./template/template.xlsx');
        $sheetname = array(
            'Read Me',
            'Definition Sheet',
            'Completion Check',
            'Borrower Information',
            "Borrower Factsheet",
            "Borrower Financials",
            "Borrower Assessment",
//            "Master"
        );
        //get cell color
        //$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->getRGB();
//       BEGIN BORROWER INFO
        $objWorksheet = $objPHPexcel->setActiveSheetIndex(3);
        $borrower_info = $this->Cases_model->get_borrower_information($case_id);
        foreach ($borrower_info as $key => $value) {
            if ($key == "id" || $key == "case_id" || $key == "turnover_amount" || $key == "bank_amount") {
                continue;
            }
            if ($key == "C7" || $key == "C8") {
                if (!empty($value) && $value != "") {
                    $objWorksheet->setCellValueExplicit($key, $value, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_NUMERIC);
                } else {
                    $objWorksheet->getCell($key)->setValue("");
                }
                continue;
            }
            if ($key == "C17") {
                $value = $case->branch_code;
            }
            if ($key == "C19") {
                $value = $case->first_name . " " . $case->last_name;
            }
            if ($key == "C20") {
                $value = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel(date("j-M-y", strtotime($case->date_created)));
            }
            if ($key == "C21") {
                $value = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel(date("j-M-y", strtotime($case->date_approved)));
            }
            if ($key == "C36") {
                $value = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value);
            }
            if ($key == "C13") {
                $value = $value / 100;
            }
            if ($key == 'C6' || $key == 'C9' || $key == 'C10' || $key == 'C11' || $key == 'E7') {
                $value = get_value_from_code(strtolower($key), $value);
            }
            $objWorksheet->getCell($key)->setValue($value);
        }
// END BORROWER INFO
//       BEGIN BORROWER FACTSHEET
        $objWorksheet = $objPHPexcel->setActiveSheetIndex(4);
        $borrower_factsheet = $this->Cases_model->get_borrower_factsheet($case_id);
        foreach ($borrower_factsheet as $rows) {
            $row = 0;
            foreach ($rows as $key => $value) {
                if ($key == "id" || $key == "case_id" || ($row == 4 && $key == "D") || ($row == 5 && $key == "D") || ($row == 5 && $key == "E")) {
                    continue;
                }
                if ($key == "row") {
                    $row = $value;
                    continue;
                }
                $objWorksheet->getCell($key . $row)->setValue($value);
            }
        }
// END BORROWER FACTSHEET
//       BEGIN BORROWER FINANCIALS
        $objWorksheet = $objPHPexcel->setActiveSheetIndex(5);
        $borrower_financials = $this->Cases_model->get_borrower_financials($case_id);
        $projected = "yes";
        foreach ($borrower_financials as $rows) {
            $row = 0;
            foreach ($rows as $key => $value) {
                if ($key == "id" || $key == "case_id") {
                    continue;
                }
                if ($key == "row") {
                    $row = $value;
                    continue;
                }
                if ($row == 3 && $key == "E") {
                    if ($value == "no") {
                        $value = "No";
                    }
                    if ($value == "yes") {
                        $value = "Yes";
                    }
                }
                if ($row == 4 && $key == "E") {
//                    $projected=$value;
                    if ($value == "no") {
                        $value = "No";
                    }
                    if ($value == "yes") {
                        $value = "Yes";
                    }
//                    $value="Yes";
                }
                if ($row == 8 && $key == "F") {
                    continue;
                }
                if ($row == 11 && $key == "F") {
                    $objWorksheet->setCellValue($key . $row, '=IFERROR($G$9+1,"-")');
                    continue;
                }
                if ($row == 11 && $key == "G") {
                    $objWorksheet->setCellValue($key . $row, '=IFERROR($G$9+2,"-")');
                    continue;
                }
                if ($row == 12) {
                    if ($key == "E") {
                        $objWorksheet->setCellValue($key . $row, "=IF(ISBLANK('Borrower Information'!C36)," . '"-"' . ",'Borrower Information'!C36)");
                    }
                    if ($key == "C") {
                        $objWorksheet->setCellValue($key . $row, '=IF(E12="-","-",DATE(YEAR($E$12)-2,MONTH($E$12),DAY($E$12)))');
                    }
                    if ($key == "D") {
                        $objWorksheet->setCellValue($key . $row, '=IF(E12="-","-",DATE(YEAR($E$12)-1,MONTH($E$12),DAY($E$12)))');
                    }
                    if ($key == "F") {
                        $objWorksheet->setCellValue($key . $row, '=IF(E12="-","-",DATE(YEAR($E$12)+1,MONTH($E$12),DAY($E$12)))');
                    }
                    if ($key == "G") {
                        $objWorksheet->setCellValue($key . $row, '=IF(E12="-","-",DATE(YEAR($E$12)+2,MONTH($E$12),DAY($E$12)))');
                    }
                    continue;
                }
                if ($row > 15 && $projected == "no" && ($key == "F" || $key == "G")) {
                    continue;
                }
                $objWorksheet->getCell($key . $row)->setValue($value);
            }
        }
//// END BORROWER FINANCIALS
////       BEGIN BORROWER ASSESSMENT
        $objWorksheet = $objPHPexcel->setActiveSheetIndex(6);
        $borrower_assessment = $this->Cases_model->get_borrower_assessment($case_id);
        foreach ($borrower_assessment as $rows) {
            $row = 0;
            foreach ($rows as $key => $value) {
                if ($key == "id" || $key == "case_id") {
                    continue;
                }
                if ($key == "row") {
                    $row = $value;
                    continue;
                }

                $value = get_value_from_code(strtolower($key . $row), $value);

                $value = str_replace("&lt;", "<", $value);
                $objWorksheet->getCell($key . $row)->setValue($value);
            }
        }
//// END BORROWER ASSESSMENT
//    
//GENERATE EXCEL

        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objPHPexcel, 'Xlsx');
        if (!is_dir('root/Cases/' . date('Y_m_d'))) {
            mkdir('root/Cases/' . date('Y_m_d'), 0777, TRUE);
            //add index file
            copy('root/index.php', 'root/Cases/' . date('Y_m_d') . "/index.php");
        }
        $excel_name = $this->Cases_model->construct_excel_file_name($case_id);
        $this->db->update('cases', array('name' => $excel_name), array('id' => $case_id));
//        if (!$excel_name) {
//            echo 'cannot_create_file';
//        } else {
        // audit
        $case_name = $this->get_case_name($case_id);
        save_audit("Bulk csv : generate excel file for case -- " . $case_name);
        /////////
        $objWriter->save("./root/Cases/" . date('Y_m_d') . "/$excel_name.xlsx");

        if (!file_exists("./root/Cases/" . date('Y_m_d') . "/$excel_name.xlsx")) {
            if ($this->excel_check_times == 2) {
                $this->db->update('cases', array('approved' => 0, 'date_approved' => NULL, 'approved_by' => 0), array('id' => $case_id));
                // echo 'cannot_create_file';
                return array(FALSE, 'cannot_create_file');
            } else {
                $this->check_generated_excel("./root/Cases/" . date('Y_m_d') . "/$excel_name.xlsx", $case_id);
            }
        }

        $this->open_save_close(date('Y_m_d') . "/$excel_name");
        clearstatcache();
        $generated_file_size = filesize("./root/Cases/" . date('Y_m_d') . "/$excel_name.xlsx") / 1000;
        while ($this->excel_size_check_times < 2 && !($generated_file_size > 160 && $generated_file_size < 180)) {
            $this->excel_size_check_times++;
            $this->open_save_close(date('Y_m_d') . "/$excel_name");
            clearstatcache();
            $generated_file_size = filesize("./root/Cases/" . date('Y_m_d') . "/$excel_name.xlsx") / 1000;
        }
        clearstatcache();
        $generated_file_size = filesize("./root/Cases/" . date('Y_m_d') . "/$excel_name.xlsx") / 1000;
        if (!($generated_file_size > 160 && $generated_file_size < 180)) {
            unlink("./root/Cases/" . date('Y_m_d') . "/$excel_name.xlsx");
            $this->db->update('cases', array('approved' => 0, 'date_approved' => NULL, 'approved_by' => 0), array('id' => $case_id));
            //echo 'file_size_error';
            return array(FALSE, 'file_size_error');
        }

        return array(TRUE);
//        }
    }
    
    function check_generated_excel($file_path, $case_id) {
        if (!file_exists($file_path)) {
            $this->excel_check_times++;
            $this->generate_excel($case_id);
        }
    }

    function open_save_close($file_path) {
//        root/Cases/
        $single_file_name = explode('/', $file_path)[1];
        $this->replace_string_in_file(getcwd() . '\root\Cases\script.ps1', '$file_name$', $single_file_name);
        $this->replace_string_in_file(getcwd() . '\root\Cases\script.ps1', '$file$', $file_path);
        $this->replace_string_in_file(getcwd() . '\root\Cases\script.ps1', '$file_path$', getcwd() . '/root/Cases/' . $file_path);

        echo shell_exec('powershell.exe  -command Set-ExecutionPolicy Bypass -Scope CurrentUser');
        echo shell_exec('powershell.exe  -command ' . getcwd() . '\root\Cases\script.ps1');

        //$this->replace_string_in_file(getcwd() . '\root\Cases\script.ps1', $file_path, '$file$');
        //$this->replace_string_in_file(getcwd() . '\root\Cases\script.ps1', getcwd() . '/root/Cases/$file$', '$file_path$');
        //$this->replace_string_in_file(getcwd() . '\root\Cases\script.ps1', $single_file_name, '$file_name$');
        $this->replace_whole_file_content();
        sleep(3);
        if (file_exists("./root/Cases/" . $file_path . "001.xlsx")) {
            unlink("./root/Cases/" . $file_path . ".xlsx");
            rename("./root/Cases/" . $file_path . "001.xlsx", "./root/Cases/" . $file_path . ".xlsx");
        }
        $this->check_duplicate($file_path);
    }

    function replace_whole_file_content() {
        $filename = getcwd() . '\root\Cases\script.ps1';
//        $content = '$excel = New-Object -ComObject Excel.Application
//$Workbook = $Excel.Workbooks.Open("$file_path$.xlsx",0, $false)
//$Workbook.SaveAs("$file_path$001.xlsx")
//$Workbook.Close()
//$excel.Quit()
//Remove-Item –path $file_path$.xlsx –recurse 
//Rename-Item -Path "$file_path$001.xlsx" -NewName "$file_name$.xlsx"';
         $content = '$excel = New-Object -ComObject Excel.Application
$excel.DisplayAlerts = $false;
$filelocation = "$file_path$.xlsx"
$Workbook = $Excel.Workbooks.Open($filelocation,0, $false)
$Workbook.SaveAs($filelocation)
$Workbook.Saved = $true
$Workbook.Close()
$excel.Quit()';
        file_put_contents($filename, $content);
    }

    function replace_string_in_file($filename, $string_to_replace, $replace_with) {
        $content = file_get_contents($filename);
        $content_chunks = explode($string_to_replace, $content);
        $content = implode($replace_with, $content_chunks);
        file_put_contents($filename, $content);
    }

    function check_case_existence($CRN) {
        $row = $this->Bulk_model->get_user_info();
        $name = "M" . $row->code . "B" . $row->branch_code . "C" . $CRN . "I" .
                $row->first_name . $row->last_name . "D";
        $name = str_replace(" ", "", $name);
        $cases = $this->Bulk_model->check_case_existence($name);
//        print_r($case);die;
        if (!empty($cases)) {
            //delete cases
            foreach ($cases as $case) {
                $this->Bulk_model->delete_case($case->id);
            }
        }
    }

    function send_notification($case_id) {
        $this->load->model("admin/Cases_model");
        //            BEGIN NOTIFICATION
        $email_settings = $this->Cases_model->get_email_settings();
        if (!empty($email_settings->notification_email)) {
            $case_info = $this->Cases_model->get_case_info($case_id);
            $msg_content .= "Hello! \r\n";
            $msg_content .= "A new case has been created.\r\n";
            $msg_content .= "Case Name: $case_info->name \r\n";
            $msg_content .= "Bank: $case_info->bank_name \r\n";
            $msg_content .= "Created By: $case_info->first_name $case_info->last_name \r\n";
            $msg_content .= "For more information, please login " . base_url() . "\r\n";
            $msg_content .= " Thank you!";
            // config email smtp
            $settings = $this->db->get('email_settings')->row();
            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => $settings->smtp_host, //ssl://smtp.googlemail.com
                //'smtp_port' => $settings->smtp_port, // 465 
                //'smtp_user' => $settings->smtp_user,
                //'smtp_pass' => $settings->smtp_pass,
                'mailtype' => 'text',
                'charset' => 'iso-8859-1',
                'newline' => "\r\n"
            );
            $this->load->library('email');
            $this->email->initialize($config);
            $this->email->from($email_settings->host_mail);
            $this->email->to($email_settings->notification_email);
            $this->email->subject('New Case');
            $this->email->message($msg_content);
            $this->email->set_newline("\r\n");
            $sent = $this->email->send();
            $err = $this->email->print_debugger();
            $err = json_encode($err) . "\n";
            $myfile = fopen("audit_trail/email.txt", "a");
            fwrite($myfile, $err);
            fclose($myfile);
        }
//            END NOTIFICATION
    }

    function check_duplicate($file_path) {
        $this->load->helper('directory');
        $dir = explode("/", $file_path);
        $path = "./root/Cases/$dir[0]/";
        $file_name = $dir[1];
        $ext = "xlsx";
        $map = directory_map($path, 1);
        foreach ($map as $file) {
            $file = explode(".", $file);
            $file_ext = $file[1];
            $file = $file[0];
            if (strpos($file, $file_name) === 0) {
                if ($file !== $file_name && $file_ext == $ext) {
                    echo $file;
                    if (file_exists($path . $file_name . ".$ext")) {
                        unlink($path . $file_name . ".$ext");
                    }
                    if (!rename($path . $file . ".$ext", $path . $file_name . ".$ext")) {
                        if (copy($path . $file . ".$ext", $path . $file_name . ".$ext")) {
                            unlink($path . $file . ".$ext");
                        }
                    }
                }
            }
        }
    }

    function not_zero($value, $label = NULL) {
        $message = "";
        if ($label) {
            $message = $this->lang->line($label) . " ";
        }
        $this->form_validation->set_message("not_zero", "The {field} " . $this->lang->line("cant_equal_zero"));
        if ($value === 0 || $value === "0") {
            return FALSE;
        }
        return TRUE;
    }

    function declare_errors() {

        $this->errors["info"] = ['C4' => '', 'E4' => '', 'C5' => '', 'C6' => '', 'C7' => '', 'C8' => '', 'C9' => '', 'C10' => '', 'C11' => '', 'C12' => '', 'C13' => '', 'B25' => '', 'C25' => '', 'D25' => '', 'B26' => '', 'C26' => '', 'D26' => '', 'B27' => '', 'C27' => '', 'D27' => '', 'B28' => '', 'C28' => '', 'D28' => '', 'B29' => '', 'C29' => '', 'D29' => '', 'B30' => '', 'C30' => '', 'D30' => '', 'D32' => '', 'C36' => '', 'E7' => '', 'E10' => ''];
        $this->errors["fact"] = ['C4' => '', 'E4' => '', 'C5' => '', 'C9' => '', 'D9' => '', 'C10' => '', 'D10' => '', 'C11' => '', 'D11' => '', 'C12' => '', 'D12' => '', 'C13' => '', 'D13' => '', 'C14' => '', 'D14' => '', 'C15' => '', 'D15' => '', 'C16' => '', 'D16' => '', 'C17' => '', 'D17' => '', 'C18' => '', 'D18' => '', 'C22' => '', 'D22' => '', 'C23' => '', 'D23' => '', 'C24' => '', 'D24' => '', 'C25' => '', 'D25' => '', 'C26' => '', 'D26' => '', 'C27' => '', 'D27' => '', 'C28' => '', 'D28' => '', 'C29' => '', 'D29' => '', 'C30' => '', 'D30' => '', 'C31' => '', 'D31' => '', 'C35' => '', 'D35' => '', 'C36' => '', 'D36' => '', 'C37' => '', 'D37' => '', 'C38' => '', 'D38' => '', 'C39' => '', 'D39' => '', 'C40' => '', 'D40' => '', 'C41' => '', 'D41' => '', 'C42' => '', 'D42' => '', 'C43' => '', 'D43' => '', 'C44' => '', 'D44' => ''];
        $this->errors['fin'] = ['E3' => '', 'E4' => '', 'G8' => '', 'E10' => '', 'E13' => '', 'E14' => '', 'E18' => '', 'E19' => '', 'E22' => '', 'E26' => '', 'E27' => '', 'E28' => '', 'E32' => '', 'E33' => '', 'E34' => '', 'E35' => '', 'E36' => '', 'E38' => '', 'E39' => '', 'E43' => '', 'E49' => '', 'E50' => '', 'E51' => '', 'E55' => '', 'E56' => '', 'E58' => '', 'E62' => '', 'E63' => '', 'E66' => '', 'E67' => '', 'E71' => '', 'E78' => '', 'E79' => '', 'E80' => '', 'E81' => '', 'E82' => '', 'E83' => '', 'E84' => '', 'E85' => '', 'E86' => '', 'E87' => '', 'E88' => '', 'E89' => '', 'E90' => '', 'E92' => '', 'E93' => '', 'E94' => '', 'E97' => '', 'E98' => '', 'E99' => '', 'E100' => '', 'E106' => '', 'E107' => '', 'E108' => '', 'E109' => '', 'E110' => '', 'E111' => '', 'E112' => '', 'E113' => '', 'E114' => '', 'E115' => '', 'E117' => '', 'E119' => '', 'E126' => '', 'E127' => '', 'E129' => '', 'E130' => '', 'E132' => '', 'E133' => '', 'E134' => '', 'E135' => '', 'E136' => '', 'E137' => '', 'E138' => '', 'E139' => '', 'E140' => '', 'E145' => '', 'E146' => '', 'E147' => '', 'E148' => '', 'E149' => '', 'E153' => '', 'E154' => '', 'E155' => '', 'E156' => '', 'E158' => '', 'E159' => '', 'E160' => '', 'E161' => '', 'E162' => '', 'E163' => '', 'E166' => '', 'E167' => '', 'D10' => '', 'D13' => '', 'D14' => '', 'D18' => '', 'D19' => '', 'D22' => '', 'D26' => '', 'D27' => '', 'D28' => '', 'D32' => '', 'D33' => '', 'D34' => '', 'D35' => '', 'D36' => '', 'D38' => '', 'D39' => '', 'D43' => '', 'D49' => '', 'D50' => '', 'D51' => '', 'D55' => '', 'D56' => '', 'D58' => '', 'D62' => '', 'D63' => '', 'D66' => '', 'D67' => '', 'D71' => '', 'D78' => '', 'D79' => '', 'D80' => '', 'D81' => '', 'D82' => '', 'D83' => '', 'D84' => '', 'D85' => '', 'D86' => '', 'D87' => '', 'D88' => '', 'D89' => '', 'D90' => '', 'D92' => '', 'D93' => '', 'D94' => '', 'D97' => '', 'D98' => '', 'D99' => '', 'D100' => '', 'D106' => '', 'D107' => '', 'D108' => '', 'D109' => '', 'D110' => '', 'D111' => '', 'D112' => '', 'D113' => '', 'D114' => '', 'D115' => '', 'D117' => '', 'D119' => '', 'D126' => '', 'D127' => '', 'D129' => '', 'D130' => '', 'D132' => '', 'D133' => '', 'D134' => '', 'D135' => '', 'D136' => '', 'D137' => '', 'D138' => '', 'D139' => '', 'D140' => '', 'D145' => '', 'D146' => '', 'D147' => '', 'D148' => '', 'D149' => '', 'D153' => '', 'D154' => '', 'D155' => '', 'D156' => '', 'D158' => '', 'D159' => '', 'D160' => '', 'D161' => '', 'D162' => '', 'D163' => '', 'D166' => '', 'D167' => '', 'C10' => '', 'C13' => '', 'C14' => '', 'C18' => '', 'C19' => '', 'C22' => '', 'C26' => '', 'C27' => '', 'C28' => '', 'C32' => '', 'C33' => '', 'C34' => '', 'C35' => '', 'C36' => '', 'C38' => '', 'C39' => '', 'C43' => '', 'C49' => '', 'C50' => '', 'C51' => '', 'C55' => '', 'C56' => '', 'C58' => '', 'C62' => '', 'C63' => '', 'C66' => '', 'C67' => '', 'C71' => '', 'C78' => '', 'C79' => '', 'C80' => '', 'C81' => '', 'C82' => '', 'C83' => '', 'C84' => '', 'C85' => '', 'C86' => '', 'C87' => '', 'C88' => '', 'C89' => '', 'C90' => '', 'C92' => '', 'C93' => '', 'C94' => '', 'C97' => '', 'C98' => '', 'C99' => '', 'C100' => '', 'C106' => '', 'C107' => '', 'C108' => '', 'C109' => '', 'C110' => '', 'C111' => '', 'C112' => '', 'C113' => '', 'C114' => '', 'C115' => '', 'C117' => '', 'C119' => '', 'C126' => '', 'C127' => '', 'C129' => '', 'C130' => '', 'C132' => '', 'C133' => '', 'C134' => '', 'C135' => '', 'C136' => '', 'C137' => '', 'C138' => '', 'C139' => '', 'C140' => '', 'C145' => '', 'C146' => '', 'C147' => '', 'C148' => '', 'C149' => '', 'C153' => '', 'C154' => '', 'C155' => '', 'C156' => '', 'C158' => '', 'C159' => '', 'C160' => '', 'C161' => '', 'C162' => '', 'C163' => '', 'C166' => '', 'C167' => '', 'F10' => '', 'F13' => '', 'F14' => '', 'F18' => '', 'F19' => '', 'F22' => '', 'F26' => '', 'F27' => '', 'F28' => '', 'F32' => '', 'F33' => '', 'F34' => '', 'F35' => '', 'F36' => '', 'F38' => '', 'F39' => '', 'F43' => '', 'F49' => '', 'F50' => '', 'F51' => '', 'F55' => '', 'F56' => '', 'F58' => '', 'F62' => '', 'F63' => '', 'F66' => '', 'F67' => '', 'F71' => '', 'F78' => '', 'F79' => '', 'F80' => '', 'F81' => '', 'F82' => '', 'F83' => '', 'F84' => '', 'F85' => '', 'F86' => '', 'F87' => '', 'F88' => '', 'F89' => '', 'F90' => '', 'F92' => '', 'F93' => '', 'F94' => '', 'F97' => '', 'F98' => '', 'F99' => '', 'F100' => '', 'F106' => '', 'F107' => '', 'F108' => '', 'F109' => '', 'F110' => '', 'F111' => '', 'F112' => '', 'F113' => '', 'F114' => '', 'F115' => '', 'F117' => '', 'F119' => '', 'F126' => '', 'F127' => '', 'F129' => '', 'F130' => '', 'F132' => '', 'F133' => '', 'F134' => '', 'F135' => '', 'F136' => '', 'F137' => '', 'F138' => '', 'F139' => '', 'F140' => '', 'F145' => '', 'F146' => '', 'F147' => '', 'F148' => '', 'F149' => '', 'F153' => '', 'F154' => '', 'F155' => '', 'F156' => '', 'F158' => '', 'F159' => '', 'F160' => '', 'F161' => '', 'F162' => '', 'F163' => '', 'F166' => '', 'F167' => '', 'G10' => '', 'G13' => '', 'G14' => '', 'G18' => '', 'G19' => '', 'G22' => '', 'G26' => '', 'G27' => '', 'G28' => '', 'G32' => '', 'G33' => '', 'G34' => '', 'G35' => '', 'G36' => '', 'G38' => '', 'G39' => '', 'G43' => '', 'G49' => '', 'G50' => '', 'G51' => '', 'G55' => '', 'G56' => '', 'G58' => '', 'G62' => '', 'G63' => '', 'G66' => '', 'G67' => '', 'G71' => '', 'G78' => '', 'G79' => '', 'G80' => '', 'G81' => '', 'G82' => '', 'G83' => '', 'G84' => '', 'G85' => '', 'G86' => '', 'G87' => '', 'G88' => '', 'G89' => '', 'G90' => '', 'G92' => '', 'G93' => '', 'G94' => '', 'G97' => '', 'G98' => '', 'G99' => '', 'G100' => '', 'G106' => '', 'G107' => '', 'G108' => '', 'G109' => '', 'G110' => '', 'G111' => '', 'G112' => '', 'G113' => '', 'G114' => '', 'G115' => '', 'G117' => '', 'G119' => '', 'G126' => '', 'G127' => '', 'G129' => '', 'G130' => '', 'G132' => '', 'G133' => '', 'G134' => '', 'G135' => '', 'G136' => '', 'G137' => '', 'G138' => '', 'G139' => '', 'G140' => '', 'G145' => '', 'G146' => '', 'G147' => '', 'G148' => '', 'G149' => '', 'G153' => '', 'G154' => '', 'G155' => '', 'G156' => '', 'G158' => '', 'G159' => '', 'G160' => '', 'G161' => '', 'G162' => '', 'total_sales' => ''];
        $this->errors['assess'] = ['F5' => '', 'F10' => '', 'F18' => '', 'F22' => '', 'F29' => '', 'F36' => '', 'F40' => '', 'F45' => '', 'F50' => '', 'F56' => '', 'F66' => '', 'F71' => '', 'F75' => '', 'F79' => '', 'F86' => '', 'F93' => '', 'F100' => '', 'F107' => '', 'F115' => '', 'F121' => '', 'F125' => '', 'F129' => '', 'F133' => '', 'F137' => '', 'F144' => '', 'F151' => '', 'F155' => '', 'F159' => '', 'F165' => '', 'F169' => '', 'F175' => '', 'F179' => '', 'F186' => '', 'F194' => '', 'F200' => '', 'F208' => '', 'F216' => ''];
//            echo '<pre>';
//            print_r($this->errors);
//            echo '</pre>';
    }

    function set_errors($form = "info") {
        $validation_errors = "";
		$errors = $this->form_validation->error_array();
		foreach($errors as $input => $error):
			if (!isset($this->errors[$form][$input])):
				$this->errors[$form][$input] = $error;
			else:
				$this->errors[$form][$input] .= $error;
			endif;
			$validation_errors .= $error;
		endforeach;
        return $validation_errors;
    }

}
