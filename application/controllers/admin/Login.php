<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin/Login_model');
        $this->load->model('admin/Dashboard_model');
        $this->load->model('admin/User_model');
        $this->load->model('admin/Back_settings_model');
        if ($this->session->userdata('language') != "") {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        } else {
            $this->lang->load('default');
            $this->lang->load('default_1');
        }
    }

    public function index() {
        if (($this->session->userdata('user_id') != "")) {
            $this->welcome();
        } else {
            $data['title'] = 'Sign in';
            $data['captcha']=  $this->captcha()['image'];
            $this->load->view("admin/pages/login", $data);
        }
    }

    public function welcome() {
        if (($this->session->userdata('user_id') != "")) {
            $data = array('title' => 'Dashboard');
            //$data['clients_count'] = $this->Dashboard_model->get_clients_count();
            $data['users_count'] = $this->db->get_where('users')->num_rows();
            $data['back_settings'] = $this->Back_settings_model->get_back_settings();
            $this->load->view('admin/pages/dashboard', $data);
        } else {
            $this->index();
        }
    }

    public function do_login() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        $this->form_validation->set_rules('captcha', 'Captcha', 'trim|required|callback_valid_captcha');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $email = $this->input->post('email');
            $password = do_hash($this->input->post('password'), 'md5');
            $password_management = $this->Login_model->get_password_management();
            $user = $this->Login_model->get_user($email);

            //check if user is locked out
            if ($user) {
                if ($user->active == 0) {
                    $this->session->set_flashdata('msg', 'you account has been deactivated. Please Contact the system admin');
                    $this->index();
                    return;
                }
                if (!empty($user->lockout_counter) && $user->lockout_counter >= $password_management->lockout_threshold) {
                    //calculate lockout duration
                    $mins = floor((strtotime("now") - strtotime($user->lockout_timestamp)) / 60);
                    if ($mins <= $password_management->lockout_duration) {
                        //destroy session and redirect to lockout page
                        save_audit('User account locked out for user: ' . $email);
                        $this->session->sess_destroy();
                        redirect(base_url("admin/Login/lockout/$password_management->lockout_duration"));
                    }
                    //if eligibile for counter reset
                    if ($mins >= $password_management->reset_counter) {
                        $reset = array(
                            "lockout_counter" => 0,
                            "lockout_timestamp" => NULL
                        );
                        $this->Crud_model->update("users", $user->id, $reset);
                    }
                }
            }
            //end check if user is locked out
            $check_login_result = FALSE;
            if (!empty($user->id)) {
                $last_password = $this->Login_model->get_last_password($user->id);
                if (!empty($last_password->password) && trim($last_password->password) == $password) {
                    $check_login_result = $user;
                }
            }
            if ($check_login_result) {
                //check if force reset password is applied
                if ($check_login_result->force_reset) {
                    $this->session->set_userdata(array("temp_id" => $check_login_result->id));
                    //redirect to change password
                    redirect('admin/Login/change_password');
                }

                //check password settings
                if (!empty($password_management)) {
                    //remove $check_login_result when active directory applied
                    $user_id = $check_login_result->id;
                    $last_password = $this->Login_model->get_last_password($user_id);
                    if (!empty($last_password)) {
                        $days = floor((strtotime("now") - strtotime($last_password->timestamp)) / 86400);
                        // check if password age expired
                        if ($days >= $password_management->max_age) {
                            save_audit('User password age expired : ' . $email);
//                            $this->session->sess_destroy();
                            $this->session->set_userdata(array("temp_id" => $user_id));
                            //redirect to change password
                            redirect('admin/Login/change_password');
                        }
                    }
                }
                //end check password settings
//                SET SESSION
                $new_data = array(
                    'user_id' => $check_login_result->id,
                    'user_image' => $check_login_result->image,
                    'user_email' => $check_login_result->email,
                    'user_first_name' => $check_login_result->first_name,
                    'user_last_name' => $check_login_result->last_name,
                    'user_display_name' => $check_login_result->display_name,
                    'logged_in' => TRUE,
                    'access_level' => $check_login_result->access_level,
                    "bank_id" => $check_login_result->bank_id
                );

                $accessLevelRow = $this->db->get_where('accesslevelsfunctions', array('levelid' => $check_login_result->access_level))->row();
                if (!$accessLevelRow) {
                    save_audit('User login failed for : ' . $email . ' permissions not found');
                    $this->session->set_flashdata('msg', $this->lang->line('not_set_permissions'));
                    $this->index();
                    return;
                }
                $this->db->select('banks.*');
                $this->db->join('banks', 'banks.id = users.bank_id AND banks.deleted=0');
                $user_bank = $this->db->get_where('users', array('users.id' => $check_login_result->id))->row();
                if (!$user_bank) {
                    save_audit('User login failed for : ' . $email . ' bank not found');
                    $this->session->set_flashdata('msg', $this->lang->line('bank_not_assigned'));
                    $this->index();
                    return;
                }
                $sys_settings = $this->db->get('back_settings')->row();

                $new_data['sidebar_bg_color'] = $user_bank->sidebar_bg_color;
                $new_data['topbar_bg_color'] = $user_bank->topbar_bg_color;
                $new_data['sidebar_links_highlight'] = $user_bank->sidebar_links_highlight;
                $new_data['sidebar_links_font_color'] = $user_bank->sidebar_links_font_color;
                $new_data['topbar_links_font_color'] = $user_bank->topbar_links_font_color;
                $new_data['pages_header_bg'] = $user_bank->pages_header_bg;
                $new_data['pages_header_font_color'] = $user_bank->pages_header_font_color;
                $new_data['sys_logo'] = $user_bank->logo != '' ? $user_bank->logo : $sys_settings->system_logo;
                $new_data['sys_name'] = $user_bank->name != '' ? $user_bank->name : $sys_settings->system_name;

                $this->session->set_userdata($new_data);
                $this->session->set_userdata('levels', $accessLevelRow);


                if ($sys_settings) {
                    $this->session->set_userdata('sys_settings', $sys_settings);
                }
                $this->session->set_userdata('active', 'dashboard');
                $this->db->where("id", $check_login_result->id);
                $this->db->update("users", array("lockout_counter" => 0, "lockout_timestamp" => NULL, "last_login" => date("Y-m-d H:i:s")));
//                END SET SESSION
                save_audit('User logged in operation');
                redirect('admin/Dashboard');
            } else {
                //count failed attempts
                $user = $this->Login_model->get_user($email);
                if (!empty($user)) {
                    $count = $user->lockout_counter + 1;
                    $this->Crud_model->update("users", $user->id, array("lockout_counter" => $count));
//                     "lockout_timestamp" =>'to_date(date("Y-m-d H:i:s"), "YYYY-MM-DD HH24:MI:SS")'
                    $oracle = array(
                        'id' => $user->id,
                        'table' => "users",
                        'col' => "lockout_timestamp",
                        'val' => date("Y-m-d H:i:s")
                    );
                    $this->Crud_model->update_oracle_date($oracle);
                }
                //end count failed attempts
                save_audit('User login falied with email : ' . $email . ' User not found');
                $this->session->set_flashdata('msg', 'Wrong email or password');
                $this->index();
            }
        }
    }

    function Logout() {
        if (($this->session->userdata('user_id') != "")) {
            $new_data = array(
                'user_id' => '',
                'user_image' => '',
                'user_email' => '',
                'user_first_name' => '',
                'user_last_name' => '',
                'user_display_name' => '',
                'logged_in' => FALSE
            );
            save_audit('User logged out operation');
            $this->session->unset_userdata($new_data);
            $this->session->sess_destroy();
            redirect('admin/Dashboard');
        } else {
            redirect('admin/Dashboard');
        }
    }

    function change_password() {
        //complex pass
        if (!$this->session->userdata("temp_id")) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $password_management = $this->Login_model->get_password_management();
        //check minimum length
        $length = "";
        if (!empty($password_management) && $password_management->min_length) {
            $length = "min_length[$password_management->min_length]";
        }
        $this->form_validation->set_rules("old_password", $this->lang->line("old_password"), "trim|required|callback_old_password_validation");
        $this->form_validation->set_rules("new_password", $this->lang->line("new_password"), "trim|required|$length|callback_new_password_validation");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("change_password");
            $data['password_management'] = $password_management;
            $this->load->view("admin/pages/change_password", $data);
        } else {
            $user = $this->Crud_model->get_one("users", $this->session->userdata("temp_id"));
            if (empty($user)) {
                redirect(base_url("Welcome/permission_denied"));
            }

            //reset password in ad
            //end reset password in ad
            //insert password in database
            $data = array(
                'id' => incremented("user_passwords"),
                'user_id' => $user->id,
                'password' => md5($this->input->post("new_password")),
                'timestamp' => date("Y-m-d H:i:s")
            );
            $this->Crud_model->insert("user_passwords", $data);
            //end insert password in database
            //update force reset flag
            $this->Global_model->global_update("users", $user->id, array("force_reset" => 0));
            //set session
//            $this->session->sess_destroy();
            $new_data = array(
                'user_id' => $user->id,
                'user_image' => $user->image,
                'user_email' => $user->email,
                'user_first_name' => $user->first_name,
                'user_last_name' => $user->last_name,
                'user_display_name' => $user->display_name,
                'logged_in' => TRUE,
                'access_level' => $user->access_level,
                'bank_id' => $user->bank_id
            );
            $this->session->set_userdata($new_data);
            $accessLevelRow = $this->db->get_where('accesslevelsfunctions', array('levelid' => $user->access_level))->row();
            $this->session->set_userdata('levels', $accessLevelRow);
            $sys_settings = $this->db->get_where('back_settings')->row();
            if ($sys_settings) {
                $this->session->set_userdata('sys_settings', $sys_settings);
            }

            save_audit('User change his password');

            redirect(base_url("admin/Dashboard"));
        }
    }

    function lockout($lockout_duration = NULL) {
        $lockout_duration = sanitize($lockout_duration);
        $data['title'] = $this->lang->line("account_lockedout");
        $data['lockout_duration'] = $lockout_duration;
        $data['msg'] = $this->lang->line("account_lockedout_msg") . " " . $lockout_duration . " " . $this->lang->line("minutes");
        $this->load->view("errors/template/error", $data);
    }

    function old_password_validation() {
        $this->form_validation->set_message('old_password_validation', 'The {field} field didn\'t match ');
        $user = $this->Crud_model->get_one("users", $this->session->userdata("temp_id"));
        $last_password = $this->Login_model->get_last_password($user->id);
        //authenticate old password against ad
        $authenticated = TRUE;
        $email = $user->email;
        $old_password = $this->input->post("old_password");
        //end authenticate old password against ad
        if (empty($last_password->password) || md5($old_password) != $last_password->password) {
            return FALSE;
        }
        return $authenticated;
    }

    function new_password_validation() {
        //check password matches any histrory
        $this->form_validation->set_message('new_password_validation', 'The {field} field shouldn\'t match any of your previous passwords');
        $new_password = $this->input->post("new_password");
        $password_management = $this->Login_model->get_password_management();
        if ($password_management->enforce_history) {
            $exists = $this->Login_model->check_password($this->session->userdata("temp_id"), md5($new_password));
            if (!empty($exists)) {
                return FALSE;
            }
        }
        return TRUE;
    }

    function captcha() {
        $this->load->helper('captcha');
        $vals = array(
            'img_path' => './captcha/',
            'img_url' => base_url("captcha/"),
            'word_length' => 4,
			'pool' => '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',
			'colors' => array(
                'background' => array(255, 255, 255),
                'border' =>array(255, 255, 255),
                'text' => array(0, 0, 0),
				'grid' => array(155, 164, 171)
            )
        );

        $captcha = create_captcha($vals);
        $this->session->set_userdata("captcha",$captcha);
        return $captcha;
    }
    
    function valid_captcha($value){
        $this->form_validation->set_message("valid_captcha",  $this->lang->line("invalid_captcha"));
        if($this->session->userdata("captcha")){
            $captcha=$this->session->userdata("captcha");
            $this->session->unset_userdata("captcha");
            if(strtolower($value)==strtolower($captcha['word'])){
                return TRUE;
            }
        }
        return FALSE;
    }
    
    function ajax_get_captcha(){
        $captcha=$this->captcha();
        echo json_encode($captcha['image']);
    }

}
