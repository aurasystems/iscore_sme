<?php

class Reports extends CI_Controller {

    public function __construct() {
        parent::__construct();
        check_login();
        $this->load->model('admin/Reports_model');
        if ($this->session->userdata('language') != "") {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        } else {
            $this->lang->load('default');
            $this->lang->load('default_1');
        }
        $this->session->set_userdata('active', 'reports');
    }

    public function cases_report() {
        //load view
        if (substr($this->session->userdata('levels')->reports, 0, 1) == '0') {
            $this->session->set_flashdata('no_permission', 1);
            redirect('admin/Dashboard');
        }
        $branch = $submitted_date = $completed_date = $submitted_date_from = $submitted_date_to = $completed_date_from = $completed_date_to = '';
        $submitted_date_from = date('m/01/Y');
        $submitted_date_to = date('m/d/Y');
        $bank = $status = "all";
        if ($this->input->post()) {
            $bank = sanitize($this->input->post('bank'));
            $branch = sanitize($this->input->post('branch'));
            $status = sanitize($this->input->post('status'));
            $submitted_date = sanitize($this->input->post('submitted_date'));
            $completed_date = sanitize($this->input->post('completed_date'));

            $submitted_date_from = sanitize($this->input->post('submitted_date_from'));
            $submitted_date_to = sanitize($this->input->post('submitted_date_to'));
            $completed_date_from = sanitize($this->input->post('completed_date_from'));
            $completed_date_to = sanitize($this->input->post('completed_date_to'));
        }
        // // audit/
        save_audit("view cases report");
        /////////////////////
        $data['submitted_date_from'] = $submitted_date_from;
        $data['submitted_date_to'] = $submitted_date_to;
        $data['title'] = $this->lang->line("cases_report");
        $data['banks'] = $this->db->get_where('banks', array('deleted' => 0))->result();
        $data['cases'] = $this->Reports_model->get_cases($bank, $branch, $status, $submitted_date, $completed_date, $submitted_date_from, $submitted_date_to, $completed_date_from, $completed_date_to);
        $data['label_att'] = 'col-sm-3 control-label';
        $this->load->view("admin/pages/reports/cases_report", $data);
    }

    public function get_bank_branches() {
        $data = [];
        $bank_id = $this->input->post('bank');
        if ($bank_id == "all") {
            $banks = $this->db->get_where('banks', array('deleted' => 0))->result();
            foreach ($banks as $one) {
                $this->db->select("branch_code");
                $this->db->group_by('branch_code');
                $this->db->where("bank_id", $one->id);
                $this->db->from("users");
                $query = $this->db->get();
                $bank_branches = $query->result();
                if ($bank_branches) {
                    foreach ($bank_branches as $branch) {
                        //send bank id with the branch id to avoid getting another bank's branch with the same code
                        $branch->branch_bank_id = $branch->branch_code . "_" . $one->id;
                        $branch->branch_code = $one->name . " - " . $branch->branch_code;
                    }
                    $data = array_merge($data, $bank_branches);
                }
            }
        } else {
            $this->db->select('branch_code');
            $this->db->group_by('branch_code');
            $data = $this->db->get_where('users', array('bank_id' => $bank_id))->result();
        }
        echo json_encode($data);
    }

    function audit_trail() {
        if (substr($this->session->userdata('levels')->reports, 0, 1) == '0') {
            $this->session->set_flashdata('no_permission', 1);
            redirect('admin/Dashboard');
        }
        if (!compare_level("super_admin")) {
            $this->session->set_flashdata('no_permission', 1);
            redirect('admin/Dashboard');
        }
        $data['title'] = $this->lang->line("audit_trail");

        //$data['changes'] = $this->Reports_model->get_logs($this->input->post('length'), $this->input->post('start'));
        $data['label_att'] = 'col-sm-3 control-label';
        $this->load->view("admin/pages/reports/audit_trail", $data);
    }

    function audit_trail_ajax() {
        $search_val = isset($this->input->post('search')["value"]) ? $this->input->post('search')["value"] : 0;

        $db_data = $this->Reports_model->get_logs($this->input->post('length'), $this->input->post('start'), $search_val);

        $arr = array();
        foreach ($db_data as $one) {
            $sub_array = array();
            $sub_array[] = $one->action;
            $sub_array[] = $one->first_name . ' ' . $one->last_name;
            $sub_array[] = $one->timestamp;
            $arr[] = $sub_array;
        }
        $all_count = $this->db->select('count(*) as allcount')->get('audits')->row()->ALLCOUNT;

        $output = array(
            "draw" => intval($this->input->post("draw")),
            "recordsTotal" => $all_count,
            "recordsFiltered" => $search_val ? $this->Reports_model->get_logs_filter($search_val) : $all_count,
            "data" => $arr
        );
        echo json_encode($output);
    }

    public function banks_report() {
        //load view
        if (substr($this->session->userdata('levels')->reports, 0, 1) == '0') {
            $this->session->set_flashdata('no_permission', 1);
            redirect('admin/Dashboard');
        }
        if (!compare_level("super_admin")) {
            $this->session->set_flashdata('no_permission', 1);
            redirect('admin/Dashboard');
        }
        $count_method = 'all';
        if ($this->input->post('count_method')) {
            $count_method = $this->input->post('count_method');
        }

        // // audit/
        save_audit("view banks report");
        /////////////////////
        $data['title'] = $this->lang->line("banks_report");
        $data['report_data'] = $this->Reports_model->get_banks_report_data($count_method);
        $data['label_att'] = 'col-sm-3 control-label';
        $this->load->view("admin/pages/reports/banks_report", $data);
    }

}
