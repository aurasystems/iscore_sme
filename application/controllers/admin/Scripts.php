<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Scripts extends CI_Controller {

    public $excel_size_check_times = 0;

    public function __construct() {
        parent::__construct();
//        $this->load->model('admin/Bulk_model');
        if ($this->session->userdata('language') != "") {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        } else {
            $this->lang->load('default');
            $this->lang->load('default_1');
        }
    }

    function remove_duplicates() {
        $this->load->helper('directory');
        $today = date("d-m-Y");
        $path = "./root/Cases/$today/";
        $map = directory_map($path, 1);
        foreach ($map as $file) {
            if ($file == "index.php" || !file_exists($path . $file)) {
                continue;
            }
            $file_name = $file;
            $size = filesize($path . $file_name);
            if ($size > 200000) {
                //get name and extension separtely
                $sep_file = explode(".", $file);
                $file_ext = $sep_file[1];
                $sep_name = $sep_file[0];
                //check if the valid file exists
                $rename_file = $sep_name . "001." . $file_ext;
                if (file_exists($path . $rename_file)) {
                    //delete the current file
                    unlink($path . $file_name);
                    //rename the valid file with the old file's name
                    rename($path . $rename_file, $path . $file_name);
                }
            }
        }
    }

    function scan_invalid_size() {
        $this->load->helper('directory');
        $today = date("Y_m_d");
        $path = "./root/Cases/$today/";
        $map = directory_map($path, 1);
        foreach ($map as $file) {
            if ($file == "index.php" || !file_exists($path . $file)) {
                continue;
            }
            $file_name = $file;
            $this->excel_size_check_times = 0;
            $generated_file_size = filesize($path . $file_name) / 1000;
            if (!($generated_file_size > 160 && $generated_file_size < 180)) {
                $sep_file = explode(".", $file);
                $file_n = $sep_file[0];
                $this->open_save_close($today . '/' . $file_n);
                while ($this->excel_size_check_times < 2 && !($generated_file_size > 160 && $generated_file_size < 180)) {
                    $this->excel_size_check_times++;
                    $this->open_save_close($today . '/' . $file_n);
                    $generated_file_size = filesize($path . $file_name) / 1000;
                    clearstatcache();
                }
                clearstatcache();
            }
        }
    }

    function open_save_close($file_path) {
        $single_file_name = explode('/', $file_path)[1];
        $this->replace_string_in_file(getcwd() . '\root\Cases\script_check.ps1', '$file_path$', getcwd() . '/root/Cases/' . $file_path);
        echo shell_exec('powershell.exe  -command Set-ExecutionPolicy Bypass -Scope CurrentUser');
        echo shell_exec('powershell.exe  -command ' . getcwd() . '\root\Cases\script_check.ps1');
        $this->replace_whole_file_content();
        sleep(3);
    }

    function replace_whole_file_content() {
        $filename = getcwd() . '\root\Cases\script_check.ps1';
        $content = '$excel = New-Object -ComObject Excel.Application
$excel.DisplayAlerts = $false;
$filelocation = "$file_path$.xlsx"
$Workbook = $Excel.Workbooks.Open($filelocation,0, $false)
$Workbook.SaveAs($filelocation)
$Workbook.Saved = $true
$Workbook.Close()
$excel.Quit()';
        file_put_contents($filename, $content);
    }

    function replace_string_in_file($filename, $string_to_replace, $replace_with) {
        $content = file_get_contents($filename);
        $content_chunks = explode($string_to_replace, $content);
        $content = implode($replace_with, $content_chunks);
        file_put_contents($filename, $content);
    }

}
