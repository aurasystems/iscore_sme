<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cases extends CI_Controller {

    public $excel_check_times = 0;
    public $excel_size_check_times = 0;

    public function __construct() {
        parent::__construct();
        check_login();
        $this->load->model('admin/Cases_model');
        if ($this->session->userdata('language') != "") {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        } else {
            $this->lang->load('default');
            $this->lang->load('default_1');
        }
        $this->session->set_userdata('active', 'cases');
    }

    function get_case($id = NULL) {
        $id = sanitize($id);
        check_parameter($id);
        $case = $this->Crud_model->get_one("cases", $id);
        check_parameter($case);
        redirect(base_url("admin/Cases/$case->status/$id"));
    }

    function borrower_information($id = NULL, $view_only = NULL) {
        $id = sanitize($id);
        $view_only = sanitize($view_only);
        /// Path Traversal Owasp testing issue
        foreach ($this->input->get() as $key => $value) {
            $_GET[$key] = $this->security->sanitize_filename($value);
            $_GET[$key] = $this->db->escape($value);
        }
        /////////////
        //check if not available for editing
        $row = [];
        if (!empty($id) && !$view_only) {
            $row = $this->Cases_model->get_bi_arr($id);
            $case = $this->db->get_where('cases', array('id' => $id))->row();
            if ($case && $case->status == 'saved' && $case->approved == 0) {
                $this->session->set_flashdata('error', $this->lang->line('cannot_edit'));
                redirect(base_url('admin/Cases_two'));
            } else if ($case->approved == 1) {
                $this->session->set_flashdata('error', $this->lang->line('appoved_cannot_edit'));
                redirect(base_url('admin/Cases_two'));
            }
        }
        ////////////
        $case = $this->Crud_model->get_one("cases", $id);
        $old_case_arr = $this->Cases_model->get_bi_arr($id);
        if ($view_only != NULL) {
            if ($case->user_id != $this->session->userdata("user_id")) {
                check_perm("cases", 0);
            }
        } else {
            check_perm("cases", 1);
        }
        $this->load->model('admin/Cases_two_model');
        $this->Cases_two_model->check_case_status($id, $view_only, 'borrower_information');
        $row = [];
        if (!empty($id)) {
            $row = $this->Cases_model->get_bi_arr($id);
        }
        //call validation
        if (!$this->input->post("continue_later")) {
            $this->borrower_information_validation($id);
        } else {
            $this->borrower_information_continue_validation();
        }
        if (!$this->form_validation->run()) {
            //load view
            $data['title'] = $this->lang->line("borrower_information");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['row'] = $row;
            $data['case_id'] = $id;
            if ($id != null) {
                $data['is_case_pass_this_setp'] = $this->Cases_two_model->if_curr_stage_pass($id, 'borrower_financials');
            } else {
                $data['is_case_pass_this_setp'] = true;
            }
            if ($view_only != NULL) {
                $data['view_only'] = 1;
            }
            $this->load->view("admin/pages/cases/borrower_information", $data);
        } else {
            //borrower information
            $data = [];
            $data["E4"] = $this->input->post("E4", TRUE);
            $data["E7"] = $this->input->post("E7", TRUE);
            $data["E10"] = $this->input->post("E10", TRUE);
            $data["turnover_amount"] = $this->input->post("turnover_amount", TRUE);
            $data["bank_amount"] = $this->input->post("bank_amount", TRUE);
            for ($i = 4; $i <= 13; $i++) {
                if ($i == 12) {
                    $data["C$i"] = $this->input->post("C$i", TRUE) * $data["turnover_amount"];
                    continue;
                }
                $data["C$i"] = $this->input->post("C$i", TRUE);
            }
            for ($i = 25; $i <= 30; $i++) {
                $data["B$i"] = "NA";
                $data["C$i"] = "NA";
                $data["D$i"] = "NA";
                if (!empty($this->input->post("B$i", TRUE)) && $this->input->post("B$i", TRUE) != "" && strtotime($this->input->post("B$i"))) {
                    $data["B$i"] = date("M-y", strtotime($this->input->post("B$i")));
                    $exp = explode('-', $this->input->post("B$i", TRUE));
                    if (count($exp) == 2) {
                        $data["B$i"] = $this->input->post("B$i", TRUE);
                    }
                }
                if (!empty($this->input->post("C$i", TRUE)) && $this->input->post("C$i", TRUE) != "") {
                    if (is_numeric($this->input->post("C$i"))) {
                        $data["C$i"] = $this->input->post("C$i") * $data["bank_amount"];
                    } else {
                        $data["C$i"] = $this->input->post("C$i");
                    }
                }
                if (!empty($this->input->post("D$i", TRUE)) && $this->input->post("D$i", TRUE) != "") {
                    if (is_numeric($this->input->post("D$i"))) {
                        $data["D$i"] = $this->input->post("D$i") * $data["bank_amount"];
                    } else {
                        $data["D$i"] = $this->input->post("D$i");
                    }
                }
            }

            $data["D32"] = "NA";
            if (!empty($this->input->post("D32", TRUE)) && $this->input->post("D32", TRUE) != "") {
                if (is_numeric($this->input->post("D32"))) {
                    $data["D32"] = $this->input->post("D32", TRUE) * $data["bank_amount"];
                } else {
                    $data["D32"] = $this->input->post("D32");
                }
            }
            $data["C36"] = date("j-M-y", strtotime($this->input->post("C36", TRUE)));

//            $data["C19"]=approvername;
//            $data["C21"]=dategenerated;

            $case_id = "";
            if ($id) {
                $case_id = $id;

                $update = array();
                if (!$this->input->post("continue_later") &&
                        $this->Cases_two_model->if_curr_stage_pass($case_id, 'borrower_factsheet')) {
                    $update['status'] = "borrower_factsheet";
                } else if ($this->Cases_two_model->if_curr_stage_pass($case_id, 'borrower_information')) {
                    $update['status'] = "borrower_information";
                }
                if (!empty($update)) {
                    $this->Crud_model->update("cases", $case_id, $update);
                }

                $this->Cases_model->update_bi($case_id, $data);
                $this->update_crn_name($case_id, $old_case_arr["C5"]);
                $this->check_update_notification($old_case_arr);
                // audit
                $case_name = $this->get_case_name($case_id);
                save_audit("Case Borrower information upated -- " . $case_name);
                /////////
            } else {
                //insert case
                $case = array(
                    'user_id' => $this->session->userdata("user_id"),
                    'date_created' => date("Y-m-d H:i:s"),
                    'id' => incremented("cases")
//                'name'=>""
                );
                if (!$this->input->post("continue_later")) {
                    $case['status'] = "borrower_factsheet";
                } else {
                    $case['status'] = "borrower_information";
                }
//                $this->db->query("alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS'", "");
                $this->Crud_model->insert("cases", $case);
                $case_id = $case["id"];



                $user_info = $this->Cases_model->get_user_info($this->session->userdata("user_id"));
                $data["C16"] = $user_info->bank_name;
                $data["C17"] = $user_info->branch_code;
                $data["C18"] = $user_info->first_name . " " . $user_info->last_name;

                $data["case_id"] = $case_id;
                $data["C20"] = $case['date_created'];
                $data["id"] = incremented("borrower_information");
                $this->Crud_model->insert("borrower_information", $data);

                $excel_name = $this->Cases_model->construct_excel_file_name($case_id);
                $this->db->update('cases', array('name' => $excel_name), array('id' => $case_id));
            }

            if (!$this->input->post("continue_later")) {
                // audit
                save_audit("Case Borrower information saved -- " . $excel_name);
                /////////
                $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
                redirect(base_url("admin/Cases_two/borrower_factsheet/$case_id"));
            } else {
                // audit
                save_audit("borrower information inserted for case " . $excel_name . " and marked as continue later");
                /////////
                $this->session->set_flashdata("success", $this->lang->line("data_saved_successfully"));
                redirect(base_url("admin/Cases_two"));
            }
        }
    }

    function get_case_name($case_id) {
        $case_id = sanitize($case_id);
        $case = $this->db->get_where('cases', array('id' => $case_id))->row();
        if ($case) {
            return $case->name;
        }
        return '';
    }

    function borrower_information_validation($id = NULL) {
        $this->form_validation->set_rules("E4", $this->lang->line("borrower_information_E4"), "xss_clean|trim|required|max_length[40]|callback_arabic_only");
        $this->form_validation->set_rules("E7", $this->lang->line("borrower_information_E7"), "xss_clean|trim|required");
        $this->form_validation->set_rules("E10", $this->lang->line("borrower_information_E10"), "xss_clean|trim|required|numeric");
        for ($i = 4; $i <= 13; $i++) {
            $extra = "";
            if ($i == 4) {
                //modified
                $extra = "|callback_english_only|max_length[40]";
            } elseif ($i == 5) {
                //modified
                $extra = "|greater_than_equal_to[1]|less_than_equal_to[99999999999999999999]|numeric|max_length[20]";
            } elseif ($i == 7) {
                //modified
                $extra = "";
                if (!empty($this->input->post("C$i")) && $this->input->post("C$i") != "") {
                    //modified
                    $extra = "|greater_than_equal_to[1]|less_than_equal_to[999999999999]|numeric|max_length[12]";
                }
                $this->form_validation->set_rules("C$i", $this->lang->line("borrower_information_C$i"), "xss_clean$extra");
                continue;
            } elseif ($i == 8) {
                //modified
                $extra = "";
                if ($this->input->post("C$i") === 0 || ($this->input->post("C$i") != NULL && $this->input->post("C$i") != "")) {
                    $extra = "|greater_than_equal_to[1]|less_than_equal_to[99999999999999]|numeric|exact_length[14]";
                }
                $this->form_validation->set_rules("C$i", $this->lang->line("borrower_information_C$i"), "xss_clean$extra");
                continue;
            } elseif ($i == 12) {
                $extra = "|callback_validate_range_amounts";
                //$extra = "|greater_than_equal_to[1]|less_than_equal_to[200000000]";
            } elseif ($i == 13) {
                $extra = "|numeric|greater_than_equal_to[-500]|less_than_equal_to[500]";
            }
            $this->form_validation->set_rules("C$i", $this->lang->line("borrower_information_C$i"), "xss_clean|required$extra");
        }
        for ($i = 25; $i <= 30; $i++) {
            $extra = "";
            $this->form_validation->set_rules("B$i", $this->lang->line("borrower_information_B$i"), "xss_clean|trim");
            $this->form_validation->set_rules("C$i", $this->lang->line("borrower_information_C$i"), "xss_clean|trim");
            $this->form_validation->set_rules("D$i", $this->lang->line("borrower_information_D$i"), "xss_clean|trim");
        }
        //modified
        $this->form_validation->set_rules("D32", $this->lang->line("borrower_information_D32"), "xss_clean|trim");
        $this->form_validation->set_rules("C36", $this->lang->line("borrower_information_C36"), "xss_clean|trim|required|callback_validate_C36_date");
        if ($id == NULL) {
            $id = 0;
        }
        $this->form_validation->set_rules("excel_name", $this->lang->line("excel_name"), "callback_validate_excel_name[$id]");
    }

    function english_only($value) {
        $this->form_validation->set_message("english_only", $this->lang->line("english_only"));
        if (!preg_match('/[^A-Za-z\s]/', $value)) {
            return TRUE;
        }
        return FALSE;
    }

    function arabic_only($value) {
        $this->form_validation->set_message("arabic_only", $this->lang->line("arabic_only"));
        if (preg_match('/^[\s\p{Arabic}]+$/u', $value)) {
            return TRUE;
        }
        return FALSE;
    }

    function validate_range_amounts($value) {
        $actual_val = $value * $this->input->post('turnover_amount');
        if ($actual_val >= 0 && $actual_val <= 200000000) {
            return TRUE;
        }
        $this->form_validation->set_message('validate_range_amounts', $this->lang->line('turnover_range_error'));
        return FALSE;
    }

    function borrower_information_continue_validation() {
        $this->form_validation->set_rules("E4", $this->lang->line("borrower_information_E4"), "xss_clean|trim");
        $this->form_validation->set_rules("E7", $this->lang->line("borrower_information_E7"), "xss_clean|trim");
        $this->form_validation->set_rules("E10", $this->lang->line("borrower_information_E10"), "xss_clean|trim");
        for ($i = 4; $i <= 13; $i++) {
            $this->form_validation->set_rules("C$i", $this->lang->line("borrower_information_C$i"), "xss_clean|trim");
        }
        for ($i = 16; $i <= 21; $i++) {
            $this->form_validation->set_rules("C$i", $this->lang->line("borrower_information_C$i"), "xss_clean|trim");
        }
        for ($i = 25; $i <= 30; $i++) {
            $this->form_validation->set_rules("B$i", $this->lang->line("borrower_information_B$i"), "xss_clean|trim");
            $this->form_validation->set_rules("C$i", $this->lang->line("borrower_information_C$i"), "xss_clean|trim");
            $this->form_validation->set_rules("D$i", $this->lang->line("borrower_information_D$i"), "xss_clean|trim");
        }
        $this->form_validation->set_rules("D32", $this->lang->line("borrower_information_D32"), "xss_clean|trim");
        $this->form_validation->set_rules("C36", $this->lang->line("borrower_information_C36"), "xss_clean|trim");
    }

    function edit_borrower_information($id = NULL) {
        $id = sanitize($id);
        check_perm("cases", 2);
        check_parameter($id);

        $row = [];
        if (!empty($id)) {
            $row = $this->Cases_model->get_bi_arr($id);
            $case = $this->db->get_where('cases', array('id' => $id))->row();
            if ($case && $case->status == 'saved' && $case->approved == 0) {
                $this->session->set_flashdata('error', $this->lang->line('cannot_edit'));
                redirect(base_url('admin/Cases_two'));
            } else if ($case->approved == 1) {
                $this->session->set_flashdata('error', $this->lang->line('appoved_cannot_edit'));
                redirect(base_url('admin/Cases_two'));
            }
        }
        $old_case_arr = $this->Cases_model->get_bi_arr($id);
        //call validation
        $this->borrower_information_validation($id);
        if (!$this->form_validation->run()) {
            //load view
            $data['title'] = $this->lang->line("borrower_information");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['row'] = $row;
            $this->load->view("admin/pages/cases/edit_borrower_information", $data);
        } else {
            //borrower information
            $data = [];
            $data["E4"] = $this->input->post("E4");
            $data["E7"] = $this->input->post("E7");
            $data["E10"] = $this->input->post("E10");
            $data["turnover_amount"] = $this->input->post("turnover_amount", TRUE);
            $data["bank_amount"] = $this->input->post("bank_amount", TRUE);
            for ($i = 4; $i <= 13; $i++) {
                if ($i == 12) {
                    $data["C$i"] = $this->input->post("C$i") * $data["turnover_amount"];
                    continue;
                }
                $data["C$i"] = $this->input->post("C$i");
            }
            for ($i = 25; $i <= 30; $i++) {
                $data["B$i"] = "NA";
                $data["C$i"] = "NA";
                $data["D$i"] = "NA";
                if (!empty($this->input->post("B$i")) && $this->input->post("B$i") != "" && strtotime($this->input->post("B$i"))) {
                    $data["B$i"] = date("M-y", strtotime($this->input->post("B$i")));
                    $exp = explode('-', $this->input->post("B$i"));
                    if (count($exp) == 2) {
                        $data["B$i"] = $this->input->post("B$i");
                    }
                }
                if (!empty($this->input->post("C$i")) && $this->input->post("C$i") != "") {
                    if (is_numeric($this->input->post("C$i"))) {
                        $data["C$i"] = $this->input->post("C$i") * $data["bank_amount"];
                    } else {
                        $data["C$i"] = $this->input->post("C$i");
                    }
                }
                if (!empty($this->input->post("D$i")) && $this->input->post("D$i") != "") {
                    if (is_numeric($this->input->post("D$i"))) {
                        $data["D$i"] = $this->input->post("D$i") * $data["bank_amount"];
                    } else {
                        $data["D$i"] = $this->input->post("D$i");
                    }
                }
            }

            $data["D32"] = "NA";
            if (!empty($this->input->post("D32", TRUE)) && $this->input->post("D32", TRUE) != "") {
                if (is_numeric($this->input->post("D32"))) {
                    $data["D32"] = $this->input->post("D32", TRUE) * $data["bank_amount"];
                } else {
                    $data["D32"] = $this->input->post("D32");
                }
            }
            $data["C36"] = date("j-M-y", strtotime($this->input->post("C36")));
            $case_id = $id;
            $this->Cases_model->update_bi($case_id, $data);
            $this->update_crn_name($case_id, $old_case_arr["C5"]);

            $this->check_update_notification($old_case_arr);
            // audit
            $case_name = $this->get_case_name($case_id);
            save_audit("Borrower information updated in case -- " . $case_name);
            /////////

            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Cases_two/borrower_factsheet/$case_id"));
        }
    }

    function approve_case() {
        $case_id = $this->input->post("case_id");
//        if (non_subscriber_bank($case_id)) {
//            $this->session->set_flashdata('error', $this->lang->line('non_subscriber_bank_error'));
//            redirect(base_url('admin/Cases_two'));
//        }
        $case = $this->db->get_where('cases', array('id' => $case_id))->row();
        if (!$case) {
            $this->session->set_flashdata('error', $this->lang->line('case_not_found_copy'));
            redirect(base_url('admin/Cases_two'));
        }
        if ($case->status != "saved" || !$case) {
            echo "incomplete";
        } else {

            $update = array(
                'approved' => 1,
                'date_approved' => date("Y-m-d H:i:s"),
                'approved_by' => $this->session->userdata("user_id"),
            );
            // if (non_subscriber_bank($case_id)) {
            //     $update['date_completed'] = date('Y-m-d H:i:s');
            //     $update['completed'] = 1;
            // }
            $this->Crud_model->update("cases", $case_id, $update); 
            save_audit("Case marked as approved -- " . $case->name);
            
        }
    }

    function test() {
//        $myfile = fopen("audit_trail/email.txt", "a");
//        fwrite($myfile, "rrrr");
//        fclose($myfile);
        $this->db->update('cases', array('approved' => 0), array('id' => 178));
    }

    function test2() {
        print_R($this->db->update('cases', array('completed' => 1), array('id' => 227)));
    }

    function generate_excel($case_id, $browser_download = 0) {    // if $browser_download =1 then it clicks from cases show page click on download excel file for an approved case.
        $this->lang->load('default');
        $this->lang->load('default_1');
        $case_id = sanitize($case_id);

        $case = $this->Cases_model->get_case_with_current_user_approve($case_id, $browser_download);

        ini_set('memory_limit', '2048M');
        set_time_limit(1800);
        $objPHPexcel = \PhpOffice\PhpSpreadsheet\IOFactory::load('./template/template.xlsx');
        $sheetname = array(
            'Read Me',
            'Definition Sheet',
            'Completion Check',
            'Borrower Information',
            "Borrower Factsheet",
            "Borrower Financials",
            "Borrower Assessment",
//            "Master"
        );
        //get cell color
        //$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->getRGB();
//       BEGIN BORROWER INFO
        $objWorksheet = $objPHPexcel->setActiveSheetIndex(3);
//        $user_info = $this->Cases_model->get_user_info($this->session->userdata("user_id"));
//        $data_bi["C16"] = $user_info->bank_name;
//        $data_bi["C17"] = $user_info->branch_code;
//        $data_bi["C18"] = $user_info->first_name . " " . $user_info->last_name;
//        $this->Cases_model->update_bi($case_id, $data_bi);

        $borrower_info = $this->Cases_model->get_borrower_information($case_id);
        foreach ($borrower_info as $key => $value) {
            if ($key == "id" || $key == "case_id" || $key == "turnover_amount" || $key == "bank_amount") {
                continue;
            }
            if ($key == "C7" || $key == "C8") {
                if (!empty($value) && $value != "") {
                    $objWorksheet->setCellValueExplicit($key, $value, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_NUMERIC);
                } else {
                    $objWorksheet->getCell($key)->setValue("");
                }
                continue;
            }
            if ($key == "C17") {
                $value = str_replace("'", "", $case->branch_code);
            }
            if ($key == "C19") {
                $value = $case->first_name . " " . $case->last_name;
            }
            if ($key == "C20") {
                $value = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel(date("j-M-y", strtotime($case->date_created)));
            }
            if ($key == "C21") {
                if ($browser_download == 1) {
                    $value = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel(date("j-M-y", strtotime($case->date_approved)));
                } else {
                    $value = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel(date("j-M-y", strtotime(date('Y-m-d'))));
                }
            }
            if ($key == "C36") {
                $value = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value);
            }
            if ($key == "C13") {
                $value = $value / 100;
            }
            if ($key == 'C6' || $key == 'C9' || $key == 'C10' || $key == 'C11' || $key == 'E7') {
                $value = get_value_from_code($key, $value);
            }
            $objWorksheet->getCell($key)->setValue($value);
        }
// END BORROWER INFO
//       BEGIN BORROWER FACTSHEET
        $objWorksheet = $objPHPexcel->setActiveSheetIndex(4);
        $borrower_factsheet = $this->Cases_model->get_borrower_factsheet($case_id);
        foreach ($borrower_factsheet as $rows) {
            $row = 0;
            foreach ($rows as $key => $value) {
                if ($key == "id" || $key == "case_id" || ($row == 4 && $key == "D") || ($row == 5 && $key == "D") || ($row == 5 && $key == "E")) {
                    continue;
                }
                if ($key == "row") {
                    $row = $value;
                    continue;
                }
                $objWorksheet->getCell($key . $row)->setValue($value);
            }
        }
// END BORROWER FACTSHEET
//       BEGIN BORROWER FINANCIALS
        $objWorksheet = $objPHPexcel->setActiveSheetIndex(5);
        $borrower_financials = $this->Cases_model->get_borrower_financials($case_id);
        $projected = "yes";
        foreach ($borrower_financials as $rows) {
            $row = 0;
            foreach ($rows as $key => $value) {
                if ($key == "id" || $key == "case_id") {
                    continue;
                }
                if ($key == "row") {
                    $row = $value;
                    continue;
                }
                if ($row == 3 && $key == "E") {
                    if ($value == "no") {
                        $value = "No";
                    }
                    if ($value == "yes") {
                        $value = "Yes";
                    }
                }
                if ($row == 4 && $key == "E") {
//                    $projected=$value;
                    if ($value == "no") {
                        $value = "No";
                    }
                    if ($value == "yes") {
                        $value = "Yes";
                    }
//                    $value="Yes";
                }
                if ($row == 8 && $key == "F") {
                    continue;
                }
                if ($row == 11 && $key == "F") {
                    $objWorksheet->setCellValue($key . $row, '=IFERROR($G$9+1,"-")');
                    continue;
                }
                if ($row == 11 && $key == "G") {
                    $objWorksheet->setCellValue($key . $row, '=IFERROR($G$9+2,"-")');
                    continue;
                }
                if ($row == 12) {
                    if ($key == "E") {
                        $objWorksheet->setCellValue($key . $row, "=IF(ISBLANK('Borrower Information'!C36)," . '"-"' . ",'Borrower Information'!C36)");
                    }
                    if ($key == "C") {
                        $objWorksheet->setCellValue($key . $row, '=IF(E12="-","-",DATE(YEAR($E$12)-2,MONTH($E$12),DAY($E$12)))');
                    }
                    if ($key == "D") {
                        $objWorksheet->setCellValue($key . $row, '=IF(E12="-","-",DATE(YEAR($E$12)-1,MONTH($E$12),DAY($E$12)))');
                    }
                    if ($key == "F") {
                        $objWorksheet->setCellValue($key . $row, '=IF(E12="-","-",DATE(YEAR($E$12)+1,MONTH($E$12),DAY($E$12)))');
                    }
                    if ($key == "G") {
                        $objWorksheet->setCellValue($key . $row, '=IF(E12="-","-",DATE(YEAR($E$12)+2,MONTH($E$12),DAY($E$12)))');
                    }
                    continue;
                }
                if ($row > 15 && $projected == "no" && ($key == "F" || $key == "G")) {
                    continue;
                }
                $objWorksheet->getCell($key . $row)->setValue($value);
            }
        }
//// END BORROWER FINANCIALS
////       BEGIN BORROWER ASSESSMENT
        $objWorksheet = $objPHPexcel->setActiveSheetIndex(6);
        $borrower_assessment = $this->Cases_model->get_borrower_assessment($case_id);
        foreach ($borrower_assessment as $rows) {
            $row = 0;
            foreach ($rows as $key => $value) {
                if ($key == "id" || $key == "case_id") {
                    continue;
                }
                if ($key == "row") {
                    $row = $value;
                    continue;
                }
                if ($row != 229 && $row != 221) {
                    $value = get_value_from_code($key . $row, $value);
                }

                $value = str_replace("&lt;", "<", $value);
                $objWorksheet->getCell($key . $row)->setValue($value);
            }
        }
//// END BORROWER ASSESSMENT
//    
//GENERATE EXCEL
        $excel_name = $case->name;
        if ($browser_download) {  // then download excel in the broswer
            save_audit("Excel file downloaded for case -- " . $excel_name);
            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            header("Content-Disposition: attachment; filename=\"$excel_name.xlsx\"");
            header("Cache-Control: max-age=0");
            $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objPHPexcel, 'Xlsx');
            $objWriter->save("php://output");
            redirect(base_url('admin/Cases_two'));
        }

        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objPHPexcel, 'Xlsx');
        if (!is_dir('root/Cases/' . date('Y_m_d'))) {
            mkdir('root/Cases/' . date('Y_m_d'), 0777, TRUE);
            //add index file
            copy('root/index.php', 'root/Cases/' . date('Y_m_d') . "/index.php");
        }
        // to be removed 
//        if (!is_dir('excel_logs/' . date('d-m-Y'))) {
//            mkdir('excel_logs/' . date('d-m-Y'), 0777, TRUE);
//            copy('root/index.php', 'excel_logs/' . date('d-m-Y') . "/index.php");
//            // to here
//        }
//        $excel_name = $this->Cases_model->construct_excel_file_name($case_id);
//        $this->db->update('cases', array('name' => $excel_name), array('id' => $case_id));

        if (!$excel_name) {
            echo 'cannot_create_file';
            return FALSE;
        } else {
            // audit
            $case_name = $this->get_case_name($case_id);
            save_audit("generate excel file for case -- " . $case_name);
            /////////
            $objWriter->save("./root/Cases/" . date('Y_m_d') . "/$excel_name.xlsx");
            // to be removed
            //$objWriter->save("./excel_logs/" . date('d-m-Y') . "/$excel_name.xlsx");
            //  copy("./root/Cases/" . date('d-m-Y') . "/$excel_name.xlsx", "./excel_logs/" . date('d-m-Y') . "/$excel_name.xlsx");
            // to here
            if (!file_exists("./root/Cases/" . date('Y_m_d') . "/$excel_name.xlsx")) {
                if ($this->excel_check_times == 2) {
                    save_audit("cannot create file for case -- " . $case_name);
                    echo 'cannot_create_file';
                    return FALSE;
                } else {
                    $this->check_generated_excel("./root/Cases/" . date('Y_m_d') . "/$excel_name.xlsx", $case_id);
                }
            }
            $this->open_save_close(date('Y_m_d') . "/$excel_name");
            clearstatcache();
            $generated_file_size = filesize("./root/Cases/" . date('Y_m_d') . "/$excel_name.xlsx") / 1000;
            while ($this->excel_size_check_times < 2 && (!$generated_file_size || ($generated_file_size > 160 && $generated_file_size < 180))) {
                $this->excel_size_check_times++;
                $this->open_save_close(date('Y_m_d') . "/$excel_name");
                clearstatcache();
                $generated_file_size = filesize("./root/Cases/" . date('Y_m_d') . "/$excel_name.xlsx") / 1000;
            }
            clearstatcache();
            $generated_file_size = filesize("./root/Cases/" . date('Y_m_d') . "/$excel_name.xlsx") / 1000;
            if (!$generated_file_size || !($generated_file_size > 160 && $generated_file_size < 180)) {
                save_audit("file size error ". $generated_file_size ." for case -- " . $case_name);
                unlink("./root/Cases/" . date('Y_m_d') . "/$excel_name.xlsx");
                echo 'file_size_error';
                return FALSE;
            }
            save_audit("successfully generated file size ". $generated_file_size ." for case -- " . $case_name);
            return $excel_name;
            // echo "root/Cases/" . date('d-m-Y') . "/$excel_name.xlsx";
        }
    }

    function get_size() {
        clearstatcache();
        echo filesize("./root/Cases/" . date('d-m-Y') . "/M1B1234C155436IRanaEmadD14042020.xlsx") / 1000;
    }

    function check_generated_excel($file_path, $case_id) {
        if (!file_exists($file_path)) {
            $this->excel_check_times++;
            $this->generate_excel($case_id);
        }
    }

    function not_approve() {
        $this->db->update('cases', array('status' => 'borrower_assessment', 'approved' => 0, 'approve_success' => 0), array('id' => 226));
    }

    function show_data() {
        print_R($this->get_case_hist(226));
    }

    function open_save_close($file_path) {
//        root/Cases/
        $single_file_name = explode('/', $file_path)[1];
        $this->replace_string_in_file(getcwd() . '\root\Cases\script.ps1', '$file_name$', $single_file_name);
        $this->replace_string_in_file(getcwd() . '\root\Cases\script.ps1', '$file$', $file_path);
        $this->replace_string_in_file(getcwd() . '\root\Cases\script.ps1', '$file_path$', getcwd() . '/root/Cases/' . $file_path);

        echo shell_exec('powershell.exe  -command Set-ExecutionPolicy Bypass -Scope CurrentUser');
        echo shell_exec('powershell.exe  -command ' . getcwd() . '\root\Cases\script.ps1');

        //$this->replace_string_in_file(getcwd() . '\root\Cases\script.ps1', $file_path, '$file$');
        //$this->replace_string_in_file(getcwd() . '\root\Cases\script.ps1', getcwd() . '/root/Cases/$file$', '$file_path$');
        //$this->replace_string_in_file(getcwd() . '\root\Cases\script.ps1', $single_file_name, '$file_name$');
        $this->replace_whole_file_content();
        //sleep for 3 seconds
        sleep(3);
        if (file_exists("./root/Cases/" . $file_path . "001.xlsx")) {
            unlink("./root/Cases/" . $file_path . ".xlsx");
            rename("./root/Cases/" . $file_path . "001.xlsx", "./root/Cases/" . $file_path . ".xlsx");
        }
        $this->check_duplicate($file_path);
    }

    function open_save_close_test() {
        $file_path = '30-02-2020/MPC04000001B0210C21557IezzatmohamedD23012020';
//        root/Cases/
        $single_file_name = explode('/', $file_path)[1];
        $this->replace_string_in_file(getcwd() . '\root\Cases\script.ps1', '$file_name$', $single_file_name);
        $this->replace_string_in_file(getcwd() . '\root\Cases\script.ps1', '$file$', $file_path);
        $this->replace_string_in_file(getcwd() . '\root\Cases\script.ps1', '$file_path$', getcwd() . '/root/Cases/' . $file_path);

        echo shell_exec('powershell.exe  -command Set-ExecutionPolicy Bypass -Scope CurrentUser');
        echo shell_exec('powershell.exe  -command ' . getcwd() . '\root\Cases\script.ps1');

        // $this->replace_string_in_file(getcwd() . '\root\Cases\script1.ps1', $file_path, '$file$');
        // $this->replace_string_in_file(getcwd() . '\root\Cases\script1.ps1', getcwd() . '/root/Cases/$file$', '$file_path$');
        // $this->replace_string_in_file(getcwd() . '\root\Cases\script1.ps1', $single_file_name, '$file_name$');
        $this->replace_whole_file_content();
        sleep(3);
        if (file_exists("./root/Cases/" . $file_path . "001.xlsx")) {
            unlink("./root/Cases/" . $file_path . ".xlsx");
            rename("./root/Cases/" . $file_path . "001.xlsx", "./root/Cases/" . $file_path . ".xlsx");
        }
        $this->check_duplicate($file_path);
    }

    function replace_string_in_file($filename, $string_to_replace, $replace_with) {
        $content = file_get_contents($filename);
        $content_chunks = explode($string_to_replace, $content);
        $content = implode($replace_with, $content_chunks);
        file_put_contents($filename, $content);
    }

    function replace_whole_file_content() {
        $filename = getcwd() . '\root\Cases\script.ps1';
//        $content = '$excel = New-Object -ComObject Excel.Application
//$Workbook = $Excel.Workbooks.Open("$file_path$.xlsx",0, $false)
//$Workbook.SaveAs("$file_path$001.xlsx")
//$Workbook.Close($true)
//$excel.Quit()
//Remove-Item –path $file_path$.xlsx –recurse 
//Rename-Item -Path "$file_path$001.xlsx" -NewName "$file_name$.xlsx"';
        $content = '$excel = New-Object -ComObject Excel.Application
$Workbook = $Excel.Workbooks.Open("$file_path$.xlsx",0, $false)
$Workbook.Close($true)
$excel.Quit()';
        file_put_contents($filename, $content);
    }

    function test_shell() {
        //  echo shell_exec('powershell.exe  -command Set-ExecutionPolicy Bypass -Scope CurrentUser');
        echo shell_exec('powershell.exe  -command ' . getcwd() . '\root\Cases\script.ps1');
    }

    function validate_C36_date($date) {
        $date = sanitize($date);
        $this->form_validation->set_message('validate_C36_date', $this->lang->line("C36_date_msg"));
        $start = strtotime("2014-01-01");
        $end = strtotime("2025-12-31");
        $date = strtotime($date);
        $today = strtotime(date("Y-m-d"));
        if ($date > $today) {
            $this->form_validation->set_message('validate_C36_date', $this->lang->line("C36_today_date_msg"));
            return FALSE;
        }
        if ($date >= $start && $date <= $end) {
            return TRUE;
        }
        return FALSE;
    }

// for generate pdf
    public function get_html($case_id) {
        $this->load->model('admin/Cases_two_model');
        //now pass the data//
        $view_only = 1;
        $data['title'] = $this->lang->line("borrower_factsheet");
        $data['factsheet_data'] = $this->Cases_two_model->get_factsheet_data($case_id);
        $data['line_activity'] = get_value_from_code("c10", $this->Cases_two_model->get_line_of_activity_value($case_id), 1);
        $data['is_case_pass_this_setp'] = $this->Cases_two_model->if_curr_stage_pass($case_id, 'borrower_factsheet');
        $data['label_att'] = 'col-sm-3 control-label';
        if ($view_only != NULL) {
            $data['view_only'] = 1;
        }
        $data['case_id'] = $case_id;
        $sum_share = 1;
        $data['sum_share_error'] = $sum_share > 100 ? TRUE : FALSE;
        ///
        $row = [];
        if (!empty($case_id)) {
            $row = $this->Cases_model->get_bi_arr($case_id);
        }
        $data['row'] = $row;
        /////
        $data['borrower_info_year'] = $this->Cases_two_model->get_borrower_info_year($case_id);
        $data['borrower_info_date'] = $this->Cases_two_model->get_borrower_info_date($case_id);
        $data['borrower_info_data'] = $this->Cases_two_model->get_borrower_information($case_id);
        $data['financial_data'] = $this->Cases_two_model->get_financials_data($case_id);
        $data['is_case_pass_this_setp'] = $this->Cases_two_model->if_curr_stage_pass($case_id, 'borrower_financials');

        ////
        $condition = 1;
        $data['assessment_data'] = $this->Cases_two_model->get_assessment_data($case_id);

        $data['is_case_pass_this_setp'] = $this->Cases_two_model->if_curr_stage_pass($case_id, 'borrower_assessment');
        $data['case_id'] = $case_id;
        $get_case = $this->Crud_model->get_one("cases", $case_id);
        $data["approved"] = $get_case->approved;
        $data["case_status"] = $get_case->status;
        $data['condition'] = $condition;
        $data["param_39"] = $this->compute_39($case_id);
        $data["param_40"] = $this->compute_40($case_id);

        $hist_years = $this->Cases_two_model->get_hist_years($case_id);
        if (!empty($hist_years)) {
            sort($hist_years);
        }
        $projected_years = $this->Cases_two_model->get_projected_years($case_id);
        if (!empty($projected_years)) {
            sort($projected_years);
        }
        $data['hist_years'] = $hist_years;
        $data['projected_years'] = $projected_years;
        $data['hist'] = $this->get_case_hist($case_id);
        $data['proj'] = $this->get_case_projected($case_id);

        $this->db->where("case_id", $case_id);
        $calc_arr = $this->db->get("calculations")->result();
        $calc = [];
        foreach ($calc_arr as $one) {
            $calc[$one->rown]['C'] = $one->C;
            $calc[$one->rown]['D'] = $one->D;
            $calc[$one->rown]['E'] = $one->E;
            $calc[$one->rown]['F'] = $one->F;
            $calc[$one->rown]['G'] = $one->G;
        }
        $data['calc'] = $calc;
        $html = $this->load->view('admin/pages/cases/export_pdf', $data, true); //load the pdf.php by passing our data and get all data in $html varriable.
        return $html;
    }

    function load_pdf($case_id) {
        $this->load->model('admin/Cases_two_model');
        //load mPDF library
        $this->load->library('m_pdf');
        //now pass the data//
        $view_only = 1;
        $data['title'] = $this->lang->line("borrower_factsheet");
        $data['factsheet_data'] = $this->Cases_two_model->get_factsheet_data($case_id);
        $data['line_activity'] = $this->Cases_two_model->get_line_of_activity_value($case_id);
        $data['is_case_pass_this_setp'] = $this->Cases_two_model->if_curr_stage_pass($case_id, 'borrower_factsheet');
        $data['label_att'] = 'col-sm-3 control-label';
        if ($view_only != NULL) {
            $data['view_only'] = 1;
        }
        $data['case_id'] = $case_id;
        $sum_share = 1;
        $data['sum_share_error'] = $sum_share > 100 ? TRUE : FALSE;
        ///
        $row = [];
        if (!empty($case_id)) {
            $row = $this->Cases_model->get_bi_arr($case_id);
        }
        $data['row'] = $row;
        /////
        $data['borrower_info_year'] = $this->Cases_two_model->get_borrower_info_year($case_id);
        $data['borrower_info_date'] = $this->Cases_two_model->get_borrower_info_date($case_id);
        $data['borrower_info_data'] = $this->Cases_two_model->get_borrower_information($case_id);
        $data['financial_data'] = $this->Cases_two_model->get_financials_data($case_id);
        $data['is_case_pass_this_setp'] = $this->Cases_two_model->if_curr_stage_pass($case_id, 'borrower_financials');

        ////
        $condition = 1;
        $data['assessment_data'] = $this->Cases_two_model->get_assessment_data($case_id);
        $data['is_case_pass_this_setp'] = $this->Cases_two_model->if_curr_stage_pass($case_id, 'borrower_assessment');
        $data['case_id'] = $case_id;
        $get_case = $this->Crud_model->get_one("cases", $case_id);
        $data["approved"] = $get_case->approved;
        $data["case_status"] = $get_case->status;
        $data['condition'] = $condition;
        $data["param_39"] = $this->compute_39($case_id);
        $data["param_40"] = $this->compute_40($case_id);
        $this->load->view('admin/pages/cases/export_pdf', $data);
    }

    function compute_40($case_id = NULL) {
        $row = $this->Cases_two_model->get_financials_row($case_id, 3);
        $return = "NA";
        $C218 = 0;

        //modified2
        $calc = $this->Cases_two_model->get_calc($case_id);
//         $this->db->where("rown", 219);
//        $this->db->where("case_id", $case_id);
//        $calc = $this->db->get("calculations")->row();
//        $calc=[];
//            foreach ($calc_arr as $one){
//                $calc[$one->rown]['C'] = $one->C;
//                $calc[$one->rown]['D'] = $one->D;
//                $calc[$one->rown]['E'] = $one->E;
//                $calc[$one->rown]['F'] = $one->F;
//                $calc[$one->rown]['G'] = $one->G;
//            }
//            $data['calc']=$calc;  


        if (!empty($row)) {
            if ($row->E != "no") {
                $C218 = $this->get_numeric($calc->C) + $this->get_numeric($calc->D) + $this->get_numeric($calc->E);
            }
        }
        if ($C218 == 3) {
            $return = 1;
        } elseif ($C218 == 2) {
            $return = 0.9;
        } elseif ($C218 == 1) {
            $return = 0.8;
        }
        return $return;
    }

    function compute_39($case_id = NULL) {
        //VLOOKUP('Borrower Financials'!E211,'Borrower Assessment'!$M$221:$O$227,3,TRUE)
        //IF(E3="No",0,
        //IF(
        //AND(ISNUMBER('Borrower Information'!D32),'Borrower Information'!D32=0)
        //,999,IF(ISERROR(E210/'Borrower Information'!$D$32),"NA",(E210/'Borrower Information'!$D$32))))
//        if(bfE3=="No"){
//           $return =0;
//        }
//        elseif(isnumber(biD32) && biD32==0){
//            $return=999;
//        }
//        elseif(ISERROR(E210/'Borrower Information'!$D$32){
//            $return="NA";
//        }
//        else{
//            $return= bfE210/'Borrower Information'!$D$32;
//        }
//        IF(ISERROR(AVERAGE('Borrower Information'!$D$25:$D$30)),"NA",AVERAGE('Borrower Information'!$D$25:$D$30))
        $return = "NA";
        $borrower_information = $this->Cases_two_model->get_borrower_information($case_id);
        $row_3 = $this->Cases_two_model->get_financials_row($case_id, 3);
        if (!empty($borrower_information) && !empty($row_3)) {
            if ($row_3->E == "no") {
                $return = "NA";
            } elseif (is_numeric($borrower_information->D32) && $borrower_information->D32 === 0) {
                $return = 999;
            } else {

                $avg = 0;
                for ($i = 25; $i <= 30; $i++) {
                    $index = "D$i";
                    if (!is_numeric($borrower_information->$index)) {
                        $avg = "NA";
                        break;
                    }
                    $avg +=$borrower_information->$index;
                }
                if ($avg != "NA" && is_numeric($borrower_information->D32)) {
                    $avg = $avg / 6;
                    $return = $avg / $borrower_information->D32;
                } else {
                    $return = "NA";
                }
            }
        }
        $option = "N/A";
        if (is_numeric($return)) {
            if ($return >= 2) {
                $option = ">=2";
            } elseif ($return >= 1.7 && $return < 2) {
                $option = ">=1.7 to <2";
            } elseif ($return >= 1.5 && $return < 1.7) {
                $option = ">=1.5 to <1.7";
            } elseif ($return >= 1.3 && $return < 1.5) {
                $option = ">=1.3 to <1.5";
            } elseif ($return >= 1 && $return < 1.3) {
                $option = ">=1 to <1.3";
            } elseif ($return < 1) {
                $option = "<1";
            }
        }
        return $option;
    }

    function non_zero($value) {
        $this->form_validation->set_message('non_zero', $this->lang->line('non_zero'));
        if ($value === 0) {
            return FALSE;
        }
        return TRUE;
    }

    function set_zero($row) {
        if (empty($row)) {
            $row = new stdClass();
            $row->C = 0;
            $row->D = 0;
            $row->E = 0;
            $row->F = 0;
            $row->G = 0;
        }
        return $row;
    }

    public
            function export_pdf($case_id) {
        ini_set('memory_limit', '2048M');
        set_time_limit(1800);
        $case = $this->Crud_model->get_one("cases", $case_id);
        $pdf_title = "Export_Case";
        if (!empty($case->name) && $case->name != "") {
            $pdf_title = "Export_" . $case->name;
        }
        //get pdf object
        $this->load->library('pdf');
        $font_size = 10;
        $pdf = new Pdf("P", 'mm', PDF_PAGE_FORMAT, true, 'UTF-8', false, false, 'invoice');

        $pdf->SetTitle($pdf_title);
        $this->pdf->SetMargins(0, 25, 0);

        $this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        $pdf->SetAuthor("iScore");
        $pdf->SetFont("dejavusanscondensed", '', "10");
        $pdf->AddPage();
// Print text using writeHTMLCell()
        $html = $this->get_html($case_id);
        //echo $html;die();
        ob_start();
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        ob_end_clean();
        $pdf->Output($pdf_title . '.pdf', 'D');
    }

    function get_pdf() {
        $this->load->library('pdf');
        $font_size = 10;
        $pdf = new Pdf("P", 'mm', PDF_PAGE_FORMAT, true, 'UTF-8', false, false, 'invoice');

        $pdf->SetTitle("Hi");
        $this->pdf->SetMargins(0, 25, 0);

        $this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        $pdf->SetAuthor("iScore");
        $pdf->SetFont("dejavusanscondensed", '', "10");
//    $pdf->setImageScale(1.53);
//    $pdf->setJPEGQuality(100);
//    $pdf->AddPage($html, $formatArray['format']);
        $pdf->AddPage();

        return $pdf;
    }

    function save_calc() {
        $case_id = $this->input->post("case_id");
        $calc = $this->input->post("calc");
        $first_hist = $calc['first_hist'];
        $last_hist = $calc['last_hist'];
        $first_proj = $calc['first_proj'];
        $last_proj = $calc['last_proj'];
        $this->db->where("case_id", $case_id);
        $this->db->delete("calculations");
        $incremented = incremented("calculations");
        $hist_incremented = incremented("hist_fins");
        $proj_incremented = incremented("projected_fins");
        for ($i = 12; $i <= 224; $i++) {
            if (isset($calc["C$i"]) || isset($calc["D$i"]) || isset($calc["E$i"]) || isset($calc["F$i"]) || isset($calc["G$i"])) {
                $one = array(
                    "id" => $incremented,
                    "case_id" => $case_id,
                    "rown" => $i,
                    "C" => isset($calc["C$i"]) ? $calc["C$i"] : NULL,
                    "D" => isset($calc["D$i"]) ? $calc["D$i"] : NULL,
                    "E" => isset($calc["E$i"]) ? $calc["E$i"] : NULL,
                    "F" => isset($calc["F$i"]) ? $calc["F$i"] : NULL,
                    "G" => isset($calc["G$i"]) ? $calc["G$i"] : NULL
                );
                $this->db->insert("calculations", $one);
                $incremented++;
            }

            for ($j = $first_hist; $j < $last_hist; $j++) {
                if (isset($calc[$j . $i])) {
                    $this->db->where("case_id", $case_id);
                    $this->db->where("rown", $i);
                    $this->db->where('year', $j);
                    $this->db->delete("hist_fins");
                    $insert = array(
                        'id' => $hist_incremented,
                        "case_id" => $case_id,
                        "rown" => $i,
                        "year" => $j,
                        "value" => !empty($calc[$j . $i]) ? $calc[$j . $i] : 0,
                        "hidden" => 0
                    );
                    $this->db->insert("hist_fins", $insert);
                    $hist_incremented++;
                }
            }
            for ($k = $first_proj + 1; $k <= $last_proj; $k++) {
                if (isset($calc[$k . $i])) {
                    $this->db->where("case_id", $case_id);
                    $this->db->where("rown", $i);
                    $this->db->where('year', $k);
                    $this->db->delete("projected_fins");
                    $insert_proj = array(
                        'id' => $proj_incremented,
                        "case_id" => $case_id,
                        "rown" => $i,
                        "year" => $k,
                        "value" => !empty($calc[$k . $i]) ? $calc[$k . $i] : 0,
                        "hidden" => 0
                    );
                    $this->db->insert("projected_fins", $insert_proj);
                    $proj_incremented++;
                }
            }
        }
        echo "success";
    }

    function test_2() {
        print_R($this->db->get_where('hist_fins', array('case_id' => 74))->result());
    }

    //modified2
    function get_numeric($value) {
        if (is_numeric($value)) {
            return $value;
        }
        return 0;
    }

//    END MODIFIED

    function validate_excel_name($value, $id) {
        $this->form_validation->set_message('validate_excel_name', $this->lang->line("excel_name_msg"));
        $user = $this->Cases_model->get_user_info($this->session->userdata("user_id"));
        $name = "M" . $user->code . "B" . $user->branch_code . "C" . $this->input->post("C5") . "I" .
                $user->first_name . $user->last_name;
        $name = str_replace(" ", "", $name);
        $check_name = $this->Cases_model->check_case($name, $id);
        if (empty($check_name)) {
            return TRUE;
        }
        return FALSE;
    }

    function get_case_projected($case_id) {
        $projected = $this->Cases_two_model->get_case_projected($case_id);
        $sorted = [];
        foreach ($projected as $one) {
            $sorted[$one->rown][$one->year] = $one->value;
        }
        return $sorted;
    }

    function get_case_hist($case_id) {
        $this->load->model("admin/Cases_two_model");
        $hist = $this->Cases_two_model->get_case_hist($case_id);
        $sorted = [];
        foreach ($hist as $one) {
            $sorted[$one->rown][$one->year] = $one->value;
        }
        return $sorted;
    }

    function check_duplicate($file_path) {
        $this->load->helper('directory');
        $dir = explode("/", $file_path);
        $path = "./root/Cases/$dir[0]/";
        $file_name = $dir[1];
        $ext = "csv";
        $map = directory_map($path, 1);
        foreach ($map as $file) {
            $file = explode(".", $file);
            $file_ext = $file[1];
            $file = $file[0];
            if (strpos($file, $file_name) === 0) {
                if ($file !== $file_name && $file_ext == $ext) {
                    echo $file;
                    if (file_exists($path . $file_name . ".$ext")) {
                        unlink($path . $file_name . ".$ext");
                    }
                    if (!rename($path . $file . ".$ext", $path . $file_name . ".$ext")) {
                        if (copy($path . $file . ".$ext", $path . $file_name . ".$ext")) {
                            unlink($path . $file . ".$ext");
                        }
                    }
                }
            }
        }
    }

    function update_crn_name($case_id, $old_C5 = NULL) {
        $excel_name = $this->Cases_model->update_case_name($case_id, $old_C5);
        if ($excel_name) {
            $this->db->update('cases', array('name' => $excel_name), array('id' => $case_id));
        }
    }

    function check_update_notification($old_case_arr) {
        //checks if the verifier made a change to a case before submission and notifies its maker via email
        $case_id = $old_case_arr["case_id"];
        $case_data = $this->Crud_model->get_one("cases", $case_id);
        //check case status and that the user is verifier
//        if($case_data->status =="saved" || !compare_level("verifier")){
        if ($this->session->userdata("user_id") == $case_data->user_id || $case_data->approved) {
            return;
        }
        $updated_case = $this->Cases_model->get_bi_arr($case_id);
        $changed = FALSE;
        foreach ($updated_case as $key => $value) {
            if ($old_case_arr[$key] != $value) {
                $changed = TRUE;
                break;
            }
        }
        if ($changed) {
            $this->send_update_notification($case_data->user_id, $case_data->name);
        }
    }

    function send_update_notification($user_id, $case_name) {
        $user = $this->Crud_model->get_one("users", $user_id);
        $email_settings = $this->Cases_model->get_email_settings();
        if (!empty($email_settings)) {
//            begin msg
            $msg_content = "";
            $name = $user->first_name . " " . $user->last_name;
            $msg_content .= "Hello $name! \r\n";
            $msg_content .= "Your case $case_name has been updated.\r\n";
            $msg_content .= "For more information, please login " . base_url() . "  \r\n";
            $msg_content .= "Thank you!";
//            end msg
            // config email smtp
            $settings = $this->db->get('email_settings')->row();
            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => $settings->smtp_host_b,
                'mailtype' => 'text',
                'charset' => 'iso-8859-1',
                'newline' => "\r\n"
            );
            $this->load->library('email');
            $this->email->initialize($config);
            $this->email->from($email_settings->host_mail_b);
            $this->email->to($user->email);
            $this->email->subject('Case Updated');
            $this->email->message($msg_content);
            $this->email->set_newline("\r\n");
            $sent = $this->email->send();
        }
    }

    function get_bank_users() {
        $case_id = $this->input->post('case_id');

        $this->db->select('users.bank_id');
        $this->db->join('users', 'users.id = cases.user_id');
        $case_user = $this->db->get_where('cases', array('cases.id' => $case_id))->row();
        $users = array();
        if ($case_user) {
            $this->db->select('id,first_name,last_name');
            $users = $this->db->get_where('users', array('bank_id' => $case_user->bank_id))->result();
        }
        echo json_encode($users);
    }

}
