<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Accesslevels extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin/Accesslevels_model');
        $this->load->model('admin/Back_settings_model');
        $this->load->library('session');
        check_login();
        if ($this->session->userdata('language') != "") {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        } else {
            $this->lang->load('default');
            $this->lang->load('default_1');
        }
        $this->session->set_userdata('active', 'settings');
    }

    function index() {
        if (substr($this->session->userdata('levels')->accleveltype, 0, 1) == '1') {
            $data = array('title' => $this->lang->line('access_levels'));
            $data['attributes'] = array('class' => 'form-horizontal', 'role' => 'form');
            $data['label_att'] = array('class' => 'col-sm-4 control-label', 'style' => 'top:0 !important;');
            $data['levels'] = $this->Accesslevels_model->getLevels();
            $data['funcs'] = $this->Accesslevels_model->getDbComments();
            $data['cols'] = $this->Accesslevels_model->getDbColumns();
            $data['settings'] = $this->Back_settings_model->get_back_settings();
            $this->session->set_userdata('active_menu', 'AccessLevels');
            $this->load->view("admin/pages/accesslevels/useraccesslevels", $data);
        } else {
            redirect('admin/Dashboard');
        }
    }

    function add_access_level() {
        if (substr($this->session->userdata('levels')->accleveltype, 0, 1) == '1' && substr($this->session->userdata('levels')->accleveltype, 1, 1) == '1') {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('level_name', 'level name', 'trim|required|xss_clean');
            if ($this->form_validation->run() == FALSE) {
                $this->index();
            } else {

                $data = array('name' => sanitize($this->input->post('level_name')), 'created' => date('Y-m-d H:i:s'));
				$data=increment("accesslevels",$data);
                /**/ $this->Global_model->global_insert('accesslevels', $data);
                // audit
                save_audit("insert new access level " . sanitize($this->input->post('level_name')));
                /////////

                $this->session->set_flashdata('msg', $this->lang->line('access_level_added_successfully'));
                redirect('admin/Accesslevels');
            }
        } else {
            redirect('admin/Dashboard');
        }
    }

    function do_delete_access_level() {
        if (substr($this->session->userdata('levels')->accleveltype, 3, 1) == '1') {
            /**/ $this->Accesslevels_model->do_delete_access_level(sanitize($this->input->post('id')));
            // audit
            save_audit("delete access level" . $this->getLevelName(sanitize($this->input->post('id'))));
            /////////
            $this->session->set_flashdata('msg', $this->lang->line('access_level_deleted_successfully'));
            echo json_encode(TRUE);
        }
    }

    function addNewAccessLevel() {
        if (substr($this->session->userdata('levels')->accleveltype, 0, 1) == '1' && substr($this->session->userdata('levels')->accleveltype, 1, 1) == '1') {
            $cols = $this->Accesslevels_model->getDbColumns(); //get column name of accesslevelfunctions table
            $data['levelid'] = sanitize($this->input->post('levelid'));
            $i = 0;
            foreach ($cols as $col) {
                $col_name = $col->COL;
                if ($col_name == "bank_only" || $col_name == "branch_only" || $col_name == "approved_only" || $col_name == "completed_only") {
                    $data[$col_name] = sanitize($this->input->post($col_name));
                    continue;
                }
                if ($col_name != 'id' && $col_name != 'levelid') {
                    $data[$col_name] = $this->checkChecked(sanitize($this->input->post($i)));
                    $i++;
                }
            }
            $this->Accesslevels_model->addAccessLevel($data);
            // audit
            save_audit("update access level permissions" . $this->getLevelName($data['levelid']));
            /////////
            redirect('admin/Accesslevels');
        } else {
            redirect('admin/Dashboard');
        }
    }

    function checkChecked($input) {
        $input = sanitize($input);

        if (isset($input)) {
            if (isset($input[0]) && $input[0] == 'on') {
                $one = 1;
            } else {
                $one = 0;
            }
            if (isset($input[1]) && $input[1] == 'on') {
                $two = 1;
            } else {
                $two = 0;
            }
            if (isset($input[2]) && $input[2] == 'on') {
                $three = 1;
            } else {
                $three = 0;
            }
            if (isset($input[3]) && $input[3] == 'on') {
                $four = 1;
            } else {
                $four = 0;
            }
            $number = $one . $two . $three . $four;

            return $number;
        } else {
            return '0000';
        }
    }

    function getLevelIdInfo() {
        if (substr($this->session->userdata('levels')->accleveltype, 0, 1) == '1') {
            $postData = $this->input->post();
            $this->db->where('levelid', $postData['levelid']);
            $result = $this->db->get('accesslevelsfunctions')->row();

            if ($result) {
                $json = json_encode($result);
                echo $json;
            } else {
                $json = json_encode('no');
                echo $json;
            }
        } else {
            redirect('admin/Dashboard');
        }
    }

    function edit() {
        if (substr($this->session->userdata('levels')->accleveltype, 0, 1) == '1' && substr($this->session->userdata('levels')->accleveltype, 2, 1) == '1') {
            $postData = $this->input->post();
            $this->db->where('id', $postData['editLevelId']);
            /**/ $this->db->update('accesslevels', array('name' => $postData['level_name']));
            $this->session->set_flashdata('msg', $this->lang->line('access_level_updated_successfully'));
            redirect('admin/Accesslevels');
            // audit
            save_audit("update access level " . $this->getLevelName($postData['editLevelId']));
            /////////
        } else {
            redirect('admin/Dashboard');
        }
    }

    function getLevelName($id) {
        $id = sanitize($id);
        $this->db->where('id', $id);
        $this->db->select('name');
        $levelName = $this->db->get('accesslevels')->row();
        if ($levelName) {
            return $levelName->name;
        }
        return '';
    }

    function delete() {
        if (substr($this->session->userdata('levels')->accleveltype, 0, 1) == '1' && substr($this->session->userdata('levels')->accleveltype, 3, 1) == '1') {
            $id = sanitize($this->input->post('levelId'));
            $this->db->where('levelid', $id);
            /**/ $this->db->delete('accesslevelsfunctions');

            $this->db->where('id', $id);
            /**/ $this->db->delete('accesslevels');

            $this->db->where('accesslevelid', $id);
            /**/ $this->db->update('employees', array('accesslevelid' => 1)); //set all emps have this deleted access level to super admin access level
            // audit
            save_audit("delete access level " . $this->getLevelName($id));
            /////////
            redirect('admin/Accesslevels');
        } else {
            redirect('admin/Dashboard');
        }
    }

    function getEmpsTakeLevel() {
        $levelId = $this->input->post('levelid');
        $this->db->select("CONCAT((firstname),(' '),(middle),(' '),(lastname)) as empname");
        echo json_encode($this->db->get_where('employees', array('accesslevelid' => $levelId))->result());
    }

    function update_access_level() {
        if (substr($this->session->userdata('levels')->accleveltype, 0, 1) == '1' && substr($this->session->userdata('levels')->accleveltype, 2, 1) == '1') {
            $postData = $this->input->post();
            $updateArr['name'] = $postData['acc_level_name'];
            $id = $postData['id'];
            /**/ $this->Global_model->global_update('accesslevels', $id, $updateArr);
            // audit
            save_audit("update access level " . $this->getLevelName($id));
            /////////
            $this->session->set_flashdata('msg', $this->lang->line('access_level_updated_successfully'));
            echo json_encode(TRUE);
        }
    }

}
