<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dbsync extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->dbforge();
    }

    function index() {
        $fields = array(
            'code' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true
            ),
            'value' => array(
                'type' => 'VARCHAR',
                'constraint' => '500',
                'null' => true
            ),
            'deleted' => array(
                'type' => 'INT',
                'constraint' => '1',
                'default' => '0'
            )
        );
        $this->create_table("c6", $fields);
        /////////////////////////////////////
    }

    function create_table($table, $fields) {
        $this->dbforge->add_field('id');
//        $fields = array(
//        'blog_id' => array(
//                'type' => 'INT',
//                'constraint' => 5,
//                'unsigned' => TRUE,
//                'auto_increment' => TRUE
//        ),
//        'blog_title' => array(
//                'type' => 'VARCHAR',
//                'constraint' => '100',
//                'unique' => TRUE,
//        ),
//        'blog_author' => array(
//                'type' =>'VARCHAR',
//                'constraint' => '100',
//                'default' => 'King of Town',
//        ),
//        'blog_description' => array(
//                'type' => 'TEXT',
//                'null' => TRUE,
//        ),
//);
        $this->dbforge->add_field($fields);
        $this->dbforge->create_table($table);
        echo "$table Created Successfully!";
    }

    function rename_table($old_table, $new_table) {
        $this->dbforge->rename_table($old_table, $new_table);
        echo "$table Renamed Successfully!";
    }

    function alter_table($table, $fields) {
        $this->dbforge->add_column($table, $fields);
        echo "$table Altered Successfully!";
    }

    function modify_column($table, $col, $data) {
        $fields = array(
            $col => $data
        );
        $this->dbforge->modify_column($table, $fields);
    }

    function c6() {
        $fields = array(
            'code' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true
            ),
            'value' => array(
                'type' => 'VARCHAR',
                'constraint' => '500',
                'null' => true
            ),
            'deleted' => array(
                'type' => 'INT',
                'constraint' => '1',
                'default' => '0'
            )
        );
        $this->create_table("c6", $fields);
        /////////////////////////////////////
    }

    function c9() {
        $fields = array(
            'code' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true
            ),
            'value' => array(
                'type' => 'VARCHAR',
                'constraint' => '500',
                'null' => true
            ),
            'deleted' => array(
                'type' => 'INT',
                'constraint' => '1',
                'default' => '0'
            )
        );
        $this->create_table("c9", $fields);
        /////////////////////////////////////
    }

    function c10() {
        $fields = array(
            'code' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true
            ),
            'value' => array(
                'type' => 'VARCHAR',
                'constraint' => '500',
                'null' => true
            ),
            'deleted' => array(
                'type' => 'INT',
                'constraint' => '1',
                'default' => '0'
            )
        );
        $this->create_table("c10", $fields);
        /////////////////////////////////////
    }

    function c11() {
        $fields = array(
            'code' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true
            ),
            'value' => array(
                'type' => 'VARCHAR',
                'constraint' => '500',
                'null' => true
            ),
            'deleted' => array(
                'type' => 'INT',
                'constraint' => '1',
                'default' => '0'
            )
        );
        $this->create_table("c11", $fields);
        /////////////////////////////////////
    }

//    BEGIN BORROWER ASSESSMENT
    function f5() {
        $fields = array(
            'code' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true
            ),
            'value' => array(
                'type' => 'VARCHAR',
                'constraint' => '500',
                'null' => true
            ),
            'deleted' => array(
                'type' => 'INT',
                'constraint' => '1',
                'default' => '0'
            )
        );
        $this->create_table("f5", $fields);
        /////////////////////////////////////
    }

    function f10() {
        $fields = array(
            'code' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true
            ),
            'value' => array(
                'type' => 'VARCHAR',
                'constraint' => '500',
                'null' => true
            ),
            'deleted' => array(
                'type' => 'INT',
                'constraint' => '1',
                'default' => '0'
            )
        );
        $this->create_table("f10", $fields);
        /////////////////////////////////////
    }

    function f18() {
        $fields = array(
            'code' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true
            ),
            'value' => array(
                'type' => 'VARCHAR',
                'constraint' => '500',
                'null' => true
            ),
            'deleted' => array(
                'type' => 'INT',
                'constraint' => '1',
                'default' => '0'
            )
        );
        $this->create_table("f18", $fields);
        /////////////////////////////////////
    }

    function f22() {
        $fields = array(
            'code' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true
            ),
            'value' => array(
                'type' => 'VARCHAR',
                'constraint' => '500',
                'null' => true
            ),
            'deleted' => array(
                'type' => 'INT',
                'constraint' => '1',
                'default' => '0'
            )
        );
        $this->create_table("f22", $fields);
        $this->create_table("f29", $fields);
        $this->create_table("f36", $fields);
        $this->create_table("f40", $fields);
        $this->create_table("f45", $fields);
        $this->create_table("f50", $fields);
        $this->create_table("f56", $fields);
        $this->create_table("f66", $fields);
        $this->create_table("f71", $fields);
        $this->create_table("f75", $fields);
        $this->create_table("f79", $fields);
        $this->create_table("f86", $fields);
        $this->create_table("f93", $fields);
        $this->create_table("f100", $fields);
        $this->create_table("f107", $fields);
        $this->create_table("f115", $fields);
        $this->create_table("f121", $fields);
        $this->create_table("f125", $fields);
        $this->create_table("f129", $fields);
        $this->create_table("f133", $fields);
        $this->create_table("f137", $fields);
        $this->create_table("f144", $fields);
        $this->create_table("f151", $fields);
        $this->create_table("f155", $fields);
        $this->create_table("f159", $fields);
        $this->create_table("f165", $fields);
        $this->create_table("f169", $fields);
        $this->create_table("f175", $fields);
        $this->create_table("f179", $fields);
        $this->create_table("f186", $fields);
        $this->create_table("f194", $fields);
        $this->create_table("f200", $fields);
        $this->create_table("f208", $fields);
        $this->create_table("f216", $fields);
        $this->create_table("f221", $fields);

        /////////////////////////////////////
    }

    function add_hide_column() {
        $col = array(
            'hidden' => array(
                'type' => 'INT',
                'null' => FALSE,
                'default' => 0
            )
        );
        $this->alter_table("hist_fins", $col);
        $this->alter_table("projected_fins", $col);
    }

    function add_arabic_select_inputs_column() {
        $fields = array(
            'arabic_value' => array(
                'type' => 'NVARCHAR2',
                'constraint' => '2000',
                'null' => true
            )
        );
        $this->alter_table("c6", $fields);
        $this->alter_table("c9", $fields);
        $this->alter_table("c10", $fields);
        $this->alter_table("c11", $fields);
        $this->alter_table("f5", $fields);
        $this->alter_table("f10", $fields);
        $this->alter_table("f18", $fields);
        $this->alter_table("f22", $fields);
        $this->alter_table("f29", $fields);
        $this->alter_table("f36", $fields);
        $this->alter_table("f40", $fields);
        $this->alter_table("f45", $fields);
        $this->alter_table("f50", $fields);
        $this->alter_table("f56", $fields);
        $this->alter_table("f66", $fields);
        $this->alter_table("f71", $fields);
        $this->alter_table("f75", $fields);
        $this->alter_table("f79", $fields);
        $this->alter_table("f86", $fields);
        $this->alter_table("f93", $fields);
        $this->alter_table("f100", $fields);
        $this->alter_table("f107", $fields);
        $this->alter_table("f115", $fields);
        $this->alter_table("f121", $fields);
        $this->alter_table("f125", $fields);
        $this->alter_table("f129", $fields);
        $this->alter_table("f133", $fields);
        $this->alter_table("f137", $fields);
        $this->alter_table("f144", $fields);
        $this->alter_table("f151", $fields);
        $this->alter_table("f155", $fields);
        $this->alter_table("f159", $fields);
        $this->alter_table("f165", $fields);
        $this->alter_table("f169", $fields);
        $this->alter_table("f175", $fields);
        $this->alter_table("f179", $fields);
        $this->alter_table("f186", $fields);
        $this->alter_table("f194", $fields);
        $this->alter_table("f200", $fields);
        $this->alter_table("f208", $fields);
        $this->alter_table("f216", $fields);
        $this->alter_table("f221", $fields);
    }

    function update_non_sucriber_column() {
        $this->db->update('banks', array('non_subscriber' => 0), array('non_subscriber' => NULL));
        $col = array(
            'null' => FALSE,
            'default' => 0
        );
        $this->modify_column('banks', 'non_subscriber', $col);
    }

//    END BORROWER ASSESSMENT
    function all() {
        $this->c6();
        $this->c9();
        $this->c10();
        $this->c11();

        $this->f5();
        $this->f10();
        $this->f18();

        //////////
        $this->f22();
        $this->add_hide_column();
        $this->update_non_sucriber_column();
        $this->add_arabic_select_inputs_column();
        /////////
    }

    function get_all() {
        $tables = $this->db->list_tables();
        sort($tables);
        foreach ($tables as $table) {
            echo "/////////////$table///////////" . "<br>";
            $fields = $this->db->list_fields($table);
            sort($fields);
            foreach ($fields as $field) {
                echo $field . "<br>";
            }
            echo "/////////////END///////////" . "<br>";
        }
    }

    ////
    //database fields
    //banks->non_subscriber
    //borrower_information->bank_amount, turnover_amount
    //cases->comment
    //hist_fins
    //projected_fins
    //users->active
    //
    /////
    function update_database() {
        //28th of August, 2018
        $banks = array(
            'non_subscriber' => array(
                'type' => 'INT',
                'null' => true
            )
        );
        $this->alter_table("banks", $banks);
        $borrower_information = array(
            'bank_amount' => array(
                'type' => 'NVARCHAR2',
                'constraint' => '90',
                'null' => true
            ),
            'turnover_amount' => array(
                'type' => 'NVARCHAR2',
                'constraint' => '45',
                'null' => true
            )
        );
        $this->alter_table("borrower_information", $borrower_information);
        $cases = array(
            'comment' => array(
                'type' => 'NVARCHAR2',
                'constraint' => '2000',
                'null' => true
            )
        );
        $this->alter_table("cases", $cases);
        $users = array(
            'active' => array(
                'type' => 'INT',
                'constraint' => '1',
                'null' => true
            )
        );
        $this->alter_table("users", $users);
        $hist_fins = array(
            'case_id' => array(
                'type' => 'INT',
                'null' => true
            ),
            'year' => array(
                'type' => 'VARCHAR2',
                'constraint' => '15',
                'null' => true
            ),
            'rown' => array(
                'type' => 'INT',
                'null' => true
            ),
            'value' => array(
                'type' => 'VARCHAR2',
                'constraint' => '45',
                'null' => true
            )
        );
        $this->create_table("hist_fins", $hist_fins);
        $projected_fins = array(
            'case_id' => array(
                'type' => 'INT',
                'null' => true
            ),
            'year' => array(
                'type' => 'VARCHAR2',
                'constraint' => '15',
                'null' => true
            ),
            'rown' => array(
                'type' => 'INT',
                'null' => true
            ),
            'value' => array(
                'type' => 'VARCHAR2',
                'constraint' => '45',
                'null' => true
            )
        );
        $this->create_table("projected_fins", $projected_fins);
        echo "Done!";
    }

    function test() {
        $this->db->where("id", 228);
        $this->db->update("cases", array("approved" => 0));
    }

    function update_c6() {
        
    }

    public function read_file() {
        $file_name = 'cat.xlsx';
        ini_set('memory_limit', '2048M');
        set_time_limit(1800);

        $objPHPexcel = \PhpOffice\PhpSpreadsheet\IOFactory::load("./template/$file_name");
        $objWorksheet = $objPHPexcel->setActiveSheetIndex(0);
        $max_row = $objWorksheet->getHighestRow();
        $max_col = $objWorksheet->getHighestColumn();
        $sheet_data = $objWorksheet->rangeToArray(
                "A5:{$max_col}{$max_row}", // The worksheet range that we want to retrieve
                NULL, // Value that should be returned for empty cells
                TRUE, // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
                TRUE, // Should values be formatted (the equivalent of getFormattedValue() for each cell)
                TRUE         // Should the array be indexed by cell row and cell column
        );

        $update['c6'] = [];
        $update['c9'] = [];
        $update['c10'] = [];
        $update['c11'] = [];
        $assess = [];
        $assess_lables = array('F5', 'F10', 'F18', 'F22', 'F29', 'F36', 'F40', 'F45', 'F50', 'F56', 'F66', 'F71', 'F75', 'F79', 'F86', 'F93', 'F100', 'F107', 'F115', 'F121', 'F125', 'F129', 'F133', 'F137', 'F144', 'F151', 'F155', 'F159', 'F165', 'F169', 'F175', 'F179', 'F186', 'F194', 'F200', 'F208', 'F216');
        $i = 0;
        foreach ($sheet_data as $row => $data) {
            if ($row <= 31) {
                $update['c6'][] = array("code" => $data['B'], "value" => $data['C']);
            }
            if ($row <= 429) {
                $update['c9'][] = array("code" => $data['E'], "value" => $data['F']);
            }
            if ($row <= 8) {
                $update['c10'][] = array("code" => $data['H'], "value" => $data['I']);
            }
            if ($row >= 13 && $row <= 19) {
                $update['c11'][] = array("code" => $data['H'], "value" => $data['I']);
            }
        }
        foreach ($sheet_data as $row => $data) {
            if ($data['L'] == 1) {
                $assess[$assess_lables[$i]]['start'] = $row;
                if ($i > 0) {
                    $assess[$assess_lables[$i - 1]]['end'] = $row - 5;
                }
                $i++;
            }
        }
        $assess['F216']['end'] = 316;
        foreach ($assess as $key => $arr) {
            for ($j = $arr['start']; $j <= $arr['end']; $j++) {
                $update[$key][] = array("code" => $sheet_data[$j]['L'], "value" => $sheet_data[$j]['M']);
            }
        }
        $this->insert_master($update);
        echo "Master Fields Updated Successfully!";
    }

    function insert_master($update) {
        foreach ($update as $table => $arr) {
            foreach ($arr as $one) {
                $data = $one;
                $data = increment(strtolower($table), $data);
                $this->db->insert(strtolower($table), $data);
            }
        }
    }

    public function trans_file() {
        $this->trans_info();
        $this->trans_assess();
    }

    public function trans_info() {
        $file_name = 'trans.xlsx';
        ini_set('memory_limit', '2048M');
        set_time_limit(1800);

        $objPHPexcel = \PhpOffice\PhpSpreadsheet\IOFactory::load("./template/$file_name");
        $objWorksheet = $objPHPexcel->setActiveSheetIndex(0);
        $max_row = $objWorksheet->getHighestRow();
        $max_col = $objWorksheet->getHighestColumn();
        $sheet_data = $objWorksheet->rangeToArray(
                "A2:{$max_col}{$max_row}", // The worksheet range that we want to retrieve
                NULL, // Value that should be returned for empty cells
                TRUE, // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
                TRUE, // Should values be formatted (the equivalent of getFormattedValue() for each cell)
                TRUE         // Should the array be indexed by cell row and cell column
        );
//                print_r($sheet_data);
        $update['c6'] = [];
        $update['c10'] = [];
        $update['c11'] = [];
        $get_c9 = $this->db->get("c9")->result();
        foreach ($get_c9 as $one) {
            $this->db->where("code", $one->code);
            $this->db->update("c9", array("arabic_value" => $one->value));
        }
        $get_c6 = $this->db->get("c6")->result();
        foreach ($get_c6 as $one) {
            $update['c6'][$one->value] = array("code" => $one->code, "arabic_lang" => "");
        }
        $get_c10 = $this->db->get("c10")->result();
        foreach ($get_c10 as $one) {
            $update['c10'][$one->value] = array("code" => $one->code, "arabic_lang" => "");
        }
        $get_c11 = $this->db->get("c11")->result();
        foreach ($get_c11 as $one) {
            $update['c11'][$one->value] = array("code" => $one->code, "arabic_lang" => "");
        }
        foreach ($sheet_data as $row => $data) {
            if ($row <= 28) {
                $update['c6'][$data['D']]['arabic_lang'] = $data['E'];
            }
            if ($row <= 5) {
                $update['c10'][$data['G']]['arabic_lang'] = $data['H'];
            }
            if ($row >= 8 && $row <= 14) {
                $update['c11'][$data['G']]['arabic_lang'] = $data['H'];
            }
        }
        foreach ($update as $table => $arr) {
            foreach ($arr as $one) {
                $this->db->where("code", $one['code']);
                $this->db->update($table, array("arabic_value" => $one['arabic_lang']));
            }
        }
//        echo "<pre>";
//        print_r($update);
//        echo "</pre>";
    }

    public function trans_assess() {
        $file_name = 'trans.xlsx';
        ini_set('memory_limit', '2048M');
        set_time_limit(1800);

        $objPHPexcel = \PhpOffice\PhpSpreadsheet\IOFactory::load("./template/$file_name");
        $objWorksheet = $objPHPexcel->setActiveSheetIndex(3);
        $max_row = $objWorksheet->getHighestRow();
        $max_col = $objWorksheet->getHighestColumn();
        $sheet_data = $objWorksheet->rangeToArray(
                "A9:{$max_col}{$max_row}", // The worksheet range that we want to retrieve
                NULL, // Value that should be returned for empty cells
                TRUE, // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
                TRUE, // Should values be formatted (the equivalent of getFormattedValue() for each cell)
                TRUE         // Should the array be indexed by cell row and cell column
        );
//                print_r($sheet_data);
        $update['c6'] = [];
        $update['c9'] = [];
        $update['c10'] = [];
        $update['c11'] = [];
        $assess = [];
        $assess_lables = array('F5', 'F10', 'F18', 'F22', 'F29', 'F36', 'F40', 'F45', 'F50', 'F56', 'F66', 'F71', 'F75', 'F79', 'F86', 'F93', 'F100', 'F107', 'F115', 'F121', 'F125', 'F129', 'F133', 'F137', 'F144', 'F151', 'F155', 'F159', 'F165', 'F169', 'F175', 'F179', 'F186', 'F194', 'F200', 'F208', 'F216');
        $i = 0;
//        BEGIN ASSESSMENT
        $fields = [];
        foreach ($assess_lables as $table) {
            $data = $this->db->get(strtolower($table))->result();
            foreach ($data as $row) {
                $fields[$row->value] = array("table" => strtolower($table), "code" => $row->code, "arabic_lang" => "");
            }
        }
        $assessment = [];
        foreach ($sheet_data as $row => $data) {
//            $assessment[$data['A']]=$data['B'];
            $fields[$data['A']]["arabic_lang"] = $data['B'];
        }
        foreach ($fields as $field) {
            if (!empty($field['table'])) {
                $this->db->where("code", $field['code']);
                $this->db->update($field['table'], array("arabic_value" => $field['arabic_lang']));
            }
        }
//        END ASSESSMENT
    }

    public function trans_cbe() {
        $file_name = 'cbe.xlsx';
        ini_set('memory_limit', '2048M');
        set_time_limit(1800);

        $objPHPexcel = \PhpOffice\PhpSpreadsheet\IOFactory::load("./template/$file_name");
        $objWorksheet = $objPHPexcel->setActiveSheetIndex(0);
        $max_row = $objWorksheet->getHighestRow();
        $max_col = $objWorksheet->getHighestColumn();
        $sheet_data = $objWorksheet->rangeToArray(
                "A3:{$max_col}{$max_row}", // The worksheet range that we want to retrieve
                NULL, // Value that should be returned for empty cells
                TRUE, // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
                TRUE, // Should values be formatted (the equivalent of getFormattedValue() for each cell)
                TRUE         // Should the array be indexed by cell row and cell column
        );
//        print_r($sheet_data);
//        die;
        foreach ($sheet_data as $arr) {
            foreach ($arr as $col => $value) {
                if ($col == "D") {
                    echo $col . ": " . $arr['D'] . "E: " . $arr['E'] . "<br>";
                    $this->db->where("code", $arr['D']);
                    $this->db->update("c9", array("arabic_value" => $arr['E']));
                }
            }
        }
//        END ASSESSMENT
//        echo "<pre>";
//        print_r($fields);
//        echo "</pre>";
    }

    function update_trans() {
//       BEGIN BORROWER INFORMATION
        $bi = array("Open Stock Company" => "شركة مساهمة ( تحت التداول)", "Closed Stock Company" => "شركة مساهمة (بدون تداول)");
        foreach ($bi as $value => $arabic_value) {
            $this->db->where("value", $value);
            $this->db->update("c11", array("arabic_value" => $arabic_value));
        }
//        END BORRWER INFORMATION
//        BEGIN BORROWER ASSESSMENT
        $ba = array("Zero cheque bounces" => "لا توجد شيكات مرتدة بدون رصيد");
        foreach ($ba as $value => $arabic_value) {
            $this->db->where("value", $value);
            $this->db->update("f208", array("arabic_value" => $arabic_value));
        }
        $ba = array("Not Available" => "غير متاح");
        foreach ($ba as $value => $arabic_value) {
            $this->db->where("value", $value);
            $this->db->update("f121", array("arabic_value" => $arabic_value));
        }
        $ba = array("Outstanding" => "ممتاز",
            "Very Good" => "جيد جدًا",
            "Not Available" => "غير متاح",
            "Satisfactory" => "مرضي",
            "Below Satisfactory" => "أقل من المرضي",
            "High Risk / Defaulted" => "مخاطر مرتفعة / متخلفة عن الدفع"
        );
        foreach ($ba as $value => $arabic_value) {
            $this->db->where("value", $value);
            $this->db->update("f137", array("arabic_value" => $arabic_value));
        }
        $ba = array(
            "N/A" => "غير متاح",
        );
        foreach ($ba as $value => $arabic_value) {
            $this->db->where("value", $value);
            $this->db->update("f186", array("arabic_value" => $arabic_value));
        }
        $ba = array(
            "N/A" => "غير متاح",
        );
        foreach ($ba as $value => $arabic_value) {
            $this->db->where("value", $value);
            $this->db->update("f194", array("arabic_value" => $arabic_value));
        }
        $ba = array(
            "N/A" => "غير متاح",
        );
        foreach ($ba as $value => $arabic_value) {
            $this->db->where("value", $value);
            $this->db->update("f200", array("arabic_value" => $arabic_value));
        }
//        END BORROWER ASSESSMENT
    }

}
