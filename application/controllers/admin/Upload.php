<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    
    class Upload extends CI_Controller {
        
        public function __construct() {
            parent::__construct();
            //        $this->load->model('admin/Bulk_model');
            if ($this->session->userdata('language') != "") {
                $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
            } else {
                $this->lang->load('default');
                $this->lang->load('default_1');
            }
        }
        
        public function index($redirect = 0) {
            $this->load->helper("directory");
            $files = directory_map('./reports/', 1);
            if(!(!$redirect || (compare_level('super_admin') && $redirect))){ 
                redirect($_SERVER['HTTP_REFERER']);
            }
			
            if ($files) {
                foreach ($files as $file_name) {
                    if($file_name=="index.php"){
                        continue;
                    }
                    //check type
                    $exp = explode('.', $file_name);
                    if (strtolower($exp[1]) != "pdf" && strtolower($exp[1]) != "xml") {
                        $error = "not allowed file type";
                        save_audit('failed to auto upload gradation report : ' . $file_name . ' due to ' . $error);
                        continue;
                    }
                    //check name
                    $pdf_name = $file_name;
                    $file = $exp[0];
                    $file = explode('.', $file_name)[0];
                    
                    $pos = strrpos($file, "D");
                    if ($pos) {
                        $file = substr($file, 0, $pos);
                    }
                    
                    $this->db->select('cases.name,banks.name as bank_name,users.branch_code,cases.date_approved as date_approved,cases.id');
                    $this->db->where("cases.completed !=", 1);
                    $this->db->where("cases.approved", 1);
                    $this->db->join('users', 'users.id=cases.user_id');
                    $this->db->join('banks', 'users.bank_id=banks.id');
                    $this->db->like('cases.name', $file, 'after');
                    $case = $this->db->get('cases')->row();
                    
                    if (!$case) {
                        $error = "a case with a corresponding name doesn't exist";
                        save_audit('failed to auto upload gradation report : ' . $file_name . ' because ' . $error);
                        continue;
                    }
                    //////////
                    //move pdf
                    $case->bank_name=  replace_bank_name($case->bank_name);
                    if (!rename("./reports/" . $file_name, "./root/$case->bank_name/$case->branch_code/Gradation Results/$file_name")) {
                        $error = "the pdf file couldn't be moved";
                        save_audit('failed to auto upload gradation report : ' . $file_name . ' because ' . $error);
                        continue;
                    }
                    if (!rename("./root/Cases/" . date('d-m-Y', strtotime($case->date_approved)) . "/$case->name.xlsx", "./root/$case->bank_name/$case->branch_code/$case->name.xlsx") &&
                        !rename("./root/Cases/" . date('Y_m_d', strtotime($case->date_approved)) . "/$case->name.xlsx", "./root/$case->bank_name/$case->branch_code/$case->name.xlsx")) {
                            $error = "the case file couldn't be moved";
                            save_audit('failed to auto upload gradation report : ' . $file_name . ' because ' . $error);
                            continue;
                        }
                        save_audit('Auto upload gradation report : ' . $file_name);
                        $this->db->update('cases', array('date_completed' => date('Y-m-d H:i:s'), 'completed' => 1, "pdf_name" => $pdf_name), array('id' => $case->id));
                }
                echo "Operation performed successfully! Please, check the event log for failed files";
                
                
            }
            if($redirect) {
                
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
        
    }
    