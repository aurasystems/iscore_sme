<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Register extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Register_model');
        $this->load->model('admin/Dashboard_model');
        $this->load->model('admin/User_model');
        $this->load->model('admin/Back_settings_model');
        if($this->session->userdata('language') != "" )
        {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        }
        else
        {
            $this->lang->load('default');
            $this->lang->load('default_1');
        }
    }
    public function index()
    {
        if(($this->session->userdata('user_id') != ""))
        {
            $this->welcome();
        }
        else
        {
            $data['title'] = 'Register';
            $this->load->view("admin/pages/register", $data);
        }
    }
    public function welcome()
    {
        if(($this->session->userdata('user_id') != ""))
        {     
            $data = array('title' => 'Dashboard');
            $data['clients_count'] = $this->Dashboard_model->get_clients_count();
            $data['users_count'] = $this->Global_model->get_count('users');
            $data['back_settings'] = $this->Back_settings_model->get_back_settings();
            $this->load->view('admin/pages/dashboard', $data);
        }
        else
        {
            $this->index();
        }
    }
    public function do_register()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name', 'first name', 'trim|required|min_length[4]');
        $this->form_validation->set_rules('last_name', 'last name', 'trim|required|min_length[4]');
        $this->form_validation->set_rules('display_name', 'display name', 'trim|required|min_length[4]');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'password', 'trim|required|matches[password2]|min_length[4]|max_length[32]');
        $this->form_validation->set_rules('password2', 'password confirmation', 'trim|required|matches[password]');
        $this->form_validation->set_rules('terms', 'terms', 'trim|required');
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'display_name' => $this->input->post('display_name'),
                'email' => $this->input->post('email'),
                'password' => do_hash($this->input->post('password'), 'md5')
            );
            $this->Global_model->global_insert('users',$data);
            redirect('admin/Register/register_success');
        }
    }
    public function register_success()
    {
        if(($this->session->userdata('user_id') != ""))
        {
            $this->welcome();
        }
        else
        {
            $data['title'] = 'Register Success';
            $this->load->view("admin/pages/register_success", $data);
        }
    }
}