<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Front_settings extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('admin/Front_settings_model');
        if($this->session->userdata('language') != "" )
        {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        }
        else
        {
            $this->lang->load('default');
            $this->lang->load('default_1');
        }
    }
    public function index($status = NULL)
    {
        if(($this->session->userdata('user_id') != ""))
        {
            $data = array('title' => 'Front Settings');
            $data['status'] = $status;
            $data['attributes'] = array('class' => 'form-horizontal', 'role'=>'form');
            $data['label_att'] = array ('class' => 'col-sm-4 control-label');
            $data['front_settings'] = $this->Front_settings_model->get_front_settings();
            $this->load->view("admin/pages/settings/front_settings", $data);
        }
        else
        {
            redirect('admin/Dashboard');
        }
    }
    public function update()
    {
        if(($this->session->userdata('user_id') != ""))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            if($this->form_validation->run() == FALSE)
            {
                $this->index();
            }
            else
            { 
                $data = array(
                    'title' => $this->input->post('title'),
                    'company_info' => $this->input->post('company_info'),
                    'address' => $this->input->post('address'),
                    'phone' => $this->input->post('phone'),
                    'email' => $this->input->post('email'),
                    'facebook_url' => $this->input->post('facebook'),
                    'twitter_url' => $this->input->post('twitter'),
                    'google_plus_url' => $this->input->post('google_plus'),
                    'pinterest_url' => $this->input->post('pinterest'),
                    'flickr_url' => $this->input->post('flickr'),
                    'linkedin_url' => $this->input->post('linkedin')
                );
                if($_FILES['userfile']['name'] != '')
                {
                    $config['upload_path'] = './uploads/settings/front';
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $config['max_size'] = '10000';
                    $config['max_width'] = '5000';
                    $config['max_height'] = '5000';
                    $this->lang->load('upload');
                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload())
                    {
                        $upload_data = $this->upload->data();
                        $data['front_logo'] = $upload_data['file_name'];
                    }
                }
                $this->Front_settings_model->update_settings($data);
            }
        }
        else
        {
            redirect('admin/Dashboard');
        } 
    }
}