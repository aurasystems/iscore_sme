<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cases_two extends CI_Controller {

    public function __construct() {
        parent::__construct();
        check_login();
        $this->load->model('admin/Cases_two_model');
        if ($this->session->userdata('language') != "") {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        } else {
            $this->lang->load('default');
            $this->lang->load('default_1');
        }
        $this->session->set_userdata('active', 'cases');
    }

    function get_cases_ajax() {
        $search_val = isset($this->input->post('search')["value"]) ? $this->input->post('search')["value"] : 0;

        $edit = get_perm("cases", 2);
        $delete = get_perm("cases", 3);
        $approve_add = get_perm("approve_case", 1);
        $approve_edit = get_perm("approve_case", 2);

        $cases = NULL;
        $cases_num = 0;
        $filtered_num = 0;
        $p = get_cases_perm();
        $user = $this->Crud_model->get_one("users", $this->session->userdata("user_id"));
        // SORT
        $cols = array('b_i.C4', 'cases.name', 'cases.date_created', 'users.first_name,users.last_name', '', '', 'cases.date_approved', 'cases.date_completed');
        if ($this->input->post('order') && isset($this->input->post('order')[0])) {
            $this->db->order_by($cols[$this->input->post('order')[0]['column']], $this->input->post('order')[0]['dir']);
        }

        if ($p == "me") {
            $cases = $this->Cases_two_model->get_user_cases($this->session->userdata("user_id"), $this->input->post('length'), $this->input->post('start'), $search_val);
            $cases_num = $this->Cases_two_model->get_user_cases($this->session->userdata("user_id"));
            $filtered_num = $this->Cases_two_model->get_user_cases($this->session->userdata("user_id"), -1, 0, $search_val);
        } elseif ($p == "bank") {
            $cases = $this->Cases_two_model->get_bank_cases($user->bank_id, $this->input->post('length'), $this->input->post('start'), $search_val);
            $cases_num = $this->Cases_two_model->get_bank_cases($user->bank_id);
            $filtered_num = $this->Cases_two_model->get_bank_cases($user->bank_id, -1, 0, $search_val);
        } elseif ($p == "branch") {
            $branch_codes = explode(',', $user->branch_code);
            $cases = $this->Cases_two_model->get_branch_cases($branch_codes, $user->bank_id, $this->input->post('length'), $this->input->post('start'), $search_val);
            $cases_num = $this->Cases_two_model->get_branch_cases($branch_codes, $user->bank_id);
            $filtered_num = $this->Cases_two_model->get_branch_cases($branch_codes, $user->bank_id, -1, 0, $search_val);
        } elseif ($p == "all") {
            $cases = $this->Cases_two_model->get_cases($this->input->post('length'), $this->input->post('start'), $search_val);
            $cases_num = $this->Cases_two_model->get_cases_count();
            $filtered_num = $search_val ? $this->Cases_two_model->get_filterd_cases_count($search_val) : $cases_num;
        }

        $can_add_case = $user ? count(explode(',', $user->branch_code)) : 0;

        $cases_arr = array();
        foreach ($cases as $case) {
            $case->created_by = $case->first_name . " " . $case->last_name;
            $sub_array = array();
            $sub_array[] = $case->company_name;
            $sub_array[] = "<a href = " . base_url() . "admin/Cases_two/view_case/" . $case->id . ">" . $case->name . "</a>";
            $sub_array[] = date('Y/m/d', strtotime($case->date_created));
            $sub_array[] = $case->created_by;
            if ($case->approved == 0) {
                $sub_array[] = '<i title="' . $this->lang->line('waiting_approval') . '" class="text-warning" style="font-weight: bold">---</i>';
            } else if ($case->approved == 1) {
                $sub_array[] = '<i class="fa fa-check fa-lg text-success"></i>';
            } else if ($case->approved == - 1) {
                $sub_array[] = '<i class="fa fa-close fa-lg text-danger"></i>' . '<a onclick="show_comment_modal(' . $case->id . ')" style="cursor:pointer">' . $this->lang->line('show_rejection_comment') . '</a>';
            }

            if ($case->completed == 1) {
                $pdf_name = $case->pdf_name;
                $pdf_exp = explode(".", $pdf_name);
                if (count($pdf_exp) < 2) {
                    $pdf_name .= ".pdf";
                }
                $non_subs = non_subscriber_bank($case->id);
                if (!$non_subs || ($non_subs && compare_level('super_admin'))) {
                    $sub_array[] = '<a title="Open PDF" class="btn btn-primary btn-sm" href="' . base_url() . 'root/' . $case->bank_name . '/' . $case->branch_code . '/Gradation Results/' . $pdf_name . '"><i class="fa fa-arrow-down"></i></a>';
                } else {
                    $sub_array[] = '<i class="fa fa-check fa-lg text-success"></i>';
                }
            } else {
                $sub_array[] = '<i class="fa fa-close fa-lg text-danger"></i>';
            }

            $sub_array[] = $case->approved == 1 ? date('Y/m/d', strtotime($case->date_approved)) : '---';

            $sub_array[] = $case->completed == 0 ? '---' : date('Y/m/d', strtotime($case->date_completed));

            $actions = "";
            if (($case->completed == 0 && (compare_level("super_admin"))) || ($case->approved == 0 && compare_level("verifier"))) {
                $actions .= '<a href="#" case_id="' . $case->id . '" class="reassign btn btn-sm btn-success" title="' . $this->lang->line("reassign_case") . '" ><i class="fa fa-user"></i></a>';
            }
            if ($case->completed == 1 && $can_add_case < 2 && (compare_level("super_admin") || compare_level("maker") || compare_level("verifier"))) {
                $actions .= '<a href="' . base_url() . 'admin/Cases_two/copy_case/' . $case->id . '" class="edit btn btn-sm btn-warning" title="' . $this->lang->line("copy_case") . '" ><i class="fa fa-file"></i></a>';
            }
            if ($case->approved != 1) {
                if ($case->status == 'saved' && ($approve_add || $approve_edit)) { // && !non_subscriber_bank($case->id)
                    $actions .= '<a onclick="approve_case(' . $case->id . ')" class=" edit btn btn-sm btn-success" data-id="' . $case->id . '" title="' . $this->lang->line("approve_generate") . '" ><i class="fa fa-check"></i></a>';
                    if ($case->approved != - 1) {
                        $actions .= '<a onclick="not_approve(' . $case->id . ')" class="edit btn btn-sm btn-danger" data-id="' . $case->id . '" title="' . $this->lang->line("not_approved") . '" ><i class="fa fa-ban"></i></a> ';
                    }
                }
                if ($edit && !($case->status == 'saved' && $case->approved == 0)) {
                    $actions .= '<a href="' . base_url() . 'admin/Cases/edit_borrower_information/' . $case->id . '" title="' . $this->lang->line('edit_case') . '" class="edit btn btn-sm btn-default"><i class="icon-note"></i></a>';
                }
            } else if (compare_level("super_admin")) {
                $actions .= '<a onclick="location.href = \'' . base_url() . 'admin/Cases/generate_excel/' . $case->id . '/1\'" class=" edit btn btn-sm btn-success" data-id="' . $case->id . '" title="' . $this->lang->line("download_excel_file") . '" ><i class="fa fa-download"></i></a>';
            }
            if ($case->status != 'saved' && $edit) {
                $actions .= '<a href="' . base_url() . 'admin/Cases_two/continue_case/' . $case->id . '" title="' . $this->lang->line('continue_case') . '" class="edit btn btn-sm btn-warning"><i class="fa fa-arrow-right"></i></a>';
            }
            if ((compare_level("super_admin") || $case->approved == 0) && $delete) {
                $actions .= '<a onclick="delete_case(' . $case->id . ')" class="btn btn-sm btn-danger delete-case" title="' . $this->lang->line('delete_case') . '" data-case_id="' . $case->id . '" ><i class="fa fa-close"></i></a>';
            }
            $sub_array[] = $actions;
            $cases_arr[] = $sub_array;
        }

        $output = array(
            "draw" => intval($this->input->post("draw")),
            "recordsTotal" => $cases_num,
            "recordsFiltered" => $filtered_num,
            "data" => $cases_arr
        );
        echo json_encode($output);
    }

    function index($success = NULL) {
        $cases = NULL;
        // check user permissions

        $user = $this->Crud_model->get_one("users", $this->session->userdata("user_id"));

        // reassign users
        // $this->db->where("id", $this->session->userdata('user_id'));
        // $bank_id = $this->db->get("users")->row()->bank_id;
        // $this->load->model("admin/User_model");
        // $users = $this->User_model->get_users($bank_id);
        $users = $this->Cases_two_model->get_reassignment_users();
        // /////////////////
        // load view

        $data['title'] = $this->lang->line("cases");
        // $data['cases'] = $cases;
        $data['label_att'] = 'col-sm-3 control-label';
        $data['user_bank_exist'] = $this->Cases_two_model->logged_user_bank_exist();
        $data['users'] = $users;
        $data['can_add_case'] = $user ? count(explode(',', $user->branch_code)) : 0;
        $data['success'] = $success;
        $this->load->view("admin/pages/cases/index", $data);
    }

    function get_case_name($case_id) {
        $case_id = sanitize($case_id);
        $case = $this->db->get_where('cases', array(
                    'id' => $case_id
                ))->row();
        if ($case) {
            return $case->name;
        }
        return '';
    }

    function borrower_factsheet($case_id = NULL, $view_only = NULL) {
        check_param($case_id);
        // / Path Traversal Owasp testing issue
        foreach ($this->input->get() as $key => $value) {
            $_GET[$key] = $this->security->sanitize_filename($value);
            $_GET[$key] = $this->db->escape($value);
        }
        // ///////////
        $case_id = sanitize($case_id);
        $view_only = sanitize($view_only);
        $case = $this->Crud_model->get_one("cases", $case_id);
        if ($view_only != NULL) {
            if ($case->user_id != $this->session->userdata("user_id")) {
                check_perm("cases", 0);
            }
        } else {

            if ($case && $case->status == 'saved' && $case->approved == 0) {
                $this->session->set_flashdata('error', $this->lang->line('cannot_edit'));
                redirect(base_url('admin/Cases_two'));
            } else if ($case->approved == 1) {
                $this->session->set_flashdata('error', $this->lang->line('appoved_cannot_edit'));
                redirect(base_url('admin/Cases_two'));
            }
            // if (!$this->input->post("continue_later")) {
            // check_perm("cases", 1);
            // } else {
            // check_perm("cases", 2);
            // }
            if (!get_perm("cases", 1) && !get_perm("cases", 2)) {
                redirect(base_url("Welcome/permission_denied"));
            }
        }

        $old_case_arr = $this->Cases_two_model->get_factsheet_arr($case_id);

        $this->Cases_two_model->check_case_status($case_id, $view_only, 'borrower_factsheet');
        // call validation
        if (!$this->input->post("continue_later")) {
            $this->borrower_factsheet_validation();
        } else {
            $this->borrower_factsheet_later_validation();
        }
        $sum_share = 100;
        if ($this->input->post()) {
            $sum_share = 0;
            for ($i = 9; $i <= 18; $i ++) {
                if (is_numeric($this->input->post("D$i"))) {
                    $sum_share += $this->input->post("D$i");
                }
            }
        }
        if (!$this->form_validation->run() || $sum_share > 100) {

            // save invalid data
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $this->save_factsheet_invalid($case_id);
            }
            // load view
            $data['title'] = $this->lang->line("borrower_factsheet");
            $data['factsheet_data'] = $this->Cases_two_model->get_factsheet_data($case_id);
            $data['line_activity'] = $this->Cases_two_model->get_line_of_activity_value($case_id);
            $data['is_case_pass_this_setp'] = $this->Cases_two_model->if_curr_stage_pass($case_id, 'borrower_factsheet');
            $data['label_att'] = 'col-sm-3 control-label';
            if ($view_only != NULL) {
                $data['view_only'] = 1;
            }
            $data['case_id'] = $case_id;
            $data['sum_share_error'] = $sum_share > 100 ? TRUE : FALSE;
            $this->load->view("admin/pages/cases/borrower_factsheet", $data);
        } else {

            $i = 0;
            $data = array();
            $row_data = array();
            $row_data['case_id'] = $case_id;
            $incremented = incremented("borrower_factsheet");
            for ($i = 4; $i <= 44; $i ++) {
                // 6,7,8 , 20,21, 33,34
                // d4,5
                if ($i == 6 || $i == 7 || $i == 8 || $i == 20 || $i == 21 || $i == 33 || $i == 34) {
                    continue;
                }
                $row_data["id"] = $incremented;
                $row_data['row'] = $i;
                $row_data['C'] = $this->input->post('C' . $i) || $this->input->post('C' . $i) === "0" ? $this->input->post('C' . $i) : '';
                $row_data['D'] = $this->input->post('D' . $i) || $this->input->post('D' . $i) === "0" ? $this->input->post('D' . $i) : '';
                $row_data['E'] = $this->input->post('E' . $i) || $this->input->post('E' . $i) === "0" ? $this->input->post('E' . $i) : '';
                if ($i == 4) {
                    $row_data['E'] *= $this->input->post('amounts');
                }
                if ($i == 5) {
                    $row_data['C'] *= $this->input->post('amounts');
                }
                // if ($row_data['C'] != '' || $row_data['D'] != '' || $row_data['E'] != '') {
                // array_push($data, $row_data);
                // }
                array_push($data, $row_data);
                $incremented ++;
            }

            $this->db->delete('borrower_factsheet', array(
                'case_id' => $case_id
            ));

            $if_exist = $this->db->affected_rows();

            // modified
            if (!empty($data)) {
                $this->db->insert_batch('borrower_factsheet', $data);
            }

            if (!$this->input->post("continue_later")) {
                if ($this->Cases_two_model->if_curr_stage_pass($case_id, 'borrower_financials')) {
                    $this->db->update('cases', array(
                        'status' => 'borrower_financials'
                            ), array(
                        'id' => $case_id
                    ));
                }
                $case_name = $this->get_case_name($case_id);
                if ($if_exist) {
                    $this->check_update_notification("borrower_factsheet", $old_case_arr);
                    save_audit("Case Borrower factsheet info. upated -- " . $case_name);
                } else {
                    save_audit("Case Borrower factsheet info. inserted -- " . $case_name);
                }
                $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
                redirect(base_url("admin/Cases_two/borrower_financials/$case_id"));
            } else {
                // if ($this->Cases_two_model->if_curr_stage_pass($case_id, 'borrower_factsheet')) {
                $this->db->update('cases', array(
                    'status' => 'borrower_factsheet'
                        ), array(
                    'id' => $case_id
                ));
                // }
                if ($if_exist) {
                    save_audit("Case Borrower factsheet info. upated and marked as continue later in case -- " . $case_name);
                } else {
                    save_audit("Case Borrower factsheet info. inserted and marked as continue later in case -- " . $case_name);
                }
                $this->session->set_flashdata("success", $this->lang->line("data_saved_successfully"));
                redirect(base_url("admin/Cases_two"));
            }
        }
    }

    function borrower_factsheet_validation() {
        $this->form_validation->set_rules("amounts", $this->lang->line("amounts"), "xss_clean|trim|required");
        for ($i = 4; $i <= 5; $i ++) {
            $extra = '';
            $year = date("Y");
            if ($year > 2025) {
                $year = 2025;
            }
            if ($i == 4) {
                $extra = '|required|numeric|greater_than_equal_to[1900]|less_than_equal_to[' . $year . ']';
            } else {
                $extra = '|required|numeric|callback_validate_C5';
            }
            $this->form_validation->set_rules("C$i", $this->lang->line("borrower_factsheet_C$i"), "xss_clean|trim$extra");
            if ($i == 4) {
                $this->form_validation->set_rules("E$i", $this->lang->line("borrower_factsheet_E$i"), "xss_clean|trim|required|numeric|callback_validate_E4");
            }
        }

        $investor_groups = array();
        $sum_share = 0;
        for ($i = 9; $i <= 18; $i ++) {
            if (is_numeric($this->input->post("D$i"))) {
                $sum_share += $this->input->post("D$i");
            }
            // modified
            $D = "";
            if ($this->input->post("C$i")) {
                $D = "|required";
                array_push($investor_groups, $this->input->post("C$i"));
                $this->form_validation->set_rules("C$i", $this->lang->line("investor_group"), "xss_clean|callback_check_unique|callback_check_chars|trim|max_length[40]");
            } elseif ($this->input->post("D$i")) {
                $this->form_validation->set_rules("C$i", $this->lang->line("investor_group"), "required|xss_clean");
            }
            $this->form_validation->set_rules("D$i", $this->lang->line("share"), "xss_clean|trim|numeric|greater_than_equal_to[0]|less_than_equal_to[100]|callback_check_on_decimal$D");
        }

        for ($i = 22; $i <= 31; $i ++) {
            $extra = '';
            $c_extra = '';
            if ($this->input->post("C$i") != '') {
                $extra = '|required';
                $c_extra = '|callback_unique_directors|callback_check_chars|max_length[40]';
            } elseif ($this->input->post("D$i")) {
                $c_extra = '|required';
            }
            $this->form_validation->set_rules("C$i", $this->lang->line("name"), "xss_clean|trim$c_extra");
            $this->form_validation->set_rules("D$i", $this->lang->line("remarks"), "xss_clean|trim|callback_check_chars|max_length[30]$extra");
        }

        for ($i = 35; $i <= 44; $i ++) {
            $extra = '';
            $c_extra = '';
            if ($this->input->post("C$i") != '') {
                $extra = '|required';
                $c_extra = '|callback_unique_personnel|callback_check_chars|max_length[40]';
            } elseif ($this->input->post("D$i")) {
                $c_extra = '|required';
            }
            $this->form_validation->set_rules("C$i", $this->lang->line("name"), "xss_clean|trim$c_extra");
            $this->form_validation->set_rules("D$i", $this->lang->line("designation"), "xss_clean|trim|callback_check_chars|max_length[30]$extra");
        }
    }

    function validate_E4($value) {
        $this->form_validation->set_message('validate_E4', $this->lang->line('E4_message'));
        $value = (float) $value;
        $amounts = (float) $this->input->post("amounts");
        $value *= $amounts;
        if ($value >= 0 && $value <= 200000000) {
            return TRUE;
        }
        return FALSE;
    }

    function validate_C5($value) {
        $this->form_validation->set_message('validate_C5', $this->lang->line('C5_message'));
        $value = (float) $value;
        $amounts = (float) $this->input->post("amounts");
        $value *= $amounts;
        if ($value >= 0 && $value <= 1000000000) {
            return TRUE;
        }
        return FALSE;
    }

    function check_unique($group) {
        $found = 0;
        for ($i = 9; $i <= 18; $i ++) {
            if ($this->input->post("C$i") == $group) {
                $this->form_validation->set_message('check_unique', $this->lang->line('group_must_unique'));
                $found ++;
                if ($found == 2) {
                    return FALSE;
                }
            }
        }
        return TRUE;
    }

    function unique_directors($group) {
        $found = 0;
        for ($i = 22; $i <= 31; $i ++) {
            if ($this->input->post("C$i") == $group) {
                $this->form_validation->set_message('unique_directors', $this->lang->line('director_must_unique'));
                $found ++;
                if ($found == 2) {
                    return FALSE;
                }
            }
        }
        return TRUE;
    }

    function unique_personnel($group) {
        $found = 0;
        for ($i = 35; $i <= 44; $i ++) {
            if ($this->input->post("C$i") == $group) {
                $this->form_validation->set_message('unique_personnel', $this->lang->line('personnel_must_unique'));
                $found ++;
                if ($found == 2) {
                    return FALSE;
                }
            }
        }
        return TRUE;
    }

    function check_chars($string) {
        $this->form_validation->set_message('check_chars', $this->lang->line('no_special_chars_validation'));
        $string = str_replace(" ", '', $string); // remove spaces
        $string = preg_replace('/[\x{0600}-\x{06FF}A-Za-z]/u', '', $string); // remove arabic chars
        if ($string == "") {
            return TRUE;
        }
        return FALSE;
    }

    function check_on_decimal($value) {
        if ($value != "") {
            $decimal_arr = explode('.', $value);
            if (count($decimal_arr) > 1) {
                // no decimal allowed
                $this->form_validation->set_message('check_on_decimal', $this->lang->line('no_decimals_allowed'));
                return FALSE;
            }
            if (!(is_numeric($value) && $value >= 0 && $value <= 100)) {
                $this->form_validation->set_message('check_on_decimal', $this->lang->line('between_0_100'));
                return FALSE;
            }
        }
        return TRUE;
    }

    function borrower_factsheet_later_validation() {
        for ($i = 4; $i <= 5; $i ++) {

            $this->form_validation->set_rules("C$i", $this->lang->line("borrower_factsheet_C$i"), "xss_clean|trim");
            if ($i == 4) {
                $this->form_validation->set_rules("E$i", $this->lang->line("borrower_factsheet_E$i"), "xss_clean|trim");
            }
        }

        for ($i = 9; $i <= 18; $i ++) {
            $this->form_validation->set_rules("C$i", $this->lang->line("investor_group"), "xss_clean|trim");
            $this->form_validation->set_rules("D$i", $this->lang->line("share"), "xss_clean|trim");
        }

        for ($i = 22; $i <= 31; $i ++) {
            $this->form_validation->set_rules("C$i", $this->lang->line("name"), "xss_clean|trim");
            $this->form_validation->set_rules("D$i", $this->lang->line("remarks"), "xss_clean|trim");
        }

        for ($i = 35; $i <= 44; $i ++) {
            $this->form_validation->set_rules("C$i", $this->lang->line("name"), "xss_clean|trim");
            $this->form_validation->set_rules("D$i", $this->lang->line("designation"), "xss_clean|trim");
        }
    }

    function borrower_financials($case_id = NULL, $view_only = NULL) {
        check_param($case_id);
        // print_r($this->input->post());die();
        // / Path Traversal Owasp testing issue
        foreach ($this->input->get() as $key => $value) {
            $_GET[$key] = $this->security->sanitize_filename($value);
            $_GET[$key] = $this->db->escape($value);
        }
        // ///////////
        $case_id = sanitize($case_id);
        $view_only = sanitize($view_only);
        $case = $this->Crud_model->get_one("cases", $case_id);
        if ($view_only != NULL) {
            if ($case->user_id != $this->session->userdata("user_id")) {
                check_perm("cases", 0);
            }
        } else {
            if ($case && $case->status == 'saved' && $case->approved == 0) {
                $this->session->set_flashdata('error', $this->lang->line('cannot_edit'));
                redirect(base_url('admin/Cases_two'));
            } else if ($case->approved == 1) {
                $this->session->set_flashdata('error', $this->lang->line('appoved_cannot_edit'));
                redirect(base_url('admin/Cases_two'));
            }
            // if (!$this->input->post("continue_later")) {
            // check_perm("cases", 1);
            // } else {
            // check_perm("cases", 2);
            // }
            if (!get_perm("cases", 1) && !get_perm("cases", 2)) {
                redirect(base_url("Welcome/permission_denied"));
            }
        }

        $this->Cases_two_model->check_case_status($case_id, $view_only, 'borrower_financials');

        $old_case_arr = $this->Cases_two_model->get_financials_arr($case_id);
        // call validation
        $turnover = $this->db->get_where('borrower_information', array(
                    'case_id' => $case_id
                ))->row()->C12;
        if (!$this->input->post("continue_later")) {
            $this->borrower_financials_validation($case_id);
        } else {
            $this->borrower_financials_later_validation();
        }
        if ($this->form_validation->run() || ($this->input->post() && $this->input->post('E3', TRUE) == 'no' && $turnover < ANNUAL_TOVR)) {
            // save in db
            $i = 0;
            $data = array();
            $row_data = array();
            $row_data['case_id'] = $case_id;
            $incremented = incremented("borrower_financials");
            for ($i = 3; $i <= 167; $i ++) {
                if ($this->input->post('E3', TRUE) == 'no' && $i > 4) {
                    break;
                }
                $row_data['id'] = $incremented;
                $row_data['row'] = $i;
                $row_data['C'] = $this->input->post('C' . $i) || $this->input->post('C' . $i) === "0" ? $this->input->post('C' . $i, TRUE) : '';
                $row_data['D'] = $this->input->post('D' . $i) || $this->input->post('D' . $i) === "0" ? $this->input->post('D' . $i, TRUE) : '';
                $row_data['E'] = $this->input->post('E' . $i) || $this->input->post('E' . $i) === "0" ? $this->input->post('E' . $i, TRUE) : '';
                if ($this->input->post('E4') == 'no') {
                    // modified
                    $row_data['F'] = '';
                    $row_data['G'] = '';
                    if ($i == 10) {
                        $row_data['F'] = 'Projected';
                        $row_data['G'] = 'Projected';
                    }
                    if ($i == 12) {
                        $borrower_info_date = $this->Cases_two_model->get_borrower_info_date($case_id);
                        $row_data['C'] = date('d-m-Y', strtotime("-2 year", strtotime($borrower_info_date)));
                        $row_data['D'] = date('d-m-Y', strtotime("-1 year", strtotime($borrower_info_date)));
                        $row_data['E'] = date('d-m-Y', strtotime($borrower_info_date));
                        $row_data['F'] = date('d-m-Y', strtotime("+1 year", strtotime($borrower_info_date)));
                        $row_data['G'] = date('d-m-Y', strtotime("+2 year", strtotime($borrower_info_date)));
                    }
                    if ($i == 13) {
                        $row_data['F'] = 12;
                        $row_data['G'] = 12;
                    }
                } else {
                    $row_data['F'] = $this->input->post('F' . $i) || $this->input->post('F' . $i) === "0" ? $this->input->post('F' . $i, TRUE) : '';
                    $row_data['G'] = $this->input->post('G' . $i) || $this->input->post('G' . $i) === "0" ? $this->input->post('G' . $i, TRUE) : '';
                }
                if ($i == 8) {
                    $row_data['F'] = '';
                    $row_data['G'] = $this->input->post('G' . $i) || $this->input->post('G' . $i) === "0" ? $this->input->post('G' . $i, TRUE) : '';
                }
                // array_push($data, $row_data);
                if ($row_data['C'] != '' || $row_data['D'] != '' || $row_data['E'] != '' || $row_data['F'] != '' || $row_data['G'] != '') {
                    array_push($data, $row_data);
                }
                $incremented ++;
            }

            $this->db->delete('borrower_financials', array(
                'case_id' => $case_id
            ));

            $if_exist = $this->db->affected_rows();

            $this->db->insert_batch('borrower_financials', $data);

            // insert his & projected years
            $this->db->delete('hist_fins', array(
                'case_id' => $case_id
            ));
            $this->db->delete('projected_fins', array(
                'case_id' => $case_id
            ));

            $base_year = $this->Cases_two_model->get_borrower_info_year($case_id);
            $last = $base_year + 2;
            $first = $base_year - 2;
            if ($this->input->post('E4') != 'no') {
                if ($last != $this->input->post("last_projected")) {
                    $this->insert_projected($case_id);
                }
            }

            if ($first != $this->input->post("first_projected")) {
                $this->insert_hist($case_id);
            }

            $this->save_calc($this->input->post('case_id'), $this->input->post('calc'));

            if (!$this->input->post("continue_later")) {
                if ($this->Cases_two_model->if_curr_stage_pass($case_id, 'borrower_assessment')) {
                    $this->db->update('cases', array(
                        'status' => 'borrower_assessment'
                            ), array(
                        'id' => $case_id
                    ));
                }
                $case_name = $this->get_case_name($case_id);
                if ($if_exist) {
                    $this->check_update_notification("borrower_financials", $old_case_arr);
                    save_audit("Case Borrower financials info. upated in case -- " . $case_name);
                } else {
                    save_audit("Case Borrower financials info. inserted in case -- " . $case_name);
                }
                $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
                redirect(base_url("admin/Cases_two/borrower_assessment/$case_id"));
            } else {
                // if ($this->Cases_two_model->if_curr_stage_pass($case_id, 'borrower_financials')) {
                $this->db->update('cases', array(
                    'status' => 'borrower_financials'
                        ), array(
                    'id' => $case_id
                ));
                // }
                $case_name = $this->get_case_name($case_id);
                if ($if_exist) {
                    save_audit("Case Borrower financials info. upated and marked as continue later in case -- " . $case_name);
                } else {
                    save_audit("Case Borrower financials info. inserted and marked as continue later in case -- " . $case_name);
                }
                $this->session->set_flashdata("success", $this->lang->line("data_saved_successfully"));
                redirect(base_url("admin/Cases_two"));
            }
        } else {

            // save invalid data
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $this->save_financials_invalid($case_id);
            }
            // load view
            $data['title'] = $this->lang->line("borrower_financials");
            $data['borrower_info_year'] = $this->Cases_two_model->get_borrower_info_year($case_id);
            $data['borrower_info_date'] = $this->Cases_two_model->get_borrower_info_date($case_id);
            $data['borrower_info_data'] = $this->Cases_two_model->get_borrower_information($case_id);
            $data['financial_data'] = $this->Cases_two_model->get_financials_data($case_id);
            $data['turnover'] = $turnover;
            $data['is_case_pass_this_setp'] = $this->Cases_two_model->if_curr_stage_pass($case_id, 'borrower_financials');
            $data['label_att'] = 'col-sm-3 control-label';

            $last_projected = $this->Cases_two_model->get_last_projected($case_id);
            $first_hist = $this->Cases_two_model->get_first_hist($case_id);
            if (!empty($last_projected->year)) {
                $last_projected = $last_projected->year;
            } else {
                $last_projected = $data['borrower_info_year'] + 2;
            }
            if (!empty($first_hist->year)) {
                $first_hist = $first_hist->year;
            } else {
                $first_hist = $data['borrower_info_year'] - 2;
            }
            $projected = $this->get_case_projected($case_id);
            $hist = $this->get_case_hist($case_id);

            $data['last_projected'] = $last_projected;
            $data['first_hist'] = $first_hist;
            $data['hist'] = $hist;
            $data['hist_years'] = $this->Cases_two_model->get_hist_years($case_id);
            $data['projected_years'] = $this->Cases_two_model->get_projected_years($case_id);
            $data['hidden_years'] = $this->Cases_two_model->get_hidden_years($case_id);
            $data['projected'] = $projected;
            if ($view_only != NULL) {
                $data['view_only'] = 1;
            }
            $data['case_id'] = $case_id;
            $this->load->view("admin/pages/cases/borrower_financials", $data);
        }
    }

    function borrower_financials_validation($case_id) {
        // $this->form_validation->set_rules("E3", $this->lang->line("borrower_financials"), "callback_validate_financials");
        $case_id = sanitize($case_id);
        $this->db->select('C12');
        $turnover = $this->db->get_where('borrower_information', array(
                    'case_id' => $case_id
                ))->row()->C12;

        $this_year_required = '';
        if ($turnover > ANNUAL_TOVR) {
            // modified
            // $this_year_required = '|required';
            $this->form_validation->set_rules("E3", $this->lang->line("borrower_financials"), "callback_validate_financials");
        }
        $this->form_validation->set_rules("G8", $this->lang->line("borrower_financial_G8"), "xss_clean|trim|required");

        // if ($this->input->post('E3') == 'yes') {
        // if (abs($turnover - $this->input->post('E224_Total_Net_Sales')) > 1000) {
        // $this->session->set_flashdata('error', $this->lang->line('financial_borrower_error'));
        // redirect('admin/Cases_two/borrower_financials/' . $case_id);
        // }
        // }

        for ($i = 10; $i <= 167; $i ++) {
            if ($i == 11 || $i == 12 || ($i >= 15 && $i <= 17) || $i == 20 || $i == 21 || ($i >= 23 && $i <= 25) || ($i >= 29 && $i <= 31) || $i == 37 || ($i >= 40 && $i <= 42) || ($i >= 44 && $i <= 48) || ($i >= 52 && $i <= 54) || $i == 57 || ($i >= 59 && $i <= 61) || $i == 64 || $i == 65 || ($i >= 68 && $i <= 70) || ($i >= 72 && $i <= 77) || $i == 91 || $i == 95 || $i == 96 || ($i >= 101 && $i <= 105) || $i == 116 || $i == 118 || ($i >= 120 && $i <= 125) || $i == 128 || $i == 131 || ($i >= 141 && $i <= 144) || ($i >= 150 && $i <= 152) || $i == 157 || $i == 164 || $i == 165) {
                continue;
            }
            // modified
            $req = "";
            if ($i == 13) {
                $req = "|required";
            }
            if ($i == 14) {
                $req .= "|max_length[25]";
            }
            $this->form_validation->set_rules("C$i", $this->lang->line("borrower_financial_$i"), "xss_clean|trim$req");
            $this->form_validation->set_rules("D$i", $this->lang->line("borrower_financial_$i"), "xss_clean|trim$req");
            // modified
            $this->form_validation->set_rules("E$i", $this->lang->line("borrower_financial_$i"), "xss_clean|trim$req");
            if ($this->input->post('E4') == 'yes') {
                $this->form_validation->set_rules("F$i", $this->lang->line("borrower_financial_$i"), "xss_clean|trim");
                $this->form_validation->set_rules("G$i", $this->lang->line("borrower_financial_$i"), "xss_clean|trim");
            }
        }
        $this->form_validation->set_rules("total_sales", "total_sales", "xss_clean|trim|required|callback_validate_total_sales[$case_id]");
        $this->form_validation->set_rules("assets", "assets", "xss_clean|trim|required|callback_validate_assets");
        $this->form_validation->set_rules("E224_Total_Net_Sales", "E224_Total_Net_Sales", "xss_clean|trim|required|callback_E224_Total_Net_Sales[$turnover]");
    }

    function validate_financials() {
        $this->form_validation->set_message("validate_financials", $this->lang->line("financials_required"));
        if ($this->input->post('E3') == 'no') {
            return FALSE;
        }
        return TRUE;
    }

    function get_case() {
        $case1 = $this->db->like('name', 'MCB01280001B0001C39405IYasminElbishlawyD28042020', 'both')->get('cases')->row();
        $case2 = $this->db->like('name', 'MCB01280001B0001C39405IYasminElbishlawyD19082021', 'both')->get('cases')->row();
        $case3 = $this->db->like('name', 'MIB43000001B0002C3456ISeifRedaD10082021', 'both')->get('cases')->row();
        $case4 = $this->db->like('name', 'MPC04000001B0316C5223ImahmoudhamadD10082021', 'both')->get('cases')->row();
        $case5 = $this->db->like('name', 'MPC04000001B0373C47284ImohamedkhaledD10082021', 'both')->get('cases')->row();
        /* for ($i = 1;$i <=5;$i++):
          ${'user' . $i} = ${'case' . $i}->approved ?  $this->db->get_where('users', ['id' => ${'case' . $i}->approved_by])->row() : NULL;
          echo 'Case: ' . ${'case' . $i}->name.(${'case' . $i}->approved ? ' Approved by ['. ${'user' . $i}->first_name . ' ' . ${'user' . $i}->last_name. '] On: '.${'case' . $i}->date_approved : ' Not approved');
          echo '<br />';
          endfor; */
        $casefrom = $this->db->where('id', 61423)->get('cases')->row();
        $casecopied = $this->db->where('id', 70630)->get('cases')->row();

        $casefrom = $this->db->like('name', 'MPC04000001B0170C39163IShimaaElsayedD22062021', 'both')->get('cases')->row();
        $casecopied = $this->db->like('name', 'MAdminB0001C39163IElsayedAllamD26082021', 'both')->get('cases')->row();
        var_export($casefrom);
        echo '<br>';
        echo '<br>';
        var_export($casecopied);
        echo '<br>';
        echo '<br>';
        $years = [];
        $dates = [date('Y-12-31', strtotime('-2 Years')), date('Y-12-31', strtotime('-1 Years')), date('Y-12-31'), date('Y-12-31', strtotime('+1 Years')), date('Y-12-31', strtotime('+2 Years'))];
        $alpha = ['C', 'D', 'E', 'F', 'G'];

        $bfy = $this->db->get_where('borrower_financials', array(
                    'case_id' => $casefrom->id,
                    'row' => '12',
                ))->row();

        $dates_compare = $bfy ? [date('Y-m-d', strtotime($bfy->C)), date('Y-m-d', strtotime($bfy->D)), date('Y-m-d', strtotime($bfy->E)), date('Y-m-d', strtotime($bfy->F)), date('Y-m-d', strtotime($bfy->G))] : [];

        foreach ($dates as $k => $d):
            $key = $dates_compare ? array_search($d, $dates_compare) : NULL;
            $years[$k] = ['key' => $key ? $alpha[$key] : NULL, 'date' => $d];
        endforeach;

        $borrower_financials_row = $this->db->get_where('borrower_financials', array(
                    'case_id' => $casefrom->id,
                ))->result_array();
        $borrower_information_row = $this->db->get_where('borrower_information', array(
                    'case_id' => $casefrom->id,
                ))->result_array();

        var_export($borrower_financials_row);

        echo '<br>';
        echo '<br>';
        var_export($borrower_information_row);

        echo '<br>';
        echo '<br>';
        foreach ($borrower_financials_row as $k => $row):
            $row['case_id'] = $casecopied->id;
            if ($row['row'] == '12') {
                for ($i = 0; $i < 5; $i++):
                    $row[$alpha[$i]] = $dates[$i];
                endfor;
            } else {
                if ($row['row'] !== '10' || $row['row'] !== '13') {
                    for ($i = 0; $i < 5; $i++):
                        $row[$alpha[$i]] = $years[$i]['key'] ? $row[$years[$i]['key']] : NULL;
                    endfor;
                }
            }
            $borrower_financials_row[$k] = $row;
        endforeach;
        var_export($borrower_financials_row);
        $borrower_financials_row = $this->db->get_where('borrower_financials', array(
                    'case_id' => $casecopied->id,
                ))->result_array();
        //$borrower_information_row_update = $this->db->update('borrower_information',['C36' => '31-DEC-' . date('y')],array('case_id' => $case2->id));
        $borrower_information_row = $this->db->get_where('borrower_information', array(
                    'case_id' => $casecopied->id,
                ))->result_array();

        echo '<br>';
        echo '<br>';
        var_export($borrower_financials_row);

        echo '<br>';
        echo '<br>';
        var_export($borrower_information_row);
    }

    function borrower_financials_later_validation() {
        $this->form_validation->set_rules("G8", $this->lang->line("borrower_financial_G8"), "xss_clean|trim");
        for ($i = 10; $i <= 167; $i ++) {
            if ($i == 11 || $i == 12 || ($i >= 15 && $i <= 17) || $i == 20 || $i == 21 || ($i >= 23 && $i <= 25) || ($i >= 29 && $i <= 31) || $i == 37 || ($i >= 40 && $i <= 42) || ($i >= 44 && $i <= 48) || ($i >= 52 && $i <= 54) || $i == 57 || ($i >= 59 && $i <= 61) || $i == 64 || $i == 65 || ($i >= 68 && $i <= 70) || ($i >= 72 && $i <= 77) || $i == 91 || $i == 95 || $i == 96 || ($i >= 101 && $i <= 105) || $i == 116 || $i == 118 || ($i >= 120 && $i <= 125) || $i == 128 || $i == 131 || ($i >= 141 && $i <= 144) || ($i >= 150 && $i <= 152) || $i == 157 || $i == 164 || $i == 165) {
                continue;
            }
            $this->form_validation->set_rules("C$i", $this->lang->line("borrower_financial_$i"), "xss_clean|trim");
            $this->form_validation->set_rules("D$i", $this->lang->line("borrower_financial_$i"), "xss_clean|trim");
            $this->form_validation->set_rules("E$i", $this->lang->line("borrower_financial_$i"), "xss_clean|trim");
            if ($this->input->post('E4') == 'yes') {
                $this->form_validation->set_rules("F$i", $this->lang->line("borrower_financial_$i"), "xss_clean|trim");
                $this->form_validation->set_rules("G$i", $this->lang->line("borrower_financial_$i"), "xss_clean|trim");
            }
        }
    }

    function get_borrower_information($case_id) {
        $this->db->select('C10,C12')->where("case_id", $case_id);
        $query = $this->db->get("borrower_information");
        return $query->row();
    }

    function test_borrower_assessment_validation($case_id = NULL, $data = []) {
        $case_id = sanitize($case_id);
        $borrower_information = $this->get_borrower_information($case_id);
        if ($borrower_information) {
            $turnover = $borrower_information->C12;
            $segment = get_value_from_code("c10", $borrower_information->C10);
            $assessments = [5, 10, 18, 22, 29, 36, 40, 45, 50, 56, 66, 71, 75, 79, 86, 93, 100, 107, 115, 121, 125, 129, 133, 137, 144, 151, 155, 159, 165, 169, 175, 179, 186, 194, 200, 208, 216, 221, 229];

            foreach ($assessments as $n):
                $value = $data["F$n"];
                $required = TRUE;
                if (($turnover < ANNUAL_TOVR && $segment == "Manufacturing") || ($turnover < ANNUAL_TOVR && $segment == "Services") || ($turnover < ANNUAL_TOVR && $segment == "Trading/Commercial")) {
                    if ($n == 4 || $n == 5 || $n == 12 || $n == 13 || $n == 14 || $n == 15 || $n == 16 || $n == 17 || $n == 18 || $n == 19 || $n == 21 || $n == 26) {
                        $required = FALSE;
                    }
                } elseif ($turnover > ANNUAL_TOVR && $segment == "Manufacturing") {
                    if ($n == 8 || $n == 14 || $n == 15 || $n == 16 || $n == 17 || $n == 18 || $n == 19 || $n == 20 || $n == 27 || $n == 29 || $n == 30 || $n == 31) {
                        $required = FALSE;
                    }
                } elseif ($turnover > ANNUAL_TOVR && $segment == "Services") {
                    if ($n == 8 || $n == 12 || $n == 13 || $n == 16 || $n == 17 || $n == 18 || $n == 19 || $n == 20 || $n == 27 || $n == 29 || $n == 30 || $n == 31) {
                        $required = FALSE;
                    }
                } elseif ($turnover > ANNUAL_TOVR && $segment == "Trading/Commercial") {
                    if ($n == 8 || $n == 12 || $n == 13 || $n == 14 || $n == 15 || $n == 18 || $n == 19 || $n == 20 || $n == 27 || $n == 29 || $n == 30 || $n == 31) {
                        $required = FALSE;
                    }
                } elseif ($turnover < ANNUAL_TOVR && $segment == "Contractors") {
                    if ($n == 4 || $n == 5 || $n == 12 || $n == 13 || $n == 14 || $n == 15 || $n == 16 || $n == 17 || $n == 19 || $n == 21 || $n == 26) {
                        $required = FALSE;
                    }
                } elseif ($turnover > ANNUAL_TOVR && $segment == "Contractors") {
                    if ($n == 8 || $n == 12 || $n == 13 || $n == 14 || $n == 15 || $n == 16 || $n == 17 || $n == 20 || $n == 27 || $n == 29 || $n == 30 || $n == 31) {
                        $required = FALSE;
                    }
                } else {
                    $required = FALSE;
                }
                if ($required && empty($value)) {
                    return FALSE;
                }
                $n++;
            endforeach;
        }
        return TRUE;
    }

    function borrower_assessment($case_id = NULL, $view_only = NULL) {
        check_param($case_id);
        // / Path Traversal Owasp testing issue
        foreach ($this->input->get() as $key => $value) {
            $_GET[$key] = $this->security->sanitize_filename($value);
            $_GET[$key] = $this->db->escape($value);
        }
        // ///////////
        $case_id = sanitize($case_id);
        $view_only = sanitize($view_only);
        $case = $this->Crud_model->get_one("cases", $case_id);
        if ($view_only != NULL) {
            if ($case->user_id != $this->session->userdata("user_id")) {
                check_perm("cases", 0);
            }
        } else {
            if ($case && $case->status == 'saved' && $case->approved == 0) {
                $this->session->set_flashdata('error', $this->lang->line('cannot_edit'));
                redirect(base_url('admin/Cases_two'));
            } else if ($case->approved == 1) {
                $this->session->set_flashdata('error', $this->lang->line('appoved_cannot_edit'));
                redirect(base_url('admin/Cases_two'));
            }
            // if (!$this->input->post("continue_later")) {
            // check_perm("cases", 1);
            // } else {
            // check_perm("cases", 2);
            // }
            if (!get_perm("cases", 1) && !get_perm("cases", 2)) {
                redirect(base_url("Welcome/permission_denied"));
            }
        }

        $this->Cases_two_model->check_case_status($case_id, $view_only, 'borrower_assessment');

        $old_case_arr = $this->Cases_two_model->get_assessment_arr($case_id);

        // call validation
        $condition = "return FALSE;";
        if (!$this->input->post("continue_later")) {
            $condition = $this->borrower_assessment_validation($case_id);
        } else {
            $this->borrower_assessment_later_validation();
        }
        if ($this->form_validation->run()) {
            // save in db
            $i = 0;
            $data = array();
            $row_data = array();
            $row_data['case_id'] = $case_id;
            $incremented = incremented("borrower_assessment");
            for ($i = 4; $i <= 229; $i ++) {
                if ($this->lang->line("borrower_assessment_F$i") != '') {
                    if ($i == 4 || $i == 17 || $i == 120 || $i == 132 || $i == 184 || $i == 214) {
                        continue;
                    }
                    $row_data["id"] = $incremented;
                    $row_data['row'] = $i;
                    $row_data['F'] = $this->input->post('F' . $i) || $this->input->post('F' . $i) === "0" ? $this->input->post('F' . $i, TRUE) : '';

                    array_push($data, $row_data);
                    $incremented ++;
                }
            }

            $this->db->delete('borrower_assessment', array(
                'case_id' => $case_id
            ));

            $if_exist = $this->db->affected_rows();

            $this->db->insert_batch('borrower_assessment', $data);

            $case_name = $this->get_case_name($case_id);
            if (!$this->input->post("continue_later")) {
                if ($this->Cases_two_model->if_curr_stage_pass($case_id, 'saved')) {
                    $this->db->update('cases', array(
                        'status' => 'saved',
                        'approved' => 0,
                        'approve_success' => 0,
                        'comment' => ''
                            ), array(
                        'id' => $case_id
                    ));
                }
                if ($if_exist) {
                    $this->check_update_notification("borrower_assessment", $old_case_arr);
                    save_audit("Case Borrower assessment info. upated in case -- " . $case_name);
                } else {
                    save_audit("Case Borrower assessment info. inserted in case -- " . $case_name);
                }
                $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully_case_finished"));
                redirect(base_url("admin/Cases_two"));
            } else {
                // if ($this->Cases_two_model->if_curr_stage_pass($case_id, 'borrower_assessment')) {
                $this->db->update('cases', array(
                    'status' => 'borrower_assessment'
                        ), array(
                    'id' => $case_id
                ));
                // }
                if ($if_exist) {
                    save_audit("Case Borrower assessment info. upated and marked as continue later in case -- " . $case_name);
                } else {
                    save_audit("Case Borrower assessment info. inserted and marked as continue later  in case -- " . $case_name);
                }
                $this->session->set_flashdata("success", $this->lang->line("data_saved_successfully"));
                redirect(base_url("admin/Cases_two"));
            }
        } else {
            // save invalid data
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $this->save_assessment_invalid($case_id);
            }
            // load view
            $data['title'] = $this->lang->line("borrower_assessment");
            // $data['borrower_info_year'] = $this->Cases_two_model->get_borrower_info_year($case_id);
            $data['assessment_data'] = $this->Cases_two_model->get_assessment_data($case_id);
            $data['label_att'] = 'col-sm-3 control-label';
            if ($view_only != NULL) {
                $data['view_only'] = 1;
            }
            $data['is_case_pass_this_setp'] = $this->Cases_two_model->if_curr_stage_pass($case_id, 'borrower_assessment');
            $data['case_id'] = $case_id;
            $get_case = $this->Crud_model->get_one("cases", $case_id);
            $data["approved"] = $get_case->approved;
            $data["case_status"] = $get_case->status;
            $data['condition'] = $condition;
            $data["param_39"] = $this->compute_39($case_id);
            $data["param_40"] = $this->compute_40($case_id);
            $this->load->view("admin/pages/cases/borrower_assessment", $data);
        }
    }

    // function borrower_assessment_validation() {
    // for ($i = 4; $i <= 229; $i++) {
    // $required = '|required';
    // if ($this->lang->line("borrower_assessment_F$i") != '') {
    // if ($i == 4 || $i == 17 || $i == 120 || $i == 132 || $i == 184 || $i == 214) {
    // continue;
    // }
    // if ($i == 22 || $i == 29 || $i == 61 || $i == 71 || $i == 75 || $i == 79 || $i == 86 || $i == 93 || $i == 100 || $i == 107 || $i == 115 || $i == 125 || $i == 151) {
    // $required = '';
    // }
    // $this->form_validation->set_rules("F$i", $this->lang->line("borrower_assessment_F$i"), "xss_clean|trim$required");
    // }
    // }
    // }
    function borrower_assessment_validation($case_id = NULL) {
        $case_id = sanitize($case_id);
        $i = 23;
        $borrower_information = $this->Cases_two_model->get_borrower_information($case_id);
        $turnover = $borrower_information->C12;
        $segment = get_value_from_code("c10", $borrower_information->C10);

        $condition = "return FALSE;";
        // modified
        if (($turnover < ANNUAL_TOVR && $segment == "Manufacturing") || ($turnover < ANNUAL_TOVR && $segment == "Services") || ($turnover < ANNUAL_TOVR && $segment == "Trading/Commercial")) {
            // 4,5,11,13,14,15,16,17,18,19,20,22& 27
            // 4,5,12,13,14,15,16,17,18,19,21& 26
            $condition = 'return $n==4||$n==5||$n==12||$n==13||$n==14||$n==15||$n==16||$n==17||$n==18||$n==19||$n==21||$n==26 ;';
        } elseif ($turnover > ANNUAL_TOVR && $segment == "Manufacturing") {
            // 8,10,15,16,17,18,19,20,21,28,30,31& 32
            // 8,14,15,16,17,18,19,20,27,29,30& 31
            $condition = 'return $n==8||$n==14||$n==15||$n==16||$n==17||$n==18||$n==19||$n==20||$n==27||$n==29||$n==30||$n==31 ;';
        } elseif ($turnover > ANNUAL_TOVR && $segment == "Services") {
            // 8,10,13,14,17,18,19,20,21,28,30,31& 32
            // 8,12,13,16,17,18,19,20,27,29,30& 31
            $condition = 'return $n==8||$n==12||$n==13||$n==16||$n==17||$n==18||$n==19||$n==20||$n==27||$n==29||$n==30||$n==31 ;';
        } elseif ($turnover > ANNUAL_TOVR && $segment == "Trading/Commercial") {
            // 8,10,13,14,15,16,19,20,21,28,30,31& 32
            // 8,12,13,14,15,18,19,20,27,29,30& 31
            $condition = 'return $n==8||$n==12||$n==13||$n==14||$n==15||$n==18||$n==19||$n==20||$n==27||$n==29||$n==30||$n==31 ;';
        } elseif ($turnover < ANNUAL_TOVR && $segment == "Contractors") {
            // 4,5,11,13,14,15,16,17,18,20,22& 27
            // 4,5,12,13,14,15,16,17,19,21& 26
            $condition = 'return $n==4||$n==5||$n==12||$n==13||$n==14||$n==15||$n==16||$n==17||$n==19||$n==21||$n==26 ;';
        } elseif ($turnover > ANNUAL_TOVR && $segment == "Contractors") {
            // 8,10,13,14,15,16,17,18,21,28,30,31& 32
            // 8,12,13,14,15,16,17,20,27,29,30& 31
            $condition = 'return $n==8||$n==12||$n==13||$n==14||$n==15||$n==16||$n==17||$n==20||$n==27||$n==29||$n==30||$n==31 ;';
        } else {
            $condition = "return 1==1;";
        }
        $n = 1;
        for ($i = 4; $i <= 229; $i ++) {
            $required = '|required|callback_check_default_val';
            if ($this->lang->line("borrower_assessment_F$i") != '') {
                if ($i == 4 || $i == 17 || $i == 120 || $i == 132 || $i == 184 || $i == 214) {
                    continue;
                }
                if (($turnover < ANNUAL_TOVR && $segment == "Manufacturing") || ($turnover < ANNUAL_TOVR && $segment == "Services") || ($turnover < ANNUAL_TOVR && $segment == "Trading/Commercial")) {
                    // 4,5,11,13,14,15,16,17,18,19,20,22& 27
                    // 4,5,12,13,14,15,16,17,18,19,21& 26
                    if ($n == 4 || $n == 5 || $n == 12 || $n == 13 || $n == 14 || $n == 15 || $n == 16 || $n == 17 || $n == 18 || $n == 19 || $n == 21 || $n == 26) {
                        $required = '';
                    }
                } elseif ($turnover > ANNUAL_TOVR && $segment == "Manufacturing") {
                    // 8,10,15,16,17,18,19,20,21,28,30,31& 32
                    // 8,14,15,16,17,18,19,20,27,29,30& 31
                    if ($n == 8 || $n == 14 || $n == 15 || $n == 16 || $n == 17 || $n == 18 || $n == 19 || $n == 20 || $n == 27 || $n == 29 || $n == 30 || $n == 31) {
                        $required = '';
                    }
                } elseif ($turnover > ANNUAL_TOVR && $segment == "Services") {
                    // 8,10,13,14,17,18,19,20,21,28,30,31& 32
                    // 8,12,13,16,17,18,19,20,27,29,30& 31
                    if ($n == 8 || $n == 12 || $n == 13 || $n == 16 || $n == 17 || $n == 18 || $n == 19 || $n == 20 || $n == 27 || $n == 29 || $n == 30 || $n == 31) {
                        $required = '';
                    }
                } elseif ($turnover > ANNUAL_TOVR && $segment == "Trading/Commercial") {
                    // 8,10,13,14,15,16,19,20,21,28,30,31& 32
                    // 8,12,13,14,15,18,19,20,27,29,30& 31
                    if ($n == 8 || $n == 12 || $n == 13 || $n == 14 || $n == 15 || $n == 18 || $n == 19 || $n == 20 || $n == 27 || $n == 29 || $n == 30 || $n == 31) {
                        $required = '';
                    }
                } elseif ($turnover < ANNUAL_TOVR && $segment == "Contractors") {
                    // 4,5,11,13,14,15,16,17,18,20,22& 27
                    // 4,5,12,13,14,15,16,17,19,21& 26
                    if ($n == 4 || $n == 5 || $n == 12 || $n == 13 || $n == 14 || $n == 15 || $n == 16 || $n == 17 || $n == 19 || $n == 21 || $n == 26) {
                        $required = '';
                    }
                } elseif ($turnover > ANNUAL_TOVR && $segment == "Contractors") {
                    // 8,10,13,14,15,16,17,18,21,28,30,31& 32
                    // 8,12,13,14,15,16,17,20,27,29,30& 31
                    if ($n == 8 || $n == 12 || $n == 13 || $n == 14 || $n == 15 || $n == 16 || $n == 17 || $n == 20 || $n == 27 || $n == 29 || $n == 30 || $n == 31) {
                        $required = '';
                    }
                } else {
                    $required = '';
                }
                // it was
                // if (eval($condition)) {
                // $required = '';
                // }
                $this->form_validation->set_rules("F$i", $this->lang->line("borrower_assessment_F$i"), "xss_clean$required");
                $n ++;
            }
        }
        return $condition;
    }

    function check_default_val($val) {
        if ($val == 'Select') {
            $this->form_validation->set_message('check_default_val', $this->lang->line('required_field'));
            return FALSE;
        }
        return TRUE;
    }

    function borrower_assessment_later_validation() {
        for ($i = 4; $i <= 229; $i ++) {
            if ($this->lang->line("borrower_assessment_F$i") != '') {
                if ($i == 4 || $i == 17 || $i == 120 || $i == 132 || $i == 184 || $i == 214) {
                    continue;
                }
                $this->form_validation->set_rules("F$i", $this->lang->line("borrower_assessment_F$i"), "xss_clean");
            }
        }
    }

    function continue_case($case_id) {
        $case_id = sanitize($case_id);
        $this->db->select('status');
        $case = $this->db->get_where('cases', array(
                    'id' => $case_id
                ))->row();

        if (!$case) {
            redirect('admin/Dashboard');
        }

        switch ($case->status) {
            case 'borrower_information':
                $redirect = 'Cases/borrower_information/' . $case_id;
                break;
            case 'borrower_factsheet':
                $redirect = 'Cases_two/borrower_factsheet/' . $case_id;
                break;
            case 'borrower_financials':
                $redirect = 'Cases_two/borrower_financials/' . $case_id;
                break;
            case 'borrower_assessment':
                $redirect = 'Cases_two/borrower_assessment/' . $case_id;
                break;
            default:
                $redirect = 'Dashboard';
                break;
        }
        redirect('admin/' . $redirect);
    }

    function view_case($case_id) {
        $case_id = sanitize($case_id);
        redirect('admin/Cases/borrower_information/' . $case_id . '/1');
    }

    function delete_case($case_id) {
        $case_id = sanitize($case_id);
        check_perm("cases", 3);
        $case_name = $this->get_case_name($case_id);
        $this->db->delete('borrower_assessment', array(
            'case_id' => $case_id
        ));
        $this->db->delete('borrower_financials', array(
            'case_id' => $case_id
        ));
        $this->db->delete('borrower_factsheet', array(
            'case_id' => $case_id
        ));
        $this->db->delete('borrower_information', array(
            'case_id' => $case_id
        ));
        $this->db->delete('cases', array(
            'id' => $case_id
        ));
        save_audit('delete case: ' . $case_name);

        $this->session->set_flashdata("success", $this->lang->line("case_deleted"));
        redirect(base_url("admin/Cases_two"));
    }

    function gradation_report() {
        $this->session->set_userdata('active', 'g_report');
        // load view
        if (substr($this->session->userdata('levels')->gradation_rep, 1, 1) != '0' || substr($this->session->userdata('levels')->gradation_rep, 2, 1) != '0') {
            $data['title'] = $this->lang->line("gradation_report");
            $data['cases'] = $this->Cases_two_model->get_cases();
            $data['label_att'] = 'col-sm-3 control-label';
            $this->load->view("admin/pages/cases/upload_gradation_report", $data);
        } else {
            $this->session->set_flashdata('no_permission', 1);
            redirect('admin/Dashboard');
        }
    }

    function test_pdf() {
        $pdf_name = $file_name = "M1B1234C77672ITestDateD25032019.pdf";
        $file = explode('.', $file_name)[0];
        $file = explode('.', $file_name)[0];

        $pos = strrpos($file, "D");
        if ($pos) {
            $file = substr($file, 0, $pos);
        }
        echo $file;
    }

    function upload_pdf() {
        $pdf_name = $file_name = $_FILES['file']['name'];
        $file = explode('.', $file_name)[0];
        // $file = explode('.', $file_name)[0];

        $pos = strrpos($file, "D");
        if ($pos) {
            $file = substr($file, 0, $pos);
        }
        $this->db->select('cases.name,banks.name as bank_name,users.branch_code,cases.date_approved as date_approved,cases.id');
        $this->db->where("cases.completed !=", 1);
        $this->db->join('users', 'users.id=cases.user_id');
        $this->db->join('banks', 'users.bank_id=banks.id'); // and banks.non_subscriber = 0
        $this->db->like('cases.name', $file, 'after');
        $case = $this->db->get('cases')->row();
        if (!$case) {
            header("HTTP/1.0 400 Bad Request");
            echo $this->lang->line('case_not_found_copy');
            return;
        }

        $case->bank_name = replace_bank_name($case->bank_name);
        $config['upload_path'] = "./root/$case->bank_name/$case->branch_code/Gradation Results/";
        // $config['upload_path'] = "$case->bank_name/$case->branch_code/Gradation Results/";
        $config['allowed_types'] = 'pdf|xml';
        $config['max_size'] = '20000';
        $this->lang->load('upload');
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('file')) {
            // $upload_data = $this->upload->data();
            // if (!unlink("./root/Cases/" . date('d-m-Y', strtotime($case->date_approved)) . "/$file.xlsx")) {
            if (!rename("./root/Cases/" . date('Y_m_d', strtotime($case->date_approved)) . "/$case->name.xlsx", "./root/$case->bank_name/$case->branch_code/$case->name.xlsx") && !rename("./root/Cases/" . date('d-m-Y', strtotime($case->date_approved)) . "/$case->name.xlsx", "./root/$case->bank_name/$case->branch_code/$case->name.xlsx")) {
                save_audit('fix :: Cannot Move File');
                header("HTTP/1.0 400 Bad Request");
                echo "Can't Move File";
                return;
            }
            $this->db->update('cases', array(
                'date_completed' => date('Y-m-d H:i:s'),
                'completed' => 1,
                "pdf_name" => $pdf_name
                    ), array(
                'id' => $case->id
            ));
            save_audit('upload gradation report : ' . $file_name);
        } else {
            $errors = $this->upload->display_errors();
            save_audit('failed to upload gradation report : ' . $file_name . ' due to ' . $errors);
            header("HTTP/1.0 400 Bad Request");
            echo $errors;
        }
    }

    function validate_total_sales($hold, $case_id) {
        $case_id = sanitize($case_id);
        $hold = sanitize($hold);
        $borrower_information = $this->Cases_two_model->get_borrower_information($case_id);
        $this->form_validation->set_message('validate_total_sales', $this->lang->line("bi_incomplete"));
        if (!empty($borrower_information)) {
            $this->form_validation->set_message('validate_total_sales', $this->lang->line("total_sales_msg"));
            $annual_turnover = 0;
            $G8 = $this->input->post("G8");
            $months = $this->input->post("E13");
            if ($borrower_information->C12) {
                $annual_turnover = $borrower_information->C12;
                $annual_turnover = ($annual_turnover / 12) * $months;
                $sum = $this->get_numeric($this->input->post("E18")) + $this->get_numeric($this->input->post("E19"));
                if ($G8 == "Thousands") {
                    $sum = $sum * 1000;
                } elseif ($G8 == "Millions") {
                    $sum = $sum * 1000000;
                }
                $diff = abs($sum - $annual_turnover);
                if ($diff <= 1000) {
                    return TRUE;
                }
            }
            return FALSE;
        }
        return FALSE;
    }

    function validate_assets($hold) {
        $hold = sanitize($hold);
        // liabilities C119+C117+C116+C101+C95
        // C119 +
        // C117+
        // C106->115+
        // C97->C100+
        // SUM(C78:C90)+C91+C94
        $this->form_validation->set_message('validate_assets', $this->lang->line("assets_msg"));
        // E23
        $E23 = $this->get_numeric($this->input->post("E18")) + $this->get_numeric($this->input->post("E19")) + $this->get_numeric($this->input->post("E22"));
        if ($E23 === 0) {
            $this->form_validation->set_message('validate_assets', $this->lang->line("borrower_financial_23") . $this->lang->line("non_zero"));
            return FALSE;
        }
        // E29
        $E29 = $this->get_numeric($this->input->post("E26")) + $this->get_numeric($this->input->post("E27")) + $this->get_numeric($this->input->post("E28"));
        if ($E29 === 0) {
            $this->form_validation->set_message('validate_assets', $this->lang->line("borrower_financial_29") . $this->lang->line("non_zero"));
            return FALSE;
        }
        // E121
        // E170
        // liabilities
        $liabilities[0] = $this->get_numeric($this->input->post("C119")) + $this->get_numeric($this->input->post("C117")) + $this->get_numeric($this->input->post("C92")) + $this->get_numeric($this->input->post("C93")) + $this->get_numeric($this->input->post("C94"));
        $liabilities[1] = $this->get_numeric($this->input->post("D119")) + $this->get_numeric($this->input->post("D117")) + $this->get_numeric($this->input->post("D92")) + $this->get_numeric($this->input->post("D93")) + $this->get_numeric($this->input->post("D94"));
        $liabilities[2] = $this->get_numeric($this->input->post("E119")) + $this->get_numeric($this->input->post("E117")) + $this->get_numeric($this->input->post("E92")) + $this->get_numeric($this->input->post("E93")) + $this->get_numeric($this->input->post("E94"));
        for ($i = 106; $i <= 115; $i ++) {
            $liabilities[0] += $this->get_numeric($this->input->post("C$i"));
            $liabilities[1] += $this->get_numeric($this->input->post("D$i"));
            $liabilities[2] += $this->get_numeric($this->input->post("E$i"));
        }
        for ($i = 97; $i <= 100; $i ++) {
            $liabilities[0] += $this->get_numeric($this->input->post("C$i"));
            $liabilities[1] += $this->get_numeric($this->input->post("D$i"));
            $liabilities[2] += $this->get_numeric($this->input->post("E$i"));
        }
        for ($i = 78; $i <= 90; $i ++) {
            $liabilities[0] += $this->get_numeric($this->input->post("C$i"));
            $liabilities[1] += $this->get_numeric($this->input->post("D$i"));
            $liabilities[2] += $this->get_numeric($this->input->post("E$i"));
        }

        if ($liabilities[2] === 0) {
            $this->form_validation->set_message('validate_assets', $this->lang->line("total_liabilities") . $this->lang->line("non_zero"));
            return FALSE;
        }

        // assets
        // Assets C168+C164+C141+C150
        // / C166-167 +
        // / C153+C154+C155+C157+C156 +
        // C126+C127+C128+C138+C131+C137+C139+C140 +
        // / (C145->148)-C149
        $assets[0] = ($this->get_numeric($this->input->post("C166")) - $this->get_numeric($this->input->post("C167"))) + (($this->get_numeric($this->input->post("C145")) + $this->get_numeric($this->input->post("C146")) + $this->get_numeric($this->input->post("C147")) + $this->get_numeric($this->input->post("C148"))) - $this->get_numeric($this->input->post("C149"))) +
                // modified
                ($this->get_numeric($this->input->post("C129")) - $this->get_numeric($this->input->post("C130")));
        $assets[1] = ($this->get_numeric($this->input->post("D166")) - $this->get_numeric($this->input->post("D167"))) + (($this->get_numeric($this->input->post("D145")) + $this->get_numeric($this->input->post("D146")) + $this->get_numeric($this->input->post("D147")) + $this->get_numeric($this->input->post("D148"))) - $this->get_numeric($this->input->post("D149"))) +
                // modified
                ($this->get_numeric($this->input->post("D129")) - $this->get_numeric($this->input->post("D130")));
        $assets[2] = ($this->get_numeric($this->input->post("E166")) - $this->get_numeric($this->input->post("E167"))) + (($this->get_numeric($this->input->post("E145")) + $this->get_numeric($this->input->post("E146")) + $this->get_numeric($this->input->post("E147")) + $this->get_numeric($this->input->post("E148"))) - $this->get_numeric($this->input->post("E149"))) +
                // modified
                ($this->get_numeric($this->input->post("E129")) - $this->get_numeric($this->input->post("E130")));
        for ($i = 153; $i <= 156; $i ++) {
            $assets[0] += $this->get_numeric($this->input->post("C$i"));
            $assets[1] += $this->get_numeric($this->input->post("D$i"));
            $assets[2] += $this->get_numeric($this->input->post("E$i"));
        }
        for ($i = 158; $i <= 163; $i ++) {
            $assets[0] += $this->get_numeric($this->input->post("C$i"));
            $assets[1] += $this->get_numeric($this->input->post("D$i"));
            $assets[2] += $this->get_numeric($this->input->post("E$i"));
        }
        for ($i = 126; $i <= 127; $i ++) {
            $assets[0] += $this->get_numeric($this->input->post("C$i"));
            $assets[1] += $this->get_numeric($this->input->post("D$i"));
            $assets[2] += $this->get_numeric($this->input->post("E$i"));
        }
        for ($i = 137; $i <= 140; $i ++) {
            $assets[0] += $this->get_numeric($this->input->post("C$i"));
            $assets[1] += $this->get_numeric($this->input->post("D$i"));
            $assets[2] += $this->get_numeric($this->input->post("E$i"));
        }
        for ($i = 132; $i <= 136; $i ++) {
            $assets[0] += $this->get_numeric($this->input->post("C$i"));
            $assets[1] += $this->get_numeric($this->input->post("D$i"));
            $assets[2] += $this->get_numeric($this->input->post("E$i"));
        }

        // modified2
        $sub[0] = $assets[0] - $liabilities[0];
        $sub[1] = $assets[1] - $liabilities[1];
        $sub[2] = $assets[2] - $liabilities[2];
        // if($assets[0]==$liabilities[0] && $assets[1]==$liabilities[1] && $assets[2]==$liabilities[2]){
        // return TRUE;
        // }
        if ($sub[0] == 0 && $sub[1] == 0 && $sub[2] == 0) {
            return TRUE;
        }

        return FALSE;
    }

    function compute_40($case_id = NULL) {
        $row = $this->Cases_two_model->get_financials_row($case_id, 3);
        $return = "NA";
        $C218 = 0;

        // modified2
        $calc = $this->Cases_two_model->get_calc($case_id);
        // $this->db->where("rown", 219);
        // $this->db->where("case_id", $case_id);
        // $calc = $this->db->get("calculations")->row();
        // $calc=[];
        // foreach ($calc_arr as $one){
        // $calc[$one->rown]['C'] = $one->C;
        // $calc[$one->rown]['D'] = $one->D;
        // $calc[$one->rown]['E'] = $one->E;
        // $calc[$one->rown]['F'] = $one->F;
        // $calc[$one->rown]['G'] = $one->G;
        // }
        // $data['calc']=$calc;
        // print_r($calc);die;
        if (!empty($row) && $calc) {
            if ($row->E != "no") {
                $C218 = $this->get_numeric($calc->C) + $this->get_numeric($calc->D) + $this->get_numeric($calc->E);
            }
        }
        if ($C218 == 3) {
            $return = 1;
        } elseif ($C218 == 2) {
            $return = 0.9;
        } elseif ($C218 == 1) {
            $return = 0.8;
        }
        return $return;
    }

    function compute_40_2($case_id = NULL) {
        $case_id = sanitize($case_id);
        // IF('Borrower Financials'!$C$218=3,D229,IF('Borrower Financials'!$C$218=2,D230,IF('Borrower Financials'!$C$218=1,D231,IF('Borrower Financials'!$C$218="NA",D232))))
        $row = $this->Cases_two_model->get_financials_row($case_id, 3);
        $return = "NA";
        $C218 = 0;
        if (!empty($row)) {
            if ($row->E != "no") {
                $C219 = 0;
                $D219 = 0;
                $E219 = 0;
                $F219 = 0;
                $G219 = 0;
                $row_18 = $this->Cases_two_model->get_financials_row($case_id, 18);
                $row_19 = $this->Cases_two_model->get_financials_row($case_id, 19);
                $row_22 = $this->Cases_two_model->get_financials_row($case_id, 22);

                $row_166 = $this->Cases_two_model->get_financials_row($case_id, 166);
                $row_167 = $this->Cases_two_model->get_financials_row($case_id, 167);

                $row_153 = $this->Cases_two_model->get_financials_row($case_id, 153);
                $row_154 = $this->Cases_two_model->get_financials_row($case_id, 154);
                $row_155 = $this->Cases_two_model->get_financials_row($case_id, 155);
                $row_156 = $this->Cases_two_model->get_financials_row($case_id, 156);

                $row_158 = $this->Cases_two_model->get_financials_row($case_id, 158);
                $row_159 = $this->Cases_two_model->get_financials_row($case_id, 159);
                $row_160 = $this->Cases_two_model->get_financials_row($case_id, 160);
                $row_161 = $this->Cases_two_model->get_financials_row($case_id, 161);
                $row_162 = $this->Cases_two_model->get_financials_row($case_id, 162);
                $row_163 = $this->Cases_two_model->get_financials_row($case_id, 163);

                $row_126 = $this->Cases_two_model->get_financials_row($case_id, 126);
                $row_127 = $this->Cases_two_model->get_financials_row($case_id, 127);

                $row_129 = $this->Cases_two_model->get_financials_row($case_id, 129);
                $row_130 = $this->Cases_two_model->get_financials_row($case_id, 130);
                $row_138 = $this->Cases_two_model->get_financials_row($case_id, 138);

                $row_132 = $this->Cases_two_model->get_financials_row($case_id, 132);
                $row_133 = $this->Cases_two_model->get_financials_row($case_id, 133);
                $row_134 = $this->Cases_two_model->get_financials_row($case_id, 134);
                $row_135 = $this->Cases_two_model->get_financials_row($case_id, 135);
                $row_136 = $this->Cases_two_model->get_financials_row($case_id, 136);

                $row_137 = $this->Cases_two_model->get_financials_row($case_id, 137);
                $row_139 = $this->Cases_two_model->get_financials_row($case_id, 139);
                $row_140 = $this->Cases_two_model->get_financials_row($case_id, 140);

                $row_145 = $this->Cases_two_model->get_financials_row($case_id, 145);
                $row_146 = $this->Cases_two_model->get_financials_row($case_id, 146);
                $row_147 = $this->Cases_two_model->get_financials_row($case_id, 147);
                $row_148 = $this->Cases_two_model->get_financials_row($case_id, 148);
                $row_149 = $this->Cases_two_model->get_financials_row($case_id, 149);

                $row_119 = $this->Cases_two_model->get_financials_row($case_id, 119);
                $row_117 = $this->Cases_two_model->get_financials_row($case_id, 117);
                $row_92 = $this->Cases_two_model->get_financials_row($case_id, 92);
                $row_93 = $this->Cases_two_model->get_financials_row($case_id, 93);
                $row_94 = $this->Cases_two_model->get_financials_row($case_id, 94);

                $row_18 = $this->set_zero($row_18);
                $row_19 = $this->set_zero($row_19);
                $row_22 = $this->set_zero($row_22);

                $row_166 = $this->set_zero($row_166);
                $row_167 = $this->set_zero($row_167);

                $row_153 = $this->set_zero($row_153);
                $row_154 = $this->set_zero($row_154);
                $row_155 = $this->set_zero($row_155);
                $row_156 = $this->set_zero($row_156);

                $row_158 = $this->set_zero($row_158);
                $row_159 = $this->set_zero($row_159);
                $row_160 = $this->set_zero($row_160);
                $row_161 = $this->set_zero($row_161);
                $row_162 = $this->set_zero($row_162);
                $row_163 = $this->set_zero($row_163);

                $row_126 = $this->set_zero($row_126);
                $row_127 = $this->set_zero($row_127);

                $row_129 = $this->set_zero($row_129);
                $row_130 = $this->set_zero($row_130);
                $row_138 = $this->set_zero($row_138);

                $row_132 = $this->set_zero($row_132);
                $row_133 = $this->set_zero($row_133);
                $row_134 = $this->set_zero($row_134);
                $row_135 = $this->set_zero($row_135);
                $row_136 = $this->set_zero($row_136);

                $row_137 = $this->set_zero($row_137);
                $row_139 = $this->set_zero($row_139);
                $row_140 = $this->set_zero($row_140);

                $row_145 = $this->set_zero($row_145);
                $row_146 = $this->set_zero($row_146);
                $row_147 = $this->set_zero($row_147);
                $row_148 = $this->set_zero($row_148);
                $row_149 = $this->set_zero($row_149);

                $row_119 = $this->set_zero($row_119);
                $row_117 = $this->set_zero($row_117);
                $row_92 = $this->set_zero($row_92);
                $row_93 = $this->set_zero($row_93);
                $row_94 = $this->set_zero($row_94);

                // BEGIN C
                $C23 = $row_18->C + $row_19->C + $row_22->C;
                // C168
                $C170 = ($row_166->C - $row_167->C) +
                        // C164
                        ($row_153->C + $row_154->C + $row_155->C + $row_156->C) + ($row_158->C + $row_159->C + $row_160->C + $row_161->C + $row_162->C + $row_163->C) +
                        // C141
                        ($row_126->C + $row_127->C + ($row_129->C - $row_130->C) + $row_138->C) + ($row_132->C + $row_133->C + $row_134->C + $row_135->C + $row_136->C) + $row_137->C + $row_139->C + $row_140->C +
                        // C150
                        (($row_145->C + $row_146->C + $row_147->C - $row_148->C) - $row_149->C);
                // C170 =//C168+//C164+//C141+//C150
                // //C121
                $C121 = $row_119->C + $row_117->C + $row_92->C + $row_93->C + $row_94->C;
                for ($i = 106; $i <= 115; $i ++) {
                    // modified
                    $object = $this->set_zero($this->Cases_two_model->get_financials_row($case_id, $i));
                    $C121 += $object->C;
                }
                for ($i = 97; $i <= 100; $i ++) {
                    // modified
                    $object = $this->set_zero($this->Cases_two_model->get_financials_row($case_id, $i));
                    $C121 += $object->C;
                }
                for ($i = 78; $i <= 90; $i ++) {
                    // modified
                    $object = $this->set_zero($this->Cases_two_model->get_financials_row($case_id, $i));
                    $C121 += $object->C;
                }

                //
                if ($C23 > 0 && $C170 > 0 && $C121 > 0) {
                    $C219 = 1;
                }
                // END C
                // BEGIN D
                $D23 = $row_18->D + $row_19->D + $row_22->D;
                // D168
                $D170 = ($row_166->D - $row_167->D) +
                        // D164
                        ($row_153->D + $row_154->D + $row_155->D + $row_156->D) + ($row_158->D + $row_159->D + $row_160->D + $row_161->D + $row_162->D + $row_163->D) +
                        // D141
                        ($row_126->D + $row_127->D + ($row_129->D - $row_130->D) + $row_138->D) + ($row_132->D + $row_133->D + $row_134->D + $row_135->D + $row_136->D) + $row_137->D + $row_139->D + $row_140->D +
                        // D150
                        (($row_145->D + $row_146->D + $row_147->D - $row_148->D) - $row_149->D);
                // D170 =//D168+//D164+//D141+//D150
                // //D121
                $D121 = $row_119->D + $row_117->D + $row_92->D + $row_93->D + $row_94->D;
                for ($i = 106; $i <= 115; $i ++) {
                    // modified
                    $object = $this->set_zero($this->Cases_two_model->get_financials_row($case_id, $i));
                    $D121 += $object->D;
                }
                for ($i = 97; $i <= 100; $i ++) {
                    // modified
                    $object = $this->set_zero($this->Cases_two_model->get_financials_row($case_id, $i));
                    $D121 += $object->D;
                }
                for ($i = 78; $i <= 90; $i ++) {
                    // modified
                    $object = $this->set_zero($this->Cases_two_model->get_financials_row($case_id, $i));
                    $D121 += $object->D;
                }

                //
                if ($D23 > 0 && $D170 > 0 && $D121 > 0) {
                    $D219 = 1;
                }
                // END D
                // BEGIN E
                $E23 = $row_18->E + $row_19->E + $row_22->E;
                // E168
                $E170 = ($row_166->E - $row_167->E) +
                        // E164
                        ($row_153->E + $row_154->E + $row_155->E + $row_156->E) + ($row_158->E + $row_159->E + $row_160->E + $row_161->E + $row_162->E + $row_163->E) +
                        // E141
                        ($row_126->E + $row_127->E + ($row_129->E - $row_130->E) + $row_138->E) + ($row_132->E + $row_133->E + $row_134->E + $row_135->E + $row_136->E) + $row_137->E + $row_139->E + $row_140->E +
                        // E150
                        (($row_145->E + $row_146->E + $row_147->E - $row_148->E) - $row_149->E);
                // E170 =//E168+//E164+//E141+//E150
                // //E121
                $E121 = $row_119->E + $row_117->E + $row_92->E + $row_93->E + $row_94->E;
                for ($i = 106; $i <= 115; $i ++) {
                    // modified
                    $object = $this->set_zero($this->Cases_two_model->get_financials_row($case_id, $i));
                    $E121 += $object->E;
                }
                for ($i = 97; $i <= 100; $i ++) {
                    // modified
                    $object = $this->set_zero($this->Cases_two_model->get_financials_row($case_id, $i));
                    $E121 += $object->E;
                }
                for ($i = 78; $i <= 90; $i ++) {
                    // modified
                    $object = $this->set_zero($this->Cases_two_model->get_financials_row($case_id, $i));
                    $E121 += $object->E;
                }

                //
                if ($E23 > 0 && $E170 > 0 && $E121 > 0) {
                    $E219 = 1;
                }
                // END E
                // BFGIN F
                $F23 = $row_18->F + $row_19->F + $row_22->F;
                // F168
                $F170 = ($row_166->F - $row_167->F) +
                        // F164
                        ($row_153->F + $row_154->F + $row_155->F + $row_156->F) + ($row_158->F + $row_159->F + $row_160->F + $row_161->F + $row_162->F + $row_163->F) +
                        // F141
                        ($row_126->F + $row_127->F + ($row_129->F - $row_130->F) + $row_138->F) + ($row_132->F + $row_133->F + $row_134->F + $row_135->F + $row_136->F) + $row_137->F + $row_139->F + $row_140->F +
                        // F150
                        (($row_145->F + $row_146->F + $row_147->F - $row_148->F) - $row_149->F);
                // F170 =//F168+//F164+//F141+//F150
                // //F121
                $F121 = $row_119->F + $row_117->F + $row_92->F + $row_93->F + $row_94->F;
                for ($i = 106; $i <= 115; $i ++) {
                    // modified
                    $object = $this->set_zero($this->Cases_two_model->get_financials_row($case_id, $i));
                    $F121 += $object->F;
                }
                for ($i = 97; $i <= 100; $i ++) {
                    // modified
                    $object = $this->set_zero($this->Cases_two_model->get_financials_row($case_id, $i));
                    $F121 += $object->F;
                }
                for ($i = 78; $i <= 90; $i ++) {
                    // modified
                    $object = $this->set_zero($this->Cases_two_model->get_financials_row($case_id, $i));
                    $F121 += $object->F;
                }

                //
                if ($F23 > 0 && $F170 > 0 && $F121 > 0) {
                    $F219 = 1;
                }
                // END F
                // BGGIN G
                $G23 = $row_18->G + $row_19->G + $row_22->G;
                // G168
                $G170 = ($row_166->G - $row_167->G) +
                        // G164
                        ($row_153->G + $row_154->G + $row_155->G + $row_156->G) + ($row_158->G + $row_159->G + $row_160->G + $row_161->G + $row_162->G + $row_163->G) +
                        // G141
                        ($row_126->G + $row_127->G + ($row_129->G - $row_130->G) + $row_138->G) + ($row_132->G + $row_133->G + $row_134->G + $row_135->G + $row_136->G) + $row_137->G + $row_139->G + $row_140->G +
                        // G150
                        (($row_145->G + $row_146->G + $row_147->G - $row_148->G) - $row_149->G);
                // G170 =//G168+//G164+//G141+//G150
                // //G121
                $G121 = $row_119->G + $row_117->G + $row_92->G + $row_93->G + $row_94->G;
                for ($i = 106; $i <= 115; $i ++) {
                    // modified
                    $object = $this->set_zero($this->Cases_two_model->get_financials_row($case_id, $i));
                    $G121 += $object->G;
                }
                for ($i = 97; $i <= 100; $i ++) {
                    // modified
                    $object = $this->set_zero($this->Cases_two_model->get_financials_row($case_id, $i));
                    $G121 += $object->G;
                }
                for ($i = 78; $i <= 90; $i ++) {
                    // modified
                    $object = $this->set_zero($this->Cases_two_model->get_financials_row($case_id, $i));
                    $G121 += $object->G;
                }

                //
                if ($G23 > 0 && $G170 > 0 && $G121 > 0) {
                    $G219 = 1;
                }
                // END G
                $C218 = $C219 + $D219 + $E219 + $F219 + $G219;
            }

            if ($C218 == 3) {
                $return = 1;
            } elseif ($C218 == 2) {
                $return = 0.9;
            } elseif ($C218 == 1) {
                $return = 0.8;
            }
        }
        return $return;
    }

    function compute_39($case_id = NULL) {
        $case_id = sanitize($case_id);
        // VLOOKUP('Borrower Financials'!E211,'Borrower Assessment'!$M$221:$O$227,3,TRUE)
        // IF(E3="No",0,
        // IF(
        // AND(ISNUMBER('Borrower Information'!D32),'Borrower Information'!D32=0)
        // ,999,IF(ISERROR(E210/'Borrower Information'!$D$32),"NA",(E210/'Borrower Information'!$D$32))))
        // if(bfE3=="No"){
        // $return =0;
        // }
        // elseif(isnumber(biD32) && biD32==0){
        // $return=999;
        // }
        // elseif(ISERROR(E210/'Borrower Information'!$D$32){
        // $return="NA";
        // }
        // else{
        // $return= bfE210/'Borrower Information'!$D$32;
        // }
        // IF(ISERROR(AVERAGE('Borrower Information'!$D$25:$D$30)),"NA",AVERAGE('Borrower Information'!$D$25:$D$30))
        $return = "NA";
        $borrower_information = $this->Cases_two_model->get_borrower_information($case_id);
        $row_3 = $this->Cases_two_model->get_financials_row($case_id, 3);
        if (!empty($borrower_information) && !empty($row_3)) {
            if ($row_3->E == "no") {
                $return = "NA";
            } elseif (is_numeric($borrower_information->D32) && $borrower_information->D32 === 0) {
                $return = 999;
            } else {

                $avg = 0;
                for ($i = 25; $i <= 30; $i ++) {
                    $index = "D$i";
                    if (!is_numeric($borrower_information->$index)) {
                        $avg = "NA";
                        break;
                    }
                    $avg += $borrower_information->$index;
                }
                if ($avg != "NA" && is_numeric($borrower_information->D32)) {
                    $avg = $avg / 6;
                    $return = $avg / $borrower_information->D32;
                } else {
                    $return = "NA";
                }
            }
        }
        $option = "N/A";
        if (is_numeric($return)) {
            if ($return >= 2) {
                $option = ">=2";
            } elseif ($return >= 1.7 && $return < 2) {
                $option = ">=1.7 to <2";
            } elseif ($return >= 1.5 && $return < 1.7) {
                $option = ">=1.5 to <1.7";
            } elseif ($return >= 1.3 && $return < 1.5) {
                $option = ">=1.3 to <1.5";
            } elseif ($return >= 1 && $return < 1.3) {
                $option = ">=1 to <1.3";
            } elseif ($return < 1) {
                $option = "<1";
            }
        }
        return $option;
    }

    function non_zero($value) {
        $value = sanitize($value);
        $this->form_validation->set_message('non_zero', $this->lang->line('non_zero'));
        if ($value === 0) {
            return FALSE;
        }
        return TRUE;
    }

    function set_zero($row) {
        $row = sanitize($row);
        if (empty($row)) {
            $row = new stdClass();
            $row->C = 0;
            $row->D = 0;
            $row->E = 0;
            $row->F = 0;
            $row->G = 0;
        }
        return $row;
    }

    function get_numeric($value) {
        if (is_numeric($value)) {
            return $value;
        }
        return 0;
    }

    function not_approve_case() {
        $case_id = $this->input->post('case_id_');
        // if (non_subscriber_bank($case_id)) {
        // $this->session->set_flashdata('error', $this->lang->line('non_subscriber_bank_error'));
        // redirect(base_url('admin/Cases_two'));
        // }
        $comment = $this->input->post('comment');
        $this->db->update('cases', array(
            'approved' => - 1,
            'comment' => $comment
                ), array(
            'id' => $case_id
        ));
        $this->session->set_flashdata('success', $this->lang->line('marked_as_rejected'));
        redirect(base_url('admin/Cases_two'));
    }

    function get_rejection_comment() {
        $case_id = $this->input->post('case_id');
        $this->db->select('comment');
        $row = $this->db->get_where('cases', array(
                    'id' => $case_id
                ))->row();
        if ($row) {
            echo $row->comment;
        } else {
            echo 0;
        }
    }

    function copy_case($case_id) {
        // insert case
        $case = $this->db->get_where('cases', array(
                    'id' => $case_id
                ))->result_array();

        if (empty($case)) {
            $this->session->set_flashdata('error', $this->lang->line('case_not_found_copy'));
            redirect(base_url('admin/Cases_two'));
        }
        if ($case[0]['completed'] == 0) {
            $this->session->set_flashdata('error', $this->lang->line('wrong_step'));
            redirect(base_url('admin/Cases_two'));
        }

        $original_user = $case[0]['user_id'];

        unset($case[0]['approved']);
        unset($case[0]['date_approved']);
        unset($case[0]['approved_by']);
        unset($case[0]['date_completed']);
        unset($case[0]['completed']);
        unset($case[0]['pdf_name']);
        unset($case[0]['approve_success']);
        $case[0]['date_created'] = date('Y-m-d H:i:s');
        $case[0]['user_id'] = $this->session->userdata('user_id');
        $case[0]['id'] = incremented("cases");
        $case[0]['status'] = 'borrower_assessment';
        $case[0]['COPIED'] = $case_id;

        $this->db->insert('cases', $case[0]);
        $new_case_id = $case[0]['id'];

        $error_report = '';

        $this->db->select('banks.code,users.branch_code,users.first_name,users.last_name,banks.name as bank_name');
        $this->db->join('banks', 'users.bank_id=banks.id');
        $issued_person_row = $this->db->get_where('users', array(
                    'users.id' => $this->session->userdata('user_id')
                ))
                ->row();

        // insert borrower info.
        $borrower_info_row = $this->db->get_where('borrower_information', array(
                    'case_id' => $case_id
                ))->result_array();
        if (!empty($borrower_info_row)) {
            $borrower_info_row[0]['id'] = incremented("borrower_information");
            $borrower_info_row[0]['case_id'] = $new_case_id;
            $borrower_info_row[0]['C16'] = $issued_person_row->bank_name;
            $borrower_info_row[0]['C17'] = $issued_person_row->branch_code;
            $borrower_info_row[0]['C18'] = $issued_person_row->first_name . ' ' . $issued_person_row->last_name;
            $borrower_info_row[0]['C20'] = date('Y-m-d H:i:s');
            $borrower_info_row[0]['C36'] = '31-Dec-' . date('y', strtotime('-1 Year'));
            $this->db->insert('borrower_information', $borrower_info_row[0]);
        } else {
            $error_report .= $this->lang->line('not_found_borrower');
        }

        $this->load->model('admin/Cases_model');
        $case_name = $this->Cases_model->construct_excel_file_name($new_case_id);
        $this->db->update('cases', array(
            'name' => $case_name
                ), array(
            'id' => $new_case_id
        ));
        // insert fachsheet info.
        $borrower_factsheet_row = $this->db->get_where('borrower_factsheet', array(
                    'case_id' => $case_id
                ))->result_array();
        if (!empty($borrower_factsheet_row)) {
            foreach ($borrower_factsheet_row as $row) {
                $row['id'] = incremented("borrower_factsheet");
                $row['case_id'] = $new_case_id;
                $this->db->insert('borrower_factsheet', $row);
            }
        } else {
            $error_report .= '<br/>' . $this->lang->line('not_found_factsheet');
        }

        // insert financials info.
        $years = [];
        $dates = [date('Y-12-31', strtotime('-3 Years')), date('Y-12-31', strtotime('-2 Years')), date('Y-12-31', strtotime('-1 Years')), date('Y-12-31'), date('Y-12-31', strtotime('+1 Years'))];
        $alpha = ['C', 'D', 'E', 'F', 'G'];

        $bfy = $this->db->get_where('borrower_financials', array(
                    'case_id' => $case_id,
                    'row' => '12',
                ))->row();

        $dates_compare = $bfy ? [date('Y-m-d', strtotime($bfy->C)), date('Y-m-d', strtotime($bfy->D)), date('Y-m-d', strtotime($bfy->E)), date('Y-m-d', strtotime($bfy->F)), date('Y-m-d', strtotime($bfy->G))] : [];

        foreach ($dates as $k => $d):
            $key = $dates_compare ? array_search($d, $dates_compare) : NULL;
            $years[$k] = ['key' => $key ? $alpha[$key] : NULL, 'date' => $d];
        endforeach;

        $borrower_financials_row = $this->db->get_where('borrower_financials', array(
                    'case_id' => $case_id,
                ))->result_array();

        if (!empty($borrower_financials_row)) {
            foreach ($borrower_financials_row as $row) {
                if ($row['row'] == '12') {
                    for ($i = 0; $i < 5; $i++):
                        $row[$alpha[$i]] = $dates[$i];
                    endfor;
                } else {
                    if ($row['row'] !== '10' || $row['row'] !== '13') {
                        for ($i = 0; $i < 5; $i++):
                            $row[$alpha[$i]] = $years[$i]['key'] ? $row[$years[$i]['key']] : NULL;
                        endfor;
                    }
                }
                $row['id'] = incremented("borrower_financials");
                $row['case_id'] = $new_case_id;
                $this->db->insert('borrower_financials', $row);
            }
        } else {
            $error_report .= '<br/>' . $this->lang->line('not_found_financials');
        }

        // insert borrower assessment info.
        $borrower_assessment_row = $this->db->get_where('borrower_assessment', array(
                    'case_id' => $case_id
                ))->result_array();
        if (!empty($borrower_assessment_row)) {
            foreach ($borrower_assessment_row as $row) {
                $row['id'] = incremented("borrower_assessment");
                $row['case_id'] = $new_case_id;
                $this->db->insert('borrower_assessment', $row);
            }
        } else {
            $error_report .= '<br/>' . $this->lang->line('not_found_assessment');
        }

        // insert calculations data
        $calculations_data = $this->db->get_where('calculations', array(
                    'case_id' => $case_id
                ))->result_array();
        if (!empty($calculations_data)) {
            foreach ($calculations_data as $row) {
                $row['id'] = incremented("calculations");
                $row['case_id'] = $new_case_id;
                $this->db->insert('calculations', $row);
            }
        } else {
            // $error_report .= '<br/>' . $this->lang->line('not_found_calculations');
        }

        if ($error_report != '') {
            $this->session->set_flashdata('error', $this->lang->line('completed_with_errors') . ':<br/>' . $error_report);
            save_audit('case ' . $case_name . ' copied from case ' . $case[0]['name'] . ' with errors ' . $error_report);
        } else {
            save_audit('case ' . $case_name . ' copied from case ' . $case[0]['name']);
            $this->session->set_flashdata('success', $this->lang->line('success_copy') . $case_name);
        }
        redirect(base_url('admin/Cases_two'));
    }

    function get_case_projected($case_id) {
        $projected = $this->Cases_two_model->get_case_projected($case_id);
        $sorted = [];
        foreach ($projected as $one) {
            $sorted[$one->rown][$one->year] = $one->value;
        }
        return $sorted;
    }

    function get_case_hist($case_id) {
        $hist = $this->Cases_two_model->get_case_hist($case_id);
        $sorted = [];
        foreach ($hist as $one) {
            $sorted[$one->rown][$one->year] = $one->value;
        }
        return $sorted;
    }

    function insert_projected($case_id) {
        $last_projected = $this->input->post("last_projected");
        if (!$last_projected) {
            $this->db->delete('projected_fins', array(
                'case_id' => $case_id
            ));
            return FALSE;
        }
        $start = $this->Cases_two_model->get_borrower_info_year($case_id) + 3;
        $borrower_info_date = $this->Cases_two_model->get_borrower_info_date($case_id);
        $i = 0;
        $data = array();
        $row_data = array();
        $row_data['case_id'] = $case_id;
        $incremented = incremented("projected_fins");
        $hidden_years = explode(',', $this->input->post('hidden_years'));
        for ($j = $start; $j <= $last_projected; $j ++) {
            if (!isset($_POST[$j . '10'])) {
                continue;
            }
            if (in_array($j, $hidden_years)) {
                $row_data['hidden'] = 1;
            } else {
                $row_data['hidden'] = 0;
            }
            for ($i = 3; $i <= 167; $i ++) {
                if ($this->input->post('E3', TRUE) == 'no' && $i > 4) {
                    break;
                }

                $row_data['id'] = $incremented;
                $row_data['case_id'] = $case_id;
                $row_data['year'] = $j;
                $row_data['rown'] = $i;
                $row_data['value'] = $this->input->post($j . $i) || $this->input->post($j . $i) === "0" ? $this->input->post($j . $i, TRUE) : '';
                if ($i == 12) {
                    $row_data['value'] = date("d-m-$j", strtotime($borrower_info_date));
                }
                if ($row_data['value'] != '') {
                    array_push($data, $row_data);
                }
                $incremented ++;
            }
        }

        $this->db->delete('projected_fins', array(
            'case_id' => $case_id
        ));

        $this->db->insert_batch('projected_fins', $data);
        // $this->save_calc($this->input->post('case_id'), $this->input->post('calc'));
        return TRUE;
    }

    function insert_hist($case_id) {
        // print_r($this->input->post());die();
        $first_projected = $this->input->post("first_projected");

        if (!$first_projected) {
            $this->db->delete('hist_fins', array(
                'case_id' => $case_id
            ));
            return FALSE;
        }
        $end = $this->Cases_two_model->get_borrower_info_year($case_id) - 3;
        $borrower_info_date = $this->Cases_two_model->get_borrower_info_date($case_id);
        $i = 0;
        $data = array();
        $row_data = array();
        $row_data['case_id'] = $case_id;
        $incremented = incremented("hist_fins");
        $hidden_years = explode(',', $this->input->post('hidden_years'));
        for ($j = $first_projected; $j <= $end; $j ++) {
            if (!isset($_POST[$j . '10'])) {

                continue;
            }
            if (in_array($j, $hidden_years)) {
                $row_data['hidden'] = 1;
            } else {
                $row_data['hidden'] = 0;
            }
            for ($i = 3; $i <= 167; $i ++) {
                // if ($this->input->post('E3', TRUE) == 'no' && $i > 4) {
                // break;
                // }

                $row_data['id'] = $incremented;
                $row_data['case_id'] = $case_id;
                $row_data['year'] = $j;
                $row_data['rown'] = $i;
                $row_data['value'] = $this->input->post($j . $i) || $this->input->post($j . $i) === "0" ? $this->input->post($j . $i, TRUE) : '';
                if ($i == 12) {
                    $row_data['value'] = date("d-m-$j", strtotime($borrower_info_date));
                }
                if ($row_data['value'] != '') {
                    array_push($data, $row_data);
                }
                $incremented ++;
            }
        }

        $this->db->delete('hist_fins', array(
            'case_id' => $case_id
        ));

        $this->db->insert_batch('hist_fins', $data);
        // $this->save_calc($this->input->post('case_id'), $this->input->post('calc'));
        return TRUE;
    }

    function save_calc($case_id, $calc) {
        // $case_id = $this->input->post("case_id");
        // $calc = $this->input->post("calc");
        $calc = json_decode($calc, TRUE);

        $first_hist = $calc['first_hist'];
        $last_hist = $calc['last_hist'];
        $first_proj = $calc['first_proj'];
        $last_proj = $calc['last_proj'];
        $this->db->where("case_id", $case_id);
        $this->db->delete("calculations");
        $incremented = incremented("calculations");
        $hist_incremented = incremented("hist_fins");
        $proj_incremented = incremented("projected_fins");
        for ($i = 12; $i <= 224; $i ++) {
            if (isset($calc["C$i"]) || isset($calc["D$i"]) || isset($calc["E$i"]) || isset($calc["F$i"]) || isset($calc["G$i"])) {
                $one = array(
                    "id" => $incremented,
                    "case_id" => $case_id,
                    "rown" => $i,
                    "C" => isset($calc["C$i"]) ? $calc["C$i"] : NULL,
                    "D" => isset($calc["D$i"]) ? $calc["D$i"] : NULL,
                    "E" => isset($calc["E$i"]) ? $calc["E$i"] : NULL,
                    "F" => isset($calc["F$i"]) ? $calc["F$i"] : NULL,
                    "G" => isset($calc["G$i"]) ? $calc["G$i"] : NULL
                );
                $this->db->insert("calculations", $one);
                $incremented ++;
            }

            for ($j = $first_hist; $j < $last_hist; $j ++) {
                if (isset($calc[$j . $i])) {
                    $this->db->where("case_id", $case_id);
                    $this->db->where('year', $j);
                    $this->db->where("rown", $i);
                    $this->db->delete("hist_fins");
                    $insert = array(
                        'id' => $hist_incremented,
                        "case_id" => $case_id,
                        "rown" => $i,
                        "year" => $j,
                        "value" => !empty($calc[$j . $i]) ? $calc[$j . $i] : 0,
                        "hidden" => 0
                    );

                    $this->db->insert("hist_fins", $insert);
                    $hist_incremented ++;
                }
            }
            for ($k = $first_proj + 1; $k <= $last_proj; $k ++) {
                if (isset($calc[$k . $i])) {
                    $this->db->where("case_id", $case_id);
                    $this->db->where('year', $k);
                    $this->db->where("rown", $i);
                    $this->db->delete("projected_fins");
                    $insert_proj = array(
                        'id' => $proj_incremented,
                        "case_id" => $case_id,
                        "rown" => $i,
                        "year" => $k,
                        "value" => !empty($calc[$k . $i]) ? $calc[$k . $i] : 0,
                        "hidden" => 0
                    );
                    $this->db->insert("projected_fins", $insert_proj);
                    $proj_incremented ++;
                }
            }
        }
        echo "success";
    }

    function not_zero($value) {
        $this->form_validation->set_message("not_zero", $this->lang->line("name") . ' ' . $this->lang->line("cant_equal_zero"));
        if ($value === 0 || $value === "0") {
            return FALSE;
        }
        return TRUE;
    }

    function reassign_case() {
        $user_id = $this->input->post("reassign_user_id");
        $case_id = $this->input->post("reassign_case_id");
        $this->load->model("admin/Cases_model");
        $user = $this->Cases_model->get_user_info($user_id);
        // update case and borrower information
        $data["C16"] = $user->bank_name;
        $data["C17"] = $user->branch_code;
        $data["C18"] = $user->first_name . " " . $user->last_name;

        $case_row = $this->db->get_where('cases', array(
                    'id' => $case_id
                ))->row();

        $this->Crud_model->update("cases", $case_id, [
            "user_id" => $user_id
        ]);
        if ($case_row->completed == 0) {
            $this->Cases_model->update_bi($case_id, $data);

            if ($case_row->approved == 1) {
                unlink("./root/Cases/" . date('d-m-Y', strtotime($case_row->date_approved)) . "/" . $case_row->name . ".xlsx");
                unlink("./root/Cases/" . date('Y-m-d', strtotime($case_row->date_approved)) . "/" . $case_row->name . ".xlsx");
            }
            $case_status = $case_row->status;
            if ($case_status == 'saved') {
                $case_status = 'borrower_assessment';
            }
            // construct case name
            $case_name = $this->Cases_two_model->construct_reassign_name($case_id);
            $this->Crud_model->update("cases", $case_id, [
                "name" => $case_name,
                'approved' => 0,
                'approve_success' => 0,
                'status' => $case_status
            ]);
        }
        save_audit("Case reassigned to user $user->first_name $user->last_name -- " . $case_name);
        $this->session->set_flashdata("success", $this->lang->line("case_reassigned_sucessfully"));
        redirect(base_url("admin/Cases_two"));
    }

    function check_update_notification($table, $old_case_arr) {
        // checks if the verifier made a change to a case before submission and notifies its maker via email
        $case_id = $old_case_arr["case_id"];
        $case_data = $this->Crud_model->get_one("cases", $case_id);
        // check case status and that the user is verifier
        // if($case_data->status =="saved" || !compare_level("verifier")){
        if ($this->session->userdata("user_id") == $case_data->user_id || $case_data->approved) {
            return;
        }
        $updated_case = [];
        switch ($table) {
            case "borrower_factsheet":
                $updated_case = $this->Cases_two_model->get_factsheet_arr($case_id);
                break;
            case "borrower_financials":
                $updated_case = $this->Cases_two_model->get_financials_arr($case_id);
                break;
            case "borrower_assessment":
                $updated_case = $this->Cases_two_model->get_assessment_arr($case_id);
                break;
        }
        $changed = FALSE;
        foreach ($updated_case as $key => $value) {
            if ($old_case_arr[$key] != $value) {
                $changed = TRUE;
                break;
            }
        }
        if ($changed) {
            $this->send_update_notification($case_data->user_id, $case_data->name);
        }
    }

    function send_update_notification($user_id, $case_name) {
        $user = $this->Crud_model->get_one("users", $user_id);
        $this->load->model("admin/Cases_model");
        $email_settings = $this->Cases_model->get_email_settings();
        if (!empty($email_settings)) {
            // begin msg
            $msg_content = "";
            $name = $user->first_name . " " . $user->last_name;
            $msg_content .= "Hello $name! \r\n";
            $msg_content .= "Your case $case_name has been updated.\r\n";
            $msg_content .= "For more information, please login " . base_url() . "  \r\n";
            $msg_content .= "Thank you!";
            // end msg
            // config email smtp
            $settings = $this->db->get('email_settings')->row();
            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => $settings->smtp_host_b,
                'mailtype' => 'text',
                'charset' => 'iso-8859-1',
                'newline' => "\r\n"
            );
            $this->load->library('email');
            $this->email->initialize($config);
            $this->email->from($email_settings->host_mail_b);
            $this->email->to($user->email);
            $this->email->subject('Case Updated');
            $this->email->message($msg_content);
            $this->email->set_newline("\r\n");
            $sent = $this->email->send();
        }
    }

    function E224_Total_Net_Sales($value, $turnover) {
        if ($this->input->post('E3') == 'yes') {
            if (abs($turnover - $this->input->post('E224_Total_Net_Sales')) >= 1000) {
                $this->form_validation->set_message('E224_Total_Net_Sales', $this->lang->line('financial_borrower_error'));
                return FALSE;
            }
        }
        return TRUE;
    }

    function save_factsheet_invalid($case_id) {
        $i = 0;
        $data = array();
        $row_data = array();
        $row_data['case_id'] = $case_id;
        $incremented = incremented("borrower_factsheet");
        for ($i = 4; $i <= 44; $i ++) {
            if ($i == 6 || $i == 7 || $i == 8 || $i == 20 || $i == 21 || $i == 33 || $i == 34) {
                continue;
            }
            $row_data["id"] = $incremented;
            $row_data['row'] = $i;
            $row_data['C'] = $this->input->post('C' . $i) || $this->input->post('C' . $i) === "0" ? $this->input->post('C' . $i) : '';
            $row_data['D'] = $this->input->post('D' . $i) || $this->input->post('D' . $i) === "0" ? $this->input->post('D' . $i) : '';
            $row_data['E'] = $this->input->post('E' . $i) || $this->input->post('E' . $i) === "0" ? $this->input->post('E' . $i) : '';
            if ($i == 4) {
                $row_data['E'] *= $this->input->post('amounts');
            }
            if ($i == 5) {
                $row_data['C'] *= $this->input->post('amounts');
            }
            array_push($data, $row_data);
            $incremented ++;
        }

        $this->db->delete('borrower_factsheet', array(
            'case_id' => $case_id
        ));

        // modified
        if (!empty($data)) {
            $this->db->insert_batch('borrower_factsheet', $data);
        }

        $this->db->update('cases', array(
            'status' => 'borrower_factsheet'
                ), array(
            'id' => $case_id
        ));
    }

    function save_financials_invalid($case_id) {
        // save in db
        $i = 0;
        $data = array();
        $row_data = array();
        $row_data['case_id'] = $case_id;
        $incremented = incremented("borrower_financials");
        for ($i = 3; $i <= 167; $i ++) {
            if ($this->input->post('E3', TRUE) == 'no' && $i > 4) {
                break;
            }
            $row_data['id'] = $incremented;
            $row_data['row'] = $i;
            $row_data['C'] = $this->input->post('C' . $i) || $this->input->post('C' . $i) === "0" ? $this->input->post('C' . $i, TRUE) : '';
            $row_data['D'] = $this->input->post('D' . $i) || $this->input->post('D' . $i) === "0" ? $this->input->post('D' . $i, TRUE) : '';
            $row_data['E'] = $this->input->post('E' . $i) || $this->input->post('E' . $i) === "0" ? $this->input->post('E' . $i, TRUE) : '';
            if ($this->input->post('E4') == 'no') {
                // modified
                $row_data['F'] = '';
                $row_data['G'] = '';
                if ($i == 10) {
                    $row_data['F'] = 'Projected';
                    $row_data['G'] = 'Projected';
                }
                if ($i == 12) {
                    $borrower_info_date = $this->Cases_two_model->get_borrower_info_date($case_id);
                    $row_data['C'] = date('d-m-Y', strtotime("-2 year", strtotime($borrower_info_date)));
                    $row_data['D'] = date('d-m-Y', strtotime("-1 year", strtotime($borrower_info_date)));
                    $row_data['E'] = date('d-m-Y', strtotime($borrower_info_date));
                    $row_data['F'] = date('d-m-Y', strtotime("+1 year", strtotime($borrower_info_date)));
                    $row_data['G'] = date('d-m-Y', strtotime("+2 year", strtotime($borrower_info_date)));
                }
                if ($i == 13) {
                    $row_data['F'] = 12;
                    $row_data['G'] = 12;
                }
            } else {
                $row_data['F'] = $this->input->post('F' . $i) || $this->input->post('F' . $i) === "0" ? $this->input->post('F' . $i, TRUE) : '';
                $row_data['G'] = $this->input->post('G' . $i) || $this->input->post('G' . $i) === "0" ? $this->input->post('G' . $i, TRUE) : '';
            }
            if ($i == 8) {
                $row_data['F'] = '';
                $row_data['G'] = $this->input->post('G' . $i) || $this->input->post('G' . $i) === "0" ? $this->input->post('G' . $i, TRUE) : '';
            }
            // array_push($data, $row_data);
            if ($row_data['C'] != '' || $row_data['D'] != '' || $row_data['E'] != '' || $row_data['F'] != '' || $row_data['G'] != '') {
                array_push($data, $row_data);
            }
            $incremented ++;
        }

        $this->db->delete('borrower_financials', array(
            'case_id' => $case_id
        ));

        $if_exist = $this->db->affected_rows();

        $this->db->insert_batch('borrower_financials', $data);

        // insert his & projected years
        $this->db->delete('hist_fins', array(
            'case_id' => $case_id
        ));
        $this->db->delete('projected_fins', array(
            'case_id' => $case_id
        ));

        $base_year = $this->Cases_two_model->get_borrower_info_year($case_id);
        $last = $base_year + 2;
        $first = $base_year - 2;
        if ($this->input->post('E4') != 'no') {
            if ($last != $this->input->post("last_projected")) {
                $this->insert_projected($case_id);
            }
        }

        if ($first != $this->input->post("first_projected")) {
            $this->insert_hist($case_id);
        }

        $this->save_calc($this->input->post('case_id'), $this->input->post('calc'));

        $this->db->update('cases', array(
            'status' => 'borrower_financials'
                ), array(
            'id' => $case_id
        ));
    }

    function save_assessment_invalid($case_id) {
        // save in db
        $i = 0;
        $data = array();
        $row_data = array();
        $row_data['case_id'] = $case_id;
        $incremented = incremented("borrower_assessment");
        for ($i = 4; $i <= 229; $i ++) {
            if ($this->lang->line("borrower_assessment_F$i") != '') {
                if ($i == 4 || $i == 17 || $i == 120 || $i == 132 || $i == 184 || $i == 214) {
                    continue;
                }
                $row_data["id"] = $incremented;
                $row_data['row'] = $i;
                $row_data['F'] = $this->input->post('F' . $i) || $this->input->post('F' . $i) === "0" ? $this->input->post('F' . $i, TRUE) : '';

                array_push($data, $row_data);
                $incremented ++;
            }
        }

        $this->db->delete('borrower_assessment', array(
            'case_id' => $case_id
        ));

        $this->db->insert_batch('borrower_assessment', $data);

        $this->db->update('cases', array(
            'status' => 'borrower_assessment'
                ), array(
            'id' => $case_id
        ));
    }

    function add_copy_col() {
        $this->db->query('ALTER TABLE "cases" 
ADD copied NUMBER default 0');
    }

    function add_comment_col() {
        $this->load->dbforge();
        $fields = array(
            'case_comment' => array(
                'type' => 'NVARCHAR2',
                'constraint' => '2000',
                'null' => TRUE,)
        );
        $this->dbforge->add_column('cases', $fields);
    }

}
