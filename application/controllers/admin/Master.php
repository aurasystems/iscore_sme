<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Master extends CI_Controller {

    public function __construct() {
        parent::__construct();
        check_login();
        $this->load->model('admin/Master_model');
        if ($this->session->userdata('language') != "") {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        } else {
            $this->lang->load('default');
            $this->lang->load('default_1');
        }
        $this->session->set_userdata('active', 'settings');
    }

    //////////////////////////////////////////////////////
    public function c6() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $c6 = $this->Crud_model->get_all_active("c6");
        $data['title'] = $this->lang->line("borrower_information_C6");
        $data['active_input_settings'] = 1;
        $data['c6'] = $c6;
        $this->load->view("admin/pages/master/c6", $data);
    }

    public function add_c6() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "c6";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_information_C6");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'),
                "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);

            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_information_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_c6($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "c6";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_information_C6");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'),
                "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_information_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_c6($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "c6";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////////////////////////////////////////////////
    //////////BEGIN///////////////////////////
    public function c9() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $c9 = $this->Crud_model->get_all_active("c9");
        $data['title'] = $this->lang->line("borrower_information_C9");
        $data['c9'] = $c9;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/c9", $data);
    }

    public function add_c9() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "c9";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_information_C9");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'),
                "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_information_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_c9($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "c9";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_information_C9");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $this->load->view("admin/pages/master/edit_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'),
                "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_information_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_c9($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "c9";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function c10() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $c10 = $this->Crud_model->get_all_active("c10");
        $data['title'] = $this->lang->line("borrower_information_C10");
        $data['c10'] = $c10;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/c10", $data);
    }

    public function add_c10() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "c10";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_information_C10");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'),
                "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_information_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_c10($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "c10";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_information_C10");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'),
                "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_information_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_c10($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "c10";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function c11() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $c11 = $this->Crud_model->get_all_active("c11");
        $data['title'] = $this->lang->line("borrower_information_C10");
        $data['c11'] = $c11;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/c11", $data);
    }

    public function add_c11() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "c11";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_information_C11");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_information_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_c11($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "c11";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_information_C11");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $this->load->view("admin/pages/master/edit_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_information_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_c11($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "c11";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    // //////////BEGIN///////////////////////////
    public function e7() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $c11 = $this->Crud_model->get_all_active("e7");
        $data['title'] = $this->lang->line("borrower_information_E7");
        $data['e7'] = $c11;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/e7", $data);
    }

    public function add_e7() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "e7";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_information_E7");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_information_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_e7($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "e7";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_information_E7");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $this->load->view("admin/pages/master/edit_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_information_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_e7($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "e7";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }
    /// END /////////////////////////////////////
//    BEGIN BORROWER ASSESSMENT
    //////////BEGIN///////////////////////////
    public function f5() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f5";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F5");
        $data['all'] = $all;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/$table", $data);
    }

    public function add_f5() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f5";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F5");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f5($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f5";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F5");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f5($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f5";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f10() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f10";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F10");
        $data['all'] = $all;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/$table", $data);
    }

    public function add_f10() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f10";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F10");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f10($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f10";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F10");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f10($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f10";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f18() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f18";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F18");
        $data['all'] = $all;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/$table", $data);
    }

    public function add_f18() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f18";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F18");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f18($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f18";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F18");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f18($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f18";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f22() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f22";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F22");
        $data['all'] = $all;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/$table", $data);
    }

    public function add_f22() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f22";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F22");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f22($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f22";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F22");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f22($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f22";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f29() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f29";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F29");
        $data['all'] = $all;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/$table", $data);
    }

    public function add_f29() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f29";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F29");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f29($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f29";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F29");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f29($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f29";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f36() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f36";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F36");
        $data['all'] = $all;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/$table", $data);
    }

    public function add_f36() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f36";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F36");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f36($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f36";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F36");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f36($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f36";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f40() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f40";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F40");
        $data['all'] = $all;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/$table", $data);
    }

    public function add_f40() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f40";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F40");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f40($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f40";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F40");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f40($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f40";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f45() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f45";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F45");
        $data['all'] = $all;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/$table", $data);
    }

    public function add_f45() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f45";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F45");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f45($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f45";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F45");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f45($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f45";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f50() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f50";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F50");
        $data['all'] = $all;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/$table", $data);
    }

    public function add_f50() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f50";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F50");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f50($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f50";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F50");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f50($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f50";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f56() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f56";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F56");
        $data['all'] = $all;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/$table", $data);
    }

    public function add_f56() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f56";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F56");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f56($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f56";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F56");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f56($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f56";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f66() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f66";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F66");
        $data['all'] = $all;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/$table", $data);
    }

    public function add_f66() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f66";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F66");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f66($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f66";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F66");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f66($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f66";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f71() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f71";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F71");
        $data['all'] = $all;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/$table", $data);
    }

    public function add_f71() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f71";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F71");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f71($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f71";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F71");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f71($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f71";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f75() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f75";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F75");
        $data['all'] = $all;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/$table", $data);
    }

    public function add_f75() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f75";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F75");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f75($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f75";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F75");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f75($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f75";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f79() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f79";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F79");
        $data['all'] = $all;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/$table", $data);
    }

    public function add_f79() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f79";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F79");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f79($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f79";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F79");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f79($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f79";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f86() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f86";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F86");
        $data['all'] = $all;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/$table", $data);
    }

    public function add_f86() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f86";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F86");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f86($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f86";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F86");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f86($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f86";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f93() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f93";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F93");
        $data['all'] = $all;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/$table", $data);
    }

    public function add_f93() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f93";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F93");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f93($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f93";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F93");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f93($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f93";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f100() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f100";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F100");
        $data['all'] = $all;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/$table", $data);
    }

    public function add_f100() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f100";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F100");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f100($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f100";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F100");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f100($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f100";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f107() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f107";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F107");
        $data['all'] = $all;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/$table", $data);
    }

    public function add_f107() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f107";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F107");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f107($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f107";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F107");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_$table", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f107($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f107";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f115() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f115";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F115");
        $data['all'] = $all;
        $data['table'] = $table;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/assessment_settings", $data);
    }

    public function add_f115() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f115";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F115");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f115($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f115";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F115");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f115($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f115";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f121() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f121";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F121");
        $data['all'] = $all;
        $data['table'] = $table;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/assessment_settings", $data);
    }

    public function add_f121() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f121";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F121");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f121($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f121";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F121");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f121($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f121";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f125() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f125";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F125");
        $data['all'] = $all;
        $data['table'] = $table;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/assessment_settings", $data);
    }

    public function add_f125() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f125";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F125");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f125($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f125";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F125");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f125($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f125";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f129() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f129";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F129");
        $data['all'] = $all;
        $data['table'] = $table;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/assessment_settings", $data);
    }

    public function add_f129() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f129";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F129");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f129($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f129";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F129");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f129($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f129";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f133() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f133";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F133");
        $data['all'] = $all;
        $data['table'] = $table;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/assessment_settings", $data);
    }

    public function add_f133() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f133";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F133");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f133($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f133";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F133");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f133($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f133";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f137() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f137";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F137");
        $data['all'] = $all;
        $data['table'] = $table;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/assessment_settings", $data);
    }

    public function add_f137() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f137";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F137");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f137($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f137";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F137");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f137($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f137";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f144() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f144";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F144");
        $data['all'] = $all;
        $data['table'] = $table;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/assessment_settings", $data);
    }

    public function add_f144() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f144";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F144");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f144($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f144";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F144");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f144($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f144";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f151() {
        $table = "f151";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F151");
        $data['all'] = $all;
        $data['table'] = $table;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/assessment_settings", $data);
    }

    public function add_f151() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f151";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F151");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f151($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f151";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F151");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f151($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f151";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f155() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f155";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F155");
        $data['all'] = $all;
        $data['table'] = $table;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/assessment_settings", $data);
    }

    public function add_f155() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f155";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F155");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f155($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f155";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F155");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f155($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f155";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f159() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f159";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F159");
        $data['all'] = $all;
        $data['table'] = $table;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/assessment_settings", $data);
    }

    public function add_f159() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f159";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F159");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f159($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f159";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F159");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f159($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f159";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f165() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f165";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F165");
        $data['all'] = $all;
        $data['table'] = $table;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/assessment_settings", $data);
    }

    public function add_f165() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f165";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F165");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f165($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f165";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F165");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f165($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f165";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f169() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f169";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F169");
        $data['all'] = $all;
        $data['table'] = $table;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/assessment_settings", $data);
    }

    public function add_f169() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f169";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F169");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f169($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f169";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F169");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f169($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f169";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f175() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f175";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F175");
        $data['all'] = $all;
        $data['table'] = $table;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/assessment_settings", $data);
    }

    public function add_f175() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f175";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F175");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f175($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f175";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F175");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f175($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f175";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f179() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f179";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F179");
        $data['all'] = $all;
        $data['table'] = $table;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/assessment_settings", $data);
    }

    public function add_f179() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f179";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F179");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f179($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f179";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F179");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f179($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f179";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f186() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f186";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F186");
        $data['all'] = $all;
        $data['table'] = $table;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/assessment_settings", $data);
    }

    public function add_f186() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f186";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F186");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f186($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f186";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F186");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f186($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f186";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f194() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f194";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F194");
        $data['all'] = $all;
        $data['table'] = $table;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/assessment_settings", $data);
    }

    public function add_f194() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f194";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F194");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f194($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f194";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F194");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f194($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f194";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f200() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f200";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F200");
        $data['all'] = $all;
        $data['table'] = $table;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/assessment_settings", $data);
    }

    public function add_f200() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f200";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F200");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f200($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f200";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F200");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f200($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f200";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f208() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f208";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F208");
        $data['all'] = $all;
        $data['table'] = $table;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/assessment_settings", $data);
    }

    public function add_f208() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f208";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F208");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f208($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f208";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F208");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f208($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f208";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
    //////////BEGIN///////////////////////////
    public function f216() {
        if (!get_perm("settings", 0)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f216";
        $all = $this->Crud_model->get_all_active($table);
        $data['title'] = $this->lang->line("borrower_assessment_F216");
        $data['all'] = $all;
        $data['table'] = $table;
        $data['active_input_settings'] = 1;
        $this->load->view("admin/pages/master/assessment_settings", $data);
    }

    public function add_f216() {
        if (!get_perm("settings", 1)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f216";
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_unique[$table]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F216");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/add_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $data = increment($table, $data);
            $this->Crud_model->insert($table, $data);
            // audit
            save_audit(sprintf($this->lang->line('add_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    public function edit_f216($id) {
        if (!get_perm("settings", 2)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f216";
        $validate_code = $table . "_" . $id;
        $this->form_validation->set_rules("code", $this->lang->line("code"), "trim|required|callback_validate_edit_unique[$validate_code]");
        $this->form_validation->set_rules("value", $this->lang->line("value"), "trim|required");
        $this->form_validation->set_rules("value_arabic", $this->lang->line("value_arabic"), "trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("borrower_assessment_F216");
            $data['label_att'] = 'col-sm-3 control-label';
            $data['one'] = $this->Crud_model->get_one($table, $id);
            $data['active_input_settings'] = 1;
            $this->load->view("admin/pages/master/edit_assessment_settings", $data);
        } else {
            $data = array(
                "code" => $this->input->post('code'),
                "value" => $this->input->post('value'), "arabic_value" => $this->input->post('value_arabic')
            );
            $this->Crud_model->update($table, $id, $data);
            // audit
            save_audit(sprintf($this->lang->line('edit_input_settings'), $this->lang->line('borrower_assessment_' . strtoupper($table)), $this->input->post('code')));
            //
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Master/$table"));
        }
    }

    function delete_f216($id) {
        if (!get_perm("settings", 3)) {
            redirect(base_url("Welcome/permission_denied"));
        }
        $table = "f216";
        $this->Crud_model->update($table, $id, array("deleted" => 1));
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Master/$table"));
    }

    //////////END///////////////////////////
//    END BORROWER ASSESSMENT
    function validate_unique($val, $table) {
        $code = $this->input->post("code");
        $this->form_validation->set_message("validate_unique", $this->lang->line("code_field_unique"));
        $valid = $this->Master_model->validate_unique($table, $code);
        return $valid;
    }

    function validate_edit_unique($val, $data) {
        $exp = explode("_", $data);
        $table = $exp[0];
        $id = $exp[1];
        $code = $this->input->post("code");
        $this->form_validation->set_message("validate_edit_unique", $this->lang->line("code_field_unique"));
        $valid = $this->Master_model->validate_edit_unique($table, $id, $code);
        return $valid;
    }

}
