<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings extends CI_Controller {

    public function __construct() {
        parent::__construct();
        check_login();
        $this->load->model('admin/Settings_model');
        if ($this->session->userdata('language') != "") {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        } else {
            $this->lang->load('default');
            $this->lang->load('default_1');
        }
        $this->session->set_userdata('active', 'settings');
    }

    public function password_management() {
        if (substr($this->session->userdata('levels')->settings, 0, 1) == '0') {
            $this->session->set_flashdata('no_permission', 1);
            redirect('admin/Dashboard');
        }
        $settings = $this->Settings_model->get_password_management();
        $this->form_validation->set_rules("enforce_history", $this->lang->line("enforce_history"), "xss_clean|trim");
        $this->form_validation->set_rules("max_age", $this->lang->line("max_age"), "xss_clean|trim|numeric");
        $this->form_validation->set_rules("min_length", $this->lang->line("min_length"), "xss_clean|trim|numeric");
        $this->form_validation->set_rules("complex", $this->lang->line("complex"), "xss_clean|trim");
        $this->form_validation->set_rules("lockout_duration", $this->lang->line("lockout_duration"), "xss_clean|trim|numeric");
        $this->form_validation->set_rules("lockout_threshold", $this->lang->line("lockout_threshold"), "xss_clean|trim|numeric");
        $this->form_validation->set_rules("reset_counter", $this->lang->line("reset_counter"), "xss_clean|trim|numeric");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("password_management");
            $data['label_att'] = 'col-sm-6 control-label';
            $data['row'] = $settings;
            $this->load->view("admin/pages/settings/password_management", $data);
        } else {
            $data = array(
                'enforce_history' => $this->input->post("enforce_history"),
                'max_age' => $this->input->post("max_age"),
                'min_length' => $this->input->post("min_length"),
                'complex' => $this->input->post("complex"),
                'lockout_duration' => $this->input->post("lockout_duration"),
                'lockout_threshold' => $this->input->post("lockout_threshold"),
                'reset_counter' => $this->input->post("reset_counter")
            );
            if (!empty($settings)) {
                if (substr($this->session->userdata('levels')->settings, 2, 1) != '0') {
                    /**/ $this->Crud_model->update("password_management", $settings->id, $data);
                } else {
                    $this->session->set_flashdata('no_permission', 1);
                    redirect('admin/Dashboard');
                }
            } else {
                if (substr($this->session->userdata('levels')->settings, 1, 1) != '0') {
                    /**/ $this->Crud_model->insert("password_management", $data);
                } else {
                    $this->session->set_flashdata('no_permission', 1);
                    redirect('admin/Dashboard');
                }
            }
            // audit
            save_audit("update password management settings");
            /////////

            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Settings/password_management"));
        }
    }

    function audit_trail() {
        if (substr($this->session->userdata('levels')->settings, 0, 1) == '0') {
            $this->session->set_flashdata('no_permission', 1);
            redirect('admin/Dashboard');
        }
        $settings = $this->Settings_model->get_audit_trail_settings();
        $this->form_validation->set_rules("audits_expiry", $this->lang->line("audits_expiry"), "xss_clean|trim|required|numeric");
        $this->form_validation->set_rules("audits_file_path", $this->lang->line("audits_file_path"), "xss_clean|trim|required");

        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("audit_trail_settings");
            $data['label_att'] = 'col-sm-6 control-label';
            $data['settings'] = $settings;
            $this->load->view("admin/pages/settings/audit_trail", $data);
        } else {
            if (substr($this->session->userdata('levels')->settings, 2, 1) == '0') {
                $this->session->set_flashdata('no_permission', 1);
                redirect('admin/Dashboard');
            }
            if (!empty($settings)) {
                $data = array(
                    "audits_expiry" => $this->input->post("audits_expiry"),
                    "audits_file_path" => $this->input->post("audits_file_path"),
                );
                $this->Crud_model->update("back_settings", $settings->id, $data);
            } else {
                $this->Crud_model->insert("back_settings", $data);
            }
            // audit
            save_audit("update audit trail settings");
            /////////

            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("admin/Settings/audit_trail"));
        }
    }

    function input_settings() { 
        if (substr($this->session->userdata('levels')->settings, 0, 1) == '0') {
            redirect(base_url("Welcome/permission_denied"));
        }
        $data['title'] = $this->lang->line("input_settings");
        $data['active_input_settings'] = 1;
        $data['label_att'] = 'col-sm-6 control-label';
        $this->load->view("admin/pages/settings/input_settings", $data);
    }

}
