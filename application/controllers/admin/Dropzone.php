<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Dropzone extends CI_Controller
{
    public function __construct()
    {
       parent::__construct();
    }
 
    public function index()
    {
        $this->load->view('dropzone_view');
    }
    
    public function back_upload()
    {
        if(!empty($_FILES))
        {
            $temp_file = $_FILES['file']['tmp_name'];
            $file_name = $_FILES['file']['name'];
            $target_path = getcwd() . '/uploads/settings/back/';
            $target_file = $target_path . "logo.png" ;
            move_uploaded_file($temp_file, $target_file);
        }
    }
    public function front_upload()
    {
        if(!empty($_FILES))
        {
            $temp_file = $_FILES['file']['tmp_name'];
            $file_name = $_FILES['file']['name'];
            $target_path = getcwd() . '/uploads/settings/front/';
            $target_file = $target_path . "logo.png" ;
            move_uploaded_file($temp_file, $target_file);
        }
    }
}
 
/* End of file dropzone.js */
/* Location: ./application/controllers/dropzone.php */