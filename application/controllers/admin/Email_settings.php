<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Email_settings extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin/Email_settings_model');
        if ($this->session->userdata('language') != "") {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        } else {
            $this->lang->load('default');
            $this->lang->load('default_1');
        }
    }

    public function index($status = NULL) {
        $status=sanitize($status);
        if (($this->session->userdata('user_id') != "")) {
            if (substr($this->session->userdata('levels')->settings, 0, 1) == '0') {
                $this->session->set_flashdata('no_permission', 1);
                redirect('admin/Dashboard');
            }
            $data = array('title' => 'Email Settings');
            $data['status'] = $status;
            $data['attributes'] = array('class' => 'form-horizontal', 'role' => 'form');
            $data['label_att'] = array('class' => 'col-sm-4 control-label');
            $data['email_settings'] = $this->Email_settings_model->get_email_settings();
            $this->load->view("admin/pages/settings/email_settings", $data);
        } else {
            redirect('admin/Dashboard');
        }
    }

    public function update() {
        if (($this->session->userdata('user_id') != "")) {
            if (substr($this->session->userdata('levels')->settings, 2, 1) == '0') {
                $this->session->set_flashdata('no_permission', 1);
                redirect('admin/Dashboard');
            }
            $this->load->library('form_validation');
            $this->form_validation->set_rules('smtp_host', 'SMTP HOST', 'trim|required');
            $this->form_validation->set_rules('host_mail', 'HOST MAIL', 'trim|required');
            $this->form_validation->set_rules('smtp_port', 'SMTP PORT', 'trim|required');
            $this->form_validation->set_rules('smtp_user', 'SMTP USER', 'trim|required');
            $this->form_validation->set_rules('smtp_pass', 'SMTP PASS', 'trim|required');
            
            $this->form_validation->set_rules('smtp_host_b', 'SMTP HOST', 'trim|required');
            $this->form_validation->set_rules('host_mail_b', 'HOST MAIL', 'trim|required');
            $this->form_validation->set_rules('smtp_port_b', 'SMTP PORT', 'trim|required');
            $this->form_validation->set_rules('smtp_user_b', 'SMTP USER', 'trim|required');
            $this->form_validation->set_rules('smtp_pass_b', 'SMTP PASS', 'trim|required');
            $this->form_validation->set_rules('notification_email', 'Notification Email', 'trim');
            if ($this->form_validation->run() == FALSE) {
                $this->index();
            } else {
                $data = array(
                    'smtp_host' => $this->input->post('smtp_host'),
                    'host_mail' => $this->input->post('host_mail'),
                    'smtp_port' => $this->input->post('smtp_port'),
                    'smtp_user' => $this->input->post('smtp_user'),
                    'smtp_pass' => $this->input->post('smtp_pass'),
                    'notification_email' => $this->input->post('notification_email'),
                    
                    'smtp_host_b' => $this->input->post('smtp_host_b'),
                    'host_mail_b' => $this->input->post('host_mail_b'),
                    'smtp_port_b' => $this->input->post('smtp_port_b'),
                    'smtp_user_b' => $this->input->post('smtp_user_b'),
                    'smtp_pass_b' => $this->input->post('smtp_pass_b')
                );
                $this->Email_settings_model->update_settings($data);
            }
        } else {
            redirect('admin/Dashboard');
        }
    }

}
