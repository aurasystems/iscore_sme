<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Client extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Client_model');
        $this->load->model('admin/Email_settings_model');
        if($this->session->userdata('language') != "" )
        {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        }
        else
        {
            $this->lang->load('default');
            $this->lang->load('default_1');
        }
    }
    public function index()
    {
        redirect('admin/Dashboard');
    }
    function get_active_clients($status = NULL)
    {
        $status=sanitize($status);
        if(($this->session->userdata('user_id') != ""))
        {
            $data = array('title' => 'Active Clients');
            $data['status'] = $status;
            $data['attributes'] = array('class' => 'form-horizontal', 'role'=>'form');
            $data['label_att'] = array ('class' => 'col-sm-4 control-label');
            $data['active_clients'] = $this->Global_model->get_clients_by_status('clients',1);
            $this->load->view("admin/pages/client/active_clients", $data);
        }
        else
        {
            redirect('admin/Dashboard');
        }
    }
    function ban_client($client_id = NULL)
    {
        $client_id=sanitize($client_id);
        if($client_id != NULL)
        {
            if(($this->session->userdata('user_id') != ""))
            {

                $this->Global_model->active_waiting_ban_client('clients',2,$client_id);
                redirect('admin/Client/get_active_clients/success');
            }
            else
            {
                redirect('admin/dashboard');
            }
        }
        else
        {
            redirect('admin/dashboard');
        }
    }
    function get_banned_clients($status = NULL)
    {
        $status=sanitize($status);
        if(($this->session->userdata('user_id') != ""))
        {
            $data = array('title' => 'Banned Clients');
            $data['status'] = $status;
            $data['attributes'] = array('class' => 'form-horizontal', 'role'=>'form');
            $data['label_att'] = array ('class' => 'col-sm-4 control-label');
            $data['banned_clients'] = $this->Global_model->get_clients_by_status('clients',2);
            $this->load->view("admin/pages/client/banned_clients", $data);
        }
        else
        {
            redirect('admin/Dashboard');
        }
    }
    function active_banned_client($client_id = NULL)
    {
        $client_id=sanitize($client_id);
        if($client_id != NULL)
        {
            if(($this->session->userdata('user_id') != ""))
            {

                $this->Global_model->active_waiting_ban_client('clients',1,$client_id);
                redirect('admin/Client/get_banned_clients/success');
            }
            else
            {
                redirect('admin/dashboard');
            }
        }
        else
        {
            redirect('admin/dashboard');
        }
    }
    function get_waiting_clients($status = NULL)
    {
        $status=sanitize($status);
        if(($this->session->userdata('user_id') != ""))
        {
            $data = array('title' => 'Waiting Clients');
            $data['status'] = $status;
            $data['attributes'] = array('class' => 'form-horizontal', 'role'=>'form');
            $data['label_att'] = array ('class' => 'col-sm-4 control-label');
            $data['waiting_clients'] = $this->Global_model->get_clients_by_status('clients',0);
            $this->load->view("admin/pages/client/waiting_clients", $data);
        }
        else
        {
            redirect('admin/Dashboard');
        }
    }
    function active_waiting_client($client_id = NULL)
    {
        $client_id=sanitize($client_id);
        if($client_id != NULL)
        {
            if(($this->session->userdata('user_id') != ""))
            {
                $this->Global_model->active_waiting_ban_client('clients',1,$client_id);
                $this->send_active_msg($client_id);
                redirect('admin/Client/get_waiting_clients/success');
            }
            else
            {
                redirect('admin/dashboard');
            }
        }
        else
        {
            redirect('admin/dashboard');
        }
    }
    function send_active_msg($client_id)
    {
        $client_id=sanitize($client_id);
        $msg_content = '';
        //get Client info
        $client = $this->Global_model->get_data_by_id('clients',$client_id);
        $msg_content .= "Dear $client->first_name $client->last_name <br>";
        $msg_content .= 'Your Account Has Been Activated. You Can Login Now From <a href="'.base_url().'Login" title="Login"> Login </a>';
        // config email smtp
        $settings = $this->Email_settings_model->get_email_settings();
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => $settings->smtp_host, //ssl://smtp.googlemail.com
            'smtp_port' => $settings->smtp_port, // 465 
            'smtp_user' => $settings->smtp_user,
            'smtp_pass' => $settings->smtp_pass,
            'mailtype' => 'html',
            'charset' => 'iso-8859-1'
        );
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from($settings->host_mail, $settings->name);
        $this->email->to($client->email);
        $this->email->subject('Activation Message');
        $this->email->message($msg_content);
        $this->email->send();
    }
    function edit_client($client_id,$status = NULL)
    {
        $status=sanitize($status);
        $client_id=sanitize($client_id);
        if($client_id != '' && $client_id != NULL)
        {
            if(($this->session->userdata('user_id') != ""))
            {
                $data = array('title' => 'Edit Client');
                $data['client_id'] = $client_id;
                $data['status'] = $status;
                $data['attributes'] = array('class' => 'form-horizontal', 'role'=>'form');
                $data['label_att'] = array ('class' => 'col-sm-4 control-label');
                $data['client_data'] = $this->Global_model->get_data_by_id('clients',$client_id);
                $this->load->view('admin/pages/client/edit_client', $data);
            }
            else
            {
                redirect('admin/Dashboard');
            }
        }
        else
        {
            redirect('admin/Dashboard');
        }
    }
    function do_update_client($client_id)
    {
        $client_id=sanitize($client_id);
        if(($this->session->userdata('user_id') != ""))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('first_name', 'first name', 'trim|required|min_length[3]');
            $this->form_validation->set_rules('last_name', 'last name', 'trim|required|min_length[4]');
            $this->form_validation->set_rules('display_name', 'display name', 'trim|required|min_length[4]');
            $this->form_validation->set_rules('phone', 'phone', 'trim|required');
            $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
            $this->form_validation->set_rules('address', 'address', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|matches[password2]|min_length[4]|max_length[32]');
            $this->form_validation->set_rules('password2', 'Password Confirmation', 'trim|matches[password]');
            if($this->form_validation->run() == FALSE)
            {
                $this->edit_client($client_id);
            }
            else
            {
                if($this->input->post('password') != "" && $this->input->post('password2') != "")
                {
                    $data = array(
                        'first_name' => $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'display_name' => $this->input->post('display_name'),
                        'phone' => $this->input->post('phone'),
                        'email' => $this->input->post('email'),
                        'address' => $this->input->post('address'),
                        'password' => md5($this->input->post('password'))
                    );
                }
                else
                {
                    $data = array(
                        'first_name' => $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'display_name' => $this->input->post('display_name'),
                        'phone' => $this->input->post('phone'),
                        'email' => $this->input->post('email'),
                        'address' => $this->input->post('address')
                    );
                }
                $this->Global_model->update_client('clients',$client_id, $data);
                redirect('admin/Client/get_active_clients/success');
            }
        }
        else
        {
            redirect('admin/Dashboard');
        }
    }
}