<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Database extends CI_Controller {

    public function __construct() {
        parent::__construct();
		$this->load->dbforge();
    }
    public function index(){
        $data=array(
            "bank_only"=>0,
            "branch_only"=>0,
            "approved_only"=>0,
            "completed_only"=>0
        );
        $this->db->where("levelid",1);
        $this->db->update("accesslevelsfunctions",$data);
        echo "The Variables Have Been Updated Successfully!";
    }
	function get_users(){
		/* echo "in";
		$this->db->where("user_id",1);
		$this->db->order_by("id","desc");
		$users=$this->db->get("user_passwords")->result();
		print_r($users); */
	}
	function insert_user(){
		$this->db->order_by("id","desc");
		$id=$this->db->get("users")->row()->id;
		 $user=array(
		"id"=>$id+1,
		"email"=>"superadmin@vwave.com",
		"first_name"=>"Super",
		"last_name"=>"Admin",
		"display_name"=>"Super Admin",
		"password"=>"e10adc3949ba59abbe56e057f20f883e",
		"bank_id"=>1,
		"branch_code"=>"Admin",
		"access_level"=>1,
		"lockout_counter"=>0
		);
		$this->db->insert("users",$user);
		$this->db->order_by("id","desc");
		$pid=$this->db->get("user_passwords")->row()->id;
		$pass=array(
			"id"=>$pid+1,
			"user_id"=>$id,
			"timestamp"=>date("Y-m-d H:i:s"),
			"password"=>"e10adc3949ba59abbe56e057f20f883e",
			
		); 
		$this->db->insert("user_passwords",$pass);
		echo "User Inserted Successfully!"; 
	}
	function change_password(){
		//inArray ( [0] => stdClass Object ( [id] => 2 [user_id] => 1 [timestamp] => 2018-04-11 14:52:51 [password] => c7df6c40ad88e1158d94614ed3c17345 ) [1] => stdClass Object ( [id] => 1 [user_id] => 1 [timestamp] => 2018-04-04 15:40:29 [password] => e10adc3949ba59abbe56e057f20f883e ) ) 
		/* $this->db->where("id",2);
		$this->db->update("user_passwords",array("password"=>"e10adc3949ba59abbe56e057f20f883e")); */
	}
	function get(){
		$get= $this->db->get("accesslevels")->result();
		 /* $this->db->where("id",391);
		$get= $this->db->get("cases")->row(); */
		echo "<pre>";
		print_r($get);
		echo "</pre>";
		/*  $this->db->where("id",9);
		$get= $this->db->get("users")->row();
		print_r($get); */
		/*
		$this->db->where("id",56);
		$get= $this->db->get("cases")->row();
		print_r($get); */
		/* $this->db->select('cases.completed,cases.name,banks.name as bank_name,users.branch_code,cases.date_approved as date_approved,cases.id');
        //$this->db->where("cases.completed !=",1);
        $this->db->join('users', 'users.id=cases.user_id');
        $this->db->join('banks', 'users.bank_id=banks.id');
        $this->db->like('cases.name', "MCB14000001B0001C5318INabihElleithy", 'after'); 
        $get = $this->db->get('cases')->row();
		print_r($get); */
	}
	function update(){
		// [id] => 42 [user_id] => 9 [date_created] => 2018-04-23 15:32:30 [status] => saved [approved] => 0 [date_approved] => [approved_by] => [date_completed] => [completed] => 0 [name] => MCB14000001B0001C40445IMohamedHasaneenD23042018 [pdf_name] => ) 
		 //[id] => 56 [user_id] => 30 [date_created] => 2018-04-23 18:30:29 [status] => saved [approved] => 1 [date_approved] => 2018-04-24 14:36:00 [approved_by] => 8 [date_completed] => 2018-04-26 10:02:44 [completed] => 1 [name] => MCB14000001B0001C224449IMohamedMabroukD23042018 [pdf_name] => MCB14000001B0001C224449IMohamedMabroukD24042018
		 /* $case=array(
			"user_id"=>9,
			"date_created"=>"2018-04-23 15:32:30",
			"status"=>"saved",
			"approved"=>0,
			"date_approved"=>NULL,
			"date_completed" => NULL,
			"completed" => 0,
			//MCB14000001B0001C40445IMohamedHasaneenD23042018
			//MCB14000001B0001C224449IMohamedMabroukD24042018
			"name" => "MCB14000001B0001C224449IMohamedMabroukD24042018",
			"pdf_name" => ""
		 ); */
		 //MCB14000001B0001C44033IMohamedHasaneenD26042018
		 /* $case=array(
		 "name" => "MCB14000001B0001C224449IMohamedMabroukD24042018",
		 );
		 
		$this->db->where("id",85);
		$this->db->update("cases",$case); */
		 //$case=array(
		 //"approved" => "0",
		 //"completed"=>"0"
		// );
		 
		//$this->db->where("id",43);
		//$this->db->update("cases",$case);
		
		$this->db->where("id",227);
		$this->db->update("users",array("deleted"=>0));
	}
	function add_col(){
         if ($this->db->field_exists('deleted', 'users'))
            {
                  echo "This column already exists";
            }
            else{
                $this->load->dbforge();
            $fields = array(
            'deleted' => array(
                 'type' =>'NUMBER',
                'constraint' => '4',
                'default' => 0,)
    );
    $this->dbforge->add_column('users', $fields);
    echo "Column added successfully!";
            }
    }
	function get_all(){
        $tables = $this->db->list_tables();
		sort($tables);
        foreach ($tables as $table)
        {
            echo "/////////////$table///////////"."<br>";
                $fields = $this->db->list_fields($table);
				sort($fields);
                foreach ($fields as $field)
                {
                        echo $field."<br>";
                }
            echo "/////////////END///////////"."<br>";    
        }
    }
	
	 function alter_table($table,$fields) {
        $this->dbforge->add_column($table, $fields);
         echo "$table Altered Successfully!";
    }
	function create_table($table,$fields) {
        $this->dbforge->add_field('id');
//        $fields = array(
//        'blog_id' => array(
//                'type' => 'INT',
//                'constraint' => 5,
//                'unsigned' => TRUE,
//                'auto_increment' => TRUE
//        ),
//        'blog_title' => array(
//                'type' => 'VARCHAR',
//                'constraint' => '100',
//                'unique' => TRUE,
//        ),
//        'blog_author' => array(
//                'type' =>'VARCHAR',
//                'constraint' => '100',
//                'default' => 'King of Town',
//        ),
//        'blog_description' => array(
//                'type' => 'TEXT',
//                'null' => TRUE,
//        ),
//);
        $this->dbforge->add_field($fields);
        $this->dbforge->create_table($table);
        echo "$table Created Successfully!";
    }
	 function update_database(){
		 //28th of August, 2018
        $banks=array(
            'non_subscriber' => array(
                'type' => 'INT',
                'null'=>true
            )
        );
        $this->alter_table("banks",$banks);
        $borrower_information=array(
            'bank_amount' => array(
                    'type' =>'NVARCHAR2',
                    'constraint' => '90',
                     'null'=>true
            ),
                'turnover_amount' => array(
                    'type' =>'NVARCHAR2',
                    'constraint' => '45',
                     'null'=>true
            )
        );
        $this->alter_table("borrower_information",$borrower_information);
        $cases=array(
            'comment' => array(
                    'type' =>'NVARCHAR2',
                    'constraint' => '2000',
                     'null'=>true
            )
        );
        $this->alter_table("cases",$cases);
        $users=array(
                'active' => array(
                    'type' =>'INT',
                    'constraint' => '1',
                     'null'=>true
            )
        );
        $this->alter_table("users",$users);
        $hist_fins=array(
             'case_id' => array(
                    'type' =>'INT',
                     'null'=>true
            ),
             'year' => array(
                    'type' =>'VARCHAR2',
                    'constraint' => '15',
                     'null'=>true
            ),
             'rown' => array(
                    'type' =>'INT',
                     'null'=>true
            ),
             'value' => array(
                    'type' =>'VARCHAR2',
                    'constraint' => '45',
                     'null'=>true
            )
        );
        $this->create_table("hist_fins", $hist_fins);
         $projected_fins=array(
             'case_id' => array(
                    'type' =>'INT',
                     'null'=>true
            ),
             'year' => array(
                    'type' =>'VARCHAR2',
                    'constraint' => '15',
                     'null'=>true
            ),
             'rown' => array(
                    'type' =>'INT',
                     'null'=>true
            ),
             'value' => array(
                    'type' =>'VARCHAR2',
                    'constraint' => '45',
                     'null'=>true
            )
        );
        $this->create_table("projected_fins", $projected_fins);
        echo "Done!";
    }
	function get_al(){
		print_r($this->db->get("accesslevelsfunctions")->result());
	}
	     function c6(){
        $fields = array(
        'code' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null'=>true
        ),
        'value' => array(
                'type' => 'VARCHAR',
                'constraint' => '500',
                'null'=>true
        ),
        'deleted' => array(
                'type' =>'INT',
                'constraint' => '1',
                'default' => '0'
        )
);
        $this->create_table("c6", $fields);
        /////////////////////////////////////
    }
    function c9(){
        $fields = array(
        'code' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null'=>true
        ),
        'value' => array(
                'type' => 'VARCHAR',
                'constraint' => '500',
                'null'=>true
        ),
        'deleted' => array(
                'type' =>'INT',
                'constraint' => '1',
                'default' => '0'
        )
);
        $this->create_table("c9", $fields);
        /////////////////////////////////////
    }
    function c10(){
        $fields = array(
        'code' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null'=>true
        ),
        'value' => array(
                'type' => 'VARCHAR',
                'constraint' => '500',
                'null'=>true
        ),
        'deleted' => array(
                'type' =>'INT',
                'constraint' => '1',
                'default' => '0'
        )
);
        $this->create_table("c10", $fields);
        /////////////////////////////////////
    }
    function c11(){
        $fields = array(
        'code' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null'=>true
        ),
        'value' => array(
                'type' => 'VARCHAR',
                'constraint' => '500',
                'null'=>true
        ),
        'deleted' => array(
                'type' =>'INT',
                'constraint' => '1',
                'default' => '0'
        )
);
        $this->create_table("c11", $fields);
        /////////////////////////////////////
    }
//    BEGIN BORROWER ASSESSMENT
    function f5(){
        $fields = array(
        'code' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null'=>true
        ),
        'value' => array(
                'type' => 'VARCHAR',
                'constraint' => '500',
                'null'=>true
        ),
        'deleted' => array(
                'type' =>'INT',
                'constraint' => '1',
                'default' => '0'
        )
);
        $this->create_table("f5", $fields);
        /////////////////////////////////////
    }
        function f10(){
        $fields = array(
        'code' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null'=>true
        ),
        'value' => array(
                'type' => 'VARCHAR',
                'constraint' => '500',
                'null'=>true
        ),
        'deleted' => array(
                'type' =>'INT',
                'constraint' => '1',
                'default' => '0'
        )
);
        $this->create_table("f10", $fields);
        /////////////////////////////////////
    }
     function f18(){
        $fields = array(
        'code' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null'=>true
        ),
        'value' => array(
                'type' => 'VARCHAR',
                'constraint' => '500',
                'null'=>true
        ),
        'deleted' => array(
                'type' =>'INT',
                'constraint' => '1',
                'default' => '0'
        )
);
        $this->create_table("f18", $fields);
        /////////////////////////////////////
    }
    
     function f22(){
        $fields = array(
        'code' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null'=>true
        ),
        'value' => array(
                'type' => 'VARCHAR',
                'constraint' => '500',
                'null'=>true
        ),
        'deleted' => array(
                'type' =>'INT',
                'constraint' => '1',
                'default' => '0'
        )
);
        $this->create_table("f22", $fields);
        $this->create_table("f29", $fields);
        $this->create_table("f36", $fields);
        $this->create_table("f40", $fields);
        $this->create_table("f45", $fields);
        $this->create_table("f50", $fields);
        $this->create_table("f56", $fields);
        $this->create_table("f66", $fields);
        $this->create_table("f71", $fields);
        $this->create_table("f75", $fields);
        $this->create_table("f79", $fields);
        $this->create_table("f86", $fields);
        $this->create_table("f93", $fields);
        $this->create_table("f100", $fields);
        $this->create_table("f107", $fields);
        $this->create_table("f115", $fields);
        $this->create_table("f121", $fields);
        $this->create_table("f125", $fields);
        $this->create_table("f129", $fields);
        $this->create_table("f133", $fields);
        $this->create_table("f137", $fields);
        $this->create_table("f144", $fields);
        $this->create_table("f151", $fields);
        $this->create_table("f155", $fields);
        $this->create_table("f159", $fields);
        $this->create_table("f165", $fields);
        $this->create_table("f169", $fields);
        $this->create_table("f175", $fields);
        $this->create_table("f179", $fields);
        $this->create_table("f186", $fields);
        $this->create_table("f194", $fields);
        $this->create_table("f200", $fields);
        $this->create_table("f208", $fields);
        $this->create_table("f216", $fields);
        $this->create_table("f221", $fields);
        
        /////////////////////////////////////
    }
//    END BORROWER ASSESSMENT
    function all(){
        $this->c6();
        $this->c9();
        $this->c10();
        $this->c11();
		
		$this->f5();
        $this->f10();
        $this->f18();
        
        //////////
        $this->f22();
        /////////
    }
	//begin insert master
	public function read_file() {
        $file_name = 'cat.xlsx';
        ini_set('memory_limit', '2048M');
        set_time_limit(1800);
        $this->load->library('excel');
        $objPHPexcel = \PhpOffice\PhpSpreadsheet\IOFactory::load("./template/$file_name");
        $objWorksheet = $objPHPexcel->setActiveSheetIndex(0);
        $max_row = $objWorksheet->getHighestRow();
        $max_col = $objWorksheet->getHighestColumn();
        $sheet_data = $objWorksheet->rangeToArray(
                "A5:{$max_col}{$max_row}", // The worksheet range that we want to retrieve
                NULL, // Value that should be returned for empty cells
                TRUE, // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
                TRUE, // Should values be formatted (the equivalent of getFormattedValue() for each cell)
                TRUE         // Should the array be indexed by cell row and cell column
        );
//                print_r($sheet_data);
        $update['c6']=[];  
        $update['c9']=[];
        $update['c10']=[];
        $update['c11']=[];
        $assess=[];
        $assess_lables=array('F5','F10','F18','F22','F29','F36','F40','F45','F50','F56','F66','F71','F75','F79','F86','F93','F100','F107','F115','F121','F125','F129','F133','F137','F144','F151','F155','F159','F165','F169','F175','F179','F186','F194','F200','F208','F216');
        $i=0;
        foreach ($sheet_data as $row=>$data){
            if($row<=31){
                    $update['c6'][]=array("code"=>$data['B'],"value"=>$data['C']);
            }
            if($row<=429){
               $update['c9'][]=array("code"=>$data['E'],"value"=>$data['F']);
            }
            if($row<=8){
                $update['c10'][]=array("code"=>$data['H'],"value"=>$data['I']);
            }
            if($row>=13 && $row<=19){
                $update['c11'][]=array("code"=>$data['H'],"value"=>$data['I']);
            }
        }
        foreach ($sheet_data as $row=>$data){
            if($data['L']==1){
                $assess[$assess_lables[$i]]['start']=$row;
                if($i>0){
                    $assess[$assess_lables[$i-1]]['end']=$row-5;
                }
                $i++;
            }
        }
        $assess['F216']['end']=316;
        foreach ($assess as $key=>$arr){
            for($j=$arr['start'];$j<=$arr['end'];$j++){
                $update[$key][]=array("code"=>$sheet_data[$j]['L'],"value"=>$sheet_data[$j]['M']);
            }
        }
        $this->insert_master($update);
        echo "Master Fields Updated Successfully!";
//        echo "<pre>";
//        print_r($update);
//        echo "</pre>";
     }
     
     function insert_master($update){
         foreach ($update as $table=>$arr){
             foreach ($arr as $one){
                 $data=$one;
                 $data=increment(strtolower($table), $data);
                 $this->db->insert(strtolower($table),$data);
             }
         }
     }
	//end insert master
	function update_case(){
		$this->db->where("id",217);
		$this->db->update("cases",array("status"=>"saved","approved"=>1));
	}
	function update_password(){
		
	// Iscore@2019
//hala.naguib@aib.com.eg
//salah.abdallah@aib.com.eg
 //insert password in database
 
 $password="Iscore@2019";
 $user_id="30";
                    $data = array(
                        'user_id' => $user_id,
                        'password' => md5($password),
                        'timestamp' => date("Y-m-d H:i:s")
                    );
                    $data['id'] = incremented('user_passwords');
                     $this->Crud_model->insert("user_passwords", $data);
					 $update = array('force_reset' => 0);
                        $this->Global_model->global_update("users", $user_id, $update);
                    //end insert password in database
                    

	}
	function test_email()
    {
		$email="verto.wave@i-score.com.eg";
        $msg_content = '';
        //get user info
        $this->db->where('email', $email);
        $user = $this->db->get("users")->row();
        $last_pass= "hhhhjjjj";
        //set a token to verify him
        $token="11233345";
        $link=base_url()."admin/User/verify_reset_password/".$token;
        $msg_content .= "Dear $user->first_name, \r\n";
        $msg_content .= "Please, use the following link to reset your password: \r\n";
        $msg_content .= "<a href='$link'>$link</a> \r\n";
        $msg_content .= "If you didn't request a password reset, please, ignore this message.";
        // config email smtp
        $settings = $this->db->get('email_settings')->row();
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => $settings->smtp_host, //ssl://smtp.googlemail.com
            'smtp_port' => 25, // 465 
            //'smtp_user' => $settings->smtp_user,
            //'smtp_pass' => $settings->smtp_pass,
           // 'mailtype' => 'html',
            //'charset' => 'utf-8',
            //'newline' => "\r\n"
			'mailtype' => 'text',
                    'charset' => 'iso-8859-1',
                    'newline' => "\r\n"
        );
        $this->load->library('email');
        $this->email->initialize($config);
		$this->email->set_crlf( "\r\n" );
        $this->email->from($settings->host_mail);
        $this->email->to("rana.emad@aura-sys.com");
        $this->email->subject('Your Password Reset Link');
        $this->email->message($msg_content);
        $this->email->send();
    }
	  function send_update_notification(){
		  $user_id=20;
		  $case_name="test case";
        $user=$this->Crud_model->get_one("users",$user_id);
		$user->email="rana.emad@aura-sys.com";
		$this->load->model("admin/Cases_model");
        $email_settings = $this->Cases_model->get_email_settings();
        if (!empty($email_settings)) {
//            begin msg
            $msg_content="";
            $name= $user->first_name." ".$user->last_name;
            $msg_content .= "Hello $name! \r\n";
            $msg_content .= "Your case $case_name has been updated.\r\n";
            $msg_content .= "For more information, please login ".  base_url() ."  \r\n" ;
            $msg_content .= "Thank you!";
//            end msg
            // config email smtp
            $settings = $this->db->get('email_settings')->row();
            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => $settings->smtp_host,
                'mailtype' => 'text',
                'charset' => 'iso-8859-1',
                'newline' => "\r\n"
            );
            $this->load->library('email');
            $this->email->initialize($config);
            $this->email->from($email_settings->host_mail);
            $this->email->to($user->email);
            $this->email->subject('Case Updated');
            $this->email->message($msg_content);
            $this->email->set_newline("\r\n");
            $sent = $this->email->send();
            $err = $this->email->print_debugger();
            echo "<pre>";
            print_r($err);
            echo "</pre>";
           die;
        }
    }
	function get_table($table){
		echo "<pre>";
		print_r($this->db->get($table)->result());
		echo "</pre>";
	}
}