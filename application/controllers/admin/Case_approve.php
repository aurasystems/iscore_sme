<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Case_approve extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin/Cases_model');
        if ($this->session->userdata('language') != "") {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        } else {
            $this->lang->load('default');
            $this->lang->load('default_1');
        }
    }

    public function index() {
        $cases = $this->db->get_where('cases', array('completed' => 0, 'approved' => 1, 'approve_success' => 0))->result();

        foreach ($cases as $case) {
            $excel_file_name = $case->name;
            $date = date("Y_m_d", strtotime($case->date_approved));
            $file_exist = file_exists("./root/Cases/" . $date . "/$excel_file_name.xlsx");
            if ($file_exist) {
                clearstatcache();
                $generated_file_size = filesize("./root/Cases/" . $date . "/$excel_file_name.xlsx") / 1000;
            }
            if (($file_exist && (!$generated_file_size || !($generated_file_size > 160 && $generated_file_size < 180))) || !$file_exist) {

                if (file_exists("./root/Cases/" . $date . "/$excel_file_name.xlsx")) {
                    unlink("./root/Cases/" . $date . "/$excel_file_name.xlsx");
                }

                if ($this->generate_excel($case->id)) {
                    $this->db->update('cases', array('approve_success' => 1), array('id' => $case->id));

                    $this->send_email($case->id);
                } else {
                    save_audit('Auto Regenrate excel(cases): failed for case :' . $case->name);
                }
            } else {
                $this->db->update('cases', array('approve_success' => 1), array('id' => $case->id));
            }
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function approve_single_case() {

        $case = $this->db->get_where('cases', array('completed' => 0, 'approved' => 1, 'approve_success' => 0))->row();

        if ($case) {
            $excel_file_name = $case->name;
            $date = date("Y_m_d", strtotime($case->date_approved));

            $file_exist = file_exists("./root/Cases/" . $date . "/$excel_file_name.xlsx");
            if ($file_exist) {
                clearstatcache();
                $generated_file_size = filesize("./root/Cases/" . $date . "/$excel_file_name.xlsx") / 1000;
            }
            if (($file_exist && (!$generated_file_size || !($generated_file_size > 160 && $generated_file_size < 180))) || !$file_exist) {

                if (file_exists("./root/Cases/" . $date . "/$excel_file_name.xlsx")) {
                    unlink("./root/Cases/" . $date . "/$excel_file_name.xlsx");
                }

                if ($this->generate_excel($case->id)) {
                    $this->db->update('cases', array('approve_success' => 1), array('id' => $case->id));

                    $this->send_email($case->id);
                } else {
                    save_audit('Auto Regenrate excel: failed for case :' . $case->name);
                }
            } else {
                $this->db->update('cases', array('approve_success' => 1), array('id' => $case->id));
            }
        }
    }

    function send_email($case_id) {
        $email_settings = $this->Cases_model->get_email_settings();

        if (!empty($email_settings->notification_email)) {
            $case_info = $this->Cases_model->get_case_info($case_id);
            $msg_content = "";
            $msg_content .= "Hello! \r\n";
            $msg_content .= "A new case has been created.\r\n";
            $msg_content .= "Case Name: $case_info->name \r\n";
            $msg_content .= "Bank: $case_info->bank_name \r\n";
            $msg_content .= "Created By: $case_info->first_name $case_info->last_name \r\n";
            $msg_content .= "For more information, please login " . base_url() . "\r\n";
            $msg_content .= " Thank you!";
            // config email smtp
            $settings = $this->db->get('email_settings')->row();
            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => $settings->smtp_host, //ssl://smtp.googlemail.com
                //'smtp_port' => $settings->smtp_port, // 465
                //'smtp_user' => $settings->smtp_user,
                //'smtp_pass' => $settings->smtp_pass,
                'mailtype' => 'text',
                'charset' => 'iso-8859-1',
                'newline' => "\r\n"
            );
            $this->load->library('email');
            $this->email->initialize($config);
            $this->email->from($email_settings->host_mail);
            $this->email->to($email_settings->notification_email);
            $this->email->subject('New Case');
            $this->email->message($msg_content);
            $this->email->set_newline("\r\n");
            $this->email->send();
            //$err = $this->email->print_debugger();
            //$err = json_encode($err) . "\n";
            //$myfile = fopen("audit_trail/email.txt", "a");
            //fwrite($myfile, $err);
            //fclose($myfile);
        }
    }

    function generate_excel($case_id) {
        $this->lang->load('default');
        $this->lang->load('default_1');
        $case_id = sanitize($case_id);

        $case = $this->Cases_model->get_case_with_current_user_approve($case_id, 1);

        ini_set('memory_limit', '-1');
        set_time_limit(1800);

        $objPHPexcel = \PhpOffice\PhpSpreadsheet\IOFactory::load('./template/template.xlsx');
        $objPHPexcel->getDefaultStyle()->getProtection()->setLocked(false);

        $sheetname = array(
            'Read Me',
            'Definition Sheet',
            'Completion Check',
            'Borrower Information',
            "Borrower Factsheet",
            "Borrower Financials",
            "Borrower Assessment",
                //            "Master"
        );

        //       BEGIN BORROWER INFO
        $objWorksheet = $objPHPexcel->setActiveSheetIndex(3);


        $borrower_info = $this->Cases_model->get_borrower_information($case_id);
        foreach ($borrower_info as $key => $value) {
            if ($key == "id" || $key == "case_id" || $key == "turnover_amount" || $key == "bank_amount") {
                continue;
            }
            if ($key == "C7" || $key == "C8") {
                if (!empty($value) && $value != "") {
                    $objWorksheet->setCellValueExplicit($key, $value, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_NUMERIC);
                } else {
                    $objWorksheet->getCell($key)->setValue("");
                }
                continue;
            }
            if ($key == "C17") {
                $value = str_replace("'", "", $case->branch_code);
            }
            if ($key == "C19") {
                $value = $case->first_name . " " . $case->last_name;
            }
            if ($key == "C20") {
                $value = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel(date("j-M-y", strtotime($case->date_created)));
            }
            if ($key == "C21") {

                $value = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel(date("j-M-y", strtotime($case->date_approved)));
            }
            if ($key == "C36") {
                $value = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($value);
            }
            if ($key == "C13") {
                $value = $value / 100;
            }
            if ($key == 'C6' || $key == 'C9' || $key == 'C10' || $key == 'C11' || $key == 'E7') {
                $value = get_value_from_code($key, $value);
            }
            $objWorksheet->getCell($key)->setValue($value);
        }
        // END BORROWER INFO
        //       BEGIN BORROWER FACTSHEET
        $objWorksheet = $objPHPexcel->setActiveSheetIndex(4);
        $borrower_factsheet = $this->Cases_model->get_borrower_factsheet($case_id);
        foreach ($borrower_factsheet as $rows) {
            $row = 0;
            foreach ($rows as $key => $value) {
                if ($key == "id" || $key == "case_id" || ($row == 4 && $key == "D") || ($row == 5 && $key == "D") || ($row == 5 && $key == "E")) {
                    continue;
                }
                if ($key == "row") {
                    $row = $value;
                    continue;
                }
                $objWorksheet->getCell($key . $row)->setValue($value);
            }
        }
        // END BORROWER FACTSHEET
        //       BEGIN BORROWER FINANCIALS
        $objWorksheet = $objPHPexcel->setActiveSheetIndex(5);
        $borrower_financials = $this->Cases_model->get_borrower_financials($case_id);
        $projected = "yes";
        foreach ($borrower_financials as $rows) {
            $row = 0;
            foreach ($rows as $key => $value) {
                if ($key == "id" || $key == "case_id") {
                    continue;
                }
                if ($key == "row") {
                    $row = $value;
                    continue;
                }
                if ($row == 3 && $key == "E") {
                    if ($value == "no") {
                        $value = "No";
                    }
                    if ($value == "yes") {
                        $value = "Yes";
                    }
                }
                if ($row == 4 && $key == "E") {
                    //                    $projected=$value;
                    if ($value == "no") {
                        $value = "No";
                    }
                    if ($value == "yes") {
                        $value = "Yes";
                    }
                    //                    $value="Yes";
                }
                if ($row == 8 && $key == "F") {
                    continue;
                }
                if ($row == 11 && $key == "F") {
                    $objWorksheet->setCellValue($key . $row, '=IFERROR($G$9+1,"-")');
                    continue;
                }
                if ($row == 11 && $key == "G") {
                    $objWorksheet->setCellValue($key . $row, '=IFERROR($G$9+2,"-")');
                    continue;
                }
                if ($row == 12) {
                    if ($key == "E") {
                        $objWorksheet->setCellValue($key . $row, "=IF(ISBLANK('Borrower Information'!C36)," . '"-"' . ",'Borrower Information'!C36)");
                    }
                    if ($key == "C") {
                        $objWorksheet->setCellValue($key . $row, '=IF(E12="-","-",DATE(YEAR($E$12)-2,MONTH($E$12),DAY($E$12)))');
                    }
                    if ($key == "D") {
                        $objWorksheet->setCellValue($key . $row, '=IF(E12="-","-",DATE(YEAR($E$12)-1,MONTH($E$12),DAY($E$12)))');
                    }
                    if ($key == "F") {
                        $objWorksheet->setCellValue($key . $row, '=IF(E12="-","-",DATE(YEAR($E$12)+1,MONTH($E$12),DAY($E$12)))');
                    }
                    if ($key == "G") {
                        $objWorksheet->setCellValue($key . $row, '=IF(E12="-","-",DATE(YEAR($E$12)+2,MONTH($E$12),DAY($E$12)))');
                    }
                    continue;
                }
                if ($row > 15 && $projected == "no" && ($key == "F" || $key == "G")) {
                    continue;
                }
                $objWorksheet->getCell($key . $row)->setValue($value);
            }
        }
        //// END BORROWER FINANCIALS
        ////       BEGIN BORROWER ASSESSMENT
        $objWorksheet = $objPHPexcel->setActiveSheetIndex(6);
        $borrower_assessment = $this->Cases_model->get_borrower_assessment($case_id);
        foreach ($borrower_assessment as $rows) {
            $row = 0;
            foreach ($rows as $key => $value) {
                if ($key == "id" || $key == "case_id") {
                    continue;
                }
                if ($key == "row") {
                    $row = $value;
                    continue;
                }
                if ($row != 229 && $row != 221) {
                    $value = get_value_from_code($key . $row, $value);
                }

                $value = str_replace("&lt;", "<", $value);
                $objWorksheet->getCell($key . $row)->setValue($value);
            }
        }
        //// END BORROWER ASSESSMENT
        //
            //GENERATE EXCEL
        $excel_name = $case->name;

        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objPHPexcel, 'Xlsx');
        $approved_date = date("Y_m_d", strtotime($case->date_approved));
        if (!is_dir('root/Cases/' . $approved_date)) {
            mkdir('root/Cases/' . $approved_date, 0777, TRUE);
            //add index file
            copy('root/index.php', 'root/Cases/' . $approved_date . "/index.php");
        }


        if (!$excel_name) {
            save_audit('AUTO regenerate excel: cannot_create_file');
            return FALSE;
        } else {
            // audit
            $case_name = $case->name;
            save_audit("AUTO: regenerate excel file for case -- " . $case_name);
            /////////
            $objWriter->save("./root/Cases/" . $approved_date . "/$excel_name.xlsx");

            if (!file_exists("./root/Cases/" . $approved_date . "/$excel_name.xlsx")) {

                save_audit("AUTO: cannot recreate file for case -- " . $case_name);
                return FALSE;
            }


            $this->open_save_close($approved_date . "/$excel_name");

            $generated_file_size = filesize("./root/Cases/" . $approved_date . "/$excel_name.xlsx") / 1000;

            if (!$generated_file_size || !($generated_file_size > 160 && $generated_file_size < 180)) {
                //unlink("./root/Cases/" . $approved_date . "/$excel_name.xlsx");
                save_audit('Auto Regenerate excel: file_size_error for case' . $case_name);
                return FALSE;
            }
            save_audit("Auto Regenerate excel: successfully generated file size " . $generated_file_size . " for case -- " . $case_name);
            return TRUE;
        }
    }

    function open_save_close($file_path) {

        $this->replace_string_in_file(getcwd() . '\root\Cases\script.ps1', '$file_path$', getcwd() . '/root/Cases/' . $file_path);

        echo shell_exec('powershell.exe  -command Set-ExecutionPolicy Bypass -Scope CurrentUser');
        echo shell_exec('powershell.exe  -command ' . getcwd() . '\root\Cases\script.ps1');
        $this->replace_whole_file_content();
    }

    function replace_string_in_file($filename, $string_to_replace, $replace_with) {
        $content = file_get_contents($filename);
        $content_chunks = explode($string_to_replace, $content);
        $content = implode($replace_with, $content_chunks);
        file_put_contents($filename, $content);
    }

    function replace_whole_file_content() {
        $filename = getcwd() . '\root\Cases\script.ps1';

        $content = '$excel = New-Object -ComObject Excel.Application
$excel.DisplayAlerts = $false;
$filelocation = "$file_path$.xlsx"
$Workbook = $Excel.Workbooks.Open($filelocation,0, $false)
$Workbook.SaveAs($filelocation)
$Workbook.Saved = $true
$Workbook.Close()
$excel.Quit()';
        file_put_contents($filename, $content);
    }

}
