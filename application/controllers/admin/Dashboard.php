<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin/Dashboard_model');
        $this->load->model('admin/User_model');
        $this->load->model('admin/Back_settings_model');
        $this->load->model('front/Client_model');
        if ($this->session->userdata('language') != "") {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        } else {
            $this->lang->load('default');
            $this->lang->load('default_1');
        }
        $this->session->set_userdata('active', 'dashboard');
    }

    public function index() {
        if (($this->session->userdata('user_id') != "")) {
//            $this->welcome();
            redirect(base_url("admin/Cases_two"));
        } else {
            redirect(base_url("admin/Login"));
        }
    }

    public function welcome() {
        if (($this->session->userdata('user_id') != "")) {
            $data = array('title' => 'Dashboard');
            $data['active_clients'] = $this->Global_model->get_count_clients_by_status('clients', 1);
            $data['waiting_clients'] = $this->Global_model->get_count_clients_by_status('clients', 0);
            $data['banned_clients'] = $this->Global_model->get_count_clients_by_status('clients', 2);
            $data['users_count'] = $this->Global_model->get_table_count('users');
            $data['back_settings'] = $this->Back_settings_model->get_back_settings();
            $this->load->view('admin/pages/dashboard', $data);
        } else {
            $this->index();
        }
    }

    public function set_language($lang) {
        $this->session->set_userdata('language', $lang);
        redirect($_SERVER['HTTP_REFERER']);
    }

}
