<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin/User_model');
        if ($this->session->userdata('language') != "") {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        } else {
            $this->lang->load('default');
            $this->lang->load('default_1');
        }
        $this->session->set_userdata('active', 'users');
    }

    public function index() {
        redirect('admin/Dashboard');
    }

    public function reset_password() {
        if (($this->session->userdata('user_id') != "")) {
            redirect('admin/Dashboard');
        } else {
            $data = array('title' => 'Reset Password');
            $data['captcha'] = $this->captcha()['image'];
            $this->load->view("admin/pages/forget_password", $data);
        }
    }

    public function do_reset_password() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('captcha', 'Captcha', 'trim|required|callback_valid_captcha');
        if ($this->form_validation->run() == FALSE) {
            $this->reset_password();
        } else {
            $email = $this->input->post('email');
            $check_user = $this->Global_model->check_email('users', $email);
            if ($check_user) {
                //user exists
                $this->User_model->send_password_reset_link($email);
                save_audit("user with mail : " . $email . " requested reset password link");
                redirect(base_url("admin/User/password_reset_success/reset_link_set"));
            } else {
                // user not exists
                save_audit("unauthorized reset password operation with email : " . $email);
                $this->session->set_flashdata('msg', 'This email is not found');
                $this->reset_password();
            }
        }
    }

    function verify_reset_password($token) {
        $verified = $this->User_model->verify_reset_password($token);
        if ($verified) {
            $new_password = random_string('alnum', 10);
            $this->Global_model->reset_password('users', $verified, $new_password);
            redirect(base_url("admin/User/password_reset_success"));
        } else {
            redirect(base_url("admin/User/password_reset_success/could_not_verify_your_request"));
        }
    }

    function password_reset_success($msg = NULL) {
        if (($this->session->userdata('user_id') != "")) {
            redirect('admin/Dashboard');
        } else {
            $data['title'] = 'Password Recover Success';
            if ($msg) {
                $data['msg'] = $this->lang->line($msg);
            }
            $this->load->view("admin/pages/password_recover_success", $data);
        }
    }

    function get_system_users($status = NULL) {
        $status = sanitize($status);
        if (($this->session->userdata('user_id') != "")) {
            if (substr($this->session->userdata('levels')->users_super_admin, 0, 1) == '0' &&
                    substr($this->session->userdata('levels')->users_bank_managers, 0, 1) == '0' &&
                    substr($this->session->userdata('levels')->users_can_approve, 0, 1) == '0' &&
                    substr($this->session->userdata('levels')->users_normal, 0, 1) == '0' &&
                    substr($this->session->userdata('levels')->users_custom_levels, 0, 1) == '0') {
                $this->session->set_flashdata('no_permission', 1);
                redirect('admin/Dashboard');
            }
            $data = array('title' => 'System Users');
            $data['status'] = $status;
            $data['attributes'] = array('class' => 'form-horizontal', 'role' => 'form');
            $data['label_att'] = array('class' => 'col-sm-4 control-label');
            $this->db->where("id", $this->session->userdata('user_id'));
            $bank_id = $this->db->get("users")->row()->bank_id;
            $data['users_data'] = $this->User_model->get_users($bank_id);
            $this->load->view("admin/pages/user/system_users", $data);
        } else {
            redirect('admin/Dashboard');
        }
    }

    function add_user_view($status = NULL) {
        $status = sanitize($status);
        if (($this->session->userdata('user_id') != "")) {
            if (substr($this->session->userdata('levels')->users_super_admin, 1, 1) == '0' &&
                    substr($this->session->userdata('levels')->users_bank_managers, 1, 1) == '0' &&
                    substr($this->session->userdata('levels')->users_can_approve, 1, 1) == '0' &&
                    substr($this->session->userdata('levels')->users_normal, 1, 1) == '0' &&
                    substr($this->session->userdata('levels')->users_custom_levels, 1, 1) == '0') {
                $this->session->set_flashdata('no_permission', 1);
                redirect('admin/Dashboard');
            }
            $data = array('title' => 'Add New User');
            $data['status'] = $status;
            $data['attributes'] = array('class' => 'form-horizontal', 'role' => 'form');
            $data['label_att'] = array('class' => 'col-sm-4 control-label');
            $data['levels'] = $this->db->get('accesslevels')->result();
            $banks = [];
            if ($this->session->userdata("access_level") == 1) {
                $banks = $this->db->get_where('banks', array('deleted' => 0))->result();
            } else {
                $this->db->select("banks.*");
                $this->db->where("users.id", $this->session->userdata("user_id"));
                $this->db->join("banks", "users.bank_id = banks.id");
                $this->db->from("users");
                $banks[] = $this->db->get()->row();
            }
            $data['banks'] = $banks;

            $this->load->model("admin/Login_model");
            $password_management = $this->Login_model->get_password_management();
            $data['password_management'] = $password_management;

            $this->load->view("admin/pages/user/add_user", $data);
        } else {
            redirect('admin/Dashboard');
        }
    }

    function add_user() {
        $this->load->model("admin/Login_model");
        $password_management = $this->Login_model->get_password_management();
        //check minimum length
        $length = "";
        if (!empty($password_management) && $password_management->min_length) {
            $length = "min_length[$password_management->min_length]";
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name', $this->lang->line('lang_first_name'), 'trim|required|min_length[2]');
        $this->form_validation->set_rules('last_name', $this->lang->line('lang_last_name'), 'trim|required|min_length[2]');
        $this->form_validation->set_rules('display_name', $this->lang->line('lang_display_name'), 'trim|required|min_length[4]');
        $this->form_validation->set_rules('email', $this->lang->line('lang_email_address'), 'trim|required|valid_email|callback_unique_email[0]');
        $this->form_validation->set_rules('password', $this->lang->line('lang_password'), "trim|matches[password2]|$length");
        $this->form_validation->set_rules('password2', $this->lang->line('pass_confirmation'), 'trim|matches[password]');
        $this->form_validation->set_rules('access_level', $this->lang->line('access_level'), 'trim|required');
        $this->form_validation->set_rules('bank', $this->lang->line('bank'), 'trim|required');
        $this->form_validation->set_rules('branch_code[]', $this->lang->line('branch_code'), 'xss_clean|trim|required|callback_valid_dir_name');
        if ($this->form_validation->run() == FALSE) {
            $this->add_user_view();
        } else {
            $data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'display_name' => $this->input->post('display_name'),
                'email' => $this->input->post('email'),
                'password' => do_hash($this->input->post('password'), 'md5'),
                'access_level' => $this->input->post('access_level'),
                'bank_id' => $this->input->post('bank'),
                'branch_code' => implode(',', $this->input->post('branch_code[]')),
                'force_reset' => $this->input->post("force_reset"),
                'active' => 1,
                'date_created' => date("Y-m-d H:i:s")
            );
            if ($_FILES['userfile']['name'] != '') {
                $config['upload_path'] = './uploads/users';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '10000';
                $config['max_width'] = '5000';
                $config['max_height'] = '5000';
                $this->lang->load('upload');
                $this->load->library('upload', $config);
                if ($this->upload->do_upload()) {
                    $upload_data = $this->upload->data();
                    $data['image'] = $upload_data['file_name'];
                } else {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    $this->add_user_view();
                    return;
                }
            }
            if (count($this->input->post('branch_code[]') == 1)) {  // because user with more than one branch can't add a case
                $bank = $this->db->get_where('banks', array('id' => $this->input->post('bank')))->row();
                if ($bank) { //&& $bank->non_subscriber == 0
                    $unreplaced_name = $bank->name;
                    $bank->name = replace_bank_name($bank->name);
                    $branch_code_n = $this->input->post('branch_code', true)[0];
                    if (!is_dir('root/' . $bank->name . '/' . $branch_code_n)) {
                        mkdir('root/' . $bank->name . '/' . $branch_code_n, 0777, TRUE);
                        //add index file
                        copy('root/index.php', 'root/' . $bank->name . '/' . $branch_code_n . "/index.php");
                        mkdir('root/' . $bank->name . '/' . $branch_code_n . '/Gradation Results', 0777, TRUE);
                        //add index file
                        copy('root/index.php', 'root/' . $bank->name . '/' . $branch_code_n . '/Gradation Results' . "/index.php");
                    }
                    $bank->name = $unreplaced_name;
                }
            }
            $data['id'] = incremented("users");
            /**/ $this->Global_model->global_insert('users', $data);
            $user_id = $data['id'];
            // audit
            save_audit("insert user with email : " . $this->input->post('email'));
            /////////
            //insert password in database

            $data = array(
                'user_id' => $user_id,
                'password' => md5($this->input->post("password")),
                'timestamp' => date("Y-m-d H:i:s")
            );
            $data['id'] = incremented("user_passwords");
            /**/ $this->Crud_model->insert("user_passwords", $data);
            //end insert password in database
            $this->send_notification_email();

            redirect('admin/User/add_user_view/success');
        }
    }

    function valid_dir_name($dir_name) {
        if(!is_array($this->input->post('branch_code'))) {
            $this->form_validation->set_message('valid_dir_name', "Enter , after branch name");
            return FALSE;
        }
        for ($i = 0; $i < count($this->input->post('branch_code')); $i++) {
            if (!preg_match("/^[0-9a-zA-Z]+$/", $this->input->post('branch_code')[$i])) {
                $this->form_validation->set_message('valid_dir_name', "The {field} must contains only Alphanumeric");
                return FALSE;
            }
            if (strlen($this->input->post('branch_code')[$i]) != 4) {
                $this->form_validation->set_message('valid_dir_name', "The {field} must be four numbers");
                return FALSE;
            }
            $dir_name = sanitize($this->input->post('branch_code')[$i]);
            if (strpos($dir_name, '/') !== false || strpos($dir_name, '\\') !== false ||
                    strpos($dir_name, ':') !== false || strpos($dir_name, '*') !== false ||
                    strpos($dir_name, '?') !== false || strpos($dir_name, '"') !== false ||
                    strpos($dir_name, '<') !== false || strpos($dir_name, '>') !== false ||
                    strpos($dir_name, '|') !== false || strpos($dir_name, '&') !== false ||
                    strpos($dir_name, ',') !== false || strpos($dir_name, '.') !== false) {
                $this->form_validation->set_message('valid_dir_name', "The {field} can't contain any of the following charachters /\:*?\"<>|&, ");
                return FALSE;
            }
        }

        return TRUE;
    }

    function edit_user_view($user_id, $status = NULL) {
        $status = sanitize($status);
        $user_id = sanitize($user_id);
        if ($user_id != '' && $user_id != NULL) {
            if (($this->session->userdata('user_id') != "")) {
                if ($this->session->userdata('user_id') != $user_id) {
                    if (substr($this->session->userdata('levels')->users_super_admin, 1, 1) == '0' &&
                            substr($this->session->userdata('levels')->users_bank_managers, 1, 1) == '0' &&
                            substr($this->session->userdata('levels')->users_can_approve, 1, 1) == '0' &&
                            substr($this->session->userdata('levels')->users_normal, 1, 1) == '0' &&
                            substr($this->session->userdata('levels')->users_custom_levels, 1, 1) == '0') {
                        $this->session->set_flashdata('no_permission', 1);
                        redirect('admin/Dashboard');
                    }
                }
                $data = array('title' => 'Edit System User');
                $data['system_user_id'] = $user_id;
                $data['status'] = $status;
                $data['attributes'] = array('class' => 'form-horizontal', 'role' => 'form');
                $data['label_att'] = array('class' => 'col-sm-4 control-label');
                $data['user_data'] = $this->Global_model->get_data_by_id('users', $user_id);
                $data['levels'] = $this->db->get('accesslevels')->result();
                $banks = [];
                if ($this->session->userdata("access_level") == 1) {
                    $banks = $this->db->get_where('banks', array('deleted' => 0))->result();
                } else {
                    $this->db->select("banks.*");
                    $this->db->where("users.id", $this->session->userdata("user_id"));
                    $this->db->join("banks", "users.bank_id = banks.id");
                    $this->db->from("users");
                    $banks[] = $this->db->get()->row();
                }
                $data['banks'] = $banks;
                $this->load->model("admin/Login_model");
                $password_management = $this->Login_model->get_password_management();
                $data['password_management'] = $password_management;
                $this->load->view('admin/pages/user/edit_user', $data);
            } else {
                redirect('admin/Dashboard');
            }
        } else {
            redirect('admin/Dashboard');
        }
    }

    function update_user($user_id) {

        $user_id = sanitize($user_id);
        if ($user_id != '' && $user_id != NULL) {
            if (($this->session->userdata('user_id') != "")) {

                $this->load->model("admin/Login_model");
                $password_management = $this->Login_model->get_password_management();
                //check minimum length
                $length = "";
                if (!empty($password_management) && $password_management->min_length) {
                    $length = "min_length[$password_management->min_length]";
                }

                $user = $this->db->get_where('users', array('id' => $user_id))->row();
                $this->load->library('form_validation');
                $this->form_validation->set_rules('first_name', $this->lang->line('lang_first_name'), 'xss_clean|trim|required|min_length[2]');
                $this->form_validation->set_rules('last_name', $this->lang->line('lang_last_name'), 'xss_clean|trim|required|min_length[2]');
                $this->form_validation->set_rules('display_name', $this->lang->line('lang_display_name'), 'xss_clean|trim|required|min_length[4]');
                $unique = '';
                if ($user && $this->input->post('email') != $user->email) {
                    $unique = "|callback_unique_email[$user->id]";
                }
                if (compare_level("super_admin")) {
                    $this->form_validation->set_rules('email', $this->lang->line('lang_email_address'), "xss_clean|trim|required|valid_email$unique");
                }
                if ($this->session->userdata("access_level") == 1 || $this->session->userdata("user_id") != $user_id) {
                    $this->form_validation->set_rules('access_level', $this->lang->line('access_level'), 'xss_clean|trim|required');
                    $this->form_validation->set_rules('bank', $this->lang->line('bank'), 'xss_clean|trim|required');
                    $this->form_validation->set_rules('branch_code[]', $this->lang->line('branch_code'), 'xss_clean|trim|required|callback_valid_dir_name');
                }
                $this->form_validation->set_rules('userfile', $this->lang->line('image'), 'xss_clean');
                if ($this->form_validation->run() == FALSE) {
                    $this->edit_user_view($user_id);
                } else {
                    if ($this->input->post('password') != "" && $this->input->post('password2') != "") {
                        $data = array(
                            'first_name' => $this->input->post('first_name'),
                            'last_name' => $this->input->post('last_name'),
                            'display_name' => $this->input->post('display_name'),
//                            'email' => $this->input->post('email'),
                            'password' => do_hash($this->input->post('password'), 'md5'),
//                            'access_level' => $this->input->post('access_level'),
//                            'bank_id' => $this->input->post('bank'),
//                            'branch_code' => $this->input->post('branch_code')
                        );
                        if (compare_level("super_admin")) {
                            $data['email'] = $this->input->post('email');
                        }
                    } else {
                        $data = array(
                            'first_name' => $this->input->post('first_name'),
                            'last_name' => $this->input->post('last_name'),
                            'display_name' => $this->input->post('display_name'),
//                            'email' => $this->input->post('email'),
//                            'access_level' => $this->input->post('access_level'),
//                            'bank_id' => $this->input->post('bank'),
//                            'branch_code' => $this->input->post('branch_code')
                        );
                        if (compare_level("super_admin")) {
                            $data['email'] = $this->input->post('email');
                        }
                    }

                    if ($_FILES['userfile']['name'] != '') {
                        $config['upload_path'] = './uploads/users';
                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config['max_size'] = '10000';
                        $config['max_width'] = '5000';
                        $config['max_height'] = '5000';
                        $this->lang->load('upload');
                        $this->load->library('upload', $config);
                        if ($this->upload->do_upload()) {
                            $upload_data = $this->upload->data();
                            $upload_data['file_name'] = $this->security->sanitize_filename($upload_data['file_name']);
                            $data['image'] = $upload_data['file_name'];
                        } else {
                            $this->session->set_flashdata('error', $this->upload->display_errors());
                            $this->edit_user_view($user_id);
                            return;
                        }
                    }
                    $update_user_cases = 0;
                    if ($this->session->userdata("access_level") == 1 || $this->session->userdata("user_id") != $user_id) {
                        $data['access_level'] = $this->input->post('access_level');
                        $data['bank_id'] = $this->input->post('bank');
                        $data['branch_code'] = implode(',', $this->input->post('branch_code[]'));
                        // update all this user cases
                        $prev_user_row = $this->db->get_where('users', array('id' => $user_id))->row();
                        if (!$prev_user_row) {
                            $this->session->set_flashdata('error', $this->upload->display_errors());
                            $this->edit_user_view($user_id);
                            return;
                        }
                        if ($prev_user_row->bank_id != $this->input->post('bank') || $prev_user_row->branch_code != $this->input->post('branch_code')) {
                            $update_user_cases = 1;
                        }
                    }
                    if (count($this->input->post('branch_code[]')) == 1) {
                        $bank = $this->db->get_where('banks', array('id' => $this->input->post('bank')))->row();
                        if ($bank) { // && $bank->non_subscriber == 0
                            $unreplaced_name = $bank->name;
                            $bank->name = replace_bank_name($bank->name);
                            $branch_code_n = $this->input->post('branch_code', true)[0];
                            if (!is_dir('root/' . $bank->name . '/' . $branch_code_n)) {
                                mkdir('root/' . $bank->name . '/' . $branch_code_n, 0777, TRUE);
                                //add index file
                                copy('root/index.php', 'root/' . $bank->name . '/' . $branch_code_n . "/index.php");
                                mkdir('root/' . $bank->name . '/' . $branch_code_n . '/Gradation Results', 0777, TRUE);
                                //add index file
                                copy('root/index.php', 'root/' . $bank->name . '/' . $branch_code_n . '/Gradation Results' . "/index.php");
                            }
                            $bank->name = $unreplaced_name;
                        }
                    }

                    /**/ $this->User_model->update_user($user_id, $data);
                    if (count($this->input->post('branch_code[]')) == 1) {
                        if ($update_user_cases) {
                            $this->db->select('id');
                            $cases = $this->db->get_where('cases', array('user_id' => $user_id, 'completed' => 0))->result();
                            $this->load->model('admin/Cases_two_model');
                            foreach ($cases as $case) {
                                $this->Cases_two_model->regenrate_ref_no($case->id);
                            }
                        }
                    }
                    // audit
                    save_audit("update user with email " . $this->input->post('email'));
                    /////////


                    redirect('admin/User/edit_user_view/' . $user_id . '/success');
                }
            } else {
                redirect('admin/Dashboard');
            }
        } else {
            redirect('admin/Dashboard');
        }
    }

    function reset_pass($user_id) {
        $user_id = sanitize($user_id);
        if ($user_id != '' && $user_id != NULL) {
            if (($this->session->userdata('user_id') != "")) {
                if ($this->session->userdata('user_id') != $user_id) {
                    if (substr($this->session->userdata('levels')->users_super_admin, 1, 1) == '0' &&
                            substr($this->session->userdata('levels')->users_bank_managers, 1, 1) == '0' &&
                            substr($this->session->userdata('levels')->users_can_approve, 1, 1) == '0' &&
                            substr($this->session->userdata('levels')->users_normal, 1, 1) == '0' &&
                            substr($this->session->userdata('levels')->users_custom_levels, 1, 1) == '0') {
                        $this->session->set_flashdata('no_permission', 1);
                        redirect('admin/Dashboard');
                    }
                }
                $user_row = $this->db->get_where('users', array('id' => $user_id))->row();
                if (!$user_row) {
                    $data = array('title' => 'Reset Password');
                    $data['system_user_id'] = $user_id;
                    $data['attributes'] = array('class' => 'form-horizontal', 'role' => 'form');
                    $data['label_att'] = array('class' => 'col-sm-4 control-label');
                    $data['user_data'] = $this->Global_model->get_data_by_id('users', $user_id);
                    $this->load->model("admin/Login_model");
                    $password_management = $this->Login_model->get_password_management();
                    $data['password_management'] = $password_management;
                    $data['error'] = 'User not found';
                    $this->load->view('admin/pages/user/reset_password', $data);
                    return;
                }
                $this->load->model("admin/Login_model");
                $password_management = $this->Login_model->get_password_management();
                //check minimum length
                $length = "";
                if (!empty($password_management) && $password_management->min_length) {
                    $length = "min_length[$password_management->min_length]";
                }
                if ($this->session->userdata('access_level') != 1 && !($this->session->userdata('access_level') == 4 && $user_row->bank_id == $this->session->userdata('bank_id'))) {
                    $this->form_validation->set_rules('old_password', $this->lang->line('old_password'), 'xss_clean|trim|required|callback_valid_old_password');
                }
                $this->form_validation->set_rules('password', $this->lang->line('lang_password'), "xss_clean|trim|required|matches[password2]|callback_password_validation|$length");
                $this->form_validation->set_rules('password2', $this->lang->line('pass_confirmation'), 'xss_clean|trim|required|matches[password]');
                if (!$this->form_validation->run()) {
                    $data = array('title' => 'Reset Password');
                    $data['system_user_id'] = $user_id;
                    $data['attributes'] = array('class' => 'form-horizontal', 'role' => 'form');
                    $data['label_att'] = array('class' => 'col-sm-4 control-label');
                    $data['user_data'] = $this->Global_model->get_data_by_id('users', $user_id);
                    $this->load->model("admin/Login_model");
                    $password_management = $this->Login_model->get_password_management();
                    $data['password_management'] = $password_management;
                    $this->load->view('admin/pages/user/reset_password', $data);
                } else {

                    //insert password in database
                    $data = array(
                        'user_id' => $user_id,
                        'password' => md5($this->input->post("password")),
                        'timestamp' => date("Y-m-d H:i:s")
                    );
                    $data['id'] = incremented('user_passwords');
                    /**/ $this->Crud_model->insert("user_passwords", $data);
                    //end insert password in database
                    if ($this->input->post("force_reset")) {
                        $update = array('force_reset' => $this->input->post("force_reset"));
                        $this->Global_model->global_update("users", $user_id, $update);
                    }
                    $user = $this->Global_model->get_data_by_id('users', $user_id);
                    save_audit("reset password for user with email " . $user->email);
                    redirect(base_url("admin/User/get_system_users/success"));
                }
            } else {
                redirect('admin/Dashboard');
            }
        } else {
            redirect('admin/Dashboard');
        }
    }

    function valid_old_password($value) {
        $this->form_validation->set_message("valid_old_password", $this->lang->line("old_password_not_match"));
        $user_id = $this->session->userdata('user_id');
        $last = $this->User_model->get_last_password($user_id);
        if (!empty($last)) {
            if ($last->password == md5($value)) {
                return TRUE;
            }
        }
        return FALSE;
    }

    function delete_user_view($user_id, $status = NULL) {
        $status = sanitize($status);
        $user_id = sanitize($user_id);
        if ($user_id != '' && $user_id != NULL) {
            if (($this->session->userdata('user_id') != "")) {
                $data = array('title' => 'Delete System User');
                $data['system_user_id'] = $user_id;
                $data['status'] = $status;
                $data['attributes'] = array('class' => 'form-horizontal', 'role' => 'form');
                $data['label_att'] = array('class' => 'col-sm-4 control-label');
                $this->load->view('admin/pages/user/delete_user', $data);
            } else {
                redirect('admin/Dashboard');
            }
        } else {
            redirect('admin/Dashboard');
        }
    }

    function delete_user($user_id = NULL) {
        $user_id = sanitize($user_id);
        if ($user_id != '' && $user_id != NULL) {
            if (($this->session->userdata('user_id') != "")) {

                $this->Global_model->global_update("users", $user_id, array("deleted" => 1));
                // audit
                save_audit("delete user with email : " . $this->getUserName($user_id));
                /////////

                redirect('admin/User/get_system_users/success');
            } else {
                redirect('admin/dashboard');
            }
        } else {
            redirect('admin/dashboard');
        }
    }

    function password_validation() {
        //check password matches any histrory
        $this->load->model("admin/Login_model");
        $this->form_validation->set_message('password_validation', 'The {field} field shouldn\'t match any of your previous passwords');
        $new_password = $this->input->post("password");
        $password_management = $this->Login_model->get_password_management();
        if ($password_management->enforce_history) {
            $exists = $this->Login_model->check_password($this->session->userdata("user_id"), md5($new_password));
            if (!empty($exists)) {
                return FALSE;
            }
        }
        return TRUE;
    }

    function getUserName($id) {
        $id = sanitize($id);
        $this->db->where('id', $id);
        $this->db->select('email');
        $userName = $this->db->get('users')->row();
        if ($userName) {
            return $userName->email;
        }
        return '';
    }

    function change_user_status($user_id, $status) {
        if (substr($this->session->userdata('levels')->users_super_admin, 1, 1) == '0' &&
                substr($this->session->userdata('levels')->users_bank_managers, 1, 1) == '0' &&
                substr($this->session->userdata('levels')->users_can_approve, 1, 1) == '0' &&
                substr($this->session->userdata('levels')->users_normal, 1, 1) == '0' &&
                substr($this->session->userdata('levels')->users_custom_levels, 1, 1) == '0') {
            $this->session->set_flashdata('no_permission', 1);
            redirect('admin/Dashboard');
        }
        $this->db->update('users', array('active' => $status), array('id' => $user_id));
        redirect(base_url('admin/User/get_system_users/success'));
    }

    function captcha() {
        $this->load->helper('captcha');
        $vals = array(
            'img_path' => './captcha/',
            'img_url' => base_url("captcha/"),
            'word_length' => 4,
			'pool' => '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ',
			'colors' => array(
                'background' => array(255, 255, 255),
                'border' =>array(255, 255, 255),
                'text' => array(0, 0, 0),
				'grid' => array(155, 164, 171)
            )
        );

        $captcha = create_captcha($vals);
        $this->session->set_userdata("captcha",$captcha);
        return $captcha;
    }

    function valid_captcha($value) {
        $this->form_validation->set_message("valid_captcha", $this->lang->line("invalid_captcha"));
        if ($this->session->userdata("captcha")) {
            $captcha = $this->session->userdata("captcha");
            $this->session->unset_userdata("captcha");
            if (strtolower($value) == strtolower($captcha['word'])) {
                return TRUE;
            }
        }
        return FALSE;
    }

    function unique_email($value, $id = 0) {
        $this->form_validation->set_message("unique_email", $this->lang->line("unique_email"));
        $user = [];
        if ($id != 0) {
            $user = $this->db->get_where('users', array("id !=" => $id, 'LOWER("email")' => strtolower($value), "deleted !=" => 1))->row();
        } else {
            $user = $this->db->get_where('users', array('LOWER("email")' => strtolower($value), "deleted !=" => 1))->row();
        }
        if (!empty($user)) {
            return FALSE;
        }
        return TRUE;
    }

    function send_notification_email() {
        $this->load->model("admin/Cases_model");
        $email_settings = $this->Cases_model->get_email_settings();
        if (!empty($email_settings->notification_email)) {
            $msg_content = $this->get_email_message();
            // config email smtp
            $settings = $this->db->get('email_settings')->row();
            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => $settings->smtp_host,
                'mailtype' => 'text',
                'charset' => 'iso-8859-1',
                'newline' => "\r\n"
            );
            $this->load->library('email');
            $this->email->initialize($config);
            $this->email->from($email_settings->host_mail);
            $this->email->to($email_settings->notification_email);
            $this->email->subject('New User');
            $this->email->message($msg_content);
            $this->email->set_newline("\r\n");
            $sent = $this->email->send();
            $err = $this->email->print_debugger();
        }
    }

    function get_email_message() {
        $msg_content = "";
        $name = $this->input->post("first_name") . " " . $this->input->post("last_name");
        $email = $this->input->post("email");
        $bank = $this->User_model->get_bank($this->input->post("bank"));
        $access_level = $this->User_model->get_access_level($this->input->post("access_level"));
        $msg_content .= "Hello! \r\n";
        $msg_content .= "A new user has been created.\r\n";
        $msg_content .= "Name: $name \r\n";
        $msg_content .= "Email: $email \r\n";
        $msg_content .= "Bank: $bank->name \r\n";
        $msg_content .= "Access Level: $access_level->name \r\n";
        $msg_content .= "For more information, please login " . base_url() . "  \r\n";
        $msg_content .= "Thank you!";
        return $msg_content;
    }

    function test_pass() {
        // echo do_hash('Iscore@2019', 'md5').'<br/>';
        echo md5('P@$$W0rd');
    }

}
