<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Master_model extends CI_Model {
    function validate_edit_unique($table,$id,$code){
        $this->db->where("code",$code);
        $this->db->where("id !=",$id);
        $this->db->where("deleted !=",1);
        $query= $this->db->get($table);
        if($query->num_rows()>0){
            return FALSE;
        }
        return TRUE;
    }
    function validate_unique($table,$code){
        $this->db->where("code",$code);
        $this->db->where("deleted !=",1);
        $query= $this->db->get($table);
        if($query->num_rows()>0){
            return FALSE;
        }
        return TRUE;
    }
}