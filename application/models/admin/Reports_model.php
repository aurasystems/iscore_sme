<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports_model extends CI_Model {

    function get_cases($bank, $branch, $status, $submitted_date, $completed_date, $submitted_date_from, $submitted_date_to, $completed_date_from, $completed_date_to
    ) {
        $logged_user_data = $this->db->get_where('users', array('id' => $this->session->userdata('user_id')))->row();
        if (!$logged_user_data) {
            redirect('admin/Login/Logout');
        }
        $this->db->select("cases.*,banks.name as bank_name, users.branch_code,"
                . "users.first_name,users.last_name,borrower_information.C4 as comp_name,"
                . "borrower_information.C5 as reg_num");
        $this->db->from("cases");
        $this->db->join('users', 'users.id=cases.user_id', 'left');
        $this->db->join("banks", "banks.id = users.bank_id and banks.deleted=0", 'left');
        $this->db->join('borrower_information', 'borrower_information.case_id=cases.id');

        if ($this->session->userdata('levels')->bank_only == '1') {
            $this->db->where('banks.id', $logged_user_data->bank_id);
        }

        if ($this->session->userdata('levels')->branch_only == '1') {
            $this->db->where('banks.id', $logged_user_data->bank_id);
            $this->db->where('users.branch_code', $logged_user_data->branch_code);
        }

        if ($bank != '' && $bank != "all") {
            $this->db->where('banks.id', $bank);
        }
        if ($branch != '') {
            if ($bank == "all") {
                $exp = explode("_", $branch);
                $branch = $exp[0];
                $bank = $exp[1];
            }
            if ($bank != "all") {
                $this->db->where('banks.id', $bank);
            }
            if ($branch != "all") {
                $this->db->where('users.branch_code', $branch);
            }
        }
        if ($status != '' && $status != "all") {
            if ($status == 0) {
                $this->db->where('cases.status !=', 'saved');
                $this->db->where('cases.status !=', 'completed');
            }
            if ($status == 1) {
                $this->db->where('cases.status', 'saved');
                $this->db->where('cases.approved', 0);
            }
            if ($status == 2) {
                $this->db->where('cases.status', 'saved');
                $this->db->where('cases.approved', 1);
                $this->db->where('cases.completed', 0);
            }
            if ($status == 3) {
                $this->db->where('cases.completed', 1);
            }
            if ($status == 4) {//rejected
                $this->db->where('cases.status', 'saved');
                $this->db->where('cases.approved', -1);
                $this->db->where('cases.completed', 0);
            }
        }
        if ($submitted_date != '') {
            $this->db->where('EXTRACT(month FROM "cases"."date_approved") =', date('m', strtotime($submitted_date)));
            $this->db->where('EXTRACT(day FROM "cases"."date_approved") =', date('d', strtotime($submitted_date)));
            $this->db->where('EXTRACT(year FROM "cases"."date_approved") =', date('Y', strtotime($submitted_date)));
        }
        if ($completed_date != '') {
            $this->db->where('EXTRACT(month FROM "cases"."date_completed") =', date('m', strtotime($completed_date)));
            $this->db->where('EXTRACT(day FROM "cases"."date_completed") =', date('d', strtotime($completed_date)));
            $this->db->where('EXTRACT(year FROM "cases"."date_completed") =', date('Y', strtotime($completed_date)));
        }

        if ($submitted_date_from != '') {
            $submitted_date_from = date("Y-m-d", strtotime("-1 days $submitted_date_from"));
            $this->db->where('cases.date_created >', $submitted_date_from);
        }
        if ($submitted_date_to != '') {
            $submitted_date_to = date("Y-m-d", strtotime("+1 days $submitted_date_to"));
            $this->db->where('cases.date_created <', $submitted_date_to);
        }

        if ($completed_date_from != '') {
            $completed_date_from = date("Y-m-d", strtotime("-1 days $completed_date_from"));
            $this->db->where('cases.date_approved >', $completed_date_from);
        }
        if ($completed_date_to != '') {
            $completed_date_to = date("Y-m-d", strtotime("+1 days $completed_date_to"));
            $this->db->where('cases.date_approved <', $completed_date_to);
        }

        return $this->db->get()->result();
    }

    function access_my_bank_only() {
        $this->db->join('accesslevelsfunctions', 'accesslevelsfunctions.levelid=users.access_level');
        $this->db->get_where('users', array('id' => $this->session->userdata('user_id')));
    }

    function get_logs($length = -1, $start = 0, $search_value = '') {
        $this->db->select('audits.*,users.first_name,users.last_name');
        //->db->select('audits.action,users.first_name,audits.timestamp');
         $this->db->order_by('audits.timestamp','DESC');
        $this->db->join('users', 'users.id=audits.user_id', 'left');
        if ($search_value) {
            $this->db->like("users.first_name", $search_value);
            $this->db->or_like("users.last_name", $search_value);
            $this->db->or_like("audits.action", $search_value);
        }
        if ($length != -1) {
            $this->db->limit($length, $start);
        }
        return $this->db->get('audits')->result();
    }

    function get_logs_filter($search_value) {
        $this->db->select('audits.*,users.first_name,users.last_name');
        $this->db->join('users', 'users.id=audits.user_id', 'left');
        $this->db->from('audits');
        if ($search_value) {
            $this->db->like("users.first_name", $search_value);
            $this->db->or_like("users.last_name", $search_value);
            $this->db->or_like("audits.action", $search_value);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_banks_report_data($count_method = 'all') {
        if($count_method == 'all'){
            $this->db->select('banks.id,banks.name,banks.non_subscriber,count(*) as cases_entries');
            $this->db->select('COUNT(CASE WHEN "cases"."completed" = 0 AND "cases"."approved" != -1 THEN 1 END) as not_completed');
            $this->db->select('COUNT(CASE WHEN "cases"."status" = \'saved\' AND "cases"."approved" = 0 THEN 1 END) as awaiting_approve');
            $this->db->select('COUNT(CASE WHEN "cases"."completed" = 1 THEN 1 END) as completed');
            $this->db->select('COUNT(CASE WHEN "cases"."approved" = -1 THEN 1 END) as rejected');
            $this->db->join('users', 'users.bank_id=banks.id');
            $this->db->join('cases', 'cases.user_id = users.id');
            $this->db->group_by('banks.id,banks.name,banks.non_subscriber');
            $banks_have_cases = $this->db->get_where('banks', array('banks.deleted' => 0))->result();
        } else {
			
	//		$this->db->select('DISTINCT "banks"."id","users"."branch_code","borrower_information"."C5"',false);
			
		//	$this->db->join('users', 'users.id=cases.user_id');
			//$this->db->join('banks', 'banks.id = users.bank_id');
			//$this->db->join('borrower_information', 'borrower_information.case_id = cases.id');
			//$this->db->group_by('banks.id');
			//$data = $this->db->get('cases')->result();
			//print_R($data);die;
            
            $this->db->select('banks.id,banks.name,banks.non_subscriber');
            $banks_have_cases = $this->db->get_where('banks', array('banks.deleted' => 0))->result();
			
			foreach($banks_have_cases as $bank){
				//$this->db->select('banks.id,banks.name,banks.non_subscriber');
				//$this->db->select('COUNT(CASE WHEN "cases"."completed" = 0 AND "cases"."approved" != -1 THEN 1 END) as not_completed');
				//$this->db->select('COUNT(CASE WHEN "cases"."status" = \'saved\' AND "cases"."approved" = 0 THEN 1 END) as awaiting_approve');
				//$this->db->select('COUNT(CASE WHEN "cases"."completed" = 1 THEN 1 END) as completed');
				//$this->db->select('COUNT(CASE WHEN "cases"."approved" = -1 THEN 1 END) as rejected');
				$this->db->select('DISTINCT "users"."branch_code","borrower_information"."C5"', false);
				$this->db->join('users', 'users.id=cases.user_id');
				$this->db->join('banks', 'banks.id = users.bank_id');
				$this->db->join('borrower_information', 'borrower_information.case_id = cases.id');
				$cases = $this->db->get_where('cases', array('banks.id' => $bank->id))->result();
				$bank->CASES_ENTRIES = count($cases);
				
				$this->db->select('DISTINCT "users"."branch_code","borrower_information"."C5"', false);
				$this->db->join('users', 'users.id=cases.user_id');
				$this->db->join('banks', 'banks.id = users.bank_id');
				$this->db->join('borrower_information', 'borrower_information.case_id = cases.id');
				$cases = $this->db->get_where('cases', array('banks.id' => $bank->id,'cases.completed' => 1))->result();
				$bank->COMPLETED = count($cases);
			}
			
        }
        $banks_id = array();
        foreach ($banks_have_cases as $banks) {
            array_push($banks_id, $banks->id);
        }

        $this->db->select('banks.id,banks.name,banks.non_subscriber,0 as CASES_ENTRIES,0 as NOT_COMPLETED,0 as AWAITING_APPROVE,'
                . '0 as COMPLETED, 0 as REJECTED');
        $this->db->where_not_in('id', $banks_id);
        $rest_banks = $this->db->get_where('banks', array('deleted' => 0))->result();

        return array_merge($banks_have_cases, $rest_banks);
    }

}
