<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Email_settings_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    function update_settings($data)
    {
        $get_settings = $this->db->get('email_settings')->row();
        if($get_settings)
        {
            //$this->db->where('id', 1);
            $this->db->update('email_settings', $data);
        }
        else
        {
            $this->db->insert('email_settings', $data);
        }
        redirect('admin/Email_settings/index/success');
    }
    function get_email_settings()
    {
        return $this->db->get('email_settings')->row();
    }
}