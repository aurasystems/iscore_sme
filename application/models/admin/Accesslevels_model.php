<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AccessLevels_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function add_level($data) {
        $this->db->where('name', $data['name']);
        $row = $this->db->get('accesslevels')->row();
        if ($row) {
            $this->session->set_flashdata('level', 'Sorry The Level Name :' . $data['name'] . ' Are Already Exist');
        } else {
            $this->db->insert('accesslevels', $data);
        }
    }

    function getLevels() {
        $this->db->select('id,name');
        $this->db->order_by('id');
        return $this->db->get('accesslevels')->result();
    }

    function addAccessLevel($data) {
        $this->db->where('levelid', $data['levelid']);
        $count = $this->db->get('accesslevelsfunctions')->row();
        if ($count) {
            $this->db->where('levelid', $data['levelid']);
            $this->db->update('accesslevelsfunctions', $data);
            return true;
        } else {
            $data['id'] = incremented('accesslevelsfunctions');
            $this->db->insert('accesslevelsfunctions', $data);
            return true;
        }
    }

    function getDbComments() {
        $qu = $this->db->query("select COMMENTS as comm
                from user_col_comments 
                where table_name = 'accesslevelsfunctions'
                    ");
//        $this->db->select('*');
//        $this->db->from('user_tab_comments');
//        $this->db->where('TABLE_NAME', 'accesslevelsfunctions');
//        
//        
//        
//        $this->db->select('COLUMN_COMMENT as comm');
//        $this->db->from('INFORMATION_SCHEMA.COLUMNS');
//        $this->db->where('TABLE_SCHEMA', $this->db->database);
//        $this->db->where('TABLE_NAME', 'accesslevelsfunctions');
        //   $qu = $this->db->get();
        // $qu = $this->db->query('SELECT COLUMN_COMMENT as comm FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = "'.$this->db->database.'" AND TABLE_NAME = "accesslevelsfunctions"');
        return $qu->result();
    }

    function getDbColumns() {
        $qu = $this->db->query("select COLUMN_NAME as col
                from user_tab_cols
                where table_name = 'accesslevelsfunctions'
                    ");
//        SELECT *
//  FROM user_col_comments
//  WHERE table_name = 'MYTABLE';
//        $this->db->select('COLUMN_NAME as col');
//        $this->db->from('INFORMATION_SCHEMA.COLUMNS');
//        $this->db->where('TABLE_SCHEMA', $this->db->database);
//        $this->db->where('TABLE_NAME', 'accesslevelsfunctions');
//        $qu = $this->db->get();
        // $qu = $this->db->query("SELECT COLUMN_NAME as col FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '" . $this->db->database . "' AND TABLE_NAME = 'accesslevelsfunctions'");
        return $qu->result();
    }

    function do_delete_access_level($access_id) {
        $this->db->where('access_level', $access_id);
        $this->db->update('users', array('access_level' => 6));
        $this->db->where('levelid', $access_id);
        $this->db->delete('accesslevelsfunctions');
        $this->db->where('id', $access_id);
        $this->db->delete('accesslevels');
    }

}
