<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Front_settings_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    function update_settings($data)
    {
        $get_settings = $this->db->get('front_settings')->row();
        if(isset($data['front_logo']))
        {
            $this->db->where('id', 1);
            $logo = $this->db->get('front_settings')->row();
            unlink('./uploads/settings/front/'.$logo->front_logo.'');
        }
        if($get_settings)
        {
            $this->db->where('id', 1);
            $this->db->update('front_settings', $data);
        }
        else
        {
            $this->db->insert('front_settings', $data);
        }
        redirect('admin/Front_settings/index/success');
    }
    function get_front_settings()
    {
        return $this->db->get('front_settings')->row();
    }
}