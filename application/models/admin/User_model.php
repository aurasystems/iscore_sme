<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function update_user($user_id, $data) {
        if (isset($data['image'])) {
            $this->db->where('id', $user_id);
            $get_user_img = $this->db->get('users')->row()->image;
        }
        $this->db->where('id', $user_id);
        $this->db->update('users', $data);
        if ($this->db->affected_rows() > 0) {
            $new_data = array(
                'user_first_name' => $data['first_name'],
                'user_last_name' => $data['last_name'],
                'user_display_name' => $data['display_name']
            );
            if (isset($data['image'])) {
                unlink('./uploads/users/' . $get_user_img . '');
                $new_data['user_image'] = $data['image'];
            }
            if ($this->session->userdata('user_id') == $user_id) {
                $this->session->set_userdata($new_data);
            }
        }
    }

    function do_system_user_delete($system_user_id) {
        $this->db->where('id', $system_user_id);
        $get_user_img = $this->db->get('users')->row()->image;
        unlink('./uploads/users/' . $get_user_img . '');
        $this->db->where('id', $system_user_id);
        $this->db->delete('users');
    }

    function get_users($bank_id = NUll) {
        $this->db->select('users.*,accesslevels.name as access_level_name, banks.name as bank_name');
        if (substr($this->session->userdata('levels')->users_custom_levels, 0, 1) == 0) {
            $this->db->where('access_level in (1,4,5,6)', NULL);
        }
        if (substr($this->session->userdata('levels')->users_super_admin, 0, 1) == 0) {
            $this->db->where('access_level !=', 1);
        }
        if (substr($this->session->userdata('levels')->users_bank_managers, 0, 1) == 0) {
            $this->db->where('access_level !=', 4);
        }
        if (substr($this->session->userdata('levels')->users_can_approve, 0, 1) == 0) {
            $this->db->where('access_level !=', 5);
        }
        if (substr($this->session->userdata('levels')->users_normal, 0, 1) == 0) {
            $this->db->where('access_level !=', 6);
        }
        if ($bank_id) {
            if ($this->session->userdata("access_level") != 1) {
                $this->db->where("bank_id", $bank_id);
            }
        }
        $this->db->where("users.deleted !=",1);
        $this->db->join('accesslevelsfunctions', 'users.access_level=accesslevelsfunctions.id');
        $this->db->join('accesslevels', 'accesslevelsfunctions.levelid=accesslevels.id');
        $this->db->join('banks', 'banks.id=users.bank_id');
        return $this->db->get('users')->result();
    }
    
    function get_last_password($user_id){
        $this->db->where("user_id",$user_id);
        $this->db->limit(1);
        $this->db->order_by("timestamp","DESC");
        $query=$this->db->get("user_passwords");
        return $query->row();
    }
    
     function send_password_reset_link($email)
    {
        $msg_content = '';
        //get user info
        $this->db->where('email', $email);
        $user = $this->db->get("users")->row();
        $last_pass= $this->get_last_password($user->id);
        //set a token to verify him
        $token=$user->id."_".substr($last_pass->password, 0,5);
        $link=base_url()."admin/User/verify_reset_password/".$token;
        $msg_content .= "Dear $user->first_name, \r\n";
        $msg_content .= "Please, use the following link to reset your password: \r\n";
        $msg_content .= "<a href='$link'>$link</a> \r\n";
        $msg_content .= "If you didn't request a password reset, please, ignore this message.";
        // config email smtp
        $settings = $this->db->get('email_settings')->row();
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => $settings->smtp_host_b, //ssl://smtp.googlemail.com
            //'smtp_port' => $settings->smtp_port, // 465 
            //'smtp_user' => $settings->smtp_user,
            //'smtp_pass' => $settings->smtp_pass,
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'newline' => "\r\n"
        );
        $this->load->library('email');
        $this->email->initialize($config);
		$this->email->set_crlf( "\r\n" );
        $this->email->from($settings->host_mail_b);
        $this->email->to($email);
        $this->email->subject('Your Password Reset Link');
        $this->email->message($msg_content);
        $this->email->send();
    }
    
    function verify_reset_password($token){
        $token=  explode("_", $token);
        if(!empty($token[0]) && !empty($token[1])){
            $last_pass=$this->get_last_password($token[0]);
            if(!empty($last_pass) && substr($last_pass->password, 0,5)==$token[1]){
                $this->db->where("id",$token[0]);
                $user= $this->db->get("users")->row();
                return $user->email;
            }
        }
        return FALSE;
    }
    
    function get_bank($bank_id){
        $this->db->where("id",$bank_id);
        return $this->db->get("banks")->row();
    }
    function get_access_level($level_id){
        $this->db->where("id",$level_id);
        return $this->db->get("accesslevels")->row();
    }

}
