<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Back_settings_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    function update_settings($data)
    {
        $get_settings = $this->db->get('back_settings')->row();
        if(isset($data['system_logo']))
        {
            $this->db->where('id', 1);
            $logo = $this->db->get('back_settings')->row()->system_logo;
            unlink('./uploads/settings/back/'.$logo.'');
        }
        if($get_settings)
        {
            $this->db->where('id', 1);
            $this->db->update('back_settings', $data);
        }
        else
        {
            $this->db->insert('back_settings', $data);
        }
        redirect('admin/Back_settings/index/success');
    }
    function get_back_settings()
    {
        return $this->db->get('back_settings')->row();
    }
}