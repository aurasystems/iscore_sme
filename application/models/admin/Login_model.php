<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function check_login($email, $password) {
        $this->db->where("email", $email);
        $this->db->where("password", $password);
        $this->db->where("deleted !=", 1);
        $get_user = $this->db->get("users")->row();
        if ($get_user) {
            //add user data to session
            $new_data = array(
                'user_id' => $get_user->id,
                'user_image' => $get_user->image,
                'user_email' => $get_user->email,
                'user_first_name' => $get_user->first_name,
                'user_last_name' => $get_user->last_name,
                'user_display_name' => $get_user->display_name,
                'logged_in' => TRUE,
                'access_level' => $get_user->access_level,
                'bank_id' => $get_user->bank_id
            );
            $this->session->set_userdata($new_data);
            $accessLevelRow = $this->db->get_where('accesslevelsfunctions', array('levelid' => $get_user->access_level))->row();
            $this->session->set_userdata('levels', $accessLevelRow);
            $sys_settings = $this->db->get_where('back_settings')->row();
            if ($sys_settings) {
                $this->session->set_userdata('sys_settings', $sys_settings);
            }
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    
    function get_password_management(){
        $query= $this->db->get("password_management");
        return $query->row();
    }
    
    function get_last_password($user_id){
        $this->db->where("user_id",$user_id);
        $this->db->order_by("timestamp","DESC");
        $query= $this->db->get("user_passwords");
        return $query->row();
    }
    function get_user($email){
        $this->db->where("email",$email);
        $this->db->where("deleted !=",1);
        $query= $this->db->get("users");
        return $query->row();
    }
    function check_password($user_id,$password){
        $this->db->where("user_id",$user_id);
        $this->db->where("password",$password);
        $query= $this->db->get("user_passwords");
        return $query->row();
    }

}
