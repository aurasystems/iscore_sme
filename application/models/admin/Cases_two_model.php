<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cases_two_model extends CI_Model {

    function get_factsheet_data($case_id) {
        $data = $this->db->get_where('borrower_factsheet', array('case_id' => $case_id))->result();
        $formatted_data = array();
        foreach ($data as $row) {
            $formatted_data[$row->row]['C'] = $row->C;
            $formatted_data[$row->row]['D'] = $row->D;
            $formatted_data[$row->row]['E'] = $row->E;
        }
        return $formatted_data;
    }

    function get_financials_data($case_id) {
        $data = $this->db->get_where('borrower_financials', array('case_id' => $case_id))->result();
        $formatted_data = array();
        foreach ($data as $row) {
            $formatted_data[$row->row]['C'] = $row->C;
            $formatted_data[$row->row]['D'] = $row->D;
            $formatted_data[$row->row]['E'] = $row->E;
            $formatted_data[$row->row]['F'] = $row->F;
            $formatted_data[$row->row]['G'] = $row->G;
        }
        return $formatted_data;
    }

    function get_borrower_info_year($case_id) {
        $this->db->select('C36');
        $row = $this->db->get_where('borrower_information', array('case_id' => $case_id))->row();
        if ($row) {
            return date('Y', strtotime($row->C36));
        }
        return 1970;
    }

    function get_borrower_info_date($case_id) {
        $this->db->select('C36');
        $row = $this->db->get_where('borrower_information', array('case_id' => $case_id))->row();
        if ($row) {
            return $row->C36;
        }
        return '';
    }

    function get_assessment_data($case_id) {
        $data = $this->db->get_where('borrower_assessment', array('case_id' => $case_id))->result();
        $formatted_data = array();
        foreach ($data as $row) {
            $formatted_data[$row->row]['F'] = $row->F;
        }
        return $formatted_data;
    }

    function get_cases($length = -1, $start = 0, $search = '') {
        //$this->db->select('cases.*,concat(users.first_name," ",users.last_name) as created_by,banks.name as bank_name,users.branch_code');
        if ($length != -1) {
            $this->db->select('cases.*,b_i.C4 as company_name,users.first_name, users.last_name ,banks.name as bank_name,users.branch_code');
        } else {
            $this->db->select('cases.id');
        }
        if ($length != -1 || $search) {
            $this->db->join('borrower_information b_i', 'b_i.case_id=cases.id', 'left');
            $this->db->join('users', 'users.id=cases.user_id', 'left');
            $this->db->join('banks', 'users.bank_id=banks.id', 'left');
        }
        $this->db->from("cases");
        if ($search) {
            $this->db->group_start();
            $this->db->like("cases.name", $search);
            $this->db->or_like("users.first_name", $search);
            $this->db->or_like("users.last_name", $search);
            $this->db->or_like("b_i.C4", $search);
            $this->db->group_end();
        }
        if ($length != -1) {
            $this->db->limit($length, $start);
            return $this->db->get()->result();
        }
        return $this->db->get()->num_rows();
    }

    function get_cases_count() {
        ## Total number of records without filtering
        $this->db->select('count(*) as allcount');
        $records = $this->db->get('cases')->row();
        return $records->ALLCOUNT;
    }

    function get_filterd_cases_count($search = '') {
        ## Total number of record with filtering
        $this->db->select('count(*) as allcount');
        $this->db->join('borrower_information b_i', 'b_i.case_id=cases.id', 'left');
        $this->db->join('users', 'users.id=cases.user_id', 'left');
        $this->db->join('banks', 'users.bank_id=banks.id', 'left');
        $this->db->group_start();
        $this->db->like("cases.name", $search);
        $this->db->or_like("users.first_name", $search);
        $this->db->or_like("users.last_name", $search);
        $this->db->or_like("b_i.C4", $search);
        $this->db->group_end();
        $records = $this->db->get('cases')->row();
        return $records->ALLCOUNT;
    }

    function get_bank_cases($bank_id, $length = -1, $start = 0, $search = '') {
        $this->db->select('distinct("b_i"."case_id"),cases.*,b_i.C4 as company_name,users.first_name, users.last_name,banks.name as bank_name,users.branch_code');
        $this->db->where("users.bank_id", $bank_id);
        $levels = $this->session->userdata("levels");
        if ($levels->approved_only && $levels->completed_only) {
            $this->db->group_start();
            $this->db->where("cases.approved", 1);
            $this->db->or_where("cases.completed", 1);
            $this->db->group_end();
        } elseif ($levels->approved_only) {
            $this->db->where("cases.approved", 1);
        } elseif ($levels->completed_only) {
            $this->db->where("cases.completed", 1);
        }
        $this->db->join('borrower_information b_i', 'b_i.case_id=cases.id', 'left');
        $this->db->join('users', 'users.id=cases.user_id', 'left');
        $this->db->join('banks', 'users.bank_id=banks.id', 'left');
        if ($search) {
            $this->db->group_start();
            $this->db->like("cases.name", $search);
            //$this->db->or_like("banks.name", $search);
            $this->db->or_like("users.first_name", $search);
            $this->db->or_like("users.last_name", $search);
            $this->db->or_like("b_i.C4", $search);
            $this->db->group_end();
        }
        if ($length != -1) {
            $this->db->limit($length, $start);
            return $this->db->get('cases')->result();
        }
        return $this->db->get('cases')->num_rows();
    }

    function get_branch_cases($branch_codes, $bank_id, $length = -1, $start = 0, $search = '') {
        $this->db->select('distinct("b_i"."case_id"),cases.*,b_i.C4 as company_name,users.first_name, users.last_name,banks.name as bank_name,users.branch_code');
        $this->db->where_in("users.branch_code", $branch_codes);
        $this->db->where("users.bank_id", $bank_id);
        $levels = $this->session->userdata("levels");
        if ($levels->approved_only && $levels->completed_only) {
            $this->db->group_start();
            $this->db->where("cases.approved", 1);
            $this->db->or_where("cases.completed", 1);
            $this->db->group_end();
        } elseif ($levels->approved_only) {
            $this->db->where("cases.approved", 1);
        } elseif ($levels->completed_only) {
            $this->db->where("cases.completed", 1);
        }
        $this->db->join('borrower_information b_i', 'b_i.case_id=cases.id', 'left');
        $this->db->join('users', 'users.id=cases.user_id', 'left');
        $this->db->join('banks', 'users.bank_id=banks.id', 'left');
        if ($search) {
            $this->db->group_start();
            $this->db->like("cases.name", $search);
            // $this->db->or_like("banks.name", $search);
            $this->db->or_like("users.first_name", $search);
            $this->db->or_like("users.last_name", $search);
            $this->db->or_like("b_i.C4", $search);
            $this->db->group_end();
        }
        if ($length != -1) {
            $this->db->limit($length, $start);
            return $this->db->get('cases')->result();
        }
        return $this->db->get('cases')->num_rows();
    }

    function get_user_cases($user_id, $length = -1, $start = 0, $search = '') {
        $this->db->select('distinct("b_i"."case_id"),cases.*,b_i.C4 as company_name,users.first_name, users.last_name,banks.name as bank_name,users.branch_code');
        $this->db->where("users.id", $user_id);
        $levels = $this->session->userdata("levels");
        if ($levels->approved_only && $levels->completed_only) {
            $this->db->group_start();
            $this->db->where("cases.approved", 1);
            $this->db->or_where("cases.completed", 1);
            $this->db->group_end();
        } elseif ($levels->approved_only) {
            $this->db->where("cases.approved", 1);
        } elseif ($levels->completed_only) {
            $this->db->where("cases.completed", 1);
        }
        $this->db->join('borrower_information b_i', 'b_i.case_id=cases.id', 'left');
        $this->db->join('users', 'users.id=cases.user_id', 'left');
        $this->db->join('banks', 'users.bank_id=banks.id', 'left');
        if ($search) {
            $this->db->group_start();
            $this->db->like("cases.name", $search);
            //$this->db->or_like("banks.name", $search);
            $this->db->or_like("users.first_name", $search);
            $this->db->or_like("users.last_name", $search);
            $this->db->or_like("b_i.C4", $search);
            $this->db->group_end();
        }
        if ($length != -1) {
            $this->db->limit($length, $start);
            return $this->db->get('cases')->result();
        }
        return $this->db->get('cases')->num_rows();
    }

    function check_case_status($case_id, $view_only, $step) {
        if ($case_id == NULL) {
            return;
        }
        $this->db->select('status');
        $case = $this->db->get_where('cases', array('id' => $case_id))->row();
        if (!$case) {
            redirect('admin/Dashboard');
        }
        $steps_arr = array('borrower_information', 'borrower_factsheet', 'borrower_financials', 'borrower_assessment', 'saved');
        $step_num = array_search($step, $steps_arr);
        $case_curr_step_num = array_search($case->status, $steps_arr);


        if ($step_num > $case_curr_step_num) {

            $controller = 'Cases_two';
            if ($case_curr_step_num == 0) {
                $controller = 'Cases';
            }
            $this->session->set_userdata('error', $this->lang->line('wrong_step'));
            redirect('admin/' . $controller . '/' . $steps_arr[$case_curr_step_num] . '/' . $case_id . '/' . $view_only);
        }
    }

    function logged_user_bank_exist() {
        $this->db->select('users.id');
        $this->db->join('banks', 'banks.id=users.bank_id and banks.deleted=0');
        return $this->db->get_where('users', array('users.id' => $this->session->userdata('user_id')))->row();
    }

    function get_line_of_activity_value($case_id) {
        $this->db->select('C10');
        $info_row = $this->db->get_where('borrower_information', array('case_id' => $case_id))->row();
        if ($info_row) {
            return $info_row->C10;
        }
        return '';
    }

    function if_curr_stage_pass($case_id, $stage) {
        $status_arr = array('borrower_information', 'borrower_factsheet', 'borrower_financials', 'borrower_assessment', 'saved');
        $case_status = $this->db->get_where('cases', array('id' => $case_id))->row()->status;

        $case_stage_index = array_search($case_status, $status_arr);
        $curr_stage_index = array_search($stage, $status_arr);


        if ($case_stage_index !== FALSE && $curr_stage_index !== FALSE) {
            if ($case_stage_index > $curr_stage_index) {
                return FALSE;
            } else {
                return TRUE;
            }
        }
        return FALSE;
    }

    function get_borrower_information($case_id) {
        $this->db->where("case_id", $case_id);
        $query = $this->db->get("borrower_information");
        return $query->row();
    }

    function get_financials_row($case_id, $row) {
        $this->db->where("case_id", $case_id);
        $this->db->where("row", $row);
        $query = $this->db->get("borrower_financials");
        return $query->row();
    }

    //modified2
    function get_calc($case_id) {
        $this->db->where("rown", 219);
        $this->db->where("case_id", $case_id);
        $query = $this->db->get("calculations");
        return $query->row();
    }

    function get_case_projected($case_id) {
        $this->db->select("*");
        $this->db->where("case_id", $case_id);
        $this->db->order_by("year", "asc");
        $this->db->from("projected_fins");
        $query = $this->db->get();
        return $query->result();
    }

    function get_case_hist($case_id) {
        $this->db->select("*");
        $this->db->where("case_id", $case_id);
        $this->db->order_by("year", "asc");
        $this->db->from("hist_fins");
        $query = $this->db->get();
        return $query->result();
    }

    function get_last_projected($case_id) {
        $this->db->select("year");
        $this->db->where("case_id", $case_id);
        $this->db->order_by("year", "desc");
        $this->db->from("projected_fins");
        $this->db->limit(2);
        $query = $this->db->get();
        return $query->row();
    }

    function get_first_hist($case_id) {
        $this->db->select("year");
        $this->db->where("case_id", $case_id);
        $this->db->order_by("year", "asc");
        $this->db->from("hist_fins");
        $this->db->limit(2);
        $query = $this->db->get();
        return $query->row();
    }

    function get_hist_years($case_id) {
        $this->db->select('year');
        $this->db->group_by('year');
        $years = $this->db->get_where('hist_fins', array('case_id' => $case_id))->result();
        $arr = array();
        foreach ($years as $year) {
            $arr[] = $year->year;
        }
        return $arr;
    }

    function get_projected_years($case_id) {
        $this->db->select('year');
        $this->db->group_by('year');
        $years = $this->db->get_where('projected_fins', array('case_id' => $case_id))->result();
        $arr = array();
        foreach ($years as $year) {
            $arr[] = $year->year;
        }
        return $arr;
    }

    function get_hidden_years($case_id) {
        $this->db->select('year');
        $this->db->group_by('year');
        $history = $this->db->get_where('hist_fins', array('hidden' => 1, 'case_id' => $case_id))->result();

        $this->db->select('year');
        $this->db->group_by('year');
        $projected = $this->db->get_where('projected_fins', array('hidden' => 1, 'case_id' => $case_id))->result();

        $arr = array();
        foreach ($history as $year) {
            $arr[] = $year->year;
        }
        foreach ($projected as $year) {
            $arr[] = $year->year;
        }
        return implode(',', $arr);
    }

    function construct_reassign_name($case_id) {
        $this->db->select('banks.code,users.branch_code,users.first_name,users.last_name,cases.date_created as date');
        $this->db->join('users', 'users.id=cases.user_id');
        $this->db->join('banks', 'users.bank_id=banks.id');
        $row = $this->db->get_where('cases', array('cases.id' => $case_id))->row();

        $this->db->select('C5');
        $commerical_registry_num = $this->db->get_where('borrower_information', array('case_id' => $case_id))->row();

        if (!$row) {
            return FALSE;
        }
        if (!$commerical_registry_num) {
            $commerical_registry_num = '';
        } else {
            $commerical_registry_num = $commerical_registry_num->C5;
        }
//        return $row->code . $row->branch_code . $commerical_registry_num .
//                $row->creator_name . date('dmY', strtotime($row->date));
        $name = "M" . $row->code . "B" . $row->branch_code . "C" . $commerical_registry_num . "I" .
                $row->first_name . $row->last_name . "D" . date('dmY', strtotime($row->date));

        $another_case_with_same_name = $this->db->get_where('cases', array('name' => $name, "id !=" => $case_id))->row();
        $inc = 1;
        while ($another_case_with_same_name) {
            $name .= '_' . $inc;
            $inc++;
            $another_case_with_same_name = $this->db->get_where('cases', array('name' => $name, "id !=" => $case_id))->row();
        }
        return str_replace(" ", "", $name);
    }

    function get_factsheet_arr($case_id) {
        $this->db->where("case_id", $case_id);
        $arr = $this->db->get("borrower_factsheet")->result_array();
        $factsheet["case_id"] = $case_id;
        for ($i = 0; $i < count($arr); $i++) {
            $factsheet["C" . $arr[$i]["row"]] = $arr[$i]["C"];
            $factsheet["D" . $arr[$i]["row"]] = $arr[$i]["D"];
            $factsheet["E" . $arr[$i]["row"]] = $arr[$i]["E"];
        }
        return $factsheet;
    }

    function get_financials_arr($case_id) {
        $this->db->where("case_id", $case_id);
        $arr = $this->db->get("borrower_financials")->result_array();
        $financials["case_id"] = $case_id;
        for ($i = 0; $i < count($arr); $i++) {
            $financials["C" . $arr[$i]["row"]] = $arr[$i]["C"];
            $financials["D" . $arr[$i]["row"]] = $arr[$i]["D"];
            $financials["E" . $arr[$i]["row"]] = $arr[$i]["E"];
            $financials["F" . $arr[$i]["row"]] = $arr[$i]["F"];
            $financials["G" . $arr[$i]["row"]] = $arr[$i]["G"];
        }
        return $financials;
    }

    function get_assessment_arr($case_id) {
        $this->db->where("case_id", $case_id);
        $arr = $this->db->get("borrower_assessment")->result_array();
        $assessment["case_id"] = $case_id;
        for ($i = 0; $i < count($arr); $i++) {
            $assessment["F" . $arr[$i]["row"]] = $arr[$i]["F"];
        }
        return $assessment;
    }

    function get_reassignment_users() {
        $users = [];
        $bank_id = $this->session->userdata("bank_id");
        if (compare_level("super_admin")) {
            $this->db->where("users.deleted !=", 1);
            $users = $this->db->get('users')->result();
        } elseif (compare_level("bank_admin")) {
            $this->db->where("users.deleted !=", 1);
            $this->db->where("users.bank_id", $bank_id);
            $this->db->where("users.access_level", get_level_id("maker"));
            $users = $this->db->get('users')->result();
        } elseif (compare_level("verifier")) {
            $this->db->where("id", $this->session->userdata('user_id'));
            $branch_code = $this->db->get("users")->row()->branch_code;
            $this->db->where("users.deleted !=", 1);
            $this->db->where("users.bank_id", $bank_id);
            $this->db->where("users.access_level", get_level_id("maker"));
            $this->db->where("users.branch_code", $branch_code);
            $users = $this->db->get('users')->result();
        }
        return $users;
    }

    function regenrate_ref_no($case_id) {

        $this->load->model("admin/Cases_model");
        //$this->db->select('user_id');
        $case_row = $this->Cases_model->get_case($case_id);
        $user = $this->Cases_model->get_user_info($case_row->user_id);
        //update case and borrower information
        $data["C16"] = $user->bank_name;
        $data["C17"] = $user->branch_code;
        //$data["C18"] = $user->first_name . " " . $user->last_name;
        // $this->Crud_model->update("cases", $case_id, ["user_id" => $user_id]);
        $this->Cases_model->update_bi($case_id, $data);

        // $case_row = $this->db->get_where('cases', array('id' => $case_id))->row();
        //construct case name
        $case_name = $this->construct_reassign_name($case_id);
        $this->Crud_model->update("cases", $case_id, ["name" => $case_name]);

        // rename excel file with the new name .. hereeeeeeee
        $date_approved = date('d-m-Y', strtotime($case_row->date_approved));
        if ($case_row->approved == 1 && file_exists("./root/Cases/" . $date_approved . "/$case_row->name.xlsx")) {   // that mean that the excel file has been generated .. 
            rename("./root/Cases/" . $date_approved . "/$case_row->name.xlsx", "./root/Cases/" . $date_approved . "/$case_name.xlsx");

            ini_set('memory_limit', '2048M');
            set_time_limit(1800);
            $this->load->library('excel');

            $excel2 = PHPExcel_IOFactory::load("./root/Cases/" . $date_approved . "/$case_name.xlsx");

            $objSheet = $excel2->setActiveSheetIndex(3);

            $objSheet->setCellValue('C17', $user->branch_code)
                    ->setCellValue('C16', $user->bank_name);

            $objWriter = PHPExcel_IOFactory::createWriter($excel2, 'Excel2007');

            $objWriter->save("./root/Cases/" . $date_approved . "/" . $case_name . "_temp.xlsx");
            unlink("./root/Cases/" . $date_approved . "/" . $case_name . ".xlsx");
            rename("./root/Cases/" . $date_approved . "/" . $case_name . "_temp.xlsx", "./root/Cases/" . $date_approved . "/" . $case_name . ".xlsx");
        }
        // end rename excel file ..
        save_audit("Regenerated ref no to Case : " . $case_name);

        return true;
    }

}
