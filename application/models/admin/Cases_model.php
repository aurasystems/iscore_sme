<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cases_model extends CI_Model {

    function get_user_info($user_id) {
        $this->db->select("banks.name as bank_name,banks.code, users.branch_code, users.first_name, users.last_name");
        $this->db->where("users.id", $user_id);
        $this->db->from("users");
        $this->db->join("banks", "banks.id =  users.bank_id");
        $query = $this->db->get();
        return $query->row();
    }

    function get_bi_arr($case_id) {
        $this->db->where("case_id", $case_id);
        $query = $this->db->get("borrower_information");
        return $query->row_array();
    }

    function update_bi($case_id, $data) {
        $this->db->where("case_id", $case_id);
        $this->db->update("borrower_information", $data);
    }

    function get_borrower_information($case_id) {
        $this->db->where("case_id", $case_id);
        $query = $this->db->get("borrower_information");
        return $query->row();
    }

    function get_borrower_factsheet($case_id) {
        $this->db->where("case_id", $case_id);
        $query = $this->db->get("borrower_factsheet");
        return $query->result();
    }

    function get_borrower_financials($case_id) {
        $this->db->where("case_id", $case_id);
        $query = $this->db->get("borrower_financials");
        return $query->result();
    }

    function get_borrower_assessment($case_id) {
        $this->db->where("case_id", $case_id);
        $query = $this->db->get("borrower_assessment");
        return $query->result();
    }

    function get_case($case_id) {
        $this->db->select("cases.*, users.first_name, users.last_name, users.branch_code");
        $this->db->where("cases.id", $case_id);
        $this->db->from("cases");
        $this->db->join("users", "users.id = cases.approved_by");
        $query = $this->db->get();
        return $query->row();
    }

    function get_case_with_current_user_approve($case_id, $browser_download) {
        if ($browser_download) {
            $this->db->select("cases.*,users.first_name,users.last_name,maker.branch_code");
            $this->db->where("cases.id", $case_id);
            $this->db->from("cases");
            $this->db->join("users", "users.id = cases.approved_by");
            $this->db->join("users maker", "maker.id = cases.user_id");
            $query = $this->db->get();
            return $query->row();
        }
        $this->db->select("cases.*"); //, users.first_name, users.last_name, users.branch_code
        $this->db->where("cases.id", $case_id);
        $this->db->from("cases");
        //$this->db->join("users", "users.id = cases.approved_by");
        $query = $this->db->get();
        $case_row = $query->row();

        if ($case_row) {
            $this->db->select('users.first_name, users.last_name, users.branch_code');
            $user_row = $this->db->get_where('users', array('id' => $this->session->userdata("user_id")))->row();
            $case_row->first_name = $user_row->first_name;
            $case_row->last_name = $user_row->last_name;
            $case_row->branch_code = $user_row->branch_code;
            return $case_row;
        }
        return FALSE;
    }

    function construct_excel_file_name($case_id) {
        $this->db->select('banks.code,users.branch_code,users.first_name,users.last_name,cases.date_created as date');
        $this->db->join('users', 'users.id=cases.user_id');
        $this->db->join('banks', 'users.bank_id=banks.id');
        $row = $this->db->get_where('cases', array('cases.id' => $case_id))->row();

        $this->db->select('C5');
        $commerical_registry_num = $this->db->get_where('borrower_information', array('case_id' => $case_id))->row();

        if (!$row) {
            return FALSE;
        }
        if (!$commerical_registry_num) {
            $commerical_registry_num = '';
        } else {
            $commerical_registry_num = $commerical_registry_num->C5;
        }
//        return $row->code . $row->branch_code . $commerical_registry_num .
//                $row->creator_name . date('dmY', strtotime($row->date));
        $name = "M" . $row->code . "B" . $row->branch_code . "C" . $commerical_registry_num . "I" .
                $row->first_name . $row->last_name . "D" . date('dmY', strtotime($row->date));

        $another_case_with_same_name = $this->db->get_where('cases', array('name' => $name))->row();
        $inc = 1;
        while ($another_case_with_same_name) {
            $name .= '_' . $inc;
            $inc++;
            $another_case_with_same_name = $this->db->get_where('cases', array('name' => $name))->row();
        }
        return str_replace(" ", "", $name);
    }

    function get_email_settings() {
        return $this->db->get("email_settings")->row();
    }

    function get_case_info($case_id) {
        $this->db->select("cases.*, banks.name as bank_name, users.first_name, users.last_name");
        $this->db->where("cases.id", $case_id);
        $this->db->join("users", "users.id = cases.user_id");
        $this->db->join("banks", "banks.id = users.bank_id");
        $this->db->from("cases");
        $query = $this->db->get();
        return $query->row();
    }

    function check_case($name, $id) {
        $this->db->where("id !=", $id);
        $this->db->where("completed !=", 1);
        $this->db->like('name', $name, 'after');
        $query = $this->db->get("cases");
        return $query->row();
    }

    function update_case_name($case_id, $old_C5 = NULL) {
        //check if the CRN is unchanged
//        $this->db->where("id",$case_id);
//        $case=$this->db->get("cases")->row();
        if (!$old_C5) {
            $this->db->where("case_id", $case_id);
            $case = $this->db->get("borrower_information")->row();
            $old_C5 = $case->C5;
        }
        if ($old_C5 == $this->input->post("C5")) {
            return FALSE;
        }
        //construct the name
        $this->db->select('banks.code,users.branch_code,users.first_name,users.last_name,cases.date_created as date');
        $this->db->join('users', 'users.id=cases.user_id');
        $this->db->join('banks', 'users.bank_id=banks.id');
        $row = $this->db->get_where('cases', array('cases.id' => $case_id))->row();

        $this->db->select('C5');
        $commerical_registry_num = $this->db->get_where('borrower_information', array('case_id' => $case_id))->row();

        if (!$row) {
            return FALSE;
        }
        if (!$commerical_registry_num) {
            $commerical_registry_num = '';
        } else {
            $commerical_registry_num = $commerical_registry_num->C5;
        }
//        return $row->code . $row->branch_code . $commerical_registry_num .
//                $row->creator_name . date('dmY', strtotime($row->date));
        $name = "M" . $row->code . "B" . $row->branch_code . "C" . $commerical_registry_num . "I" .
                $row->first_name . $row->last_name . "D" . date('dmY', strtotime($row->date));
        //check any case but this one
        $another_case_with_same_name = $this->db->get_where('cases', array('name' => $name, "id !=", $case_id))->row();
        $inc = 1;
        while ($another_case_with_same_name) {
            $name .= '_' . $inc;
            $inc++;
            $another_case_with_same_name = $this->db->get_where('cases', array('name' => $name, "id !=", $case_id))->row();
        }
        return str_replace(" ", "", $name);
    }

}
