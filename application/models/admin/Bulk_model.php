<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bulk_model extends CI_Model {

    function map_code($table, $code) {
        if ($code != "" && $code != NULL) {
            $code.="";
            $this->db->where("code", $code);
            $this->db->where("deleted !=", 1);
            $query = $this->db->get($table);
            if ($query->num_rows() > 0) {
                return $query->row()->value;
            }
        }
        return NULL;
    }
    function unmap_code($table, $value) {
        if ($value != "" && $value != NULL) {
            $value.="";
            $this->db->where("value", $value);
            $this->db->where("deleted !=", 1);
            $query = $this->db->get($table);
            if ($query->num_rows() > 0) {
                return $query->row()->code;
            }
        }
        return NULL;
    }
    
    function get_user_info() {
        $this->db->select('banks.code,users.branch_code,users.first_name,users.last_name');
        $this->db->where("users.id",$this->session->userdata("user_id"));
        $this->db->from("users");
        $this->db->join('banks', 'users.bank_id=banks.id');
        $query= $this->db->get();
        return $query->row();
    }
    function check_case_existence($name){
        $this->db->like("name",$name,"after");
        $this->db->where("approved !=",1);
        $this->db->where("completed !=",1);
        $query= $this->db->get("cases");
        return $query->result();
    }
    
    function delete_case($case_id){
        $this->db->where("id",$case_id);
        $this->db->delete("cases");
        
        $this->db->where("case_id",$case_id);
        $this->db->delete("borrower_information");
        
        $this->db->where("case_id",$case_id);
        $this->db->delete("borrower_factsheet");
        
        $this->db->where("case_id",$case_id);
        $this->db->delete("borrower_financials");
        
        $this->db->where("case_id",$case_id);
        $this->db->delete("borrower_assessment");
        
        $this->db->where("case_id",$case_id);
        $this->db->delete("calculations");
    }

}
