<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    function check_login($email,$password)
    {
        $this->db->where("status", 1);
        $this->db->where("email", $email);
        $this->db->where("password", $password);
        $get_client = $this->db->get("clients")->row();
        if($get_client)
        {
            //add user data to session
            $new_data = array(
                'client_id' => $get_client->id,
                'client_email' => $get_client->email,
                'client_first_name' => $get_client->first_name,
                'client_last_name' => $get_client->last_name,
                'client_display_name' => $get_client->display_name,
                'logged_in' => TRUE
            );
            $this->session->set_userdata($new_data);
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}