<?php

class Crud_model extends CI_Model
{
    function __construct() {
        parent::__construct();
        $this->db->query("alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS'", "");
    }
    function insert($table,$data)
    {
        $this->db->insert($table,$data);
//        return $this->db->insert_id();
    }
    function update($table,$id,$data)
    {
        $this->db->where('id',$id);
        $this->db->update($table,$data);
    }
    function get_one($table,$id)
    {
        $this->db->where('id',$id);
        $query=  $this->db->get($table);
        return $query->row();
    }
    function query_group()
    {
        $this->db->group_start();
        $this->db->where('date >=',$start_date);
        $this->db->where('date <=',$end_date);
        $this->db->group_end();
        
        $this->db->or_group_start();
        $this->db->where('date >=',$start_date);
        $this->db->where('date <=',$end_date);
        $this->db->group_end();
    }
    function delete($table,$id)
    {
        $this->db->where('id',$id);
        $this->db->delete($table);
    }
    function soft_delete($table,$id)
    {
        $this->db->where('id',$id);
        $this->db->update($table,array('deleted'=>1));
    }
    function get_all($table)
    {
        $query=$this->db->get($table);
        return $query->result();
    }
    function get_all_active($table)
    {
        $this->db->where('deleted !=',1);
        $query= $this->db->get($table);
        return $query->result();
    }
    function send_email($email,$subject,$message)
    {
        $email_settings= $this->db->get('email_settings')->row();
        if(!empty($email_settings)){
            $this->load->library('email');
          $config = Array(
            'smtp_host' => $email_settings->smtp_host, //ssl://smtp.googlemail.com
            'smtp_port' => $email_settings->smtp_port, // 465 
            'smtp_user' => $email_settings->smtp_user,
            'smtp_pass' => $email_settings->smtp_pass
        );
        $this->email->initialize($config);
        $this->email->set_mailtype("html");

        $this->email->from($email_settings->host_mail, $email_settings->sender_name);
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);

        $this->email->send();
        }
    }
    function get_settings($table)
    {
        $query=$this->db->get($table);
        return $query->row();
    }
     function utf8_to_unicode_codepoints($text) {
     return ''.implode(unpack('H*', iconv("UTF-8", "UCS-2BE", $text)));
 }
    
     function send_sms($to,$message,$lang=NULL)
    {
         $sms_settings= $this->get_settings("sms_settings");
         if(!empty($sms_settings)){
             $language=1;
             if(!empty($lang) && $lang="arabic")
             {
                 $language=3;
//                 $message= strtoupper(str_replace(array('"', '\u'), array('',''), json_encode($message)));
                 $message= $this->utf8_to_unicode_codepoints($message); 
             }
             else{
                   $message= str_replace(" ", "%20", $message);
             }
//        $url='https://platform.clickatell.com/messages';
        
 
$url="https://smsmisr.com/api/send.aspx?username=$sms_settings->username&password=$sms_settings->password&language=$language&sender=$sms_settings->sender&mobile=$to&message=$message" ;
         $headers = [
            'Content-Type: application/json',
            'Accept: application/json',
//            'Authorization: ' . "$sms_settings->api_key"
        ];
//         $data=array(
//             'content'=>"$message",
//             'to'=>["$to"],
//         );
         $data=array();
         $postData=  json_encode($data);
        $ch = curl_init();

//    BEGIN LOCAL
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
//    END LOCAL

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);


        $errors = curl_error($ch);
        $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);
        $this->update("sms_settings", 1, array("verification_header"=>  json_encode($response)));
        return $response;
         }
    }
    
    function check_view($function)
    {
         $permission=substr($this->session->userdata('levels')->$function, 0, 1);
          if(!$permission)
         {
             redirect(base_url("Settings/permission_denied"));
         }
         else{
             return $permission;
         }
    }
    function check_add($function)
    {
         $permission=substr($this->session->userdata('levels')->$function, 1, 1);
          if(!$permission)
         {
             redirect(base_url("Settings/permission_denied"));
         }
         else{
             return $permission;
         }
    }
    function check_edit($function)
    {
         $permission=substr($this->session->userdata('levels')->$function, 2, 1);
          if(!$permission)
         {
             redirect(base_url("Settings/permission_denied"));
         }
         else{
             return $permission;
         }
    }
    function check_delete($function)
    {
         $permission=substr($this->session->userdata('levels')->$function, 3, 1);
         if(!$permission)
         {
             redirect(base_url("Settings/permission_denied"));
         }
         else{
             return $permission;
         }
    }
    function get_view($function)
    {
        $permission=substr($this->session->userdata('levels')->$function, 0, 1);
        return $permission;
    }
    function get_add($function)
    {
        $permission=substr($this->session->userdata('levels')->$function, 1, 1);
        return $permission;
    }
    function get_edit($function)
    {
        $permission=substr($this->session->userdata('levels')->$function, 2, 1);
        return $permission;
    }
    function get_delete($function)
    {
        $permission=substr($this->session->userdata('levels')->$function, 3, 1);
        return $permission;
    }
    function test()
    {
        return 1;
    }

    function get_one_array($table,$id)
    {
        $this->db->where('id',$id);
        $query=  $this->db->get($table);
        return $query->row_array();
    }
    function update_oracle_date($data){
        $this->db->where($data["table"].'.id',$data['id']);
        $this->db->set('"'.$data['col'].'"',"to_date('".$data['val']."','YYYY-MM-DD HH24:MI:SS')",FALSE);
        $this->db->update($data['table']);
    }
}