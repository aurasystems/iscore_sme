<!DOCTYPE html>
<html>
    <head>
         <?php $this->load->view('admin/private/head'); ?>
    </head>
    <body class="error-page">
        <div class="row">
            <div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
                <div class="error-container">
                    <div class="error-main">
                        <h2><strong><span id="500"></span></strong></h2>
                        <h3><span id="500-txt"></span></h3>
                        <h4><span id="500-txt-2"></span></h4>
                        <br>
                    
                    </div>
                </div>
            </div>
        </div>
      
        <script src="<?=  base_url()?>assets/plugins/jquery/jquery-1.11.1.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/jquery-ui/jquery-ui-1.11.2.min.js"></script>
        <script src="<?=  base_url()?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?=  base_url()?>assets/plugins/typed/typed.js"></script>
        <script>
            $(function(){
                $("#500").typed({
                    strings: ["<?=$title?>"],
                    typeSpeed: 1,
                    backDelay: 500,
                    loop: false,
                    contentType: 'html',
                    loopCount: false,
                    callback: function() {
                        $('h1 .typed-cursor').css('-webkit-animation', 'none').animate({opacity: 0}, 400);
                        $("#500-txt").typed({
                            strings: [""],
                            typeSpeed: 1,
                            backDelay: 500,
                            loop: false,
                            contentType: 'html', 
                            loopCount: false,
                            callback: function() {
                                $('h3 .typed-cursor').css('-webkit-animation', 'none').animate({opacity: 0}, 400);
                                $("#500-txt-2").typed({
                                    strings: ["<?=$msg?>"],
                                    typeSpeed: 1,
                                    backDelay: 500,
                                    loop: false,
                                    contentType: 'html', 
                                    loopCount: false,
                                    callback: function() {
                                        $('#content-500').delay(300).slideDown();
                                    },
                                }); 
                            }
                        });  
                    }
                });  
            });
        </script>
    </body>
</html>