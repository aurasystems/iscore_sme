<header class="center header-md parallax" data-img="" data-speed="0.5" style="background-color:#1D5F77;">
  <div class="container">
    <div class="fade-text">
      <h1 class="header-title">Make Pages</h1>
      <h3 class="header-message">Make theme comes with more more than 90 pre-designs demo pages.</h3>
      <h3 class="header-message">Want custom variation? Create your own page with builder.</h3>
    </div>
  </div>
</header>