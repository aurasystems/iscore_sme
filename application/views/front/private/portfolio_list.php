 <!-- Begin Portfolio List -->
    <section class="section-portfolio portfolio-list bg-gray-light">
      <div class="container">
        <div class="row">
          <div class="center m-b-30 p-t-30">
            <h3 class="section-title">Similar Projects</h3>
            <p class="p-b-20">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore. Sit voluptatem accusantium doloremque laudantium, totam rem aperiam, </p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="portfolio">
              <div class="portfolio-thumbnail">
                  <figure class="effect-bubba">
                    <img src="../../common-files/images/gallery/1.jpg" alt="portfolio 1">
                    <figcaption>
                      <h2>In the <span>Dark</span></h2>
                      <p>Voluptatem accusantium doloremque laudantium.</p>
                    </figcaption>     
                  </figure>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="portfolio">
              <div class="portfolio-thumbnail">
                <a href="portfolio-single-left.html">
                  <figure class="effect-bubba">
                    <img src="../../common-files/images/gallery/2.jpg" alt="portfolio 2">
                    <figcaption>
                      <h2>At the <span>Beach</span></h2>
                      <p>Voluptatem accusantium doloremque laudantium.</p>
                    </figcaption>     
                  </figure>
                </a>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="portfolio">
              <div class="portfolio-thumbnail">
                <a href="portfolio-single-left.html">
                  <figure class="effect-bubba">
                    <img src="../../common-files/images/gallery/3.jpg" alt="portfolio 3">
                    <figcaption>
                      <h2>Feeling the <span>Field</span></h2>
                      <p>Voluptatem accusantium doloremque laudantium.</p>
                    </figcaption>     
                  </figure>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--End  Portfolio List -->