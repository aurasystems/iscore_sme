<a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>
<script src="<?=base_url()?>assets/front/plugins/jquery/jquery-1.11.1.min.js"></script>
<script src="<?=base_url()?>assets/front/plugins/bootstrap/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/front/plugins/modernizr/modernizr.js"></script>
<script src="<?=base_url()?>assets/front/plugins/jquery/jquery.easing.1.3.js"></script>
<script src="<?=base_url()?>assets/front/plugins/appear/jquery.appear.js"></script>
<script src="<?=base_url()?>assets/front/plugins/smart-menu/jquery.smartmenus.js"></script>
<script src="<?=base_url()?>assets/front/plugins/smart-menu/addons/bootstrap/jquery.smartmenus.bootstrap.js"></script>
<!-- BEGIN PAGE SCRIPTS -->
<script src="<?=base_url()?>assets/front/plugins/parallax/scripts/jquery.parallax-1.1.3.js"></script>
<script src="<?=base_url()?>assets/front/plugins/magnific/jquery.magnific-popup.min.js"></script>
<script src="<?=base_url()?>assets/front/plugins/isotope/isotope.pkgd.min.js"></script>
<!-- END PAGE SCRIPTS -->
<script src="<?=base_url()?>assets/front/js/application.js"></script>