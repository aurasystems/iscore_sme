<div class="section-footer footer-wrap bg-dark">
  <div class="container footer center">
    <div class="row">
      <div class="col-md-3">
        <h4>Company Info</h4>
        <p><?= $front_settings->company_info ?></p>
      </div>
      <div class="col-md-3">
        <h4>Contact Info</h4>
        <p><i class="line-icon-map"></i><?= $front_settings->address ?></p>
        <p><i class="line-icon-screen-smartphone"></i><?= $front_settings->phone ?></p>
        <p><i class="line-icon-envelope-open"></i><?= $front_settings->email ?></p>
      </div>
      <div class="col-md-3">
        <h4>Support</h4>
        <p><a href="#">Faq</a></p>
        <p><a href="#">Terms of Services</a></p>
        <p><a href="#">Privacy Policy</a></p>
        <p><a href="#">Contact us</a></p>
      </div>
      <div class="col-md-3">
        <h4>Get In Touch</h4>
        <div class="social-icons social-square">
            <ul class="text-left">
                <li><a href="<?= $front_settings->facebook_url ?>" class="animated" data-animation="fadeIn" data-animation-delay="300"><i class="fa fa-facebook"></i></a></li>
                <li><a href="<?= $front_settings->twitter_url ?>" class="animated" data-animation="fadeIn" data-animation-delay="500"><i class="fa fa-twitter"></i></a></li>
                <li><a href="<?= $front_settings->google_plus_url ?>" class="animated" data-animation="fadeIn" data-animation-delay="700"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="<?= $front_settings->pinterest_url ?>" class="animated" data-animation="fadeIn" data-animation-delay="900"><i class="fa fa-pinterest"></i></a></li>
                <li><a href="<?= $front_settings->flickr_url ?>" class="animated" data-animation="fadeIn" data-animation-delay="1000"><i class="fa fa-flickr"></i></a></li>
                <li><a href="<?= $front_settings->linkedin_url ?>" class="animated" data-animation="fadeIn" data-animation-delay="1200"><i class="fa fa-linkedin"></i></a></li>
            </ul>
        </div>
      </div>
    </div>
  </div>
</div>