<div class="topnav fixed-topnav transparent">
  <form action="#" class="form-search" method="get" role="form">
    <div class="container">
      <div class="search-inner clearfix">
        <i class="fa fa-search"></i>
        <input type="text" placeholder="Search" name="s" class="search-input" autocomplete="off">
        <input type="submit" value="Search">
        <div class="search-close">
          <a href="#"><i class="line-icon-close"></i></a>
        </div>
      </div>
    </div>
  </form>
  <div class="section">
    <div id="topbar-hold" class="nav-hold container">
      <nav class="navbar" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only"><?= $this->lang->line('lang_toggle_navi') ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- Site Name -->
            <a class="site-name navbar-brand" href="<?= base_url() ?>Dashboard"></a>
        </div>
        <!-- Main Navigation menu Starts -->
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <?php if($this->session->userdata('client_id')!= ""){ ?>
                <li><a href="<?= base_url() ?>Dashboard"><?=$this->lang->line('lang_home')?></a></li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle top-menu" data-toggle="dropdown" data-hover="dropdown" data-close-others="false"><?= $this->session->userdata('client_display_name') ?></a>
                  <ul class="dropdown-menu">
                    <li><a href="<?= base_url() ?>Client"><?=$this->lang->line('lang_edit_profile')?></a></li>
                    <li><a href="<?= base_url() ?>Login/Logout"><?=$this->lang->line('lang_logout')?></a></li>
                  </ul>
                </li>
                <li><a href="<?= base_url() ?>Contact_us"><?=$this->lang->line('lang_contact_us')?></a></li>
                <?php }else{ ?>
                <li><a href="<?= base_url() ?>Dashboard"><?=$this->lang->line('lang_home')?></a></li>
                <li><a href="<?= base_url() ?>Login"><?=$this->lang->line('lang_login')?></a></li>
                <li><a href="<?= base_url() ?>Register"><?=$this->lang->line('lang_register')?></a></li>
                <li><a href="<?= base_url() ?>Contact_us"><?=$this->lang->line('lang_contact_us')?></a></li>
                <?php } ?>
                <li class="li-search"><a href="#" class="toggle-search"><i class="line-icon-magnifier"></i></a></li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle top-menu lang" data-toggle="dropdown" data-hover="dropdown" data-close-others="false"><?=$this->lang->line('lang_en')?></a>
                  <ul class="dropdown-menu">
                    <li><a data-lang="es" class="lang" href="#"><?=$this->lang->line('lang_ar')?></a></li>
                  </ul>
                </li>
            </ul>
        </div>
        <!-- Main Navigation menu ends-->
      </nav>
    </div>
  </div>
</div>