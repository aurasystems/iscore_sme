<div class="section-copyright bg-dark">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <p>Themes Lab All rights reserved &copy; 2015</p>
      </div>
      <div class="col-md-6">
        <p class="copyright-simple-menu">
          <span>Home</span>
          <span>Services</span>
          <span>Pricing</span>
          <span>Team</span>
          <span>Support</span>
        </p>
      </div>
    </div>
  </div>
</div>