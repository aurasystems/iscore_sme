<!-- Begin  Portfolio Single -->
<?php $settings= $this->db->get('settings')->row();?>
    <section class="section-portfolio portfolio-single clearfix p-t-0">
      <div class="portfolio">
        <div class="portfolio-thumbnail">
          <figure>
            <div class="owl-carousel">
              <div class="item">
                <div class="section-overlay"></div>
                <img src="<?=base_url()?>assets/common-files/images/gallery/25.jpg" alt="25">
              </div>
              <div class="item">
                <div class="section-overlay"></div>
                <img src="<?=base_url()?>assets/common-files/images/gallery/26.jpg" alt="26">
              </div>
              <div class="item">
                <div class="section-overlay"></div>
                <img src="<?=base_url()?>assets/common-files/images/gallery/27.jpg" alt="27">
              </div>
            </div>
          </figure>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="portfolio-text">
                <div class="portfolio-content">
                  <div class="row">
                    <div class="col-md-12">
                      <h3 class="section-title"><?=$settings->homeTitle?></h3>
                      <p><?=$settings->homeContent?></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
     <!--End  Portfolio Single -->