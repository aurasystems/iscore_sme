<title><?= MY_APP_NAME ?> | <?= $title ?></title>
<meta charset="utf-8">
<link rel="shortcut icon" href="<?=  base_url() ?>assets/front/images/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?=base_url()?>assets/front/css/style.css"/>
<link rel="stylesheet" href="<?=base_url()?>assets/front/css/ui.css"/>
<link href="<?=base_url()?>assets/front/css/custom.css" rel="stylesheet">
<link rel="stylesheet" id="scheme-source" href="<?=base_url()?>assets/front/css/schemes/gray.css" />
<!-- BEGIN PAGE STYLE -->
<link rel="stylesheet" href="<?=base_url()?>assets/front/plugins/magnific/magnific-popup.min.css" />
<!-- END PAGE STYLE -->
<!-- [if lt IE 9]>
    <script src="http://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="http://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif] -->