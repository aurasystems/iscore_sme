<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <?php $this->load->view('front/private/head'); ?>
    </head>
    <body class="demo-page">
        <!-- BEGIN PRELOADER -->
        <?php $this->load->view('front/private/preloader'); ?>
        <!-- END PRELOADER -->
        
        <!--  Begin Topbar simple -->
        <?php $this->load->view('front/private/topbar'); ?>
        <!--  End Topbar simple -->
        
        <!-- Header Parallax Image -->
        <?php $this->load->view('front/private/slider'); ?>
        <!-- End Header Parallax Image -->
        
        <!-- Begin Contact Form Left Map Right -->
        <section class="section-contact contact-wrap">
            <div class="container contact center">
                <div class="center section-title">
                    <h3 class="section-title"><?= $this->lang->line('lang_leave_msg') ?></h3>
                </div>
                <div class="col-md-8">
                    <div class="confirmation">
                        <p><span class="fa fa-check"></span></p>
                    </div>                  
                    <?php echo form_open('Contact_us/send_contact_us'); ?>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <?php echo form_input(array('id' => 'name', 'name' => 'name', 'class'=>'input-field form-item','size' => 30 ,'placeholder'=>'Name','required'=>'required')); ?>
                                <?php echo form_error('name'); ?>
                            </div>
                            <div class="form-group">
                                <?php echo form_input(array('type' => 'email','id' => 'email', 'name' => 'email', 'class'=>'input-field form-item','size' => 30 ,'placeholder'=>'Email','required'=>'required')); ?>
                                <?php echo form_error('email'); ?>
                            </div>
                            <div class="form-group">
                                <?php echo form_input(array('id' => 'subject', 'name' => 'subject', 'class'=>'input-field form-item','size' => 30 ,'placeholder'=>'Subject','required'=>'required')); ?>
                                <?php echo form_error('subject'); ?>
                            </div>
                            <div class="form-group">
                                <?php echo form_textarea(array('id' => 'message', 'name' => 'message', 'class'=>'textarea-field form-item field-message','rows' => 5 ,'required'=>'required')); ?>
                                <?php echo form_error('message'); ?>
                            </div>
                        </div>
                        <?php echo form_submit(array('id' => 'submit', 'name' => 'submit', 'class'=>'btn btn-dark btn-rounded hover-effect m-t-30','value' => $this->lang->line('lang_send_msg'))); ?>
                    <?php echo form_close(); ?>
                </div>
                <div class="col-md-4">
                    <h3 class="text-left section-title">Get in touch</h3>
                    <p class="contact-description text-left c-gray">
                        Make theme comes with more more than 90 pre-designs demo pages.
                        Want custom variation? Create your own page with builder.
                    </p>
                </div>
            </div>
        </section>
        <!-- End Contact Form Left Map Right -->
        
        <!-- Begin Footer 3 columns Dark -->
        <?php $this->load->view('front/private/footer_columns'); ?>
        <!-- End Footer 3 columns Dark -->
        
        <!-- Begin Copyright Dark -->
        <?php $this->load->view('front/private/copyright'); ?>
        <!-- End Copyright Dark -->
        
        <?php $this->load->view('front/private/footer'); ?>
    </body>
</html>