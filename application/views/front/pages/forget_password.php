<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <?php $this->load->view('front/private/head'); ?>
    </head>
    <body class="demo-page">
        <!-- BEGIN PRELOADER -->
        <?php $this->load->view('front/private/preloader'); ?>
        <!-- END PRELOADER -->
        
        <!--  Begin Topbar simple -->
        <?php $this->load->view('front/private/topbar'); ?>
        <!--  End Topbar simple -->
        
        <!-- Header Parallax Image -->
        <?php $this->load->view('front/private/slider'); ?>
        <!-- End Header Parallax Image -->
        
        <!-- Begin User Login -->
        <section class="section-account container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <h2 class="text-center"><?= $this->lang->line('lang_forget_password') ?></h2>
                    <div class="row editErrorMsg">
                        <?php if($this->session->flashdata('reset_password_msg')){ ?>
                        <?php echo $this->session->flashdata('reset_password_msg'); }?>
                    </div>
                    <?php echo form_open('Client/do_password_reset'); ?>
                        <div class="form-group">
                            <p class="center"><?= $this->lang->line('lang_receive_msg') ?></p>
                            <div class="append-icon">
                                <?php echo form_input(array('type' => 'email','id' => 'email', 'name' => 'email', 'class'=>'form-control form-white email' ,'placeholder'=>'Enter your email...','required'=>'required')); ?>
                                <i class="icon-envelope"></i>
                            </div>
                        </div>
                        <div class="text-center m-t-20">
                            <button type="submit" class="btn btn-animated btn-dark btn-login"><?= $this->lang->line('lang_send_me_pass') ?> <i class="fa fa-chevron-right"></i></button>
                        </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </section>
        <!-- End User Login -->
        
        <!-- Begin Footer 3 columns Dark -->
        <?php $this->load->view('front/private/footer_columns'); ?>
        <!-- End Footer 3 columns Dark -->
        
        <!-- Begin Copyright Dark -->
        <?php $this->load->view('front/private/copyright'); ?>
        <!-- End Copyright Dark -->
        
        <?php $this->load->view('front/private/footer'); ?>
    </body>
</html>