<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <?php $this->load->view('front/private/head'); ?>
    </head>
    <body class="demo-page">
        <!-- BEGIN PRELOADER -->
        <?php $this->load->view('front/private/preloader'); ?>
        <!-- END PRELOADER -->
        
        <!--  Begin Topbar simple -->
        <?php $this->load->view('front/private/topbar'); ?>
        <!--  End Topbar simple -->
        
        <!-- Header Parallax Image -->
        <?php $this->load->view('front/private/slider'); ?>
        <!-- End Header Parallax Image -->
        
        <!-- Begin Portfolio Boxed 4 columns with Space -->
        <section class="section-call-action bg-gray-light">
            <div class="container center">
              <div class="row">
                <div class="col-md-12">
                  <h3 class="section-title">Want to use this template for your next project?</h3>
                </div>
                <div class="col-md-12">
                  <a href="http://themeforest.net/item/make-multipurpose-onemultipage-theme/11399294?ref=themes-lab" class="btn btn-primary btn-rounded hover-effect">Buy Template</a>
                </div>
              </div>
            </div>
        </section>
        <!-- End Portfolio Boxed 4 columns with Space -->
        
        <!-- Begin Footer 3 columns Dark -->
        <?php $this->load->view('front/private/footer_columns'); ?>
        <!-- End Footer 3 columns Dark -->
        
        <!-- Begin Copyright Dark -->
        <?php $this->load->view('front/private/copyright'); ?>
        <!-- End Copyright Dark -->
        
        <?php $this->load->view('front/private/footer'); ?>
    </body>
</html>