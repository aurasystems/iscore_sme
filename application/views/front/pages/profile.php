<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <?php $this->load->view('front/private/head'); ?>
    </head>
    <body class="demo-page">
        <!-- BEGIN PRELOADER -->
        <?php $this->load->view('front/private/preloader'); ?>
        <!-- END PRELOADER -->
        
        <!--  Begin Topbar simple -->
        <?php $this->load->view('front/private/topbar'); ?>
        <!--  End Topbar simple -->
        
        <!-- Header Parallax Image -->
        <?php $this->load->view('front/private/slider'); ?>
        <!-- End Header Parallax Image -->
        
        
        <!-- Begin User Register -->
        <section class="section-account container">
            <h2 class="text-center m-t-0 m-b-40"><?=$this->lang->line('lang_client_profile')?></h2>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <?php if($status == "success"){?>
                    <div class="alert alert-block alert-success fade in">
                        <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                        <p></p><h4><i class="fa fa-check"></i> <?=$this->lang->line('lang_success')?></h4> <?=$this->lang->line('atumsl_alertsettingssuccess')?><p></p>
                    </div>
                    <?php }?>
                    <?php echo form_open('Client/do_update_client'); ?>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?php echo form_label($this->lang->line('lang_first_name').':','lang_first_name'); ?> 
                                <div class="append-icon">
                                    <?php
                                    if($client_data)
                                    {
                                        $first_name = $client_data->first_name;
                                    }
                                    else
                                    {
                                        $first_name = '';
                                    }
                                    ?>
                                    <?php echo form_input(array('id' => 'first_name', 'name' => 'first_name', 'class'=>'form-control form-white', 'placeholder'=>'FIRST NAME','required' => 'required' ,'value'=>$first_name)); ?>
                                    <i class="icon-user"></i>
                                    <?php echo form_error('first_name'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?php echo form_label($this->lang->line('lang_last_name').':','lang_last_name'); ?> 
                                <div class="append-icon">
                                    <?php
                                    if($client_data)
                                    {
                                        $last_name = $client_data->last_name;
                                    }
                                    else
                                    {
                                        $last_name = '';
                                    }
                                    ?>
                                    <?php echo form_input(array('id' => 'last_name', 'name' => 'last_name', 'class'=>'form-control form-white', 'placeholder'=>'LAST NAME','required' => 'required' ,'value'=>$last_name)); ?>
                                    <i class="icon-user"></i>
                                    <?php echo form_error('last_name'); ?>
                                </div>
                            </div>
                        </div>
                    </div><div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?php echo form_label($this->lang->line('lang_display_name').':','lang_display_name'); ?> 
                                <div class="append-icon">
                                    <?php
                                    if($client_data)
                                    {
                                        $display_name = $client_data->display_name;
                                    }
                                    else
                                    {
                                        $display_name = '';
                                    }
                                    ?>
                                    <?php echo form_input(array('id' => 'display_name', 'name' => 'display_name', 'class'=>'form-control form-white', 'placeholder'=>'DISPLAY NAME','required' => 'required' ,'value'=>$display_name)); ?>
                                    <i class="icon-user"></i>
                                    <?php echo form_error('display_name'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?php echo form_label($this->lang->line('lang_phone').':','lang_phone'); ?> 
                                <div class="append-icon">
                                    <?php
                                    if($client_data)
                                    {
                                        $phone = $client_data->phone;
                                    }
                                    else
                                    {
                                        $phone = '';
                                    }
                                    ?>
                                    <?php echo form_input(array('id' => 'phone', 'name' => 'phone', 'class'=>'form-control form-white', 'placeholder'=>'PHONE','required' => 'required' ,'value'=>$phone)); ?>
                                    <i class="icon-user"></i>
                                    <?php echo form_error('phone'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?php echo form_label($this->lang->line('lang_email_address').':','lang_email_address'); ?>
                                <div class="append-icon">
                                    <?php
                                    if($client_data)
                                    {
                                        $email = $client_data->email;
                                    }
                                    else
                                    {
                                        $email = '';
                                    }
                                    ?>
                                    <?php echo form_input(array('type' => 'email','id' => 'email', 'name' => 'email', 'class'=>'form-control form-white', 'placeholder'=>'ENTER YOUR EMAIL...','required' => 'required' ,'value'=>$email)); ?>
                                    <i class="icon-envelope"></i>
                                    <?php echo form_error('email'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?php echo form_label($this->lang->line('lang_address').':','lang_address'); ?>
                                <div class="append-icon">
                                    <?php
                                    if($client_data)
                                    {
                                        $address = $client_data->address;
                                    }
                                    else
                                    {
                                        $address = '';
                                    }
                                    ?>
                                    <?php echo form_input(array('id' => 'address', 'name' => 'address', 'class'=>'form-control form-white', 'placeholder'=>'ADDRESS','required' => 'required' ,'value'=>$address)); ?>
                                    <i class="icon-lock"></i>
                                    <?php echo form_error('address'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?php echo form_label($this->lang->line('lang_password').':','lang_password'); ?>
                                <div class="append-icon">
                                    <?php echo form_input(array('type' => 'password','id' => 'password' ,'name' => 'password', 'class'=>'form-control form-white')); ?>
                                    <i class="icon-lock"></i>
                                    <?php echo form_error('password'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?php echo form_label($this->lang->line('lang_repeat_password').':','lang_repeat_password'); ?>
                                <div class="append-icon">
                                    <?php echo form_input(array('type' => 'password','id' => 'password2' ,'name' => 'password2', 'class'=>'form-control form-white')); ?>
                                    <i class="icon-lock"></i>
                                    <?php echo form_error('password2'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center  m-t-20">
                        <button type="submit" class="btn btn-animated btn-dark pull-left btn-signup"><?=$this->lang->line('lang_save')?><i class="fa fa-chevron-right"></i></button>
                    </div>
                <?php echo form_close(); ?>
                </div>
            </div>
        </section>
        <!-- End User Register -->

        <!-- Begin Footer 3 columns Dark -->
        <?php $this->load->view('front/private/footer_columns'); ?>
        <!-- End Footer 3 columns Dark -->
        
        <!-- Begin Copyright Dark -->
        <?php $this->load->view('front/private/copyright'); ?>
        <!-- End Copyright Dark -->
        
        <?php $this->load->view('front/private/footer'); ?>
    </body>
</html>