<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <?php $this->load->view('front/private/head'); ?>
    </head>
    <body class="demo-page">
        <!-- BEGIN PRELOADER -->
        <?php $this->load->view('front/private/preloader'); ?>
        <!-- END PRELOADER -->
        
        <!--  Begin Topbar simple -->
        <?php $this->load->view('front/private/topbar'); ?>
        <!--  End Topbar simple -->
        
        <!-- Header Parallax Image -->
        <?php $this->load->view('front/private/slider'); ?>
        <!-- End Header Parallax Image -->
        
        <!-- Begin User Login -->
    <section class="section-account container">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <h2 class="text-center"><?= $this->lang->line('lang_login') ?></h2>
                <div class="row editErrorMsg">
                    <?php if($this->session->flashdata('login_msg')){
                        echo $this->session->flashdata('login_msg'); }?>
                </div>
                <?php echo form_open('Login/do_login'); ?>
                <div class="form-group">
                    <div class="append-icon">
                        <?php echo form_label($this->lang->line('lang_email_address').':','lang_email_address'); ?> 
                        <?php echo form_input(array('type' => 'email','id' => 'email', 'name' => 'email', 'class'=>'form-control form-white','required'=>'required')); ?>
                        <i class="icon-envelope"></i>
                        <?php echo form_error('email'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="append-icon">
                        <?php echo form_label($this->lang->line('lang_password').':','lang_password'); ?> 
                        <?php echo form_input(array('type' => 'password', 'name' => 'password', 'class'=>'form-control form-white','required'=>'required')); ?>
                        <i class="icon-lock"></i>
                        <?php echo form_error('password'); ?>
                    </div>
                    <?php echo form_submit(array('class'=>'btn btn-lg btn-danger btn-block ladda-button','data-style' => 'expand-left','value' => $this->lang->line('lang_sign_in'))); ?>
                    <div class="clearfix">
                        <p class="pull-left m-t-20"><a href="<?= base_url() ?>Client/reset_password"><?= $this->lang->line('lang_forget_password') ?></a></p>
                        <p class="pull-right m-t-20"><a href="<?= base_url() ?>Register"><?= $this->lang->line('lang_sign_up') ?></a></p>
                    </div>
                </div>
            <?php echo form_close(); ?>
            </div>
        </div>
    </section>
    <!-- End User Login -->
        
        <!-- Begin Footer 3 columns Dark -->
        <?php $this->load->view('front/private/footer_columns'); ?>
        <!-- End Footer 3 columns Dark -->
        
        <!-- Begin Copyright Dark -->
        <?php $this->load->view('front/private/copyright'); ?>
        <!-- End Copyright Dark -->
        
        <?php $this->load->view('front/private/footer'); ?>
    </body>
</html>