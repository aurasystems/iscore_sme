<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="header">
                        <div class="breadcrumb-wrapper editWrapper">
                            <ol class="breadcrumb">
                                <li><a href="<?= base_url() ?>admin/Dashboard"><?= $this->lang->line('lang_dashboard') ?></a></li>
                                <li class="active"><?= $this->lang->line('lang_back_settings') ?></li>
                            </ol>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php if ($status == "success") { ?>
                                <div class="alert alert-block alert-success fade in">
                                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                    <p></p><h4><i class="fa fa-check"></i> <?= $this->lang->line('lang_success') ?></h4> <?= $this->lang->line('atumsl_alertsettingssuccess') ?><p></p>
                                </div>
                            <?php } ?>
                            <div class="panel panel-default">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><i class="fa fa-gear"></i> <?= $this->lang->line("lang_back_settings") ?></h2>
                                </div>

                                <div class="panel-body">
                                    <div class="col-md-6">
                                        <?php
                                        echo form_open_multipart('admin/Back_settings/update', $attributes);
                                        ?>
                                        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                                        <div class="form-group">
                                            <?php echo form_label($this->lang->line('lang_system_logo') . ':', 'lang_system_logo', $label_att); ?>
                                            <div class="col-sm-7 controls">
                                                <?php if ($back_settings) { ?>
                                                    <img src="<?= base_url() ?>uploads/settings/back/<?= $back_settings->system_logo ?>" alt="<?= $back_settings->system_name ?>" class="img-responsive thumbnail" width="100" >
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo form_label($this->lang->line('lang_system_name') . ':', 'lang_system_name', $label_att); ?>
                                            <div class="col-sm-7">
                                                <?php
                                                if ($back_settings) {
                                                    $system_name = $back_settings->system_name;
                                                } else {
                                                    $system_name = '';
                                                }
                                                ?>
                                                <?php echo form_input(array('id' => 'system_name', 'name' => 'system_name', 'class' => 'form-control', 'placeholder' => 'System Name', 'value' => $system_name)); ?>
                                                <?php echo form_error('system_name'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo form_label($this->lang->line('lang_system_logo') . ':', 'lang_system_logo', $label_att); ?> 
                                            <div class="col-sm-7">
                                                <input type="file" name="userfile" id="userfile" size="20" class="form-control"/>
                                                <?php echo form_error('userfile'); ?>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                    <?php if (substr($this->session->userdata('levels')->settings, 2, 1) != '0') { ?>
                                        <div class="pull-right">
                                            <?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('lang_save'), 'class' => 'btn btn-primary')); ?>
                                            <span class="btn btn-default btn-mini"><a style="text-decoration: none;" href="<?= base_url() ?>admin/Dashboard"><?= $this->lang->line('lang_cancel') ?></a></span>
                                        </div>
                                    <?php } ?>
                                    <?php echo form_close(); ?>
                                </div>

                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
    </body>
</html>