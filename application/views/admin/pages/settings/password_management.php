<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <style>
            .icheckbox_minimal-grey{
                margin-top: 12px;
            }
        </style>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="header">
                        <div class="breadcrumb-wrapper editWrapper">
                            <ol class="breadcrumb">
                                <li><a href="<?= base_url() ?>admin/Dashboard"><?= $this->lang->line('lang_dashboard') ?></a></li>
                                <li class="active"><?= $this->lang->line('password_management') ?></li>
                            </ol>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("success")) { ?>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="alert alert-success"><?= $this->session->flashdata("success") ?></div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata("error")) { ?>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="alert alert-danger"><?= $this->session->flashdata("error") ?></div>
                                    </div>
                                </div>
                            <?php } ?>

                            <div class="panel panel-default">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><i class="fa fa-gear"></i> <?= $this->lang->line("password_management") ?></h2>
                                </div>

                                <div class="panel-body">
                                    <form id="form" method="POST" action="" role="form" class="form-horizontal form-validation">
                                        <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="<?= $label_att ?>"><?= $this->lang->line("enforce_history") ?>
                                                    <i class="icon-info" rel="popover" data-container="body" data-toggle="popover" data-placement="right" data-content="<?= $this->lang->line('enforce_password_history_hint') ?>" data-original-title="Support"></i>
                                                </label>
                                                <div class="col-sm-6">
                                                    <input type="checkbox" name="enforce_history" class="form-control" value="1" <?= (isset($row->enforce_history) && $row->enforce_history == 1) || $this->input->post("enforce_history") == 1 ? "checked" : "" ?> >
                                                    <span class="c-red"><?= form_error("enforce_history") ?></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="<?= $label_att ?>"><?= $this->lang->line("complex_password") ?>
                                                    <i class="icon-info" rel="popover" data-container="body" data-toggle="popover" data-placement="right" data-content="<?= $this->lang->line('complex_password_hint') ?>" data-original-title="Support"></i>
                                                </label>
                                                <div class="col-sm-6">
                                                    <input type="checkbox" name="complex" class="form-control" value="1" <?= (isset($row->complex) && $row->complex == 1) || $this->input->post("complex") == 1 ? "checked" : "" ?> >
                                                    <span class="c-red"><?= form_error("complex") ?></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="<?= $label_att ?>"><?= $this->lang->line("max_age") ?>
                                                    <i class="icon-info" rel="popover" data-container="body" data-toggle="popover" data-placement="right" data-content="<?= $this->lang->line('max_age_hint') ?>" data-original-title="Support"></i>
                                                </label>
                                                <div class="col-sm-6">
                                                    <input type="text" name="max_age" class="form-control" value="<?= isset($row->max_age) ? $row->max_age : $this->input->post("max_age") ?>" >
                                                    <span class="c-red"><?= form_error("max_age") ?></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="<?= $label_att ?>"><?= $this->lang->line("min_length") ?>
                                                    <i class="icon-info" rel="popover" data-container="body" data-toggle="popover" data-placement="right" data-content="<?= $this->lang->line('min_length_hint') ?>" data-original-title="Support"></i>
                                                </label>
                                                <div class="col-sm-6">
                                                    <input type="text" name="min_length" class="form-control" value="<?= isset($row->min_length) ? $row->min_length : $this->input->post("min_length") ?>" >
                                                    <span class="c-red"><?= form_error("min_length") ?></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="<?= $label_att ?>"><?= $this->lang->line("lockout_duration") ?>
                                                    <i class="icon-info" rel="popover" data-container="body" data-toggle="popover" data-placement="right" data-content="<?= $this->lang->line('lockout_duration_hint') ?>" data-original-title="Support"></i>
                                                </label>
                                                <div class="col-sm-6">
                                                    <input type="text" name="lockout_duration" class="form-control" value="<?= isset($row->lockout_duration) ? $row->lockout_duration : $this->input->post("lockout_duration") ?>" >
                                                    <span class="c-red"><?= form_error("lockout_duration") ?></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="<?= $label_att ?>"><?= $this->lang->line("lockout_threshold") ?>
                                                    <i class="icon-info" rel="popover" data-container="body" data-toggle="popover" data-placement="right" data-content="<?= $this->lang->line('lockout_threshold_hint') ?>" data-original-title="Support"></i>
                                                </label>
                                                <div class="col-sm-6">
                                                    <input type="text" name="lockout_threshold" class="form-control" value="<?= isset($row->lockout_threshold) ? $row->lockout_threshold : $this->input->post("lockout_threshold") ?>" >
                                                    <span class="c-red"><?= form_error("lockout_threshold") ?></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="<?= $label_att ?>"><?= $this->lang->line("reset_counter") ?>
                                                    <i class="icon-info" rel="popover" data-container="body" data-toggle="popover" data-placement="right" data-content="<?= $this->lang->line('reset_counter_hint') ?>" data-original-title="Support"></i>
                                                </label>
                                                <div class="col-sm-6">
                                                    <input type="text" name="reset_counter" class="form-control" value="<?= isset($row->reset_counter) ? $row->reset_counter : $this->input->post("reset_counter") ?>" >
                                                    <span class="c-red"><?= form_error("reset_counter") ?></span>
                                                </div>
                                            </div>

                                        </div>

                                        <?php
                                        if (substr($this->session->userdata('levels')->settings, 2, 1) != '0' ||
                                                substr($this->session->userdata('levels')->settings, 1, 1) != '0') {
                                            ?>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="pull-right">
                                                        <button type="submit" class="btn btn-embossed btn-primary m-r-20"><?= $this->lang->line("save") ?></button>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
    </body>
</html>