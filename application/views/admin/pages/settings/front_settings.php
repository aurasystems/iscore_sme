<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?=base_url()?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="header">
                        <div class="breadcrumb-wrapper editWrapper">
                            <ol class="breadcrumb">
                                <li><a href="<?= base_url() ?>admin/Dashboard"><?=$this->lang->line('lang_dashboard')?></a></li>
                                <li class="active"><?=$this->lang->line('lang_front_settings')?></li>
                            </ol>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php if($status == "success"){?>
                            <div class="alert alert-block alert-success fade in">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                <p></p><h4><i class="fa fa-check"></i> <?=$this->lang->line('lang_success')?></h4> <?=$this->lang->line('atumsl_alertsettingssuccess')?><p></p>
                            </div>
                            <?php }?>
                            
                                <div class="box-title editBoxTitle">
                                    <h4><i class="fa fa-globe"></i> <?=$this->lang->line('lang_front_settings');?></h4>
                                </div>
                                <div class="box-body big col-md-6">
                                    <?php
                                    echo form_open_multipart('admin/Front_settings/update', $attributes);
                                    ?>
                                    <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_front_logo').':','lang_front_logo',$label_att); ?>
                                        <div class="col-sm-7 controls">
                                            <?php if($front_settings){ ?>
                                            <img src="<?=base_url()?>uploads/settings/front/<?= $front_settings->front_logo ?>" alt="<?= $front_settings->title ?>" class="img-responsive thumbnail" width="100" >
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_front_logo').':','lang_front_logo',$label_att); ?> 
                                        <div class="col-sm-7">
                                            <?php echo form_input(array('type' => 'file','name' => 'userfile', 'class' => 'form-control' ,'size' => 20)); ?>
                                            <?php echo form_error('userfile'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_front_title').':','lang_front_title',$label_att); ?>
                                        <div class="col-sm-7">
                                            <?php
                                            if($front_settings)
                                            {
                                                $front_title = $front_settings->title;
                                            }
                                            else
                                            {
                                                $front_title = '';
                                            }
                                            ?>
                                            <?php echo form_input(array('id' => 'title', 'name' => 'title', 'class'=>'form-control', 'placeholder'=>'Title', 'value'=>$front_title)); ?>
                                            <?php echo form_error('title'); ?>
                                        </div>
                                    </div>
                                    <h3><?= $this->lang->line('lang_company_info') ?></h3>
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_company_info').':','lang_company_info',$label_att); ?>
                                        <div class="col-sm-7">
                                            <?php
                                            if($front_settings)
                                            {
                                                $company_info = $front_settings->company_info;
                                            }
                                            else
                                            {
                                                $company_info = '';
                                            }
                                            ?>
                                            <?php echo form_textarea(array('id' => 'company_info', 'name' => 'company_info', 'class'=>'form-control', 'placeholder'=>'Company Info',  'rows' => '5', 'cols' => '10', 'value'=>$company_info)); ?>
                                            <?php echo form_error('company_info'); ?>
                                        </div>
                                    </div>
                                    <h3><?= $this->lang->line('lang_contact_info') ?></h3>
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_address').':','lang_address',$label_att); ?>
                                        <div class="col-sm-7">
                                            <?php
                                            if($front_settings)
                                            {
                                                $address = $front_settings->address;
                                            }
                                            else
                                            {
                                                $address = '';
                                            }
                                            ?>
                                            <?php echo form_input(array('id' => 'address', 'name' => 'address', 'class'=>'form-control', 'placeholder'=>'Address', 'value'=>$address)); ?>
                                            <?php echo form_error('address'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_phone').':','lang_phone',$label_att); ?>
                                        <div class="col-sm-7">
                                            <?php
                                            if($front_settings)
                                            {
                                                $phone = $front_settings->phone;
                                            }
                                            else
                                            {
                                                $phone = '';
                                            }
                                            ?>
                                            <?php echo form_input(array('id' => 'phone', 'name' => 'phone', 'class'=>'form-control', 'placeholder'=>'Phone', 'value'=>$phone)); ?>
                                            <?php echo form_error('phone'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_email_address').':','lang_email_address',$label_att); ?>
                                        <div class="col-sm-7">
                                            <?php
                                            if($front_settings)
                                            {
                                                $email = $front_settings->email;
                                            }
                                            else
                                            {
                                                $email = '';
                                            }
                                            ?>
                                            <?php echo form_input(array('id' => 'email', 'name' => 'email', 'class'=>'form-control', 'placeholder'=>'Email', 'value'=>$email)); ?>
                                            <?php echo form_error('email'); ?>
                                        </div>
                                    </div>
                                    <h3><?= $this->lang->line('lang_social_media') ?></h3>
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_facebook').':','lang_facebook',$label_att); ?>
                                        <div class="col-sm-7">
                                            <?php
                                            if($front_settings)
                                            {
                                                $facebook = $front_settings->facebook_url;
                                            }
                                            else
                                            {
                                                $facebook = '';
                                            }
                                            ?>
                                            <?php echo form_input(array('id' => 'facebook', 'name' => 'facebook', 'class'=>'form-control', 'placeholder'=>'Facebook', 'value'=>$facebook)); ?>
                                            <?php echo form_error('facebook'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_twitter').':','lang_twitter',$label_att); ?>
                                        <div class="col-sm-7">
                                            <?php
                                            if($front_settings)
                                            {
                                                $twitter = $front_settings->twitter_url;
                                            }
                                            else
                                            {
                                                $twitter = '';
                                            }
                                            ?>
                                            <?php echo form_input(array('id' => 'twitter', 'name' => 'twitter', 'class'=>'form-control', 'placeholder'=>'Twitter', 'value'=>$twitter)); ?>
                                            <?php echo form_error('twitter'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_google_plus').':','lang_google_plus',$label_att); ?>
                                        <div class="col-sm-7">
                                            <?php
                                            if($front_settings)
                                            {
                                                $google_plus = $front_settings->google_plus_url;
                                            }
                                            else
                                            {
                                                $google_plus = '';
                                            }
                                            ?>
                                            <?php echo form_input(array('id' => 'google_plus', 'name' => 'google_plus', 'class'=>'form-control', 'placeholder'=>'Google Plus', 'value'=>$google_plus)); ?>
                                            <?php echo form_error('google_plus'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_pinterest').':','lang_pinterest',$label_att); ?>
                                        <div class="col-sm-7">
                                            <?php
                                            if($front_settings)
                                            {
                                                $pinterest = $front_settings->pinterest_url;
                                            }
                                            else
                                            {
                                                $pinterest = '';
                                            }
                                            ?>
                                            <?php echo form_input(array('id' => 'pinterest', 'name' => 'pinterest', 'class'=>'form-control', 'placeholder'=>'Pinterest', 'value'=>$pinterest)); ?>
                                            <?php echo form_error('pinterest'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_flickr').':','lang_flickr',$label_att); ?>
                                        <div class="col-sm-7">
                                            <?php
                                            if($front_settings)
                                            {
                                                $flickr = $front_settings->flickr_url;
                                            }
                                            else
                                            {
                                                $flickr = '';
                                            }
                                            ?>
                                            <?php echo form_input(array('id' => 'flickr', 'name' => 'flickr', 'class'=>'form-control', 'placeholder'=>'Flickr', 'value'=>$flickr)); ?>
                                            <?php echo form_error('flickr'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_linkedin').':','lang_linkedin',$label_att); ?>
                                        <div class="col-sm-7">
                                            <?php
                                            if($front_settings)
                                            {
                                                $linkedin = $front_settings->linkedin_url;
                                            }
                                            else
                                            {
                                                $linkedin = '';
                                            }
                                            ?>
                                            <?php echo form_input(array('id' => 'linkedin', 'name' => 'linkedin', 'class'=>'form-control', 'placeholder'=>'Linkedin', 'value'=>$linkedin)); ?>
                                            <?php echo form_error('linkedin'); ?>
                                        </div>
                                    </div>
                                    
                                    <?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('lang_save'), 'class'=>'btn btn-success')); ?>
                                    <span class="btn btn-default btn-mini"><a style="text-decoration: none;" href="<?=base_url()?>admin/Dashboard"><?=$this->lang->line('lang_cancel')?></a></span>
                                    <?php echo form_close(); ?>
                                </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
    </body>
</html>