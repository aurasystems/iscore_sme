<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <style>
            .icheckbox_minimal-grey{
                margin-top: 12px;
            }
        </style>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="header">
                        <div class="breadcrumb-wrapper editWrapper">
                            <ol class="breadcrumb">
                                <li><a href="<?= base_url() ?>admin/Dashboard"><?= $this->lang->line('lang_dashboard') ?></a></li>
                                <li class="active"><?= $this->lang->line('audit_trail_settings') ?></li>
                            </ol>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("success")) { ?>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="alert alert-success"><?= $this->session->flashdata("success") ?></div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata("error")) { ?>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="alert alert-danger"><?= $this->session->flashdata("error") ?></div>
                                    </div>
                                </div>
                            <?php } ?>

                            <div class="panel panel-default">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><i class="fa fa-gear"></i> <?= $this->lang->line("audit_trail_settings") ?></h2>
                                </div>

                                <div class="panel-body">
                                    <form id="form" method="POST" action="" role="form" class="form-horizontal form-validation">
                                        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="<?= $label_att ?>"><?= $this->lang->line("audits_expiry") ?>
                                                <i class="icon-info" rel="popover" data-container="body" data-toggle="popover" data-placement="top" data-content="If set to 0 purge will be disabled" data-original-title="Info"></i>
                                                </label>
                                                <div class="col-sm-6">
                                                    <input type="text" name="audits_expiry" class="form-control" value="<?= $settings ? $settings->audits_expiry : '' ?>" />
                                                    <span class="c-red"><?= form_error("audits_expiry") ?></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="<?= $label_att ?>"><?= $this->lang->line("audits_file_path") ?></label>
                                                <div class="col-sm-6">
                                                    <input type="text" name="audits_file_path" class="form-control" value="<?= $settings ? $settings->audits_file_path : '' ?>" /> 
                                                    <span class="c-red"><?= form_error("audits_file_path") ?></span>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-6 m-t-10">
                                            <?= $this->lang->line('days') ?>
                                        </div>

                                        <?php
                                        if (substr($this->session->userdata('levels')->settings, 2, 1) != '0' ||
                                                substr($this->session->userdata('levels')->settings, 1, 1) != '0') {
                                            ?>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="pull-right">
                                                        <button type="submit" class="btn btn-embossed btn-primary m-r-20"><?= $this->lang->line("save") ?></button>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
    </body>
</html>