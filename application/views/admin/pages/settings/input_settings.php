<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <style>
            .icheckbox_minimal-grey{
                margin-top: 12px;
            }
        </style>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="header">
                        <div class="breadcrumb-wrapper editWrapper">
                            <ol class="breadcrumb">
                                <li><a href="<?= base_url() ?>admin/Dashboard"><?= $this->lang->line('lang_dashboard') ?></a></li>
                                <li class="active"><?= $this->lang->line('input_settings') ?></li>
                            </ol>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("success")) { ?>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="alert alert-success"><?= $this->session->flashdata("success") ?></div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata("error")) { ?>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="alert alert-danger"><?= $this->session->flashdata("error") ?></div>
                                    </div>
                                </div>
                            <?php } ?>

                            <div class="panel panel-default">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><i class="fa fa-gear"></i> <?= $this->lang->line("input_settings") ?></h2>
                                </div>

                                <div class="panel-body">
                                    <fieldset>
                                        <legend><?= $this->lang->line('borrower_information') ?></legend>

                                        <ul style="margin-left: 5%">
                                            <?php
                                            $i = 6;
                                            while ($i < 12) {
                                                ?>
                                                <li><a href="<?= base_url() ?>admin/Master/c<?= $i ?>"><?= $this->lang->line("borrower_information_C$i") . ' ' . $this->lang->line('options') ?></a></li>
                                                <?php
                                                if ($i == 6) {
                                                    $i+=3;
                                                } else {
                                                    $i++;
                                                }
                                            }
                                            ?>
                                                <li><a href="<?= base_url() ?>admin/Master/E7"><?= $this->lang->line("borrower_information_E7") . ' ' . $this->lang->line('options') ?></a></li>
                                        </ul>

                                    </fieldset>
                                    <br/>
                                    <fieldset>
                                        <legend><?= $this->lang->line('borrower_assessment') ?></legend>

                                        <ul style="margin-left: 5%">
                                            <?php
                                            for ($i = 5; $i <= 216; $i++) {
                                                if ($this->lang->line("borrower_assessment_F$i") != '') {
                                                    if ($i == 4 || $i == 17 || $i == 120 || $i == 132 || $i == 184 || $i == 214) {
                                                        continue;
                                                    }
                                                    ?>
                                                    <li><a href="<?= base_url() ?>admin/Master/f<?= $i ?>"><?= $this->lang->line("borrower_assessment_F$i") . ' ' . $this->lang->line('options') ?></a></li>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>

                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
    </body>
</html>