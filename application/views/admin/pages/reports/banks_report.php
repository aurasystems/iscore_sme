<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/plugins/export/jquery.dataTables.min.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/plugins/export/Buttons-1.5.6/css/buttons.dataTables.min.css" rel="stylesheet">
        <style>
            .ui-datepicker{
                z-index: 1000 !important;
            }
            table.dataTable thead th, table.dataTable thead td {
                border-bottom-color: #ddd;
            }
            #table_wrapper{
                overflow: auto;
            }
        </style>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $this->lang->line("banks_report") ?></h2>
                                </div>
                                <div class="panel-content">

                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">

<form id="form" method="POST" action="" role="form" class="form-horizontal form-validation">
                                                <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
                                                <h4><strong><?= $this->lang->line('filter') ?> :</strong></h4>
                                               
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="<?= $label_att ?>"><?= $this->lang->line("count_method") ?></label>
                                                            <div class="col-sm-9">
                                                                <select  name="count_method" id="count_method" class="form-control">
                                                                    <option value><?= $this->lang->line('select') ?></option>
                                                                    <option value="all" <?=$this->input->post("count_method")=="all"?"selected":""?> ><?= $this->lang->line('all_cases') ?></option>
                                                                    <option value="same_reg_num" <?=$this->input->post("count_method")=="same_reg_num"?"selected":""?> ><?= $this->lang->line('combine_same_registry_num') ?></option>
                                                                   
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                              
                                                
                                                <div class="pull-right">
                                                        <div class="col-md-4">
                                                            <button type="submit" class="btn btn-sm btn-blue filter"><?= $this->lang->line('filter') ?></button>
                                                        </div>
                                                </div>
                                                
                                                </form>
												<div class="clearfix"></div>
<br/>
<br/>


<!--<table class="table table-hover table-dynamic table-tools">-->
                                            <table id="table"  class="reports-table">
                                                <thead>
                                                    <tr>
                                                        <th><?= $this->lang->line('bank') ?></th>
                                                        <th><?= $this->lang->line('status') ?></th>
														<th class="text-center"><?= $this->lang->line('cases_entries') ?></th>
														<?php if(!$this->input->post() || $this->input->post('count_method')=='all'){ ?>
															<th class="text-center"><?= $this->lang->line('not_completed') ?></th>
															<th class="text-center"><?= $this->lang->line('waiting_approval') ?></th>
															<th class="text-center"><?= $this->lang->line('rejected_actually_lang') ?></th>
														<?php } ?>

														<th class="text-center"><?= $this->lang->line('completed_actually_lang') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($report_data as $case) { ?>
                                                        <tr>
                                                            <td><?= $case->name ?></td>
                                                            <td><?= $case->non_subscriber ? $this->lang->line('non_subscribed') : $this->lang->line('subscribed') ?></td>
															<td class="text-center"><?= $case->CASES_ENTRIES ?></td>
															<?php if(!$this->input->post() || $this->input->post('count_method')=='all'){ ?>
																<td class="text-center"><?= $case->NOT_COMPLETED ?></td>
																<td class="text-center"><?= $case->AWAITING_APPROVE ?></td>
																<td class="text-center"><?= $case->REJECTED ?></td>
															<?php } ?>
															<td class="text-center"><?= $case->COMPLETED ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>

        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->

        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script>

        <script src="<?= base_url() ?>assets/plugins/export/jquery.dataTables.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/export/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/export/Buttons-1.5.6/js/buttons.flash.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/export/jszip.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/export/pdfmake.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/export/vfs_fonts.js"></script>
        <script src="<?= base_url() ?>assets/plugins/export/Buttons-1.5.6/js/buttons.html5.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/export/Buttons-1.5.6/js/buttons.print.min.js"></script>
        <script>
            $(document).ready(function () {

                $('.reports-table').each(function () {
                    var opt = {
                        language: {search: ""},
                        dom: 'Bfrtip',
                        buttons: [
//             'pdfFlash','copy', 'csv', 'excel', 'pdf', 'print'
                            'excel',
                            {extend: 'pdf',
//        text: 'Selected Excel',
                                orientation: 'landscape',
                                pageSize: 'LEGAL',
                                exportOptions: {
                                    modifier: {
                                        selected: true
                                    }
                                }
                            },
                        ],
                        "pageLength": 25};
                    // Tools: export to Excel, CSV, PDF & Print
//            if ($(this).hasClass('table-tools')) {
//                opt.sDom = "<'row'<'col-md-6'f><'col-md-6'T>r>t<'row'<'col-md-6'i><'spcol-md-6an6'p>>",
//                opt.oTableTools = {
//                    "sSwfPath": "assets/plugins/datatables/swf/copy_csv_xls_pdf.swf",
//                    "aButtons": ["csv", "xls", "pdf", "print"]
//                };
//            }
                    if ($(this).hasClass('no-header')) {
                        opt.bFilter = false;
                        opt.bLengthChange = false;
                    }
                    if ($(this).hasClass('no-footer')) {
                        opt.bInfo = false;
                        opt.bPaginate = false;
                    }
                    if ($(this).hasClass('filter-head')) {
                        $('.filter-head thead th').each(function () {
                            var title = $('.filter-head thead th').eq($(this).index()).text();
                            $(this).append('<input type="text" onclick="stopPropagation(event);" class="form-control" placeholder="Filter ' + title + '" />');
                        });
                        var table = $('.filter-head').DataTable();
                        $(".filter-head thead input").on('keyup change', function () {
                            table.column($(this).parent().index() + ':visible').search(this.value).draw();
                        });
                    }
                    if ($(this).hasClass('filter-footer')) {
                        $('.filter-footer tfoot th').each(function () {
                            var title = $('.filter-footer thead th').eq($(this).index()).text();
                            $(this).html('<input type="text" class="form-control" placeholder="Filter ' + title + '" />');
                        });
                        var table = $('.filter-footer').DataTable();
                        $(".filter-footer tfoot input").on('keyup change', function () {
                            table.column($(this).parent().index() + ':visible').search(this.value).draw();
                        });
                    }
                    if ($(this).hasClass('filter-select')) {
                        $(this).DataTable({
                            language: {search: ""},
                            dom: 'Bfrtip',
                            buttons: [
//             'pdfFlash','copy', 'csv', 'excel', 'pdf', 'print'
                                'excel',
                                {extend: 'pdf',
//        text: 'Selected Excel',
                                    orientation: 'landscape',
                                    pageSize: 'LEGAL',
                                    exportOptions: {
                                        modifier: {
                                            selected: true
                                        }
                                    }
                                },
                            ],
                            "pageLength": 25,
                            initComplete: function () {
                                var api = this.api();
                                api.columns().indexes().flatten().each(function (i) {
                                    var column = api.column(i);
                                    var select = $('<select class="form-control" data-placeholder="Select to filter"><option value=""></option></select>')
                                            .appendTo($(column.footer()).empty())
                                            .on('change', function () {
                                                var val = $(this).val();
                                                column
                                                        .search(val ? '^' + val + '$' : '', true, false)
                                                        .draw();
                                            });
                                    column.data().unique().sort().each(function (d, j) {
                                        select.append('<option value="' + d + '">' + d + '</option>')
                                    });
                                });
                            }
                        });
                    }
                    if (!$(this).hasClass('filter-head') && !$(this).hasClass('filter-footer') && !$(this).hasClass('filter-select')) {
                        var oTable = $(this).dataTable(opt);
                        oTable.fnDraw();
                    }

                });
            });
        </script>
    </body>
</html>