<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <style>
            .ui-datepicker{
                z-index: 1000 !important;
            }
            .dataTables_filter{
                float: right !important;
            }
        </style>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $this->lang->line("audit_trail") ?></h2>
                                </div>
                                <div class="panel-content">
                                    <div class="text-center">
                                        <a class="btn btn-primary btn-square" href="<?= base_url() ?>/admin/Reports/audit_trail"><?= $this->lang->line('reload_logs') ?></a>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">


                                            <table class="table table-hover audit_trail">
                                                <thead>
                                                    <tr>
                                                        <th><?= $this->lang->line('action') ?></th>
                                                        <th><?= $this->lang->line('user') ?></th>
                                                        <th><?= $this->lang->line('time') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php //foreach ($changes as $change) { ?>
<!--                                                        <tr>
                                                            <td><?//= $change->action ?></td>
                                                            <td><?//= $change->first_name . ' ' . $change->last_name ?></td>
                                                            <td><?//= $change->timestamp ?></td>
                                                        </tr>-->
                                                    <?php //} ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>

        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>

        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script>
        <script>
            $(function () {

                $("#bank").change(function () {
                    if ($("#bank").val() == '') {
                        $("#branch").html('');
                        return;
                    }
                    $('.loader-overlay').removeClass('loaded');
                    $.ajax({
                        url: '<?= base_url() ?>admin/Reports/get_bank_branches',
                        type: "POST",
                        data: {bank: $("#bank").val(), "<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>"},
                        dataType: "JSON",
                        success: function (result) {
                            $("#branch").html('');
                            for (var i = 0; i < result.length; i++) {
                                $("#branch").append('<option value="' + result[i].branch_code + '">' + result[i].branch_code + '</option>');
                            }
<?php if ($this->input->post()) { ?>
                                $("#branch").val('<?= $this->input->post('branch') ?>');
                                $("#branch").select2();
<?php } ?>
                            $('.loader-overlay').addClass('loaded');
                        }
                    });
                });
                if ($("#bank").val() != '') {
                    $("#bank").change();
                }

                $(".audit_trail").DataTable({
                    // Processing indicator
                    "processing": true,
                    // DataTables server-side processing mode
                    "serverSide": true,
                    // Initial no order.
                    "order": [],
                    "ordering": false,
                    "pageLength": 10,
                    // Load data from an Ajax source
                    "ajax": {
                        "url": "<?php echo base_url('admin/reports/audit_trail_ajax/'); ?>",
                        "type": "POST",
                        "data": {"<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>"}
                    },
                    //Set column definition initialisation properties
                    "columnDefs": [{
                            "targets": [0],
                            "orderable": false
                        }]
                });
            });

        </script>
    </body>
</html>