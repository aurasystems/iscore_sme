<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <link href="<?=base_url()?>assets/plugins/export/jquery.dataTables.min.css" rel="stylesheet">
        <link href="<?=base_url()?>assets/plugins/export/Buttons-1.5.6/css/buttons.dataTables.min.css" rel="stylesheet">
        <style>
            .ui-datepicker{
                z-index: 1000 !important;
            }
            table.dataTable thead th, table.dataTable thead td {
                border-bottom-color: #ddd;
            }
            #table_wrapper{
                overflow: auto;
            }
        </style>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $this->lang->line("cases_report") ?></h2>
                                </div>
                                <div class="panel-content">

                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <form id="form" method="POST" action="" role="form" class="form-horizontal form-validation">
                                                <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
                                                <h4><strong><?= $this->lang->line('filter') ?> :</strong></h4>
                                                <?php if ($this->session->userdata('levels')->bank_only == '0' && $this->session->userdata('levels')->branch_only == '0') { ?>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="<?= $label_att ?>"><?= $this->lang->line("bank") ?></label>
                                                            <div class="col-sm-9">
                                                                <select data-search="true" name="bank" id="bank" class="form-control">
                                                                    <option value><?= $this->lang->line('select') ?></option>
                                                                    <option value="all" <?=$this->input->post("bank")=="all"?"selected":""?> ><?= $this->lang->line('all') ?></option>
                                                                    <?php
                                                                    foreach ($banks as $bank) {
                                                                        $selected = '';
                                                                        if ($this->input->post() && $this->input->post('bank') == $bank->id) {
                                                                            $selected = 'selected';
                                                                        }
                                                                        ?>
                                                                        <option <?= $selected ?> value="<?= $bank->id ?>"><?= $bank->name ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <?php if ($this->session->userdata('levels')->branch_only == '0') { ?>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="<?= $label_att ?>"><?= $this->lang->line("branch") ?></label>
                                                            <div class="col-sm-9">
                                                                <select name="branch" id="branch" class="form-control">
                                                                    <option value><?= $this->lang->line('select') ?></option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="<?= $label_att ?>"><?= $this->lang->line("status") ?></label>
                                                        <div class="col-sm-9">
                                                            <select name="status" class="form-control">
                                                                <option value><?= $this->lang->line('select') ?></option>
                                                                <option value="all" <?= $this->input->post('status') == "all" || !$this->input->post("status") ? 'selected' : '' ?>><?= $this->lang->line('all') ?></option>
                                                                <option value="0" <?= $this->input->post('status') === "0" ? 'selected' : '' ?>><?= $this->lang->line('not_completed') ?></option>
                                                                <option value="1" <?= $this->input->post('status') == 1 ? 'selected' : '' ?>><?= $this->lang->line('waiting_approval') ?></option>
                                                                <option value="2" <?= $this->input->post('status') == 2 ? 'selected' : '' ?>><?= $this->lang->line('approved') ?></option>
                                                                <option value="4" <?= $this->input->post('status') == 4 ? 'selected' : '' ?>><?= $this->lang->line('rejected') ?></option>
                                                                <option value="3" <?= $this->input->post('status') == 3 ? 'selected' : '' ?>><?= $this->lang->line('completed') ?></option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-md-12 text-center"><?= $this->lang->line("initiation_date") ?></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-md-12 text-center"><?= $this->lang->line("finalization_date") ?></label>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="clearfix"></div>
                                                
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="<?= $label_att ?>"><?= $this->lang->line("from") ?></label>
                                                        <div class="col-sm-9"> <!--datetimepicker-->
                                                            <input typ="text" name="submitted_date_from" id="submitted_date_from" class="form-control" value="<?= set_value('submitted_date_from', $submitted_date_from) ?>" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="<?= $label_att ?>"><?= $this->lang->line("to") ?></label>
                                                        <div class="col-sm-9"> <!--datetimepicker-->
                                                            <input typ="text" name="submitted_date_to" id="submitted_date_to" class="form-control" value="<?= set_value('submitted_date_to', $submitted_date_to) ?>" />
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="<?= $label_att ?>"><?= $this->lang->line("from") ?></label>
                                                        <div class="col-sm-9"> <!--datetimepicker-->
                                                            <input typ="text" name="completed_date_from" id="completed_date_from" class="form-control" value="<?= set_value('completed_date_from') ?>" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="<?= $label_att ?>"><?= $this->lang->line("to") ?></label>
                                                        <div class="col-sm-9"> <!--datetimepicker-->
                                                            <input typ="text" name="completed_date_to" id="completed_date_to" class="form-control" value="<?= set_value('completed_date_to') ?>" />
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                
                                                
                                                <div class="pull-right">
                                                        <div class="col-md-4">
                                                            <button type="submit" class="btn btn-sm btn-blue filter"><?= $this->lang->line('filter') ?></button>
                                                        </div>
                                                </div>
<!--                                                <div class="pull-right">
                                                    <div class="col-md-3">
                                                        <button type="submit" class="btn btn-sm btn-blue filter"><?= $this->lang->line('filter') ?></button>
                                                    </div>
                                                </div>-->
                                            </form>
                                            <div class="clearfix"></div>
                                            <hr/>
                                            <br/>

                                            <!--<table class="table table-hover table-dynamic table-tools">-->
                                            <table id="table"  class=" reports-table">
                                                <thead>
                                                    <tr>
                                                        <th><?= $this->lang->line('case_name') ?></th>
                                                        <th><?= $this->lang->line('created_by') ?></th>
                                                        <th class="text-center"><?= $this->lang->line('status') ?></th>
                                                        <th><?= $this->lang->line('initiation_date') ?></th>
                                                        <th class="text-center"><?= $this->lang->line('finalization_date') ?></th>
                                                        <th><?= $this->lang->line('bank') ?></th>
                                                        <th><?= $this->lang->line('branch_code') ?></th>
                                                        <th><?= $this->lang->line('comp_name') ?></th>
                                                        <th><?= $this->lang->line('reg_num') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($cases as $case) { ?>
                                                        <tr>
                                                            <td><a href="<?= base_url() ?>admin/Cases_two/view_case/<?= $case->id ?>"><?= $case->name ?></a></td>
                                                            <td><?= $case->first_name . ' ' . $case->last_name ?></td>
                                                            <td><?php
                                                                if ($case->status != 'saved' && $case->status != 'completed') {
                                                                    echo '<span class="label label-danger">Not Completed</span>';
                                                                } else if ($case->completed == 1) {
                                                                    echo '<span class="label label-success">Completed</span>';
                                                                }else if ($case->approved == -1) {
                                                                    echo '<span class="label label-danger">Rejected</span>';
                                                                } else if ($case->approved != 1) {
                                                                    echo '<span class="label label-warning">Waiting Approval</span>';
                                                                } else {
                                                                    echo '<span class="label label-warning">Approved</span>';
                                                                }
                                                                ?></td>
                                                            <td>
                                                                <?php //if($case->approved==0){ 
                                                                    //echo "---";
                                                                //}
                                                               // elseif($case->approved==-1){
                                                                    echo date('Y/m/d', strtotime($case->date_created));
                                                               // }
                                                               // else{
                                                                   // echo date('Y/m/d', strtotime($case->date_approved));
                                                               // }
                                                                ?>
                                                            
                                                            </td>

                                                            <td class="text-center"><?= $case->completed == 0 ? '---' : date('Y/m/d', strtotime($case->date_completed)) ?></td>

                                                            <td><?= $case->bank_name ?></td>

                                                            <td><?= $case->branch_code ?></td>

                                                            <td><?= $case->comp_name ?></td>

                                                            <td><?= $case->reg_num ?></td>

                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>

        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->

        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script>
        
        <script src="<?=base_url()?>assets/plugins/export/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/export/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/export/Buttons-1.5.6/js/buttons.flash.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/export/jszip.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/export/pdfmake.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/export/vfs_fonts.js"></script>
        <script src="<?=base_url()?>assets/plugins/export/Buttons-1.5.6/js/buttons.html5.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/export/Buttons-1.5.6/js/buttons.print.min.js"></script>
        
        <script>
            $(function () {

                $("#bank").change(function () {
                    if ($("#bank").val() == '') {
                        $("#branch").html('');
                        return;
                    }
                    $('.loader-overlay').removeClass('loaded');
                    $.ajax({
                        url: '<?= base_url() ?>admin/Reports/get_bank_branches',
                        type: "POST",
                        data: {bank: $("#bank").val(), "<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>"},
                        dataType: "JSON",
                        success: function (result) {
                            $("#branch").html('');
                             if($("#bank").val()=="all"){
                                 $("#branch").append('<option value="all_all" <?=$this->input->post("branch")=="all"?"selected":""?> ><?= $this->lang->line('all') ?></option>');
                             }
                            else{
                                $("#branch").append('<option value="all" <?=$this->input->post("branch")=="all"?"selected":""?> ><?= $this->lang->line('all') ?></option>');
                            }
                            for (var i = 0; i < result.length; i++) {
                                if($("#bank").val()=="all"){
                                    $("#branch").append('<option value="' + result[i].branch_bank_id + '">' + result[i].branch_code + '</option>');
                                }
                                else{
                                    $("#branch").append('<option value="' + result[i].branch_code + '">' + result[i].branch_code + '</option>');
                                }
                            }
<?php if ($this->input->post()) { ?>
                                $("#branch").val('<?= $this->input->post('branch') ?>');
                                $("#branch").select2();
<?php } ?>
                            $('.loader-overlay').addClass('loaded');
                        }
                    });
                });
                if ($("#bank").val() != '') {
                    $("#bank").change();
                }
            });
        </script>
        <script>
  $( function() {
    var dateFormat = "mm/dd/yy";
    function dateRange(date_from,date_to){
        var from = $( "#"+date_from )
        .datepicker({
          changeMonth: true,
          numberOfMonths: 2
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
        to = $( "#"+date_to ).datepicker({
          changeMonth: true,
          numberOfMonths: 2
        })
        .on( "change", function() {
          date_from.datepicker( "option", "maxDate", getDate( this ) );
        });
    }
    
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
    dateRange("submitted_date_from","submitted_date_to");
    dateRange("completed_date_from","completed_date_to");
    
  } );
  
  
  
  $(document).ready(function() {
      
       $('.reports-table').each(function () {
            var opt = {
                 language: { search: "" },
              dom: 'Bfrtip',
        buttons: [
//             'pdfFlash','copy', 'csv', 'excel', 'pdf', 'print'
              'excel', 
              {extend: 'pdf',
//        text: 'Selected Excel',
orientation:'landscape',
                pageSize: 'LEGAL',
        exportOptions: {
        modifier: {
        selected: true
                }
            }
        },
        ],    
            "pageLength": 25};
            // Tools: export to Excel, CSV, PDF & Print
//            if ($(this).hasClass('table-tools')) {
//                opt.sDom = "<'row'<'col-md-6'f><'col-md-6'T>r>t<'row'<'col-md-6'i><'spcol-md-6an6'p>>",
//                opt.oTableTools = {
//                    "sSwfPath": "assets/plugins/datatables/swf/copy_csv_xls_pdf.swf",
//                    "aButtons": ["csv", "xls", "pdf", "print"]
//                };
//            }
            if ($(this).hasClass('no-header')) {
                opt.bFilter = false;
                opt.bLengthChange = false;
            }
            if ($(this).hasClass('no-footer')) {
                opt.bInfo = false;
                opt.bPaginate = false;
            }
            if ($(this).hasClass('filter-head')) {
                $('.filter-head thead th').each( function () {
                    var title = $('.filter-head thead th').eq($(this).index()).text();
                    $(this).append( '<input type="text" onclick="stopPropagation(event);" class="form-control" placeholder="Filter '+title+'" />' );
                });
                var table = $('.filter-head').DataTable();
                $(".filter-head thead input").on( 'keyup change', function () {
                    table.column( $(this).parent().index()+':visible').search( this.value ).draw();
                });
            } 
            if ($(this).hasClass('filter-footer')) {
                $('.filter-footer tfoot th').each( function () {
                    var title = $('.filter-footer thead th').eq($(this).index()).text();
                    $(this).html( '<input type="text" class="form-control" placeholder="Filter '+title+'" />' );
                });
                var table = $('.filter-footer').DataTable();
                $(".filter-footer tfoot input").on( 'keyup change', function () {
                    table.column( $(this).parent().index()+':visible').search( this.value ).draw();
                });
            } 
            if ($(this).hasClass('filter-select')) {
                $(this).DataTable( {
                      language: { search: "" },
              dom: 'Bfrtip',
        buttons: [
//             'pdfFlash','copy', 'csv', 'excel', 'pdf', 'print'
              'excel', 
              {extend: 'pdf',
//        text: 'Selected Excel',
orientation:'landscape',
                pageSize: 'LEGAL',
        exportOptions: {
        modifier: {
        selected: true
                }
            }
        },
        ], 
                    "pageLength": 25,
                    initComplete: function () {
                        var api = this.api();
             
                        api.columns().indexes().flatten().each( function ( i ) {
                            var column = api.column( i );
                            var select = $('<select class="form-control" data-placeholder="Select to filter"><option value=""></option></select>')
                                .appendTo( $(column.footer()).empty() )
                                .on( 'change', function () {
                                    var val = $(this).val();
             
                                    column
                                        .search( val ? '^'+val+'$' : '', true, false )
                                        .draw();
                                } );
             
                            column.data().unique().sort().each( function ( d, j ) {
                                select.append( '<option value="'+d+'">'+d+'</option>' )
                            } );
                        } );
                    }
                } );
            } 
            if (!$(this).hasClass('filter-head') && !$(this).hasClass('filter-footer') && !$(this).hasClass('filter-select'))  {
                var oTable = $(this).dataTable(opt);
                oTable.fnDraw();
            }
           
        });
      
       $('#table_filter input').addClass('form-control');
       $('#table_filter input').attr('placeholder',"Search...");

} );
  </script>
    </body>
</html>