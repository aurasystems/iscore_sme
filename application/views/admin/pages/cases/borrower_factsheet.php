<?php
$add = get_perm("cases", 1);
$edit = get_perm("cases", 2);
?>
<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $this->lang->line("borrower_factsheet") ?></h2>
                                </div>
                                <div class="panel-content">
                                    <?php if ($this->session->flashdata("success")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-success"><?= $this->session->flashdata("success") ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($this->session->flashdata("error")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-danger"><?= $this->session->flashdata("error") ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($sum_share_error) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-danger"><?= $this->lang->line('sum_share_error') ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($this->session->userdata("error")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-danger"><?= $this->session->userdata("error") ?></div>
                                            </div>
                                        </div>
                                        <?php
                                        $this->session->unset_userdata('error');
                                    }
                                    ?>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <form id="form" method="POST" action="" role="form" class="form-horizontal form-validation">
                                                <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
                                                <h4><?= $this->lang->line("company_information") ?></h4>
                                                <hr>
                                                <!--                        <div class="form-group">
                                                                          <label class="<?= $label_att ?>">Name
                                                                          </label>
                                                                          <div class="col-sm-9">
                                                                            <input type="text" name="name" class="form-control" required>
                                                                          </div>
                                                                        </div>-->
                                                <?php for ($i = 4; $i <= 5; $i++) { ?>
                                                    <div class="form-group">
                                                        <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_factsheet_C$i") ?></label>
                                                        <div class="col-sm-9">
                                                            <?php
                                                            $value = '';
                                                            if ($this->input->post()) {
                                                                $value = $this->input->post("C$i");
                                                            } else if (isset($factsheet_data[$i])) {
                                                                $value = $factsheet_data[$i]["C"];
                                                            }
                                                            ?>

                                                            <input type="text" name="C<?= $i ?>" class="form-control" value="<?= $value ?>" >
                                                            <span class="text-danger"><?= form_error("C$i") ?></span>
                                                        </div>
                                                    </div>
                                                    <?php if ($i == 4) { ?>
                                                        <div class="form-group">
                                                            <label class="<?= $label_att ?>"><?= $this->lang->line("amounts") ?></label>
                                                            <div class="col-sm-9">
                                                                <?php
                                                                $value = '';
                                                                if ($this->input->post()) {
                                                                    $value = $this->input->post("amounts");
                                                                }
                                                                ?>

                                                                <select class="form-control" name="amounts" id="amounts">
                                                                    <option <?= $this->input->post("amounts") == 1 ? 'selected' : '' ?> value="1"><?= $this->lang->line('actuals') ?></option>
                                                                    <option <?= $this->input->post("amounts") == 1000 ? 'selected' : '' ?> value="1000"><?= $this->lang->line('thousands') ?></option>
                                                                    <option <?= $this->input->post("amounts") == 1000000 ? 'selected' : '' ?> value="1000000"><?= $this->lang->line('millions') ?></option>
                                                                </select>
                                                                <span class="text-danger"><?= form_error("amounts") ?></span>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <?php
                                                    $value = '';
                                                    if ($this->input->post()) {
                                                        $value = $this->input->post("E$i");
                                                    } else if (isset($factsheet_data[$i])) {
                                                        $value = $factsheet_data[$i]["E"];
                                                    }
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_factsheet_E$i") ?></label>
                                                        <div class="col-sm-9 m-t-10">
                                                            <?php if ($i == 4) { ?>
                                                                <input type="text" name="E<?= $i ?>" class="form-control" value="<?= $value ?>" >
                                                            <?php } else { ?>
                                                                <span><?= get_value_from_code("c10", $line_activity,1); ?></span>
                                                            <?php } ?>
                                                            <span class="text-danger"><?= form_error("E$i") ?></span>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    //}
                                                }
                                                ?>
                                                <br/>
                                                <h4><?= $this->lang->line("shareholding") ?></h4>
                                                <hr>
                                                <table class='table table-hover'>
                                                    <thead>
                                                        <tr>
                                                            <td><?= $this->lang->line('sr_no') ?></td>
                                                            <td class='text-center'><?= $this->lang->line('investor_group') ?></td>
                                                            <td class='text-center'><?= $this->lang->line('share') ?></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <?php for ($i = 9; $i <= 18; $i++) { ?>
                                                            <tr>
                                                                <td><?= $i - 8 ?></td>
                                                                <?php
                                                                $value = '';
                                                                if ($this->input->post()) {
                                                                    $value = $this->input->post("C$i");
                                                                } else if (isset($factsheet_data[$i])) {
                                                                    $value = $factsheet_data[$i]["C"];
                                                                }
                                                                ?>
                                                                <td><input type="text" name="C<?= $i ?>" class="form-control" value="<?= $value ?>"><span class="text-danger"><?= form_error("C$i") ?></span></td>
                                                                <?php
                                                                $value = '';
                                                                if ($this->input->post()) {
                                                                    $value = $this->input->post("D$i");
                                                                } else if (isset($factsheet_data[$i])) {
                                                                    $value = $factsheet_data[$i]["D"];
                                                                }
                                                                ?>
                                                                <td><input type="text" name="D<?= $i ?>" class="form-control" value="<?= $value ?>"><span class="text-danger"><?= form_error("D$i") ?></span></td>
                                                            </tr>            
                                                        <?php } ?>

                                                    </tbody>

                                                </table>

                                                <br/>
                                                <h4><?= $this->lang->line("board_of_directors") ?></h4>
                                                <hr>
                                                <table class='table table-hover'>
                                                    <thead>
                                                        <tr>
                                                            <td><?= $this->lang->line('sr_no') ?></td>
                                                            <td class='text-center'><?= $this->lang->line('name') ?></td>
                                                            <td class='text-center'><?= $this->lang->line('remarks') ?></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <?php for ($i = 22; $i <= 31; $i++) { ?>
                                                            <tr>
                                                                <td><?= $i - 21 ?></td>
                                                                <?php
                                                                $value = '';
                                                                if ($this->input->post()) {
                                                                    $value = $this->input->post("C$i");
                                                                } else if (isset($factsheet_data[$i])) {
                                                                    $value = $factsheet_data[$i]["C"];
                                                                }
                                                                ?>
                                                                <td><input type="text" name="C<?= $i ?>" class="form-control" value="<?= $value ?>"><span class="text-danger"><?= form_error("C$i") ?></span></td>
                                                                <?php
                                                                $value = '';
                                                                if ($this->input->post()) {
                                                                    $value = $this->input->post("D$i");
                                                                } else if (isset($factsheet_data[$i])) {
                                                                    $value = $factsheet_data[$i]["D"];
                                                                }
                                                                ?>
                                                                <td><input type="text" name="D<?= $i ?>" class="form-control" value="<?= $value ?>"><span class="text-danger"><?= form_error("D$i") ?></span></td>
                                                            </tr>            
                                                        <?php } ?>

                                                    </tbody>

                                                </table>


                                                <br/>
                                                <h4><?= $this->lang->line("key_personnel") ?></h4>
                                                <hr>
                                                <table class='table table-hover'>
                                                    <thead>
                                                        <tr>
                                                            <td><?= $this->lang->line('sr_no') ?></td>
                                                            <td class='text-center'><?= $this->lang->line('name') ?></td>
                                                            <td class='text-center'><?= $this->lang->line('designation') ?></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <?php for ($i = 35; $i <= 44; $i++) { ?>
                                                            <tr>
                                                                <td><?= $i - 34 ?></td>
                                                                <?php
                                                                $value = '';
                                                                if ($this->input->post()) {
                                                                    $value = $this->input->post("C$i");
                                                                } else if (isset($factsheet_data[$i])) {
                                                                    $value = $factsheet_data[$i]["C"];
                                                                }
                                                                ?>
                                                                <td><input type="text" name="C<?= $i ?>" class="form-control" value="<?= $value ?>"><span class="text-danger"><?= form_error("C$i") ?></span></td>
                                                                <?php
                                                                $value = '';
                                                                if ($this->input->post()) {
                                                                    $value = $this->input->post("D$i");
                                                                } else if (isset($factsheet_data[$i])) {
                                                                    $value = $factsheet_data[$i]["D"];
                                                                }
                                                                ?>
                                                                <td><input type="text" name="D<?= $i ?>" class="form-control" value="<?= $value ?>"><span class="text-danger"><?= form_error("D$i") ?></span></td>
                                                            </tr>            
                                                        <?php } ?>

                                                    </tbody>

                                                </table>



                                                <input type="hidden" value="0" name="continue_later" id="continue_later"/>

                                                <div class="row">
                                                    <div class="col-sm-9">
                                                        <div class="pull-left">
                                                            <a class="btn btn-embossed btn-primary m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases/borrower_information/<?= $case_id ?>/<?= isset($view_only) ? '1' : '' ?>'"><?= $this->lang->line("borrower_information") ?></a>
                                                            <a class="btn btn-embossed btn-primary m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases_two/borrower_financials/<?= $case_id ?>/<?= isset($view_only) ? '1' : '' ?>'"><?= $this->lang->line("borrower_financials") ?></a>
                                                            <a class="btn btn-embossed btn-primary m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases_two/borrower_assessment/<?= $case_id ?>/<?= isset($view_only) ? '1' : '' ?>'"><?= $this->lang->line("borrower_assessment") ?></a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-9 col-sm-offset-3">

                                                        <div class="pull-right">
                                                            <?php
                                                            if (!isset($view_only)) {
                                                                if ($add || $edit) {
//                                                                    if ($is_case_pass_this_setp) {
                                                                        ?>
                                                                        <?php //if (empty($factsheet_data)) {   ?>
                                                                        <button type="submit" id="save_and_continue_later" class="cancel btn btn-embossed btn-default m-b-10 m-r-0"><?= $this->lang->line("save_continue_later") ?></button>
                                                                    <?php //} ?>
                                                                    <button type="submit" class="btn btn-embossed btn-primary m-r-20"><?= $this->lang->line("save_next") ?></button>
                                                                <?php }
                                                                ?>
                                                                <a class="btn btn-embossed btn-danger m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases_two'"><?= $this->lang->line('discard'); ?></a>
                                                            <?php } else {
                                                                ?>
                                                                <a class="btn btn-embossed btn-danger m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases_two'"><?= $this->lang->line('close'); ?></a>
                                                                <a class="btn btn-embossed btn-primary m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases_two/borrower_financials/<?= $case_id ?>/1'"><?= $this->lang->line("next") ?></a>
                                                            <?php } ?>
                                                            <a class="back-btn btn btn-embossed btn-primary m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases/borrower_information/<?= $case_id ?>/<?= isset($view_only) ? 1 : '' ?>'"><?= $this->lang->line("back") ?></a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>

        <script>
            $(document).ready(function () {
                $("#save_and_continue_later").click(function () {
                    $("#continue_later").val("1");
                    $("input").each(function (index, value) {
                        $(this).removeAttr("required");
                    });
                    $("#form").submit();
                });

<?php if (isset($view_only)) { ?>
                    $(".c-red").hide();
                    $('input').each(function () {
                        $(this).not(':hidden').replaceWith("<span class='view_span'>" + $(this).val() + "</span>");
                    });

                    $('textarea').each(function () {
                        $(this).not(':hidden').replaceWith("<span class='view_span'>" + $(this).val() + "</span>");
                    });

                    $('select').not('#amounts').each(function () {
                        $(this).not(':hidden').replaceWith("<span class='view_span'>" + $(this).val() + "</span>");
                    });

                    $("#amounts").each(function () {
                        var text = '';
                        if ($(this).val() == 1) {
                            text = "<?= $this->lang->line('actuals') ?>";
                        } else if ($(this).val() == 1000) {
                            text = "<?= $this->lang->line('thousands') ?>";
                        } else if ($(this).val() == 1000000) {
                            text = "<?= $this->lang->line('millions') ?>";
                        }

                        $(this).not(':hidden').replaceWith("<span class='view_span'>" + text + "</span>");
                    });
<?php } ?>
            });
        </script>
    </body>
</html>