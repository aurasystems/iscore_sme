<?php 
$approve_add = get_perm("approve_case", 1);
$approve_edit = get_perm("approve_case", 2);
?>
<div class="modal fade" id="not_approved_comment" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                <h4 class="modal-title"><strong><?= $this->lang->line('add') ?> </strong> <?= $this->lang->line('comment') ?></h4>
            </div>
            <form id="form" method="POST" action="<?= base_url() ?>admin/Cases_two/not_approve_case" role="form" class="form-horizontal form-validation">
                <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
                <div class="modal-body">

                    <input type="hidden" name="case_id_" id="case_id_" />
                    <div class="col-md-12">
                        <label class="control-label"><?= $this->lang->line('comment') ?> <?= $this->lang->line('optional') ?></label>
                        <textarea class="form-control" name="comment" id="comment" rows="4"></textarea>
                    </div>

                </div>
                <div class="clearfix"></div>
                <br/>
                <hr/>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal"><?= $this->lang->line('close') ?></button>
                    <?php if ($approve_add || $approve_edit) { ?>
                        <button type="submit" class="btn btn-primary btn-embossed"><?= $this->lang->line('lang_save') ?></button>
                    <?php } ?>
                </div>
            </form>
        </div>
    </div>
</div>

