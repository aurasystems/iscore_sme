<?php
$add = get_perm("cases", 1);
$edit = get_perm("cases", 2);
$approve_add = get_perm("approve_case", 1);
$approve_edit = get_perm("approve_case", 2);
?>
<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <style>
            .text_red{
                color: red;
            }
            .select2-drop, .select2-container{
                max-width: 480px;
            }
        </style>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $this->lang->line("borrower_assessment") ?></h2>
                                </div>
                                <div class="panel-content">
                                    <?php if ($this->session->flashdata("success")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-success"><?= $this->session->flashdata("success") ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($this->input->post()) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-danger"><?= $this->lang->line('check_errors') ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($this->session->userdata("error")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-danger"><?= $this->session->userdata("error") ?></div>
                                            </div>
                                        </div>
                                        <?php
                                        $this->session->unset_userdata('error');
                                    }
                                    ?>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <p><strong class="text_red">*</strong> <?= $this->lang->line('required_param') ?></p>
                                            <form id="form" method="POST" action="" role="form" class="form-horizontal form-validation">
                                                <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
                                                <table class='table table-hover table-bordered'>
                                                    <thead>
                                                        <tr>
                                                            <td class='text-center'><?= $this->lang->line('sr_no') ?></td>
                                                            <td class='text-center'><?= $this->lang->line('assessment_parameter') ?></td>
                                                            <td class='text-center'><?= $this->lang->line('parameter_description') ?></td>
                                                            <td class='text-center' style="width: 50%"><?= $this->lang->line('select') ?></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $sr_no = 1;
                                                        for ($i = 4; $i <= 229; $i++) {
                                                            if ($this->lang->line("borrower_assessment_F$i") != '') {
                                                                ?>

                                                                <tr>

                                                                    <?php if ($i == 4 || $i == 17 || $i == 120 || $i == 132 || $i == 184 || $i == 214) { ?>
                                                                        <td colspan="4" class="text-center"><strong><?= $this->lang->line("borrower_assessment_F$i") ?></strong></td>
                                                                        <?php
                                                                        continue;
                                                                    }
                                                                    ?>
                                                                    <td class="text-center"><?= $sr_no ?></td>
                                                                    <td class="text-center"><?= $this->lang->line("borrower_assessment_F$i") ?></td>
                                                                    <td class="text-center"><?= $this->lang->line("borrower_assessment_F$i" . "_desc") ?></td>
                                                                    <?php
                                                                    $background = "";
                                                                    $selected = "";
                                                                    //modified
                                                                    if ($sr_no == 38) {
                                                                        $background = "background-color:#ffff82;";
                                                                        $selected = str_replace('&lt;', '<', $param_39);
                                                                    } elseif ($sr_no == 39) {
                                                                        $background = "background-color:#ffff82;";
                                                                        $selected = str_replace('&lt;', '<', $param_40);
                                                                    }
                                                                    ?>
                                                                    <td style="width: 50%;<?= $background ?>"> <strong id="star<?= $i ?>" class="text_red">*</strong>
                                                                        <!--//modified-->
                                                                        <?php if ($sr_no == 38 || $sr_no == 39) { ?>
                                                                            <span><?= $selected ?></span>
                                                                            <select  name="F<?= $i ?>" id="<?= $i ?>" class="form-control" data-search="true" n="<?= $sr_no ?>">
                                                                                <option value><?= $this->lang->line("select") ?></option>
                                                                                <?php
                                                                                foreach (get_options("borrower_assessment_F$i") as $one) {
                                                                                    ?>
                                                                                    <option <?= $selected == $one ? 'selected' : '' ?> value="<?= $one ?>"><?= $one ?></option>
                                                                                <?php } ?>
                                                                            </select>
                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <select name="F<?= $i ?>" id="<?= $i ?>" class="form-control" data-search="true" n="<?= $sr_no ?>">
                                                                                <option value><?= $this->lang->line("select") ?></option>
                                                                                <?php
                                                                                // foreach (get_options("borrower_assessment_F$i") as $one) {
                                                                                foreach (get_options_dynamically("f$i") as $one) {
                                                                                    $selected = '';
                                                                                    if ($this->input->post()) {
                                                                                        $selected = str_replace('&lt;', '<', $this->input->post("F$i"));
                                                                                    } else if (isset($assessment_data[$i])) {
                                                                                        // $selected = $assessment_data[$i]['F'];
                                                                                        $selected = str_replace('&lt;', '<', $assessment_data[$i]['F']);
                                                                                    }
                                                                                    ?>
                                                                                    <option <?= $selected == $one->code ? 'selected' : '' ?> value="<?= $one->code ?>"><?= $one->value ?></option>
                                                                                <?php } ?>
                                                                            </select>
                                                                        <?php } ?>
                                                                        <span class="c-red"><?= form_error("F$i") ?></span>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                                $sr_no++;
                                                            }
                                                        }//die();
                                                        ?>
                                                    </tbody>

                                                </table>





                                                <input type="hidden" value="0" name="continue_later" id="continue_later"/>

                                                <div class="row">
                                                    <div class="col-sm-9">
                                                        <div class="pull-left">
                                                            <a class="btn btn-embossed btn-primary m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases/borrower_information/<?= $case_id ?>/<?= isset($view_only) ? '1' : '' ?>'"><?= $this->lang->line("borrower_information") ?></a>
                                                            <a class="btn btn-embossed btn-primary m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases_two/borrower_factsheet/<?= $case_id ?>/<?= isset($view_only) ? '1' : '' ?>'"><?= $this->lang->line("borrower_factsheet") ?></a>
                                                            <a class="btn btn-embossed btn-primary m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases_two/borrower_financials/<?= $case_id ?>/<?= isset($view_only) ? '1' : '' ?>'"><?= $this->lang->line("borrower_financials") ?></a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-9 col-sm-offset-3">
                                                        <div class="pull-right">
                                                            <?php
                                                            if (!isset($view_only)) {
                                                                if ($add || $edit) {
                                                                    // if ($is_case_pass_this_setp) {
                                                                    ?>
                                                                    <button type="submit" id="save_and_continue_later" class="cancel btn btn-embossed btn-default m-b-10 m-r-0"><?= $this->lang->line("save_continue_later") ?></button>
                                                                    <?php //} ?>

                                                                    <button type="submit" class="btn btn-embossed btn-primary m-r-20" ><?= $this->lang->line("next") ?></button>
                                                                <?php }
                                                                ?>
                                                                <a class="btn btn-embossed btn-danger m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases_two'"><?= $this->lang->line('discard'); ?></a>
                                                            <?php } else {
                                                                ?>
                                                                <!--BEGIN EXPORT PDF-->
                                                                <a  href="<?= base_url() ?>admin/Cases/export_pdf/<?= $case_id ?>" id="export-pdf" class="btn btn-danger">Export PDF</a>
                                                                <!--END EXPORT PDF-->
                                                                <?php if ($approve_add || $approve_edit) { ?>
                                                                    <!-- //modified-->
                                                                    <!--BEGIN APPROVE GENERATE-->
                                                                    <?php
                                                                    if ($case_status == 'saved') {
                                                                        if (($approved == 0 || $approved == -1)) {
                                                                            ?>
                                                                            <img id="ajax-loader" style="display:none;position: absolute; top: 30%; left: 1%;transform: translate(-50%, -50%);width: 100px;" src="<?= base_url() ?>assets/images/ajax-loader.gif"/>
                                                                            <div id="approve-generate" class="btn btn-success"><?= $this->lang->line("approve_generate") ?></div>
                                                                            <a id="download-excel" style="display: none;" href=""></a>
                                                                        <?php } if ($approved == 0) { ?>
                                                                            <div id="not_approve" class="btn btn-danger"><?= $this->lang->line("not_approved") ?></div>
                                                                        <?php } else if ($approved == 1) { ?>
                                                                            <span class="c-green f-32" title="<?= $this->lang->line("approved") ?>"><i class="fa fa-check-circle"></i></span>
                                                                        <?php } else if ($approved == -1) { ?>
                                                                            <span class="c-red f-32" id="rejected" title="<?= $this->lang->line("rejected") ?>"><i class="fa fa-close"></i></span>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <!--END APPROVE GENERATE-->
                                                                <?php } ?>
                                                                <a class="btn btn-embossed btn-danger m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases_two'"><?= $this->lang->line('close'); ?></a>
                                                            <?php } ?>

                                                            <a class="back-btn btn btn-embossed btn-primary m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases_two/borrower_financials/<?= $case_id ?>/<?= isset($view_only) ? 1 : '' ?>'"><?= $this->lang->line("back") ?></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <?php $this->load->view('admin/pages/cases/comment_modal') ?>

        <script>
            $(document).ready(function () {
                //   hide * for optional fields.......
                var condition = "<?= $condition ?>";
                condition = condition.replace("return", "");
                condition = condition.replace(";", "");
                $("select").each(function () {
                    var id_ = $(this).attr('id');
//                    if (id_ == 22 || id_ == 29 || id_ == 61 || id_ == 71 || id_ == 75 || id_ == 79 || id_ == 86 || id_ == 93 || id_ == 100 || id_ == 107 || id_ == 115 || id_ == 125 || id_ == 151) {
//                        $("#star" + id_).hide();
//                    }
                    var $n = $(this).attr("n");
                    if (eval(condition)) {
                        $("#star" + id_).hide();
                    }
                    if ($n == 38 || $n == 39) {
                        $("#" + id_).hide();
                        $("#star" + id_).hide();
                    }
                });

                $("#save_and_continue_later").click(function () {
                    $("#continue_later").val("1");
                    $("input").each(function (index, value) {
                        $(this).removeAttr("required");
                    });
                    $("#form").submit();
                });



<?php if (isset($view_only)) { ?>
                    $(".c-red").hide();
                    $(".text_red").hide();
                    $('input').each(function () {
                        $(this).not(':hidden').replaceWith("<span class='view_span'>" + $(this).val() + "</span>");
                    });

                    $('textarea').each(function () {
                        $(this).not(':hidden').replaceWith("<span class='view_span'>" + $(this).val() + "</span>");
                    });

                    $('select').each(function () {
                        $(this).not(':hidden').replaceWith("<span class='view_span'>" + $(this).find("option:selected").text() + "</span>");
                    });
<?php } ?>

                $("#rejected").show();

//BEGIN APPROVE
                $("#approve-generate").click(function () {
                    $("#approve-generate").attr("disabled", "disabled");
                    $("#ajax-loader").show();
                    $.ajax({
                        url: "<?= base_url() ?>admin/Cases/approve_case",
                        type: "POST",
                        data: {'case_id': "<?= $case_id ?>", "<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>"},
                        success: function (response) {
                            if (response == "incomplete") {
                                alert("<?= $this->lang->line("case_incomplete") ?>");
                                $("#ajax-loader").hide();
                            } else if (response == "cannot_create_file") {
                                alert("<?= $this->lang->line("cannot_create_file") ?>");
                                $("#ajax-loader").hide();
                            } else if (response == "file_size_error") {
                                alert("<?= $this->lang->line("file_size_error") ?>");
                                $("#ajax-loader").hide();
                            } else {
                                $.ajax({
                                    url: "<?= base_url() ?>admin/Cases/replace_whole_file_content",
                                    type: "POST",
                                    data: {"<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>"},
                                    success: function () {
                                        // $("#download-excel").attr("href", "<?= base_url() ?>" + response);
                                        $("#ajax-loader").hide();
                                        // $("#download-excel")[0].click();
                                        location.href = '<?= base_url() ?>admin/Cases_two/index/case_approved';
                                    }
                                });
                            }
                        }
                    });
                });
//END APPROVE


                $("#not_approve").click(function () {
                    $("#case_id_").val(<?= $case_id ?>);
                    $("#not_approved_comment").modal('show');
                });


            });
        </script>
    </body>
</html>