<?php
$add = get_perm("cases", 1);
$edit = get_perm("cases", 2);
$delete = get_perm("cases", 3);
$approve_add = get_perm("approve_case", 1);
$approve_edit = get_perm("approve_case", 2);
?>
<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $this->lang->line("cases") ?></h2>
                                </div>
                                <div class="panel-content">
                                    <?php if ((isset($success) && $success != '')) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-success"><?= $success == 'case_approved' ? $this->lang->line("approved_successfully") : $this->lang->line("cases_imported_successfully") ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    
                                    <?php if ($this->session->flashdata("success")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-success"><?= $this->session->flashdata("success") ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if ($this->session->flashdata("error")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-danger"><?= $this->session->flashdata("error") ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($user_bank_exist && $add && $can_add_case < 2) { ?>
                                        <div class="text-center">
                                            <button type="button" onclick="location.href = '<?= base_url() ?>admin/Cases/borrower_information'" class="btn btn-primary btn-square"><?= $this->lang->line('add_case') ?></button>
                                            <a href = '<?= base_url() ?>admin/Bulk_csv/' class="btn btn-success btn-square"><?= $this->lang->line('import_cases_by_bulk') ?> (CSV)</a> 
                                            <a href = '<?= base_url() ?>admin/Bulk/' class="btn btn-success btn-square"><?= $this->lang->line('import_cases_by_bulk') ?> (XLSX)</a>
                                        </div>
                                    <?php } ?>
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <table class="table table-hover table-dynamic table-tools">
                                                <thead>
                                                    <tr>
                                                        <th><?= $this->lang->line('comp_name') ?></th>
                                                        <th><?= $this->lang->line('ref_no') ?></th>
                                                        <th><?= $this->lang->line('initiation_date') ?></th>
                                                        <th><?= $this->lang->line('created_by') ?></th>
                                                        <th class="text-center"><?= $this->lang->line('approved') ?></th>
                                                        <th class="text-center"><?= $this->lang->line('completed') ?></th>
                                                        <th class="text-center"><?= $this->lang->line('finalization_date') ?></th>
                                                        <th class="text-center"><?= $this->lang->line('completion_date') ?></th>
                                                        <th><?= $this->lang->line('actions') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach ($cases as $case) {
                                                        $case->created_by = $case->first_name . " " . $case->last_name;
                                                        ?>
                                                        <tr>
                                                            <td><?= $case->company_name ?></td>
                                                            <td><a href="<?= base_url() ?>admin/Cases_two/view_case/<?= $case->id ?>"><?= $case->name ?></a></td>
                                                            <td><?= date('Y/m/d', strtotime($case->date_created)) ?></td>
                                                            <td><?= $case->created_by ?></td>
                                                            <td class="text-center">
                                                                <?php
                                                                if ($case->approved == 0) {
                                                                    echo '<i title="' . $this->lang->line('waiting_approval') . '" class="text-warning" style="font-weight: bold">---</i>';
                                                                } else if ($case->approved == 1) {
                                                                    echo '<i class="fa fa-check fa-lg text-success"></i>';
                                                                } else if ($case->approved == -1) {
                                                                    echo '<i class="fa fa-close fa-lg text-danger"></i>';
                                                                    echo '<a onclick="show_comment_modal(' . $case->id . ')" style="cursor:pointer">' . $this->lang->line('show_rejection_comment') . '</a>';
                                                                }
                                                                ?>

                                                            </td>
                                                            <td class="text-center"><?php
                                                                if ($case->completed == 1) {
                                                                    $pdf_name = $case->pdf_name;
                                                                    $pdf_exp = explode(".", $pdf_name);
                                                                    if (count($pdf_exp) < 2) {
                                                                        $pdf_name.=".pdf";
                                                                    }
//                                                                                                                              echo '<i class="fa fa-check fa-lg text-success"></i>';
                                                                    ?>
                                                                    <a title="Open PDF" class="btn btn-primary btn-sm" href="<?= base_url() ?>root/<?= $case->bank_name ?>/<?= $case->branch_code ?>/Gradation Results/<?= $pdf_name ?>"><i class="fa fa-arrow-down"></i></a>
                                                                    <?php
                                                                } else {
                                                                    echo '<i class="fa fa-close fa-lg text-danger"></i>';
                                                                }
                                                                ?></td>
                                                            <td><?= $case->approved == 1 ? date('Y/m/d', strtotime($case->date_approved)) : '---' ?></td>
                                                            <td class="text-center"><?= $case->completed == 0 ? '---' : date('Y/m/d', strtotime($case->date_completed)) ?></td>

                                                            <td>
                                                                <!--allowed to reassign if allowed to edit or his case-->
                                                                <?php if ((!($case->completed == 0 && $case->approved == 1)) && (compare_level("super_admin") || compare_level("bank_admin") || compare_level("verifier"))) { ?>
                                                                    <a href="#" case_id="<?= $case->id ?>" class="reassign btn btn-sm btn-success" title="<?= $this->lang->line("reassign_case") ?>" ><i class="fa fa-user"></i></a>
                                                                <?php } ?>

                                                                <?php if ($case->completed == 1 && $can_add_case < 2) { ?>
                                                                    <a href="<?= base_url() ?>admin/Cases_two/copy_case/<?= $case->id ?>" class="edit btn btn-sm btn-warning" title="<?= $this->lang->line("copy_case") ?>" ><i class="fa fa-file"></i></a>
                                                                    <?php // } else {
                                                                    ?>
                                                                    <!--<a onclick="regenrate_ref_no(<?/= $case->id ?>)"  class="edit btn btn-sm btn-warning" title="<?//= $this->lang->line("regenrate_ref_no") ?>" ><i class="fa fa-refresh"></i></a>-->
                                                                    <?php
                                                                }
                                                                if ($case->approved != 1) {
                                                                    if ($case->status == 'saved' && ($approve_add || $approve_edit)) {// && !non_subscriber_bank($case->id)
                                                                        ?>
                                                                        <a onclick="approve_case('<?= $case->id ?>')" class=" edit btn btn-sm btn-success" data-id="<?= $case->id ?>" title="<?= $this->lang->line("approve_generate") ?>" ><i class="fa fa-check"></i></a>

                                                                        <?php if ($case->approved != -1) { ?>
                                                                            <a onclick="not_approve('<?= $case->id ?>')" class="edit btn btn-sm btn-danger" data-id="<?= $case->id ?>" title="<?= $this->lang->line("not_approved") ?>" ><i class="fa fa-ban"></i></a> 
                                                                            <?php
                                                                        }
                                                                    }
                                                                    if ($edit && !($case->status == 'saved' && $case->approved == 0)) {
                                                                        ?>
                                                                        <a href="<?= base_url() ?>admin/Cases/edit_borrower_information/<?= $case->id ?>" title="<?= $this->lang->line('edit_case') ?>" class="edit btn btn-sm btn-default"><i class="icon-note"></i></a>
                                                                        <?php
                                                                    }
                                                                } else if (compare_level("super_admin")) {
                                                                    ?>
                                                                    <a onclick="location.href='<?= base_url() ?>admin/Cases/generate_excel/<?= $case->id ?>/1'" class=" edit btn btn-sm btn-success" data-id="<?= $case->id ?>" title="<?= $this->lang->line("download_excel_file") ?>" ><i class="fa fa-download"></i></a>
                                                                    <?php
                                                                }
                                                                if ($case->status != 'saved' && $edit) {
                                                                    ?>
                                                                    <a href="<?= base_url() ?>admin/Cases_two/continue_case/<?= $case->id ?>" title="<?= $this->lang->line('continue_case') ?>" class="edit btn btn-sm btn-warning"><i class="fa fa-arrow-right"></i></a>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <?php if ((compare_level("super_admin") || $case->approved == 0) && $delete) { ?>
                                                                    <a onclick="delete_case('<?= $case->id ?>')" class="btn btn-sm btn-danger delete-case" title="<?= $this->lang->line('delete_case') ?>" data-case_id="<?= $case->id ?>" ><i class="fa fa-close"></i></a>
                                                                <?php } ?>
                                                            </td>

                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>

        <div class="modal fade" id="reassign_modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icons-office-52"></i></button>
                        <h4 class="modal-title"><?= $this->lang->line('reassign_case_to_user') ?></h4>
                    </div>
                    <form id="reassign_form" method="POST" action="<?= base_url() ?>admin/Cases_two/reassign_case" role="form" class="form-horizontal form-validation">
                        <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
                        <div class="modal-body">

                            <input type="hidden" name="reassign_case_id" id="reassign_case_id" />
                            <div class="col-md-12">
                                <label class="control-label"><?= $this->lang->line('user') ?></label>
                                <select name="reassign_user_id" id="reassign_user_id" data-search="true" class="form-control">
                                    <option value=""><?= $this->lang->line("select") ?></option>
                                    <?php foreach ($users as $user) { ?>
                                        <option value="<?= $user->id ?>"><?= $user->first_name . ' ' . $user->last_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                        </div>
                        <div class="clearfix"></div>
                        <br/>
                        <hr/>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-embossed" data-dismiss="modal"><?= $this->lang->line('close') ?></button>
                            <button type="submit" class="btn btn-primary btn-embossed"><?= $this->lang->line('reassign_case') ?></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <?php $this->load->view('admin/pages/cases/comment_modal') ?>

        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script>
                                                                        function delete_case(case_id) {
                                                                            if (window.confirm("Are you sure ? You can't undo this action")) {
                                                                                location.href = "<?= base_url() ?>admin/Cases_two/delete_case/" + case_id;
                                                                            }
                                                                        }
                                                                        function show_comment_modal(case_id) {
                                                                            $('.loader-overlay').removeClass('loaded');
                                                                            $.ajax({
                                                                                url: "<?= base_url() ?>admin/Cases_two/get_rejection_comment",
                                                                                type: "POST",
                                                                                data: {'case_id': case_id, "<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>"},
                                                                                success: function (response) {
                                                                                    $('.loader-overlay').addClass('loaded');
                                                                                    if (response !== 0) {
                                                                                        $("#case_id_").val(case_id);
                                                                                        $("#comment").val(response);
                                                                                        $("#not_approved_comment").modal('show');
                                                                                    }
                                                                                }
                                                                            });
                                                                        }
                                                                        
//                                                                BEGIN APPROVE
                                                                        function approve_case(case_id) {
                                                                            $('.loader-overlay').removeClass('loaded');
//                                                                        var case_id = $(this).data('id');

                                                                            $.ajax({
                                                                                url: "<?= base_url() ?>admin/Cases/approve_case",
                                                                                type: "POST",
                                                                                data: {'case_id': case_id, "<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>"},
                                                                                success: function (response) {
                                                                                    if (response == "incomplete") {
                                                                                        alert("<?= $this->lang->line("case_incomplete") ?>");
                                                                                        $('.loader-overlay').addClass('loaded');
//                                                                                            $("#ajax-loader").hide();
                                                                                    } else if (response == "cannot_create_file") {
                                                                                        alert("<?= $this->lang->line("cannot_create_file") ?>");
                                                                                        $('.loader-overlay').addClass('loaded');
//                                                                                            $("#ajax-loader").hide();
                                                                                    } else if (response == "file_size_error") {
                                                                                        alert("<?= $this->lang->line("file_size_error") ?>");
                                                                                        $("#ajax-loader").hide();
                                                                                    } else {
                                                                                        $.ajax({
                                                                                            url: "<?= base_url() ?>admin/Cases/replace_whole_file_content",
                                                                                            type: "POST",
                                                                                            data: {"<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>"},
                                                                                            success: function () {
                                                                                                // $("#download-excel").attr("href", "<?= base_url() ?>" + response);
//                                                                                            $("#ajax-loader").hide();
                                                                                                $('.loader-overlay').addClass('loaded');
                                                                                                // $("#download-excel")[0].click();
                                                                                                location.href = '<?= base_url() ?>admin/Cases_two/index/case_approved';
                                                                                            }
                                                                                        });

                                                                                    }
                                                                                }
                                                                            });
                                                                        }
//                                                                    END APPROVE
                                                                        function not_approve(case_id) {
                                                                            $("#case_id_").val(case_id);
                                                                            $("#not_approved_comment").modal('show');
                                                                        }

                                                                        function regenrate_ref_no(case_id) {
                                                                            var r = confirm("Are you sure you want to update the case ref. no. ?");
                                                                            if (r == true) {
                                                                                location.href = "<?= base_url() ?>admin/Cases_two/regenrate_ref_no/" + case_id;
                                                                            }

                                                                        }

                                                                        $(function () {
                                                                            //BEGIN APPROVE
//                                                                    $(".approve-generate").click(function () {
//                                                                        $('.loader-overlay').removeClass('loaded');
//                                                                        var case_id = $(this).data('id');
//
//                                                                        $.ajax({
//                                                                            url: "<?= base_url() ?>admin/Cases/approve_case",
//                                                                            type: "POST",
//                                                                            data: {'case_id': case_id, "<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>"},
//                                                                            success: function (response) {
//                                                                                if (response == "incomplete") {
//                                                                                    alert("<?= $this->lang->line("case_incomplete") ?>");
//                                                                                    $('.loader-overlay').addClass('loaded');
////                                                                                            $("#ajax-loader").hide();
//                                                                                } else if (response == "cannot_create_file") {
//                                                                                    alert("<?= $this->lang->line("cannot_create_file") ?>");
//                                                                                    $('.loader-overlay').addClass('loaded');
////                                                                                            $("#ajax-loader").hide();
//                                                                                } else {
//                                                                                    // $("#download-excel").attr("href", "<?= base_url() ?>" + response);
////                                                                                            $("#ajax-loader").hide();
//                                                                                    $('.loader-overlay').addClass('loaded');
//                                                                                    // $("#download-excel")[0].click();
//                                                                                    location.href = '<?= base_url() ?>admin/Cases_two';
//                                                                                }
//                                                                            }
//                                                                        });
//                                                                    });
                                                                            //END APPROVE
                                                                        });
                                                                        /* $(function () {
                                                                         $(".delete-case").click(function () {
                                                                         if (window.confirm("Are you sure ? You can't undo this action")) {
                                                                         location.href = "<?= base_url() ?>admin/Cases_two/delete_case/" + $(this).data('case_id');
                                                                         }
                                                                         });
                                                                         }); */
        </script>
        <script>
            $(document).ready(function () {
                $(".reassign").live("click", function (e) {
                    e.preventDefault();
                    var case_id = $(this).attr("case_id");
                    $("#reassign_case_id").val(case_id);
                    $("#reassign_user_id").val("");
                    $("#reassign_modal").modal("show");
                });
            });
        </script>
    </body>
</html>