<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
         <!-- BEGIN PAGE CSS -->
         <link href="<?=  base_url()?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
    <!-- END PAGE CSS -->
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                     <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-header bg-primary">
                  <h2 class="panel-title"><?=$this->lang->line("borrower_information")?></h2>
                </div>
                <div class="panel-content">
                    <?php if($this->session->flashdata("success")){ ?>
                     <div class="row">
                         <div class="col-md-12 col-sm-12 col-xs-12">
                             <div class="alert alert-success"><?=$this->session->flashdata("success")?></div>
                         </div>
                     </div>
                    <?php } ?>
                       <?php if($this->session->flashdata("error")){ ?>
                     <div class="row">
                         <div class="col-md-12 col-sm-12 col-xs-12">
                             <div class="alert alert-danger"><?=$this->session->flashdata("error")?></div>
                         </div>
                     </div>
                       <?php } ?>
                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div id="counter"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
                     </div>
                  
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
                
                <!--BEGIN APPROVE GENERATE-->
                <div id="approve-generate" class="btn btn-success"><?=$this->lang->line("approve_generate")?></div>
                <img id="ajax-loader" style="display:none;position: absolute; top: 30%; left: 50%;transform: translate(-50%, -50%);" src="<?=  base_url()?>assets/images/ajax-loader.gif"/>
                <a id="download-excel" style="display: none;" href="<?=  base_url()?>root/Cases/1.xlsx"></a>
                <!--END APPROVE GENERATE-->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?=  base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script>
        $(document).ready(function(){
        $("#approve-generate").click(function(){
            $("#ajax-loader").show();
            $.ajax({
            url:"<?=  base_url()?>admin/Cases/approve_case",
            type:"POST",
            data:{'case_id':"<?=$case_id?>","<?=$this->security->get_csrf_token_name()?>":"<?=$this->security->get_csrf_hash()?>"},
            success:function(response){
                if(response=="incomplete"){
                    alert("<?=$this->lang->line("case_incomplete")?>");
                     $("#ajax-loader").hide();
                }
                else{
					$.ajax({
                                    url: "<?= base_url() ?>admin/Cases/replace_whole_file_content",
                                    type: "POST",
									data:{"<?=$this->security->get_csrf_token_name()?>":"<?=$this->security->get_csrf_hash()?>"},
                                    success: function () {
                                        $("#download-excel").attr("href", "<?= base_url() ?>root/Cases/<?= $case_id ?>.xlsx");
                                        $("#ajax-loader").hide();
                                        $("#download-excel")[0].click();
                                    }
                                });
              //  $("#download-excel").attr("href","<?=  base_url()?>root/Cases/<?=$case_id?>.xlsx");
               // $("#ajax-loader").hide();
               // $("#download-excel")[0].click();
            }
            }
        });
        
        
        });

        
        });    
        </script>
    </body>
</html>