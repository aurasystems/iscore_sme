<?php
$add = get_perm("cases", 1);
$edit = get_perm("cases", 2);
?>
<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <style>
            hr{
                border-top: 5px solid #eee;
            }
            .totals input{
                background-color: #ffff82 !important;
            }
            a{
                cursor: pointer;
            }
        </style>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $this->lang->line("borrower_financials") ?></h2>
                                </div>
                                <div class="panel-content">
                                    <?php if ($this->session->flashdata("success")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-success"><?= $this->session->flashdata("success") ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if ($this->session->userdata("error")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-danger"><?= $this->session->userdata("error") ?></div>
                                            </div>
                                        </div>
                                        <?php
                                        $this->session->unset_userdata('error');
                                    }
                                    ?>
                                    <?php if (form_error("total_sales")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-danger"><?= form_error("total_sales") ?></div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <?php if (form_error("assets") && !form_error("E3")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-danger"><?= form_error("assets") ?></div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    if (form_error("E224_Total_Net_Sales")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-danger"><?= form_error("E224_Total_Net_Sales") ?></div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:auto;">
                                            <?php
                                            for ($i = 25; $i <= 32; $i++) {
                                                if ($i == 31) {
                                                    continue;
                                                }
                                                $col = 'C' . $i;
                                                if ($i != 32) {
                                                    ?>
                                                    <input type="hidden" id="i_C<?= $i ?>" value="<?= $borrower_info_data->$col ?>" />
                                                    <?php
                                                }
                                                $col = 'D' . $i;
                                                ?>
                                                <input type="hidden" id="i_D<?= $i ?>" value="<?= $borrower_info_data->$col ?>" />
                                            <?php } ?>
                                            <form id="form_1" method="POST" action="" role="form" class="form-horizontal form-validation">
                                                <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
                                                <input type="hidden" name="calc" id="calc" />
                                                <input type="hidden" name="case_id" id="case_id" />

                                                <h4><?= $this->lang->line("financial_data_template") ?></h4>
                                                <hr>
                                                <?php for ($i = 3; $i <= 4; $i++) { ?>
                                                    <div class="form-group">
                                                        <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_financial_E$i") ?></label>
                                                        <div class="col-sm-9">
                                                            <?php
                                                            $selected = '';
                                                            if ((!isset($financial_data[$i]) || isset($financial_data[$i]) && $financial_data[$i]["E"] == 'no')) {
                                                                $selected = 'selected';
                                                            }
                                                            ?>
                                                            <select name="E<?= $i ?>" id="select<?= $i ?>" class="form-control">
                                                                <option value="yes"><?= $this->lang->line('yes') ?></option>
                                                                <option value="no" <?= $selected ?>><?= $this->lang->line('no') ?></option>
                                                            </select>

                                                            <span class="text-danger"><?= form_error("E$i") ?></span>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <div id="all_form">
                                                    <br/>
                                                    <p class="bold"><?= $this->lang->line('borrower_financial_notes') ?><br/><?= $this->lang->line('borrower_financial_notes_2') ?></p>
                                                    <br/>

                                                    <div class="form-group">
                                                        <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_financial_G8") ?></label>
                                                        <div class="col-sm-9">

                                                            <?php
                                                            $data = '';
                                                            if ($this->input->post()) {
                                                                $data = $this->input->post('G8');
                                                            } else if (isset($financial_data[8])) {
                                                                $data = $financial_data[8]['G'];
                                                            }
															
                                                            ?>
                                                            <select name="G8" id="G8" class="form-control">
                                                                <option <?= $data == 'Actuals' ? 'selected' : '' ?> value="Actuals"><?= $this->lang->line('actuals') ?></option>
                                                                <option <?= $data == 'Thousands' ? 'selected' : '' ?> value="Thousands"><?= $this->lang->line('thousands') ?></option>
                                                                <option <?= $data == 'Millions' ? 'selected' : '' ?> value="Millions"><?= $this->lang->line('millions') ?></option>
                                                            </select>
                                                            <span class="text-danger"><?= form_error("G8") ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12" id="hidden_links">

                                                    </div>
                                                    <br/>
                                                    <br/>
                                                    <div class="col-sm-12">
                                                        <?php if (!isset($view_only)) { ?>        
                                                            <a class="add_hist c-orange f-28 pull-left" title="<?= $this->lang->line("add_hist") ?>" style="cursor: pointer;"><i class="fa fa-plus"></i></a>
                                                            <a class="add_proj c-primary f-28 pull-right" title="<?= $this->lang->line("add_proj") ?>" style="cursor: pointer;"><i class="fa fa-plus"></i></a>
                                                        <?php } ?>
                                                    </div>
                                                    <br/>
                                                    <br/>

                                                    <table class="table table-hover">
                                                        <thead>
                                                            <tr class="table_head">
                                                                <th style="width: 40%"></th>
                                                                <th class="text-center"><?= $borrower_info_year - 2 ?></th>
                                                                <th class="text-center"><?= $borrower_info_year - 1 ?></th>
                                                                <th class="text-center"><?= $borrower_info_year ?></th>
                                                                <th class="text-center projected"><?= $borrower_info_year + 1 ?></th>
                                                                <th class="text-center projected"><?= $borrower_info_year + 2 ?></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="table_body">
                                                            <tr class="audited">
                                                                <td></td>
                                                                <?php
                                                                $str = 'C';
                                                                for ($i = 0; $i < 3; $i++) {
                                                                    $data = '';
                                                                    if ($this->input->post()) {
                                                                        $data = $this->input->post($str . '10');
                                                                    } else if (isset($financial_data[10])) {
                                                                        $data = $financial_data[10][$str];
                                                                    }
                                                                    ?>
                                                                    <td>
                                                                        <select name="<?= $str . '10' ?>" class="form-control" >
                                                                            <option <?= $data == "Audited" ? 'selected' : '' ?> value="Audited"><?= $this->lang->line('audited') ?></option>
                                                                            <option <?= $data == "Provisional" ? 'selected' : '' ?> value="Provisional"><?= $this->lang->line('provisional') ?></option>
                                                                        </select>
                                                                        <span class="text-danger"><?= form_error($str . "10") ?></span>
                                                                    </td>
                                                                    <?php
                                                                    $str++;
                                                                }
                                                                for ($i = 0; $i < 2; $i++) {
                                                                    $value = '';
                                                                    if ($this->input->post()) {
                                                                        $value = $this->input->post($str . '10');
                                                                    } else if (isset($financial_data[10])) {
                                                                        $value = $financial_data[10][$str];
                                                                    }
                                                                    ?>
                                                                    <td>
                                                                        <input type="text" class="form-control projected" value="<?= $value ?>" name="<?= $str . '10' ?>"/>
                                                                        <span class="text-danger"><?= form_error($str . "10") ?></span>
                                                                    </td>
                                                                    <?php
                                                                    $str++;
                                                                }
                                                                ?>
                                                            </tr>

                                                            <?php
                                                            for ($i = 12; $i <= 224; $i++) {
                                                                if ($i == 16 || $i == 17 || $i == 25 || $i == 47 || $i == 75 || $i == 77 || $i == 105 || $i == 125 || $i == 123 || $i == 143 || $i == 152 ||
                                                                        $i == 173 || $i == 174 || $i == 182 || $i == 188 || $i == 194 || $i == 202) {
                                                                    if ($i == 75) {
                                                                        echo '<tr style="height:120px"><td class="colspan" colspan="6"><strong><hr/></strong></td></tr>';
                                                                    }
                                                                    ?>
                                                                    <tr><td class="colspan" colspan="6"><strong><?= $this->lang->line("borrower_financial_$i") ?></strong></td></tr>
                                                                    <?php
                                                                    continue;
                                                                }
                                                                ?>
                                                                <?php
                                                                if (($i >= 15 && $i <= 17) ||
                                                                        $i == 21 ||
                                                                        ($i > 23 && $i < 25) ||
                                                                        ($i > 29 && $i < 31) || $i == 37 ||
                                                                        $i == 42 || $i == 40 ||
                                                                        $i == 44 || $i == 46 ||
                                                                        ($i > 52 && $i < 54) || $i == 57 ||
                                                                        $i == 59 || $i == 61 || $i == 64 ||
                                                                        $i == 68 || $i == 70 || $i == 72 ||
                                                                        ($i >= 74 && $i <= 77) || $i == 96 ||
                                                                        $i == 102 || ($i >= 104 && $i < 105) || $i == 118 ||
                                                                        $i == 120 || ($i >= 122 && $i <= 125) ||
                                                                        ($i > 141 && $i < 144) ||
                                                                        ($i > 150 && $i <= 152) || $i == 165 || $i == 169 || $i == 172 ||
                                                                        $i == 181 || $i == 193 || $i == 201 || $i == 215 || $i == 216 || $i == 217) {
                                                                    if ($i == 96) {
                                                                        echo '<tr style="height:40px"><td class="colspan" colspan="6"><strong>' . $this->lang->line("borrower_financial_96") . '</strong></td></tr>';
                                                                    } else {
                                                                        echo '<tr style="height:40px"><td class="colspan" colspan="6"></td></tr>';
                                                                    }
                                                                    continue;
                                                                }
                                                                ?>
                                                                <?php
                                                                $value_C = '';
                                                                $value_D = '';
                                                                $value_E = '';
                                                                $value_F = '';
                                                                $value_G = '';
                                                                if ($this->input->post()) {
                                                                    $value_C = $this->input->post("C$i");
                                                                    $value_D = $this->input->post("D$i");
                                                                    $value_E = $this->input->post("E$i");
                                                                    $value_F = $this->input->post("F$i");
                                                                    $value_G = $this->input->post("G$i");
                                                                } else if (isset($financial_data[$i])) {
                                                                    $value_C = $financial_data[$i]['C'];
                                                                    $value_D = $financial_data[$i]['D'];
                                                                    $value_E = $financial_data[$i]['E'];
                                                                    $value_F = $financial_data[$i]['F'];
                                                                    $value_G = $financial_data[$i]['G'];
                                                                }
                                                                ?>
                                                                <?php if ($i == 13) { ?>
                                                                    <tr class="months">
                                                                        <td><?= $this->lang->line("borrower_financial_$i") ?></td>
                                                                        <td><select name="C<?= $i ?>" id="C<?= $i ?>" class="form-control"><option <?= $value_C == 3 ? 'selected' : '' ?> value="3">3</option><option <?= $value_C == 6 ? 'selected' : '' ?> value="6">6</option><option <?= $value_C == 9 ? 'selected' : '' ?> value="9">9</option><option <?= $value_C == 12 ? 'selected' : '' ?> value="12">12</option></select><span class="text-danger"><?= form_error("C$i") ?></span></td>
                                                                        <td><select name="D<?= $i ?>" id="D<?= $i ?>" class="form-control"><option <?= $value_D == 3 ? 'selected' : '' ?> value="3">3</option><option <?= $value_D == 6 ? 'selected' : '' ?> value="6">6</option><option <?= $value_D == 9 ? 'selected' : '' ?> value="9">9</option><option <?= $value_D == 12 ? 'selected' : '' ?> value="12">12</option></select><span class="text-danger"><?= form_error("D$i") ?></span></td>
                                                                        <td><select name="E<?= $i ?>" id="E<?= $i ?>" class="form-control"><option <?= $value_E == 3 ? 'selected' : '' ?> value="3">3</option><option <?= $value_E == 6 ? 'selected' : '' ?> value="6">6</option><option <?= $value_E == 9 ? 'selected' : '' ?> value="9">9</option><option <?= $value_E == 12 ? 'selected' : '' ?> value="12">12</option></select><span class="text-danger"><?= form_error("E$i") ?></span></td>
                                                                        <td><select name="F<?= $i ?>" id="F<?= $i ?>" class="form-control projected"><option <?= $value_F == 3 ? 'selected' : '' ?> value="3">3</option><option <?= $value_F == 6 ? 'selected' : '' ?> value="6">6</option><option <?= $value_F == 9 ? 'selected' : '' ?> value="9">9</option><option <?= $value_F == 12 ? 'selected' : '' ?> value="12">12</option></select><span class="text-danger"><?= form_error("F$i") ?></span></td>
                                                                        <td><select name="G<?= $i ?>" id="G<?= $i ?>" class="form-control projected"><option <?= $value_G == 3 ? 'selected' : '' ?> value="3">3</option><option <?= $value_G == 6 ? 'selected' : '' ?> value="6">6</option><option <?= $value_G == 9 ? 'selected' : '' ?> value="9">9</option><option <?= $value_G == 12 ? 'selected' : '' ?> value="12">12</option></select><span class="text-danger"><?= form_error("G$i") ?></span></td>
                                                                    </tr>
                                                                    <?php
                                                                    continue;
                                                                } else if ($i == 12 || $i == 20 || $i == 23 || $i == 29 || $i == 31 || $i == 41 || $i == 45 || $i == 48 || $i == 52 ||
                                                                        $i == 54 || $i == 60 || $i == 65 || $i == 69 || $i == 73 || $i == 91 || $i == 95 || $i == 101 || $i == 103 ||
                                                                        $i == 116 || $i == 121 || $i == 128 || $i == 131 || $i == 141 || $i == 144 || $i == 150 || $i == 157 || $i == 164 ||
                                                                        $i == 168 || $i == 170 || $i == 171 || ($i >= 175 && $i <= 180) || ($i >= 183 && $i <= 187) ||
                                                                        ($i >= 189 && $i <= 192) || ($i >= 195 && $i <= 200) || ($i >= 203 && $i <= 214) || ($i >= 218 && $i <= 224)) {
                                                                    ?>
                                                                    <tr class="totals" data-i="<?= $i ?>" style="background-color: #ffff82">
                                                                        <td><?= $this->lang->line("borrower_financial_$i") ?></td>
                                                                        <td><input type="text" id="C<?= $i ?>" class="form-control" disabled /></td>
                                                                        <td><input type="text" id="D<?= $i ?>" class="form-control" disabled /></td>
                                                                        <td><input type="text" id="E<?= $i ?>" class="form-control" disabled /></td>
                                                                        <td><input type="text" id="F<?= $i ?>" class="projected form-control" disabled /></td>
                                                                        <td><input type="text" id="G<?= $i ?>" class="projected form-control" disabled /></td>
                                                                    </tr>
                                                                    <?php
                                                                    continue;
                                                                }
                                                                ?>

                                                                <tr class="data" data-i="<?= $i ?>" >
                                                                    <td><?= $this->lang->line("borrower_financial_$i") ?></td>
                                                                    <td><input type="text" name="C<?= $i ?>" class="form-control" id="C<?= $i ?>" value="<?= $value_C ?>" ><span class="text-danger"><?= form_error("C$i") ?></span></td>
                                                                    <td><input type="text" name="D<?= $i ?>" class="form-control" id="D<?= $i ?>"  value="<?= $value_D ?>" ><span class="text-danger"><?= form_error("D$i") ?></span></td>
                                                                    <td><input type="text" name="E<?= $i ?>" class="form-control" id="E<?= $i ?>" value="<?= $value_E ?>" ><span class="text-danger"><?= form_error("E$i") ?></span></td>
                                                                    <td><input type="text" name="F<?= $i ?>" class="form-control projected" id="F<?= $i ?>" value="<?= $value_F ?>" ><span class="text-danger"><?= form_error("F$i") ?></span></td>
                                                                    <td><input type="text" name="G<?= $i ?>" class="form-control projected" id="G<?= $i ?>" value="<?= $value_G ?>" ><span class="text-danger"><?= form_error("G$i") ?></span></td>
                                                                </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>

                                                </div>

                                                <input type="hidden" value="0" name="continue_later" id="continue_later"/>
                                                <input type="hidden" id="total_sales" name="total_sales" value="total_sales"/>
                                                <input type="hidden" id="assets" name="assets" value="assets"/>
                                                <input type="hidden" id="E224_Total_Net_Sales" name="E224_Total_Net_Sales" />
                                                <br/>
                                                <hr/>
                                                <div class="row">
                                                    <div class="col-sm-9">
                                                        <div class="pull-left">
                                                            <a class="btn btn-embossed btn-primary m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases/borrower_information/<?= $case_id ?>/<?= isset($view_only) ? '1' : '' ?>'"><?= $this->lang->line("borrower_information") ?></a>
                                                            <a class="btn btn-embossed btn-primary m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases_two/borrower_factsheet/<?= $case_id ?>/<?= isset($view_only) ? '1' : '' ?>'"><?= $this->lang->line("borrower_factsheet") ?></a>
                                                            <a class="btn btn-embossed btn-primary m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases_two/borrower_assessment/<?= $case_id ?>/<?= isset($view_only) ? '1' : '' ?>'"><?= $this->lang->line("borrower_assessment") ?></a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-9 col-sm-offset-3">
                                                        <div class="pull-right">
                                                            <?php
                                                            if (!isset($view_only)) {
                                                                if ($add || $edit) {
                                                                   // if ($is_case_pass_this_setp) {
                                                                        ?>
                                                                        <button type="submit" id="save_and_continue_later" class="cancel btn btn-embossed btn-default m-b-10 m-r-0"><?= $this->lang->line("save_continue_later") ?></button>
                                                                    <?php //} ?>
                                                                    <!--//modified-->
                                                                    <a  id="save_next" class="cancel btn btn-embossed btn-primary m-r-20"><?= $this->lang->line("save_next") ?></a>
                                                                <?php }
                                                                ?>
                                                                <a class="btn btn-embossed btn-danger m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases_two'"><?= $this->lang->line('discard'); ?></a>
                                                            <?php } else {
                                                                ?>
                                                                <a class="btn btn-embossed btn-danger m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases_two'"><?= $this->lang->line('close'); ?></a>
                                                                <a class="btn btn-embossed btn-primary m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases_two/borrower_assessment/<?= $case_id ?>/1'"><?= $this->lang->line("next") ?></a>
                                                            <?php } ?>
                                                            <a class="back-btn btn btn-embossed btn-primary m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases_two/borrower_factsheet/<?= $case_id ?>/<?= isset($view_only) ? 1 : '' ?>'"><?= $this->lang->line("back") ?></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="deleted_historical" name="deleted_historical" value="<?= set_value('deleted_historical') ?>" />
                                                <input type="hidden" id="deleted_projected" name="deleted_projected" value="<?= set_value('deleted_projected') ?>" />
                                                <input type="hidden" id="hidden_years" name="hidden_years" value="<?= $this->input->post() ? set_value('hidden_years') : $hidden_years ?>" />
                                                <input type="hidden" id="last_projected" name="last_projected" value="0" />
                                                <input type="hidden" id="first_projected" name="first_projected" value="0"/>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/js/dynamic_borrower_financials.js"></script>
        <script src="<?= base_url() ?>assets/js/borrower_financials.js"></script>
        <script>

                                                                $(document).ready(function () {

                                                                    $("#C12").val('<?= date('d-m-Y', strtotime("-2 year", strtotime($borrower_info_date))) ?>');
                                                                    $("#D12").val('<?= date('d-m-Y', strtotime("-1 year", strtotime($borrower_info_date))) ?>');
                                                                    $("#E12").val('<?= date('d-m-Y', strtotime($borrower_info_date)) ?>');
                                                                    $("#F12").val('<?= date('d-m-Y', strtotime("+1 year", strtotime($borrower_info_date))) ?>');
                                                                    $("#G12").val('<?= date('d-m-Y', strtotime("+2 year", strtotime($borrower_info_date))) ?>');



<?php if (!empty($financial_data)) { ?>
                                                                        adjust_totals();
<?php } ?>

                                                                    $("#save_and_continue_later").click(function () {
                                                                        $("#continue_later").val("1");
                                                                        // $("input").each(function (index, value) {
                                                                        //$(this).removeAttr("required");
                                                                        //  });
                                                                        $("#form_1").submit();
                                                                    });
                                                                    //modified
                                                                    $("#save_next").click(function () {
                                                                        console.log("in");
                                                                        var calc = {};
                                                                        $(".totals input").each(function (index, value) {
                                                                            calc[$(this).attr("id")] = $(this).val();
                                                                        });
                                                                        calc['first_hist'] = $("#first_projected").val();
                                                                        calc['last_proj'] = $("#last_projected").val();
                                                                        calc['last_hist'] = "<?= $borrower_info_year ?>";
                                                                        calc['first_proj'] = "<?= $borrower_info_year + 2 ?>";
                                                                        $("#calc").val(JSON.stringify(calc));
                                                                        $("#case_id").val("<?= $case_id ?>");
//                                                                        $.ajax({
//                                                                            url: "<?= base_url() ?>admin/Cases/save_calc",
//                                                                            type: "POST",
//                                                                            data: {'case_id': "<?= $case_id ?>", 'calc': calc},
//                                                                            success: function (result) {
//                                                                            alert(result);
                                                                        $("#form_1").submit();
//                                                                            },
//                                                                            error: function (jqXHR, exception) {
//                                                                                console.log(jqXHR);
//                                                                                alert("Uncaught error. Form can not be submitted");
//                                                                            }
//                                                                        });
//                                                                        $("#form_1").submit();
                                                                    });
                                                                    //modifed
                                                                    $("#select4").change(function () {

                                                                        if ($("#select4").val() == 'yes') {
                                                                            // $(".projected").not("select").attr('required', true);
                                                                            $(".projected").not('select').show();  // becouse select2 make div and hide my original select so not to show my original select
                                                                            $(".add_proj").show();
//                                                                            $(".add_hist").show();

                                                                            $("#last_projected").val(last_projected);
                                                                            $("#first_projected").val(first_projected);
                                                                        } else {
                                                                            // $(".projected").removeAttr('required');
                                                                            $(".projected").hide();
                                                                            $(".add_proj").hide();
//                                                                            $(".add_hist").hide();

                                                                            $("#last_projected").val("0");
                                                                            //  $("#first_projected").val("0");
                                                                        }
                                                                    });

                                                                    $("#select3").change(function () {
                                                                        if ($("#select3").val() == 'yes') {
                                                                            //  $("input").attr('required', true);
                                                                            $("#all_form").show();
                                                                        } else {
                                                                            // $("input").removeAttr('required');
                                                                            $("#all_form").hide();
                                                                        }
                                                                    });
<?php if ($this->input->post()) { ?>
                                                                        $("#select3").val('<?= $this->input->post('E3') ?>');
                                                                        $("#select4").val('<?= $this->input->post('E4') ?>');

<?php } ?>
                                                                    $("#select3").change();
                                                                    $("#select4").change();






<?php if (isset($view_only)) { ?>

                                                                        $('select').each(function () {
                                                                            $(this).not(':hidden').replaceWith("<span class='view_span'>" + $(this).find('option:selected').text() + "</span>");
                                                                        });
                                                                        var interval = setInterval(function () {
                                                                            $('.loader-overlay').removeClass('loaded');
                                                                        }, 100);
                                                                        setTimeout(function () {

                                                                            $(".c-red").hide();
                                                                            $('input').each(function () {
                                                                                $(this).not(':hidden').replaceWith("<span class='view_span'>" + $(this).val() + "</span>");
                                                                            });

                                                                            $('textarea').each(function () {
                                                                                $(this).not(':hidden').replaceWith("<span class='view_span'>" + $(this).val() + "</span>");
                                                                            });

                                                                            clearInterval(interval);
                                                                            $('.loader-overlay').addClass('loaded');
                                                                        }, 6000);
<?php } ?>
                                                                });
        </script>
        <!--BEGIN PROJECTED-->
        <script>
            $(document).ready(function () {
                $(".add_proj").hide();
                //                $(".add_hist").hide();
                if ($("#select4").val() == "yes") {
                    $(".add_proj").show();
                    $(".add_hist").show();
                }
                //                $("#select3").val("yes");
                //                $("#select3").trigger("change"); //                $("#select4").val("yes");
//                $("#select4").trigger("change");

                var colspan = 6;
                var first_projected = parseInt("<?= $first_hist; ?>");
                var last_projected = parseInt("<?= $last_projected ?>");
                var date = "<?= date('Y-m-d', strtotime("-2 year", strtotime($borrower_info_date))) ?>";
                date = new Date(date);
                $(".add_proj").click(function () {
                    $('.loader-overlay').removeClass('loaded');
                    last_projected += 1;
                    $("#last_projected").val(last_projected);
                    colspan += 1;
                    var header = "<th class='projected text-center " + last_projected + "'><a class='pull-left hide_col' data-col-index='" + last_projected + "' title='<?= $this->lang->line('hide') ?>'><i class='fa fa-ban fa-lg c-blue' ></i></a>" + last_projected + "<a class='pull-right delete_col' data-projected='1' data-col-index='" + last_projected + "' title='<?= $this->lang->line('delete') ?>'><i class='fa fa-close fa-lg c-red'></i></a></th>";
                    var td = "";
                    var i = 0;
                    var totals_td = "";
                    var audited_td = "";
                    var months_td = "";
                    $(".table_head").append(header);
                    $(".data").each(function (index, row) {
                        i = $(this).attr("data-i");
                        td = '<td class="' + last_projected + '"><input type="text" name="' + last_projected + i + '" class="form-control projected" id="' + last_projected + i + '" value="" ><span class="text-danger"></span></td>';
                        $(this).append(td);
                    });
                    $(".totals").each(function (index, row) {
                        i = $(this).attr("data-i");
                        totals_td = '<td class="' + last_projected + '"><input type="text" id="' + last_projected + i + '" class="projected form-control" disabled /></td>';
                        $(this).append(totals_td);
                        if (i == '12') {
                            var mon = parseInt(date.getMonth()) + 1;
                            $("#" + last_projected + i).val(date.getDate() + "-" + mon + "-" + last_projected);
                        }
                    });
                    $(".audited").each(function (index, row) {
                        audited_td = '<td class="' + last_projected + '"><input type="text" class="form-control projected" value="" name="' + last_projected + '10"/><span class="text-danger"></span></td>';
                        $(this).append(audited_td);
                    });
                    $(".months").each(function (index, row) {
                        months_td = '<td class="' + last_projected + '"><select name="' + last_projected + '13" id="' + last_projected + '13" class="form-control projected"><option value="3">3</option><option value="6">6</option><option value="9">9</option><option value="12">12</option></select><span class="text-danger"></span></td>';
                        var ident = '#' + last_projected + '13';
                        $(this).append(months_td);
                        //                        $(ident).val("3");
                        //                        $(ident).trigger("change");
                        $(ident).select2();
                    });
                    $(".colspan").each(function (index, row) {
                        $(this).attr("colspan", colspan);
                    });
                    proj_calc(last_projected);
                    $('.loader-overlay').addClass('loaded');
                });

                $(".add_hist").click(function () {
                    $('.loader-overlay').removeClass('loaded');
                    first_projected -= 1;
                    $("#first_projected").val(first_projected);
                    colspan += 1;
                    var header = "<th class=' text-center " + first_projected + "'><a class='pull-left hide_col' data-col-index='" + first_projected + "' title='<?= $this->lang->line('hide') ?>'><i class='fa fa-ban fa-lg c-blue' ></i></a>" + first_projected + "<a class='pull-right delete_col' data-col-index='" + first_projected + "' title='<?= $this->lang->line('delete') ?>'><i class='fa fa-close fa-lg c-red pull-right'></i></a></th>";
                    var td = "";
                    var i = 0;
                    var totals_td = "";
                    var audited_td = "";
                    var months_td = "";
                    $(".table_head > th:nth-child(1)").after(header);
                    $(".data").each(function (index, row) {
                        i = $(this).attr("data-i");
                        td = '<td class="' + first_projected + '"><input type="text" name="' + first_projected + i + '" class="form-control " id="' + first_projected + i + '" value="" ><span class="text-danger"></span></td>';
                        $(this).children("td:nth-child(1)").after(td);
                    });
                    $(".totals").each(function (index, row) {
                        i = $(this).attr("data-i");
                        totals_td = '<td class="' + first_projected + '"><input type="text" id="' + first_projected + i + '" class=" form-control" disabled /></td>';
                        $(this).children("td:nth-child(1)").after(totals_td);
                        if (i == '12') {
                            var mon = parseInt(date.getMonth()) + 1;
                            $("#" + first_projected + i).val(date.getDate() + "-" + mon + "-" + first_projected);
                        }
                    });
                    $(".audited").each(function (index, row) {
                        audited_td = '<td class="' + first_projected + '"><select class="form-control" name="' + first_projected + '10" id="' + first_projected + '10" ><option value="Audited"><?= $this->lang->line('audited') ?></option><option value="Provisional"><?= $this->lang->line('provisional') ?></option></select><span class="text-danger"></span></td>';
                        $(this).children("td:nth-child(1)").after(audited_td);
                        var ident = '#' + first_projected + '10';
                        $(ident).select2();
                    });
                    $(".months").each(function (index, row) {
                        months_td = '<td class="' + first_projected + '"><select name="' + first_projected + '13" id="' + first_projected + '13" class="form-control"><option value="3">3</option><option value="6">6</option><option value="9">9</option><option value="12">12</option></select><span class="text-danger"></span></td>';
                        var ident = '#' + first_projected + '13';
                        $(this).children("td:nth-child(1)").after(months_td);
                        $(ident).select2();
                    });
                    $(".colspan").each(function (index, row) {
                        $(this).attr("colspan", colspan);
                    });
                    proj_calc(first_projected);
                    $('.loader-overlay').addClass('loaded');
                });

                //begin sett ing existing projected 
                function set_existing_hist() {
                    $("#first_projected").val(first_projected);
                    var hist =<?= json_encode($hist) ?>;

                    var borrower_year = "<?= $borrower_info_year - 3 ?>";

                    for (var year = borrower_year; year >= first_projected; year--) {
                        colspan += 1;
                        var header = "<th class=' text-center " + year + "'><a class='pull-left hide_col' data-col-index='" + year + "' title='<?= $this->lang->line('hide') ?>'><i class='fa fa-ban fa-lg c-blue' ></i></a>" + year + "<a class='pull-right delete_col' data-col-index='" + year + "' title='<?= $this->lang->line('delete') ?>'><i class='fa fa-close fa-lg c-red pull-right'></i></a></th>";
                        var td = "";
                        var i = 0;
                        var totals_td = "";
                        var audited_td = "";
                        var months_td = "";
                        // alert(hist[11][2014]);
//                        if(hist[10][year]){
                        //                            continue;
//                        }
                        var hist_inserted_years = JSON.stringify(<?= json_encode($hist_years) ?>);
                        if (!hist_inserted_years.includes(year)) {
                            continue;
                        }
<?php if ($this->input->post()) { ?>
                            var post = <?= json_encode($this->input->post()) ?>;

                            if (post['deleted_historical'] != '') {
                                var proj_inserted_years = post['deleted_historical'].split(',');
                                if (proj_inserted_years.includes(year.toString())) {
                                    continue;
                                }
                            }
<?php } ?>
                        $(".table_head > th:nth-child(1)").after(header);
                        $(".data").each(function (index, row) {
                            i = $(this).attr("data-i");
                            var value = "";
                            if (!$.isEmptyObject(hist[i]) && hist[i].hasOwnProperty(year)) {
                                value = hist[i][year];
                            }
                            td = '<td class="' + year + '"><input type="text" name="' + year + i + '" class="form-control projected" id="' + year + i + '" value="' + value + '" ><span class="text-danger"></span></td>';
                            $(this).children("td:nth-child(1)").after(td);
                        });
                        $(".totals").each(function (index, row) {
                            i = $(this).attr("data-i");
                            totals_td = '<td class="' + year + '"><input type="text" id="' + year + i + '" class=" form-control" disabled /></td>';
                            $(this).children("td:nth-child(1)").after(totals_td);
                            if (i == '12') {
                                var mon = parseInt(date.getMonth()) + 1;
                                $("#" + year + i).val(date.getDate() + "-" + mon + "-" + year);
                            }
                        });
                        $(".audited").each(function (index, row) {
                            var value = "Audited";
                            if (!$.isEmptyObject(hist[10]) && hist[10].hasOwnProperty(year)) {
                                value = hist[10][year];
                            }
                            audited_td = '<td class="' + year + '"><select class="form-control" name="' + year + '10" id="' + year + '10" ><option value="Audited"><?= $this->lang->line('audited') ?></option><option value="Provisional"><?= $this->lang->line('provisional') ?></option></select><span class="text-danger"></span></td>';
                            $(this).children("td:nth-child(1)").after(audited_td);
                            var ident = '#' + year + '10';


                            $(ident).val(value);
<?php if (isset($view_only)) { ?>
                                $(ident).replaceWith("<span class='view_span'>" + $(ident).find('option:selected').text() + "</span>");
<?php } ?>
                            $(ident).select2();
                            $(ident).trigger("change");
                        });
                        $(".months").each(function (index, row) {
                            months_td = '<td class="' + year + '"><select name="' + year + '13" id="' + year + '13" class="form-control "><option value="3">3</option><option value="6">6</option><option value="9">9</option><option value="12">12</option></select><span class="text-danger"></span></td>';
                            var ident = '#' + year + '13';
                            $(this).children("td:nth-child(1)").after(months_td);


                            var value = "3";
                            if (!$.isEmptyObject(hist[13]) && hist[13].hasOwnProperty(year)) {
                                value = hist[13][year];
                            }
                            $(ident).val(value);
<?php if (isset($view_only)) { ?>
                                $(ident).replaceWith("<span class='view_span'>" + $(ident).find('option:selected').text() + "</span>");
<?php } ?>
                            $(ident).select2();
                            $(ident).trigger("change");
                        });
                        $(".colspan").each(function (index, row) {
                            $(this).attr("colspan", colspan);
                        });
                        proj_calc(year);
                    }
                }
                set_existing_hist();
                function set_existing_proj() {

                    $("#last_projected").val(last_projected);
                    var proj =<?= json_encode($projected) ?>;
                    //                    console.log(hist[1300]===undefined || !hist[1300].hasOwnProperty(year));
                    var borrower_year = "<?= $borrower_info_year + 3 ?>";
                    console.log(borrower_year);
                    console.log(last_projected);
                    for (var year = borrower_year; year <= last_projected; year++) {
                        colspan += 1;
                        var header = "<th class='projected text-center " + year + "'><a class='pull-left hide_col' data-col-index='" + year + "' title='<?= $this->lang->line('hide') ?>'><i class='fa fa-ban fa-lg c-blue' ></i></a>" + year + "<a class='pull-right delete_col' data-projected='1' data-col-index='" + year + "' title='<?= $this->lang->line('delete') ?>'><i class='fa fa-close fa-lg c-red'></i></a></th>";
                        var td = "";
                        var i = 0;
                        var totals_td = "";
                        var audited_td = "";
                        var months_td = "";
<?php if ($this->input->post()) { ?>
                            var post = <?= json_encode($this->input->post()) ?>;

                            if (post['deleted_projected'] != '') {
                                var proj_inserted_years = post['deleted_projected'].split(',');

                                if (proj_inserted_years.includes(year.toString())) {
                                    continue;
                                }
                            }
<?php } ?>

                        var proj_inserted_years = JSON.stringify(<?= json_encode($projected_years) ?>);

                        if (!proj_inserted_years.includes(year)) {
                            continue;
                        }
                        $(".table_head").append(header);
                        $(".data").each(function (index, row) {
                            i = $(this).attr("data-i");
                            var value = "";
                            if (!$.isEmptyObject(proj[i]) && proj[i].hasOwnProperty(year)) {
                                value = proj[i][year];
                            }
                            td = '<td class="' + year + '"><input type="text" name="' + year + i + '" class="form-control projected" id="' + year + i + '" value="' + value + '" ><span class="text-danger"></span></td>';
                            $(this).append(td);
                        });
                        $(".totals").each(function (index, row) {
                            i = $(this).attr("data-i");
                            totals_td = '<td class="' + year + '"><input type="text" id="' + year + i + '" class="projected form-control" disabled /></td>';
                            $(this).append(totals_td);
                            if (i == '12') {
                                var mon = parseInt(date.getMonth()) + 1;
                                $("#" + year + i).val(date.getDate() + "-" + mon + "-" + year);
                            }
                        });
                        $(".audited").each(function (index, row) {
                            var value = "";
                            if (!$.isEmptyObject(proj[10]) && proj[10].hasOwnProperty(year)) {
                                value = proj[10][year];
                            }
                            audited_td = '<td class="' + year + '"><input type="text" class="form-control projected" value="' + value + '" name="' + year + '10"/><span class="text-danger"></span></td>';
                            $(this).append(audited_td);
                        });
                        $(".months").each(function (index, row) {
                            months_td = '<td class="' + year + '"><select name="' + year + '13" id="' + year + '13" class="form-control projected"><option value="3">3</option><option value="6">6</option><option value="9">9</option><option value="12">12</option></select><span class="text-danger"></span></td>';
                            var ident = '#' + year + '13';
                            $(this).append(months_td);

                            var value = "3";
                            if (!$.isEmptyObject(proj[13]) && proj[13].hasOwnProperty(year)) {
                                value = proj[13][year];
                            }
                            $(ident).val(value);
<?php if (isset($view_only)) { ?>
                                $(ident).replaceWith("<span class='view_span'>" + $(ident).find('option:selected').text() + "</span>");
<?php } ?>
                            $(ident).select2();
                            $(ident).trigger("change");
                        });
                        $(".colspan").each(function (index, row) {
                            $(this).attr("colspan", colspan);
                        });
                        proj_calc(year);
                    }
                }
                set_existing_proj();
                /////////////////////////////////
//                BEGIN SETTING POST
                function set_post_hist() {
                    var post = <?= json_encode($this->input->post()) ?>;
                    first_projected = "<?= $this->input->post("first_projected") ?>";

                    $("#first_projected").val(first_projected);
                    var borrower_year = "<?= $borrower_info_year - 3 ?>";
                    for (var year = borrower_year - 1; year >= first_projected; year--) {
                        colspan += 1;
                        var header = "<th class=' text-center " + year + "'><a class='pull-left hide_col' data-col-index='" + year + "' title='<?= $this->lang->line('hide') ?>'><i class='fa fa-ban fa-lg c-blue' ></i></a>" + year + "<a class='pull-right delete_col' data-col-index='" + year + "' title='<?= $this->lang->line('delete') ?>'><i class='fa fa-close fa-lg c-red pull-right'></i></a></th>";
                        var td = "";
                        var i = 0;
                        var totals_td = "";
                        var audited_td = "";
                        var months_td = "";
                        if (post['deleted_historical'] != '') {
                            var proj_inserted_years = post['deleted_historical'].split(',');
                            if (proj_inserted_years.includes(year)) {
                                continue;
                            }
                        }
                        var hist_inserted_years = JSON.stringify(<?= json_encode($hist_years) ?>);

                        if (hist_inserted_years.includes(year)) {
                            continue;
                        }



                        $(".table_head > th:nth-child(1)").after(header);
                        $(".data").each(function (index, row) {
                            i = $(this).attr("data-i");
                            var value = "";
                            if (!$.isEmptyObject(post[year + i]) && post[year + i] != undefined) {
                                value = post[year + i];
                            }
                            td = '<td class="' + year + '"><input type="text" name="' + year + i + '" class="form-control " id="' + year + i + '" value="' + value + '" ><span class="text-danger"></span></td>';
                            $(this).children("td:nth-child(1)").after(td);
                        });
                        $(".totals").each(function (index, row) {
                            i = $(this).attr("data-i");
                            totals_td = '<td class="' + year + '"><input type="text" id="' + year + i + '" class=" form-control" disabled /></td>';
                            $(this).children("td:nth-child(1)").after(totals_td);
                            if (i == '12') {
                                var mon = parseInt(date.getMonth()) + 1;
                                $("#" + first_projected + i).val(date.getDate() + "-" + mon + "-" + first_projected);
                            }
                        });
                        $(".audited").each(function (index, row) {
                            var value = "Audited";
                            if (!$.isEmptyObject(post[year + "10"]) && post[year + "10"] != undefined) {
                                value = post[year + "10"];
                            }
                            audited_td = '<td class="' + year + '"><select class="form-control" name="' + year + '10" id="' + year + '10" ><option value="Audited"><?= $this->lang->line('audited') ?></option><option value="Provisional"><?= $this->lang->line('provisional') ?></option></select><span class="text-danger"></span></td>';
                            $(this).children("td:nth-child(1)").after(audited_td);
                            var ident = '#' + year + '10';


                            $(ident).val(value);
<?php if (isset($view_only)) { ?>
                                $(ident).replaceWith("<span class='view_span'>" + $(ident).find('option:selected').text() + "</span>");
<?php } ?>
                            $(ident).select2();
                            $(ident).trigger("change");
                        });
                        $(".months").each(function (index, row) {
                            months_td = '<td class="' + year + '"><select name="' + year + '13" id="' + year + '13" class="form-control "><option value="3">3</option><option value="6">6</option><option value="9">9</option><option value="12">12</option></select><span class="text-danger"></span></td>';
                            var ident = '#' + year + '13';
                            $(this).children("td:nth-child(1)").after(months_td);


                            var value = "3";
                            if (!$.isEmptyObject(post[year + "13"]) && post[year + "13"] != undefined) {
                                value = post[year + "13"];
                            }
                            $(ident).val(value);
<?php if (isset($view_only)) { ?>
                                $(ident).replaceWith("<span class='view_span'>" + $(ident).find('option:selected').text() + "</span>");
<?php } ?>
                            $(ident).select2();
                            $(ident).trigger("change");
                        });
                        $(".colspan").each(function (index, row) {
                            $(this).attr("colspan", colspan);
                        });
                        proj_calc(year);
                    }
                }
                function set_post_proj() {
                    last_projected = "<?= $this->input->post("last_projected") ?>";
                    $("#last_projected").val(last_projected);
                    var post =<?= json_encode($this->input->post()) ?>;
                    //                    console.log(hist[1300]===undefined || !hist[1300].hasOwnProperty(year));
                    var borrower_year = parseInt("<?= $borrower_info_year + 3 ?>");
                    for (var year = borrower_year + 1; year <= last_projected; year++) {
                        colspan += 1;
                        var header = "<th class='projected text-center " + year + "'><a class='pull-left hide_col' data-col-index='" + year + "' title='<?= $this->lang->line('hide') ?>'><i class='fa fa-ban fa-lg c-blue' ></i></a>" + year + "<a class='pull-right delete_col' data-projected='1' data-col-index='" + year + "' title='<?= $this->lang->line('delete') ?>'><i class='fa fa-close fa-lg c-red'></i></a></th>";
                        var td = "";
                        var i = 0;
                        var totals_td = "";
                        var audited_td = "";
                        var months_td = "";
                        if (post['deleted_projected'] != '') {
                            var proj_inserted_years = post['deleted_projected'].split(',');
                            if (proj_inserted_years.includes(year)) {
                                continue;
                            }
                        }
                        var proj_inserted_years = JSON.stringify(<?= json_encode($projected_years) ?>);

                        if (proj_inserted_years.includes(year)) {
                            continue;
                        }

                        $(".table_head").append(header);
                        $(".data").each(function (index, row) {
                            i = $(this).attr("data-i");
                            var value = "";
                            if (!$.isEmptyObject(post[year + i]) && post[year + i] != undefined) {
                                value = post[year + i];
                            }
                            td = '<td class="' + year + '"><input type="text" name="' + year + i + '" class="form-control projected" id="' + year + i + '" value="' + value + '" ><span class="text-danger"></span></td>';
                            $(this).append(td);
                        });
                        $(".totals").each(function (index, row) {
                            i = $(this).attr("data-i");
                            totals_td = '<td class="' + year + '"><input type="text" id="' + year + i + '" class="projected form-control" disabled /></td>';
                            $(this).append(totals_td);
                            if (i == '12') {
                                var mon = parseInt(date.getMonth()) + 1;
                                $("#" + year + i).val(date.getDate() + "-" + mon + "-" + year);
                            }
                        });
                        $(".audited").each(function (index, row) {
                            var value = "";
                            if (!$.isEmptyObject(post[year + "10"]) && post[year + "10"] != undefined) {
                                value = post[year + "10"];
                            }
                            audited_td = '<td class="' + year + '"><input type="text" class="form-control projected" value="' + value + '" name="' + year + '10"/><span class="text-danger"></span></td>';
                            $(this).append(audited_td);
                        });
                        $(".months").each(function (index, row) {
                            months_td = '<td class="' + year + '"><select name="' + year + '13" id="' + year + '13" class="form-control projected"><option value="3">3</option><option value="6">6</option><option value="9">9</option><option value="12">12</option></select><span class="text-danger"></span></td>';
                            var ident = '#' + year + '13';
                            $(this).append(months_td);


                            var value = "3";
                            if (!$.isEmptyObject(post[year + "13"]) && post[year + "13"] != undefined) {
                                value = post[year + "13"];
                            }
                            $(ident).val(value);
<?php if (isset($view_only)) { ?>
                                $(ident).replaceWith("<span class='view_span'>" + $(ident).find('option:selected').text() + "</span>");
<?php } ?>
                            $(ident).select2();
                            $(ident).trigger("change");
                        });
                        $(".colspan").each(function (index, row) {
                            $(this).attr("colspan", colspan);
                        });
                        proj_calc(year);
                    }
                }
<?php
if ($this->input->post()) {
    ?>
                    set_post_hist();
                    set_post_proj();
<?php } ?>
//                END SETTING POST
                $("#select4").change(function () {

                    if ($("#select4").val() == 'yes') {
                        // $(".projected").not("select").attr('required', true);
                        $(".projected").not('select').show();  // becouse select2 make div and hide my original select so not to show my original select
                        $(".add_proj").show();
                        //                        $(".add_hist").show();

                        $("#last_projected").val(last_projected);
                        $("#first_projected").val(first_projected);
                    } else {
                        // $(".projected").removeAttr('required');
                        $(".projected").hide();
                        $(".add_proj").hide();
                        //                        $(".add_hist").hide();

                        $("#last_projected").val("0");
                        //$("#first_projected").val("0");
                    }
                });

                /////////////////////////////////////////////////////////////////////
                $(document).on('click', ".hide_col", function () {
                    var id = $(this).data('col-index');
                    $("." + id).hide();
                    $("#hidden_links").append('<a class="show_col pull-right" data-col-index="' + id + '">Show ' + id + '</a>');
                    $("#hidden_years").val($("#hidden_years").val() + ',' + id);
                });
                // hide col again case of(data from DB Or from post data).
                if ($("#hidden_years").val() != '') {
                    var hidden_y = $("#hidden_years").val().split(',');
                    for (var i = 0; i < hidden_y.length; i++) {
                        $("." + hidden_y[i]).hide();
                        $("#hidden_links").append('<a class="show_col pull-right" data-col-index="' + hidden_y[i] + '">Show ' + hidden_y[i] + '</a>');
                    }
                }
                ////////////
                $(document).on('click', ".delete_col", function () {
                    var id = $(this).data('col-index');

                    if ($(this).data('projected') == 1) {
                        if (id == last_projected) {
                            last_projected--;
                        }
                        $("#last_projected").val(last_projected);
                        $("#deleted_projected").val($("#deleted_projected").val() + ',' + id);
                    } else {
                        if (id == first_projected) {
                            first_projected++;
                        }
                        $("#first_projected").val(first_projected);
                        $("#deleted_historical").val($("#deleted_historical").val() + ',' + id);
                    }

                    $("." + id).remove();
                });
                $(document).on('click', ".show_col", function () {
                    var id = $(this).data('col-index');
                    $("." + id).show();
                    $("#hidden_years").val($("#hidden_years").val().replace(id, ''));
                    $(this).remove();
                });
/////////////////////////////////////////////////////////////////////

            });
        </script>
        <!--END PROJECTED-->
    </body>
</html>