<?php
$add = get_perm("cases", 1);
?>
<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $this->lang->line("borrower_information") ?></h2>
                                </div>
                                <div class="panel-content">
                                    <?php if ($this->session->flashdata("success")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-success"><?= $this->session->flashdata("success") ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($this->session->flashdata("error")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-danger"><?= $this->session->flashdata("error") ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($this->session->userdata("error")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-danger"><?= $this->session->userdata("error") ?></div>
                                            </div>
                                        </div>
                                        <?php
                                        $this->session->unset_userdata('error');
                                    }
                                    ?>
                                    <?php if (form_error("excel_name")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-danger"><?= form_error("excel_name") ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <form id="form" method="POST" action="" role="form" class="form-horizontal form-validation">
                                                <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
                                                <input name="excel_name" type="hidden"/>
                                                <h4><?= $this->lang->line("company_details_english") ?></h4>
                                                <hr>

                                                <?php $i = 4; ?>
                                                <div class="form-group">
                                                    <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_information_C$i") ?></label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="C<?= $i ?>" class="form-control" value="<?= isset($row["C$i"]) ? $row["C$i"] : $this->input->post("C$i") ?>" required>
                                                        <span class="c-red"><?= form_error("C$i") ?></span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_information_E4") ?></label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="E4" class="form-control" value="<?= isset($row["E4"]) ? $row["E4"] : $this->input->post("E4") ?>" required>
                                                        <span class="c-red"><?= form_error("E4") ?></span>
                                                    </div>
                                                </div>

                                                <?php for ($i = 5; $i <= 5; $i++) { ?>
                                                    <div class="form-group">
                                                        <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_information_C$i") ?></label>
                                                        <div class="col-sm-9">
                                                            <input type="text" name="C<?= $i ?>" class="form-control" value="<?= isset($row["C$i"]) ? $row["C$i"] : $this->input->post("C$i") ?>" required>
                                                            <span class="c-red"><?= form_error("C$i") ?></span>
                                                        </div>
                                                    </div>
                                                <?php } ?>


                                                <?php $i = 6; ?>
                                                <div class="form-group">
                                                    <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_information_C$i") ?></label>
                                                    <div class="col-sm-9">
                                                        <select name="C<?= $i ?>" class="form-control" data-search="true"  id="C<?= $i ?>">
                                                            <option value=""><?= $this->lang->line("select") ?></option>
                                                            <?php foreach (get_options_dynamically("c$i") as $one) { ?>
                                                                <option <?= (isset($row["C$i"]) && $row["C$i"] == $one->code) || $this->input->post("C$i") == $one->code ? "selected" : "" ?> value="<?= $one->code ?>"><?= $one->value ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        <span id="C<?= $i ?>_error" class="c-red"><?= form_error("C$i") ?></span>
                                                    </div>
                                                </div>

                                                <?php $i = 7; ?>

                                                <div class="form-group">
                                                    <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_information_C$i") ?></label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="C<?= $i ?>" class="form-control" value="<?= isset($row["C$i"]) ? $row["C$i"] : $this->input->post("C$i") ?>" >
                                                        <span class="c-red"><?= form_error("C$i") ?></span>
                                                    </div>
                                                </div>

                                                <?php $i = 8; ?>
                                                <div class="form-group">
                                                    <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_information_C$i") ?></label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="C<?= $i ?>" class="form-control" value="<?= isset($row["C$i"]) ? $row["C$i"] : $this->input->post("C$i") ?>" >
                                                        <span class="c-red"><?= form_error("C$i") ?></span>
                                                    </div>
                                                </div>

                                                <?php $i = 9;
                                                ?>
                                                <div class="form-group">
                                                    <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_information_C$i") ?></label>
                                                    <div class="col-sm-9">
                                                        <select name="C<?= $i ?>" class="form-control" data-search="true"  id="C<?= $i ?>">
                                                            <option value=""><?= $this->lang->line("select") ?></option>
                                                            <?php foreach (get_options_dynamically("c$i") as $one) { ?>
                                                                <option <?= (isset($row["C$i"]) && $row["C$i"] == $one->code) || $this->input->post("C$i") == $one->code ? "selected" : "" ?> value="<?= $one->code ?>"><?= $one->value ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        <span id="C<?= $i ?>_error" class="c-red"><?= form_error("C$i") ?></span>
                                                    </div>
                                                </div>


                                                <?php $i = 10;
                                                ?>
                                                <div class="form-group">
                                                    <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_information_C$i") ?></label>
                                                    <div class="col-sm-9">
                                                        <select name="C<?= $i ?>" class="form-control" data-search="true"  id="C<?= $i ?>">
                                                            <option value=""><?= $this->lang->line("select") ?></option>
                                                            <?php foreach (get_options_dynamically("c$i") as $one) { ?>
                                                                <option <?= (isset($row["C$i"]) && $row["C$i"] == $one->code) || $this->input->post("C$i") == $one->code ? "selected" : "" ?> value="<?= $one->code ?>"><?= $one->value ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        <span id="C<?= $i ?>_error" class="c-red"><?= form_error("C$i") ?></span>
                                                    </div>
                                                </div>

                                                <?php $i = 11;
                                                ?>
                                                <div class="form-group">
                                                    <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_information_C$i") ?></label>
                                                    <div class="col-sm-9">
                                                        <select name="C<?= $i ?>" class="form-control" data-search="true"  id="C<?= $i ?>">
                                                            <option value=""><?= $this->lang->line("select") ?></option>
                                                            <?php foreach (get_options_dynamically("c$i") as $one) { ?>
                                                                <option <?= (isset($row["C$i"]) && $row["C$i"] == $one->code) || $this->input->post("C$i") == $one->code ? "selected" : "" ?> value="<?= $one->code ?>"><?= $one->value ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        <span id="C<?= $i ?>_error" class="c-red"><?= form_error("C$i") ?></span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="<?= $label_att ?>"><?= $this->lang->line("annual_turnover_amount") ?></label>
                                                    <div class="col-sm-9">
                                                        <select name="turnover_amount" class="form-control" data-search="true"  required>
                                                            <option  <?= (isset($row["turnover_amount"]) && $row["turnover_amount"] == "1") || $this->input->post("turnover_amount") == "1" ? "selected" : "" ?> value="1"><?= $this->lang->line("actual") ?></option>
                                                            <option <?= (isset($row["turnover_amount"]) && $row["turnover_amount"] == "1000") || $this->input->post("turnover_amount") == "1000" ? "selected" : "" ?> value="1000"><?= $this->lang->line("thousands") ?></option>
                                                            <option <?= (isset($row["turnover_amount"]) && $row["turnover_amount"] == "1000000") || $this->input->post("turnover_amount") == "1000000" ? "selected" : "" ?> value="1000000"><?= $this->lang->line("millions") ?></option>
                                                        </select>
                                                        <span class="c-red"><?= form_error("turnover_amount") ?></span>
                                                    </div>
                                                </div>

                                                <?php
                                                $i = 12;
                                                $turnover_amount = 1;
                                                if (isset($row["turnover_amount"])) {
                                                    $turnover_amount = $row["turnover_amount"];
                                                }
                                                ?>
                                                <div class="form-group">
                                                    <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_information_C$i") ?></label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="C<?= $i ?>" class="form-control" value="<?= isset($row["C$i"]) ? ((float)$row["C$i"] ) / $turnover_amount : $this->input->post("C$i") ?>" required>
                                                        <span class="c-red"><?= form_error("C$i") ?></span>
                                                    </div>
                                                </div>

                                                <?php $i = 13; ?>
                                                <div class="form-group">
                                                    <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_information_C$i") ?></label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="C<?= $i ?>" class="form-control" value="<?= isset($row["C$i"]) ? $row["C$i"] : $this->input->post("C$i") ?>" required>
                                                        <span class="c-red"><?= form_error("C$i") ?></span>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_information_E7") ?></label>
                                                    <div class="col-sm-9">
                                                        <select name="E7" class="form-control" data-search="true"  id="E7">
                                                            <option value=""><?= $this->lang->line("select") ?></option>
                                                            <?php foreach (get_options_dynamically("e7") as $one) { ?>
                                                                <option <?= (isset($row["E7"]) && $row["E7"] == $one->code) || $this->input->post("E7") == $one->code ? "selected" : "" ?> value="<?= $one->code ?>"><?= $one->value ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        <span class="c-red"><?= form_error("E7") ?></span>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_information_E10") ?></label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="E10" class="form-control" value="<?= isset($row["E10"]) ? $row["E10"] : $this->input->post("E10") ?>" required>
                                                        <span class="c-red"><?= form_error("E10") ?></span>
                                                    </div>
                                                </div>


                                                <br>

                                                <h4><?= $this->lang->line("bank_statement_details") ?></h4>
                                                <hr>

                                                <?php
                                                $bank_amount = 1;
                                                if (isset($row["bank_amount"])) {
                                                    $bank_amount = $row["bank_amount"];
                                                }
                                                $counter = 1;
                                                for ($i = 25; $i <= 30; $i++) {
                                                    ?>
                                                    <h4><?= $counter ?></h4>
                                                    <div class="form-group">
                                                        <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_information_B$i") ?></label>
                                                        <div class="col-sm-9">
                                                            <input type="text" name="B<?= $i ?>" class="date-picker form-control" value="<?= isset($row["B$i"]) ? $row["B$i"] : $this->input->post("B$i") ?>" >
                                                            <span class="c-red"><?= form_error("B$i") ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_information_C$i") ?></label>
                                                        <div class="col-sm-9">
                                                            <?php
                                                            $C="";
                                                            if(isset($row["C$i"])){
                                                                if(is_numeric($row["C$i"])){
                                                                    $C=$row["C$i"] / $bank_amount;
                                                                }
                                                                else{
                                                                    $C=$row["C$i"];
                                                                }
                                                            }
                                                            else{
                                                                $C=$this->input->post("C$i");
                                                            }
                                                            ?>
                                                            <input type="text" name="C<?= $i ?>" class="form-control" value="<?= $C ?>" >
                                                            <span class="c-red"><?= form_error("C$i") ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_information_D$i") ?></label>
                                                        <div class="col-sm-9">
                                                            <?php
                                                            $D="";
                                                            if(isset($row["D$i"])){
                                                                if(is_numeric($row["D$i"])){
                                                                    $D=$row["D$i"] / $bank_amount;
                                                                }
                                                                else{
                                                                    $D=$row["D$i"];
                                                                }
                                                            }
                                                            else{
                                                                $D=$this->input->post("D$i");
                                                            }
                                                            ?>
                                                            <input type="text" name="D<?= $i ?>" class="form-control" value="<?= $D ?>" >
                                                            <span class="c-red"><?= form_error("D$i") ?></span>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <?php
                                                    $counter++;
                                                }
                                                ?>

                                                <div class="form-group">
                                                    <label class="<?= $label_att ?>"><?= $this->lang->line("bank_amount") ?></label>
                                                    <div class="col-sm-9">
                                                        <select name="bank_amount" class="form-control" data-search="true"  required>
                                                            <option  <?= (isset($row["bank_amount"]) && $row["bank_amount"] == "1") || $this->input->post("bank_amount") == "1" ? "selected" : "" ?> value="1"><?= $this->lang->line("actual") ?></option>
                                                            <option <?= (isset($row["bank_amount"]) && $row["bank_amount"] == "1000") || $this->input->post("bank_amount") == "1000" ? "selected" : "" ?> value="1000"><?= $this->lang->line("thousands") ?></option>
                                                            <option <?= (isset($row["bank_amount"]) && $row["bank_amount"] == "1000000") || $this->input->post("bank_amount") == "1000000" ? "selected" : "" ?> value="1000000"><?= $this->lang->line("millions") ?></option>
                                                        </select>
                                                        <span class="c-red"><?= form_error("bank_amount") ?></span>
                                                    </div>
                                                </div>    

                                                <?php $i = 32;
                                                ?>
                                                <?php
                                                $D="";
                                                if(isset($row["D$i"])){
                                                    if(is_numeric($row["D$i"])){
                                                        $D=$row["D$i"] / $bank_amount;
                                                    }
                                                    else{
                                                        $D=$row["D$i"];
                                                    }
                                                }
                                                else{
                                                    $D=$this->input->post("D$i");
                                                }
                                                ?>
                                                <div class="form-group">
                                                    <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_information_D$i") ?></label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="D<?= $i ?>" class="form-control" value="<?= $D ?>" >
                                                        <span class="c-red"><?= form_error("D$i") ?></span>
                                                    </div>
                                                </div>

                                                <?php $i = 36;
                                                ?>
                                                <div class="form-group">
                                                    <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_information_C$i") ?></label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="C<?= $i ?>" class="date-picker form-control" value="<?= isset($row["C$i"]) ? $row["C$i"] : $this->input->post("C$i") ?>" required>
                                                        <span class="c-red"><?= form_error("C$i") ?></span>
                                                    </div>
                                                </div>

                                                <input type="hidden" value="0" name="continue_later" id="continue_later"/>
                                                
                                                <?php
                                                if(!empty($case_id)){
                                                    ?>
                                                <div class="row">
                                                    <div class="col-sm-9">
                                                        <div class="pull-left">
                                                            <a class="btn btn-embossed btn-primary m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases_two/borrower_factsheet/<?= $case_id ?>/<?= isset($view_only) ? '1' : '' ?>'"><?= $this->lang->line("borrower_factsheet") ?></a>
                                                            <a class="btn btn-embossed btn-primary m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases_two/borrower_financials/<?= $case_id ?>/<?= isset($view_only) ? '1' : '' ?>'"><?= $this->lang->line("borrower_financials") ?></a>
                                                            <a class="btn btn-embossed btn-primary m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases_two/borrower_assessment/<?= $case_id ?>/<?= isset($view_only) ? '1' : '' ?>'"><?= $this->lang->line("borrower_assessment") ?></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                    <?php 
                                                }
                                                ?>

                                                <div class="row">
                                                    <div class="col-sm-9 col-sm-offset-3">
                                                      
                                                        <div class="pull-right">
                                                            <?php
                                                            if (!isset($view_only)) {
                                                                if ($add) {
                                                                    if ($is_case_pass_this_setp) {
                                                                        ?>
                                                                                                <!--<div id="save_and_continue_later"  class="cancel btn btn-embossed btn-default m-b-10 m-r-0"><?= $this->lang->line("save_continue_later") ?></div>-->
                                                                    <?php } ?>
                                                                    <button type="submit" class="btn btn-embossed btn-primary m-r-20"><?= $this->lang->line("save_next") ?></button>
                                                                <?php }
                                                                ?>
                                                                <a class="btn btn-embossed btn-danger m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases_two'"><?= $this->lang->line('discard'); ?></a>
                                                            <?php } else {
                                                                ?>
                                                                <a class="btn btn-embossed btn-primary m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases_two/borrower_factsheet/<?= $case_id ?>/1'"><?= $this->lang->line("next") ?></a>
                                                                <a class="btn btn-embossed btn-danger m-r-20" onclick="location.href = '<?= base_url() ?>admin/Cases_two'"><?= $this->lang->line('close'); ?></a>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script>
                                                                    $(document).ready(function () {
//                                                                        $("#submit_borrower_info").click(function(e){
//                                                                            alert("in");
//                                                                            e.preventDefault();
//                                                                            var requireds=[6,9,10,11];
//                                                                            for(var i=0; i<requireds.length;i++){
//                                                                                if($("#C"+requireds[i]).val()==""){
//                                                                                    $("#C"+requireds[i]."_error").html("<?=$this->lang->line("field_required")?>");
//                                                                                }
//                                                                            }
//                                                                            
//                                                                        });
                                                                        $("#save_and_continue_later").click(function () {
                                                                            $("#continue_later").val("1");
                                                                            $("input").each(function (index, value) {
                                                                                $(this).removeAttr("required");
                                                                                $("#form").submit();
                                                                            });
                                                                        });

<?php if (isset($view_only)) { ?>
                                                                            $(".c-red").hide();
                                                                            $('input').each(function () {
                                                                                $(this).not(':hidden').replaceWith("<span class='view_span'>" + $(this).val() + "</span>");
                                                                            });

                                                                            $('textarea').each(function () {
                                                                                $(this).not(':hidden').replaceWith("<span class='view_span'>" + $(this).val() + "</span>");
                                                                            });

                                                                            $('select').each(function () {
                                                                                $(this).not(':hidden').replaceWith("<span class='view_span'>" + $(this).find("option:selected").text() + "</span>");
                                                                            });
<?php } ?>
//            $(".d").datepicker({
//                dateFormat: 'dd-mm-yy',
//            numberOfMonths: 1,
//            isRTL : $('body').hasClass('rtl') ? true : false,
//            prevText: '<i class="fa fa-angle-left"></i>',
//            nextText: '<i class="fa fa-angle-right"></i>',         
//            showButtonPanel: false
//        });
                                                                    });
        </script>
    </body>
</html>