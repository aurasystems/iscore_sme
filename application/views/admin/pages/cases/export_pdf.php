<html lang="en">
    <head>
        <!--<meta http-equiv="Content-Type" content="charset=utf-8"/>-->
        <style>
            .label{
                font-weight: bold;
            }
            thead { display: table-header-group }
            tfoot { display: table-row-group }
            tr { page-break-inside: avoid;
            }
            th, td{
                padding: 3px;
            }
            .sr{
                width:5% !important;
            }
            .param{
                width:20% !important;
            }
            .desc, .choice{
/*                padding: 1% !important;
                margin: 10% !important;*/
                width:37.5% !important;
            }
        </style>
    </head>
    <body>

        <!--BEGIN BORROWER INFORMATION-->
        <h1><?= $this->lang->line('borrower_information') ?></h1>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <form id="form" method="POST" action="" role="form" class="form-horizontal form-validation">
                    <h4><?= $this->lang->line("company_details_english") ?></h4>
                    <table border="1">
                        <tbody>
                            <?php $i = 4; ?>
                            <tr>
                                <td class="label">
                                    <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_information_C$i") ?></label>
                                </td>
                                <td>
                                    <?= isset($row["C$i"]) ? $row["C$i"] : "" ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_information_E4") ?></label>
                                </td>
                                <td>
                                    <?= isset($row["E4"]) ? $row["E4"] : "" ?>
                                </td>
                            </tr>
                            <?php for ($i = 5; $i <= 13; $i++) { ?>
                                <tr>
                                    <td class="label">
                                        <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_information_C$i") ?></label>
                                    </td>
                                    <td>
                                        <?php
                                        if ($i == '6' || $i == '9' || $i == '10' || $i == '11') {
                                            $code = isset($row["C$i"]) ? $row["C$i"] : "";
                                            $value = get_value_from_code("C$i", $code, 1);
                                        } else {
                                            $value = isset($row["C$i"]) ? $row["C$i"] : "";
                                        }
                                        ?>
                                        <?= $value ?>
                                    </td>
                                </tr>
                            <?php } ?>


                        </tbody>
                    </table>

                    <br>

                    <h4><?= $this->lang->line("bank_statement_details") ?></h4>


                    <table border="1">
                        <thead>
                            <tr class="label">
                                <th>#</th>
                                <th><?= $this->lang->line("borrower_information_B25") ?></th>
                                <th><?= $this->lang->line("borrower_information_C25") ?></th>
                                <th><?= $this->lang->line("borrower_information_D25") ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $counter = 1;
                            for ($i = 25; $i <= 30; $i++) {
                                ?>
                                <tr>
                                    <td><?= $counter ?></td>
                                    <td>
                                        <?= isset($row["B$i"]) ? $row["B$i"] : "" ?>
                                    </td>
                                    <td>
                                        <?= isset($row["C$i"]) ? $row["C$i"] : "" ?>
                                    </td>
                                    <td>
                                        <?= isset($row["D$i"]) ? $row["D$i"] : "" ?>
                                    </td>
                                </tr>
                                <?php
                                $counter++;
                            }
                            ?>


                        </tbody>
                    </table>

                    <br><br>


                    <table  border="1">
                        <tbody>
                            <?php $i = 32; ?>
                            <tr>
                                <td class="label">
                                    <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_information_D$i") ?></label>
                                </td>
                                <td>
                                    <?= isset($row["D$i"]) ? $row["D$i"] : "" ?>
                                </td>
                            </tr>
                            <?php $i = 36; ?>
                            <tr>
                                <td class="label">
                                    <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_information_C$i") ?></label>
                                </td>
                                <td>
                                    <?= isset($row["C$i"]) ? $row["C$i"] : "" ?>
                                </td>
                            </tr>

                        </tbody>
                    </table>

                </form>
            </div>
        </div>
        <!--END BORRWER INFORMATION-->

        <!--BEGIN BORROWER FACTSHEET-->
        <h1><?= $this->lang->line('borrower_factsheet') ?></h1>
        <h4><?= $this->lang->line("company_information") ?></h4>

        <table border="1">
            <tbody>
                <?php for ($i = 4; $i <= 5; $i++) { ?>
                    <tr>
                        <td class="label"><label class="<?= $label_att ?>"><?= $this->lang->line("borrower_factsheet_C$i") ?></label></td>
                        <td>
                            <?= isset($factsheet_data[$i]) ? $factsheet_data[$i]['C'] : "" ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label"><label class="<?= $label_att ?>"><?= $this->lang->line("borrower_factsheet_E$i") ?></label></td>
                        <td>
                            <?php if ($i == 4) { ?>
                                <?= isset($factsheet_data[$i]) ? $factsheet_data[$i]['E'] : "" ?>
                            <?php } else { ?>
                                <?= $line_activity ?>
                            <?php } ?>
                        </td>
                    </tr>
                <?php }
                ?>


            </tbody>
        </table>            
        <br/>
        <h4><?= $this->lang->line("shareholding") ?></h4>

        <table border="1">
            <thead>
                <tr class="label">
                    <th><?= $this->lang->line('sr_no') ?></th>
                    <th><?= $this->lang->line('investor_group') ?></th>
                    <th><?= $this->lang->line('share') ?></th>
                </tr>
            </thead>
            <tbody>

                <?php for ($i = 9; $i <= 18; $i++) { ?>
                    <tr>
                        <td><?= $i - 8 ?></td>
                        <?php
                        $value = '';
                        if (isset($factsheet_data[$i])) {
                            $value = $factsheet_data[$i]["C"];
                        }
                        ?>
                        <td><?= $value ?></td>
                        <?php
                        $value = '';
                        if (isset($factsheet_data[$i])) {
                            $value = $factsheet_data[$i]["D"];
                        }
                        ?>
                        <td><?= $value ?></td>
                    </tr>            
                <?php } ?>

            </tbody>

        </table>

        <br/>
        <h4><?= $this->lang->line("board_of_directors") ?></h4>

        <table border="1">
            <thead>
                <tr class="label">
                    <th><?= $this->lang->line('sr_no') ?></th>
                    <th class='text-center'><?= $this->lang->line('name') ?></th>
                    <th class='text-center'><?= $this->lang->line('remarks') ?></th>
                </tr>
            </thead>
            <tbody>

                <?php for ($i = 22; $i <= 31; $i++) { ?>
                    <tr>
                        <td><?= $i - 21 ?></td>
                        <?php
                        $value = '';
                        if (isset($factsheet_data[$i])) {
                            $value = $factsheet_data[$i]["C"];
                        }
                        ?>
                        <td><?= $value ?></td>
                        <?php
                        $value = '';
                        if (isset($factsheet_data[$i])) {
                            $value = $factsheet_data[$i]["D"];
                        }
                        ?>
                        <td><?= $value ?></td>
                    </tr>            
                <?php } ?>

            </tbody>

        </table>
        <br/>
        <h4><?= $this->lang->line("key_personnel") ?></h4>

        <table border="1">
            <thead>
                <tr class="label">
                    <th><?= $this->lang->line('sr_no') ?></th>
                    <th class='text-center'><?= $this->lang->line('name') ?></th>
                    <th class='text-center'><?= $this->lang->line('designation') ?></th>
                </tr>
            </thead>
            <tbody>

                <?php for ($i = 35; $i <= 44; $i++) { ?>
                    <tr>
                        <td><?= $i - 34 ?></td>
                        <?php
                        $value = '';
                        if (isset($factsheet_data[$i])) {
                            $value = $factsheet_data[$i]["C"];
                        }
                        ?>
                        <td><?= $value ?></td>
                        <?php
                        $value = '';
                        if (isset($factsheet_data[$i])) {
                            $value = $factsheet_data[$i]["D"];
                        }
                        ?>
                        <td><?= $value ?></td>
                    </tr>            
                <?php } ?>

            </tbody>

        </table>

        <!--END BORROWER FACTSHEET-->

        <!--BEGIN BORROWER FINANCIALS-->
        <h1><?= $this->lang->line('borrower_financials') ?></h1>
        <h4><?= $this->lang->line("financial_data_template") ?></h4>
        <table border="1">
            <tbody>
                <?php for ($i = 3; $i <= 4; $i++) { ?>
                    <tr>
                        <td class="label">
                            <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_financial_E$i") ?></label>
                        </td>
                        <td>
                            <?= isset($financial_data[$i]) ? $financial_data[$i]["E"] : "" ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>

        <br/>
        <p class="bold"><?= $this->lang->line('borrower_financial_notes') ?><br/><?= $this->lang->line('borrower_financial_notes_2') ?></p>
        <br/>

        <table border="1">
            <tbody>
                <tr>
                    <td class="label">
                        <label class="<?= $label_att ?>"><?= $this->lang->line("borrower_financial_G8") ?></label>
                    </td>
                    <td>
                        <?= isset($financial_data[8]) ? $this->lang->line(strtolower($financial_data[8]["G"])) : "" ?>
                    </td>
                </tr>
            </tbody>
        </table>
        <br><br>

        <?php
        $colspan=6;
        if(count($hist_years)>0){
            $colspan+=count($hist_years);
        }
        if(count($projected_years)>0){
            $colspan+=count($projected_years);
        }
        ?>
        <table border="1">
            <tbody>
                <tr class="label">
                    <td></td>
                    <?php foreach ($hist_years as $year){?>
                     <td><?= $year ?></td>
                    <?php }?>
                    <td><?= $borrower_info_year - 2 ?></td>
                    <td><?= $borrower_info_year - 1 ?></td>
                    <td><?= $borrower_info_year ?></td>
                    <td><?= $borrower_info_year + 1 ?></td>
                    <td><?= $borrower_info_year + 2 ?></td>
                     <?php foreach ($projected_years as $year){?>
                     <td><?= $year ?></td>
                    <?php }?>
                </tr>
                <tr>
                    <td></td>
                     <?php foreach ($hist_years as $year){
                          $data = '';
                        if (isset($hist[10]) && isset($hist[10][$year])) {
                            $data = $hist[10][$year];
                        }
                         ?>
                     <td><?= $data ?></td>
                    <?php }?>
                    <?php
                    $str = 'C';
                    for ($i = 0; $i < 3; $i++) {
                        $data = '';
                        if (isset($financial_data[10])) {
                            $data = $financial_data[10][$str];
                        }
                        ?>
                        <td>
                            <?= $data ?>
                        </td>
                        <?php
                        $str++;
                    }
                    for ($i = 0; $i < 2; $i++) {
                        $value = '';
                        if (isset($financial_data[10])) {
                            $value = $financial_data[10][$str];
                        }
                        ?>
                        <td>
                            <?= $value ?>
                        </td>
                        <?php
                        $str++;
                    }
                    ?>
                           <?php foreach ($projected_years as $year){
                          $data = '';
                        if (isset($proj[10]) && isset($proj[10][$year])) {
                            $data = $proj[10][$year];
                        }
                         ?>
                     <td><?= $data ?></td>
                    <?php }?>
                </tr>

                <?php
                for ($i = 12; $i <= 224; $i++) {
                    if ($i == 16 || $i == 17 || $i == 25 || $i == 47 || $i == 75 || $i == 77 || $i == 105 || $i == 125 || $i == 123 || $i == 143 || $i == 152 ||
                            $i == 173 || $i == 174 || $i == 182 || $i == 188 || $i == 194 || $i == 202) {
                        if ($i == 75) {
                            echo '<tr style="height:120px"><td colspan="'.$colspan.'"><strong><hr/></strong></td></tr>';
                        }
                        ?>
                <tr><td colspan="<?=$colspan?>"><strong><?= $this->lang->line("borrower_financial_$i") ?></strong></td></tr>
                        <?php
                        continue;
                    }
                    ?>
                    <?php
                    if (($i >= 15 && $i <= 17) ||
                            $i == 21 ||
                            ($i > 23 && $i < 25) ||
                            ($i > 29 && $i < 31) || $i == 37 ||
                            $i == 42 || $i == 40 ||
                            $i == 44 || $i == 46 ||
                            ($i > 52 && $i < 54) || $i == 57 ||
                            $i == 59 || $i == 61 || $i == 64 ||
                            $i == 68 || $i == 70 || $i == 72 ||
                            ($i >= 74 && $i <= 77) || $i == 96 ||
                            $i == 102 || ($i >= 104 && $i < 105) || $i == 118 ||
                            $i == 120 || ($i >= 122 && $i <= 125) ||
                            ($i > 141 && $i < 144) ||
                            ($i > 150 && $i <= 152) || $i == 165 || $i == 169 || $i == 172 ||
                            $i == 181 || $i == 193 || $i == 201 || $i == 215 || $i == 216 || $i == 217) {
                        echo '<tr style="height:40px"><td colspan="'.$colspan.'"></td></tr>';
                        continue;
                    }
                    ?>
                    <?php
                    $value_C = '';
                    $value_D = '';
                    $value_E = '';
                    $value_F = '';
                    $value_G = '';
                    if (isset($financial_data[$i])) {
                        $value_C = $financial_data[$i]['C'];
                        $value_D = $financial_data[$i]['D'];
                        $value_E = $financial_data[$i]['E'];
                        $value_F = $financial_data[$i]['F'];
                        $value_G = $financial_data[$i]['G'];
                    }
                    ?>
                    <?php if ($i == 13) { ?>
                        <tr>
                            <td class="label"><?= $this->lang->line("borrower_financial_$i") ?></td>
                            <?php foreach ($hist_years as $year){
                          $data = '';
                        if (isset($hist[13]) && isset($hist[13][$year])) {
                            $data = $hist[13][$year];
                        }
                         ?>
                     <td><?= $data ?></td>
                    <?php }?>
                     
                            <td><?= $value_C ?></td>
                            <td><?= $value_D ?></td>
                            <td><?= $value_E ?></td>
                            <td><?= $value_F ?></td>
                            <td><?= $value_G ?></td>
                            
                            <?php foreach ($projected_years as $year){
                          $data = '';
                        if (isset($proj[13]) && isset($proj[13][$year])) {
                            $data = $proj[13][$year];
                        }
                         ?>
                     <td><?= $data ?></td>
                    <?php }?>
                        </tr>
                        <?php
                        continue;
                    } else if ($i == 12 || $i == 20 || $i == 23 || $i == 29 || $i == 31 || $i == 41 || $i == 45 || $i == 48 || $i == 52 ||
                            $i == 54 || $i == 60 || $i == 65 || $i == 69 || $i == 73 || $i == 91 || $i == 95 || $i == 101 || $i == 103 ||
                            $i == 116 || $i == 121 || $i == 128 || $i == 131 || $i == 141 || $i == 144 || $i == 150 || $i == 157 || $i == 164 ||
                            $i == 168 || $i == 170 || $i == 171 || ($i >= 175 && $i <= 180) || ($i >= 183 && $i <= 187) ||
                            ($i >= 189 && $i <= 192) || ($i >= 195 && $i <= 200) || ($i >= 203 && $i <= 214) || ($i >= 218 && $i <= 224)) {
                        ?>
                        <tr class="totals" style="background-color: #ffff82">
                            <td><?= $this->lang->line("borrower_financial_$i") ?></td>
                            <?php foreach ($hist_years as $year){
                          $data = '';
                        if (isset($hist[$i]) && isset($hist[$i][$year])) {
                            $data = $hist[$i][$year];
                        }
                         ?>
                     <td><?= $data ?></td>
                    <?php }?>
                            <td><?= isset($calc[$i]) ? $calc[$i]["C"] : "" ?></td>
                            <td><?= isset($calc[$i]) ? $calc[$i]["D"] : "" ?></td>
                            <td><?= isset($calc[$i]) ? $calc[$i]["E"] : "" ?></td>
                            <td><?= isset($calc[$i]) ? $calc[$i]["F"] : "" ?></td>
                            <td><?= isset($calc[$i]) ? $calc[$i]["G"] : "" ?></td>
                            <?php foreach ($projected_years as $year){
                          $data = '';
                        if (isset($proj[$i]) && isset($proj[$i][$year])) {
                            $data = $proj[$i][$year];
                        }
                         ?>
                     <td><?= $data ?></td>
                    <?php }?>

                        </tr>
                        <?php
                        continue;
                    }
                    ?>
                    <tr>
                        <td class="label"><?= $this->lang->line("borrower_financial_$i") ?></td>
                          <?php foreach ($hist_years as $year){
                          $data = '';
                        if (isset($hist[$i]) && isset($hist[$i][$year])) {
                            $data = $hist[$i][$year];
                        }
                         ?>
                     <td><?= $data ?></td>
                    <?php }?>
                        <td><?= $value_C ?></td>
                        <td><?= $value_D ?></td>
                        <td><?= $value_E ?></td>
                        <td><?= $value_F ?></td>
                        <td><?= $value_G ?></td>
                          <?php foreach ($projected_years as $year){
                          $data = '';
                        if (isset($proj[$i]) && isset($proj[$i][$year])) {
                            $data = $proj[$i][$year];
                        }
                         ?>
                     <td><?= $data ?></td>
                    <?php }?>
                    </tr>

                <?php } ?>


            </tbody>
        </table>
        <!--END BORROWER FINANCIALS-->

        <!--BEGIN BORROWER ASSESSMENT--> 
        <h1><?= $this->lang->line('borrower_assessment') ?></h1>
        <table border="1" cellpadding="2" cellspacing="1">
            <thead>
                <tr class="label">
                    <th class="sr"><?= $this->lang->line('sr_no') ?></th>
                    <th class="param"><?= $this->lang->line('assessment_parameter') ?></th>
                    <th class="desc"><?= $this->lang->line('parameter_description') ?></th> 
                    <th class="choice"><?= $this->lang->line('select') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sr_no = 1;
                for ($i = 4; $i <= 229; $i++) {
                    if ($this->lang->line("borrower_assessment_F$i") != '') {
                        ?>
                        <?php if ($i == 4 || $i == 17 || $i == 120 || $i == 132 || $i == 184 || $i == 214) { ?>
                            <tr>
                                <td colspan="4" class="label"><?= $this->lang->line("borrower_assessment_F$i") ?></td>
                            </tr>
                            <?php
                            continue;
                        }
                        ?>

                        <tr>
                            <td class="sr"><?= $sr_no ?></td>
                            <td class="param" style="padding: 10px"><?= $this->lang->line("borrower_assessment_F$i") ?></td>
                            <td class="desc" style="padding: 10px"><?= $this->lang->line("borrower_assessment_F$i" . "_desc") ?></td>
                            <?php
                            $background = "";
                            $selected = "";
                            //modified
                            if ($sr_no == 38) {
                                $background = "background-color:#ffff82;";
                                $selected = str_replace('>', '&gt;', $param_39);
                                $selected = str_replace('<', '&lt;', $param_39);
                            } elseif ($sr_no == 39) {
                                $background = "background-color:#ffff82;";
                                $selected = str_replace('>', '&gt;', $param_40);
                                $selected = str_replace('<', '&lt;', $param_40);
                            } elseif (isset($assessment_data[$i])) {

                                $selected = $assessment_data[$i]['F'];
                                $selected = get_value_from_code('F' . $i, $selected, 1);
                                $selected = str_replace('>', '&gt;', $selected);
                                $selected = str_replace('<', '&lt;', $selected);
//                                                                                    $selected = str_replace('&lt;', '<', $assessment_data[$i]['F']);
                            }
                            ?>
                            <td class="choice" style="<?= $background ?>padding:10px">
                                <?= $selected; ?>
                            </td>
                        </tr>
                        <?php
                        $sr_no++;
                    }
                }//die();
                ?>
            </tbody>


        </table>




        <!--END BORROWER ASSESSMENT-->
    </body>
</html>