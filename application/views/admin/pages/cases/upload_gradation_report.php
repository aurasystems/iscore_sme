<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $this->lang->line("gradation_report") ?></h2>
                                </div>
                                <div class="panel-content">
                                    <?php if ($this->session->flashdata("success")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-success"><?= $this->session->flashdata("success") ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($this->session->flashdata("error")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-danger"><?= $this->session->flashdata("error") ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <form action="<?= base_url() ?>admin/Cases_two/upload_pdf" id='pdfsUpload' enctype="multipart/form-data" method="post" class="dropzone">
                                                <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                                                <div class="fallback">
                                                    <input name="file" type="file" multiple />
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script>
            $(function () {
                Dropzone.options.pdfsUpload = {
                    maxFilesize: 2000000,
                    acceptedFiles: ".pdf,.xml"
                };
                $("#upload_btn").click(function () {
                    $(".dropzone").submit();
                });
            });
        </script>
    </body>
</html>