<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <style>
            .icheckbox_minimal-grey{
                margin-top: 12px;
            }
        </style>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata("success")) { ?>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="alert alert-success"><?= $this->session->flashdata("success") ?></div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata("error")) { ?>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="alert alert-danger"><?= $this->session->flashdata("error") ?></div>
                                    </div>
                                </div>
                            <?php } ?>

                            <div class="panel panel-default">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><i class="fa fa-gear"></i> <?= $this->lang->line("edit") ?>&nbsp;<?= $this->lang->line("borrower_information_E7") ?></h2>
                                </div>

                                <div class="panel-body">
                                    <form id="form" method="POST" action="" role="form" class="form-horizontal form-validation">
                                        <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label class="<?= $label_att ?>"><?=$this->lang->line("code")?></label>
                                                <div class="col-sm-6">
                                                    <input type="text" name="code" class="form-control" value="<?=$one->code ?>" >
                                                    <span class="c-red"><?= form_error("code") ?></span>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="<?= $label_att ?>"><?=$this->lang->line("value")?></label>
                                                <div class="col-sm-6">
                                                    <input type="text" name="value" class="form-control" value="<?=$one->value ?>" >
                                                    <span class="c-red"><?= form_error("value") ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="<?= $label_att ?>"><?=$this->lang->line("value_arabic")?></label>
                                                <div class="col-sm-6">
                                                    <input type="text" name="value_arabic" class="form-control" value="<?=$one->arabic_value ?>" >
                                                    <span class="c-red"><?= form_error("value_arabic") ?></span>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="pull-right">
                                                    <button type="submit" class="btn btn-embossed btn-primary m-r-20"><?= $this->lang->line("save") ?></button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
    </body>
</html>