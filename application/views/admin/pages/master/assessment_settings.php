<?php
//$add = get_perm("cases", 1);
//$edit = get_perm("cases", 2);
//$delete = get_perm("cases", 3);
//$approve_add = get_perm("approve_case", 1);
//$approve_edit = get_perm("approve_case", 2);
$add = get_perm("settings", 1);
$edit = get_perm("settings", 2);
$delete = get_perm("settings", 3);
?>
<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $title ?></h2>
                                </div>
                                <div class="panel-content">
                                    <?php if ($this->session->flashdata("success")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-success"><?= $this->session->flashdata("success") ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if ($this->session->flashdata("error")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-danger"><?= $this->session->flashdata("error") ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($add) { ?>
                                        <div class="text-center">
                                            <a href = '<?= base_url() ?>admin/Master/add_<?= $table ?>' class="btn btn-success btn-square"><?= $this->lang->line('add') ?></a>
                                        </div>
                                    <?php } ?>
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <table class="table table-hover table-dynamic table-tools">
                                                <thead>
                                                    <tr>
                                                        <th><?= $this->lang->line('code') ?></th>
                                                        <th><?= $this->lang->line('value') ?></th>
                                                        <th><?= $this->lang->line('actions') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach ($all as $one) {
                                                        ?>
                                                        <tr>
                                                            <td><?= $one->code ?></td>
                                                            <td><?= $one->value ?></td>
                                                            <td>
                                                                <div class="btn-group">
                                                                    <?php if ($edit) { ?>
                                                                        <a  class="btn btn-sm btn-warning " title="<?= $this->lang->line('edit') ?>" href="<?= base_url() ?>admin/Master/edit_<?= $table ?>/<?= $one->id ?>"><i class="fa fa-edit"></i></a>
                                                                    <?php } ?>
                                                                    <?php if ($delete) { ?>
                                                                        <a onclick="delete_case('<?= $one->id ?>')" class="btn btn-sm btn-danger delete" title="<?= $this->lang->line('delete') ?>" data-id="<?= $one->id ?>" ><i class="fa fa-close"></i></a>
                                                                        <?php } ?>
                                                                </div>
                                                            </td>

                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <?php $this->load->view('admin/pages/cases/comment_modal') ?>

        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script>
                                                                        function delete_case(id) {
                                                                            if (window.confirm("<?= $this->lang->line("r_u_sure") ?>")) {
                                                                                location.href = "<?= base_url() ?>admin/Master/delete_<?= $table ?>/" + id;
                                                                            }
                                                                        }
        </script>
    </body>
</html>