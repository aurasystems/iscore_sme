<?php
$add = get_perm("cases", 1);
?>
<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $this->lang->line("bulk_case_creation_report") ?></h2>
                                </div>
                                <div class="panel-content">
                                    <?php if ($this->session->flashdata("success")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-success"><?= $this->session->flashdata("success") ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($this->session->flashdata("error")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-danger"><?= $this->session->flashdata("error") ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if($status=="invalid"){ ?>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="alert alert-danger"><?= $this->lang->line("bulk_failed_report") ?><a href="<?= $failed_url ?>"><?= $this->lang->line("here") ?></a></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <table class="table table-hover table-dynamic">
                                                <thead>
                                                <th><?= $this->lang->line("row_no_sheet") ?></th>
                                                <th><?= $this->lang->line("errors") ?></th>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($invalid_cases as $row => $one) { ?>
                                                        <tr>
                                                            <td><?= $row ?></td>
                                                            <td><?= $one ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <?php }
                                    else{
                                    ?>
                                     <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-success"><?= $this->lang->line("cases_imported_successfully") ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <form class="calc_form">
                                                <?php foreach ($fin_labels as $one) {
                                                    ?>
                                                <input type="text" class="" name="<?= $one ?>" id="<?= $one ?>" value="20"/>
                                                <?php } ?>
                                                <?php foreach ($fields as $one) {
                                                    $class="";
                                                    $i= substr($one, 1);
                                                    if ($i == 12 || $i == 20 || $i == 23 || $i == 29 || $i == 31 || $i == 41 || $i == 45 || $i == 48 || $i == 52 ||
                                                                        $i == 54 || $i == 60 || $i == 65 || $i == 69 || $i == 73 || $i == 91 || $i == 95 || $i == 101 || $i == 103 ||
                                                                        $i == 116 || $i == 121 || $i == 128 || $i == 131 || $i == 141 || $i == 144 || $i == 150 || $i == 157 || $i == 164 ||
                                                                        $i == 168 || $i == 170 || $i == 171 || ($i >= 175 && $i <= 180) || ($i >= 183 && $i <= 187) ||
                                                                        ($i >= 189 && $i <= 192) || ($i >= 195 && $i <= 200) || ($i >= 203 && $i <= 214) || ($i >= 218 && $i <= 224)) {
                                                        $class="totals";
                                                    }
                                                    ?>
                                                <input type="text" class="<?=$class?>" name="<?= $one ?>" id="<?= $one ?>" value="10"/>
                                                <?php } ?>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/js/bulk_borrower_financials.js"></script>
        <script>
            $(document).ready(function () {
                var interval = setInterval(function () {
                    $('.loader-overlay').removeClass('loaded');
                }, 100);
                function save_calc(calc) {
                    $.ajax({
                        url: "<?= base_url() ?>admin/Bulk/save_calc",
                        type: "POST",
                        data: {'calc': JSON.stringify(calc),"<?=$this->security->get_csrf_token_name()?>":"<?=$this->security->get_csrf_hash()?>"},
                        success: function (result) {
                            console.log(result);
                            $(".calc_form").hide();
                             clearInterval(interval);
                              $('.loader-overlay').addClass('loaded');
                        },
                        error: function (jqXHR, exception) {
                            console.log(jqXHR);
                            alert("Uncaught error. Form can not be submitted");
                        }
                    });
//                                                                        $("#form").submit();              
                }
                function set_calc() {
                $('.loader-overlay').addClass('loaded');
                    var valid_fins=<?=  json_encode($valid_fins)?>;
                    var calc={};
                    $.each(valid_fins, function(i, fin){
                        var case_id="";
                        $.each(fin, function(key, value){
                            if(key=="case_id"){
                                case_id=value;
                            }
                            else{
                                var identifier= "#"+key;
                            $(identifier).val(value);
                            }
                        });
                        adjust_totals();
                        //wait 3 secs
                         calc[case_id]={};
                         $(".totals").each(function (index, value) {
                            calc[case_id][$(this).attr("id")]=$(this).val();
                        });
                    });       
                    save_calc(calc);
                }
                set_calc();
            });
        </script>
    </body>
</html>