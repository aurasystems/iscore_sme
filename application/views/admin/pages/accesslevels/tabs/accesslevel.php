<?php if(substr($this->session->userdata('levels')->accleveltype, 0, 1) == '1') { ?>
<div class="tab-pane" id="tab8_2">
    <div id="accesslevelalert">
    </div>
    <form method="post" action="<?= base_url() ?>admin/Accesslevels/addNewAccessLevel">
        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
        <select name="levelid" id="levelid" class="form-control" required="required" onchange="getUserAccessLevel()" style="max-width: 30%;">
            <option value=""><?= $this->lang->line('select_access_level') ?></option>
            <?php foreach ($levels as $level): ?>
                <option value="<?= $level->id ?>"><?= $level->name; ?></option>
            <?php endforeach; ?>
        </select>
        <table class="table table-responsive table-hover table-striped">
            <thead class="fixedElement">
                <tr>
                    <th class="operation_col_header">
                        <b><?= $this->lang->line('operation') ?></b>
                    </th>
                    <th class="text-center checkbox_cols_header">
                        <b><?= $this->lang->line('view') ?></b><br>
                        <input type="checkbox" id="view" class="checkAll" ><?= $this->lang->line('check_all') ?>
                    </th>
                    <th class="text-center checkbox_cols_header">
                        <b><?= $this->lang->line('create') ?></b><br>
                        <input type="checkbox" id="create" class="checkAll" onchange="checkAll('create')"><?= $this->lang->line('check_all') ?>
                    </th>
                    <th class="text-center checkbox_cols_header">
                        <b><?= $this->lang->line('update') ?></b><br>
                        <input type="checkbox" id="update" class="checkAll" onchange="checkAll('update')"><?= $this->lang->line('check_all') ?>
                    </th>
                    <th class="text-center checkbox_cols_header">
                        <b><?= $this->lang->line('delete') ?></b><br>
                        <input type="checkbox" id="delete" class="checkAll" onchange="checkAll('delete')"><?= $this->lang->line('check_all') ?>
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                foreach ($funcs as $func) {
                    if ($func->COMM != '') {// to not get comm of id colum
                ?>
                <tr>
                    <td class="operation_col">
                        <b><?= $this->lang->line("$func->COMM") ?></b>
                        <?php if($func->COMM == "cases"){ ?>
                        &nbsp;&nbsp;<input id="bank_only" type="checkbox" name="bank_only" value="1" /><?=$this->lang->line("bank_only")?>
                        <input id="branch_only" type="checkbox" name="branch_only" value="1" /><?=$this->lang->line("branch_only")?>
                        <input id="approved_only" type="checkbox" name="approved_only" value="1" /><?=$this->lang->line("approved_only")?>
                        <input id="completed_only" type="checkbox" name="completed_only" value="1" /><?=$this->lang->line("completed_only")?>
                        <?php } ?>
                    </td>
                    <td class="text-center checkbox_cols">
                        <input type="checkbox" class="view" id="<?= $i; ?>0" name="<?= $i; ?>[0]" >
                        <input type="hidden" name="<?= $i; ?>[4]" value="0">
                    </td>
                    <td class="text-center checkbox_cols">
                        <input type="checkbox" class="create" id="<?= $i; ?>1" name="<?= $i; ?>[1]" >

                    </td>
                    <td class="text-center checkbox_cols">
                        <input type="checkbox" class="update" id="<?= $i; ?>2" name="<?= $i; ?>[2]" >
                    </td>
                    <td class="text-center checkbox_cols">
                        <input type="checkbox" class="delete" id="<?= $i; ?>3" name="<?= $i; ?>[3]" >
                    </td>
                </tr>
                <?php
                        $i++;
                    }
                }
                ?>
            </tbody>
        </table>
        <?php if($this->session->userdata('user_id') != "" && substr($this->session->userdata('levels')->accleveltype, 0, 1) == '1' && substr($this->session->userdata('levels')->accleveltype, 1, 1) == '1' && substr($this->session->userdata('levels')->accleveltype, 2, 1) == '1') { ?>
        <button class="btn btn-success" id="saveBtn"><?= $this->lang->line('lang_save') ?></button>
        <?php } ?>
    </form>
</div>
<?php } ?>