<?php if (substr($this->session->userdata('levels')->accleveltype, 0, 1) == '1' && substr($this->session->userdata('levels')->accleveltype, 1, 1) == '1') { ?>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <?php echo form_open('admin/Accesslevels/add_access_level', $attributes); ?>
                    <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                    <div class="form-group">
                        <?php echo form_label($this->lang->line('level_name') . ': <span class="text-red">*</span>', 'level_name', $label_att); ?>
                        <div class="col-sm-8">
                            <?php echo form_input(array('id' => 'level_name', 'name' => 'level_name', 'class' => 'form-control', 'placeholder' => 'Access level name', 'value' => set_value('level_name'), 'required' => 'required')); ?>
                            <span class="text-red"><?php echo form_error('level_name'); ?></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-sm-offset-4">
                            <div class="pull-left">
                                <?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('lang_save'), 'class' => 'btn btn-embossed btn-success')); ?>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
    <hr>
<?php } ?>
<div class="row">
    <div class="col-lg-12 portlets">
        <table class="table table-hover table-dynamic">
            <thead>
                <tr>
                    <th><?= $this->lang->line('#') ?></th>
                    <th><?= $this->lang->line('users_access_level') ?></th>
                    <th><?= $this->lang->line('lang_action') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($levels) {
                    $i = 1;
                    foreach ($levels as $level) {
                        ?>
                        <tr>
                            <td><?= $i++ ?></td>
                            <td>
                                <input class="form-control" id='acc_level_name_<?= $level->id ?>' disabled="" value='<?= $level->name ?>'>
                            </td>
                            <td>
                                <?php if (substr($this->session->userdata('levels')->accleveltype, 0, 1) == '1' && substr($this->session->userdata('levels')->accleveltype, 2, 1) == '1') { ?>
                                    <button class="btn btn-dark btn-sm no_margin" id='edit_btn_<?= $level->id ?>' onclick="edit_access_level(<?= $level->id ?>)">
                                        <i class="fa fa-edit edit_i_tag"></i>
                                    </button>
                                <?php } ?>
                                <button class="btn btn-dark btn-sm no_left_margin no_margin" id='update_btn_<?= $level->id ?>' style="display: none" onclick="update_access_level(<?= $level->id ?>)">
                                    <i class="fa fa-floppy-o edit_i_tag"></i>
                                </button>
                                <?php if (substr($this->session->userdata('levels')->accleveltype, 3, 1) == '1' && $level->id != 1 && $level->id != 4 && $level->id != 5 && $level->id != 6) { ?>
                                    <button class="btn btn-sm btn-danger no_margin" onclick="delete_access_level(<?= $level->id ?>)" >
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
</div>