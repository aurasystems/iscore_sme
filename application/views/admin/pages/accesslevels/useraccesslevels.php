<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <div class="page-content page-thin">
                    <div class="header">
                        <div class="breadcrumb-wrapper editWrapper">
                            <ol class="breadcrumb">
                                <li><a href="<?= base_url() ?>admin/Dashboard"><?= $this->lang->line('lang_dashboard') ?></a></li>
                                <li><?= $this->lang->line('lang_settings') ?></li>
                                <li class="active"><?= $this->lang->line('access_levels') ?></li>
                            </ol>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-12 ">
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><i class="fa fa-key"></i> <?= $this->lang->line("access_levels") ?></h2>
                                </div>
                                <div class="panel-content">
                                    <div class="">
                                        <div class="tab-pane fade active in">
                                            <?php if ($this->session->flashdata('msg')) { ?>
                                                <div class="row">
                                                    <div class="col-md-12">                                        
                                                        <div class="alert alert-block alert-success fade in">
                                                            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                                            <p></p><h4><i class="fa fa-check"></i> <?= $this->lang->line('lang_success') ?></h4>
                                                            <?= $this->session->flashdata('msg') ?>                                                
                                                            <p></p>
                                                        </div>                                       
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12">
                                                    <ul class="nav nav-tabs nav-primary">
                                                        <!--<li class="active"><a data-toggle="tab" href="#tab2_1" aria-expanded="false"><i class="fa fa-plus-circle" aria-hidden="true"></i> <?= $this->lang->line('add_level_type') ?></a></li>-->
                                                        <li class="active"><a data-toggle="tab" href="#tab2_2" aria-expanded="false"><i class="fa fa-table" aria-hidden="true"></i> <?= $this->lang->line('users_access_level') ?></a></li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div id="tab2_1" class="tab-pane fade">
                                                            <?php  $this->load->view('admin/pages/accesslevels/tabs/addtype') ?>
                                                        </div>
                                                        <div id="tab2_2" class="tab-pane fade active in">
                                                            <?php $this->load->view('admin/pages/accesslevels/tabs/accesslevel') ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script type='text/javascript'>
            function edit_access_level($id) {
                $("#acc_level_name_" + $id).removeAttr('disabled');
                $("#edit_btn_" + $id).slideUp();
                $("#update_btn_" + $id).slideDown();
            }
            function update_access_level($id) {
                $acc_level_name = $("#acc_level_name_" + $id).val();
                if ($acc_level_name != '') {
                    $('.loader-overlay').removeClass('loaded');
                    $.ajax({
                        url: '<?= base_url() ?>admin/Accesslevels/update_access_level',
                        type: "POST",
                        data: {id: $id, acc_level_name: $acc_level_name,"<?=$this->security->get_csrf_token_name()?>":"<?=$this->security->get_csrf_hash()?>"},
                        dataType: "JSON",
                        success: function (result) {
                            location.reload();
                        }
                    });
                }
            }
            function delete_access_level($id) {
            if (window.confirm("Are you sure ? You can't undo this action")) {
                confirm_delete_access_level($id);
            }
//                $("#modalMsg").html('<?= $this->lang->line('confirm_delete_access_level') ?>');
//                $("#deleteItemBtn").attr('onclick', 'confirm_delete_access_level(' + $id + ')');
//                $("#deleteItemModal").modal('show');
            }
            function confirm_delete_access_level($id) {
                $.ajax({
                    url: '<?= base_url() ?>admin/Accesslevels/do_delete_access_level',
                    type: "POST",
                    data: {id: $id,"<?=$this->security->get_csrf_token_name()?>":"<?=$this->security->get_csrf_hash()?>"},
                    dataType: "JSON",
                    success: function (success) {
                        if (success == true) {
                            location.reload();
                        }
                    }
                });
            }
            function getUserAccessLevel() {
                var $levelid = $("#levelid").val();
                $('.loader-overlay').removeClass('loaded');
                $.ajax({
                    url: '<?= base_url() ?>admin/Accesslevels/getLevelIdInfo',
                    type: "POST",
                    data: {levelid: $levelid,"<?=$this->security->get_csrf_token_name()?>":"<?=$this->security->get_csrf_hash()?>"},
                    dataType: "JSON",
                    success: function (result) {
                        if (result == 'no') {
                            $('input[type=checkbox]').each(function () {
                                $(this).removeAttr('checked');
                            });
                        }
                        else {
                            var i = 0;
<?php foreach ($cols as $col) { ?>
                                var col_name = '<?= $col->COL ?>';
                                if (col_name == 'bank_only') {
                                    if(result.bank_only){
                                    $("#bank_only").iCheck('check');
                                }
                                }
                                else if (col_name == 'branch_only') {
                                    if(result.branch_only){
                                    $("#branch_only").iCheck('check');
                                }
                                }
                                else if (col_name == 'approved_only') {
                                    if(result.approved_only){
                                    $("#approved_only").iCheck('check');
                                }
                                }
                                else if (col_name == 'completed_only') {
                                    if(result.completed_only){
                                    $("#completed_only").iCheck('check');
                                }
                                }
                                else{
                                if (col_name != 'id' && col_name != 'levelid') {
                                    checkUnckeckLevel(i, result.<?= $col->COL ?>);
                                    i++;
                                }
                                }
<?php } ?>
                        }
                        if (result.levelid == '1') {
                            $('input[type=checkbox]').each(function () {
                                $(this).attr('disabled', 'disabled');
                            });
                            $("#saveBtn").slideUp();
                        } else {
                            $('input[type=checkbox]').each(function () {
                                $(this).removeAttr('disabled');
                            });
                            $("#saveBtn").slideDown();
                        }
                        $('.loader-overlay').addClass('loaded');
                    }
                });
            }
            function checkUnckeckLevel($selector, $value) {
            
                if ($value) {
                    if ($value.substring(0, 1) == 1) {
                        $("#" + $selector + "0").iCheck('check');
                      //  $("#" + $selector + "0").prop('checked', true);
                    } else {
                        $("#" + $selector + "0").iCheck('uncheck');
                    }
                    if ($value.substring(1, 2) == 1) {

                        $("#" + $selector + "1").iCheck('check');
                       // $("#" + $selector + "1").prop('checked', true);
                    } else {
                        $("#" + $selector + "1").iCheck('uncheck');
                    }
                    if ($value.substring(2, 3) == 1) {

                        $("#" + $selector + "2").iCheck('check');
                      //  $("#" + $selector + "2").prop('checked', true);

                    } else {
                        $("#" + $selector + "2").iCheck('uncheck');
                    }
                    if ($value.substring(3, 4) == 1) {
                        $("#" + $selector + "3").iCheck('check');
                       // $("#" + $selector + "3").prop('checked', true);
                    } else {
                        $("#" + $selector + "3").iCheck('uncheck');
                    }
                } else {
                    $("#" + $selector + "0").iCheck('uncheck');
                    $("#" + $selector + "1").iCheck('uncheck');
                    $("#" + $selector + "2").iCheck('uncheck');
                    $("#" + $selector + "3").iCheck('uncheck');
                }
            }
            $(function () {
                $(".checkAll").on('ifToggled', function () {
                    var $class = $(this).attr('id');
                    if ($(this).attr('checked')) {
                        $("." + $class).each(function () {
                            $(this).iCheck('check');
//                            $(this).attr('checked', 'checked');
//                            $(this).prop('checked', true);
                        });
                    } else {
                        $("." + $class).each(function () {
                            $(this).iCheck('uncheck');
                         //   $(this).removeAttr('checked');
                        });
                    }
                });
            });
//            function checkAll($class) {
//                if ($("#" + $class + "AllCheck").prop('checked') == true) {
//                    $("." + $class).each(function () {
//                        $(this).attr('checked', 'checked');
//                        $(this).prop('checked', true);
//                    });
//                } else {
//                    $("." + $class).each(function () {
//                        $(this).removeAttr('checked');
//                    });
//                }
//            }
        </script>
    </body>
</html>