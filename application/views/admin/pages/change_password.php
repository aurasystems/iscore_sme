<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= MY_APP_NAME ?> | <?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="" name="description" />
        <meta content="themes-lab" name="author" />
        <link rel="shortcut icon" href="<?= base_url() ?>assets/img/favicon.png">
        <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/ui.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/custom.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/plugins/bootstrap-loading/lada.min.css" rel="stylesheet">
       
    </head>
    <body class="account" data-page="login">
        <!-- BEGIN LOGIN BOX -->
        <div class="container" id="login-block">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <div class="account-wall">
                        <i class="user-img icons-faces-users-03"></i>
                        <div class="row editErrorMsg">
                            <?php if($this->session->flashdata('msg')){ ?>
                            <?php echo $this->session->flashdata('msg'); }?>
                        </div>
                        <?php echo form_open('admin/Login/change_password'); ?>
                        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                            <div class="append-icon m-b-20">
                                <?php echo form_input(array('type' => 'password','id' => 'old_password', 'name' => 'old_password', 'class'=>'form-control form-white old_password' ,'placeholder'=>$this->lang->line("old_password"),'required'=>'required',"autocomplete"=>"off")); ?>
                                <i class="icon-lock"></i>
                                <span class="c-red"><?php echo form_error('old_password'); ?></span>
                            </div>
                          <div class="append-icon m-b-20">
                                <?php echo form_input(array('type' => 'password','id' => 'new_password', 'name' => 'new_password', 'class'=>'form-control form-white new_password' ,'placeholder'=>$this->lang->line("new_password"),'required'=>'required',"autocomplete"=>"off")); ?>
                                <i class="icon-lock"></i>
                                <span class="c-red"> <?php echo form_error('new_password'); ?></span>
                            </div>
                        <div class="progress" style="display: none;">
                        <div id="result" class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="25"
                             aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                        </div>
                      </div>
                        <?php echo form_submit(array('disabled'=>"",'id'=>"submit",'class'=>'btn btn-lg btn-danger btn-block ladda-button','data-style' => 'expand-left','value' => $this->lang->line('change_password'))); ?>
<!--                            <div class="text-center">
                                <a href="<?= base_url() ?>admin/Login"><?= $this->lang->line('lang_back_to_login') ?></a>
                            </div>-->
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <script src="<?= base_url() ?>assets/plugins/jquery/jquery-1.11.1.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/gsap/main-gsap.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/backstretch/backstretch.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-loading/lada.min.js"></script>
        <?php $this->load->view('admin/private/scripts/login_js'); ?>
        <script>
        $(document).ready(function(){
            $("#submit").removeAttr('disabled');
//            BEGIN PASSWORD STRENGTH
<?php if(!empty($password_management) && $password_management->complex){ ?>
        
                    $("#submit").attr("disabled","disabled");
            $('#new_password').keyup(function() {
                checkStrength($('#new_password').val());
            });
            function checkStrength(password) {
                
            var strength = 0;
            <?php
            if(!empty($password_management) && $password_management->min_length){
                ?>
                            var min_length= "<?=$password_management->min_length?>";
            if (password.length < min_length) {
           progress_bar(25,"progress-bar-danger","<?=$this->lang->line("too_short")?>");
            }
            else{
                strength += 1;
            }
                <?php
            }
            else{
                ?>
                            strength+=1;
                            <?php
            }
            ?>
            // If password contains both lower and uppercase characters, increase strength value.
            if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1;
            // If it has numbers and characters, increase strength value.
            if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1;
            // If it has one special character, increase strength value.
            if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1;
            // If it has two special characters, increase strength value.
            if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1;
            // Calculated strength value, we can return messages
            // If value is less than 2
            console.log(strength);
            if (strength < 2) {
                progress_bar(50,"progress-bar-danger","<?=$this->lang->line("weak")?>");
            } else if (strength <=3) {
           progress_bar(75,"progress-bar-warning","<?=$this->lang->line("medium")?>");
            } else {
           progress_bar(100,"progress-bar-success","<?=$this->lang->line("strong")?>");
            }
            }
            
            
            function progress_bar(per,c,text){
                $("#result").css("width",per+"%");
                $("#result").attr("aria-valuenow",per);
                $("#result").removeClass("progress-bar-danger");
                $("#result").removeClass("progress-bar-warning");
                $("#result").removeClass("progress-bar-success");
                $("#result").addClass(c);
                $("#result").html(text);
                $(".progress").show();
                if(per==100){
                    $("#submit").removeAttr('disabled');
                }
                else{
                    $("#submit").attr("disabled","disabled");
                }
            }
<?php } ?>
//            END PASSWORD STRENGTH
        });
        </script>
        
    </body>
</html>