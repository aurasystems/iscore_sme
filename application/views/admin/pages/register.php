<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= MY_APP_NAME ?> | <?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="" name="description" />
        <meta content="themes-lab" name="author" />
        <link rel="shortcut icon" href="assets/img/favicon.png">
        <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/ui.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/plugins/icheck/skins/all.css" rel="stylesheet"/>
        <link href="<?= base_url() ?>assets/plugins/bootstrap-loading/lada.min.css" rel="stylesheet">
    </head>
    <body class="account separate-inputs boxed" data-page="signup">
        <!-- BEGIN LOGIN BOX -->
        <div class="container" id="login-block">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-md-offset-3">
                    <div class="account-wall">
                        <i class="user-img icons-faces-users-03"></i>
                        <div class="row editErrorMsg">
                            <?php if($this->session->flashdata('msg')){ ?>
                            <?php echo $this->session->flashdata('msg'); }?>
                        </div>
                        <?php $attributes = array('id' => 'submit_form','class' => 'form-signup', 'role'=>'form'); ?>
                        <?php echo form_open('admin/Register/do_register'); ?>
                        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="append-icon">
                                        <?php echo form_input(array('type' => 'text','id' => 'first_name', 'name' => 'first_name', 'class'=>'form-control form-white first_name','value' => set_value('first_name') ,'placeholder'=>'First Name','required'=>'required','autofocus' => 'autofocus')); ?>
                                        <i class="icon-user"></i>
                                        <?php echo form_error('first_name'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="append-icon">
                                        <?php echo form_input(array('type' => 'text','id' => 'last_name', 'name' => 'last_name', 'class'=>'form-control form-white last_name','value' => set_value('last_name') ,'placeholder'=>'Last Name','required'=>'required')); ?>
                                        <i class="icon-user"></i>
                                        <?php echo form_error('last_name'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="append-icon">
                                        <?php echo form_input(array('type' => 'text','id' => 'display_name', 'name' => 'display_name', 'class'=>'form-control form-white display_name','value' => set_value('display_name') ,'placeholder'=>'Display Name','required'=>'required')); ?>
                                        <i class="icon-user"></i>
                                        <?php echo form_error('display_name'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="append-icon">
                                        <?php echo form_input(array('type' => 'email','id' => 'email', 'name' => 'email', 'class'=>'form-control form-white email','value' => set_value('email') ,'placeholder'=>'Email','required'=>'required')); ?>
                                        <i class="icon-envelope"></i>
                                        <?php echo form_error('email'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="append-icon">
                                        <?php echo form_input(array('type' => 'password','id' => 'password', 'name' => 'password', 'class'=>'form-control form-white password','placeholder'=>'Password','required'=>'required',"autocomplete"=>"off")); ?>
                                        <i class="icon-lock"></i>
                                        <?php echo form_error('password'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="append-icon m-b-20">
                                        <?php echo form_input(array('type' => 'password','id' => 'password2', 'name' => 'password2', 'class'=>'form-control form-white password2','placeholder'=>'Repeat Password','required'=>'required',"autocomplete"=>"off")); ?>
                                        <i class="icon-lock"></i>
                                        <?php echo form_error('password2'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="terms option-group">
                                <label  for="terms" class="m-t-10">
                                    <?php echo form_checkbox(array('type' => 'checkbox','id'=>'terms','name'=>'terms','data-checkbox' => 'icheckbox_square-blue','required' => 'required','value' => 1)); ?>
                                <?= $this->lang->line('lang_terms_conditions') ?>
                                <?php echo form_error('terms'); ?>
                                </label>  
                            </div>
                            <div class="text-center">
                                <?php echo form_submit(array('class'=>'btn btn-lg btn-dark m-t-20','data-style' => 'expand-left','value' => $this->lang->line('lang_register'))); ?>
                            </div>
                            <div class="text-center">
                                <a href="<?= base_url() ?>admin/Login"><?= $this->lang->line('lang_have_account') ?></a>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- END LOCKSCREEN BOX -->
        <script src="<?= base_url() ?>assets/plugins/jquery/jquery-1.11.1.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/gsap/main-gsap.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/icheck/icheck.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/backstretch/backstretch.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-loading/lada.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/jquery-validation/additional-methods.min.js"></script>
        <script src="<?= base_url() ?>assets/js/plugins.js"></script>
        <?php $this->load->view('admin/private/scripts/login_js'); ?>
    </body>
</html>