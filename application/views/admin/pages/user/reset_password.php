<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url() ?>assets/plugins/jquery-ui/jquery-ui-1.10.3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="header">
                        <div class="breadcrumb-wrapper editWrapper">
                            <ol class="breadcrumb">
                                <li><a href="<?= base_url() ?>admin/Dashboard"><?= $this->lang->line('lang_dashboard') ?></a></li>
                                <li class="active"><?= $this->lang->line('reset_password') ?></li>
                            </ol>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><i class="fa fa-user"></i> <?= $this->lang->line("reset_password") ?></h2>
                        </div>

                        <div class="panel-content row">
                            <div class="col-md-12">
                                <?php if ($this->session->flashdata('error')) { ?>
                                    <div class="alert alert-block alert-danger fade in">
                                        <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                        <p><strong><h4><i class="fa fa-close"></i> <?= $this->lang->line('lang_error') ?></h4></strong> <?= $this->session->flashdata('error') ?></p>
                                    </div>
                                <?php } ?>
                                <?php if (isset($error)) { ?>
                                    <div class="alert alert-block alert-danger fade in">
                                        <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                        <p><strong><h4><i class="fa fa-close"></i> <?= $this->lang->line('lang_error') ?></h4></strong> <?= $error ?></p>
                                    </div>
                                <?php } ?>
                                <div class="box-body big col-md-12">
                                    <div class="row editErrorMsg editThisMsg">
                                        <?php if ($this->session->flashdata('msg')) { ?>
                                            <?php
                                            echo $this->session->flashdata('msg');
                                        }
                                        ?>
                                    </div>
                                    <?php echo form_open_multipart('admin/User/reset_pass/' . $system_user_id, $attributes); ?>
                                    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
                                    <?php if ($this->session->userdata('access_level') != 1 && !($this->session->userdata('access_level') == 4 && $user_data->bank_id == $this->session->userdata('bank_id'))) { ?>
                                        <div class="form-group">
                                            <?php echo form_label($this->lang->line('old_password') . ':', 'old_password', $label_att); ?>
                                            <div class="col-sm-7">
                                                <?php echo form_input(array('type' => 'password', 'id' => 'old_password', 'name' => 'old_password', 'class' => 'form-control', 'placeholder' => 'Old Password', "autocomplete" => "off")); ?>
                                                <span class="c-red"><?php echo form_error('old_password'); ?></span>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_password') . ':', 'lang_password', $label_att); ?>
                                        <div class="col-sm-7">
                                            <?php echo form_input(array('type' => 'password', 'id' => 'password', 'name' => 'password', 'class' => 'form-control', 'placeholder' => 'Type password if you want to change it.', "autocomplete" => "off")); ?>
                                            <span class="c-red"><?php echo form_error('password'); ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-7 progress" style="display: none;">
                                            <div id="result" class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="25"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_repeat_password') . ':', 'lang_repeat_password', $label_att); ?>
                                        <div class="col-sm-7">
                                            <?php echo form_input(array('type' => 'password', 'id' => 'password2', 'name' => 'password2', 'class' => 'form-control', 'placeholder' => 'Confirm Password', "autocomplete" => "off")); ?>
                                            <span class="c-red"><?php echo form_error('password2'); ?></span>
                                        </div>
                                    </div>
                                    <?php if ($this->session->userdata('user_id') != $system_user_id) { ?>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label"><?= $this->lang->line("force_reset") ?> </label>

                                            <div class="col-sm-7">
                                                <input <?= $this->input->post("force_reset") ? "checked" : "" ?> type="checkbox" class="form-control" name="force_reset" id="force_reset" value="1"/>
                                                <span class="c-red"><?php echo form_error('force_reset'); ?></span>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <br/>
                                    <br/>
                                    <div class="text-center">
                                        <?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('lang_save'), 'class' => 'btn btn-primary')); ?>
                                        <span class="btn btn-default btn-mini"><a style="text-decoration: none;" href="<?= base_url() ?>admin/User/get_system_users"><?= $this->lang->line('lang_cancel') ?></a></span>
                                    </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>

                    <!-- END PAGE CONTENT -->
                </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
//            BEGIN PASSWORD STRENGTH
<?php if (!empty($password_management) && $password_management->complex) { ?>
                    $('#password').keyup(function () {
                        checkStrength($('#password').val());
                    });
                    function checkStrength(password) {

                        var strength = 0;
    <?php
    if (!empty($password_management) && $password_management->min_length) {
        ?>
                            var min_length = "<?= $password_management->min_length ?>";
                            if (password.length < min_length) {
                                progress_bar(25, "progress-bar-danger", "<?= $this->lang->line("too_short") ?>");
                            }
                            else {
                                strength += 1;
                            }
        <?php
    } else {
        ?>
                            strength += 1;
        <?php
    }
    ?>
                        // If password contains both lower and uppercase characters, increase strength value.
                        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
                            strength += 1;
                        // If it has numbers and characters, increase strength value.
                        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
                            strength += 1;
                        // If it has one special character, increase strength value.
                        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
                            strength += 1;
                        // If it has two special characters, increase strength value.
                        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
                            strength += 1;
                        // Calculated strength value, we can return messages
                        // If value is less than 2
                        console.log(strength);
                        if (strength < 2) {
                            progress_bar(50, "progress-bar-danger", "<?= $this->lang->line("weak") ?>");
                        } else if (strength <= 3) {
                            progress_bar(75, "progress-bar-warning", "<?= $this->lang->line("medium") ?>");
                        } else {
                            progress_bar(100, "progress-bar-success", "<?= $this->lang->line("strong") ?>");
                        }
                    }


                    function progress_bar(per, c, text) {
                        $("#result").css("width", per + "%");
                        $("#result").attr("aria-valuenow", per);
                        $("#result").removeClass("progress-bar-danger");
                        $("#result").removeClass("progress-bar-warning");
                        $("#result").removeClass("progress-bar-success");
                        $("#result").addClass(c);
                        $("#result").html(text);
                        $(".progress").show();
                        if (per == 100) {
                            $("#submit").removeAttr('disabled');
                        }
                        else {
                            $("#submit").attr("disabled", "disabled");
                        }
                    }
<?php } ?>
//            END PASSWORD STRENGTH
            });
        </script>
    </body>
</html>