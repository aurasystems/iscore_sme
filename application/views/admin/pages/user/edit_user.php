<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url() ?>assets/plugins/jquery-ui/jquery-ui-1.10.3.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url() ?>assets/css/multi_text_inputs.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="header">
                        <div class="breadcrumb-wrapper editWrapper">
                            <ol class="breadcrumb">
                                <li><a href="<?= base_url() ?>admin/Dashboard"><?= $this->lang->line('lang_dashboard') ?></a></li>
                                <li class="active"><?= $this->lang->line('lang_edit_user') ?></li>
                            </ol>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><i class="fa fa-user"></i> <?= $this->lang->line("lang_edit_user") ?></h2>
                        </div>

                        <div class="panel-content row">
                            <div class="col-md-12">
                                <?php if ($status == "success") { ?>
                                    <div class="alert alert-block alert-success fade in">
                                        <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                        <p></p><h4><i class="fa fa-check"></i> <?= $this->lang->line('lang_success') ?></h4> <?= $this->lang->line('atumsl_alertsettingssuccess') ?><p></p>
                                    </div>
                                <?php } ?>
                                <?php if ($this->session->flashdata('error')) { ?>
                                    <div class="alert alert-block alert-danger fade in">
                                        <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                        <p><strong><h4><i class="fa fa-close"></i> <?= $this->lang->line('lang_error') ?></h4></strong> <?= $this->session->flashdata('error') ?></p>
                                    </div>
                                <?php } ?>
                                <div class="box-body big col-md-12">
                                    <div class="row editErrorMsg editThisMsg">
                                        <?php if ($this->session->flashdata('msg')) { ?>
                                            <?php
                                            echo $this->session->flashdata('msg');
                                        }
                                        ?>
                                    </div>
                                    <?php
                                    $attributes['id'] = "user_form";
                                    echo form_open_multipart('admin/User/update_user/' . $system_user_id, $attributes);
                                    ?>
                                    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_user_img') . ':', 'lang_user_img', $label_att); ?>
                                        <div class="col-sm-7 controls">
                                            <?php
                                            if ($user_data) {
                                                if ($user_data->image) {
                                                    $image = $user_data->image;
                                                } else {
                                                    $image = 'default_avatar.png';
                                                }
                                                ?>
                                                <img src="<?= base_url() ?>uploads/users/<?= $image ?>" alt="user image" class="img-responsive thumbnail" width="100" >
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_upload_user_img') . ':', 'lang_upload_user_img', $label_att); ?> 
                                        <div class="col-sm-7">
                                            <?php echo form_input(array('type' => 'file', 'id' => 'userfile', 'name' => 'userfile', 'class' => 'form-control', 'size' => 20)); ?>
                                            <span class="c-red"><?php echo form_error('userfile'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <label class="col-sm-4 control-label"><?= $this->lang->line("lang_first_name") ?> * </label>

                                        <div class="col-sm-7">
                                            <?php
                                            $first_name = '';
                                            if ($this->input->post()) {
                                                $first_name = $this->input->post('first_name');
                                            } else if ($user_data) {
                                                $first_name = $user_data->first_name;
                                            }
                                            ?>
                                            <?php echo form_input(array('id' => 'first_name', 'name' => 'first_name', 'class' => 'form-control', 'placeholder' => 'FIRST NAME', 'value' => $first_name)); ?>
                                            <span class="c-red"><?php echo form_error('first_name'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= $this->lang->line("lang_last_name") ?> * </label>
                                        <div class="col-sm-7">
                                            <?php
                                            $last_name = '';
                                            if ($this->input->post()) {
                                                $last_name = $this->input->post('last_name');
                                            } else if ($user_data) {
                                                $last_name = $user_data->last_name;
                                            }
                                            ?>
                                            <?php echo form_input(array('id' => 'last_name', 'name' => 'last_name', 'class' => 'form-control', 'placeholder' => 'LAST NAME', 'value' => $last_name)); ?>
                                            <span class="c-red"><?php echo form_error('last_name'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= $this->lang->line("lang_display_name") ?> * </label>
                                        <div class="col-sm-7">
                                            <?php
                                            $display_name = '';
                                            if ($this->input->post()) {
                                                $display_name = $this->input->post('display_name');
                                            } else if ($user_data) {
                                                $display_name = $user_data->display_name;
                                            }
                                            ?>
                                            <?php echo form_input(array('id' => 'display_name', 'name' => 'display_name', 'class' => 'form-control', 'placeholder' => 'DISPLAY NAME', 'value' => $display_name)); ?>
                                            <span class="c-red"><?php echo form_error('display_name'); ?></span>
                                        </div>
                                    </div>
                                    <?php
                                    if (compare_level("super_admin")) {
                                        ?>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label"><?= $this->lang->line("lang_email_address") ?> * </label>
                                            <div class="col-sm-7">
                                                <?php
                                                $email = '';
                                                if ($this->input->post()) {
                                                    $email = $this->input->post('email');
                                                } else if ($user_data) {
                                                    $email = $user_data->email;
                                                }
                                                ?>
                                                <?php echo form_input(array('type' => 'email', 'id' => 'email', 'name' => 'email', 'class' => 'form-control', 'placeholder' => 'EMAIL', 'value' => $email)); ?>
                                                <span class="c-red"><?php echo form_error('email'); ?></span>
                                            </div>
                                        </div>

                                        <?php
                                    }
                                    ?>
                                    <?php if ($this->session->userdata("access_level") == 1 || $this->session->userdata("user_id") != $system_user_id) { ?>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label"><?= $this->lang->line("access_level") ?> * </label>
                                            <div class="col-sm-7">
                                                <?php
                                                $access_level = '';
                                                if ($this->input->post()) {
                                                    $access_level = $this->input->post('access_level');
                                                } else if ($user_data) {
                                                    $access_level = $user_data->access_level;
                                                }
                                                ?>
                                                <select class="form-control" name="access_level">
                                                    <option value><?= $this->lang->line('select') ?></option>
                                                    <?php
                                                    foreach ($levels as $level) {
                                                        if (($level->id == 1 && substr($this->session->userdata('levels')->users_super_admin, 1, 1) == '0') ||
                                                                ($level->id == 4 && substr($this->session->userdata('levels')->users_bank_managers, 1, 1) == '0') ||
                                                                ($level->id == 5 && substr($this->session->userdata('levels')->users_can_approve, 1, 1) == '0') ||
                                                                ($level->id == 7 && substr($this->session->userdata('levels')->users_can_approve, 1, 1) == '0') ||
                                                                ($level->id == 6 && substr($this->session->userdata('levels')->users_normal, 1, 1) == '0') ||
                                                                ($level->id != 1 && $level->id != 4 && $level->id != 5 && $level->id != 6 && $level->id != 7 && substr($this->session->userdata('levels')->users_custom_levels, 1, 1) == '0')) {
                                                            continue;
                                                        }
                                                        $selected = '';
                                                        if ($level->id == $access_level) {
                                                            $selected = 'selected';
                                                        }
                                                        ?>
                                                        <option <?= $selected ?> value="<?= $level->id ?>"><?= $level->name ?></option>
                                                    <?php } ?>
                                                </select>
                                                <span class="c-red"><?php echo form_error('access_level'); ?></span>
                                            </div>
                                        </div>
                                        <?php if (!compare_level("bank_admin")) { ?>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label"><?= $this->lang->line("bank") ?> * </label>
                                                <div class="col-sm-7">
                                                    <select class="form-control" name="bank" id="bank">
                                                        <option value><?= $this->lang->line('select') ?></option>
                                                        <?php
                                                        $bank_value = '';
                                                        if ($this->input->post()) {
                                                            $bank_value = $this->input->post('bank');
                                                        } else if ($user_data) {
                                                            $bank_value = $user_data->bank_id;
                                                        }
                                                        foreach ($banks as $bank) {
                                                            $selected = '';
                                                            if ($bank->id == $bank_value) {
                                                                $selected = 'selected';
                                                            }
                                                            ?>
                                                            <option <?= $selected ?> value="<?= $bank->id ?>"><?= $bank->name ?></option>
                                                        <?php } ?>
                                                    </select>
                                                    <span class="c-red"><?php echo form_error('bank'); ?></span>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <input type="hidden" name="bank" value="<?= get_session_user_bank() ?>" />                               
                                        <?php } ?>

                                        <!--                                    <div class="form-group">
                                                                                <label class="col-sm-4 control-label"><?= $this->lang->line("branch_code") ?> * </label>
                                                                                <div class="col-sm-7">
                                        <?php
                                        $branch_code = '';
                                        if ($this->input->post()) {
                                            $branch_code = $this->input->post('branch_code');
                                        } else if ($user_data) {
                                            $branch_code = $user_data->branch_code;
                                        }
                                        ?>
                                                                                    <input type="text" class="form-control" name="branch_code" id="branch_code"  value="<?= $branch_code ?>" />
                                                                                    <span class="c-red"><?php echo form_error('branch_code'); ?></span>
                                                                                </div>
                                                                            </div>-->

                                        <div class='form-group'>
                                            <label class="col-sm-4 control-label"><?= $this->lang->line("branch_code") ?> * </label>
                                            <div class='col-sm-7'>
                                                <?php
                                                $arr = array();
                                                if ($this->input->post()) {
                                                    foreach ($this->input->post('branch_code') as $value) {
                                                        $arr[] = $value;
                                                    }
                                                } else if ($user_data) {
                                                    $arr = explode(',', $user_data->branch_code);
                                                }
                                                ?>
                                                <input type='text' id='example_email' class='form-control' value='<?= json_encode($arr) ?>' />
                                                <span class="c-red"><?php echo form_error('branch_code[]'); ?></span>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <br/>
                                    <br/>
                                    <div class="text-center">
                                        <?php echo form_submit(array('id' => 'btn_submit', 'value' => $this->lang->line('lang_save'), 'class' => 'btn btn-primary')); ?>
                                        <span class="btn btn-default btn-mini"><a style="text-decoration: none;" href="<?= base_url() ?>admin/User/get_system_users"><?= $this->lang->line('lang_cancel') ?></a></span>
                                    </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>

                    <!-- END PAGE CONTENT -->
                </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/js/multi_text_inputs.js" type="text/javascript"></script>
        <script>
            $(function () {
                $('#current_emails').text($('#example_email').val());
                $('#example_email').multiple_emails();
                $('#example_email').change(function () {
                    $('#current_emails').text($(this).val());
                });

                $("#bank").change(function () {

                    $("#branch_code").autocomplete({disabled: true});
                    if ($("#bank").val() == '') {
                        return;
                    }
                    $.ajax({
                        url: '<?= base_url() ?>admin/Banks/get_bank_branches/' + $("#bank option:selected").text(),
                        type: "POST",
                        dataType: "JSON",
                        data: {"<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>"},
                        success: function (result) {
                            if (result.length > 2) {
                                $("#branch_code").autocomplete({
                                    source: result,
                                    disabled: false
                                });
                            }
                        }
                    });
                });
                $("#bank").change();

                //disable special characters
                $("#btn_submit").click(function (e) {
                    e.preventDefault();
                    var first_name = $("#first_name").val();
                    var last_name = $("#last_name").val();
                    var display_name = $("#display_name").val();
                    //var branch_code = $("#branch_code").val();

                    first_name = first_name.split(' ').join('');
                    last_name = last_name.split(' ').join('');
                    display_name = display_name.split(' ').join('');
<?php //if ($this->session->userdata("user_id") != $system_user_id && !compare_level("maker") && !compare_level("verifier")) {   ?>
                    //branch_code = branch_code.split(' ').join('');
<?php //}   ?>

                    var regex = new RegExp("^[a-zA-Z0-9]+$");
                    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                    console.log(regex.test(e.which));
                    console.log(regex.test(e.charCode));
<?php if ($this->session->userdata("user_id") != $system_user_id && !compare_level("maker") && !compare_level("verifier")) { ?>
                        var error = 0;
                        $("input[name='branch_code[]']").each(function () {
                            if (!regex.test($(this).val())) {
                                alert("<?= $this->lang->line("no_special_chars") ?>");
                                error = 1;
                                return;
                            }
                        });
                        if (error) {
                            return;
                        }
<?php } ?>
                    if (regex.test(first_name) && regex.test(last_name) && regex.test(display_name)) {
                        $("#user_form").submit();
                    }
                    else {
                        alert("<?= $this->lang->line("no_special_chars") ?>");
                    }
                });
                /////////////////////////////

            });
        </script>
    </body>
</html>