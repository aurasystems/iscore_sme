<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="header">
                        <div class="breadcrumb-wrapper editWrapper">
                            <ol class="breadcrumb">
                                <li><a href="<?= base_url() ?>admin/Dashboard"><?= $this->lang->line('lang_dashboard') ?></a></li>
                                <li class="active"><?= $this->lang->line('lang_delete_user') ?></li>
                            </ol>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><i class="fa fa-user"></i> <?= $this->lang->line("lang_delete_user") ?></h2>
                        </div>

                        <div class="panel-content row">
                            <div class="col-md-12">
                                <?php if ($status == "success") { ?>
                                    <div class="alert alert-block alert-success fade in">
                                        <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                        <p></p><h4><i class="fa fa-check"></i> <?= $this->lang->line('lang_success') ?></h4> <?= $this->lang->line('atumsl_alertsettingssuccess') ?><p></p>
                                    </div>
                                <?php } ?>
                                <div class="panel">
                                    <div class="panel-content">
                                        <div class="filter-left">
                                            <!-- BOX -->
                                            <div class="box border red">
                                                <div class="box-body big">
                                                    <div class="alert alert-block alert-danger fade in"><?= $this->lang->line('lang_confirm_delete_user') ?><br /><br /><br />
                                                        <div class="right"><a href="<?= site_url() ?>admin/User/delete_user/<?= $system_user_id ?>"><button class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</button></a><a href="<?= site_url() ?>admin/User/get_system_users"> <button class="btn btn-default"> Cancel</button></a></div>
                                                    </div>      
                                                </div>
                                            </div>
                                            <!-- /BOX -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
    </body>
</html>