<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url() ?>assets/plugins/jquery-ui/jquery-ui-1.10.3.css" rel="stylesheet" type="text/css"/>
        <link href="<?= base_url() ?>assets/css/multi_text_inputs.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="header">
                        <div class="breadcrumb-wrapper editWrapper">
                            <ol class="breadcrumb">
                                <li><a href="<?= base_url() ?>admin/Dashboard"><?= $this->lang->line('lang_dashboard') ?></a></li>
                                <li class="active"><?= $this->lang->line('lang_add_user') ?></li>
                            </ol>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><i class="fa fa-user"></i> <?= $this->lang->line("lang_add_user") ?></h2>
                        </div>

                        <div class="panel-content row">
                            <div class="col-md-12">
                                <?php if ($status == "success") { ?>
                                    <div class="alert alert-block alert-success fade in">
                                        <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                        <p></p><h4><i class="fa fa-check"></i> <?= $this->lang->line('lang_success') ?></h4> <?= $this->lang->line('atumsl_alertsettingssuccess') ?><p></p>
                                    </div>
                                <?php } ?>
                                <?php if ($this->session->flashdata('error')) { ?>
                                    <div class="alert alert-block alert-danger fade in">
                                        <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                        <p><strong><h4><i class="fa fa-close"></i> <?= $this->lang->line('lang_error') ?></h4></strong> <?= $this->session->flashdata('error') ?></p>
                                    </div>
                                <?php } ?>
                                <div class="box-body big col-md-12">
                                    <div class="row editErrorMsg editThisMsg">
                                        <?php if ($this->session->flashdata('msg')) { ?>
                                            <?php
                                            echo $this->session->flashdata('msg');
                                        }
                                        ?>
                                    </div>
                                    <?php
                                    $attributes['id'] = "user_form";
                                    echo form_open_multipart('admin/User/add_user', $attributes);
                                    ?>
                                    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_upload_user_img') . ':', 'lang_upload_user_img', $label_att); ?> 
                                        <div class="col-sm-7">
                                            <?php echo form_input(array('type' => 'file', 'id' => 'userfile', 'name' => 'userfile', 'class' => 'form-control', 'size' => 20)); ?>
                                            <span class="c-red"><?php echo form_error('userfile'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= $this->lang->line("lang_first_name") ?> * </label>
                                        <div class="col-sm-7">
                                            <?php echo form_input(array('id' => 'first_name', 'name' => 'first_name', 'class' => 'form-control', 'value' => set_value('first_name'), 'placeholder' => 'FIRST NAME', 'required' => 'required')); ?>
                                            <span class="c-red"><?php echo form_error('first_name'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= $this->lang->line("lang_last_name") ?> * </label>
                                        <div class="col-sm-7">
                                            <?php echo form_input(array('id' => 'last_name', 'name' => 'last_name', 'class' => 'form-control', 'value' => set_value('last_name'), 'placeholder' => 'LAST NAME', 'required' => 'required')); ?>
                                            <span class="c-red"><?php echo form_error('last_name'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= $this->lang->line("lang_display_name") ?> * </label>
                                        <div class="col-sm-7">
                                            <?php echo form_input(array('id' => 'display_name', 'name' => 'display_name', 'class' => 'form-control', 'placeholder' => 'DISPLAY NAME', 'value' => set_value('display_name'))); ?>
                                            <span class="c-red"><?php echo form_error('display_name'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= $this->lang->line("lang_email_address") ?> * </label>
                                        <div class="col-sm-7">
                                            <?php echo form_input(array('type' => 'email', 'id' => 'email', 'name' => 'email', 'class' => 'form-control', 'value' => set_value('email'), 'placeholder' => 'EMAIL', 'required' => 'required')); ?>
                                            <span class="c-red"><?php echo form_error('email'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= $this->lang->line("lang_password") ?> * </label>
                                        <div class="col-sm-7">
                                            <?php echo form_input(array('type' => 'password', 'id' => 'password', 'name' => 'password', 'class' => 'form-control', 'placeholder' => 'PASSWORD', 'required' => 'required', "autocomplete" => "off")); ?>
                                            <span class="c-red"><?php echo form_error('password'); ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-7 progress" style="display: none;">
                                            <div id="result" class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="25"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= $this->lang->line("lang_repeat_password") ?> * </label>
                                        <div class="col-sm-7">
                                            <?php echo form_input(array('type' => 'password', 'id' => 'password2', 'name' => 'password2', 'class' => 'form-control', 'placeholder' => 'CONFIRM PASSWORD', 'required' => 'required', "autocomplete" => "off")); ?>
                                            <span class="c-red"><?php echo form_error('password2'); ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= $this->lang->line("access_level") ?> * </label>
                                        <div class="col-sm-7">
                                            <select class="form-control" name="access_level">
                                                <option value><?= $this->lang->line('select') ?></option>
                                                <?php
                                                foreach ($levels as $level) {
                                                    if (($level->id == 1 && substr($this->session->userdata('levels')->users_super_admin, 1, 1) == '0') ||
                                                            ($level->id == 4 && substr($this->session->userdata('levels')->users_bank_managers, 1, 1) == '0') ||
                                                            ($level->id == 5 && substr($this->session->userdata('levels')->users_can_approve, 1, 1) == '0') ||
                                                            ($level->id == 7 && substr($this->session->userdata('levels')->users_can_approve, 1, 1) == '0') ||
                                                            ($level->id == 6 && substr($this->session->userdata('levels')->users_normal, 1, 1) == '0') ||
                                                            ($level->id != 1 && $level->id != 4 && $level->id != 5 && $level->id != 6 && $level->id != 7 && substr($this->session->userdata('levels')->users_custom_levels, 1, 1) == '0')) {
                                                        continue;
                                                    }
                                                    $selected = '';
                                                    if ($level->id == set_value('access_level')) {
                                                        $selected = 'selected';
                                                    }
                                                    ?>
                                                    <option <?= $selected ?> value="<?= $level->id ?>"><?= $level->name ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="c-red"><?php echo form_error('access_level'); ?></span>
                                        </div>
                                    </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label"><?= $this->lang->line("bank") ?> * </label>
                                            <div class="col-sm-7">
                                                <select class="form-control" name="bank" id="bank">
                                                    <option value><?= $this->lang->line('select') ?></option>
                                                    <?php
                                                    foreach ($banks as $bank) {
                                                        $selected = '';
                                                        if ($bank->id == $this->session->userdata("bank_id") || $bank->id == set_value('bank')) {
                                                            $selected = 'selected';
                                                        }
                                                        ?>
                                                        <option <?= $selected ?> value="<?= $bank->id ?>"><?= $bank->name ?></option>
                                                    <?php } ?>
                                                </select>
                                                <span class="c-red"><?php echo form_error('bank'); ?></span>
                                            </div>
                                        </div>
                                   


                                    <!--                                                                        <div class="form-group">
                                                                                                                <label class="col-sm-4 control-label"><?= $this->lang->line("branch_code") ?> * </label>
                                                                        
                                                                                                                <div class="col-sm-7">
                                                                                                                    <input type="text" class="form-control" name="branch_code" id="branch_code" value="<?= set_value('branch_code') ?>" />
                                                                                                                    <span class="c-red"><?php echo form_error('branch_code'); ?></span>
                                                                                                                </div>
                                                                                                            </div>-->
                                    <div class='form-group'>
                                        <label class="col-sm-4 control-label"><?= $this->lang->line("branch_code") ?> * </label>
                                        <div class='col-sm-7'>
                                            <?php
                                            $arr = array();
                                            if ($this->input->post() && is_array($this->input->post('branch_code'))) {
                                                foreach ($this->input->post('branch_code') as $value) {
                                                    $arr[] = $value;
                                                }
                                            }
                                            ?>
                                            <input type='text' id='example_email' class='form-control' value='<?= json_encode($arr) ?>' />
                                            <span class="c-red"><?php echo form_error('branch_code[]'); ?></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><?= $this->lang->line("force_reset") ?> </label>

                                        <div class="col-sm-7">
                                            <input <?= $this->input->post("force_reset") ? "checked" : "" ?> type="checkbox" class="form-control" name="force_reset" id="force_reset" value="1"/>
                                            <span class="c-red"><?php echo form_error('force_reset'); ?></span>
                                        </div>
                                    </div>
                                    <br/>
                                    <br/>
                                    <div class="text-center">
                                        <?php echo form_submit(array('id' => 'btn_submit', 'value' => $this->lang->line('lang_save'), 'class' => 'btn btn-primary')); ?>
                                        <span class="btn btn-default btn-mini"><a style="text-decoration: none;" href="<?= base_url() ?>admin/User/get_system_users"><?= $this->lang->line('lang_cancel') ?></a></span>
                                    </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/js/multi_text_inputs.js" type="text/javascript"></script>

        <script>
            $(function () {
                $('#current_emails').text($('#example_email').val());
                $('#example_email').multiple_emails();
                $('#example_email').change(function () {
                    $('#current_emails').text($(this).val());
                });

                $("#bank").change(function () {
                    $("#branch_code").autocomplete({disabled: true});
                    if ($("#bank").val() == '') {
                        return;
                    }
                    $.ajax({
                        url: '<?= base_url() ?>admin/Banks/get_bank_branches/' + $("#bank option:selected").text(),
                        type: "POST",
                        dataType: "JSON",
                        data: {"<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>"},
                        success: function (result) {
                            if (result.length > 2) {
                                $("#branch_code").autocomplete({
                                    source: result,
                                    disabled: false
                                });
                            }
                        }
                    });
                });

            });
        </script>
        <script>
            $(document).ready(function () {
//                $(".select-tags").select2({
//                    tags: '',
//                    tokenSeparators: [","],
//                    multiple: true
//                });

//            BEGIN PASSWORD STRENGTH
<?php if (!empty($password_management) && $password_management->complex) { ?>
                    $('#password').keyup(function () {
                        checkStrength($('#password').val());
                    });
                    function checkStrength(password) {

                        var strength = 0;
    <?php
    if (!empty($password_management) && $password_management->min_length) {
        ?>
                            var min_length = "<?= $password_management->min_length ?>";
                            if (password.length < min_length) {
                                progress_bar(25, "progress-bar-danger", "<?= $this->lang->line("too_short") ?>");
                            }
                            else {
                                strength += 1;
                            }
        <?php
    } else {
        ?>
                            strength += 1;
        <?php
    }
    ?>
                        // If password contains both lower and uppercase characters, increase strength value.
                        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
                            strength += 1;
                        // If it has numbers and characters, increase strength value.
                        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
                            strength += 1;
                        // If it has one special character, increase strength value.
                        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
                            strength += 1;
                        // If it has two special characters, increase strength value.
                        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
                            strength += 1;
                        // Calculated strength value, we can return messages
                        // If value is less than 2
                        console.log(strength);
                        if (strength < 2) {
                            progress_bar(50, "progress-bar-danger", "<?= $this->lang->line("weak") ?>");
                        } else if (strength <= 3) {
                            progress_bar(75, "progress-bar-warning", "<?= $this->lang->line("medium") ?>");
                        } else {
                            progress_bar(100, "progress-bar-success", "<?= $this->lang->line("strong") ?>");
                        }
                    }


                    function progress_bar(per, c, text) {
                        $("#result").css("width", per + "%");
                        $("#result").attr("aria-valuenow", per);
                        $("#result").removeClass("progress-bar-danger");
                        $("#result").removeClass("progress-bar-warning");
                        $("#result").removeClass("progress-bar-success");
                        $("#result").addClass(c);
                        $("#result").html(text);
                        $(".progress").show();
                        if (per == 100) {
                            $("#submit").removeAttr('disabled');
                        }
                        else {
                            $("#submit").attr("disabled", "disabled");
                        }
                    }
<?php } ?>
//            END PASSWORD STRENGTH

                $("#first_name,#last_name").focusout(function () {
                    if ($("#display_name").val() == '' && $("#first_name").val() != '' && $("#last_name").val() != '') {
                        $("#display_name").val($("#first_name").val() + ' ' + $("#last_name").val());
                    }
                });

                //disable special characters
                $("#btn_submit").click(function (e) {
                    e.preventDefault();
                    var first_name = $("#first_name").val();
                    var last_name = $("#last_name").val();
                    var display_name = $("#display_name").val();

                    first_name = first_name.split(' ').join('');
                    last_name = last_name.split(' ').join('');
                    display_name = display_name.split(' ').join('');

                    var regex = new RegExp("^[a-zA-Z0-9]+$");
                    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                    console.log(regex.test(e.which));
                    console.log(regex.test(e.charCode));
                    var error = 0;
                    $("input[name='branch_code[]']").each(function () {
                        if (!regex.test($(this).val())) {
                            alert("<?= $this->lang->line("no_special_chars") ?>");
                            error = 1;
                            return;
                        }
                    });
                    if (error) {
                        return;
                    }
                    if (regex.test(first_name) && regex.test(last_name) && regex.test(display_name)) {
                        $("#user_form").submit();
                    } else {
                        alert("<?= $this->lang->line("no_special_chars") ?>");
                    }
                });
                /////////////////////////////

            });
        </script>
    </body>
</html>