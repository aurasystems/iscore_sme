<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <link href="<?=base_url()?>assets/plugins/export/jquery.dataTables.min.css" rel="stylesheet">
        <link href="<?=base_url()?>assets/plugins/export/Buttons-1.5.6/css/buttons.dataTables.min.css" rel="stylesheet">
        <style>
        .dataTables_filter{
            float: right !important;
        }
        </style>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="header">
                        <div class="breadcrumb-wrapper editWrapper">
                            <ol class="breadcrumb">
                                <li><a href="<?= base_url() ?>admin/Dashboard"><?= $this->lang->line('lang_dashboard') ?></a></li>
                                <li class="active"><?= $this->lang->line('lang_view_users') ?></li>
                            </ol>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><i class="fa fa-users"></i> <?= $this->lang->line("lang_view_users") ?></h2>
                        </div>

                        <div class="panel-content row">
                            <div class="col-lg-12 portlets">
                                <?php if ($status == "success") { ?>
                                    <div class="alert alert-block alert-success fade in">
                                        <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                        <p></p><h4><i class="fa fa-check"></i> <?= $this->lang->line('lang_success') ?></h4> <?= $this->lang->line('atumsl_alertsettingssuccess') ?><p></p>
                                    </div>
                                <?php } ?>
                                <?php
                                if (substr($this->session->userdata('levels')->users_super_admin, 1, 1) != '0' ||
                                        substr($this->session->userdata('levels')->users_bank_managers, 1, 1) != '0' ||
                                        substr($this->session->userdata('levels')->users_can_approve, 1, 1) != '0' ||
                                        substr($this->session->userdata('levels')->users_normal, 1, 1) != '0' ||
                                        substr($this->session->userdata('levels')->users_custom_levels, 1, 1) != '0') {
                                    ?>
                                    <div class="text-center">
                                        <a class="btn btn-primary btn-square" href="<?= base_url() ?>admin/User/add_user_view"><?= $this->lang->line('lang_add_user') ?></a>
                                    </div>
                                <?php } ?>
                                <div class="panel">
                                    <div class="panel-content">
                                        <div class="filter-left">
                                            <table class="table reports-table">
                                                <thead>
                                                    <tr>
                                                        <th><?= $this->lang->line('lang_first_name') ?></th>
                                                        <th><?= $this->lang->line('lang_last_name') ?></th>
                                                        <th><?= $this->lang->line('lang_display_name') ?></th>
                                                        <th><?= $this->lang->line('lang_email_address') ?></th>
                                                        <th><?= $this->lang->line('access_level') ?></th>
                                                        <?php if ($this->session->userdata("access_level") == 1) {
                                                            ?>
                                                            <th><?= $this->lang->line('bank') ?></th>
                                                        <?php } ?>
                                                        <th><?= $this->lang->line('create_date') ?></th>
                                                        <th><?= $this->lang->line('lang_actions') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if ($users_data) {
                                                        foreach ($users_data as $user) {
                                                            ?>
                                                            <tr style="<?= !$user->active ? 'background-color: #ccc;' : '' ?>">
                                                                <td><?= $user->first_name ?></td>
                                                                <td><?= $user->last_name ?></td>
                                                                <td><?= $user->display_name ?></td>
                                                                <td><?= $user->email ?></td>
                                                                <td><?= $user->access_level_name ?></td>
                                                                <?php if ($this->session->userdata("access_level") == 1) {
                                                                    ?>
                                                                    <td><?= $user->bank_name ?></td>
                                                                <?php } ?>
                                                                <td><?= $user->date_created ?></td>
                                                                <td>
                                                                    <?php
                                                                    if (($user->access_level == 1 && substr($this->session->userdata('levels')->users_super_admin, 2, 1) == 1) ||
                                                                            ($user->access_level == 4 && substr($this->session->userdata('levels')->users_bank_managers, 2, 1) == 1) ||
                                                                            ($user->access_level == 5 && substr($this->session->userdata('levels')->users_can_approve, 2, 1) == 1) ||
                                                                            ($user->access_level == 6 && substr($this->session->userdata('levels')->users_normal, 2, 1) == 1) ||
                                                                            ($user->access_level != 1 && $user->access_level != 4 && $user->access_level != 5 && $user->access_level != 6 && substr($this->session->userdata('levels')->users_custom_levels, 2, 1) == 1)) {
                                                                        ?>
                                                                        <a title="<?= $this->lang->line('edit') ?>" href="<?= base_url() ?>admin/User/edit_user_view/<?= $user->id ?>" class="edit btn btn-sm btn-default"><i class="icon-note"></i></a>
                                                                        <a title="<?= $user->active ? $this->lang->line('deactivate') : $this->lang->line('activate') ?>" href="<?= base_url() ?>admin/User/change_user_status/<?= $user->id ?>/<?= $user->active ? 0 : 1 ?>" class="edit btn btn-sm <?= $user->active ? 'btn-danger' : 'btn-success' ?>"><i class="fa fa-refresh"></i></a>
                                                                        <a title="<?= $this->lang->line('reset_password') ?>" href="<?= base_url() ?>admin/User/reset_pass/<?= $user->id ?>" class="edit btn btn-sm btn-warning"><i class="icon-lock"></i></a>
                                                                    <?php } ?>
                                                                    <?php
                                                                    if (($user->access_level == 1 && substr($this->session->userdata('levels')->users_super_admin, 3, 1) == 1) ||
                                                                            ($user->access_level == 4 && substr($this->session->userdata('levels')->users_bank_managers, 3, 1) == 1) ||
                                                                            ($user->access_level == 5 && substr($this->session->userdata('levels')->users_can_approve, 3, 1) == 1) ||
                                                                            ($user->access_level == 6 && substr($this->session->userdata('levels')->users_normal, 3, 1) == 1) ||
                                                                            ($user->access_level != 1 && $user->access_level != 4 && $user->access_level != 5 && $user->access_level != 6 && substr($this->session->userdata('levels')->users_custom_levels, 3, 1) == 1)) {
                                                                        ?>
                                                                        <a title="<?= $this->lang->line('delete') ?>" href="<?= base_url() ?>admin/User/delete_user_view/<?= $user->id ?>" class="delete btn btn-sm btn-danger"><i class="icons-office-52"></i></a>
                                                                        <?php } ?>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <!-- <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script> -->
        
        <script src="<?=base_url()?>assets/plugins/export/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/export/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/export/Buttons-1.5.6/js/buttons.flash.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/export/jszip.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/export/pdfmake.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/export/vfs_fonts.js"></script>
        <script src="<?=base_url()?>assets/plugins/export/Buttons-1.5.6/js/buttons.html5.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/export/Buttons-1.5.6/js/buttons.print.min.js"></script>
        <script>
        $(document).ready(function() {
      
       $('.reports-table').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        pageLength : 10
     } );
    });
       </script> 
    </body>
</html>