<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= MY_APP_NAME ?> | <?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="" name="description" />
        <meta content="themes-lab" name="author" />
        <link rel="shortcut icon" href="assets/img/favicon.png">
        <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/ui.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/custom.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/plugins/icheck/skins/all.css" rel="stylesheet"/>
        <link href="<?= base_url() ?>assets/plugins/bootstrap-loading/lada.min.css" rel="stylesheet">
    </head>
    <body class="account separate-inputs boxed" data-page="signup">
        <!-- BEGIN LOGIN BOX -->
        <div class="container" id="login-block">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-md-offset-3">
                    <div class="account-wall">
                        <i class="user-img icons-faces-users-03"></i>
                        <div class="clearfix">
                            <p class="m-t-20 editThisP">
                                <span>
                                    <?php if(!empty($msg)){
                                        echo $msg;
                                    }
                                    else{
                                        echo $this->lang->line('lang_thank_msg');
                                    }
                                    ?>
                                </span>
                                <a href="<?= base_url() ?>admin/Login"><?= $this->lang->line('lang_go_to_login') ?></a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END LOCKSCREEN BOX -->
        <script src="<?= base_url() ?>assets/plugins/jquery/jquery-1.11.1.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/gsap/main-gsap.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/icheck/icheck.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/backstretch/backstretch.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-loading/lada.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/jquery-validation/additional-methods.min.js"></script>
        <script src="<?= base_url() ?>assets/js/plugins.js"></script>
        <?php $this->load->view('admin/private/scripts/login_js'); ?>
    </body>
</html>