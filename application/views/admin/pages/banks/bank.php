<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><i class="fa fa-university"></i> <?= $this->lang->line("banks") ?></h2>
                                </div>
                                <div class="panel-content">
                                    <?php if ($this->session->flashdata("success")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-success"><?= $this->session->flashdata("success") ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($this->session->flashdata("error")) { ?>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="alert alert-danger"><?= $this->session->flashdata("error") ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <form enctype="multipart/form-data" id="form" method="POST" action="" role="form" class="form-horizontal form-validation">
                                               <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />

                                                <div class="form-group">
                                                    <label class="<?= $label_att ?>"><?= $this->lang->line("bank_logo") ?></label>
                                                    <div class="col-sm-9">
                                                        <?php if (isset($bank) && $bank->logo != '') { ?>
                                                            <img src="<?= base_url() ?>uploads/banks_logos/<?= $bank->logo ?>" width="100" class="img-responsive thumbnail" />
                                                        <?php } ?>
                                                        <input type="file" name="userfile" class="form-control" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="<?= $label_att ?>"><?= $this->lang->line("bank_name") ?> * </label>
                                                    <div class="col-sm-9">
                                                        <?php
                                                        $bank_name = '';
                                                        if ($this->input->post()) {
                                                            $bank_name = set_value('bank_name');
                                                        } else if (isset($bank)) {
                                                            $bank_name = $bank->name;
                                                        }
                                                        ?>
                                                        <input type="text" name="bank_name" value="<?= $bank_name ?>" class="form-control" placeholder="<?= $this->lang->line('bank_name') ?>"/>
                                                        <span class="c-red"><?= form_error('bank_name') ?></span>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="<?= $label_att ?>"><?= $this->lang->line("bank_code") ?> * </label>
                                                    <div class="col-sm-9">
                                                        <?php
                                                        $bank_code = '';
                                                        if ($this->input->post()) {
                                                            $bank_code = set_value('bank_code');
                                                        } else if (isset($bank)) {
                                                            $bank_code = $bank->code;
                                                        }
                                                        ?>
                                                        <input type="text" name="bank_code" value="<?= $bank_code ?>" class="form-control" placeholder="<?= $this->lang->line('bank_code') ?>"/>
                                                        <span class="c-red"><?= form_error('bank_code') ?></span>
                                                    </div>
                                                </div>
                                               
                                                <div class="form-group">
                                                    <label class="<?= $label_att ?>"><?= $this->lang->line("non_subscriber") ?> * </label>
                                                    <div class="col-sm-9 m-t-10">
                                                        <?php
                                                        $checked = '';
                                                        if (($this->input->post()&&$this->input->post('non_subscriber'))||
                                                                (isset($bank)&&$bank->non_subscriber==1)) {
                                                            $checked = 'checked';
                                                        } 
                                                        ?>
                                                        <input <?= $checked ?> type="checkbox" value="1" name="non_subscriber" data-checkbox="icheckbox_square-blue">
                                                        
                                                    </div>
                                                </div>
                                               
                                                <hr/>
                                                <fieldset>
                                                    <legend><?= $this->lang->line('appearance') ?></legend>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label"><?= $this->lang->line("sidebar_bg_color") ?></label>
                                                        <div class="col-sm-7">
                                                            <?php
                                                            $sidebar_bg_color = '#2b2e33';
                                                            if ($this->input->post()) {
                                                                $sidebar_bg_color = set_value('sidebar_bg_color');
                                                            } else if (isset($bank)) {
                                                                $sidebar_bg_color = $bank->sidebar_bg_color;
                                                            }
                                                            ?>
                                                            <input type="color" name="sidebar_bg_color" value="<?= $sidebar_bg_color ?>" class="m-t-20" placeholder="<?= $this->lang->line('sidebar_bg_color') ?>"/>
                                                            <span class="c-red"><?= form_error('sidebar_bg_color') ?></span>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label"><?= $this->lang->line("topbar_bg_color") ?></label>
                                                        <div class="col-sm-7">
                                                            <?php
                                                            $sidebar_bg_color = '#ffffff';
                                                            if ($this->input->post()) {
                                                                $sidebar_bg_color = set_value('topbar_bg_color');
                                                            } else if (isset($bank)) {
                                                                $sidebar_bg_color = $bank->topbar_bg_color;
                                                            }
                                                            ?>
                                                            <input type="color" name="topbar_bg_color" value="<?= $sidebar_bg_color ?>" class="m-t-20" placeholder="<?= $this->lang->line('topbar_bg_color') ?>"/>
                                                            <span class="c-red"><?= form_error('topbar_bg_color') ?></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label"><?= $this->lang->line("sidebar_links_font_color") ?></label>
                                                        <div class="col-sm-7">
                                                            <?php
                                                            $sidebar_bg_color = '#ffffff';
                                                            if ($this->input->post()) {
                                                                $sidebar_bg_color = set_value('sidebar_links_font_color');
                                                            } else if (isset($bank)) {
                                                                $sidebar_bg_color = $bank->sidebar_links_font_color;
                                                            }
                                                            ?>
                                                            <input type="color" name="sidebar_links_font_color" value="<?= $sidebar_bg_color ?>" class="m-t-20" placeholder="<?= $this->lang->line('sidebar_links_font_color') ?>"/>
                                                            <span class="c-red"><?= form_error('sidebar_links_font_color') ?></span>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label"><?= $this->lang->line("topbar_links_font_color") ?></label>
                                                        <div class="col-sm-7">
                                                            <?php
                                                            $sidebar_bg_color = '#000';
                                                            if ($this->input->post()) {
                                                                $sidebar_bg_color = set_value('topbar_links_font_color');
                                                            } else if (isset($bank)) {
                                                                $sidebar_bg_color = $bank->topbar_links_font_color;
                                                            }
                                                            ?>
                                                            <input type="color" name="topbar_links_font_color" value="<?= $sidebar_bg_color ?>" class="m-t-20" placeholder="<?= $this->lang->line('topbar_links_font_color') ?>"/>
                                                            <span class="c-red"><?= form_error('topbar_links_font_color') ?></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label"><?= $this->lang->line("sidebar_links_highlight") ?></label>
                                                        <div class="col-sm-7">
                                                            <?php
                                                            $sidebar_bg_color = '#319db5';
                                                            if ($this->input->post()) {
                                                                $sidebar_bg_color = set_value('sidebar_links_highlight');
                                                            } else if (isset($bank)) {
                                                                $sidebar_bg_color = $bank->sidebar_links_highlight;
                                                            }
                                                            ?>
                                                            <input type="color" name="sidebar_links_highlight" value="<?= $sidebar_bg_color ?>" class="m-t-20" placeholder="<?= $this->lang->line('sidebar_links_highlight') ?>"/>
                                                            <span class="c-red"><?= form_error('sidebar_links_highlight') ?></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label"><?= $this->lang->line("pages_header_bg") ?></label>
                                                        <div class="col-sm-7">
                                                            <?php
                                                            $sidebar_bg_color = '#319db5';
                                                            if ($this->input->post()) {
                                                                $sidebar_bg_color = set_value('pages_header_bg');
                                                            } else if (isset($bank)) {
                                                                $sidebar_bg_color = $bank->pages_header_bg;
                                                            }
                                                            ?>
                                                            <input type="color" name="pages_header_bg" value="<?= $sidebar_bg_color ?>" class="m-t-20" placeholder="<?= $this->lang->line('pages_header_bg') ?>"/>
                                                            <span class="c-red"><?= form_error('pages_header_bg') ?></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label"><?= $this->lang->line("pages_header_font_color") ?></label>
                                                        <div class="col-sm-7">
                                                            <?php
                                                            $sidebar_bg_color = '#ffffff';
                                                            if ($this->input->post()) {
                                                                $sidebar_bg_color = set_value('pages_header_font_color');
                                                            } else if (isset($bank)) {
                                                                $sidebar_bg_color = $bank->pages_header_font_color;
                                                            }
                                                            ?>
                                                            <input type="color" name="pages_header_font_color" value="<?= $sidebar_bg_color ?>" class="m-t-20" placeholder="<?= $this->lang->line('pages_header_font_color') ?>"/>
                                                            <span class="c-red"><?= form_error('pages_header_font_color') ?></span>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <br/>
                                                <div class="row">
                                                    <div class="col-sm-7 col-sm-offset-5">
                                                        <div class="pull-right">
                                                            <button type="submit" class="btn btn-embossed btn-primary m-r-20"><?= $this->lang->line("save") ?></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->

    </body>
</html>