<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="header">
                        <div class="breadcrumb-wrapper editWrapper">
                            <ol class="breadcrumb">
                                <li><a href="<?= base_url() ?>admin/Dashboard"><?= $this->lang->line('lang_dashboard') ?></a></li>
                                <li class="active"><?= $this->lang->line('banks') ?></li>
                            </ol>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><i class="fa fa-university"></i> <?= $this->lang->line("banks") ?></h2>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 portlets">
                                <?php if ($this->session->flashdata('success')) { ?>
                                    <div class="alert alert-block alert-success fade in">
                                        <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                        <p></p><h4><i class="fa fa-check"></i> <?= $this->lang->line('lang_success') ?></h4> <?= $this->session->flashdata('success') ?><p></p>
                                    </div>
                                <?php } ?>
                                <div class="panel">
                                    <div class="panel-content">
                                        <?php if (substr($this->session->userdata('levels')->banks, 1, 1) != '0') { ?>
                                            <div class="text-center">
                                                <button type="button" onclick="location.href = '<?= base_url() ?>admin/Banks/Bank'" class="btn btn-primary btn-square"><?= $this->lang->line('add_bank') ?></button>
                                            </div>
                                        <?php } ?>
                                        <div class="filter-left">
                                            <table class="table table-dynamic table-tools">
                                                <thead>
                                                    <tr>
                                                        <th><?= $this->lang->line('bank_logo') ?></th>
                                                        <th><?= $this->lang->line('bank_name') ?></th>
                                                        <th><?= $this->lang->line('bank_code') ?></th>
                                                        <th><?= $this->lang->line('lang_action') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($banks as $bank) { ?>
                                                        <tr>
                                                            <td><?= $bank->logo != '' ? '<img src="' . base_url() . 'uploads/banks_logos/' . $bank->logo . '" class="img-responsive thumbnail" width="100" />' : '' ?></td>
                                                            <td><?= $bank->name ?></td>
                                                            <td><?= $bank->code ?></td>
                                                            <td>
                                                                <?php if (substr($this->session->userdata('levels')->banks, 2, 1) != '0') { ?>
                                                                    <a title="<?= $this->lang->line('edit') ?>" href="<?= base_url() ?>admin/Banks/bank/<?= $bank->id ?>" class="edit btn btn-sm btn-default"><i class="icon-note"></i></a>
                                                                <?php } ?>
                                                                <?php if (substr($this->session->userdata('levels')->banks, 3, 1) != '0') { ?>
                                                                    <a title="<?= $this->lang->line('delete') ?>" onclick="delete_bank(<?= $bank->id ?>)" class="delete btn btn-sm btn-danger"><i class="fa fa-close"></i></a>
                                                                    <?php } ?>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script>
                                                                    function delete_bank(bank_id) {
                                                                        if (window.confirm("Are you sure ? You can't undo this action")) {
                                                                            location.href = "<?= base_url() ?>admin/Banks/delete_bank/" + bank_id;
                                                                        }
                                                                    }
        </script>
    </body>
</html>