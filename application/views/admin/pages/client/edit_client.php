<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?=base_url()?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="header">
                        <div class="breadcrumb-wrapper editWrapper">
                            <ol class="breadcrumb">
                                <li><a href="<?= base_url() ?>admin/Dashboard"><?=$this->lang->line('lang_dashboard')?></a></li>
                                <li class="active"><?=$this->lang->line('lang_edit_client')?></li>
                            </ol>
                        </div>
                    </div>
                    <div class="box-title editBoxTitle">
                        <h4><i class="fa fa-globe"></i> <?=$this->lang->line('lang_edit_client');?></h4>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php if($status == "success"){?>
                            <div class="alert alert-block alert-success fade in">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                <p></p><h4><i class="fa fa-check"></i> <?=$this->lang->line('lang_success')?></h4> <?=$this->lang->line('atumsl_alertsettingssuccess')?><p></p>
                            </div>
                            <?php }?>
                                <div class="box-body big col-md-6">
                                    <div class="row editErrorMsg editThisMsg">
                                        <?php if($this->session->flashdata('msg')){ ?>
                                        <?php echo $this->session->flashdata('msg'); }?>
                                    </div>
                                    <?php echo form_open('admin/Client/do_update_client/'.$client_id, $attributes); ?>
                                    <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_first_name').':','lang_first_name',$label_att); ?>
                                        <div class="col-sm-7">
                                            <?php
                                            if($client_data)
                                            {
                                                $first_name = $client_data->first_name;
                                            }
                                            else
                                            {
                                                $first_name = '';
                                            }
                                            ?>
                                            <?php echo form_input(array('id' => 'first_name', 'name' => 'first_name', 'class'=>'form-control', 'placeholder'=>'FIRST NAME', 'value'=>$first_name)); ?>
                                            <?php echo form_error('first_name'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_last_name').':','lang_last_name',$label_att); ?>
                                        <div class="col-sm-7">
                                            <?php
                                            if($client_data)
                                            {
                                                $last_name = $client_data->last_name;
                                            }
                                            else
                                            {
                                                $last_name = '';
                                            }
                                            ?>
                                            <?php echo form_input(array('id' => 'last_name', 'name' => 'last_name', 'class'=>'form-control', 'placeholder'=>'LAST NAME', 'value'=>$last_name)); ?>
                                            <?php echo form_error('last_name'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_display_name').':','lang_display_name',$label_att); ?>
                                        <div class="col-sm-7">
                                            <?php
                                            if($client_data)
                                            {
                                                $display_name = $client_data->display_name;
                                            }
                                            else
                                            {
                                                $display_name = '';
                                            }
                                            ?>
                                            <?php echo form_input(array('id' => 'display_name', 'name' => 'display_name', 'class'=>'form-control', 'placeholder'=>'DISPLAY NAME', 'value'=>$display_name)); ?>
                                            <?php echo form_error('display_name'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_phone').':','lang_phone',$label_att); ?>
                                        <div class="col-sm-7">
                                            <?php
                                            if($client_data)
                                            {
                                                $phone = $client_data->phone;
                                            }
                                            else
                                            {
                                                $phone = '';
                                            }
                                            ?>
                                            <?php echo form_input(array('id' => 'phone', 'name' => 'phone', 'class'=>'form-control', 'placeholder'=>'PHONE', 'value'=>$phone)); ?>
                                            <?php echo form_error('phone'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_email_address').':','lang_email_address',$label_att); ?>
                                        <div class="col-sm-7">
                                            <?php
                                            if($client_data)
                                            {
                                                $email = $client_data->email;
                                            }
                                            else
                                            {
                                                $email = '';
                                            }
                                            ?>
                                            <?php echo form_input(array('type' => 'email','id' => 'email', 'name' => 'email', 'class'=>'form-control', 'placeholder'=>'EMAIL', 'value'=>$email)); ?>
                                            <?php echo form_error('email'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_address').':','lang_address',$label_att); ?>
                                        <div class="col-sm-7">
                                            <?php
                                            if($client_data)
                                            {
                                                $address = $client_data->address;
                                            }
                                            else
                                            {
                                                $address = '';
                                            }
                                            ?>
                                            <?php echo form_input(array('id' => 'address', 'name' => 'address', 'class'=>'form-control', 'placeholder'=>'ADDRESS', 'value'=>$address)); ?>
                                            <?php echo form_error('address'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_password').':','lang_password',$label_att); ?>
                                        <div class="col-sm-7">
                                            <?php echo form_input(array('type' => 'password','id' => 'password', 'name' => 'password', 'class'=>'form-control', 'placeholder'=>'Type password if you want to change it.',"autocomplete"=>"off")); ?>
                                            <?php echo form_error('password'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_repeat_password').':','lang_repeat_password',$label_att); ?>
                                        <div class="col-sm-7">
                                            <?php echo form_input(array('type' => 'password','id' => 'password2', 'name' => 'password2', 'class'=>'form-control', 'placeholder'=>'Confirm Password',"autocomplete"=>"off")); ?>
                                            <?php echo form_error('password2'); ?>
                                        </div>
                                    </div>
                                    <?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('lang_save'), 'class'=>'btn btn-success')); ?>
                                    <span class="btn btn-default btn-mini"><a style="text-decoration: none;" href="<?= base_url() ?>admin/Client/get_active_clients"><?=$this->lang->line('lang_cancel')?></a></span>
                                    <?php echo form_close(); ?>
                                </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
    </body>
</html>