<html lang="en">
    <head>
        <?php $this->load->view('admin/private/head'); ?>
        <link href="<?=base_url()?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default">        
        <section>
            <?php $this->load->view('admin/private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('admin/private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="header">
                        <div class="breadcrumb-wrapper editWrapper">
                            <ol class="breadcrumb">
                                <li><a href="<?= base_url() ?>admin/Dashboard"><?=$this->lang->line('lang_dashboard')?></a></li>
                                <li class="active"><?=$this->lang->line('lang_waiting_clients')?></li>
                            </ol>
                        </div>
                    </div>
                    <div class="box-title editBoxTitle">
                        <h4><i class="fa fa-globe"></i> <?=$this->lang->line('lang_waiting_clients');?></h4>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 portlets">
                            <?php if($status == "success"){?>
                            <div class="alert alert-block alert-success fade in">
                                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                <p></p><h4><i class="fa fa-check"></i> <?=$this->lang->line('lang_success')?></h4> <?=$this->lang->line('atumsl_alertsettingssuccess')?><p></p>
                            </div>
                            <?php }?>
                            <div class="panel">
                                <div class="panel-content">
                                    <div class="filter-left">
                                        <table class="table table-dynamic table-tools">
                                            <thead>
                                              <tr>
                                                <th><?=$this->lang->line('lang_first')?></th>
                                                <th><?=$this->lang->line('lang_last')?></th>
                                                <th><?=$this->lang->line('lang_display')?></th>
                                                <th><?=$this->lang->line('lang_phone')?></th>
                                                <th><?=$this->lang->line('lang_email_address')?></th>
                                                <th><?=$this->lang->line('lang_address')?></th>
                                                <th><?=$this->lang->line('lang_action')?></th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if($waiting_clients){
                                                    foreach($waiting_clients as $client){
                                                ?>
                                                <tr>
                                                    <td><?= $client->first_name ?></td>
                                                    <td><?= $client->last_name ?></td>
                                                    <td><?= $client->display_name ?></td>
                                                    <td><?= $client->phone ?></td>
                                                    <td><?= $client->email ?></td>
                                                    <td><?= $client->address ?></td>
                                                    <td>
                                                        <a href="<?= base_url() ?>admin/Client/active_waiting_client/<?= $client->id ?>" class="delete btn btn-sm btn-danger"><?=$this->lang->line('lang_active')?></a>
                                                    </td>
                                                </tr>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('admin/private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('admin/private/quick_sidebar'); ?>
        <?php $this->load->view('admin/private/search'); ?>
        <?php $this->load->view('admin/private/preloader'); ?>
        <?php $this->load->view('admin/private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
    </body>
</html>