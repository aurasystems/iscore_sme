<a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a> 
<script src="<?= base_url() ?>assets/plugins/jquery/jquery-1.11.1.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/jquery-ui/jquery-ui-1.11.2.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/gsap/main-gsap.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/jquery-cookies/jquery.cookies.min.js"></script> <!-- Jquery Cookies, for theme -->
<script src="<?= base_url() ?>assets/plugins/jquery-block-ui/jquery.blockUI.min.js"></script> <!-- simulate synchronous behavior when using AJAX -->
<script src="<?= base_url() ?>assets/plugins/translate/jqueryTranslator.min.js"></script> <!-- Translate Plugin with JSON data -->
<script src="<?= base_url() ?>assets/plugins/bootbox/bootbox.min.js"></script> <!-- Modal with Validation -->
<script src="<?= base_url() ?>assets/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script> <!-- Custom Scrollbar sidebar -->
<script src="<?= base_url() ?>assets/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script> <!-- Show Dropdown on Mouseover -->
<script src="<?= base_url() ?>assets/plugins/charts-sparkline/sparkline.min.js"></script> <!-- Charts Sparkline -->
<script src="<?= base_url() ?>assets/plugins/retina/retina.min.js"></script> <!-- Retina Display -->
<script src="<?= base_url() ?>assets/plugins/select2/select2.min.js"></script> <!-- Select Inputs -->
<script src="<?= base_url() ?>assets/plugins/icheck/icheck.min.js"></script> <!-- Checkbox & Radio Inputs -->
<script src="<?= base_url() ?>assets/plugins/backstretch/backstretch.min.js"></script> <!-- Background Image -->
<script src="<?= base_url() ?>assets/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script> <!-- Animated Progress Bar -->
<script src="<?= base_url() ?>assets/plugins/charts-chartjs/Chart.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/dropzone/dropzone.min.js"></script>  <!-- Upload Image & File in dropzone -->
<script src="<?= base_url() ?>assets/js/builder.js"></script> <!-- Theme Builder -->
<script src="<?= base_url() ?>assets/js/sidebar_hover.js"></script> <!-- Sidebar on Hover -->
<script src="<?= base_url() ?>assets/js/widgets/notes.js"></script> <!-- Notes Widget -->
<script src="<?= base_url() ?>assets/js/quickview.js"></script> <!-- Chat Script -->
<script src="<?= base_url() ?>assets/js/pages/search.js"></script> <!-- Search Script -->
<!-- Main Plugin Initialization Script -->
<?php $this->load->view('admin/private/plugins_js'); ?>
<?php $this->load->view('admin/private/application_script'); ?><!-- Main Application Script -->
<script>
$(document).ready(function(){
    $(".sidebar-inner.mCustomScrollbar._mCS_1").css("padding-top",$(".bank_title").height()+5);
});
</script>

