<!-- BEGIN SIDEBAR -->
<div class="sidebar" style="background: <?= $this->session->userdata('sidebar_bg_color') ?>">
    <div class="logopanel" style="background: <?= $this->session->userdata('sidebar_bg_color') ?>">
        <h1>
            <a style="background: none;color: #fff" href="<?= base_url() ?>admin/Cases_two">
                <?php
                if ($this->session->userdata('sys_settings')) {
                    echo '<img class="img-responsive sys_logo" src="' . base_url() . 'uploads/banks_logos/' . $this->session->userdata('sys_logo') . '" />  ' . '<br><span style="width:100%;word-wrap:break-word;" class="bank_title">' . $this->session->userdata('sys_name') . '</span>';
                } else {
                    echo '';
                }
                ?>
            </a>
        </h1>
    </div>
    <div class="sidebar-inner" style="padding-top:55px;background: <?= $this->session->userdata('sidebar_bg_color') ?>">
        <!--        <div class="sidebar-top">
                    <form action="search-result.html" method="post" class="searchform" id="search-results">
                        <input type="text" class="form-control" name="keyword" placeholder="Search...">
                    </form>
                </div>-->
        <ul class="nav nav-sidebar">
            <?php if (substr($this->session->userdata('levels')->cases, 0, 1) != '0') { ?>
                <li class="nav <?= $this->session->userdata('active') == 'cases' ? 'active nav-active' : '' ?>"><a href="<?= base_url() ?>admin/Cases_two"><i class="fa fa-files-o"></i><span data-translate="cases"><?= $this->lang->line('cases') ?></span></a></li>
            <?php } ?>
            <?php
            if (substr($this->session->userdata('levels')->gradation_rep, 1, 1) != '0' ||
                    substr($this->session->userdata('levels')->gradation_rep, 2, 1) != '0') {
                ?>
                <li class="nav <?= $this->session->userdata('active') == 'g_report' ? 'active nav-active' : '' ?>"><a href="<?= base_url() ?>admin/Cases_two/gradation_report"><i class="fa fa-file-pdf-o"></i><span data-translate="gradation_report"><?= $this->lang->line('gradation_report') ?></span></a></li>
            <?php } ?>

            <?php if (substr($this->session->userdata('levels')->banks, 0, 1) != '0') { ?>
                <li class="nav <?= $this->session->userdata('active') == 'banks' ? 'nav-active active' : '' ?>"><a href="<?= base_url() ?>admin/Banks"><i class="fa fa-university"></i><span data-translate="banks"><?= $this->lang->line('banks') ?></span></a></li>
            <?php } ?>
            <!-- For Users -->
            <?php
            if (substr($this->session->userdata('levels')->users_super_admin, 0, 1) != '0' ||
                    substr($this->session->userdata('levels')->users_bank_managers, 0, 1) != '0' ||
                    substr($this->session->userdata('levels')->users_can_approve, 0, 1) != '0' ||
                    substr($this->session->userdata('levels')->users_normal, 0, 1) != '0' ||
                    substr($this->session->userdata('levels')->users_custom_levels, 0, 1) != '0') {
                ?>
                <li class="nav <?= $this->session->userdata('active') == 'users' ? 'active nav-active' : '' ?>"><a href="<?= base_url() ?>admin/User/get_system_users"><i class="fa fa-files-o"></i><span data-translate="System Users"><?= $this->lang->line('lang_system_users') ?></span></a></li>
            <?php } ?>
            <?php if (substr($this->session->userdata('levels')->reports, 0, 1) != '0') { ?>
                <li class="nav-parent <?= $this->session->userdata('active') == 'reports' ? 'active' : '' ?>">
                    <a href="#"><i class="fa fa-files-o"></i><span data-translate="Reports"><?= $this->lang->line('reports') ?></span> <span class="fa arrow <?= $this->session->userdata('active') == 'reports' ? 'active' : '' ?>"></span></a>
                    <ul class="children collapse">
                        <li class="<?= $title == $this->lang->line("cases_report") ? 'active' : '' ?>"><a href="<?= base_url() ?>admin/Reports/cases_report"><?= $this->lang->line('cases_report') ?></a></li>
                    </ul>
					<?php if(compare_level("super_admin")){ ?>
                    <ul class="children collapse">
                        <li class="<?= $title == $this->lang->line("banks_report") ? 'active' : '' ?>"><a href="<?= base_url() ?>admin/Reports/banks_report"><?= $this->lang->line('banks_report') ?></a></li>
                    </ul>
                    
                    <ul class="children collapse">
                        <li class="<?= $title == $this->lang->line("audit_trail") ? 'active' : '' ?>"><a href="<?= base_url() ?>admin/Reports/audit_trail"><?= $this->lang->line('audit_trail') ?></a></li>
                    </ul>
                    <?php } ?>
                </li>
            <?php } ?>

            <!-- For Settings -->
            <?php
            if (substr($this->session->userdata('levels')->settings, 0, 1) != '0' ||
                    substr($this->session->userdata('levels')->accleveltype, 0, 1) != '0') {
                ?>
                <li class="nav-parent <?= $this->session->userdata('active') == 'settings' ? 'active' : '' ?>" >
                    <a href="#"><i class="fa fa-cog"></i><span data-translate="settings"><?= $this->lang->line('lang_settings') ?></span> <span class="fa arrow <?= $this->session->userdata('active') == 'settings' ? 'active' : '' ?>"></span></a>
                    <ul class="children collapse">
                        <?php if (substr($this->session->userdata('levels')->settings, 0, 1) != '0') { ?>
                            <li class="<?= $title == $this->lang->line("password_management") ? 'active' : '' ?>"><a href="<?= base_url() ?>admin/Settings/password_management"><?= $this->lang->line('password_management') ?></a></li>

                                                    <!--<li><a href="<?= base_url() ?>admin/Back_settings"><?= $this->lang->line('lang_back_settings') ?></a></li>-->

                            <li class="<?= $title == $this->lang->line("audit_trail_settings") ? 'active' : '' ?>"><a href="<?= base_url() ?>admin/Settings/audit_trail"><?= $this->lang->line('audit_trail_settings') ?></a></li>
                            <!--<li><a href="<?//= base_url() ?>admin/Front_settings"><?//= $this->lang->line('lang_front_settings') ?></a></li>-->
                        <?php } ?>
                        <?php if (substr($this->session->userdata('levels')->accleveltype, 0, 1) != '0') { ?>
                            <li class="<?= $title == $this->lang->line("access_levels") ? 'active' : '' ?>"><a href="<?= base_url() ?>admin/Accesslevels"><?= $this->lang->line('permissions') ?></a></li>
                        <?php } ?>
                        <?php if (substr($this->session->userdata('levels')->settings, 0, 1) != '0') { ?>
                            <li class="<?= $title == 'Email Settings' ? 'active' : '' ?>"><a href="<?= base_url() ?>admin/Email_settings"><?= $this->lang->line('lang_email_settings') ?></a></li>
                        <?php } ?>
                        <?php if (substr($this->session->userdata('levels')->settings, 0, 1) != '0') { ?>
                            <li class="<?= $title == 'input_settings' || isset($active_input_settings) ? 'active' : '' ?>"><a href="<?= base_url() ?>admin/Settings/input_settings"><?= $this->lang->line('input_settings') ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
<!-- END SIDEBAR -->