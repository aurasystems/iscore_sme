 <!-- BEGIN SEARCH -->
    <div id="morphsearch" class="morphsearch">
        <form class="morphsearch-form" method="POST" action="<?=base_url()?>admin/Search/searchResult">
            <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" />
        <input class="morphsearch-input" type="search" name="text" placeholder="Search..."/>
        <button class="morphsearch-submit" type="submit">Search</button>
      </form>
     
      <!-- /morphsearch-content -->
      <span class="morphsearch-close"></span>
    </div>