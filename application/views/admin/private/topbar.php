<!-- BEGIN TOPBAR -->
<div class="topbar" style="background: <?= $this->session->userdata('topbar_bg_color') ?>">
    <div class="header-left">
        <div class="topnav">
            <a class="menutoggle" href="#" data-toggle="sidebar-collapsed"><span class="menu__handle"><span><?= $this->lang->line('lang_menu') ?></span></span></a>
        </div>
    </div>
    <div class="header-right">
        <ul class="header-menu nav navbar-nav">
            <!-- BEGIN LANGUAGE DROPDOWN -->
            <?php if(compare_level('super_admin')){ ?>
            <li class="dropdown" id="language-header">
                <button type="button" onclick ='location.href="<?= base_url() ?>admin/case_approve"' style="margin-top: 7px" class="btn btn-primary"><?= $this->lang->line('generate_excels_manually') ?></button>
            </li>
            
            <li class="dropdown" id="language-header">
                <button type="button" onclick ='location.href="<?= base_url() ?>admin/upload/index/1"' style="margin-top: 7px" class="btn btn-primary">Upload PDFs</button>
            </li>
            <?php } ?>
            <li class="dropdown" id="language-header">
                <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <i class="icon-globe"></i>
                    <span>Language</span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="<?= base_url() ?>admin/Dashboard/set_language/default" data-lang="en"><img src="<?= base_url() ?>assets/images/flags/usa.png" alt="flag-english"> <span>English</span></a>
                    </li>
                    <li>
                        <a href="<?= base_url() ?>admin/Dashboard/set_language/arabic" data-lang="ar"><img src="<?= base_url() ?>assets/images/flags/Egypt.png" alt="flag-arabic"> <span>عربى</span></a>
                    </li>
                </ul>
            </li>
            <!-- END LANGUAGE DROPDOWN -->


            <!-- BEGIN USER DROPDOWN -->
            <li class="dropdown" id="user-header">
                <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <?php
                    if ($this->session->userdata('user_image')) {
                        $image = $this->session->userdata('user_image');
                    } else {
                        $image = 'default_avatar.png';
                    }
                    ?>
                    <img src="<?= base_url() ?>uploads/users/<?= $image ?>" alt="user image">
                    <span class="username">Hi, <?= $this->session->userdata('user_display_name') ?></span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="<?= base_url() ?>admin/User/edit_user_view/<?= $this->session->userdata('user_id') ?>"><i class="icon-settings"></i><span><?= $this->lang->line('lang_account_settings') ?></span></a>
                    </li>
                    <li>
                        <a href="<?= base_url() ?>admin/User/reset_pass/<?= $this->session->userdata('user_id') ?>"><i class="icon-lock"></i><span><?= $this->lang->line('reset_password') ?></span></a>
                    </li>
                    <li>
                        <a href="<?= base_url() ?>admin/Login/Logout"><i class="icon-logout"></i><span><?= $this->lang->line('lang_logout') ?></span></a>
                    </li>
                    
                </ul>
            </li>
            <!-- END USER DROPDOWN -->

        </ul>
    </div>
    <!-- header-right -->
</div>
<!-- END TOPBAR -->