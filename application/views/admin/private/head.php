<?php header('X-Frame-Options: DENY'); ?>
<?php header('X-Content-Type-Options: nosniff'); ?>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="description" content="admin-themes-lab">
<meta name="author" content="themes-lab">
<title><?= MY_APP_NAME ?> | <?= $title ?></title>
<link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/theme.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/ui.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/css/custom.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/plugins/dropzone/dropzone.min.css" rel="stylesheet">
<script src="<?= base_url() ?>assets/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<style>
    .sidebar .sidebar-inner .nav-sidebar .arrow:before{
        display: block;
    }
    .sidebar .sidebar-inner .nav-sidebar > li.active > a {
        background-color: <?= $this->session->userdata('sidebar_links_highlight') ?>;
    }
    .sidebar .sidebar-inner .nav-sidebar > li > a{
        color: <?= $this->session->userdata('sidebar_links_font_color') ?>;
    }
    .panel-header.bg-primary{
        background-color: <?= $this->session->userdata('pages_header_bg') ?> !important;
    }
    .btn-primary,.btn-blue{
        background-color: <?= $this->session->userdata('pages_header_bg') ?> !important;
        color: <?= $this->session->userdata('pages_header_font_color') ?> !important;;
    }
    .panel-header.bg-primary .panel-title{
        color: <?= $this->session->userdata('pages_header_font_color') ?> !important;;
    }
    .dropdown .username{
        color: <?= $this->session->userdata('topbar_links_font_color') ?> !important;;
    }
    .topbar .header-left .topnav .menu__handle span,
    .topbar .header-left .topnav .menu__handle::before,
    .topbar .header-left .topnav .menu__handle::after{
        background: <?= $this->session->userdata('topbar_links_font_color') ?> !important;
    }
    .theme-sdtl.color-default .sidebar .sidebar-inner .nav-sidebar .nav-parent .children, .theme-sdtl.color-default .sidebar .sidebar-inner .sidebar-footer, .theme-sdtl.color-default .sidebar .sidebar-inner .sidebar-top .searchform input{
        background: <?= $this->session->userdata('sidebar_bg_color') ?> !important;
    }
    .sidebar .sidebar-inner .nav-sidebar > li.nav-parent.active > a:hover{
        background-color: <?= $this->session->userdata('sidebar_links_highlight') ?>;
    }
    .sidebar-collapsed .topbar .header-left .topnav .menutoggle{
        background: <?= $this->session->userdata('topbar_bg_color') ?> !important;
    }
    .topbar .header-right .header-menu #user-header .dropdown-menu li a{
        background: <?= $this->session->userdata('sidebar_bg_color') ?> !important;
        /*color: <?//= $this->session->userdata('topbar_links_font_color') ?> !important;*/
    }

</style>