<?php
$lang['lang_dashboard']         = "لوحة القيادة";
$lang['lang_settings']          = "الاعداد";
$lang['lang_back_settings']     = "الاعداد الخلفى";
$lang['lang_front_settings']    = "الاعداد الامامى";
$lang['lang_email_settings']    = "اعداد البريد الالكترونى";
$lang['lang_success']           = "النجاح";
$lang['lang_system_name']       = "اسم النظام";
$lang['lang_system_logo']       = "شعار النظام";
$lang['lang_front_logo']        = "شعار امامى";
$lang['lang_footer_text']       = "نص الختام";
$lang['lang_save']              = "حفظ";
$lang['lang_cancel']            = "الغاء";
$lang['lang_front_title']       = "العنوان";
$lang['lang_smtp_host']         = "SMTP مضيف";
$lang['lang_host_mail']         = "SMTP بريد الكترونى";
$lang['lang_smtp_port']         = "SMTP منفذ";
$lang['lang_smtp_user']         = "SMTP مستخدم";
$lang['lang_smtp_pass']         = "SMTP مرور";
$lang['lang_basic_info']        = "قاعدة بيانات";
$lang['lang_first_name']        = "الاسم الاول";
$lang['lang_last_name']         = "اسم العائلة";
$lang['lang_display_name']      = "اسم العرض";
$lang['lang_phone']             = "هاتف";
$lang['lang_email_address']     = "بريد الكترونى";
$lang['lang_address']           = "العنوان";
$lang['lang_password']          = "كلمة المرور";
$lang['pass_confirmation']      = "تاكيد الرقم السرى";
$lang['lang_repeat_password']   = "تكرار كلمة المرور";
$lang['lang_home']              = "الصفحة الرئيسية";
$lang['lang_login']             = "الدخول";
$lang['lang_register']          = "تسجيل";
$lang['lang_logout']            = "خروج";
$lang['lang_en']                = "انجليزى";
$lang['lang_ar']                = "عربى";
$lang['lang_edit_profile']      = "تعديل الملف الشخصى";
$lang['lang_client_profile']    = "الملف الشخصى للعميل";
$lang['lang_user_profile']      = "الملف الشخصى للمستخدم";
$lang['lang_facebook']          = "فيس بوك";
$lang['lang_twitter']           = "تويتر";
$lang['lang_google_plus']       = "جوجل بلس";
$lang['lang_pinterest']         = "موقع pinterest";
$lang['lang_flickr']            = "فليكر";
$lang['lang_linkedin']          = "لينكد ان";
$lang['lang_social_media']      = "التواصل الاجتماعى";
$lang['lang_company_info']      = "معلومات الشركة";
$lang['lang_contact_info']      = "معلومات الاتصال";
$lang['lang_phone']             = "هاتف";
$lang['lang_opening_time']      = "وقت مفتوح";
$lang['lang_user_img']          = "صورة المستخدم";
$lang['lang_upload_user_img']   = "تحميل صورة المستخدم";
$lang['lang_contact_us']        = "اتصل بنا";
$lang['lang_name']              = "الاسم";
$lang['lang_pass_reset_link']   = "ارسال اعدة تعيين الرقم السرى";
$lang['lang_forget_password']   = "هل نسيت كلمة المرور?";
$lang['lang_sign_up']           = "جديد هنا؟سجل";
$lang['lang_sign_up_now']       = "سجل";
$lang['lang_sign_in']           = "تسجيل دخول";
$lang['lang_back_to_login']     = "العودة لتسجيل الدخول";
$lang['lang_have_account']      = "هل لديك حساب ؟ تسجيل دخول";
$lang['lang_send_me_pass']      = "ارسل لى الرقم السرى";
$lang['lang_terms_conditions']  = "موافق على الشروط والاحكام.";
$lang['lang_users']             = "المستخدميين";
$lang['lang_clients']           = "عملاء";
$lang['lang_leave_msg']         = "اترك رسالة";
$lang['lang_send_msg']          = "ارسال رسالة";
$lang['lang_toggle_navi']       = "Toggle تبديل";
$lang['lang_menu']              = "القائمة";
$lang['lang_language']          = "اللغة";
$lang['lang_english']           = "انجليزى";
$lang['lang_espanol']           = "اسبانى";
$lang['lang_francais']          = "فرنسى";
$lang['lang_see_all_msgs']      = "اظهار كل الرسائل";
$lang['lang_see_all_notif']     = "اظهار كل الاخطارات";
$lang['lang_account_settings']  = "ضبط الحساب";
$lang['lang_system_users']      = "المستخدمين";
$lang['lang_add_user']          = "إضافة مستخدم جديد";
$lang['lang_actions']           = "اختيارات";
$lang['lang_view_users']        = "كل المستخدمين";
$lang['lang_edit_user']         = "تعديل المستخدم";
$lang['lang_delete_user']       = "ازالة المستخدم";
$lang['lang_confirm_delete_user']  = "هل انت متأكد من الغغاء هذا المستخدم?";
$lang['lang_system_clients']    = "انظمة المستخدمين";
$lang['lang_active_clients']    = "مستخدمين النشطين";
$lang['lang_waiting_clients']   = "المستخدمين المنتظريين";
$lang['lang_banned_clients']    = "المستخدمين المحظورين";
$lang['lang_ban']               = "نصف";
$lang['lang_active']            = "نشط";
$lang['lang_#']                 = "#";
$lang['lang_first']             = "اول";
$lang['lang_last']              = "اخر";
$lang['lang_display']           = "عرض";
$lang['lang_action']            = "الاختيار";
$lang['lang_receive_msg']       = "لتلقى كلمة مرور جديدة , ادخل عنوان بريدك الالكترونى ادناة.";
$lang['lang_thank_msg']         = "شكرا لك , تم ارسال كلمة المرور الجديدة لك.";
$lang['lang_email_sent']        = "شكرا لك . لقد تم ارسال رسالتك بنجاح.";
$lang['lang_go_to_login']       = "اذهب الى الدخول";
$lang['lang_register_success']  = ". لقد تم تسجيلك بنجاح. سنقوم بتنشيط حسابك قريبا. شكرا لك.";
$lang['lang_edit_client']       = "تحرير العميل";
/////////////////////////////////////////////////////////////////////////////////////

$lang['permission_denied']="طلب الاذن مرفوض";
$lang['permission_denied_msg']="ليس لديك اذن الوصول الى هذة الصفحة";
$lang['data_submitted_successfully']="تم تقدالبيانات بنجاح!";
$lang['data_saved_successfully']="تم حفظ البيانات بنجاح!";
$lang['select']="اختيار";
$lang['add']="اضافة";
$lang['edit']="تحرير";
$lang['delete']="ازالة";

$lang['borrower_information']=" بيانات المقترض";
$lang['save_continue_later']="حفظ ومتابعة فى وقت لاحق";
$lang['next']="التالى";




$lang['approve_generate']="الموافقة وتقديم الى I-Score";
$lang['case_incomplete']="هذة الحالة غير كاملة. من فضلك, اكمال قبل توليد هذا الملف";

$lang['cases'] = "لحالات";
$lang['approved'] = "وافق";

//BEGIN PASSWORD MANAGEMENT
$lang['password_management'] = "ادارة كلمة المرور";
$lang['enforce_history'] = "فرض محفوظات كلمة مرور";
$lang['max_age'] = "الحد الاقصى لعمر كلمة المرور (ايام)";
$lang['min_length'] = " الحد الادنى لطول الكلمة";
$lang['complex_password'] = "كلمة مرور معقدة";
$lang['lockout_duration'] = "مدةاقفال الحساب (Mins)";
$lang['lockout_threshold'] = "تأمين الحساب";
$lang['reset_counter'] = "

Reset Account Lockout Counter After

35/5000
إعادة تعيين حساب قفل حساب بعد (Mins)";
$lang['save']="Save";
//END PASSWORD MANAGEMENT

$lang['account_lockedout']="حساب مغلق";
$lang['account_lockedout_msg']="لقد بلغت الحد الأقصى لعدد التجارب. حسابك مغلق. من فضلك ، حاول تسجيل الدخول مرة أخرى";
$lang['minutes']="الدقائق";
$lang['old_password']="كلمة المرور القديمة";
$lang['new_password']="كلمة المرور جديدة";
$lang['change_password']="تغيير كلمة المرور";
$lang["too_short"]="قصير جدا";
$lang["weak"]="ضعيف";
$lang["medium"]="متوسط";
$lang["strong"]="قوى";
$lang["bank_only"]="البنك فقط";
$lang["branch_only"]="الفرع فقط";

$lang["bi_incomplete"]= "معلومات المقترض لهذه الحالة غير كامل";
$lang["total_sales_msg"]='يجب أن يكون معدل الدوران السنوي الذي يتم إدخاله في نموذج معلومات المقترض مساوياً لإجمالي المبيعات لأحدث البيانات المالية في النموذج المالي للمقترض';
$lang["assets_msg"]="يجب أن تكون متوازنة مجموع الأصول والخصوم الإجمالية";

$lang["C36_date_msg"]="يجب أن يكون التاريخ بين 1 يناير و 14 ديسمبر و 31 ديسمبر";
$lang["non_zero"]=" لا يمكن ان يكون صفر";
$lang["total_liabilities"]="اجمالي المطلوبات";
$lang["total_assets"]="مجموع الأصول";
$lang["approved_only"]="موافقة فقط";
$lang["completed_only"]="اكتمل فقط";
$lang["force_reset"]= "فرض المستخدم لإعادة تعيين كلمة المرور عند تسجيل الدخول";
$lang["excel_name_msg"]="توجد حالة غير كاملة بنفس الاسم. من فضلك ، أكملها أولاً";
$lang['financials_required']="البيانات المالية للمقترض مطلوبة";
$lang["C36_today_date_msg"]="لا يمكن أن يتجاوز التاريخ تاريخ اليوم";
$lang["annual_turnover_amount"]="مبالغ رقم الاعمال";
$lang["actual"]="جم";
$lang["bank_amount"]="مبالغ كشف الحساب";
$lang["file_required"]="تحميل ملف الحالات مطلوب";
$lang["cases_file"]="ملف الحالات";
$lang["bulk_case_creation"]="خلق مجموعة حالات";

//  ===================================================  another lamg file ======================================
$lang['yes'] = "نعم";
$lang['no'] = "لا";
// factsheet 
$lang['borrower_factsheet'] = "بيانات الملكية والادارة";
$lang['company_information'] = "بيانات الشركة ";
$lang['borrower_factsheet_C4'] = "سنة التاسيس";
$lang['borrower_factsheet_E4'] = "اجمالى المبيعات";
$lang['borrower_factsheet_C5'] = "اجمالي الأصول (جم)";
$lang['borrower_factsheet_E5'] = "نوع النشاط";
$lang['sr_no'] = "م";
$lang['investor_group'] = "الاسم";
$lang['share'] = "النسبة%";
$lang['shareholding'] = "هيكل الملكية";
$lang['board_of_directors'] = "مجلس الادارة";
$lang['name'] = "الاسم";
$lang['remarks'] = "الصفة";
$lang['key_personnel'] = "المديرين التنفيذيين";
$lang['designation'] = "الوظيفة";
$lang['borrower_financials'] = "نموذج البيانات المالية";
$lang['financial_data_template'] = "نموذج البيانات المالية";






$lang['required_param'] = "معلومات الزامية مدرجة";
$lang['check_errors'] = "يرجى التحقق من الحقول مع وجود أخطاء أدناه";
$lang['audited'] = "قوائم مدققة";
$lang['provisional'] = "قوائم مؤقتة";
$lang['actuals'] = "جم";
$lang['thousands'] = "بالالف";
$lang['millions'] = "بالملايين";
$lang['cases'] = "حالات الشركات الصغيرة والمتوسطة";
$lang['ref_no'] = "الرقم المرجعي";
$lang['created_by'] = "انشاء بواسطة";
$lang['actions'] = "اختيارات";
$lang['edit_case'] = "تعديل الحالة";
$lang['continue_case'] = "اكمل الحالة";
$lang['delete_case'] = " مسح الحالة";
$lang['back'] = "عودة";
$lang['wrong_step'] = "هذة الحالة لم تكتمل بعد.";
$lang['case_deleted'] = "تم ازالة الحالة بنجاح.";
$lang['add_case'] = "إضافة حالة";

$lang['created_date'] = "االتاريخ";
$lang['completed'] = "التقرير";
$lang['date_completed'] = "اكتمل التاريخ";

// Access levels 
$lang['access_levels'] = "أنواع المستخدمين";
$lang['access_level'] = "نوع المستخدم";
$lang['specify_access_level'] = "تحديد نوع المستخدم";
$lang['reset_password'] = "إعادة تعيين كلمة المرور";
$lang['email_not_exists'] = "هذا البريد الإلكتروني غير موجود";
$lang['acc_accessleveltypes'] = "أنواع المستخدمين";
$lang['add_level_type'] = "إضافة نوع المستوى";
$lang['level_name'] = "اسم المستوى";
$lang['users_access_level'] = "أنواع المستخدمين";
$lang['access_level_added_successfully'] = "تمت إضافة نوع المستخدم بنجاح.";
$lang['confirm_delete_access_level']   = "هل أنت متأكد من حذف نوع المستخدم هذا؟ سيكون جميع المستخدمين الذين لديهم هذا المستوى مشرفًا متميزًا.";
$lang['access_level_deleted_successfully'] = "تم حذف نوع المستخدم بنجاح.";
$lang['access_level_updated_successfully'] = "تم تحديث نوع المستخدم بنجاح.";
$lang['view'] = "معاينة";
$lang['create'] = "انشاء";
$lang['update'] = "تحديث";
$lang['delete'] = "حذف";
$lang['check_all'] = "تحقق من الكل
";
$lang['banks'] = "البنوك";
$lang['approve_case'] = "تأكيد الحالة";
$lang['users'] = "المستخدمين";
$lang['users_bank_managers'] = "إدارة المستخدمين - مسؤول البنك";
$lang['users_can_approve'] = "إدارة المستخدمين - التحقق";
$lang['users_normal'] = "إدارة المستخدمين - صانع";

$lang['banks'] = "البنوك";
$lang['add_bank'] = "إضافة بنك";
$lang['bank_name'] = "اسم البنك";
$lang['bank'] = "بنك";
$lang['lang_upload_bank_logo'] = "تحميل شعار البنك";
$lang['bank_logo'] = "شعار البنك";
$lang['data_deleted_successfully'] = "تم حذف البيانات بنجح!";
$lang['lang_error'] = "خطأ";
$lang['branch_code'] = "كود الفرع";
$lang['data_submitted_successfully_case_finished'] = "البيانات مقدمة بنجاح . الحالة انتهت";
$lang['bank_code'] = "كود البنك";
$lang['cannot_create_bank'] = "آسف! لا يمكنك إنشاء حالة جديدة نظرًا لأنه تم حذف البنك المعين.";
$lang['incompleted_case'] = "اسف لا يمكن الموافقة على الحالة نظرا لانها غير كا ملة";
$lang['cannot_create_file'] = "اسف ! لا يمكن انشاء ملف الاكسل";
$lang['case_approved'] = "تم الموافقة على الحالة وملف الاكسل بنجاح";
$lang['gradation_report']  = "تقارير التقييم";
$lang['upload'] = "تحميل";
$lang['case_not_found'] = "الحالةغير موجودة. يرجى التحقق من اسم الملف الذي تم تحميله ";
$lang['cannot_delete_file'] = "لا يمكن حذف ملف الكسل";
$lang['permissions'] = "صلاحيات";
$lang['gradation_rep'] = "تقرير متدرج";
$lang['settings'] = "الضبط";
$lang['users_super_admin'] = "إدارة المستخدمين - المشرفون المميزون";
$lang['users_custom_levels'] = "إدارة المستخدمين - مستويات مخصصة";
$lang['no_permission'] = "آسف! ليس لديك إذن للوصول إلى هذه الصفحة أو اتخاذ هذا الإجراء.";
$lang['not_set_permissions'] = "لم يتم تعيين أذوناتك بعد.";
$lang['reports'] = "تقارير";
$lang['cases_report'] = "تقرير الحالة";
$lang['bank_not_assigned'] = "لا يوجد بنك معين لك. <br/> فقد يكون البنك الذي تتعامل معه قد تم حذفه.";
$lang['case_name'] = "اسم الحالة";
$lang['comp_name'] = "اسم الشركة";
$lang['reg_num'] = "رقم السجل التجارى";
$lang['submitted_date'] = "تاريخ التقديم";
$lang['completed_date'] = "تاريخ الانتهاء";
$lang['status'] = "الحالة";
$lang['not_completed'] = "لم يكتمل";
$lang['waiting_approval'] = "انتظار التاكيد";
$lang['approved'] = "الموافقة";
$lang['branch'] = "فرع";
$lang['filter'] = "فلتر";
$lang['appearance'] = "موضوع";
$lang['sidebar_bg_color'] = "لون خلفية الشريط الجانبي";
$lang['sidebar_links_highlight'] = "روابط الشريط الجانبى المضيئة";
$lang['sidebar_links_font_color'] = "لون خط روابط الشريط الجانبى";
$lang['topbar_links_font_color'] = "Topbar Links لون الخط";
$lang['pages_header_bg'] = "لون خلفية رأس الصفحة";
$lang['pages_header_font_color'] = "لون خط بداية الصفحة";
$lang['topbar_bg_color'] = "لون خلفيةالجزء العلوى";
$lang['audit_trail'] = "سجل الاحداث";
$lang['action'] = "الاختيار";
$lang['user'] = "مستخدم";
$lang['time'] = "الوقت";
$lang['audit_trail_settings'] = "تقرير الفاعليات";
$lang['days'] = "ايام";
$lang['audits_expiry'] = "معدل حذف الفاعليات ";
$lang['audits_file_path'] = "مسار ملف الفاعليات";
$lang['save_next'] = "حفظ & التالى";
$lang['group_must_unique'] = "اسم المجموعة لابد ان يكون مختلف.";
$lang['sum_share_error'] = "نموذج المساهمة ، النسبة المئوية: يجب أن يكون مجموع كل حصة مساوياً لـ 100٪.";
$lang['enforce_password_history_hint'] = "يحدد إعداد الأمان هذا عدد كلمات المرور الجديدة الفريدة التي يجب إقرانها بحساب مستخدم ، في حالة التعطيل ، يمكن إعادة استخدام كلمة مرور قديمة.";
$lang['complex_password_hint'] = "يحدد إعداد الأمان هذا ما إذا كانت كلمات المرور يجب أن تفي بمتطلبات التعقيد ، إذا تم تمكين هذه السياسة ، يجب أن تتوافق كلمات المرور مع الحد الأدنى من المتطلبات التالية ، وتحتوي على أحرف من ثلاث فئات من الفئات الأربع التالية: الأحرف الإنجليزية الكبيرة (من أ إلى ي) ، والأحرف الصغيرة الإنجليزية (من خلال z) ، الأرقام الأساسية للخانة 10 (من 0 إلى 9) ، والأحرف غير الأبجدية (على سبيل المثال:! ، $ ، # ،٪). يتم فرض متطلبات التعقيد عند تغيير كلمات المرور أو إنشاؤها.";
$lang['max_age_hint'] = 'يحدد إعداد الأمان هذا الفترة الزمنية (بالأيام) التي يمكن فيها استخدام كلمة مرور قبل أن يطلب النظام من المستخدم تغييرها. يمكنك تعيين كلمات المرور بحيث تنتهي صلاحيتها بعد عدد من الأيام بين 1 و 999 ، أو يمكنك تحديد أن كلمات المرور لا تنتهي صلاحيتها أبدًا عن طريق تعيين عدد الأيام على 0.';
$lang['min_length_hint'] = 'يحدد إعداد الأمان هذا الحد الأدنى لعدد الأحرف المطلوب في كلمة مرور المستخدم';
$lang['lockout_duration_hint'] = '
يحدد إعداد الأمان هذا عدد الدقائق التي لا يزال حساب تأمينها مغلقًا قبل أن يتم إلغاء قفله تلقائيًا. النطاق المتاح من 0 دقائق خلال 99،999 دقيقة. إذا عيّنت مدة قفل الحساب إلى 0 ، فسيتم حظر الحساب إلى أن يلغيه المشرف صراحةً
.';
$lang['lockout_threshold_hint'] = 'يحدد إعداد الأمان هذا عدد محاولات تسجيل الدخول الفاشلة التي تؤدي إلى تأمين حساب مستخدم. لا يمكن استخدام حساب مقفل حتى يتم إعادة تعيينه من قبل مسؤول أو حتى انتهاء مدة التأمين للحساب. يمكنك تعيين قيمة بين 0 و 999 محاولات تسجيل الدخول الفاشلة. إذا قمت بتعيين القيمة إلى 0 ، فلن يتم تأمين الحساب أبدًا.';
$lang['reset_counter_hint'] = 'يحدد إعداد الأمان هذا عدد الدقائق التي يجب أن تنقضي بعد محاولة تسجيل دخول فاشلة قبل إعادة تعيين عداد محاولة تسجيل الدخول الفاشلة إلى 0 محاولات تسجيل الدخول غير الصحيحة. النطاق المتاح من دقيقة واحدة إلى 99،999 دقيقة.';


$lang['discard'] = "الغاء";
$lang['close'] = "اغلاق";
$lang['reload_logs'] = "تحديث السجلات";

$lang['must_be_2_decimals'] = "يجب أن يكون الرقم بحد أقصى عشريين";
$lang['financial_borrower_error'] = "يجب أن يكون الفرق بين رقم الأعمال السنوي الذي تم إدخاله في معلومات المقترض وإجمالي المبيعات لأحدث سنة مالية تم إدخالها في التمويل المالي للمقترض أقل من 1000";
$lang['amounts'] = "المبالغ (إجمالي المبيعات ، إجمالي الأصول)";
$lang['deactivate'] = "الغاء";
$lang['activate'] = "تفعيل";
$lang['select_access_level'] = "تحديد نوع المستخدم";
$lang['initiation_date'] = "تاريخ بدء التقييم";
$lang['finalization_date'] = "تاريخ انهاء التقييم";
$lang['submission_date'] = "تاريخ التقديم";
$lang['cases_imported_successfully'] = "تم ادخال الحالات بنجاح!";
$lang["bulk_case_creation_report"]="تقرير انشاء مجموعة الحالات";
$lang["row_no_sheet"]="رقم الصف فى الملف الاصلى";
$lang["errors"]="خطأ";
$lang["bulk_failed_report"]="بعض الحالات كانت غير صالحة أثناء الاستيراد. يرجى إصلاح الأخطاء وإعادة تحميل الملف. قم بتنزيل الملف مع الحالات غير الصالحة ";
$lang['here']="هنا";
$lang['not_approved'] = "رفض الحالة";
$lang['comment'] = "تعليق";
$lang['rejected'] = "رفض";
$lang['show_rejection_comment'] = "إظهار تعليق الرفض";
$lang['cannot_edit'] = "هذه الحالة تنتظر الموافقة. لا يمكنك تحريرها.";
$lang['appoved_cannot_edit'] = "تمت الموافقة على هذه الحالة. لا يمكنك تحريرها.";
$lang['marked_as_rejected'] = "تم تمييز الحالة بنجاح على أنها مرفوضة.";
$lang['copy_case'] = "نسخ الحالة";
$lang['case_not_found_copy'] = "الحالة غير موجودة.";
$lang['not_found_borrower'] = "لم يتم العثور على بيانات معلومات المقترض.";
$lang['not_found_factsheet'] = "لم يتم العثور على بيانات الملكية والإدارة";
$lang['not_found_financials'] = "لم يتم العثور على البيانات المالية.";
$lang['not_found_assessment'] = "لم يتم العثور على بيانات تقييم المقترض.";
$lang['completed_with_errors'] = "اكتمل مع الأخطاء";
$lang['success_copy'] = "تم نسخ الحالة بنجاح لحالة جديدة برقم مرجعي: ";
$lang['optional'] = "(اختيارى)";
$lang['non_subscriber'] = "غير مشترك بالخدمة";
$lang['non_subscriber_bank_error'] = "لا يمكن اتخاذ هذا الاجراء.هذة الحالة من بنك غير مشترك";
$lang['case_not_found_or_non_subscriber'] = "لم يتم العثور على الحالةاو فى حالة بنك غير مشترك.يرجى التحقق من اسم الملف الذى تم تحميلة";



$lang['import_cases_by_bulk'] = "إضافة مجموعة حالات";
$lang['download_template'] = "تحميل القالب";
$lang['add_hist'] = "إضافة البيانات المالية التاريخية";
$lang['add_proj'] = "اضافة مالية التوقعة";
$lang['r_u_sure']="هل أنت واثق؟ لا يمكنك التراجع عن هذا الإجراء";
$lang['code']="كود";
$lang['value']="قيمة";
$lang['code_field_unique']="يجب أن يكون حقل الكود فريدً";

$lang['input_settings'] = "إعدادات الإدخال";
$lang['options'] = "اختيارات";
$lang['add_input_settings'] = "Add new input for %s from settings with code: %s";
$lang['edit_input_settings'] = "Update input for %s from settings with code: %s";




// ====================================== From Here Must Be From Excel File ========================================


//BEGIN BORROWER INFORMATION
$lang["borrower_information_E4"]="اسم الشركة (باللغة العربية)";
$lang["borrower_information_C4"]="اسم الشركة (باللغة الإنجليزية)";
$lang["borrower_information_C5"]="رقم السجل التجاري";
$lang["borrower_information_C6"]="جهة/محافظة اصدار السجل التجاري ";
$lang["borrower_information_C7"]="الرقم الرمزي";
$lang["borrower_information_C8"]="الرقم القومي";
$lang["borrower_information_C9"]="الصناعة طبقا لتصنيف البنك المركزي المصري";
$lang["borrower_information_C10"]="نوع النشاط";
$lang["borrower_information_C11"]="الشكل القانونى";
$lang["borrower_information_C12"]="رقم الاعمال (جم) (طبقا لاخر ميزانية مقدمة)";
$lang["borrower_information_C13"]=" نسبة نمو المبيعات (%)";
$lang["borrower_information_C16"]="اسم البنك";
$lang["borrower_information_C17"]="كود الفرع";
$lang["borrower_information_C18"]="اسم المستخدم (المقيًم)";
$lang["borrower_information_C19"]="سم المستخدم (المعتمد)";
$lang["borrower_information_C20"]="تاريخ بدأ التقييم";
$lang["borrower_information_C21"]="تاريخ انتهاء التقييم";
$lang["borrower_information_C36"]="تاريخ اخر ميزانية مقدمة (يوم-شهر-سنة)";

$lang["borrower_information_B24"]="شهر & سنة";
$lang["borrower_information_C24"]="الايداعات الشهرية (جم)";
$lang["borrower_information_D24"]=" ";

$lang["borrower_information_B25"]="شهر & سنة";
$lang["borrower_information_C25"]="الايداعات الشهرية (جم)";
$lang["borrower_information_D25"]="رصيد الحساب في اليوم ال 15 من الشهر";

$lang["borrower_information_B26"]="شهر & سنة";
$lang["borrower_information_C26"]="الايداعات الشهرية (جم)";
$lang["borrower_information_D26"]="رصيد الحساب في اليوم ال 15 من الشهر";

$lang["borrower_information_B27"]="شهر & سنة";
$lang["borrower_information_C27"]="الايداعات الشهرية (جم)";
$lang["borrower_information_D27"]="رصيد الحساب في اليوم ال 15 من الشهر";

$lang["borrower_information_B28"]="شهر & سنة";
$lang["borrower_information_C28"]="الايداعات الشهرية (جم)";
$lang["borrower_information_D28"]="رصيد الحساب في اليوم ال 15 من الشهر";

$lang["borrower_information_B29"]="شهر & سنة";
$lang["borrower_information_C29"]="الايداعات الشهرية (جم)";
$lang["borrower_information_D29"]="رصيد الحساب في اليوم ال 15 من الشهر";

$lang["borrower_information_B30"]="شهر & سنة";
$lang["borrower_information_C30"]="الايداعات الشهرية (جم)";
$lang["borrower_information_D30"]="رصيد الحساب في اليوم ال 15 من الشهر";

$lang["borrower_information_D32"]="الالتزامات الشهرية الثابتة (من كشف الحساب)";

$lang['company_details_english']="بيانات الشركة";
$lang['company_details_arabic']="تفاصيل الشركة (العربية)";
$lang['bank_statement_details']="تفاصيل كشف الحساب(اخر 6 اشهر) (على مستوى جميع حسابات العميل بالبنوك)";
//END BORROWER INFORMATION


// assessment file
$lang['borrower_assessment'] = "بيان تقييم المقترض";
$lang['assessment_parameter'] = " معيار التقييم";
$lang['parameter_description'] = "تعريف المعيار";
$lang['select'] = "الاختيار";
$lang['borrower_assessment_F4'] = "تقييم الصناعة";
$lang['borrower_assessment_F17'] = " تقييم النشاط";
$lang['borrower_assessment_F5'] = "مخاطر الصناعة";
$lang['borrower_assessment_F5_desc'] = "يركز تقييم مخاطر الصناعة على الصناعة التي تعمل فيها الشركة ككل. العوامل الخاصة المتعلقة بالشركة لا يتم اخذها في الاعتبار أثناء عملية تحليل الصناعة.";
$lang['borrower_assessment_F10'] = "أثر السياسات / اللوائح الحكومية";
$lang['borrower_assessment_F10_desc'] = "يقوم هذا العامل بالنظر عما إذا كانت سياسة الحكومة (فرض الضرائب، القواعد التنظيمية ...إلخ) خلال 1-2 سنة التالية ستؤثر على الصناعة على نحو إيجابي أو سلبي.";
$lang['borrower_assessment_F18'] = "تقلبات الصرف الأجنبي";
$lang['borrower_assessment_F18_desc'] = 'يحدد نطاق الواردات / الصادرات كنسبة للمبيعات التعرض لمخاطر العملة الأجنبية كما أن وجود التحوط الطبيعي يقلل من ذلك الخطر. يلزم تقييم هذا العنصر على أساس وجود مخاطر الفوركس "سوق تداول العملات الأجنبية" وجهود الحد من هذه المخاطر بفاعلية أو وجود التحوط الطبيعي.';
$lang['borrower_assessment_F22'] = "مخاطر المورديين";
$lang['borrower_assessment_F22_desc'] = "أحتمالية أن تكون قوة مساومة المورد أعلى عندما:
* يسيطر قلة من كبار الموردين على السوق بدلًا عن مصدر توريد متجزأ
* لا يوجد بدائل للمدخلات المحددة
* عملاء المورد متفرقون، وبالتالي تنخفض قوة مساومة العملاء
* ارتفاع تكلفة التبديل من مورد لآخر
* وجود احتمالية دمج الموردين مستقبلًا بغرض الحصول على أسعار وهوامش أعلى";
$lang['borrower_assessment_F29'] = "خطر تركز العملاء";
$lang['borrower_assessment_F29_desc'] = "يقيم هذا العنصر انتشار قاعدة عملاء الكيان من ناحية عدد العملاء المساهمين في قوام إيرادات الشركة. يجب أن يُنظر إلى قاعدة العملاء الثابتة والمستقرة باستحسان";
$lang['borrower_assessment_F36'] = "الميزة المتعلقة بالموقع";
$lang['borrower_assessment_F36_desc'] = "تؤدي الميزة المتعلقة بالموقع إلى تقليل مخاطر الأعمال التجارية. الكيان القريب من سوقه المستهدف يتمتع بميزة كما يحسن أيضًا من إمكانية الكيان على خدمة عملائه بنطاق أوسع. ومع ذلك يلزم تقييم الموقع ليس فقط من الناحية الجغرافية ولكن يجب أخذ العوامل الحاسمة الأخرى في الحسبان مثل التنافسية والبنية الأساسية الداعمة وحجم السوق ...إلخ";
$lang['borrower_assessment_F40'] = "استقرار الأعمال التجارية";
$lang['borrower_assessment_F40_desc'] = "يقوم العنصر بتقييم مدى استقرار النشاط من ناحية عدد سنوات التشغيل من نفس مقر النشاط. مستوى الاستقرار الأعلى مؤشر على استدامة النشاط وقوته";
$lang['borrower_assessment_F45'] = " هيئة المنتج / العميل";
$lang['borrower_assessment_F45_desc'] = "
سيتم تقييم التنوع في قاعدة العملاء / المنتجات ومخاطر التركز للإيراد.";
$lang['borrower_assessment_F50'] = "ملكية مقر النشاط";
$lang['borrower_assessment_F50_desc'] = "يتم استخدام نوع ملكية مقر النشاط لتقييم استقرار النشاط من ناحية ملائته المالية والالتزام تجاه الأعمال التجارية. مقر النشاط المسجل باسم المشروع التجاري أو المؤسس أقل خطورة من مقر النشاط مؤجر. ";
$lang['borrower_assessment_F56'] = "عدد سنوات النشاط";
$lang['borrower_assessment_F56_desc'] = "عدد السنوات التي باشر فيها الكيان العمل في نفس نوع نشاط.";
//$lang['borrower_assessment_F61'] = "عدد سنوات النشاط";
//$lang['borrower_assessment_F61_desc'] = "عدد السنوات التي باشر فيها الكيان العمل في نفس نوع نشاط";
$lang['borrower_assessment_F66'] = "الترخيص والتصريح";
$lang['borrower_assessment_F66_desc'] = "توافر الترخيص / التصريح بمزاولة النشاط";
$lang['borrower_assessment_F71'] = "نوع الآلات";
$lang['borrower_assessment_F71_desc'] = "نوع الآلات المستخدمة للتصنيع / الإنتاج. استخدام آلات متطورة يساعد في تحسين الفاعلية.";
$lang['borrower_assessment_F75'] = "مخاطر التكنولوجيا ";
$lang['borrower_assessment_F75_desc'] = "يمكن أن يقود أثر التقدم التكنولوجي إلى دورة حياة أقصر للمنتج كما سيكون معرض لمخاطر التقادم";
$lang['borrower_assessment_F79'] = "جودة الخدمة";
$lang['borrower_assessment_F79_desc'] = "يقوم تقييم هذا العنصر بناءا على جودة الخدمة وصورة العلامة التجارية";
$lang['borrower_assessment_F86'] = "العمليات والرقابة";
$lang['borrower_assessment_F86_desc'] = "وجود نظام لقياس الجودة وضمان مستوى الخدمة مع وجود إجراءات تشغيل قياسية وآليات لقياس رضا العملاء وأنظمة تقييم الأداء يؤدي إلى درجات تقييم أعلى. شهادات جودة العمليات المتنوعة يجب ان تكون إيجابية. وفي حالة نموذج المشروع التجاري صاحب الامتياز، يجب تقييم هذا العنصر بعد تقييم عمل الكيان وصاحب وكالته.";
$lang['borrower_assessment_F93'] = " مخاطر المخزون ";
$lang['borrower_assessment_F93_desc'] = "يقيم هذا العنصر مخاطر المخزون مع الاخذ في الاعتبار 
1) مخاطر السعر - انخفاض حاد في سعر المنتجات يمكن أن يؤدي إلى تهاوي قيمة المخزون.
2) متوسط فترة التخزين - تواجه الكيانات ذات فترات تخزين المخزون الأعلى مخاطر أكبر";
$lang['borrower_assessment_F100'] = "تنوع المنتجات";
$lang['borrower_assessment_F100_desc'] = "التاجر صاحب التشكيلة الأكبر من المنتجات المعروضة للبيع، سيصنف أعلى في هذا المعيار. حيث سيقلل الاعتماد على منتج بعينه كما يعطي ميزة التعددية. إن مجموعة المنتجات المتنوعة تحدّ من المخاطر المترتبة على تباطؤ الطلب أو التنافسية. يتم تقييم تشكيلة المنتجات بناء على الاتساع (تنوع المنتجات) بجانب العمق (التنوع في المنتج الواحد). وجود منتجات ذات القيمة المضافة العالية يساعد على تقليل المخاطر.";
$lang['borrower_assessment_F107'] = "تصنيف الاتحاد المصري لمقاولي التشييد والبناء";
$lang['borrower_assessment_F107_desc'] = "وفقا لتصنيف الاتحاد المصري لمقاولي التشييد والبناء من 1 إلى 7";
$lang['borrower_assessment_F115'] = "القدرة على التنفيذ";
$lang['borrower_assessment_F115_desc'] = "القدرة على تنفيذ المشروعات ذات نفس الحجم والطبيعة سيساعد في تحسين وضع السوق للشركات التي تقدم الخدمات لهذه الشريحة من المشروعات. ";
$lang['borrower_assessment_F120'] = "التقييم المالي";
$lang['borrower_assessment_F121'] = "توافر القوائم المالية ونوعها";
$lang['borrower_assessment_F121_desc'] = "تقوم بقياس جودة القوائم المالية. الممارسات المحاسبية المالية الواهنة ستؤدي إلى نتائج متضخمة.";
$lang['borrower_assessment_F125'] = "نوع القوائم المالية وجودتها";
$lang['borrower_assessment_F125_desc'] = "تقوم بقياس جودة القوائم المالية. الممارسات المحاسبية المالية الواهنة ستؤدي إلى نتائج متضخمة. يقوم هذا العامل بدور مثبط لتقييم المخاطر المالية.";
$lang['borrower_assessment_F129'] = "الموسمية في الأعمال التجارية";
$lang['borrower_assessment_F129_desc'] = "يعمل الكيان في شريحة منتجات / خدمات  موسمية بطبيعتها.";
$lang['borrower_assessment_F132'] = "تقييم الإدارة";
$lang['borrower_assessment_F133'] = "سمعة العميل";
$lang['borrower_assessment_F133_desc'] = "يقيم هذا العنصر سمعة الأشخاص / المؤسسين الرئيسيين الذين يديرون النشاط. يأخذ المعيار بعين الاعتبار ردود أفعال المتعاملين مع الكيان (الموردين والعملاء) لكي يتم تحديد مقدار الثقة والنزاهة التي يتمتع بها الكيان في سوقه. وبالإضافة إلى ذلك، يمكن أن تضفي سمعة العميل في محل النشاط بعض المعلومات عن النشاط بالنسبة للمشروعات الصغيرة ومتناهية الصغر";
$lang['borrower_assessment_F137'] = "التقرير الائتماني للعميل";
$lang['borrower_assessment_F137_desc'] = "درجة التقييم الرقم للجدارة الائتمانية (آي-سكور) للفرد / المؤسس الرئيسي الذي يدير المشروع التجاري";
$lang['borrower_assessment_F144'] = "التقرير الائتماني للكفيل";
$lang['borrower_assessment_F144_desc'] = "درجة التقييم الرقم للجدارة الائتمانية (آي-سكور) للكفيل";
$lang['borrower_assessment_F151'] = "مخاطر استمرار الأعمال التجارية";
$lang['borrower_assessment_F151_desc'] ="يقيم العنصر وضوح خطط التناوب ووجود طبقة ثانية للإدارة من أجل الاستدامة على المدى البعيد.";
$lang['borrower_assessment_F155'] = "مخاطر استمرار الأعمال التجارية / مخاطر التناوب ";
$lang['borrower_assessment_F155_desc'] = "يقيم هذا العنصر الاعتماد على الفرد / مجموعة الأفراد والوضوح في خطط التناوب للاستدامة على المدى البعيد.";
$lang['borrower_assessment_F159'] = "خبرة وكفاءة الإدارة / الفريق التنفيذي";
$lang['borrower_assessment_F159_desc'] = "المؤسس / المالك ذو الخبرة والكفاءة العالية في النشاط يضع الشركة في منزلة مجدية في مواجهة التنافسية، كما أن الالمام ببيئة العمل التشغيلية والكفاءات والمهارات الفنية المطلوبة يساعد على صنع القرار بشكل أفضل.";
$lang['borrower_assessment_F165'] = "سجل سداد المرافق";
$lang['borrower_assessment_F165_desc'] = "يقيم هذا العنصر سجل سداد المرافق للأشخاص / المؤسسين الرئيسيين الذين يديرون / يملكون النشاط";
$lang['borrower_assessment_F169'] = "ملكية محل السكن";
$lang['borrower_assessment_F169_desc'] = "يتم اتخاذ نوع ملكية محل سكن المقترض في الاعتبار عند تقييم استقرار المقترض من ناحية ملائته المالية واستقراره في المسكن. المقترض المقيم بمحل سكن مملوك لوالديه أو المملوك لنفسه سيكون أقل خطورة مقارنة بمقترض يعيش لفترات زمنية قصيرة في محل سكن مؤجر";
$lang['borrower_assessment_F175'] = "موقع محل السكن";
$lang['borrower_assessment_F175_desc'] = "المستوى الاجتماعي والمالي للمؤسسين / الشركاء يمكن تحديدهم من خلال موقع محل السكن. فإن كانوا يقطنون في نواحي معروفة و ذو متسوى عال، يمكن اعتبارهم ذوي مستوى مالي أفضل عن أولئك الذين يعيشون في نواحي أقل شهرة.";
$lang['borrower_assessment_F179'] = "الضمان الشخصي / ضمان الغير و/أو بوليصة التأمين على الحياة ";
$lang['borrower_assessment_F179_desc'] = "سيتم تقييم التوافر";
$lang['borrower_assessment_F184'] = "تقييم السلوك المصرفي";
$lang['borrower_assessment_F186'] = "سجل السداد الخاص بالكيان";
$lang['borrower_assessment_F186_desc'] = "سيهدف هذا العامل إلى النظر في سجل المدفوعات السابق الخاص بالكيان كما يشتمل كذلك على سجل إنجازات سداد المستحقات النظامية أو الالتزامات المالية بمعرفة الكيان";
$lang['borrower_assessment_F194'] = "متوسط الحدود الائتمانية المستخدمة";
$lang['borrower_assessment_F194_desc'] = "يحلل هذا المعيار متوسط استخدام العميل لحسابات السحب على المكشوف / القروض النقدية في آخر 6 أشهر";
$lang['borrower_assessment_F200'] = "العلاقة مع البنوك";
$lang['borrower_assessment_F200_desc'] = "يقيم هذا العنصر تعامل الكيان / المؤسس مع البنك المعني في عدد من السنوات";
$lang['borrower_assessment_F208'] = "الشيكات المرتدة / المرفوضة خلال آخر 6 أشهر";
$lang['borrower_assessment_F208_desc'] ="سيتم اخذ الكشوفات البنكية للمقترض في الاعتبار أثناء عملية تقييم هذا العنصر. سيتم تقويمه، في حالة اذا كان عدد الشيكات الصادرة أقل من 30 في آخر 6 أشهر. 
وستحسب كالتالي
نسبة الشيكات المرتدة = (عدد الشيكات المرتدة/ المرفوضة بسبب عجز النقدية ) / عدد الشيكات الصادرة من الكيان خلال آخر 6 أشهر";
$lang['borrower_assessment_F214'] = "تفاصيل التسهيلات";
$lang['borrower_assessment_F216'] = "نوع التسهيل الائتماني ";
$lang['borrower_assessment_F216_desc'] = "قرض لأجل / قرض لرأس المال العامل / الاعتماد المستندي ";
$lang['borrower_assessment_F221'] = "متوسط الرصيد البنكي الى الالتزامات الشهرية الثابتة";
$lang['borrower_assessment_F221_desc'] = "يتم احتساب هذا العنصر بناءا على القوائم المالية للمقترض.";
$lang['borrower_assessment_F229'] = "توافر القوائم المالية";
$lang['borrower_assessment_F229_desc'] = "عدد القوائم المالية التي توافرت في السنوات الماضية ستقوم بدور المثبط";


// financial file
$lang['borrower_financial_E3'] = "هل يمتلك المقترض قوائم مالية ؟";
$lang['borrower_financial_E4'] = " هل ترغب في إدخال البيانات المالية المتوقعة ؟";
$lang['borrower_financial_G8'] = "الارقام ب";
$lang['borrower_financial_notes'] = "الشركات التى يقل حجم اعمالها عن 10 مليون جم ,ادخال قوائم مالية لتلك الشركات اختيارى, ما لم تقدم تلك الشركات قوائم مالية للبنك لذا وجب ادخالها";
$lang['borrower_financial_notes_2'] = "ادخال البيانات المالية لسنة الاساس للشركات ذات حجم اعمال اكبر من 10 مليون جم الزامى , ادخال القوائم المالية للسنوات السابقة اختيارى , ما لم تقدم تلك الشركات قوائم مالية للبنك عن تلك السنوات للبنك لذا وجب ادخالها ";
$lang['borrower_financial_12'] = "تاريخ اقفال الميزانية العمومية";
$lang['borrower_financial_13'] = "عدد الأشهر";
$lang['borrower_financial_14'] = "أسم مراقب الحسابات";
$lang['borrower_financial_16'] = "قائمة الدخل";
$lang['borrower_financial_17'] = "صافي المبيعات";
$lang['borrower_financial_18'] = "المبيعات (محلي)";
$lang['borrower_financial_19'] = "المبيعات (تصدير)";
$lang['borrower_financial_20'] = "اجمالي صافي المبيعات";
$lang['borrower_financial_22'] = "ايراد تشغيلي اخر";
$lang['borrower_financial_23'] = "اجمال الدخل التشغيلي";
$lang['borrower_financial_25'] = "تكلفة المبيعات";
$lang['borrower_financial_26'] = "المواد الخام";
$lang['borrower_financial_27'] = "العمالة المباشرة (الأجور والمرتبات)";
$lang['borrower_financial_28'] = "مصروفات مباشرة أخرى";
$lang['borrower_financial_29'] = "اجمالي تكلفة المبيعات";
$lang['borrower_financial_31'] = "مصروفات بيعية، عمومية و إدارية";
$lang['borrower_financial_32'] = "مصروفات الأجور والمرتبات";
$lang['borrower_financial_33'] = "ايجارات";
$lang['borrower_financial_34'] = "مصروفات بيعية، عمومية و إدارية أخرى";
$lang['borrower_financial_35'] = "مصروفات السفر والانتقالات";
$lang['borrower_financial_36'] = "مصروفات بيعية وتسويقية";
$lang['borrower_financial_38'] = "أهلاكات وإستهلاكات";
$lang['borrower_financial_39'] = "مخصص تشغيلي";
$lang['borrower_financial_41'] = "الربح التشغيلي قبل الفوائد";
$lang['borrower_financial_43'] = "فوائد مدينة";
$lang['borrower_financial_45'] = "الربح التشغيلي بعد الفوائد";
$lang['borrower_financial_47'] = "إيرادات غير تشغيليه أخرى";
$lang['borrower_financial_48'] = "البنود النقدية";
$lang['borrower_financial_49'] = "فوائد دائنة";
$lang['borrower_financial_50'] = "توزيعات أرباح";
$lang['borrower_financial_51'] = "البنود غير النقدية";
$lang['borrower_financial_52'] = "اجمالي الإيرادات غير التشغيليه الأخرى";
$lang['borrower_financial_54'] = "مصروفات غير تشغيليه أخرى";
$lang['borrower_financial_55'] = "خسائر فروق عملة";
$lang['borrower_financial_56'] = "بنود غير نقدية";
$lang['borrower_financial_58'] = "مخصصات أخرى";
$lang['borrower_financial_60'] = "الربح قبل الضرائب";
$lang['borrower_financial_62'] = "ربح من بيع أصول ثابتة / استثمارات";
$lang['borrower_financial_63'] = "خسارة من بيع أصول ثابتة / استثمارات";
$lang['borrower_financial_65'] = "مخصص الضرائب";
$lang['borrower_financial_66'] = "ضرائب حالية";
$lang['borrower_financial_67'] = "ضرائب مؤجلة";
$lang['borrower_financial_69'] = "صافي الربح / الخسارة";
$lang['borrower_financial_71'] = "توزيعات الأرباح (مدفوع + مقترح)";
$lang['borrower_financial_73'] = "الأرباح المحتجزة";
$lang['borrower_financial_75'] = "الميزانية العمومية (الخصوم)";
$lang['borrower_financial_77'] = "الخصوم المتداولة";
$lang['borrower_financial_78'] = "الاقتراض قصير الأجل من البنوك
                                    (بما في ذلك الائتمان المشترين ، فواتير
                                    تم شراؤها ، فواتير مخفضة &
                                    زيادة الاقتراض على أساس السداد)";
$lang['borrower_financial_79'] = "دائنون وأرصدة دائنة أخرى";
$lang['borrower_financial_80'] = "دفعات مقدمة من العملاء";
$lang['borrower_financial_81'] = "توزيعات أرباح اجله";
$lang['borrower_financial_82'] = "خصوم قانونية أخرى (تستحق خلال سنة واحدة)";
$lang['borrower_financial_83'] = "الجزء المستحق من الديون طويلة الأجل";
$lang['borrower_financial_84'] = "مستحق لأطراف ذات علاقة";
$lang['borrower_financial_85'] = "فوائد مستحقة";
$lang['borrower_financial_86'] = "مصروفات مستحقة";
$lang['borrower_financial_87'] = "مستحق لمقاولي الباطن";
$lang['borrower_financial_88'] = "ضرائب مستحقة";
$lang['borrower_financial_89'] = "مستحق للشركاء / المساهمين / المالكين";
$lang['borrower_financial_90'] = "ضمان تشغيلي";
$lang['borrower_financial_91'] = "خصوم متداولة أخرى";
$lang['borrower_financial_92'] = "دائنون لتغطية النفقات";
$lang['borrower_financial_93'] = "مصروفات الموظف مستحقة الدفع";
$lang['borrower_financial_94'] = "الجزء المستحق من التأجير";
$lang['borrower_financial_95'] = "أجمالي الخصوم المتداولة";
$lang['borrower_financial_96'] = "خصوم طويلة الأجل";
$lang['borrower_financial_97'] = "ديون طويلة الأجل (جزء غير متداول)";
$lang['borrower_financial_98'] = "مستحق لأطراف ذات علاقة";
$lang['borrower_financial_99'] = "دفعات مقدمة من العملاء (جزء غير متداول)";
$lang['borrower_financial_100'] = "ديون طويلة الأجل أخرى";
$lang['borrower_financial_101'] = "أجمالي الخصوم طويلة الأجل";
$lang['borrower_financial_103'] = "أجمالي الخصوم";
$lang['borrower_financial_105'] = "حقوق المساهمين";
$lang['borrower_financial_106'] = "رأس المال المدفوع";
$lang['borrower_financial_107'] = "الاحتياطي القانوني";
$lang['borrower_financial_108'] = "الاحتياطي العام";
$lang['borrower_financial_109'] = "احتياطي إعادة التقييم";
$lang['borrower_financial_110'] = "الأرباح المحتجزة";
$lang['borrower_financial_111'] = "الاحتياطيات الأخرى (باستثناء المخصصات)";
$lang['borrower_financial_112'] = "الفائض (+) / العجز (-) في قائمة الدخل";
$lang['borrower_financial_113'] = "مستحق للشركاء / المساهمين / المالكين";
$lang['borrower_financial_114'] = "أخرى";
$lang['borrower_financial_115'] = "إضافي رأس المال المدفوع";
$lang['borrower_financial_116'] = "صافي حقوق المساهمين";
$lang['borrower_financial_117'] = "مخصص الضرائب";
$lang['borrower_financial_119'] = "حقوق الأقلية";
$lang['borrower_financial_121'] = "أجمالي الخصوم وحقوق المساهمين";
$lang['borrower_financial_123'] = "الميزانية العمومية (الأصول)";
$lang['borrower_financial_125'] = "الأصول المتداولة";
$lang['borrower_financial_126'] = "النقدية بالبنوك وما في حكمها";
$lang['borrower_financial_127'] = "استثمارات قصيرة الاجل";
$lang['borrower_financial_128'] = "صافي أوراق القبض";
$lang['borrower_financial_129'] = "مدينون وأرصدة مدينة أخرى";
$lang['borrower_financial_130'] = "مخصص الديون مشكوك في تحصيلها";
$lang['borrower_financial_131'] = "المخزون:";
$lang['borrower_financial_132'] = "المواد الخام";
$lang['borrower_financial_133'] = "مخزون تحت التشغيل";
$lang['borrower_financial_134'] = "بضاعة في الطريق";
$lang['borrower_financial_135'] = "مخزون أخرى";
$lang['borrower_financial_136'] = "مخزون تام الصنع";
$lang['borrower_financial_137'] = "دفعات مقدمة للموردين (مواد خام، مخزون و قطع غيار)";
$lang['borrower_financial_138'] = "أصول متداولة أخرى";
$lang['borrower_financial_139'] = "مستحق من اطراف ذات علاقة";
$lang['borrower_financial_140'] = "مستحق من مقاولي الباطن";
$lang['borrower_financial_141'] = "اجمالي الأصول المتداولة";
$lang['borrower_financial_143'] = "الأصول الثابتة";
$lang['borrower_financial_144'] = "اجمال الأصول الثابتة";
$lang['borrower_financial_145'] = "أراضي";
$lang['borrower_financial_146'] = "مباني";
$lang['borrower_financial_147'] = "الات ومعدات";
$lang['borrower_financial_148'] = "اعمال تحت التنفيذ";
$lang['borrower_financial_149'] = "محمع الاهلاك";
$lang['borrower_financial_150'] = "صافي الأصول الثابتة";
$lang['borrower_financial_152'] = "أصول غير متداولة أخرى";
$lang['borrower_financial_153'] = "مستحق من اطراف ذات علاقة";
$lang['borrower_financial_154'] = "استثمارات";
$lang['borrower_financial_155'] = "دفعات مقدمة للموردين (أصول راسمالية)";
$lang['borrower_financial_156'] = "ضرائب مؤجلة";
$lang['borrower_financial_157'] = "اخرى";
$lang['borrower_financial_158'] = "ضمان تشغيلي";
$lang['borrower_financial_159'] = "أصول ملموسة أخرى";
$lang['borrower_financial_160'] = "مصروفات مقدمة";
$lang['borrower_financial_161'] = "مستحق من المساهمين / الشركاء / الماكين";
$lang['borrower_financial_162'] = "ضرائب راسمالية مؤجلة";
$lang['borrower_financial_163'] = "أوراق قبض (اكثر من سنة واحدة)";
$lang['borrower_financial_164'] = "اجمالي الأصول غير المتدولة الأخرى";
$lang['borrower_financial_166'] = "أصول غير ملموسة";
$lang['borrower_financial_167'] = "مجمع استهلاك الأصول غير الملموسة";
$lang['borrower_financial_168'] = "صافي الأصول غير الملموسة";
$lang['borrower_financial_170'] = "اجمالي الأصول";
$lang['borrower_financial_171'] = "التحقق من صحة الميزانية العمومية(اجمالي الأصول يطرح منه اجمالي الخصوم)";
$lang['borrower_financial_173'] = "نسب الربحية";
$lang['borrower_financial_174'] = " ";
$lang['borrower_financial_175'] = "COGS Margin";
$lang['borrower_financial_176'] = "EBITDA Margin - 3 years moving average";
$lang['borrower_financial_177'] = "Interest Coverage";
$lang['borrower_financial_178'] = "Net Profit Margin “ROS” - 3 years moving average";
$lang['borrower_financial_179'] = "Return-on-Equity “ROE”";
$lang['borrower_financial_180'] = "Dividends Payout";
$lang['borrower_financial_182'] = "رافعة مالية";
$lang['borrower_financial_183'] = "Financial Leverage (TOL/TNW)";
$lang['borrower_financial_184'] = "Gearing (TTL/TNW)";
$lang['borrower_financial_185'] = "Net Leverage (TOL-Cash/TNW)";
$lang['borrower_financial_186'] = "Net Debt to EBITDA";
$lang['borrower_financial_187'] = "Total Debt / TNW";
$lang['borrower_financial_188'] = "سيولة";
$lang['borrower_financial_189'] = "Current Ratio";
$lang['borrower_financial_190'] = "Working Capital";
$lang['borrower_financial_191'] = "Quick Ratio";
$lang['borrower_financial_192'] = "Working Investment";
$lang['borrower_financial_194'] = "كفاءة الأصول التجارية";
$lang['borrower_financial_195'] = "Inventory DOH";
$lang['borrower_financial_196'] = "Receivables DOH";
$lang['borrower_financial_197'] = "Payables DOH";
$lang['borrower_financial_198'] = "Cycle Gap";
$lang['borrower_financial_199'] = "Accrued Expense DOH";
$lang['borrower_financial_200'] = "Debt Service Coverage";
$lang['borrower_financial_202'] = "النسب الإضافية";
$lang['borrower_financial_203'] = "ROCE";
$lang['borrower_financial_204'] = "Growth in Sales";
$lang['borrower_financial_205'] = "Asset Turnover - 3 years moving average";
$lang['borrower_financial_206'] = "Working Investment/Sales";
$lang['borrower_financial_207'] = "Cash Accrual to Total Debt";
$lang['borrower_financial_208'] = "Total Credit amount in account - Annualized";
$lang['borrower_financial_209'] = "Credit Turnover to Total Sales";
$lang['borrower_financial_210'] = "Average Bank Balance in the account";
$lang['borrower_financial_211'] = "Average Bank Balance to Monthly Fixed Obligations";
$lang['borrower_financial_212'] = "EBITDA Margin";
$lang['borrower_financial_213'] = "Net Profit Margin";
$lang['borrower_financial_214'] = "Asset Turnover";
$lang['borrower_financial_218'] = "Number of years past financials available";
$lang['borrower_financial_219'] = "Financials Availibility";
$lang['borrower_financial_220'] = "Multiplication Factor for (Annualization)";
$lang['borrower_financial_221'] = "Multiplication Factor for (Denomination)";
$lang['borrower_financial_222'] = "Overall Multiplication Factor";
$lang['borrower_financial_223'] = "Annualized Operating Income";
$lang['borrower_financial_224'] = "Annualized Total Net Sales";


$lang['hide'] = "إخفاء";
$lang['completion_date'] = "تاريخ صدور التقرير";
$lang['no_special_chars']="الحروف الخاصة غير مقبولة لا يمكن إدخال سوى الحروف والأرقام في المدخلات ولا يمكن تركها خالية";
$lang['cannot_copy_case_files'] = "لا يمكن نسخ ملفات الحالة(PDF & xlsx)";
$lang['not_found_calculations'] = "لم يتم العثور على بيانات حسابات المقترض";
$lang['value_arabic'] ="القيمة(عربى)";
$lang['turnover_range_error'] = "رقم الاعمال يجب ان يكون رقم بين 1 و 200,000,000";

$lang['director_must_unique'] = "اسم عضو مجلس الإدارة لابد ان يكون مختلف.";
$lang['personnel_must_unique'] = "اسم المدير التنفيذي لابد ان يكون مختلف.";
$lang['no_special_chars_validation']="الحروف الخاصة غير مقبولة.";
$lang['no_decimals_allowed'] = "الكسور العشرية غير مقبولة";
$lang['between_0_100'] = "فقط الأرقام من 0 إلى 100 مقبولة.";
////////////////////////////
$lang['english_only'] = "اسم الشركة (باللغة الإنجليزية) لا ينبغي أن يحتوي سوى الحروف الإنجليزية";
$lang['arabic_only'] = "اسم الشركة (باللغة العربية) لا ينبغي أن يحتوي سوى الحروف العربية";

$lang['old_password_not_match'] = "كلمة المرور القديمة غير متطابقة";
$lang['could_not_verify_your_request']="نأسف لم نستطع تأكيد طلب";
$lang['reset_link_set']= "تم إرسال رابط إعادة تعيين كلمة المرور. من فضلك تفقد بريدك الإلكتروني.";
$lang['invalid_captcha']= "كلمة التحقق التي قمت بكتابتها غير متطابقة";
$lang['filed_required']= "هذا الحقل مطلوب";
$lang['bank_name_unique']= "حقل اسم البنك يجب أن يحتوي على قيمة فريدة";
$lang['bank_code_unique']= "حقل كود البنك يجب أن يحتوي على قيمة فريدة";
$lang['unique_email']= "حقل البريد الإلكتروني يجب أن يحتوي على قيمة فريدة";
$lang['E4_message']= "حقل إجمالي المبيعات يجب أن يكون بين 0 و 200,000,000";
$lang['C5_message']= "حقل مجموع الأصول يجب أن يكون بين 0 و 1000,000,000";

$lang['from']= "من";
$lang['to']= "إلى";
$lang['all']= "الكل";
$lang['change_captcha']= "قم بتغيير كلمة التحقق";

$lang['banks_report'] = "تقرير البنوك";
$lang['non_subscribed'] = "غير مشترك بالخدمة";
$lang['subscribed'] = "مشترك بالخدمة";
$lang['cases_entries'] = "اجمالى الحالات";
$lang['not_completed'] = "الغير مكتملة";
$lang['waiting_approval'] = "فى انتظار الموافقة";
$lang['completed_actually_lang'] = "المكتملة";
$lang['rejected_actually_lang'] = "المرفوضة";
$lang['file_size_error'] = "هناك خطأ بحجم الملف. من فضلك حاول مرة اخرى";

$lang['borrower_information_E7'] = "شكل التقرير";
$lang['borrower_information_E10'] = "الرقم الضريبي";

$lang['download_excel_file'] = "تنزيل ملف الexcel";

$lang['admin_area'] = "مدير الموقع";
$lang['bank_users_area'] = "المستخدمين التابعين لبنك";

$lang['something_went_wrong'] = "حدث خطأ ما.";

$lang['generate_excels_manually'] = "استخراج ملفات الاكسيل يدويا";

$lang['all_cases'] = "كل الحالات";
$lang['combine_same_registry_num'] = "دمج الحالات لنفس السجل التجارى";
$lang['count_method'] = "طريقة الحساب";
$lang['create_date'] = "تاريخ الاضافة";
