<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'third_party/tcpdf/tcpdf.php';

class Pdf extends TCPDF {

    public $_fonts_list = array();
    protected $last_page_flag = false;
    private $pdf_type = '';
    public $comp_logo_header = false;
    public $comp_address_footer = false;
    public $is_registration = false;

    function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $pdfa = false, $pdf_type = '') {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
        $this->pdf_type = $pdf_type;
        $lg = array();
        $lg['a_meta_charset'] = 'UTF-8';
        // set some language-dependent strings (optional)
        $this->setLanguageArray($lg);
        $this->_fonts_list = $this->fontlist;
    }

    public function Close() {
        $this->last_page_flag = true;
        parent::Close();
    }

    public function Header() {
        $this->SetFont('helvetica', 'B', 20);
        if ($this->comp_logo_header) {

            $dimensions = $this->getPageDimensions();
            $this->SetY(8);

            $logo = pdf_logo_url();



            if ($this->is_registration) {
                
//                $this->SetFontSize(15);
                $header = _l("registration_form");
                $form_title = '<h2 class="no-margin bold" style="color: #2f617d;">' . $header . '</h2>';
                $this->MultiCell(($dimensions['wk'] / 2) - $dimensions['lm'], 0, $form_title, 0, 'J', 0, 0, '', '', true, 0, true, true, 0);
                
                $this->MultiCell(($dimensions['wk'] / 2) - $dimensions['rm'], 0, $logo, 0, 'R', 0, 1, '', '', true, 0, true, false, 0);
            } else {
                $this->MultiCell(($dimensions['wk'] / 2) - $dimensions['rm'], 0, $logo, 0, 'R', 0, 1, ($dimensions['wk'] / 2), 5, true, 0, true, false, 0);

//                $this->SetFontSize(15);
                $this->SetY(3);
                $header = _l("the_agreement");
                $form_title = '<h3 class="no-margin bold" style="color: #2f617d">' . $header . '</h3>';
                $cont = $this->getPage() > 1 ? _l('cont') : '';
                $header .= $cont;
                $this->MultiCell(($dimensions['wk'] / 2) - $dimensions['lm'], 0, $form_title, 0, 'J', 0, 0, '', 20, true, 0, true, true, 0);
            }


            
            $this->SetFontSize(10);




            //$this->writeHTMLCell(($dimensions['wk'] / 2) - $dimensions['lm'], 0,  $this->GetX(),  $this->GetY(), 'TEST CELL STRETCH: no stretch', 0, 1, TRUE);
            //$this->SetY($this->GetY() + 20);
            // $this->Cell(10, 10, 'TEST CELL STRETCH: no stretch', 1, 2, 'C', 0, '', 0);
        }
    }

    public function Footer() {
        // Position at 15 mm from bottom
        if ($this->comp_address_footer) {
            if ($this->is_registration) {
                $this->SetY(-15);
                $this->SetFontSize(8);
            } else {
                $this->SetY(-13);
                $this->SetFontSize(8);
            }
        } else {
            $this->SetY(-15);
            $this->SetFontSize(10);
        }



        if ($this->comp_address_footer) {
            $this->SetTextColor(127, 127, 127);
            $footer = get_option('invoice_company_name') . ', ' . get_option('invoice_company_address') . ', ' .
                    get_option('invoice_company_city') . ', ' . get_option('invoice_company_postal_code') . ', ' .
                    get_option('invoice_company_country_code');
            $this->Cell(0, 0, $footer, 0, TRUE, 'C', 0, '', 0, false, 'T', 'M');

            $footer = _l('comp_registered');
            $this->Cell(0, 0, $footer, 0, TRUE, 'C', 0, '', 0, false, 'T', 'M');

            $footer = _l('email') . ': ' . get_option('invoice_company_email') . ', ' . _l('internet_site') . ': ' . get_option('invoice_company_website');
            $this->Cell(0, 0, $footer, 0, TRUE, 'C', 0, '', 0, false, 'T', 'M');
        } else {
            $font_name = "courier";
            $font_size = 10;

            if ($font_size == '') {
                $font_size = 10;
            }

            $this->SetFont($font_name, '', $font_size);
//
//            do_action('pdf_footer', array('pdf_instance' => $this, 'type' => $this->pdf_type));
            // Set font
            $this->SetFont('helvetica', 'I', 8);
            if (TRUE) {
                $this->SetTextColor(142, 142, 142);
                $this->Cell(0, 15, $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
            }
        }
    }

    public function get_fonts_list() {
        return $this->_fonts_list;
    }

}

/* End of file Pdf.php */
/* Location: ./application/libraries/Pdf.php */
