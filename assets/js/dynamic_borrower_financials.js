function proj_calc(year) {

    $("#" + year + "176").val('NA');
    $("#" + year + "178").val('NA');


    $("#" + year + "23, #" + year + "29, #" + year + "31, #" + year + "38, #" + year + "39").change(function () {
        var sum_cols = [year + "23", year + "29", year + "31", year + "38", year + "39"];
        $("#" + year + "41").val(get_sub(sum_cols));
        $("#" + year + "41").change();
    });

    $("#" + year + "20, #" + year + "22").change(function () {
        var sum_cols = [year + "20", year + "22"];
        $("#" + year + "23").val(get_sum(sum_cols));
        $("#" + year + "23").change();
    });


    $("#" + year + "18, #" + year + "19").change(function () {
        var sum_cols = [year + "18", year + "19"];
        $("#" + year + "20").val(get_sum(sum_cols));
        $("#" + year + "20").change();
    });


    $("#C26, #C27, #C28").change(function () {
        var sum_cols = [year + "26", year + "27", year + "28"];
        $("#" + year + "29").val(get_sum(sum_cols));
        $("#" + year + "29").change();
    });



    $("#" + year + "32, #" + year + "33, #" + year + "34, #" + year + "35, #" + year + "36").change(function () {
        var sum_cols = [year + "32", year + "33", year + "34", year + "35", year + "36"];
        $("#" + year + "31").val(get_sum(sum_cols));
        $("#" + year + "31").change();
    });
    $("#" + year + "43, #" + year + "41, #" + year + "38").change(function () {
        var val = $("#" + year + "43").val() == 0 ? 999 : 'NA';
        if (val == 'NA') {
            val = isFinite((Number($("#" + year + "41").val()) + Number($("#" + year + "38").val())) / $("#" + year + "43").val()) ? (Number($("#" + year + "41").val()) + Number($("#" + year + "38").val())) / $("#" + year + "43").val() : 'NA';
        }
        $("#" + year + "177").val(approx(val, 1));
    });
    $("#" + year + "45, #" + year + "52, #" + year + "54, #" + year + "58").change(function () {
        var sum_cols = [year + "45", year + "52", year + "54", year + "58"];
        $("#" + year + "60").val(get_sum_sub(sum_cols));
        $("#" + year + "60").change();
    });
    $("#" + year + "41, #" + year + "43").change(function () {
        var sum_cols = [year + "41", year + "43"];
        $("#" + year + "45").val(get_sub(sum_cols));
        $("#" + year + "45").change();
    });

    $("#" + year + "49, #" + year + "50").change(function () {
        var sum_cols = [year + "49", year + "50"];
        $("#" + year + "48").val(get_sum(sum_cols));
    });

    $("#" + year + "60, #" + year + "62, #" + year + "63, #" + year + "65").change(function () {
        var sum_cols = [year + "60", year + "62", year + "63", year + "65"];
        $("#" + year + "69").val(get_sum_sub(sum_cols));
        $("#" + year + "69").change();
    });

    $("#" + year + "49, #" + year + "50, #" + year + "51").change(function () {
        var sum_cols = [year + "49", year + "50", year + "51"];
        $("#" + year + "52").val(get_sum(sum_cols));
        $("#" + year + "52").change();
    });

    $("#" + year + "55, #" + year + "56").change(function () {
        var sum_cols = [year + "55", year + "56"];
        $("#" + year + "54").val(get_sum(sum_cols));
        $("#" + year + "54").change();
    });


    $("#" + year + "66, #" + year + "67").change(function () {
        var sum_cols = [year + "66", year + "67"];
        $("#" + year + "65").val(get_sum(sum_cols));
        $("#" + year + "65").change();
    });

    $("#" + year + "69, #" + year + "71").change(function () {
        var sum_cols = [year + "69", year + "71"];
        $("#" + year + "73").val(get_sub(sum_cols));
    });

    $("#" + year + "71, #" + year + "69").change(function () {
        //modified
        var val = isFinite($("#" + year + "71").val() / $("#" + year + "69").val()) ? perc($("#" + year + "71").val() / $("#" + year + "69").val()) : 'NA';
        $("#" + year + "180").val(val);
    });

    $("#" + year + "101, #" + year + "95").change(function () {
        var sum_cols = [year + "101", year + "95"];
        $("#" + year + "103").val(get_sum(sum_cols));
    });
    $("#" + year + "78, #" + year + "79, #" + year + "80, #" + year + "81, #" + year + "82, #" + year + "83, #" + year + "84, #" + year + "85, #" + year + "86, #" + year + "87, #" + year + "88, #" + year + "89, #" + year + "90, #" + year + "91, #" + year + "94").change(function () {
        var sum_cols = [year + "78", year + "79", year + "80", year + "81", year + "82", year + "83", year + "84", year + "85", year + "86", year + "87", year + "88", year + "89", year + "90", year + "91", year + "94"];
        $("#" + year + "95").val(get_sum(sum_cols));
        $("#" + year + "95").change();
    });
    $("#" + year + "92, #" + year + "93").change(function () {
        var sum_cols = [year + "92", year + "93"];
        $("#" + year + "91").val(get_sum(sum_cols));
        $("#" + year + "91").change();
    });

    $("#" + year + "97, #" + year + "98, #" + year + "99, #" + year + "100").change(function () {
        var sum_cols = [year + "97", year + "98", year + "99", year + "100"];
        $("#" + year + "101").val(get_sum(sum_cols));
        $("#" + year + "101").change();
    });

    $("#" + year + "69, #" + year + "116").change(function () {
        //modified

        var val = isFinite($("#" + year + "69").val() / $("#" + year + "116").val()) ? perc($("#" + year + "69").val() / $("#" + year + "116").val()) : 'NA';
        $("#" + year + "179").val(approx(val, 2));
    });
    $("#" + year + "106, #" + year + "107, #" + year + "108, #" + year + "109, #" + year + "110, #" + year + "111, #" + year + "112, #" + year + "113, #" + year + "114, #" + year + "115").change(function () {
        var sum_cols = [year + "106", year + "107", year + "108", year + "109", year + "110", year + "111", year + "112", year + "113", year + "114", year + "115"];
        $("#" + year + "116").val(get_sum(sum_cols));
        $("#" + year + "116").change();
    });

    $("#" + year + "170, #" + year + "121").change(function () {
        var sum_cols = [year + "170", year + "121"];
        $("#" + year + "171").val(get_sub(sum_cols));
    });
    $("#" + year + "95, #" + year + "101, #" + year + "116, #" + year + "117, #" + year + "119").change(function () {
        var sum_cols = [year + "95", year + "101", year + "116", year + "117", year + "119"];
        $("#" + year + "121").val(get_sum(sum_cols));
        $("#" + year + "121").change();
    });
    $("#" + year + "126, #" + year + "127, #" + year + "128, #" + year + "131, #" + year + "138, #" + year + "137, #" + year + "139, #" + year + "140").change(function () {
        var sum_cols = [year + "126", year + "127", year + "128", year + "131", year + "138", year + "137", year + "139", year + "140"];
        $("#" + year + "141").val(get_sum(sum_cols));
        $("#" + year + "141").change();
    });
    $("#" + year + "129, #" + year + "130").change(function () {
        var sum_cols = [year + "129", year + "130"];
        $("#" + year + "128").val(get_sub(sum_cols));
        $("#" + year + "128").change();
    });
    $("#" + year + "195, #" + year + "196, #" + year + "197, #" + year + "199").change(function () {
        var sum = get_sum([year + "195", year + "196"]);
        var val = (sum - $("#" + year + "197").val() - $("#" + year + "199").val());
        val = isFinite(val) ? val : 'NA';
        $("#" + year + "198").val(approx(val, 2));
    });
    $("#" + year + "29, #" + year + "131").change(function () {
        var val = isFinite(($("#" + year + "131").val() / $("#" + year + "29").val()) * 365) ? ($("#" + year + "131").val() / $("#" + year + "29").val()) * 365 : 'NA';
        $("#" + year + "195").val(approx(val, 2));
        $("#" + year + "195").change();
    });
    $("#" + year + "132, #" + year + "133, #" + year + "134, #" + year + "135, #" + year + "136").change(function () {
        var sum_cols = [year + "132", year + "133", year + "134", year + "135", year + "136"];
        $("#" + year + "131").val(get_sum(sum_cols));
        $("#" + year + "131").change();
    });
    $("#" + year + "168, #" + year + "164, #" + year + "141, #" + year + "150").change(function () {
        var sum_cols = [year + "168", year + "164", year + "141", year + "150"];
        $("#" + year + "170").val(get_sum(sum_cols));
        $("#" + year + "170").change();
    });

    $("#" + year + "144, #" + year + "149").change(function () {
        var sum_cols = [year + "144", year + "149"];
        $("#" + year + "150").val(get_sub(sum_cols));
        $("#" + year + "150").change();
    });
    $("#" + year + "145, #" + year + "146, #" + year + "147, #" + year + "148").change(function () {
        var sum_cols = [year + "145", year + "146", year + "147", year + "148"];
        $("#" + year + "144").val(get_sum(sum_cols));
        $("#" + year + "144").change();
        $("#" + year + "170").change();
    });

    $("#" + year + "153, #" + year + "154, #" + year + "155, #" + year + "156, #" + year + "157").change(function () {
        setTimeout(function () {
            var sum_cols = [year + "153", year + "154", year + "155", year + "156", year + "157"];
            $("#" + year + "164").val(get_sum(sum_cols));
            $("#" + year + "164").change();
        }, 2000);
    });

    $("#" + year + "158, #" + year + "159, #" + year + "160, #" + year + "161, #" + year + "162, #" + year + "163").change(function () {
        var sum_cols = [year + "158", year + "159", year + "160", year + "161", year + "162", year + "163"];
        $("#" + year + "157").val(get_sum(sum_cols));
        $("#" + year + "157").change();
        $("#" + year + "170").change();
    });

    $("#" + year + "166, #" + year + "167").change(function () {
        var sum_cols = [year + "166", year + "167"];
        $("#" + year + "168").val(get_sub(sum_cols));
        $("#" + year + "168").change();
    });


    $("#" + year + "29, #" + year + "20").change(function () {
        //modified
        var val = isFinite($("#" + year + "29").val() / $("#" + year + "20").val()) ? approx(($("#" + year + "29").val() / $("#" + year + "20").val()) * 100, 2) : 'NA';
        $("#" + year + "175").val(val);
    });



    $("#" + year + "103, #" + year + "116").change(function () {
        var val = isFinite($("#" + year + "103").val() / $("#" + year + "116").val()) ? $("#" + year + "103").val() / $("#" + year + "116").val() : 'NA';
        $("#" + year + "183").val(approx(val, 4));
    });
    $("#" + year + "78, #" + year + "83, #" + year + "94, #" + year + "97, #" + year + "116").change(function () {
        var sum = get_sum([year + "78", year + "83", year + "94", year + "97"]);
        var val = isFinite(sum / $("#" + year + "116").val()) ? sum / $("#" + year + "116").val() : 'NA';
        $("#" + year + "184").val(approx(val, 4));
    });
    $("#" + year + "103, #" + year + "126, #" + year + "116").change(function () {
        var sum = get_sub([year + "103", year + "126"]);
        var val = isFinite(sum / $("#" + year + "116").val()) ? sum / $("#" + year + "116").val() : 'NA';
        $("#" + year + "185").val(approx(val, 4));
    });
    $("#" + year + "101, #" + year + "78, #" + year + "83, #" + year + "116").change(function () {
        //modified
        var sum = get_sum([year + "101", year + "78", year + "83"]);
        var val = isFinite(sum / $("#" + year + "116").val()) ? sum / $("#" + year + "116").val() : 'NA';
        $("#" + year + "187").val(approx(val, 3));
    });
    $("#" + year + "95, #" + year + "141").change(function () {
        var val = $("#" + year + "95").val() == 0 ? 999 : 'NA';
        if (val == 'NA') {
            val = isFinite(Number($("#" + year + "141").val()) / $("#" + year + "95").val()) ? Number($("#" + year + "141").val()) / $("#" + year + "95").val() : 'NA';
        }
        $("#" + year + "189").val(approx(val, 4));
    });
    $("#" + year + "95, #" + year + "141, #" + year + "131").change(function () {
        var val = $("#" + year + "95").val() == 0 ? 999 : 'NA';
        if (val == 'NA') {
            val = isFinite(($("#" + year + "141").val() - $("#" + year + "131").val()) / $("#" + year + "95").val()) ? ($("#" + year + "141").val() - $("#" + year + "131").val()) / $("#" + year + "95").val() : 'NA';
        }
        $("#" + year + "191").val(approx(val, 4));
    });

    $("#" + year + "20, #" + year + "128").change(function () {
        var val = isFinite(($("#" + year + "128").val() / $("#" + year + "20").val()) * 365) ? ($("#" + year + "128").val() / $("#" + year + "20").val()) * 365 : 'NA';
        $("#" + year + "196").val(approx(val, 2));
    });
    $("#" + year + "79, #" + year + "29").change(function () {
        var val = isFinite(($("#" + year + "79").val() / $("#" + year + "29").val()) * 365) ? ($("#" + year + "79").val() / $("#" + year + "29").val()) * 365 : 'NA';
        $("#" + year + "197").val(approx(val, 2));
    });
    $("#" + year + "79, #" + year + "29").change(function () {
        var val = isFinite(($("#" + year + "79").val() / $("#" + year + "29").val()) * 365) ? ($("#" + year + "79").val() / $("#" + year + "29").val()) * 365 : 'NA';
        $("#" + year + "197").val(approx(val, 2));
    });
    $("#" + year + "27, #" + year + "28, #" + year + "31, #" + year + "86, #" + year + "220").change(function () {
        var sum = get_sum([year + "27", year + "28", year + "31"]);
        sum = sum * $("#" + year + "220").val();
        var val = isFinite(($("#" + year + "86").val() / sum) * 365) ? ($("#" + year + "86").val() / sum) * 365 : 'NA';
        $("#" + year + "199").val(approx(val, 2));
    });
    ////////////////////////////////////////////////////

    $("#" + year + "41, #" + year + "38, #" + year + "20").change(function () {
        var sum = get_sum([year + "41", year + "38"]);
        //modified
        var val = isFinite(sum / $("#" + year + "20").val()) ? perc(sum / $("#" + year + "20").val()) : 'NA';
        $("#" + year + "212").val(approx(val, 2));
    });
    $("#" + year + "69, #" + year + "20").change(function () {
        //modified
        var val = isFinite($("#" + year + "69").val() / $("#" + year + "20").val()) ? perc($("#" + year + "69").val() / $("#" + year + "20").val()) : 'NA';
        $("#" + year + "213").val(approx(val, 2));
    });
    $("#" + year + "220, #" + year + "20, #" + year + "170").change(function () {
        var val = isFinite(($("#" + year + "20").val() * $("#" + year + "220").val()) / $("#" + year + "170").val()) ? ($("#" + year + "20").val() * $("#" + year + "220").val()) / $("#" + year + "170").val() : 'NA';
        $("#" + year + "214").val(approx(val, 2));
    });
    $("#" + year + "23, #" + year + "121, #" + year + "170").change(function () {
        var val = $("#" + year + "23").val() > 0 && $("#" + year + "170").val() > 0 && $("#" + year + "121").val() > 0 ? 1 : 0;
        $("#" + year + "219").val(val);
    });
    ////////////////////////////////////////////////////
    $("#" + year + "220, #" + year + "221").change(function () {
        var val = $("#" + year + "220").val() * $("#" + year + "221").val();
        $("#" + year + "222").val(val);
    });
    $("#" + year + "13").change(function () {

        var val;
        if ($("#" + year + "13").val() == 3) {
            val = 4;
        } else if ($("#" + year + "13").val() == 6) {
            val = 2;
        } else if ($("#" + year + "13").val() == 9) {
            val = 1 / 0.75;
        } else if ($("#" + year + "13").val() == 12) {
            val = 1;
        }
        $("#" + year + "220").val(val);
        $("#" + year + "220").change();
    });
    $("#G8").change(function () {
        //modified
        var val;
        if ($("#G8").val() == 'Actuals') {
            val = 1;
        } else if ($("#G8").val() == 'Thousands') {
            val = 1000;
        } else if ($("#G8").val() == 'Millions') {
            val = 1000000;
        }
        $("#" + year + "221").val(val);
        $("#" + year + "221").change();
    });
    ////////////////////////////////////////////////////

    ////////////////////////////////////////////////////
    $("#" + year + "222, #" + year + "23").change(function () {
        var val = $("#" + year + "222").val() * $("#" + year + "23").val();
        $("#" + year + "223").val(val);
    });
    $("#" + year + "222, #" + year + "20").change(function () {
        var val = $("#" + year + "222").val() * $("#" + year + "20").val();
        $("#" + year + "224").val(val);
    });
    ////////////////////////////////////////////////////
    $("#C218, #C213, #D213, #E213").change(function () {
        $("#" + year + "178").val('NA');
    });
    ////////////////////////////////////////////////////
    $("#" + year + "78, #" + year + "83, #" + year + "94, #" + year + "97, #" + year + "126, #" + year + "41, #" + year + "38, #" + year + "220").change(function () {
        var sum = get_sum([year + "78", year + "83", year + "94", year + "97"]);
        var sum2 = get_sum([year + "41", year + "38"]);
        var val = (sum - $("#" + year + "126").val()) / (sum2 * $("#" + year + "220").val());
        val = isFinite(val) ? val : 'NA';
        $("#" + year + "186").val(approx(val, 4));
    });
    $("#" + year + "141, #" + year + "95, #" + year + "221").change(function () {
        var val = ($("#" + year + "141").val() - $("#" + year + "95").val()) * $("#" + year + "221").val();
        val = isFinite(val) ? val : 'NA';
        $("#" + year + "190").val(val);
    });
    $("#" + year + "131, #" + year + "128, #" + year + "79, #" + year + "86, #" + year + "221").change(function () {
        var sum = get_sum([year + "131", year + "128"]);
        //modified
        var val = (sum - $("#" + year + "79").val() - $("#" + year + "86").val()) * $("#" + year + "221").val();
        val = isFinite(val) ? val : 'NA';
        $("#" + year + "192").val(val);
    });

    $("#" + year + "83, #" + year + "43, #" + year + "69, #" + year + "220").change(function () {
        var sum = get_sum([year + "83", year + "43"]);
        if (sum == 0) {
            $("#" + year + "200").val(approx(999, 2));
            return;
        }
        var val = ($("#" + year + "69").val() * $("#" + year + "220").val()) / ($("#" + year + "83").val() + ($("#" + year + "43").val() * $("#" + year + "220").val()));
        val = isFinite(val) ? val : 'NA';
        $("#" + year + "200").val(approx(val, 2));
    });
    $("#" + year + "60, #" + year + "43, #" + year + "220, #" + year + "101, #" + year + "116, #" + year + "78, #" + year + "82").change(function () {
        var sum = get_sum([year + "60", year + "43"]);
        var sum2 = get_sum([year + "101", year + "116", year + "78", year + "82"]);
        var val = (sum * $("#" + year + "220").val()) / sum2;
        val = isFinite(val) ? val : 'NA';
        $("#" + year + "203").val(approx(val, 2));
    });
    $("#" + year + "224, #E224").change(function () {
        var prev_year = parseInt(year) - 1;
        var val = ($("#" + year + "224").val() - $("#" + prev_year + "224").val()) / $("#" + prev_year + "224").val();
        //modified
        val = isFinite(val) ? perc(val) : 'NA';
        $("#" + year + "204").val(approx(val, 2));
    });
    ////////////////////////////////////////////////////
    $("#C218, #C214, #D214, #E214").change(function () {
        $("#" + year + "205").val('NA');
    });
    $("#" + year + "192, #" + year + "20, #" + year + "222").change(function () {
        var val = ($("#" + year + "192").val() / ($("#" + year + "20").val() * $("#" + year + "222").val()));
        val = isFinite(val) ? val : 'NA';
        $("#" + year + "206").val(approx(val, 2));
    });

    $("#" + year + "101, #" + year + "78, #" + year + "83, #" + year + "73, #" + year + "38, #" + year + "220").change(function () {
        var sum = get_sum([year + "101", year + "78", year + "83"]);
        if (sum == 0) {
            $("#" + year + "207").val(approx(999, 2));
            return;
        }
        var sum2 = get_sum([year + "73", year + "38"]);
        var val = (sum2 * $("#" + year + "220").val()) / sum;
        val = isFinite(val) ? val : 'NA';
        $("#" + year + "207").val(approx(val, 2));
    });
    //$("#" + year + "13").change();
    adjust_totals_s(year);
}//end function

function get_sum(sum_cols) {
    var sum = 0;
    var col;
    for (var i = 0; i < sum_cols.length; i++) {
        col = $("#" + sum_cols[i]).val();
        if (col == '') {
            col = 0;
        }
        sum += Number(col);
    }
    return sum;
}

function get_sub(sum_cols) {
    var sum = 0;
    var col;
    for (var i = 0; i < sum_cols.length; i++) {
        col = $("#" + sum_cols[i]).val();
        if (col == '') {
            col = 0;
        }

        sum -= Number(col);

        if (i == 0) {
//            alert(sum);
            sum *= -1;
//            alert(sum);
        }
    }
    return sum;
}

function get_sum_sub(sum_cols) {
    var sum = 0;
    var col;
    for (var i = 0; i < sum_cols.length; i++) {
        col = $("#" + sum_cols[i]).val();
        if (col == '') {
            col = 0;
        }
        if (i < 2) {
            sum += Number(col);
        }
        else {
            sum -= Number(col);
        }
//        if (i == 0) {
//            sum *= -1;
//        }
    }
    return sum;
}

function perc(val) {
    val = parseFloat(Math.round(val * 100 * 100) / 100).toFixed(2);
    return val;
}
function approx(val, to) {
    if (val != 'NA') {
        var times = 1;
        for (var i = 0; i < to; i++) {
            times = times * 10;
        }
        val = parseFloat(Math.round(val * times) / times).toFixed(to);
    }
    return val;
}

function adjust_totals_s(year) {

    for (var i = 0; i <= 224; i++) {
        $("#" + year + i).change();
    }

    $("#" + year + "60").change();

    $("#" + year + "69").change();

    $("#" + year + "128").change();

    $("#" + year + "131").change();

    $("#" + year + "195").change();

    $("#" + year + "220").change();

    $("#" + year + "218").change();

    $("#G8").change();

    setTimeout(function () {
        $("#" + year + "164").trigger("change");

        $("#" + year + "170").trigger("change");
    }, 3000);


}