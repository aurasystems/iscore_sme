$excel = New-Object -ComObject Excel.Application
$excel.DisplayAlerts = $false;
$filelocation = "$file_path$.xlsx"
$Workbook = $Excel.Workbooks.Open($filelocation,0, $false)
$Workbook.SaveAs($filelocation)
$Workbook.Saved = $true
$Workbook.Close()
$excel.Quit()